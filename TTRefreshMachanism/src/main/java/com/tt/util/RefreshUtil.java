package com.tt.util;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tt.dao.HibernateUtil;

/**
 * Utility class
 * 
 * @author Infosys
 * @version 1.0
 */
public class RefreshUtil
{
	@Autowired
	private static SessionFactory sessionFactory;
	private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * Converts String to Date
	 * 
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public static Date convertStringToDate(String dateString) throws ParseException
	{
		Date date = dateString == null || dateString.equals("") ? null : FORMATTER.parse(dateString);
		return date;
	}

	public static Date convertStringToOnlyDate(String dateString) throws ParseException
	{
		Date date = dateString == null || dateString.equals("") ? null : DATE_FORMATTER.parse(dateString);
		return date;
	}
	
	public static Date addDays(String dateString, int days) throws ParseException
	{
		Date date = dateString == null || dateString.equals("") ? null : FORMATTER.parse(dateString);
		Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        
		return cal.getTime();
	}
	
	public static Date addHours(String dateString, int hours) throws ParseException
	{
		Date date = dateString == null || dateString.equals("") ? null : FORMATTER.parse(dateString);
		Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours);
        
		return cal.getTime();
	}
	
	public static String convertDateToString(Date date)
	{
		String dateString = date == null? null : FORMATTER.format(date);
		return dateString;
	}
    /** Check whether under Tenoss Maintenance **/
    public static int isTenossMaintenance(String serviceName, String customerId){
    	Session session = HibernateUtil.getSessionFactory().openSession();
    	
    	StringBuilder queryString = new StringBuilder();
		//queryString.append("select count(1) from configurationitem where rootProductId=:serviceName and customeruniqueid=:customerId");
		//queryString.append("select count(1) from ten2infi where customeruniqueid=:customerId and CIDeliveryStatus in ('Detailed Design','Order Received')");
		queryString.append("select count(1) from ten2infi where customeruniqueid=:customerId");
		Query query = session.createSQLQuery(queryString.toString());
		//query.setString("serviceName", serviceName);
		query.setString("customerId", customerId);
		
		int count = ((BigInteger) query.uniqueResult()).intValue();
		
		session.close();
		return count;
    }
}
