package com.tt;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response;
import com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response.Changes.Change;
import com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource;
import com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService;
import com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval;
import com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response.Changes.Change.Comments.Comment;
import com.tt.common.RefreshUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ChangeRequestApprovals;
import com.tt.model.servicerequest.ChangeRequestDevices;
import com.tt.model.servicerequest.ChangeRequestNotes;
import com.tt.model.servicerequest.ChangeRequestServices;


public class CreateChangeRequestList {


    private final static Logger logger = Logger.getLogger(CreateServiceRequestList.class);
    
    public List<ChangeRequest> createChangeRequest(Response changeRequestResponse, String customerId, String toDate, String fromDate) throws ParseException {
    	
    	List<ChangeRequest> changeRequestList = new ArrayList<ChangeRequest>();
    	for (Change change :  changeRequestResponse.getChanges().getChange()){
    		ChangeRequest changeRequest = new ChangeRequest();
        	changeRequest.setPlannedstartdate(RefreshUtil.convertStringToDate(change.getPlannedStartDate()));
        	changeRequest.setPlannedenddate(RefreshUtil.convertStringToDate(change.getPlannedEndDate()));
        	changeRequest.setCategory(change.getCategory());
        	changeRequest.setRisk(change.getRisk());
        	
        	logger.debug("Priority from Snow :: " + Integer.toString(change.getPriority()));
        	
        	if(null != Integer.toString(change.getPriority())){
        		changeRequest.setPriority(Integer.toString(change.getPriority()));	
        	}else
        		changeRequest.setPriority("");
        	logger.debug("Priority stored on portal " + changeRequest.getPriority());
        	
        	changeRequest.setImpact(change.getImpact());
        	changeRequest.setResolvergroup(change.getAssignmentResolverGroupName());
        	changeRequest.setCasemanager(change.getAssigneeName());
        	changeRequest.setOutagepresent(change.getOutageRequired());
        	
        	if("Yes".equalsIgnoreCase(change.getOutageRequired()))
        	{
        		changeRequest.setOutagestarttime(RefreshUtil.convertStringToDate(change.getOutageStartTime()));
        		changeRequest.setOutageendtime(RefreshUtil.convertStringToDate(change.getOutageEndTime()));
        	}
        	else
        	{
        		changeRequest.setOutagestarttime(null);
        		changeRequest.setOutageendtime(null);
        	}
        	
        	changeRequest.setSitename(change.getCustomerSite());// this not getting populated from wsdl
        	changeRequest.setChangerequestid(change.getChangeNumber());
        	changeRequest.setSubject(change.getShortDescription());
        	changeRequest.setDescription(change.getDescription());
        	changeRequest.setRequestor(change.getRequestedBy());
        	changeRequest.setStatus(change.getTicketStatus());
        	changeRequest.setCreateddate(RefreshUtil.convertStringToDate(change.getCreated()));
        	changeRequest.setLastupdatedate(RefreshUtil.convertStringToDate(change.getUpdated()));
        	changeRequest.setChangesource(change.getChangeSource());
        	changeRequest.setChangetype(change.getType());
        	changeRequest.setRequestedbydate(RefreshUtil.convertStringToDate(change.getRequestedByDate()));
        	changeRequest.setCustomerapproval(change.getCustomerApproval());
        	changeRequest.setCustomerapprover(change.getCustomerApproverName());
        	changeRequest.setCustomerapprovaldate(RefreshUtil.convertStringToDate(change.getCustomerApprovalDate()));
        	changeRequest.setCustomerapprovalstatus(change.getCustomerApprovalStatus());
        	changeRequest.setOutcomestatus(change.getOutcomeStatus());
        	
        	List<ChangeRequestDevices> changeRequestDevicesList = new ArrayList<ChangeRequestDevices>();
        	
        	if(null != change.getAffectedResources() && null != change.getAffectedResources().getAffectedResource() && change.getAffectedResources().getAffectedResource().size()>0)
        		for (AffectedResource  affectedResources :  change.getAffectedResources().getAffectedResource()){
        			ChangeRequestDevices changeRequestDevices = new ChangeRequestDevices();
        			changeRequestDevices.setCiid(affectedResources.getCiId());
        			changeRequestDevices.setCiname(affectedResources.getCiName());
        			changeRequestDevices.setChangerequestid(changeRequest);
        			changeRequestDevicesList.add(changeRequestDevices);
        		}
        	
        	List<ChangeRequestServices> changeRequestServiceList = new ArrayList<ChangeRequestServices>();
        	
        	if(null != change.getAffectedServices() && null != change.getAffectedServices().getAffectedService() && change.getAffectedServices().getAffectedService().size()>0)
        		for (AffectedService affectedService : change.getAffectedServices().getAffectedService()){
        			ChangeRequestServices changeRequestServices = new ChangeRequestServices();
        			changeRequestServices.setChangerequestid(changeRequest);
        			changeRequestServices.setServiceid(affectedService.getCiId());
        			changeRequestServices.setServicename(affectedService.getCiName());
        			changeRequestServiceList.add(changeRequestServices);
        		}
        	
        	List<ChangeRequestNotes> changeRequestNoteList = new ArrayList<ChangeRequestNotes>();
        	
        	if(null != change.getComments() && null != change.getComments().getComment() && change.getComments().getComment().size()>0)
        		for (Comment comment : change.getComments().getComment()){
        			ChangeRequestNotes changeRequestNotes = new ChangeRequestNotes();
        			changeRequestNotes.setUserid(comment.getUserid());
        			changeRequestNotes.setCreateddatetime(RefreshUtil.convertStringToDate(comment.getDatetime()));
        		    String note=new String();
        			if(comment.getNote().length()>5000)
        				note = comment.getNote().substring(0,4999);
        			changeRequestNotes.setNotes(note);
        			changeRequestNotes.setChangerequestid(changeRequest);
        			changeRequestNoteList.add(changeRequestNotes);
        		}
        	
        	List<ChangeRequestApprovals> changeRequestApprovalList = new ArrayList<ChangeRequestApprovals>();
        	
        	if(null != change.getApprovals() && null != change.getApprovals().getApproval() && change.getApprovals().getApproval().size() > 0)
        	{
        		for(Approval approval : change.getApprovals().getApproval())
        		{
        			ChangeRequestApprovals changeRequestApprovals = new ChangeRequestApprovals();
        			changeRequestApprovals.setApprover(approval.getApprover());
        			changeRequestApprovals.setApprovaldate(RefreshUtil.convertStringToDate(approval.getApprovalDateTime()));
        			
        			/* customerapprovalstatus is also used to store tt approval status
        			 * if we have got approval date from snow apis then it is approved.
        			 * For customer initiated change request if approval date is 
        			 * obtained as null then TT approval status is pending.    
        			 * */
        			if(approval.getApprovalDateTime() == null)
        			{
        				changeRequest.setCustomerapprovalstatus(GenericConstants.PENDING);
        			}
        			else
        			{
        				changeRequest.setCustomerapprovalstatus(GenericConstants.APPROVED);
        			}
        			changeRequestApprovals.setApprovalgroup(approval.getApprovalGroup());
        			changeRequestApprovals.setChangerequestid(changeRequest);
        			changeRequestApprovalList.add(changeRequestApprovals);
        		}
        	}
        	
        	changeRequest.setChangeRequestNotes(changeRequestNoteList);
        	changeRequest.setChangeRequestDevices(changeRequestDevicesList);
        	changeRequest.setChangeRequestServices(changeRequestServiceList);
        	changeRequest.setChangeRequestApprovals(changeRequestApprovalList);
        	
        	changeRequestList.add(changeRequest);
    	}
    	
    	
    	
    	return changeRequestList;
    	
    }
}
