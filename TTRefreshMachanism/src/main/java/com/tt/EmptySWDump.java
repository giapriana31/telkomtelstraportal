package com.tt;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;

import com.tt.utils.PropertyReader;

public class EmptySWDump {
	
	Properties prop = PropertyReader.getProperties();
	File archiveDirectory = new File(
			prop.getProperty("SWReportsPath"));		
	
	public void EmptyDump(String category){
		

		File archiveDirectory = new File(
				prop.getProperty("SWReportsPath"));		
		
		deleteDirectory(archiveDirectory,category);
		
	
	}
	
	
public void deleteDirectory (File file,String category){
		
	
	  if (!file.exists())
          return;
      
      //if directory, go inside and call recursively
      if (file.isDirectory()) {
    	  System.out.println("Directory "+file.getName());
          for (File f : file.listFiles()) {
              //call recursively
              deleteDirectory(f,category);
          }
      }
      else{
    		if(file.getParent().toLowerCase().contains(category.toLowerCase())){
    			file.delete();
    		}
    	  
      }
	}
	
}
