package com.tt;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.tt.client.model.retrieveOutage.ExecuteResponse.Response;
import com.tt.client.model.retrieveOutage.ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService;
import com.tt.logging.Logger;
import com.tt.model.AffectedServices;
import com.tt.model.Outage;
import com.tt.util.RefreshUtil;


public class CreateOutagesList 
{
	private final static Logger logger = Logger.getLogger(CreateOutagesList.class);
	
	public List<Outage> createOutageList(Response outageResponse, String customerId, String fromDate, String toDate) throws ParseException
	{
		logger.debug("Class - CreateOutagesList, Method - createOutageList, Start");
		
		List<Outage> outagesList = new ArrayList<Outage>();
		
		
		if(outageResponse != null && outageResponse.getOutages() != null && !outageResponse.getOutages().getOutage().isEmpty())
		{
			for(com.tt.client.model.retrieveOutage.ExecuteResponse.Response.Outages.Outage outageFromResponse : outageResponse.getOutages().getOutage())
			{
				boolean isAffectedServicesAdded = false; 
				
				Outage outage = new Outage();
				
				outage.setIncidentid(outageFromResponse.getTaskNumber());
				outage.setContractedoutage(outageFromResponse.getContractualDuration());
				outage.setExcludedFlag(outageFromResponse.getExcludeFromAvailability());
				outage.setIncidentcloseddate(RefreshUtil.convertStringToDate(outageFromResponse.getIncidentClosedDate()));
				outage.setOutagestarttime(RefreshUtil.convertStringToDate(outageFromResponse.getBegin()));
				outage.setOutageendtime(RefreshUtil.convertStringToDate(outageFromResponse.getEnd()));
				outage.setRootProductId(outageFromResponse.getRootProductId());
				if(null != outageFromResponse.getCategory() && outageFromResponse.getCategory().startsWith("Mandoe")
						&& "SaaS".equalsIgnoreCase(outageFromResponse.getRootProductId())){
					outage.setCategory("Mandoe");
				}else{
					outage.setCategory(outageFromResponse.getCategory());
				}
				
				
				List<AffectedServices> affectedServicesList = new ArrayList<AffectedServices>();
				
				if(null != outageFromResponse.getAffectedServices() && null != outageFromResponse.getAffectedServices().getAffectedService() && !outageFromResponse.getAffectedServices().getAffectedService().isEmpty())
				{
					for(AffectedService affectedServiceResponse : outageFromResponse.getAffectedServices().getAffectedService())
					{
						AffectedServices affectedServices = new AffectedServices();
						affectedServices.setCiid(affectedServiceResponse.getCiId());
						affectedServices.setSiteId(affectedServiceResponse.getSiteId());
						affectedServices.setCmdbClass(affectedServiceResponse.getCmdbClass());
						affectedServices.setCustomerId(affectedServiceResponse.getCustomerId());
						affectedServices.setServiceoutageid(outage);
						
						if(affectedServiceResponse.getCustomerId().equalsIgnoreCase(customerId))
						{
							affectedServicesList.add(affectedServices);
							isAffectedServicesAdded = true;
						}
					}
				}
				
				outage.setAffectedServices(affectedServicesList);
				
				if(isAffectedServicesAdded)
				{
					outagesList.add(outage);
				}
			}
		}
		
		logger.debug("outageList :: " + outagesList.size());
		
		return outagesList;
	}
}
