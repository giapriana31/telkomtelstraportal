package com.tt;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.tt.batchrefresh.model.ci.Incident;
import com.tt.dao.ServiceRequestDao;
import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ChangeRequestCustomer;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.utils.PropertyReader;

public class NotificationAPI 
{
	private static final Properties prop = PropertyReader.getProperties();
	private static final Logger logger = Logger.getLogger(NotificationAPI.class.getName());
	
	static
	{
		logger.debug("NotificationAPI --------------------------");
		logger.debug(prop.getProperty("Notification.API.URL"));
		logger.debug(prop.getProperty("Portal.Admin.UserID"));
		logger.debug(prop.getProperty("Portal.Admin.Password"));
		logger.debug("NotificationAPI --------------------------");
	}
	
	public void sendChangeRequest(List<ChangeRequest> changeRequestList,String fromDate,String toDate, String customerId)
	{
		try
		{
			Date from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fromDate);
			Date to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(toDate);			
			long diff =  Math.abs(from.getTime() - to.getTime());
			ServiceRequestDao dao = new ServiceRequestDao();
			
			if(prop.getProperty("Notification.CR.Hourly").trim().equalsIgnoreCase("true"))
			{
				logger.debug("NotificationAPI --> CR --> Hourly");
				long diffMinutes = diff / (60 * 1000) ;
				long propertyMinutes = Long.parseLong(prop.getProperty("Notification.CR.Hourly.Time").trim(), 10) * 60;
				
				if(diffMinutes <= propertyMinutes)
				{	
					for(ChangeRequest cr : changeRequestList)
					{
						List<ChangeRequestCustomer> changeRequestCustomer = dao.getChangeRequestCustomerList(cr.getChangerequestid());
						if(null != changeRequestCustomer && changeRequestCustomer.size()>0){
							for(ChangeRequestCustomer cust : changeRequestCustomer)
								sendNotification(cr.getChangerequestid(),cr.getChangerequestid()+" "+prop.getProperty("CR.Created/Updated.Message")+" "+cr.getStatus()+".",prop.getProperty("CR.view.URL")+DatatypeConverter.printBase64Binary((cr.getChangerequestid()+"&Change").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,cust.getCustomeruniqueid());
						}else{
							sendNotification(cr.getChangerequestid(),cr.getChangerequestid()+" "+prop.getProperty("CR.Created/Updated.Message")+" "+cr.getStatus()+".",prop.getProperty("CR.view.URL")+DatatypeConverter.printBase64Binary((cr.getChangerequestid()+"&Change").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,customerId);
						}
					}
				}
			}
			else if(prop.getProperty("Notification.CR.Weekly").trim().equalsIgnoreCase("true"))
			{
				logger.debug("NotificationAPI --> CR --> Weekly");
				long diffDays = diff / (24 * 60 * 60 * 1000);
				long propertyDays = Long.parseLong(prop.getProperty("Notification.CR.Weekly.Days").trim(), 10);
				
				if(diffDays <= propertyDays)
				{	
					for(ChangeRequest cr : changeRequestList)
					{
						List<ChangeRequestCustomer> changeRequestCustomer = dao.getChangeRequestCustomerList(cr.getChangerequestid());
						if(null != changeRequestCustomer && changeRequestCustomer.size()>0){
							for(ChangeRequestCustomer cust : changeRequestCustomer)
								sendNotification(cr.getChangerequestid(),cr.getChangerequestid()+" "+prop.getProperty("CR.Created/Updated.Message")+" "+cr.getStatus()+".",prop.getProperty("CR.view.URL")+DatatypeConverter.printBase64Binary((cr.getChangerequestid()+"&Change").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,cust.getCustomeruniqueid());
						}else{
							sendNotification(cr.getChangerequestid(),cr.getChangerequestid()+" "+prop.getProperty("CR.Created/Updated.Message")+" "+cr.getStatus()+".",prop.getProperty("CR.view.URL")+DatatypeConverter.printBase64Binary((cr.getChangerequestid()+"&Change").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,customerId);
						}
					}
				}
							
			}
			else
			{
				logger.debug("NotificationAPI --> CR --> NO TIME");
				{	
					for(ChangeRequest cr : changeRequestList)
					{
						List<ChangeRequestCustomer> changeRequestCustomer = dao.getChangeRequestCustomerList(cr.getChangerequestid());
						if(null != changeRequestCustomer && changeRequestCustomer.size()>0){
							for(ChangeRequestCustomer cust : changeRequestCustomer)
								sendNotification(cr.getChangerequestid(),cr.getChangerequestid()+" "+prop.getProperty("CR.Created/Updated.Message")+" "+cr.getStatus()+".",prop.getProperty("CR.view.URL")+DatatypeConverter.printBase64Binary((cr.getChangerequestid()+"&Change").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,cust.getCustomeruniqueid());
						}else{
							sendNotification(cr.getChangerequestid(),cr.getChangerequestid()+" "+prop.getProperty("CR.Created/Updated.Message")+" "+cr.getStatus()+".",prop.getProperty("CR.view.URL")+DatatypeConverter.printBase64Binary((cr.getChangerequestid()+"&Change").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,customerId);
						}
						
						
					}
				}
					
			}
			
		}
		catch(Exception e)
		{
			logger.error(e);
		}
	}
	
	public void sendIncident(List<Incident> incidentList,String fromDate,String toDate)
	{
		try
		{
			Date from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fromDate);
			Date to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(toDate);			
			long diff =  Math.abs(from.getTime() - to.getTime());
			
			if(prop.getProperty("Notification.Incident.Hourly").trim().equalsIgnoreCase("true"))
			{
				logger.debug("NotificationAPI --> Incident --> Hourly");
				long diffMinutes = diff / (60 * 1000) ;
				long propertyMinutes = Long.parseLong(prop.getProperty("Notification.Incident.Hourly.Time").trim(), 10) * 60;
				
				if(diffMinutes <= propertyMinutes)
					for(Incident incd : incidentList)
						sendNotification(incd.getIncidentid(),incd.getIncidentid()+" "+prop.getProperty("Incident.Created/Updated.Message")+" "+incd.getIncidentstatus()+".",prop.getProperty("Incident.view.URL")+DatatypeConverter.printBase64Binary(incd.getIncidentid().getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,incd.getCustomeruniqueid());	
			}
			else if(prop.getProperty("Notification.Incident.Weekly").trim().equalsIgnoreCase("true"))
			{
				logger.debug("NotificationAPI --> Incident --> Weekly");
				long diffDays = diff / (24 * 60 * 60 * 1000);
				long propertyDays = Long.parseLong(prop.getProperty("Notification.Incident.Weekly.Days").trim(), 10);
				
				if(diffDays <= propertyDays)
					for(Incident incd : incidentList)
						sendNotification(incd.getIncidentid(),incd.getIncidentid()+" "+prop.getProperty("Incident.Created/Updated.Message")+" "+incd.getIncidentstatus()+".",prop.getProperty("Incident.view.URL")+DatatypeConverter.printBase64Binary(incd.getIncidentid().getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,incd.getCustomeruniqueid());	
			}
			else
			{
				logger.debug("NotificationAPI --> Incident --> NO TIME");
				for(Incident incd : incidentList)
					sendNotification(incd.getIncidentid(),incd.getIncidentid()+" "+prop.getProperty("Incident.Created/Updated.Message")+" "+incd.getIncidentstatus()+".",prop.getProperty("Incident.view.URL")+DatatypeConverter.printBase64Binary(incd.getIncidentid().getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,incd.getCustomeruniqueid());
			}
			
		}
		catch(Exception e)
		{
			logger.error(e);
		}
	}
	
	public void sendServiceRequest(List<ServiceRequest> serviceRequestList,String fromDate,String toDate)
	{
		try
		{
			Date from = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fromDate);
			Date to = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(toDate);			
			long diff =  Math.abs(from.getTime() - to.getTime());
			
			if(prop.getProperty("Notification.SR.Hourly").trim().equalsIgnoreCase("true"))
			{
				logger.debug("NotificationAPI --> Service Request --> Hourly");
				long diffMinutes = diff / (60 * 1000) ;
				long propertyMinutes = Long.parseLong(prop.getProperty("Notification.SR.Hourly.Time").trim(), 10) * 60;
				
				if(diffMinutes <= propertyMinutes)
					for(ServiceRequest sr : serviceRequestList)
						sendNotification(sr.getRequestid(),sr.getRequestid()+" "+prop.getProperty("SR.Created/Updated.Message")+" "+sr.getStatus()+".",prop.getProperty("SR.view.URL")+DatatypeConverter.printBase64Binary((sr.getRequestid()+"&Service").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,sr.getCustomeruniqueid());	
			}
			else if(prop.getProperty("Notification.SR.Weekly").trim().equalsIgnoreCase("true"))
			{
				logger.debug("NotificationAPI --> Service Request --> Weekly");
				long diffDays = diff / (24 * 60 * 60 * 1000);
				long propertyDays = Long.parseLong(prop.getProperty("Notification.SR.Weekly.Days").trim(), 10);
				
				if(diffDays <= propertyDays)
					for(ServiceRequest sr : serviceRequestList)
						sendNotification(sr.getRequestid(),sr.getRequestid()+" "+prop.getProperty("SR.Created/Updated.Message")+" "+sr.getStatus()+".",prop.getProperty("SR.view.URL")+DatatypeConverter.printBase64Binary((sr.getRequestid()+"&Service").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,sr.getCustomeruniqueid());	
			}
			else
			{
				logger.debug("NotificationAPI --> Service Request --> NO TIME");
				for(ServiceRequest sr : serviceRequestList)
					sendNotification(sr.getRequestid(),sr.getRequestid()+" "+prop.getProperty("SR.Created/Updated.Message")+" "+sr.getStatus()+".",prop.getProperty("SR.view.URL")+DatatypeConverter.printBase64Binary((sr.getRequestid()+"&Service").getBytes()),"TTPortal",prop.getProperty("Portal.Admin.UserID"),null,sr.getCustomeruniqueid());
			}
			
		}
		catch(Exception e)
		{
			logger.error(e);
		}
	}
	
	public void sendNotification(String title,String description,String URL,String producerSystem,String producerUser,String email,String custmerID)
	{
		if(title==null || title.trim().equalsIgnoreCase(""))
			title= "NULL";
		if(description==null || description.trim().equalsIgnoreCase(""))
			description= "NULL";
		if(URL==null || URL.trim().equalsIgnoreCase(""))
			URL= "NULL";
		if(producerSystem==null || producerSystem.trim().equalsIgnoreCase(""))
			producerSystem= "NULL";
		if(producerUser==null || producerUser.trim().equalsIgnoreCase(""))
			producerUser= "NULL";
		if(email==null || email.trim().equalsIgnoreCase(""))
			email= "NULL";
		if(custmerID==null || custmerID.trim().equalsIgnoreCase(""))
			custmerID= "NULL";
		
		try
		{
			String webPage = prop.getProperty("Notification.API.URL");
			webPage += "title/"+DatatypeConverter.printBase64Binary(title.getBytes());
			webPage += "/description/"+DatatypeConverter.printBase64Binary(description.getBytes());
			webPage += "/url/"+DatatypeConverter.printBase64Binary(URL.getBytes());
			webPage += "/producer-system/"+DatatypeConverter.printBase64Binary(producerSystem.getBytes()); 
			webPage += "/producer-user/"+DatatypeConverter.printBase64Binary(producerUser.getBytes());
			webPage += "/users-email/"+DatatypeConverter.printBase64Binary(email.getBytes());
			webPage += "/customer-id/"+DatatypeConverter.printBase64Binary(custmerID.getBytes());
			
			String userid = prop.getProperty("Portal.Admin.UserID");
			String password = prop.getProperty("Portal.Admin.Password");

			String authString = userid + ":" + password;
			String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));			

			URL url = new URL(webPage);
			URLConnection urlConnection = url.openConnection();
			urlConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			InputStream is = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);

			int numCharsRead;
			char[] charArray = new char[1024];
			StringBuffer sb = new StringBuffer();
			while ((numCharsRead = isr.read(charArray)) > 0) 
				sb.append(charArray, 0, numCharsRead);

			logger.info("Notification response for :::: "+title+"  ::::  "+sb.toString());
		}
		catch(Exception e)
		{
			logger.error(e);
		}
	}
}
