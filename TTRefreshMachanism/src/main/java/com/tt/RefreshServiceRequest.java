package com.tt;

import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;

import com.tt.client.common.WebServiceErrorCodes;
import com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response;
import com.tt.client.model.retrieveServiceRequest.ExecuteResponse;
import com.tt.client.service.incident.RetrieveCRWSCallService;
import com.tt.client.service.incident.ServiceRequestWSCallService;
import com.tt.dao.ServiceRequestDao;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.utils.RefreshUtil;
import com.tt.model.servicerequest.ChangeRequest;


public class RefreshServiceRequest
{

	private final static Logger logger = Logger.getLogger(RefreshServiceRequest.class);

	private List<ServiceRequest> serviceRequestList;
	private List<ChangeRequest> changeRequestList;
	
	private String fromDate = "";
	private String toDate = "";
	private String interval = "";
	private String customerId = "";
	
	public void refreshServiceRequest(String customerId, String toDate, String fromDate, String interval) throws ParseException {
		
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.interval = interval;
		this.customerId = customerId;
		
		RefreshUtil.setProxyObject();
		refreshChangeRequest(customerId, toDate, fromDate, interval);	
		
	    ExecuteResponse.Response serviceRequests = new ServiceRequestWSCallService().getRetrieveServiceRequestResponse(toDate, fromDate, customerId);
	    if (WebServiceErrorCodes.RI_SUCCESS.equals(serviceRequests.getErrorCode())) {
	    	
	      logger.debug("toDate--------->>" + toDate + "------------fromDate------->>" + fromDate);
	      this.serviceRequestList = new CreateServiceRequestList().createServiceRequest(serviceRequests.getServiceRequests(), customerId, toDate, fromDate);
	      
	      if(serviceRequestList!=null && serviceRequestList.size()>0)
				logger.warn("No. of SR/CR records updated after Batch Refresh--->>"+serviceRequestList.size());
	      
	      refreshServiceRequestThread();
	      /**
	      if(null !=interval && !"CustomBatch".equalsIgnoreCase(interval))
	    	  new NotificationAPI().sendServiceRequest(serviceRequestList,toDate,fromDate);
	      new ServiceRequestDao().refreshServiceRequest(serviceRequestList);
	      new ServiceRequestDao().createLastRefreshTimeEntry(customerId, toDate, "ServiceRequest", interval);
	      **/
	    }  else{
	      logger.error(serviceRequests.getErrorCode() + " | " + serviceRequests.getErrorMessage());
	      if ((serviceRequests != null) && (serviceRequests.getErrorCode() != null) && ("RI1003".equalsIgnoreCase(serviceRequests.getErrorCode()))) {
	        logger.debug("If No service request for that customer fetch :: " + customerId);
	        new ServiceRequestDao().createLastRefreshTimeEntry(customerId, toDate, "ServiceRequest", interval);
	      }
	    }
	  }
	
	public void refreshServiceRequestThread(){
		Thread tsr1 = new Thread(new Runnable(){

			@Override
			public void run() {
				sendChangeRequestNotif();
				
			}
			
		});
		
		Thread tsr2 = new Thread(new Runnable(){

			@Override
			public void run() {
				refreshChangeRequest();
				
			}
			
		});
		
		Thread tsr3 = new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					createLastRefreshTimeEntry("ServiceRequest");
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		tsr1.setName("Thread 1-sendServiceRequestNotif : " + customerId);
		tsr2.setName("Thread 2-refreshServiceRequest : " + customerId);
		tsr3.setName("Thread 3-createLastRefreshTimeEntrySR : " + customerId);
		
		try {
			tsr1.start();
			tsr1.join();
			tsr2.start();
			tsr2.join();
			tsr3.start();
			tsr3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void sendServiceRequestNotif(){
		if(null !=interval && !"CustomBatch".equalsIgnoreCase(interval)){
			 new NotificationAPI().sendServiceRequest(serviceRequestList,toDate,fromDate);
		}
	}
	
	public synchronized void refreshServiceRequest(){
		new ServiceRequestDao().refreshServiceRequest(serviceRequestList);
	}
	
	public synchronized void createLastRefreshTimeEntry(String ServiceName) throws ParseException{
		new ServiceRequestDao().createLastRefreshTimeEntry(customerId, toDate, ServiceName, interval);
	}
	
	public void refreshChangeRequest (String customerId, String toDate, String fromDate, String interval) throws ParseException{

		RefreshUtil.setProxyObject();
	    Response changeRequest = new RetrieveCRWSCallService().getRetrieveCRListResponse(toDate, fromDate, customerId);
	   
	    System.out.println("toDate--------->>" + toDate + "------------fromDate------->>" + fromDate);
	    if (WebServiceErrorCodes.RC_SUCCESS.equals(changeRequest.getErrorCode())){
	      
	    	this.changeRequestList = new CreateChangeRequestList().createChangeRequest(changeRequest, customerId, toDate, fromDate);
	    	
	    	if(changeRequestList!=null && changeRequestList.size()>0)
	    		logger.warn("Size of changeRequestList after batchRefresh--->>"+changeRequestList.size());
	      
	    	refreshChangeRequestThread();
	    	/**
	    	if(null !=interval && !"CustomBatch".equalsIgnoreCase(interval))
	    		new NotificationAPI().sendChangeRequest(changeRequestList,toDate,fromDate, customerId);
	    	new ServiceRequestDao().refreshChangeRequest(changeRequestList, customerId);
	    	new ServiceRequestDao().createLastRefreshTimeEntry(customerId, toDate, "ChangeRequest", interval);
	    	**/
	    } else { 
	    	System.out.println("NO DATA ------------"+changeRequest.getErrorCode() + " | " + changeRequest.getErrorMessage());
	    	
	    	if ((changeRequest != null) && (changeRequest.getErrorCode() != null) && ("RI1003".equalsIgnoreCase(changeRequest.getErrorCode()))) {
	    		System.out.println("If No change request for that customer fetch :: " + customerId);
	    		new ServiceRequestDao().createLastRefreshTimeEntry(customerId, toDate, "ChangeRequest", interval);
	    	}
	    }  
	}
	
	public void refreshChangeRequestThread(){
		
		Thread tcr1 = new Thread(new Runnable(){

			@Override
			public void run() {
				sendChangeRequestNotif();
				
			}
			
		});
		
		Thread tcr2 = new Thread(new Runnable(){

			@Override
			public void run() {
				refreshChangeRequest();
				
			}
			
		});
		
		Thread tcr3 = new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					createLastRefreshTimeEntry("ChangeRequest");
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		tcr1.setName("Thread 1-sendChangeRequestNotif : " + customerId);
		tcr2.setName("Thread 2-refreshChangeRequest : " + customerId);
		tcr3.setName("Thread 3-createLastRefreshTimeEntryCR : " + customerId);
		
		try {
			tcr1.start();
			tcr1.join();
			tcr2.start();
			tcr2.join();
			tcr3.start();
			tcr3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public synchronized void sendChangeRequestNotif(){
		if(null !=interval && !"CustomBatch".equalsIgnoreCase(interval)){
			new NotificationAPI().sendChangeRequest(changeRequestList,toDate,fromDate, customerId);
		}	  
	}

	public synchronized void refreshChangeRequest(){
		new ServiceRequestDao().refreshChangeRequest(changeRequestList, customerId);
	}


	public static void main(String[] args)
	{

		String toDate = "2015-07-30 10:00:00";
		String fromDate = "2015-07-30 09:30:00";
		String customerId = "5155";

		try
		{
			long begin = System.currentTimeMillis();
			new RefreshServiceRequest().refreshChangeRequest(customerId, toDate, fromDate, "CUST");
			long end = System.currentTimeMillis();
			System.out.printf("Time taken is : %d seconds", (end - begin) / 1000);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
