/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys			15-JUL-2015			Initial Version
 * 
 * 
 * **************************************************************************************************************
 */
package com.tt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.tt.batchrefresh.model.ci.Incident;
import com.tt.client.common.WebServiceErrorCodes;
import com.tt.client.model.RetrieveIncidentList.RetrieveIncsResponse;
import com.tt.client.service.incident.RetrieveIncidentListWSCallService;
import com.tt.dao.IncidentDAO;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;

/**	
 * This class updates the Incident Table and all its relations after fetching the list of 
 * modified incidents from Service Now
 * @author Infosys
 * @version 1.0
 */
public class RefreshIncident
{
	Properties prop = PropertyReader.getProperties();
	
	private static final Logger logger = Logger.getLogger(RefreshIncident.class.getName());
	
	/** This method fetches the list of modified / created Incidents from SNOW and updates
	 * the portal database. 
	 * 
	 * @param customerId
	 * @param startDate
	 * @param endDate
	 * @throws ParseException 
	 */
	
	private List<Incident> incidentList;
	
	private String fromDate = "";
	private String toDate = "";
	private String interval = "";
	private String customerId = "";
	
	public void refreshIncident(String customerId, String toDate, String fromDate, String interval) throws ParseException
	{
		
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.interval = interval;
		this.customerId = customerId;
		
		RefreshUtil.setProxyObject();
		logger.info("Starting refresh for customer: " + customerId+" fromDate : "+fromDate+" toDate : "+toDate+ "   Interval:"+interval);
		RetrieveIncsResponse.Response response = new RetrieveIncidentListWSCallService().getRetrieveIncidentListResponse(toDate, fromDate, customerId);

		if (WebServiceErrorCodes.RI_SUCCESS.equals(response.getErrorCode()))
		{
			this.incidentList = new CreateIncidentList().createIncidentList(response.getIncidents(), customerId, toDate);
			
			if(incidentList!=null && incidentList.size()>0){
				
				
				logger.warn("No. of Incident records updated in Batch Refresh -->>"+incidentList.size());
				
				
				refreshIncidentThread();
				
				
				/*
				if(null !=interval && !"CustomBatch".equalsIgnoreCase(interval))
					new NotificationAPI().sendIncident(incidentList,toDate,fromDate);
				
				new IncidentDAO().refreshIncidents(incidentList);
				new IncidentDAO().saveDeviceServiceMapping(incidentList);
				
				logger.info("Ending refresh for customer: " + customerId);
				new IncidentDAO().refreshMetricsFromDate(incidentList, customerId, fromDate);
				logger.info("Metrics refreshed for customer: " + customerId);
				new IncidentDAO().refreshNetworkSites();
				new IncidentDAO().calculateNetworkPerform();
				logger.info("Network sites refreshed for customer: " + customerId);
				*/
			}
			
			new IncidentDAO().createLastRefreshTimeEntry(customerId, toDate, interval);
			logger.info("Last refreshed time updated for customer: " + customerId);	
		}
		else
		{
			logger.error(response.getErrorCode() + " | " + response.getErrorMessage());			
			if(response!=null && response.getErrorCode()!=null && "RI1003".equalsIgnoreCase(response.getErrorCode())){
				new IncidentDAO().createLastRefreshTimeEntry(customerId, toDate, interval);
				logger.info("Last refreshed time updated if No Incidents are found ::  " + customerId);
			}
		}
	}

	public void refreshIncidentThread(){
		
		Thread tNotif = new Thread(new Runnable(){

			@Override
			public void run() {
				sendIncidentsNotif();
			}
		});
		
		Thread t1 = new Thread(new Runnable(){

			@Override
			public void run() {
				refreshIncidents();
			}
		});
		
		Thread t2 = new Thread(new Runnable(){

			@Override
			public void run() {
				saveDeviceServiceMapping();
			}
		});
		
		Thread t3 = new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					refreshMetricsFromDate();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		Thread t4 = new Thread(new Runnable(){

			@Override
			public void run() {
				refreshNetworkSites();
			}
		});
		
		Thread t5 = new Thread(new Runnable(){

			@Override
			public void run() {
				calculateNetworkPerform();
			}
		});
		
		tNotif.setName("Thread Notif-SendIncNotif");
		t1.setName("Thread 1-refreshIncidents");
		t2.setName("Thread 2-saveDeviceServiceMapping");
		t3.setName("Thread 3-refreshMetricsFromDate");
		t4.setName("Thread 4-refreshNetworkSites");
		t5.setName("Thread 5-calculateNetworkPerform");
		
		//tNotif.start();
		
		try {
			t1.start();
			t1.join();
			t2.start();
			t2.join();
			t3.start();
			t3.join();
			t4.start();
			t4.join();
			t5.start();
			t5.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void sendIncidentsNotif(){
		if(null !=interval && !"CustomBatch".equalsIgnoreCase(interval)){
			new NotificationAPI().sendIncident(incidentList,toDate,fromDate);
		}
	}
	
	public synchronized void refreshIncidents(){
		new IncidentDAO().refreshIncidents(incidentList);
	}
	
	public synchronized void saveDeviceServiceMapping(){
		new IncidentDAO().saveDeviceServiceMapping(incidentList);
		
		logger.info("Ending refresh for customer: " + customerId);
	}
	
	public synchronized void refreshMetricsFromDate() throws ParseException{
		new IncidentDAO().refreshMetricsFromDate(incidentList, customerId, fromDate);
		
		logger.info("Metrics refreshed for customer: " + customerId);
	}
	
	public synchronized void refreshNetworkSites(){
		new IncidentDAO().refreshNetworkSites();
	}
	
	public synchronized void calculateNetworkPerform(){
		new IncidentDAO().calculateNetworkPerform();
		
		logger.info("Network sites refreshed for customer: " + customerId);
	}
	
	public static void main(String[] args)
	{
		//String fromDate = "2015-07-11 12:19:13";
		/*String customerId = "C008";

		try
		{
			String fromDate = "2015-01-01 00:00:00";
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			String toDate = df.format(dt);
			long begin = System.currentTimeMillis();
			new RefreshIncident().refreshIncident(customerId,  toDate, fromDate, "");
			long end = System.currentTimeMillis();
			System.out.printf("Time taken is : %d seconds", (end - begin) / 1000);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}*/
		
		
		System.out.println(new SimpleDateFormat("dd/mm/yy hh:mm:ss").format(new Date(1465467563065L)));
	}
	
	
	
	

}
