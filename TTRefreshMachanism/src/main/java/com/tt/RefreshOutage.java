package com.tt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tt.batchrefresh.IncidentBatchRefresh;
import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.client.common.WebServiceErrorCodes;
import com.tt.client.model.retrieveOutage.ExecuteResponse.Response;
import com.tt.client.service.incident.RetrieveOutageWSCallService;
import com.tt.dao.HibernateUtil;
import com.tt.dao.OutageDao;
import com.tt.model.Outage;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;

public class RefreshOutage 
{
	private final static Logger logger = Logger.getLogger(RefreshOutage.class);
	private static Properties prop = PropertyReader.getProperties();
	
	public void refreshOutageData(String fromDate, String toDate, String interval) throws ParseException
	{
		RefreshUtil.setProxyObject();
		// Fetch all customer Ids
		List<String> custIdList = new BatchRefreshDAO().getCustomerIdList();
		logger.info("CuSt Ids retrieved : " + custIdList);
		
		/* Fetch list of ids to be ignored */
		String ids = prop.getProperty("SKIP_CUSTOMER_LIST");
		Set<String> skipSet = new HashSet<>();
		if (null != ids) 
		{
			for (String id : ids.split(",")) 
			{
				skipSet.add(id);
			}
		}
		
		if(null != fromDate && "2015-01-01 00:00:00".equalsIgnoreCase(fromDate))
		{
			new BatchRefreshDAO().truncateOutageRecords();
		}
		
		RetrieveOutageWSCallService outageWSCall = new RetrieveOutageWSCallService();
		
		for(String custId : custIdList)
		{
			if(skipSet.contains(custId))
			{
				logger.info("Skipping customer "+custId);
				continue;
			}
			
			Response outageResponse = outageWSCall.getRetrieveOutageListResponse(custId, toDate, fromDate);
			if(WebServiceErrorCodes.RO_SUCCESS.equals(outageResponse.getErrorCode()))
			{
				List<Outage> outageList = new CreateOutagesList().createOutageList(outageResponse, custId, fromDate, toDate);
				new OutageDao().insertOutageData(outageList);
			}
		}
		
		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		new BatchRefreshDAO().updateLastRefresh("Outage", toDateFormatted, interval);
	}
	
	/**
	 * This table calls the 'calculate_outage' stored procedure to refresh
	 * the sla availability data.<BR>
	 * calculate_outage accepts the last updated date
	 * 
	 * @param fromDate  
	 *            
	 */
	public void refreshSitesAvailabilityTable()
	{

		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL calculate_outage()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
		
		refreshServiceSLAAvailabilityTable();
	}
	
	
	/**
	 * This table calls the 'calculate_servicetier_availability' stored procedure to refresh
	 * the sla availability data.<BR>
	 * calculate_outage accepts the last updated date
	 * 
	 * @param fromDate  
	 *            
	 */
	public void refreshServiceSLAAvailabilityTable()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL calculate_servicetier_availability()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
		
		refreshPrivateCloudAvailabilityTable();
	}
	
	/**
	 * This table calls the 'calculate_cloud_availability' stored procedure to refresh
	 * the cloud availability data.<BR>
	 * 
	 *            
	 */
	private void refreshPrivateCloudAvailabilityTable() 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL calculate_cloud_availability()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
		
		refreshSaasDashboardAvailabilityTable();
	}
	
	private void refreshSaasDashboardAvailabilityTable() 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL calculate_saas_dashboard_availability()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
	}

	public static void main(String[] args)
	{
		String fromDate = new BatchRefreshDAO().retrieveLastRefreshDate("Outage", "");
		
		if (null == fromDate)
			fromDate = "2015-01-01 00:00:00";
		// String date = "2015-01-01 12:19:13";
		System.out.println(fromDate);
		
		try
		{
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			String toDate = df.format(dt);
			System.out.println(toDate);
			
			new RefreshOutage().refreshOutageData(fromDate, toDate, "");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
