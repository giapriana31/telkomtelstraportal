/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys			15-JUL-2015			Initial Version
 * 
 * 
 * **************************************************************************************************************
 */

package com.tt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.tt.batchrefresh.model.ci.Configuration;
import com.tt.batchrefresh.model.ci.Incident;
import com.tt.batchrefresh.model.ci.IncidentToDevice;
import com.tt.batchrefresh.model.ci.IncidentToService;
import com.tt.batchrefresh.model.ci.Notes;
import com.tt.client.model.RetrieveIncidentList.RetrieveIncsResponse;
import com.tt.common.RefreshUtil;


/**
 * This class reads an Incidents object that was retrieved from a web service
 * call, and populates a list of Incident Objects
 * 
 * @author Infosys
 * @version 1.0
 */
public class CreateIncidentList {
	/**
	 * @param incidents
	 * @return
	 * @throws ParseException
	 */
	public List<Incident> createIncidentList(RetrieveIncsResponse.Response.Incidents incidents, String customerId, String toDate) throws ParseException {
		Incident incident;
		List<Incident> incList = new ArrayList<Incident>();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat jktDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone timeZoneJKT = TimeZone.getTimeZone("Asia/Jakarta");
        jktDateFormat.setTimeZone(timeZoneJKT);
        DateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone timeZoneGMT = TimeZone.getTimeZone("GMT");
        gmtDateFormat.setTimeZone(timeZoneGMT);
        
		
		for (RetrieveIncsResponse.Response.Incidents.Incident in : incidents.getIncident()) {
			incident = new Incident();
			incident.setIncidentid(in.getIncidentId());
			incident.setCaller(in.getCaller());
			incident.setFullname(in.getCaseManagerFullName());
			
			
			if(null != in.getCategory() && "SaaS".equalsIgnoreCase(in.getCategory())){
				if(null != in.getSubcategory() && in.getSubcategory().startsWith("Mandoe")){
					incident.setCategory("Mandoe");
				}else{
					incident.setCategory(in.getSubcategory());
				}
			} else {
				incident.setCategory(in.getCategory());	
			}
			
			
			if(in.getClosedAt()!=null && !in.getClosedAt().isEmpty())
				incident.setCloseddate(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(in.getClosedAt()))));
			
			incident.setCustomerimpacted(new Boolean(in.getCustomerImpacted()));
			incident.setCustomername(in.getCustomerName());
			incident.setCustomeruniqueid(customerId);
			incident.setCustomerpreferedlanguage(null);
			incident.setDueByTime(null);
			
			incident.setImpact(in.getImpact());
			incident.setIncidentstatus(in.getTicketStatus());
			incident.setMIM(in.getMim());

			if(in.getLastUpdated()!=null && !in.getLastUpdated().isEmpty())
				incident.setLastupdated(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(in.getLastUpdated()))));
			if(in.getOpenedAt()!=null && !in.getOpenedAt().isEmpty())
				incident.setOpendatetime(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(in.getOpenedAt()))));
			if(in.getReportedDateTime()!=null && !in.getReportedDateTime().isEmpty())
				incident.setReporteddatetime(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(in.getReportedDateTime()))));
			if(in.getServiceLevelTarget()!=null && !in.getServiceLevelTarget().isEmpty())
				incident.setServiceleveltarget(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(in.getServiceLevelTarget()))));
			if(in.getResolutionDateTime()!=null && !in.getResolutionDateTime().isEmpty())
				incident.setResolutiondatetime(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(in.getResolutionDateTime()))));
			if(in.getCreated()!=null && !in.getCreated().isEmpty())
				incident.setCreatedDate(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(in.getCreated()))));
			
			incident.setPriority(Long.valueOf(in.getPriority()));
			incident.setResolvergroupname(null);
			incident.setServiceid(null);
			incident.setShowEscalationFlag(false);
			incident.setSiteid(String.valueOf(in.getSiteId()));
			incident.setSitename(in.getSiteName());
			incident.setSiteservicetier(in.getSiteServiceTier());
			incident.setSitezone(in.getSiteZone());
			incident.setSubcategory(in.getSubcategory());
			incident.setSummary(in.getShortDescription());
			incident.setTicketsource(null);
			incident.setTickettype(null);
			incident.setUrgency(in.getUrgency());
			incident.setUserid(in.getCaseManagerUserId());
			incident.setNotes(in.getNotes());
			incident.setCreatedBy(in.getCreatedBy());
			
			incident.setModifiedBy(in.getUpdatedBy());
			incident.setModifiedDate(RefreshUtil.convertStringToDate(toDate));
			incident.setHierEscalationFlag(new Boolean(in.getHierarchicalEscalation()));
			incident.setComments(createNoteList(in));
			incident.setIncidentToServiceList(createServiceList(in));
			if(in.getRootProductId()!=null && "PRIVATE_CLOUD_IAAS".equalsIgnoreCase(in.getRootProductId()))
				incident.setIncidentToDeviceList(createResourceList(in));
			
			incident.setActualcustomeruniqueid(String.valueOf(in.getCustomerUniqueId()));
			if(in.getRootProductId()!=null && "MNS".equalsIgnoreCase(in.getRootProductId())){
				incident.setRootProductId("MNS");
			} else if(in.getRootProductId()!=null && "PRIVATE_CLOUD_IAAS".equalsIgnoreCase(in.getRootProductId())){
				incident.setRootProductId("Private Cloud");
			} else if(in.getRootProductId()!=null && "SaaS".equalsIgnoreCase(in.getRootProductId())){
				if(null != in.getSubcategory() && in.getSubcategory().startsWith("Mandoe")){
					incident.setRootProductId("Mandoe");
				}else{
					incident.setRootProductId(in.getSubcategory());
				}
			} else
				incident.setRootProductId(in.getRootProductId());	
			
			
			incident.setResolutioncategory(in.getResolutionCategory());
			incident.setResolutionSubCategory1(in.getResolutionSubCategory1());
			incident.setResolutionSubCategory2(in.getResolutionSubCategory2());
			incident.setResolutionSubCategory3(in.getResolutionSubCategory3());
			incList.add(incident);

		}
		return incList;
	}

	/**
	 * @param in
	 * @return
	 * @throws ParseException 
	 */
	private List<Notes> createNoteList(RetrieveIncsResponse.Response.Incidents.Incident in) throws ParseException
	{
		if (in.getComments() == null)
			return null;
		Notes note;
		List<Notes> noteList = new ArrayList<Notes>();
		for (RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment comment : in.getComments().getComment())
		{
			note = new Notes();
			note.setNote(comment.getNote());
			note.setName(comment.getUserid());
			note.setTime(RefreshUtil.convertStringToDate(comment.getDatetime()));
			noteList.add(note);
		}
		return noteList;
	}

	/**
	 * @param in
	 * @return
	 */
	private List<IncidentToService> createServiceList(RetrieveIncsResponse.Response.Incidents.Incident in)
	{
		if (in.getAffectedServices() == null)
		{
			return null;
			
		} else {
			
			IncidentToService config;
			List<IncidentToService> configList = new ArrayList<IncidentToService>();
			for (RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService service : in.getAffectedServices().getAffectedService())
			{
				config = new IncidentToService();
				config.setIncidentid(in.getIncidentId());
				config.setServiceciid(service.getCiId());
				configList.add(config);
			}

			return configList;
		}
		
	}

	/**
	 * @param in
	 * @return
	 */
	private List<IncidentToDevice> createResourceList(RetrieveIncsResponse.Response.Incidents.Incident in)
	{

		if (in.getAffectedResources() == null){
			
			return null;
		} else {
			
			List<IncidentToDevice> configList = new ArrayList<IncidentToDevice>();
			IncidentToDevice config = new IncidentToDevice();
			
			for (RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource resource : in.getAffectedResources().getAffectedResource())
			{
				config = new IncidentToDevice();
				config.setIncidentid(in.getIncidentId());
				config.setDeviceciid(resource.getCiId());
				configList.add(config);
			}

			return configList;
		}
	}

}
