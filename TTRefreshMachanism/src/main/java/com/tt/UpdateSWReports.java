package com.tt;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

import com.tt.common.FSFileRead;
import com.tt.dao.SWReportsDao;
import com.tt.model.SWReportImages;
import com.tt.model.SWReports;
import com.tt.utils.PropertyReader;

public class UpdateSWReports {

	HashMap<String, String> customerMap;
	List<File> ListOfFilesToDelete = new ArrayList<File>();
	SWReportsDao dao = new SWReportsDao();
	Properties prop = PropertyReader.getProperties();

	public UpdateSWReports() {
		customerMap = dao.getCustomerNameAndId();
	}

	public boolean refreshSWReports(String categoryName) {

		FSFileRead fileRead = new FSFileRead();
		ArrayList<SWReportData> reportDatas = fileRead.getReportData(prop
				.getProperty("SWReportsPath"));
		
		deleteDocFromDb(reportDatas,categoryName);

		if (reportDatas.isEmpty()) {
			deleteArchivedReports();
		} else {
			boolean runStatus = addDocToRepository(reportDatas,categoryName);
			if (runStatus) {
				deleteArchivedReports();
			}
		}
		return true;
	}


	public void deleteDocFromDb(ArrayList<SWReportData> datas, String categoryName) {

		for (SWReportData swReportData : datas) {

			HashMap<String, File[]> fileList = swReportData.getReportFile();
			String customerName = swReportData.getCustomerName();
			String customerId = customerMap.get(customerName);
			Set<String> categoryList = fileList.keySet();

			for (String category : categoryList) {
				if(category.equalsIgnoreCase(categoryName)){
					dao.deleteReportImages(category, customerId);
					dao.deleteCategory(category, customerId);
				}
			}

		}

	}

	public boolean addDocToRepository(ArrayList<SWReportData> datas,String categoryName) {

		String reportType = null;
		String reportName = null;
		String reportPath = null;
		SerialBlob blob = null;
		Date updateDate = null;
		String id = null;

		FileInputStream stream = null;

		byte[] reportData;

		for (SWReportData swReportData : datas) {

			String customerName = swReportData.getCustomerName();

			String customerId = customerMap.get(customerName);
			HashMap<String, File[]> map = swReportData.getReportFile();
			Set<String> key = map.keySet();

			List<String> categoryList = new ArrayList<String>();
			for (String string : key) {
				String category = string;
				System.out.println("Category Name11: " + category);

				if (category.equalsIgnoreCase(categoryName)) {
					File[] reportFile = map.get(string);
					for (File file : reportFile) {

						String mimeType = URLConnection
								.guessContentTypeFromName(file.getName());

						reportType = mimeType;

						String tempString = file.getName();
						String[] fileNameSplit = tempString.split("_");
						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"dd/MM/yyyy HH:mm:ss");
						Calendar calendar = Calendar.getInstance();
						calendar.set(Integer.parseInt(fileNameSplit[0]),
								(Integer.parseInt(fileNameSplit[1]) - 1),
								Integer.parseInt(fileNameSplit[2]));
						calendar.set(Calendar.HOUR_OF_DAY,
								Integer.parseInt(fileNameSplit[3]));
						calendar.set(Calendar.MINUTE,
								Integer.parseInt(fileNameSplit[4]));
						calendar.set(Calendar.SECOND,
								Integer.parseInt(fileNameSplit[5]));
						calendar.set(Calendar.MILLISECOND, 0);
						Date date2 = calendar.getTime();
						System.out.println("Date is :"
								+ dateFormat.format(date2));
						updateDate = new Date();
						updateDate = date2;

						reportName = fileNameSplit[6].substring(0,
								fileNameSplit[6].lastIndexOf('.'));

						System.out.println("Report Name: " + reportName);
						reportPath = file.getName();
						try {
							stream = new FileInputStream(file);


							reportData = IOUtils.toByteArray(stream);
							
							
							System.out.println("Is empty " + reportData.length);

							blob = new SerialBlob(reportData);

						} catch (IOException | SQLException e) {
							System.out.println("Catch1");
							e.printStackTrace();
							return false;
						}
						
						PDDocument document = null;
						
							try {
								document = PDDocument.load(file);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						
						List<PDPage> list = document.getDocumentCatalog().getAllPages();
						System.out.println("Total files to be converted -> " + list.size());

						String fileName = file.getName().replace(".pdf", "");
						List<SWReportImages> reportImages=new ArrayList<SWReportImages>();
						int pageNumber = 1;
						 String destinationDir = "D:/TestImage/";
						SWReports reports = new SWReports();
						for (PDPage page : list) {
							BufferedImage image = null;
							SerialBlob imageBlob =null;
							
								try {
									image = page.convertToImage();
									
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							//reportData = IOUtils.toByteArray(stream);
							
							try {
								ImageIO.write( image, "png", baos );
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
							byte[] reportImage = baos.toByteArray();
							try {
								
								 imageBlob = new SerialBlob(reportImage);
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							SWReportImages swReportImages= new SWReportImages();
							swReportImages.setImage_data(imageBlob);
							swReportImages.setReportId(reports);
							swReportImages.setCustomerId(customerId);
							swReportImages.setCategory(category);
							reportImages.add(swReportImages);
						}
						
						java.util.Date date = new java.util.Date();
						System.out.println("customerId: " + customerId);
						System.out.println("category: " + category);
						System.out.println("reportName: " + reportName);
						System.out.println("reportPath: " + reportPath);
						System.out.println("reportType: " + reportType);

						reports.setCreateTime(new Timestamp(date.getTime()));
						reports.setCustomerId(customerId);
						reports.setReportCategory(category.trim());
						reports.setReportName(reportName);
						reports.setReportPath(reportPath);
						reports.setReportType(reportType);
						reports.setReportData(blob);
						reports.setUpdateDate(new Timestamp(date2.getTime()));
						reports.setReportImages(reportImages);

						id = dao.saveReports(reports);
						if (id != null && id.length() > 0) {
							archiveReports(category, file, customerName);
							System.out.println("File Path: "
									+ file.getAbsolutePath());
							ListOfFilesToDelete.add(file);
							
						}

					}
					categoryList.add(category);
				}
			}

		}

		if (id != null) {
			return true;
		} else {

			return false;
		}

	}
	
	
	public void saveReportImages(String reportId,File file){
		System.out.println("Save image reports");
		System.out.println("File Name: "+file.getName());
		System.out.println("Report id:"+ reportId);
		PDDocument document;
		try {
			document = PDDocument.load(file);
		
		List<PDPage> list = document.getDocumentCatalog().getAllPages();
		System.out.println("Total files to be converted -> " + list.size());

		String fileName = file.getName().replace(".pdf", "");
		int pageNumber = 1;
		for (PDPage page : list) {
			BufferedImage image;
			
				image = page.convertToImage();
			
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			//reportData = IOUtils.toByteArray(stream);
			
			ImageIO.write( image, "png", baos );
		
			byte[] reportImage = baos.toByteArray();
			SerialBlob blob = new SerialBlob(reportImage);
			SWReportImages swReportImages= new SWReportImages();
			swReportImages.setImage_data(blob);
			String imageId=dao.saveImageToDB(swReportImages);
			System.out.println("Image Id: "+imageId);
			pageNumber++;
		
			document.close();
		}} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}
		
	
	
	
	public static void archiveReports(String category, File file,
			String customerName) {

		FileInputStream archiveStream = null;
		Properties prop = PropertyReader.getProperties();

		FileOutputStream outStream = null;

		File archiveDirectory = new File(
				prop.getProperty("SWArchiveRepository"));
		String path = prop.getProperty("SWArchiveRepository");

		File[] directoryList = archiveDirectory.listFiles();
	
		if (!(directoryList.length == 0)) {
			for (File customerNameDirectory : directoryList) {

				if (customerNameDirectory.getName().equalsIgnoreCase(
						customerName)) {
					File[] subdirectories = customerNameDirectory.listFiles();
					for (File file2 : subdirectories) {

						if (category.equalsIgnoreCase(file2.getName())) {
							File file3 = new File(path + File.separator
									+ customerName + File.separator + category);

							File[] files = file3.listFiles();
							List<File> fileList = Arrays.asList(files);
							try {
								File outputFile = new File(path
										+ File.separator + customerName
										+ File.separator + category
										+ File.separator + file.getName());
								outStream = new FileOutputStream(outputFile);

								byte[] buffer = new byte[1024];
								int length;

								archiveStream = new FileInputStream(file);
								while ((length = archiveStream.read(buffer)) > 0) {

									outStream.write(buffer, 0, length);

								}

							} catch (IOException e) {
								System.out.println("catch 2");
								e.printStackTrace();
							} finally {

								try {

									outStream.flush();
									archiveStream.close();
									outStream.close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						} else {
							System.out.println("4: " + customerName);
							System.out.println("else");
							File file3 = new File(path + File.separator
									+ customerName + File.separator + category);
							System.out.println("path: " + path + File.separator
									+ customerName + File.separator + category);
							Boolean directoryMade = file3.mkdirs();
							System.out.println("Directory made: "
									+ directoryMade);

							try {
								File outputFile = new File(path
										+ File.separator + customerName
										+ File.separator + category
										+ File.separator + file.getName());
								outStream = new FileOutputStream(outputFile);

								byte[] buffer = new byte[1024];
								int length;

								archiveStream = new FileInputStream(file);

								while ((length = archiveStream.read(buffer)) > 0) {

									outStream.write(buffer, 0, length);

								}

							} catch (IOException e) {
								System.out.println("catch3");
								e.printStackTrace();
							} finally {

								try {
									outStream.flush();

									archiveStream.close();
									outStream.close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

						}
					}
					System.out.println("before break1");
					break;

				}

				else {
					System.out.println("Elsssss");
					System.out.println("5: " + customerName);

					File file2 = new File(path + File.separator + customerName
							+ File.separator + category);
					System.out.println("Name jikdjkdjkdj: " + path
							+ File.separator + customerName + File.separator
							+ category);
					Boolean directoryMade = file2.mkdirs();
					System.out.println("Directory Made " + directoryMade);
					try {
						File outputFile = new File(path + File.separator
								+ customerName + File.separator + category
								+ File.separator + file.getName());

						outStream = new FileOutputStream(outputFile);

						byte[] buffer = new byte[1024];
						int length;
						archiveStream = new FileInputStream(file);

						while ((length = archiveStream.read(buffer)) > 0) {

							outStream.write(buffer, 0, length);

						}

					} catch (IOException e) {
						System.out.println("catch4");
						e.printStackTrace();
					} finally {

						try {
							outStream.flush();

							archiveStream.close();
							outStream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					System.out.println("before break2");

				}

			}

		} else {
			System.out.println("Elsssss");
			System.out.println("5: " + customerName);

			File file2 = new File(path + File.separator + customerName
					+ File.separator + category);
			System.out.println("Name jikdjkdjkdj: " + path + File.separator
					+ customerName + File.separator + category);
			Boolean directoryMade = file2.mkdirs();
			System.out.println("Directory Made " + directoryMade);
			try {
				File outputFile = new File(path + File.separator + customerName
						+ File.separator + category + File.separator
						+ file.getName());

				outStream = new FileOutputStream(outputFile);

				byte[] buffer = new byte[1024];
				int length;
				archiveStream = new FileInputStream(file);

				while ((length = archiveStream.read(buffer)) > 0) {

					outStream.write(buffer, 0, length);

				}

			} catch (IOException e) {
				System.out.println("catch4");
				e.printStackTrace();
			} finally {

				try {
					outStream.flush();

					archiveStream.close();
					outStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			System.out.println("before break2");

		}

	}

	public static void deleteArchivedReports() {
		System.out.println("Delete Archived666666666666666666666666666666666 ");
		// String customer="Infosys";
		// String category="Weekly";
		Properties prop = PropertyReader.getProperties();

		File archiveDirectory = new File(
				prop.getProperty("SWArchiveRepository"));
		File[] directoryList = archiveDirectory.listFiles();
		for (File file : directoryList) {

			if (file.isDirectory()) {

				File[] categoryList = file.listFiles();
				for (File file2 : categoryList) {

					if (file2.isDirectory()) {

						File[] fileList = file2.listFiles();

						for (File file3 : fileList) {

							String tempString = file3.getName();
							String[] fileNameSplit = tempString.split("_");
							SimpleDateFormat dateFormat = new SimpleDateFormat(
									"dd/MM/yyyy HH:mm:ss");
							Calendar calendar = Calendar.getInstance();
							calendar.set(Integer.parseInt(fileNameSplit[0]),
									(Integer.parseInt(fileNameSplit[1]) - 1),
									Integer.parseInt(fileNameSplit[2]));
							calendar.set(Calendar.HOUR_OF_DAY,
									Integer.parseInt(fileNameSplit[3]));
							calendar.set(Calendar.MINUTE,
									Integer.parseInt(fileNameSplit[4]));
							calendar.set(Calendar.SECOND,
									Integer.parseInt(fileNameSplit[5]));
							calendar.set(Calendar.MILLISECOND, 0);
							Date date2 = calendar.getTime();
							Calendar currentCalender = Calendar.getInstance();

							if (file2.getName().equalsIgnoreCase("daily")) {

								currentCalender.add(Calendar.DAY_OF_YEAR, -3);

								if (date2.before(currentCalender.getTime())) {

									Boolean isDelete = file3.delete();
									System.out
											.println("Deleted7777777777777777777777777777777777: "
													+ isDelete);
								}
							} else if (file2.getName().equalsIgnoreCase(
									"weekly")) {
								currentCalender.add(Calendar.DAY_OF_YEAR, -21);
								System.out.println("Date Weekly: "
										+ currentCalender.getTime());

								if (date2.before(currentCalender.getTime())) {
									Boolean isDelete = file3.delete();
									System.out
											.println("Deleted7777777777777777777777777777777777: "
													+ isDelete);
								}

							} else if (file2.getName().equalsIgnoreCase(
									"monthly")) {
								currentCalender.add(Calendar.MONTH, -3);
								System.out.println("Date Monthly: "
										+ currentCalender.getTime());

								if (date2.before(currentCalender.getTime())) {

									Boolean isDelete = file3.delete();
									System.out
											.println("Deleted7777777777777777777777777777777777: "
													+ isDelete);
								}
							}

						}

					}

				}
			}

		}
	}

}
