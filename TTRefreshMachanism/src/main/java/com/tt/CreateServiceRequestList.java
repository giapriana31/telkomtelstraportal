package com.tt;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response.ServiceRequests;
import com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response.ServiceRequests.Servicerequest;
import com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem;
import com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment;
import com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName;
import com.tt.common.RefreshUtil;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.model.servicerequest.ServiceRequestItemDevices;
import com.tt.model.servicerequest.ServiceRequestItemNotes;

public class CreateServiceRequestList {

    private final static Logger logger = Logger.getLogger(CreateServiceRequestList.class);
    
    public List<ServiceRequest> createServiceRequest(ServiceRequests serviceRequest, String customerId, String toDate, String fromDate) throws ParseException {

	List<ServiceRequest> serviceRequests = new ArrayList<ServiceRequest>();
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateFormat jktDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    TimeZone timeZoneJKT = TimeZone.getTimeZone("Asia/Jakarta");
    jktDateFormat.setTimeZone(timeZoneJKT);
    DateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    TimeZone timeZoneGMT = TimeZone.getTimeZone("GMT");
    gmtDateFormat.setTimeZone(timeZoneGMT);
	

    
	if (serviceRequest != null
		&& !serviceRequest.getServicerequest().isEmpty()) {
	    for (Servicerequest serviceReq : serviceRequest.getServicerequest()) {
		logger.info("serviceReq.get------"+ serviceReq.getServiceRequestNumber());
		ServiceRequest request = new ServiceRequest();
		request.setCreatedby(serviceReq.getCreatedBy());
		if(serviceReq.getSysCreatedOn()!=null && !serviceReq.getSysCreatedOn().isEmpty())
			request.setCreateddate(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(serviceReq.getSysCreatedOn()))));
		request.setDescription(serviceReq.getDescription());
		request.setEnddate(RefreshUtil.convertStringToOnlyDate(serviceReq.getProjectExpectedEndDate()));
		
		if(serviceReq.getSysUpdatedOn()!=null && !serviceReq.getSysUpdatedOn().isEmpty())
			request.setLastupdatedate(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(serviceReq.getSysUpdatedOn()))));
		request.setLastupdatedby(serviceReq.getUpdatedBy());
		request.setRequestid(serviceReq.getServiceRequestNumber());
		request.setRequestor(serviceReq.getRequestorFor());
		request.setStartdate(RefreshUtil.convertStringToOnlyDate(serviceReq.getProjectStartDate()));
		request.setStatus(serviceReq.getRequestState());
		request.setSubject(serviceReq.getShortDescription());
		request.setCustomeruniqueid(customerId);
		request.setRequesttype("Service");
		List<ServiceRequestItem> serviceRequestItem = new ArrayList<ServiceRequestItem>();
		if (serviceReq.getRequestedItems() != null
			&& !serviceReq.getRequestedItems().getRequestedItem().isEmpty())
		    for (RequestedItem requestedItem : serviceReq.getRequestedItems().getRequestedItem()) {
			logger.info("requestedItem.getRequestedItemNumber()--------"+ requestedItem.getRequestedItemNumber());
			ServiceRequestItem requestItem = new ServiceRequestItem();
			requestItem.setCategory("MACD");
			requestItem.setCreatedby(requestedItem.getCreatedBy());
			if(requestedItem.getRequestedItemCreatedOn()!=null && !requestedItem.getRequestedItemCreatedOn().isEmpty())
				requestItem.setCreateddate(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(requestedItem.getRequestedItemCreatedOn()))));
			requestItem.setItemid(requestedItem.getRequestedItemNumber());
			if(requestedItem.getRequestedItemUpdatedOn()!=null && !requestedItem.getRequestedItemUpdatedOn().isEmpty())
				requestItem.setLastupdate(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(requestedItem.getRequestedItemUpdatedOn()))));
			requestItem.setLastupdatedby(requestedItem.getUpdatedBy());
			requestItem.setProjecttype(requestedItem.getComplexityServiceRequest());
			requestItem.setItemtype(requestedItem.getRITMShortDescription());
			requestItem.setServicename(requestedItem.getServiceId());
			requestItem.setSitename(requestedItem.getSiteName());
			requestItem.setStatus(requestedItem.getState());
			requestItem.setSubcategory(requestedItem.getServiceRequestCategory());
			requestItem.setServicerequestid(request);
			List<ServiceRequestItemDevices> serviceRequestItemDevicesList = new ArrayList<ServiceRequestItemDevices>();
			if (requestedItem.getDeviceNames() != null && !requestedItem.getDeviceNames().getDeviceName().isEmpty())
			    for (DeviceName serviceRequestItemDevices : requestedItem.getDeviceNames().getDeviceName()) {
				logger.info("serviceRequestItemDevices.getCiName()---"+ serviceRequestItemDevices.getCiName());
				ServiceRequestItemDevices devices = new ServiceRequestItemDevices();
				//devices.setCiid("");
				devices.setItemid(requestItem);
				devices.setCiname(serviceRequestItemDevices.getCiName());
				serviceRequestItemDevicesList.add(devices);
			    }

			List<ServiceRequestItemNotes> serviceRequestItemNotesList = new ArrayList<ServiceRequestItemNotes>();
			if (requestedItem.getComments() != null && !requestedItem.getComments().getComment().isEmpty())
			    for (Comment serviceRequestItemNotes : requestedItem.getComments().getComment()) {
				ServiceRequestItemNotes itemNotes = new ServiceRequestItemNotes();
				
				logger.info("serviceRequestItemNotes.getNote()-------"+ serviceRequestItemNotes.getNote());
				itemNotes.setCustinfonotes(serviceRequestItemNotes.getNote());
				if(serviceRequestItemNotes.getDatetime()!=null && !serviceRequestItemNotes.getDatetime().isEmpty())
					itemNotes.setModifiedtime(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(serviceRequestItemNotes.getDatetime()))));
				//itemNotes.setUserid("");
				itemNotes.setUsername(serviceRequestItemNotes.getUserid());
				itemNotes.setItemid(requestItem);
				serviceRequestItemNotesList.add(itemNotes);
			    }

			requestItem.setServiceRequestItemDevicesList(serviceRequestItemDevicesList);
			requestItem.setServiceRequestItemNotesList(serviceRequestItemNotesList);
			serviceRequestItem.add(requestItem);
		    }
		request.setServiceRequestItem(serviceRequestItem);
		serviceRequests.add(request);
	    }
	}

	return serviceRequests;
    }

}
