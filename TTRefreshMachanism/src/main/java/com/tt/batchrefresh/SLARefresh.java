package com.tt.batchrefresh;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tt.dao.HibernateUtil;

public class SLARefresh
{
	public static void main(String[] args)
	{
		SLARefresh sl = new SLARefresh();
		sl.refreshSLA();
		sl.refreshSiteColor();
		sl.refreshServiceColor();

	}

	public void refreshSLA()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL CALCULATE_AVG_SLA ()");

		Transaction txn = session.beginTransaction();

		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
	}

	public void refreshSiteColor()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL CALCULATE_SITE_COLOR ()");

		Transaction txn = session.beginTransaction();

		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
	}

	public void refreshServiceColor()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL CALCULATE_SERVICE_COLOR ()");

		Transaction txn = session.beginTransaction();

		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
	}

}
