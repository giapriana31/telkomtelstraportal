package com.tt.batchrefresh;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.tt.utils.PropertyReader;

public class DataRetrieve
{

	public void retrieveData()
	{
		Properties prop = PropertyReader.getProperties();
		//Connection conn = null;
		//Connection conn1 = null;
		final String dburlMYSQL = prop.getProperty("dburlMYSQL");
		final String usernameMySQL = prop.getProperty("usernameMySQL");
		final String passwordMySQL = prop.getProperty("passwordMySQL");
		final String dburlMsSQL = prop.getProperty("dburlMsSQL");
		System.out.println("path " + prop.getProperty("FAQFilePath"));

		try
		{
			Connection conn1 = DriverManager.getConnection(dburlMYSQL, usernameMySQL, passwordMySQL);
			Statement stmt1 = conn1.createStatement();
		
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = java.sql.DriverManager.getConnection(dburlMsSQL);

			Statement stmt = conn.createStatement();

			ResultSet rs;
			
			String query = "select * from SWAvailabilityReplica where DateTime between  DATEADD(DD, -1,DATEADD(DD,  DATEDIFF(D, 0, GETDATE()), 0)) and  DATEADD(S, -1, DATEADD(DD, DATEDIFF(D, 0, GETDATE()), 0))";
			rs = stmt.executeQuery(query);
			while (rs.next())
			{
				String Devicename = rs.getString(1);
				String datedetails = rs.getString(2);
				int Availability = rs.getInt(3);
				// System.out.println(Devicename);
				if (Devicename != null)
				{
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date = formatter.parse(datedetails);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					datedetails = sdf.format(date);
					String sql = "insert into swdeviceavailability values (" + "'" + Devicename + "'" + "," + "'" + datedetails + "'" + "," + "'" + Availability + "'" + ")";
	
					stmt1.executeUpdate(sql);
				}
			}
			conn.close();
			conn1.close();

		}
		catch (Exception e)
		{
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

	}

	public static void main(String[] args)
	{

	}

}
