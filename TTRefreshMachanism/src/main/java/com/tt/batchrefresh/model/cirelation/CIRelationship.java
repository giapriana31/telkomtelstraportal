/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys				24-JUL-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh.model.cirelation;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/** POJO for cirelationship table
 * @author Infosys
 * @version 1.0
 */
@Entity
@Table(name = "cirelationship")
public class CIRelationship
{
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name = "relationship")
	private String relationShip;
	
	@Column(name = "baseciname")
	private String baseCIName;
	
	@Column(name = "dependentciname")
	private String dependentCIName;
	
	@Column(name = "baseciid")
	private String baseCIId;
	
	@Column(name = "dependentciid")
	private String dependentCIId;
	
	@Column(name = "dependentciclass")
	private String dependentCIClass;
	
	@Column(name = "baseciclass")
	private String baseCIClass;
	
	@Column(name = "createDate")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date createdDate;
	
	@Column(name = "updateDate")
	@Temporal(javax.persistence.TemporalType.DATE)
	private Date updatedDate;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "updatedBy")
	private String updatedBy;

	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public String getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}
	
	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	public String getRelationShip()
	{
		return relationShip;
	}
	public void setRelationShip(String relationShip)
	{
		this.relationShip = relationShip;
	}
	public String getBaseCIName()
	{
		return baseCIName;
	}
	public void setBaseCIName(String baseCIName)
	{
		this.baseCIName = baseCIName;
	}
	public String getDependentCIName()
	{
		return dependentCIName;
	}
	public void setDependentCIName(String dependentCIName)
	{
		this.dependentCIName = dependentCIName;
	}
	public String getBaseCIId()
	{
		return baseCIId;
	}
	public void setBaseCIId(String baseCIId)
	{
		this.baseCIId = baseCIId;
	}
	public String getDependentCIId()
	{
		return dependentCIId;
	}
	public void setDependentCIId(String dependentCIId)
	{
		this.dependentCIId = dependentCIId;
	}
	public String getDependentCIClass()
	{
		return dependentCIClass;
	}
	public void setDependentCIClass(String dependentCIClass)
	{
		this.dependentCIClass = dependentCIClass;
	}
	public String getBaseCIClass()
	{
		return baseCIClass;
	}
	public void setBaseCIClass(String baseCIClass)
	{
		this.baseCIClass = baseCIClass;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public Date getUpdateDate()
	{
		return updatedDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updatedDate = updateDate;
	}
	
	
	
	
}
