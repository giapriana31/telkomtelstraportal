package com.tt.batchrefresh.model.ci;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="vcenter")
public class VCenter implements Serializable
{
	
	@Id
	@GeneratedValue
	private long id;
	
	@OneToOne
	@JoinColumn(name = "ciid", referencedColumnName="ciid")
	private Configuration ciid;

	@Column(name = "vcenterurl")
	private String vCenterURL;
	private String description;

	@Column(name = "fullname")
	private String fullName;

	@Column(name = "apiversion")
	private String apiVersion;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getvCenterURL() {
		return vCenterURL;
	}

	public void setvCenterURL(String vCenterURL) {
		this.vCenterURL = vCenterURL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public Configuration getCiid() {
		return ciid;
	}

	public void setCiid(Configuration ciid) {
		this.ciid = ciid;
	}
}
