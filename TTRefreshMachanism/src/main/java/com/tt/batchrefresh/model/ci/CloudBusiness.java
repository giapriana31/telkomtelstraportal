package com.tt.batchrefresh.model.ci;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="cloudbusiness")
public class CloudBusiness implements Serializable{

	@Id
	@GeneratedValue
	private long id;
	
	@OneToOne
	@JoinColumn(name = "ciid", referencedColumnName="ciid")
	private Configuration ciid;

	@Column(name = "deploymenttype")
	private String deploymentType;
	private String vendor;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDeploymentType() {
		return deploymentType;
	}

	public void setDeploymentType(String deploymentType) {
		this.deploymentType = deploymentType;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	
	public Configuration getCiid() {
		return ciid;
	}

	public void setCiid(Configuration ciid) {
		this.ciid = ciid;
	}
}
