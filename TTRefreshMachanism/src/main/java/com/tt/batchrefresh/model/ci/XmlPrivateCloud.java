package com.tt.batchrefresh.model.ci;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uCis"
})
@XmlRootElement(name = "xml")
public class XmlPrivateCloud {

    @XmlElement(name = "u_cis")
    protected List<XmlPrivateCloud.UCis> uCis;
    public List<XmlPrivateCloud.UCis> getUCis() {
        if (uCis == null) {
            uCis = new ArrayList<XmlPrivateCloud.UCis>();
        }
        return this.uCis;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "ciName",
        "ciOperationalStatus",
        "ciSysClassName",
        "ciSysCreatedBy",
        "ciSysCreatedOn",
        "ciSysDomain",
        "ciSysDomainPath",
        "ciSysId",
        "ciSysUpdatedBy",
        "ciSysUpdatedOn",
        "ciuactivatedon",
        "ciuccdcustomercommitteddate",
        "ciucideliverystatus",
        "ciucitype",
        "ciucustomeruniqueid",
        "ciudecommissioneddate",
        "ciulink",
        "ciunumber",
        "ciuproductid",
        "ciurootproductid",
        "ciusrireferencenumber",
        "cpename",
        "cpesysdomain",
        "cpesysdomainpath",
        "cpesysid",
        "cpeucustomeredgedevice",
        "cpeudevicetype",
        "cpeudevicevendor",
        "peinterfacename",
        "peinterfacesysdomain",
        "peinterfacesysdomainpath",
        "peinterfacesysid",
        "peinterfaceucustomerservingnetworkinterface",
        "reqnumber",
        "reqsysdomain",
        "reqsysdomainpath",
        "reqsysid",
        "ritmnumber",
        "sitesysid",
        "siteusiteid",
        "ciusrreferenceid",
        "ciusitename",
        "vbulocalsitefloor",
        "vmimodelid",
        "vcsysdomainpath",
        "cbssysdomain",
        "cbsudevicevendor",
        "vbumaxofcpus",
        "vmimemory",
        "vbumaintenanceoption",
        "vbubuilding",
        "vcapiversion",
        "vbuciscocontractnumber",
        "vbutypemodel",
        "vbsysdomainpath",
        "cbssysdomainpath",
        "vmiimagepath",
        "vbumaxsizeofstoragegb",
        "vmileaseend",
        "vbudeviceownership",
        "vmistate",
        "vcshortdescription",
        "vmisysdomain",
        "vbuvblockrcmreleaseconfigurationmatrix",
        "vbuemcserialnumber",
        "vbsysid",
        "vbufinancialmodel",
        "vbsysdomain",
        "vbunumberofvcpusforvblock",
        "vmicorrelationid",
        "vmisysdomainpath",
        "cbsudeploymenttype",
        "vmitemplate",
        "vmiulink",
        "vbusotragemodel",
        "vbufloor",
        "vcfullname",
        "vbunumberofracks",
        "vbudeliverymodel",
        "vbulocalsitebuidling",
        "cbssysid",
        "vbserialnumber",
        "vcsysid",
        "vbuvcecontractnumber",
        "vmicpus",
        "vcsysdomain",
        "vbuvblocktype",
        "vbumaxmemorygb",
        "vbuvcevisionurl",
        "ciucontractenddate",
        "ciucontractstartdate",
        "ciucontractduration",
        "vmishortdescription",
        "vmidiskssize",
        "vbuboqurl",
        "vmileasestart",
        "vcurl",
        "vminics",
        "vbuemcsiteid",
        "vbuvmwarecontractnumber",
        "vmidisks",
        "vburoom",
        "vbuvblocksystemtype",
        "vmisysid",
        "usritype",
        "cpeuproductclassification",
        "vmiuvmalias"
    })
    public static class UCis {

        @XmlElement(name = "ci_name", required = true)
        protected String ciName;
        @XmlElement(name = "ci_operational_status")
        protected byte ciOperationalStatus;
        @XmlElement(name = "ci_sys_class_name", required = true)
        protected String ciSysClassName;
        @XmlElement(name = "ci_sys_created_by", required = true)
        protected String ciSysCreatedBy;
        @XmlElement(name = "ci_sys_created_on", required = true)
        protected String ciSysCreatedOn;
        @XmlElement(name = "ci_sys_domain", required = true)
        protected String ciSysDomain;
        @XmlElement(name = "ci_sys_domain_path", required = true)
        protected String ciSysDomainPath;
        @XmlElement(name = "ci_sys_id", required = true)
        protected String ciSysId;
        @XmlElement(name = "ci_sys_updated_by", required = true)
        protected String ciSysUpdatedBy;
        @XmlElement(name = "ci_sys_updated_on", required = true)
        protected String ciSysUpdatedOn;
        @XmlElement(name = "ci_u_activated_on", required = true)
        protected String ciuactivatedon;
        @XmlElement(name = "ci_u_ccd_customer_committed_date", required = true)
        protected String ciuccdcustomercommitteddate;
        @XmlElement(name = "ci_u_ci_delivery_status", required = true)
        protected String ciucideliverystatus;
        @XmlElement(name = "ci_u_ci_type", required = true)
        protected String ciucitype;
        @XmlElement(name = "ci_u_customer_uniqueid", required = true)
        protected String ciucustomeruniqueid;
        @XmlElement(name = "ci_u_decommissioned_date", required = true)
        protected String ciudecommissioneddate;
        @XmlElement(name = "ci_u_link", required = true)
        protected String ciulink;
        @XmlElement(name = "ci_u_number", required = true)
        protected String ciunumber;
        @XmlElement(name = "ci_u_product_id", required = true)
        protected String ciuproductid;
        @XmlElement(name = "ci_u_root_product_id", required = true)
        protected String ciurootproductid;
        @XmlElement(name = "ci_u_sri_reference_number", required = true)
        protected String ciusrireferencenumber;
        @XmlElement(name = "cpe_name", required = true)
        protected String cpename;
        @XmlElement(name = "cpe_sys_domain", required = true)
        protected String cpesysdomain;
        @XmlElement(name = "cpe_sys_domain_path", required = true)
        protected String cpesysdomainpath;
        @XmlElement(name = "cpe_sys_id", required = true)
        protected String cpesysid;
        @XmlElement(name = "cpe_u_customer_edge_device", required = true)
        protected String cpeucustomeredgedevice;
        @XmlElement(name = "cpe_u_device_type", required = true)
        protected String cpeudevicetype;
        @XmlElement(name = "cpe_u_device_vendor", required = true)
        protected String cpeudevicevendor;
        @XmlElement(name = "peinterface_name", required = true)
        protected String peinterfacename;
        @XmlElement(name = "peinterface_sys_domain", required = true)
        protected String peinterfacesysdomain;
        @XmlElement(name = "peinterface_sys_domain_path", required = true)
        protected String peinterfacesysdomainpath;
        @XmlElement(name = "peinterface_sys_id", required = true)
        protected String peinterfacesysid;
        @XmlElement(name = "peinterface_u_customer_serving_network_interface", required = true)
        protected String peinterfaceucustomerservingnetworkinterface;
        @XmlElement(name = "req_number", required = true)
        protected String reqnumber;
        @XmlElement(name = "req_sys_domain", required = true)
        protected String reqsysdomain;
        @XmlElement(name = "req_sys_domain_path", required = true)
        protected String reqsysdomainpath;
        @XmlElement(name = "req_sys_id", required = true)
        protected String reqsysid;
        @XmlElement(name = "ritm_number", required = true)
        protected String ritmnumber;
        @XmlElement(name = "site_sys_id", required = true)
        protected String sitesysid;
        @XmlElement(name = "site_u_site_id", required = true)
        protected String siteusiteid;
        @XmlElement(name = "ci_u_sr_reference_id", required = true)
        protected String  ciusrreferenceid;
        @XmlElement(name = "ci_u_site_name", required = true)
		protected String  ciusitename;
        @XmlElement(name = "vb_u_local_site_floor", required = true)
        protected String  vbulocalsitefloor;
        @XmlElement(name = "vmi_model_id", required = true)
        protected String  vmimodelid;
        @XmlElement(name = "vc_sys_domain_path", required = true)        
        protected String  vcsysdomainpath;
        @XmlElement(name = "cbs_sys_domain", required = true)
        protected String  cbssysdomain;
        @XmlElement(name = "cbs_u_device_vendor", required = true)
        protected String  cbsudevicevendor;
        @XmlElement(name = "vb_u_max_of_cpus", required = true)
        protected String  vbumaxofcpus;
        @XmlElement(name = "vmi_memory", required = true)
        protected String  vmimemory;
        @XmlElement(name = "vb_u_maintenance_option", required = true)
        protected String  vbumaintenanceoption;
        @XmlElement(name = "vb_u_building", required = true)
        protected String  vbubuilding;
        @XmlElement(name = "vc_api_version", required = true)
        protected String  vcapiversion;
        @XmlElement(name = "vb_u_cisco_contract_number", required = true)
        protected String  vbuciscocontractnumber;
        @XmlElement(name = "vb_u_type_model", required = true)
        protected String  vbutypemodel;
        @XmlElement(name = "vb_sys_domain_path", required = true)
        protected String  vbsysdomainpath;
        @XmlElement(name = "cbs_sys_domain_path", required = true)
        protected String  cbssysdomainpath;
        @XmlElement(name = "vmi_image_path", required = true)
        protected String  vmiimagepath;
        @XmlElement(name = "vb_u_max_size_of_storage_gb", required = true)
        protected String  vbumaxsizeofstoragegb;
        @XmlElement(name = "vmi_lease_end", required = true)
        protected String  vmileaseend;
        @XmlElement(name = "vb_u_device_ownership", required = true)
        protected String  vbudeviceownership;
        @XmlElement(name = "vmi_state", required = true)
        protected String  vmistate;
        @XmlElement(name = "vc_short_description", required = true)
        protected String  vcshortdescription;
        @XmlElement(name = "vmi_sys_domain", required = true)
        protected String  vmisysdomain;
        @XmlElement(name = "vb_u_vblock_rcm_release_configuration_matrix", required = true)
        protected String  vbuvblockrcmreleaseconfigurationmatrix;
        @XmlElement(name = "vb_u_emc_serial_number", required = true)
        protected String  vbuemcserialnumber;
        @XmlElement(name = "vb_sys_id", required = true)
        protected String  vbsysid;
        @XmlElement(name = "vb_u_financial_model", required = true)
        protected String  vbufinancialmodel;
        @XmlElement(name = "vb_sys_domain", required = true)
        protected String  vbsysdomain;
        @XmlElement(name = "vb_u_number_of_vcpus_for_vblock", required = true)
        protected String  vbunumberofvcpusforvblock;
        @XmlElement(name = "vmi_correlation_id", required = true)
        protected String  vmicorrelationid;
        @XmlElement(name = "vmi_sys_domain_path", required = true)
        protected String  vmisysdomainpath;
        @XmlElement(name = "cbs_u_deployment_type", required = true)
        protected String  cbsudeploymenttype;
        @XmlElement(name = "vmi_template", required = true)
        protected String  vmitemplate;
        @XmlElement(name = "vmi_u_vm_alias", required = true)
        protected String  vmiuvmalias;
		@XmlElement(name = "vmi_u_link", required = true)
        protected String  vmiulink;
        @XmlElement(name = "vb_u_sotrage_model", required = true)
        protected String  vbusotragemodel;
        @XmlElement(name = "vb_u_floor", required = true)
        protected String  vbufloor;
        @XmlElement(name = "vc_fullname", required = true)
        protected String  vcfullname;
        @XmlElement(name = "vb_u_number_of_racks", required = true)
        protected String  vbunumberofracks;
        @XmlElement(name = "vb_u_delivery_model", required = true)
        protected String  vbudeliverymodel;
        @XmlElement(name = "vb_u_local_site_buidling", required = true)
        protected String  vbulocalsitebuidling;
        @XmlElement(name = "cbs_sys_id", required = true)
        protected String  cbssysid;
        @XmlElement(name = "vb_serial_number", required = true)
        protected String  vbserialnumber;
        @XmlElement(name = "vc_sys_id", required = true)
        protected String  vcsysid;
        @XmlElement(name = "vb_u_vce_contract_number", required = true)
        protected String  vbuvcecontractnumber;
        @XmlElement(name = "vmi_cpus", required = true)
        protected String  vmicpus;
        /*@XmlElement(name = "vb_u_vblock_status", required = true)
        protected String  vbuvblockstatus;
        */@XmlElement(name = "vc_sys_domain", required = true)
        protected String  vcsysdomain;
        @XmlElement(name = "vb_u_vblock_type", required = true)
        protected String  vbuvblocktype;
        @XmlElement(name = "vb_u_max_memory_gb", required = true)
        protected String  vbumaxmemorygb;
        @XmlElement(name = "vb_u_vce_vision_url", required = true)
        protected String  vbuvcevisionurl;
        @XmlElement(name = "ci_u_contract_end_date", required = true)
        protected String  ciucontractenddate;
        @XmlElement(name = "ci_u_contract_start_date", required = true)
        protected String  ciucontractstartdate;
        @XmlElement(name = "ci_u_contract_duration", required = true)
        protected String  ciucontractduration;
        @XmlElement(name = "vmi_short_description", required = true)
        protected String  vmishortdescription;
        @XmlElement(name = "vmi_disks_size", required = true)
        protected String  vmidiskssize;
        @XmlElement(name = "vb_u_boq_url", required = true)
        protected String  vbuboqurl;
        @XmlElement(name = "vmi_lease_start", required = true)
        protected String  vmileasestart;
        @XmlElement(name = "vc_url", required = true)
        protected String  vcurl;
        @XmlElement(name = "vmi_nics", required = true)
        protected String  vminics;
        @XmlElement(name = "vb_u_emc_site_id", required = true)
        protected String  vbuemcsiteid;
        @XmlElement(name = "vb_u_vmware_contract_number", required = true)
        protected String  vbuvmwarecontractnumber;
        @XmlElement(name = "vmi_disks", required = true)
        protected String  vmidisks;
        @XmlElement(name = "vb_u_room", required = true)
        protected String  vburoom;
        @XmlElement(name = "vb_u_vblock_system_type", required = true)
        protected String  vbuvblocksystemtype;
        @XmlElement(name = "vmi_sys_id", required = true)
        protected String  vmisysid;
        @XmlElement(name = "ci_u_sri_type")
        protected String  usritype;        
        @XmlElement(name = "cpe_u_product_classification", required = true)
        protected String cpeuproductclassification; 
        
        
        
        
        public String getVmiuvmalias() {
   			return vmiuvmalias;
   		}
   		public void setVmiuvmalias(String vmiuvmalias) {
   			this.vmiuvmalias = vmiuvmalias;
   		}
		public String getCiName() {
			return ciName;
		}
		public void setCiName(String ciName) {
			this.ciName = ciName;
		}
		public byte getCiOperationalStatus() {
			return ciOperationalStatus;
		}
		public void setCiOperationalStatus(byte ciOperationalStatus) {
			this.ciOperationalStatus = ciOperationalStatus;
		}
		public String getCiSysClassName() {
			return ciSysClassName;
		}
		public void setCiSysClassName(String ciSysClassName) {
			this.ciSysClassName = ciSysClassName;
		}
		public String getCiSysCreatedBy() {
			return ciSysCreatedBy;
		}
		public void setCiSysCreatedBy(String ciSysCreatedBy) {
			this.ciSysCreatedBy = ciSysCreatedBy;
		}
		public String getCiSysCreatedOn() {
			return ciSysCreatedOn;
		}
		public void setCiSysCreatedOn(String ciSysCreatedOn) {
			this.ciSysCreatedOn = ciSysCreatedOn;
		}
		public String getCiSysDomain() {
			return ciSysDomain;
		}
		public void setCiSysDomain(String ciSysDomain) {
			this.ciSysDomain = ciSysDomain;
		}
		public String getCiSysDomainPath() {
			return ciSysDomainPath;
		}
		public void setCiSysDomainPath(String ciSysDomainPath) {
			this.ciSysDomainPath = ciSysDomainPath;
		}
		public String getCiSysId() {
			return ciSysId;
		}
		public void setCiSysId(String ciSysId) {
			this.ciSysId = ciSysId;
		}
		public String getCiSysUpdatedBy() {
			return ciSysUpdatedBy;
		}
		public void setCiSysUpdatedBy(String ciSysUpdatedBy) {
			this.ciSysUpdatedBy = ciSysUpdatedBy;
		}
		public String getCiSysUpdatedOn() {
			return ciSysUpdatedOn;
		}
		public void setCiSysUpdatedOn(String ciSysUpdatedOn) {
			this.ciSysUpdatedOn = ciSysUpdatedOn;
		}
		public String getCiuactivatedon() {
			return ciuactivatedon;
		}
		public void setCiuactivatedon(String ciuactivatedon) {
			this.ciuactivatedon = ciuactivatedon;
		}
		public String getCiuccdcustomercommitteddate() {
			return ciuccdcustomercommitteddate;
		}
		public void setCiuccdcustomercommitteddate(String ciuccdcustomercommitteddate) {
			this.ciuccdcustomercommitteddate = ciuccdcustomercommitteddate;
		}
		public String getCiucideliverystatus() {
			return ciucideliverystatus;
		}
		public void setCiucideliverystatus(String ciucideliverystatus) {
			this.ciucideliverystatus = ciucideliverystatus;
		}
		public String getCiucitype() {
			return ciucitype;
		}
		public void setCiucitype(String ciucitype) {
			this.ciucitype = ciucitype;
		}
		public String getCiucustomeruniqueid() {
			return ciucustomeruniqueid;
		}
		public void setCiucustomeruniqueid(String ciucustomeruniqueid) {
			this.ciucustomeruniqueid = ciucustomeruniqueid;
		}
		public String getCiudecommissioneddate() {
			return ciudecommissioneddate;
		}
		public void setCiudecommissioneddate(String ciudecommissioneddate) {
			this.ciudecommissioneddate = ciudecommissioneddate;
		}
		public String getCiulink() {
			return ciulink;
		}
		public void setCiulink(String ciulink) {
			this.ciulink = ciulink;
		}
		public String getCiunumber() {
			return ciunumber;
		}
		public void setCiunumber(String ciunumber) {
			this.ciunumber = ciunumber;
		}
		public String getCiuproductid() {
			return ciuproductid;
		}
		public void setCiuproductid(String ciuproductid) {
			this.ciuproductid = ciuproductid;
		}
		public String getCiurootproductid() {
			return ciurootproductid;
		}
		public void setCiurootproductid(String ciurootproductid) {
			this.ciurootproductid = ciurootproductid;
		}
		public String getCiusrireferencenumber() {
			return ciusrireferencenumber;
		}
		public void setCiusrireferencenumber(String ciusrireferencenumber) {
			this.ciusrireferencenumber = ciusrireferencenumber;
		}
		public String getCpename() {
			return cpename;
		}
		public void setCpename(String cpename) {
			this.cpename = cpename;
		}
		public String getCpesysdomain() {
			return cpesysdomain;
		}
		public void setCpesysdomain(String cpesysdomain) {
			this.cpesysdomain = cpesysdomain;
		}
		public String getCpesysdomainpath() {
			return cpesysdomainpath;
		}
		public void setCpesysdomainpath(String cpesysdomainpath) {
			this.cpesysdomainpath = cpesysdomainpath;
		}
		public String getCpesysid() {
			return cpesysid;
		}
		public void setCpesysid(String cpesysid) {
			this.cpesysid = cpesysid;
		}
		public String getCpeucustomeredgedevice() {
			return cpeucustomeredgedevice;
		}
		public void setCpeucustomeredgedevice(String cpeucustomeredgedevice) {
			this.cpeucustomeredgedevice = cpeucustomeredgedevice;
		}
		public String getCpeudevicetype() {
			return cpeudevicetype;
		}
		public void setCpeudevicetype(String cpeudevicetype) {
			this.cpeudevicetype = cpeudevicetype;
		}
		public String getCpeudevicevendor() {
			return cpeudevicevendor;
		}
		public void setCpeudevicevendor(String cpeudevicevendor) {
			this.cpeudevicevendor = cpeudevicevendor;
		}
		public String getPeinterfacename() {
			return peinterfacename;
		}
		public void setPeinterfacename(String peinterfacename) {
			this.peinterfacename = peinterfacename;
		}
		public String getPeinterfacesysdomain() {
			return peinterfacesysdomain;
		}
		public void setPeinterfacesysdomain(String peinterfacesysdomain) {
			this.peinterfacesysdomain = peinterfacesysdomain;
		}
		public String getPeinterfacesysdomainpath() {
			return peinterfacesysdomainpath;
		}
		public void setPeinterfacesysdomainpath(String peinterfacesysdomainpath) {
			this.peinterfacesysdomainpath = peinterfacesysdomainpath;
		}
		public String getPeinterfacesysid() {
			return peinterfacesysid;
		}
		public void setPeinterfacesysid(String peinterfacesysid) {
			this.peinterfacesysid = peinterfacesysid;
		}
		public String getPeinterfaceucustomerservingnetworkinterface() {
			return peinterfaceucustomerservingnetworkinterface;
		}
		public void setPeinterfaceucustomerservingnetworkinterface(
				String peinterfaceucustomerservingnetworkinterface) {
			this.peinterfaceucustomerservingnetworkinterface = peinterfaceucustomerservingnetworkinterface;
		}
		public String getReqnumber() {
			return reqnumber;
		}
		public void setReqnumber(String reqnumber) {
			this.reqnumber = reqnumber;
		}
		public String getReqsysdomain() {
			return reqsysdomain;
		}
		public void setReqsysdomain(String reqsysdomain) {
			this.reqsysdomain = reqsysdomain;
		}
		public String getReqsysdomainpath() {
			return reqsysdomainpath;
		}
		public void setReqsysdomainpath(String reqsysdomainpath) {
			this.reqsysdomainpath = reqsysdomainpath;
		}
		public String getReqsysid() {
			return reqsysid;
		}
		public void setReqsysid(String reqsysid) {
			this.reqsysid = reqsysid;
		}
		public String getRitmnumber() {
			return ritmnumber;
		}
		public void setRitmnumber(String ritmnumber) {
			this.ritmnumber = ritmnumber;
		}
		public String getSitesysid() {
			return sitesysid;
		}
		public void setSitesysid(String sitesysid) {
			this.sitesysid = sitesysid;
		}
		public String getSiteusiteid() {
			return siteusiteid;
		}
		public void setSiteusiteid(String siteusiteid) {
			this.siteusiteid = siteusiteid;
		}
		public String getVbsysdomainpath() {
			return vbsysdomainpath;
		}
		public void setVbsysdomainpath(String vbsysdomainpath) {
			this.vbsysdomainpath = vbsysdomainpath;
		}
		public String getVbuvblockrcmreleaseconfigurationmatrix() {
			return vbuvblockrcmreleaseconfigurationmatrix;
		}
		public void setVbuvblockrcmreleaseconfigurationmatrix(
				String vbuvblockrcmreleaseconfigurationmatrix) {
			this.vbuvblockrcmreleaseconfigurationmatrix = vbuvblockrcmreleaseconfigurationmatrix;
		}
		public String getVbulocalsitebuidling() {
			return vbulocalsitebuidling;
		}
		public void setVbulocalsitebuidling(String vbulocalsitebuidling) {
			this.vbulocalsitebuidling = vbulocalsitebuidling;
		}
		public String getVbuciscocontractnumber() {
			return vbuciscocontractnumber;
		}
		public void setVbuciscocontractnumber(String vbuciscocontractnumber) {
			this.vbuciscocontractnumber = vbuciscocontractnumber;
		}
		public String getVcapiversion() {
			return vcapiversion;
		}
		public void setVcapiversion(String vcapiversion) {
			this.vcapiversion = vcapiversion;
		}
		public String getVbuemcserialnumber() {
			return vbuemcserialnumber;
		}
		public void setVbuemcserialnumber(String vbuemcserialnumber) {
			this.vbuemcserialnumber = vbuemcserialnumber;
		}
		public String getVmiimagepath() {
			return vmiimagepath;
		}
		public void setVmiimagepath(String vmiimagepath) {
			this.vmiimagepath = vmiimagepath;
		}
		public String getVcsysdomain() {
			return vcsysdomain;
		}
		public void setVcsysdomain(String vcsysdomain) {
			this.vcsysdomain = vcsysdomain;
		}
		public String getVmileasestart() {
			return vmileasestart;
		}
		public void setVmileasestart(String vmileasestart) {
			this.vmileasestart = vmileasestart;
		}
		public String getVmicpus() {
			return vmicpus;
		}
		public void setVmicpus(String vmicpus) {
			this.vmicpus = vmicpus;
		}
		public String getVbsysid() {
			return vbsysid;
		}
		public void setVbsysid(String vbsysid) {
			this.vbsysid = vbsysid;
		}
		public String getVbuboqurl() {
			return vbuboqurl;
		}
		public void setVbuboqurl(String vbuboqurl) {
			this.vbuboqurl = vbuboqurl;
		}
		public String getCbsudeploymenttype() {
			return cbsudeploymenttype;
		}
		public void setCbsudeploymenttype(String cbsudeploymenttype) {
			this.cbsudeploymenttype = cbsudeploymenttype;
		}
		public String getVbumaxofcpus() {
			return vbumaxofcpus;
		}
		public void setVbumaxofcpus(String vbumaxofcpus) {
			this.vbumaxofcpus = vbumaxofcpus;
		}
		public String getCbssysdomainpath() {
			return cbssysdomainpath;
		}
		public void setCbssysdomainpath(String cbssysdomainpath) {
			this.cbssysdomainpath = cbssysdomainpath;
		}
		public String getVmisysdomain() {
			return vmisysdomain;
		}
		public void setVmisysdomain(String vmisysdomain) {
			this.vmisysdomain = vmisysdomain;
		}
		public String getVmitemplate() {
			return vmitemplate;
		}
		public void setVmitemplate(String vmitemplate) {
			this.vmitemplate = vmitemplate;
		}
		public String getVbumaintenanceoption() {
			return vbumaintenanceoption;
		}
		public void setVbumaintenanceoption(String vbumaintenanceoption) {
			this.vbumaintenanceoption = vbumaintenanceoption;
		}
		public String getVbuvblocktype() {
			return vbuvblocktype;
		}
		public void setVbuvblocktype(String vbuvblocktype) {
			this.vbuvblocktype = vbuvblocktype;
		}
		public String getVbserialnumber() {
			return vbserialnumber;
		}
		public void setVbserialnumber(String vbserialnumber) {
			this.vbserialnumber = vbserialnumber;
		}
		public String getVminics() {
			return vminics;
		}
		public void setVminics(String vminics) {
			this.vminics = vminics;
		}
		public String getVbudeviceownership() {
			return vbudeviceownership;
		}
		public void setVbudeviceownership(String vbudeviceownership) {
			this.vbudeviceownership = vbudeviceownership;
		}
		public String getVmistate() {
			return vmistate;
		}
		public void setVmistate(String vmistate) {
			this.vmistate = vmistate;
		}
		public String getVbulocalsitefloor() {
			return vbulocalsitefloor;
		}
		public void setVbulocalsitefloor(String vbulocalsitefloor) {
			this.vbulocalsitefloor = vbulocalsitefloor;
		}
		public String getVmileaseend() {
			return vmileaseend;
		}
		public void setVmileaseend(String vmileaseend) {
			this.vmileaseend = vmileaseend;
		}
		public String getCbssysdomain() {
			return cbssysdomain;
		}
		public void setCbssysdomain(String cbssysdomain) {
			this.cbssysdomain = cbssysdomain;
		}
		public String getVmimodelid() {
			return vmimodelid;
		}
		public void setVmimodelid(String vmimodelid) {
			this.vmimodelid = vmimodelid;
		}
		public String getVbubuilding() {
			return vbubuilding;
		}
		public void setVbubuilding(String vbubuilding) {
			this.vbubuilding = vbubuilding;
		}
		public String getVmicorrelationid() {
			return vmicorrelationid;
		}
		public void setVmicorrelationid(String vmicorrelationid) {
			this.vmicorrelationid = vmicorrelationid;
		}
		public String getVbuvblocksystemtype() {
			return vbuvblocksystemtype;
		}
		public void setVbuvblocksystemtype(String vbuvblocksystemtype) {
			this.vbuvblocksystemtype = vbuvblocksystemtype;
		}
		public String getVcsysdomainpath() {
			return vcsysdomainpath;
		}
		public void setVcsysdomainpath(String vcsysdomainpath) {
			this.vcsysdomainpath = vcsysdomainpath;
		}
		public String getVbumaxsizeofstoragegb() {
			return vbumaxsizeofstoragegb;
		}
		public void setVbumaxsizeofstoragegb(String vbumaxsizeofstoragegb) {
			this.vbumaxsizeofstoragegb = vbumaxsizeofstoragegb;
		}
		public String getVbumaxmemorygb() {
			return vbumaxmemorygb;
		}
		public void setVbumaxmemorygb(String vbumaxmemorygb) {
			this.vbumaxmemorygb = vbumaxmemorygb;
		}
		public String getVmishortdescription() {
			return vmishortdescription;
		}
		public void setVmishortdescription(String vmishortdescription) {
			this.vmishortdescription = vmishortdescription;
		}
		public String getVbufloor() {
			return vbufloor;
		}
		public void setVbufloor(String vbufloor) {
			this.vbufloor = vbufloor;
		}
		public String getVcfullname() {
			return vcfullname;
		}
		public void setVcfullname(String vcfullname) {
			this.vcfullname = vcfullname;
		}
		public String getVbufinancialmodel() {
			return vbufinancialmodel;
		}
		public void setVbufinancialmodel(String vbufinancialmodel) {
			this.vbufinancialmodel = vbufinancialmodel;
		}
		public String getVbudeliverymodel() {
			return vbudeliverymodel;
		}
		public void setVbudeliverymodel(String vbudeliverymodel) {
			this.vbudeliverymodel = vbudeliverymodel;
		}
		public String getVbutypemodel() {
			return vbutypemodel;
		}
		public void setVbutypemodel(String vbutypemodel) {
			this.vbutypemodel = vbutypemodel;
		}
		public String getVbuvmwarecontractnumber() {
			return vbuvmwarecontractnumber;
		}
		public void setVbuvmwarecontractnumber(String vbuvmwarecontractnumber) {
			this.vbuvmwarecontractnumber = vbuvmwarecontractnumber;
		}
		public String getVcshortdescription() {
			return vcshortdescription;
		}
		public void setVcshortdescription(String vcshortdescription) {
			this.vcshortdescription = vcshortdescription;
		}
		public String getCbsudevicevendor() {
			return cbsudevicevendor;
		}
		public void setCbsudevicevendor(String cbsudevicevendor) {
			this.cbsudevicevendor = cbsudevicevendor;
		}
		public String getVbuvcecontractnumber() {
			return vbuvcecontractnumber;
		}
		public void setVbuvcecontractnumber(String vbuvcecontractnumber) {
			this.vbuvcecontractnumber = vbuvcecontractnumber;
		}
		public String getVmimemory() {
			return vmimemory;
		}
		public void setVmimemory(String vmimemory) {
			this.vmimemory = vmimemory;
		}
		public String getVburoom() {
			return vburoom;
		}
		public void setVburoom(String vburoom) {
			this.vburoom = vburoom;
		}
		public String getVbsysdomain() {
			return vbsysdomain;
		}
		public void setVbsysdomain(String vbsysdomain) {
			this.vbsysdomain = vbsysdomain;
		}
		public String getVbusotragemodel() {
			return vbusotragemodel;
		}
		public void setVbusotragemodel(String vbusotragemodel) {
			this.vbusotragemodel = vbusotragemodel;
		}
		public String getVbunumberofracks() {
			return vbunumberofracks;
		}
		public void setVbunumberofracks(String vbunumberofracks) {
			this.vbunumberofracks = vbunumberofracks;
		}
		public String getVmiulink() {
			return vmiulink;
		}
		public void setVmiulink(String vmiulink) {
			this.vmiulink = vmiulink;
		}
		public String getCbssysid() {
			return cbssysid;
		}
		public void setCbssysid(String cbssysid) {
			this.cbssysid = cbssysid;
		}
		public String getVmidiskssize() {
			return vmidiskssize;
		}
		public void setVmidiskssize(String vmidiskssize) {
			this.vmidiskssize = vmidiskssize;
		}
		public String getVcsysid() {
			return vcsysid;
		}
		public void setVcsysid(String vcsysid) {
			this.vcsysid = vcsysid;
		}
		public String getVbuvcevisionurl() {
			return vbuvcevisionurl;
		}
		public void setVbuvcevisionurl(String vbuvcevisionurl) {
			this.vbuvcevisionurl = vbuvcevisionurl;
		}
		public String getVmidisks() {
			return vmidisks;
		}
		public void setVmidisks(String vmidisks) {
			this.vmidisks = vmidisks;
		}
		public String getVmisysid() {
			return vmisysid;
		}
		public void setVmisysid(String vmisysid) {
			this.vmisysid = vmisysid;
		}
		public String getVcurl() {
			return vcurl;
		}
		public void setVcurl(String vcurl) {
			this.vcurl = vcurl;
		}
		public String getVbuemcsiteid() {
			return vbuemcsiteid;
		}
		public void setVbuemcsiteid(String vbuemcsiteid) {
			this.vbuemcsiteid = vbuemcsiteid;
		}
		public String getVmisysdomainpath() {
			return vmisysdomainpath;
		}
		public void setVmisysdomainpath(String vmisysdomainpath) {
			this.vmisysdomainpath = vmisysdomainpath;
		}
		
		
		
		public String getCiusrreferenceid() {
			return ciusrreferenceid;
		}
		public void setCiusrreferenceid(String ciusrreferenceid) {
			this.ciusrreferenceid = ciusrreferenceid;
		}
		public String getCiusitename() {
			return ciusitename;
		}
		public void setCiusitename(String ciusitename) {
			this.ciusitename = ciusitename;
		}
		public String getVbunumberofvcpusforvblock() {
			return vbunumberofvcpusforvblock;
		}
		public void setVbunumberofvcpusforvblock(String vbunumberofvcpusforvblock) {
			this.vbunumberofvcpusforvblock = vbunumberofvcpusforvblock;
		}
		/*public String getVbuvblockstatus() {
			return vbuvblockstatus;
		}
		public void setVbuvblockstatus(String vbuvblockstatus) {
			this.vbuvblockstatus = vbuvblockstatus;
		}
		*/public String getCiucontractenddate() {
			return ciucontractenddate;
		}
		public void setCiucontractenddate(String ciucontractenddate) {
			this.ciucontractenddate = ciucontractenddate;
		}
		public String getCiucontractstartdate() {
			return ciucontractstartdate;
		}
		public void setCiucontractstartdate(String ciucontractstartdate) {
			this.ciucontractstartdate = ciucontractstartdate;
		}
		public String getCiucontractduration() {
			return ciucontractduration;
		}
		public void setCiucontractduration(String ciucontractduration) {
			this.ciucontractduration = ciucontractduration;
		}
		public String getUsritype() {
			return usritype;
		}
		public void setUsritype(String usritype) {
			this.usritype = usritype;
		}
		
		public String getCpeuproductclassification() {
			return cpeuproductclassification;
		}
		public void setCpeuproductclassification(String cpeuproductclassification) {
			this.cpeuproductclassification = cpeuproductclassification;
		}
		
		
		
  }

}
