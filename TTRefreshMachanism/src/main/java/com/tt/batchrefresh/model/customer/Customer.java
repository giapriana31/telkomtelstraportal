/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh.model.customer;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Infosys
 *
 */
@Entity
@Table(name = "customers")
public class Customer {
    
    private Long id;
    
    @Column(name = "customeruniqueid")
    private String customeruniqueid;

    @Column(name = "country")
    private String country;

    @Column(name = "fiscalyear")
    private String fiscalYear;

    @Column(name = "accountname")
    private String accountName;

    @Column(name = "state")
    private String state;

    @Column(name = "createdby")
    private String createdBy;

    @Column(name = "createdDate")
    private Date createdDate;

    @Column(name = "updatedBy")
    private String updatedBy;

    @Column(name = "updatedDate")
    private Date updatedDate;

    @Column(name = "bronzeavailabilitysla")
    private BigDecimal bronzeAvailabilitySLA;

    @Column(name = "description")
    private String description;

    @Column(name = "goldavailabilitysla")
    private BigDecimal goldAvailabilitySLA;

    @Column(name = "silveravailabilitysla")
    private BigDecimal silverAvailabilitySLA;

    @Column(name = "active")
    private String active;

    
    private BigDecimal managedAvailability;


    
   


    @Column(name = "customertype")
    private String customerType;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "numberofoktausers")
    private Integer numberOfOktaUsers;

   
    @Column(name="fullymanagedcontractedavailability")
    public BigDecimal getManagedAvailability() {
		return managedAvailability;
	}

	public void setManagedAvailability(BigDecimal managedAvailability) {
		this.managedAvailability = managedAvailability;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	@Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   
    public String getCustomeruniqueid() {
        return customeruniqueid;
    }

    public void setCustomeruniqueid(String customeruniqueid) {
        this.customeruniqueid = customeruniqueid;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public String getFiscalYear() {
	return fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
	this.fiscalYear = fiscalYear;
    }

    public String getAccountName() {
	return accountName;
    }

    public void setAccountName(String accountName) {
	this.accountName = accountName;
    }

    public String getState() {
	return state;
    }

    public void setState(String state) {
	this.state = state;
    }

    public String getCreatedBy() {
	return createdBy;
    }

    public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
	return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
	this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
	return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
	this.updatedDate = updatedDate;
    }

   

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }
    
	public BigDecimal getBronzeAvailabilitySLA() {
		return bronzeAvailabilitySLA;
	}

	public void setBronzeAvailabilitySLA(BigDecimal bronzeAvailabilitySLA) {
		this.bronzeAvailabilitySLA = bronzeAvailabilitySLA;
	}

	public BigDecimal getGoldAvailabilitySLA() {
		return goldAvailabilitySLA;
	}

	public void setGoldAvailabilitySLA(BigDecimal goldAvailabilitySLA) {
		this.goldAvailabilitySLA = goldAvailabilitySLA;
	}

	public BigDecimal getSilverAvailabilitySLA() {
		return silverAvailabilitySLA;
	}

	public void setSilverAvailabilitySLA(BigDecimal silverAvailabilitySLA) {
		this.silverAvailabilitySLA = silverAvailabilitySLA;
	}

	public String getActive() {
	return active;
    }

    public void setActive(String active) {
	this.active = active;
    }
    
    

    public Integer getNumberOfOktaUsers() {
		return numberOfOktaUsers;
	}

	public void setNumberOfOktaUsers(Integer numberOfOktaUsers) {
		this.numberOfOktaUsers = numberOfOktaUsers;
	}

	@Override
    public String toString() {
	return "Customer [customerUniqueId=" + customeruniqueid + ", country="
		+ country + ", fiscalYear=" + fiscalYear + ", accountName="
		+ accountName + ", state=" + state + ", createdBy=" + createdBy
		+ ", createdDate=" + createdDate + ", updatedBy=" + updatedBy
		+ ", updatedDate=" + updatedDate + ", bronzeAvailabilitySLA="
		+ bronzeAvailabilitySLA + ", description=" + description
		+ ", goldAvailabilitySLA=" + goldAvailabilitySLA
		+ ", silverAvailabilitySLA=" + silverAvailabilitySLA
		+ ", active=" + active + "]";
    }

    
    
}
