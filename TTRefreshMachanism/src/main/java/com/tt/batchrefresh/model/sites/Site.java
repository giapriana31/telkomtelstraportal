package com.tt.batchrefresh.model.sites;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;


@Entity
@Table(name = "site")
public class Site {

    @Id
    private String siteid;
    private String city;
    private String country;
    private Double latitude;
    private Double longitude;
    private String customersite;
    private String stateprovince;
    private String createdby;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createddate;
    private String updatedby;
    @Column(name="updatedate")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updatedon;
    private String timezone;
    @Column(name = "zone")
    private String sysZone; // zone is a reserved SQL99 keyword
    private String servicetier;
    private String siteoperationalhours;
    private String customeruniqueid;
    private String status;
    
    private String isDataCenter;
    private String dataCenterType;
    private String dataCenterName;
    private String isManaged;
    @Column(name = "rootproductid")
    private String rootProductId;
    
    @Column(name = "pvsitedeliverystatus")
    private String pvsitedeliverystatus;
    
    public String getSiteid() {
	return siteid;
    }

    public void setSiteid(String siteid) {
	this.siteid = siteid;
    }

    public String getCity() {
	return city;
    }

    public void setCity(String city) {
	this.city = city;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public Double getLatitude() {
	return latitude;
    }

    public void setLatitude(Double latitude) {
	this.latitude = latitude;
    }

    public Double getLongitude() {
	return longitude;
    }

    public void setLongitude(Double longitude) {
	this.longitude = longitude;
    }

    public String getCustomersite() {
	return customersite;
    }

    public void setCustomersite(String customersite) {
	this.customersite = customersite;
    }

    public String getStateprovince() {
	return stateprovince;
    }

    public void setStateprovince(String stateprovince) {
	this.stateprovince = stateprovince;
    }

    public String getCreatedby() {
	return createdby;
    }

    public void setCreatedby(String createdby) {
	this.createdby = createdby;
    }

    public Date getCreateddate() {
	return createddate;
    }

    public void setCreateddate(Date createddate) {
	this.createddate = createddate;
    }

    public String getUpdatedby() {
	return updatedby;
    }

    public void setUpdatedby(String updatedby) {
	this.updatedby = updatedby;
    }

    public Date getUpdatedon() {
	return updatedon;
    }

    public void setUpdatedon(Date updatedon) {
	this.updatedon = updatedon;
    }

    public String getTimezone() {
	return timezone;
    }

    public void setTimezone(String timezone) {
	this.timezone = timezone;
    }

    public String getSysZone() {
	return sysZone;
    }

    public void setSysZone(String sysZone) {
	this.sysZone = sysZone;
    }

    public String getServicetier() {
	return servicetier;
    }

    public void setServicetier(String servicetier) {
	this.servicetier = servicetier;
    }

    public String getSiteoperationalhours() {
	return siteoperationalhours;
    }

    public void setSiteoperationalhours(String siteoperationalhours) {
	this.siteoperationalhours = siteoperationalhours;
    }

    public String getCustomeruniqueid() {
	return customeruniqueid;
    }

    public void setCustomeruniqueid(String customeruniqueid) {
	this.customeruniqueid = customeruniqueid;
    }
    
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getIsDataCenter() {
		return isDataCenter;
	}

	public void setIsDataCenter(String isDataCenter) {
		this.isDataCenter = isDataCenter;
	}

	public String getDataCenterType() {
		return dataCenterType;
	}

	public void setDataCenterType(String dataCenterType) {
		this.dataCenterType = dataCenterType;
	}

	public String getDataCenterName() {
		return dataCenterName;
	}

	public void setDataCenterName(String dataCenterName) {
		this.dataCenterName = dataCenterName;
	}

	public String getIsManaged() {
		return isManaged;
	}

	public void setIsManaged(String isManaged) {
		this.isManaged = isManaged;
	}

	public String getPvsitedeliverystatus() {
		return pvsitedeliverystatus;
	}

	public void setPvsitedeliverystatus(String pvsitedeliverystatus) {
		this.pvsitedeliverystatus = pvsitedeliverystatus;
	}
	
	
	public String getRootProductId() {
		return rootProductId;
	}

	public void setRootProductId(String rootProductId) {
		this.rootProductId = rootProductId;
	}

	public String toString()
    {
    	StringBuilder sb = new StringBuilder();
    	sb.append("siteID: ")
    	.append(siteid)
    	.append(" | custuniqueid: ")
    	.append(customeruniqueid);
    	return sb.toString();
    }

}
