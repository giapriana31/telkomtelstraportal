package com.tt.batchrefresh.model.ci;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="vblock")
public class VBlock implements Serializable
{
	@Id
	@GeneratedValue
	private long id;
	
	@OneToOne
	@JoinColumn(name = "ciid", referencedColumnName="ciid")
	private Configuration ciid;
	
	@Column(name = "deliverymodel")
	private String deliveryModel;

	@Column(name = "vblocktype")
	private String vBlockType;

	@Column(name = "vblocksystemtype")
	private String vBlockSystemType;

	@Column(name = "storagemodel")
	private String storageModel;

	@Column(name = "numberofracks")
	private String numberOfRacks;

	@Column(name = "maxnumberofvms")
	private String maxNumberOfVMs;

	@Column(name = "maxsizeofstorage")
	private String maxSizeOfStorage;

	@Column(name = "maxnumberofcpus")
	private String maxNumberOfCPUs;

	@Column(name = "maxmemory")
	private String maxMemory;

	@Column(name = "vcecontractnumber")
	private String vceContractNumber;

	@Column(name = "ciscocontractnumber")
	private String ciscoContractNumber;

	@Column(name = "vmwarecontractnumber")
	private String vmwareContractNumber;

	@Column(name = "emcsiteid")
	private String emcSiteId;

	@Column(name = "emcserialnumber")
	private String emcSerialNumber;

	@Column(name = "typemodel")
	private String typeModel;

	@Column(name = "deviceownership")
	private String deviceOwnerShip;

	@Column(name = "serialnumber")
	private String serialNumber;

	@Column(name = "maintainanceoption")
	private String maintainanceOption;

	@Column(name = "vblockrcm")
	private String vBlockRCM;

	@Column(name = "localsitebuilding")
	private String localSiteBuilding;

	@Column(name = "localsitefloor")
	private String localSiteFloor;

	@Column(name = "boqurl")
	private String boqURL;

	@Column(name = "financialmodel")
	private String financialModel;

	@Column(name = "vcevisionurl")
	private String vceVisionURL;

	private String room;
	private String floor;
	private String building;

	
	@Column(name= "numberofvcpu")
	private String numberOfVcpu;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDeliveryModel() {
		return deliveryModel;
	}

	public void setDeliveryModel(String deliveryModel) {
		this.deliveryModel = deliveryModel;
	}

	public String getvBlockType() {
		return vBlockType;
	}

	public void setvBlockType(String vBlockType) {
		this.vBlockType = vBlockType;
	}

	public String getvBlockSystemType() {
		return vBlockSystemType;
	}

	public void setvBlockSystemType(String vBlockSystemType) {
		this.vBlockSystemType = vBlockSystemType;
	}

	public String getStorageModel() {
		return storageModel;
	}

	public void setStorageModel(String storageModel) {
		this.storageModel = storageModel;
	}

	public String getNumberOfRacks() {
		return numberOfRacks;
	}

	public void setNumberOfRacks(String numberOfRacks) {
		this.numberOfRacks = numberOfRacks;
	}

	public String getMaxNumberOfVMs() {
		return maxNumberOfVMs;
	}

	public void setMaxNumberOfVMs(String maxNumberOfVMs) {
		this.maxNumberOfVMs = maxNumberOfVMs;
	}

	public String getMaxSizeOfStorage() {
		return maxSizeOfStorage;
	}

	public void setMaxSizeOfStorage(String maxSizeOfStorage) {
		this.maxSizeOfStorage = maxSizeOfStorage;
	}

	public String getMaxNumberOfCPUs() {
		return maxNumberOfCPUs;
	}

	public void setMaxNumberOfCPUs(String maxNumberOfCPUs) {
		this.maxNumberOfCPUs = maxNumberOfCPUs;
	}

	public String getMaxMemory() {
		return maxMemory;
	}

	public void setMaxMemory(String maxMemory) {
		this.maxMemory = maxMemory;
	}

	public String getVceContractNumber() {
		return vceContractNumber;
	}

	public void setVceContractNumber(String vceContractNumber) {
		this.vceContractNumber = vceContractNumber;
	}

	public String getCiscoContractNumber() {
		return ciscoContractNumber;
	}

	public void setCiscoContractNumber(String ciscoContractNumber) {
		this.ciscoContractNumber = ciscoContractNumber;
	}

	public String getVmwareContractNumber() {
		return vmwareContractNumber;
	}

	public void setVmwareContractNumber(String vmwareContractNumber) {
		this.vmwareContractNumber = vmwareContractNumber;
	}

	public String getEmcSiteId() {
		return emcSiteId;
	}

	public void setEmcSiteId(String emcSiteId) {
		this.emcSiteId = emcSiteId;
	}

	public String getEmcSerialNumber() {
		return emcSerialNumber;
	}

	public void setEmcSerialNumber(String emcSerialNumber) {
		this.emcSerialNumber = emcSerialNumber;
	}

	public String getTypeModel() {
		return typeModel;
	}

	public void setTypeModel(String typeModel) {
		this.typeModel = typeModel;
	}

	public String getDeviceOwnerShip() {
		return deviceOwnerShip;
	}

	public void setDeviceOwnerShip(String deviceOwnerShip) {
		this.deviceOwnerShip = deviceOwnerShip;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getMaintainanceOption() {
		return maintainanceOption;
	}

	public void setMaintainanceOption(String maintainanceOption) {
		this.maintainanceOption = maintainanceOption;
	}

	public String getvBlockRCM() {
		return vBlockRCM;
	}

	public void setvBlockRCM(String vBlockRCM) {
		this.vBlockRCM = vBlockRCM;
	}

	public String getLocalSiteBuilding() {
		return localSiteBuilding;
	}

	public void setLocalSiteBuilding(String localSiteBuilding) {
		this.localSiteBuilding = localSiteBuilding;
	}

	public String getLocalSiteFloor() {
		return localSiteFloor;
	}

	public void setLocalSiteFloor(String localSiteFloor) {
		this.localSiteFloor = localSiteFloor;
	}

	public String getBoqURL() {
		return boqURL;
	}

	public void setBoqURL(String boqURL) {
		this.boqURL = boqURL;
	}

	public String getFinancialModel() {
		return financialModel;
	}

	public void setFinancialModel(String financialModel) {
		this.financialModel = financialModel;
	}

	public String getVceVisionURL() {
		return vceVisionURL;
	}

	public void setVceVisionURL(String vceVisionURL) {
		this.vceVisionURL = vceVisionURL;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}

	public Configuration getCiid() {
		return ciid;
	}

	public void setCiid(Configuration ciid) {
		this.ciid = ciid;
	}

	public String getNumberOfVcpu() {
		return numberOfVcpu;
	}

	public void setNumberOfVcpu(String numberOfVcpu) {
		this.numberOfVcpu = numberOfVcpu;
	}

	
}
