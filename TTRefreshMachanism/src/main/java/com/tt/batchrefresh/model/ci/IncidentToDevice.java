package com.tt.batchrefresh.model.ci;

import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "incdtodevicemapping")
public class IncidentToDevice {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="incidentid")
	private String incidentid;
	
	@Column(name = "deviceciid")
	private String deviceciid;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIncidentid() {
		return incidentid;
	}

	public void setIncidentid(String incidentid) {
		this.incidentid = incidentid;
	}
	
	public String getDeviceciid() {
		return deviceciid;
	}

	public void setDeviceciid(String deviceciid) {
		this.deviceciid = deviceciid;
	}
	
	

}
