package com.tt.batchrefresh.model.contarctAvailability;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "uContractAvailability"
})
@XmlRootElement(name = "xml")
public class Xml {

    @XmlElement(name = "u_contract_availability")
    protected List<Xml.uContractAvailability> uContractAvailability;

    public List<Xml.uContractAvailability> getUContractAvailability() {
        if (uContractAvailability == null) {
        	uContractAvailability = new ArrayList<Xml.uContractAvailability>();
        }
        return this.uContractAvailability;
    }


    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "createdBy",
        "createdOn",
        "sysId",
        "modCount",
        "updatedBy",
        "updatedOn",
        "contractAvailability",
        "customerDisplayName",
        "customerName",
        "number",
        "service",
        "serviceTier",
        "status"
    })
    public static class uContractAvailability {

        @XmlElement(name = "sys_created_by", required = true)
        protected String createdBy;
        @XmlElement(name = "sys_created_on", required = true)
        protected String createdOn;
        @XmlElement(name = "sys_id", required = true)
        protected String sysId;
        @XmlElement(name = "sys_mod_count", required = true)
        protected String modCount;
        @XmlElement(name = "sys_updated_by", required = true)
        protected String updatedBy;
        @XmlElement(name = "sys_updated_on", required = true)
        protected String updatedOn;
        @XmlElement(name = "u_contract_availability", required = true)
        protected String contractAvailability;
        @XmlElement(name = "u_customer_display_name", required = true)
        protected String customerDisplayName;
        @XmlElement(name = "u_customer_name", required = true)
        protected String customerName;
        @XmlElement(name = "u_number", required = true)
        protected String number;
        @XmlElement(name = "u_service", required = true)
        protected String service;
        @XmlElement(name = "u_service_tier", required = true)
        protected String serviceTier;
        @XmlElement(name = "u_status", required = true)
        protected String status;
        
        
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		public String getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}
		public String getSysId() {
			return sysId;
		}
		public void setSysId(String sysId) {
			this.sysId = sysId;
		}
		public String getModCount() {
			return modCount;
		}
		public void setModCount(String modCount) {
			this.modCount = modCount;
		}
		public String getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}
		public String getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(String updatedOn) {
			this.updatedOn = updatedOn;
		}
		public String getContractAvailability() {
			return contractAvailability;
		}
		public void setContractAvailability(String contractAvailability) {
			this.contractAvailability = contractAvailability;
		}
		public String getCustomerDisplayName() {
			return customerDisplayName;
		}
		public void setCustomerDisplayName(String customerDisplayName) {
			this.customerDisplayName = customerDisplayName;
		}
		public String getCustomerName() {
			return customerName;
		}
		public void setCustomerName(String customerName) {
			this.customerName = customerName;
		}
		public String getNumber() {
			return number;
		}
		public void setNumber(String number) {
			this.number = number;
		}
		public String getService() {
			return service;
		}
		public void setService(String service) {
			this.service = service;
		}
		public String getServiceTier() {
			return serviceTier;
		}
		public void setServiceTier(String serviceTier) {
			this.serviceTier = serviceTier;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
     
    }

}
