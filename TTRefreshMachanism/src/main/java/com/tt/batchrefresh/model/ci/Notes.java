package com.tt.batchrefresh.model.ci;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "incdnotes")
public class Notes {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "portalusername")
    private String name;

    @Column(name = "custinfonotes")
    private String note;

    @Column(name = "modifiedtime")
    private Date time;

    @ManyToOne
    @JoinColumn(name = "incidentid")
    private Incident incident;

    public int getId() {
	return id;
    }

    public Incident getIncident() {
	return incident;
    }

    public void setIncident(Incident incident) {
	this.incident = incident;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getNote() {
	return note;
    }

    public void setNote(String note) {
    	
    	if(note!=null && note.length()>2000){
			this.note = note.substring(0, 1999);
		}else {
			this.note = note;
		}
    }

    public Date getTime() {
	return time;
    }

    public void setTime(Date time) {
	this.time = time;
    }

}
