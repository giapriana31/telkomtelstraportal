/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh.populator;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;





import com.tt.batchrefresh.ApplicationProperties;
import com.tt.batchrefresh.FetchRefreshData;
import com.tt.batchrefresh.model.cirelation.Xml;
import com.tt.common.RefreshUtil;
import com.tt.batchrefresh.model.cirelation.CIRelationship;
import com.tt.batchrefresh.model.customer.Customer;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

/**
 * @author Infosys
 *
 */
public class CIRelationPopulator implements ModelPopulator
{

	private static final Logger logger = Logger.getLogger(CIRelationPopulator.class);
	
	public List<CIRelationship> populateModel(String url, String... params) throws JAXBException, IOException
	{
		List<CIRelationship> ciList = new ArrayList<>();
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(new FetchRefreshData().fetchRefreshDataStream(url, params));
		for(Xml.UCiRelations uc : xml.getUCiRelations())
		{
			ciList.add(createCIRelationship(uc));
		}
		
		if(ciList!=null && ciList.size()>0)
			logger.warn("No. of CIRelationship records updated after Batch Refresh--->>"+ciList.size());
		return ciList;
	}
	

	/* (non-Javadoc)
	 * @see com.tt.batchrefresh.populator.ModelPopulator#populateModel()
	 */
	@Override
	public List<CIRelationship> populateModel() throws JAXBException
	{
		Properties prop = ApplicationProperties.getInstance();
		List<CIRelationship> ciList = new ArrayList<>();

		File f = new File("/data1/IJV/cirelns.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(f);
		for(Xml.UCiRelations uc : xml.getUCiRelations())
		{
			ciList.add(createCIRelationship(uc));
		}
		return ciList;
	}

	private CIRelationship createCIRelationship(Xml.UCiRelations uc)
	{
		try
		{
			CIRelationship ciRel = new CIRelationship();
			ciRel.setBaseCIId(uc.getParentUNumber());
			ciRel.setBaseCIName(uc.getParentName());
			ciRel.setBaseCIClass(uc.getParentSysClassName());
			ciRel.setDependentCIId(uc.getChildUNumber());
			ciRel.setDependentCIName(uc.getChildName());
			ciRel.setDependentCIClass(uc.getChildSysClassName());
			ciRel.setRelationShip(uc.getReltypeSysName());
			ciRel.setCreatedDate(RefreshUtil.convertStringToDate(uc.getRelSysCreatedOn()));
			ciRel.setUpdatedDate(RefreshUtil.convertStringToDate(uc.getRelSysUpdatedOn()));
			ciRel.setCreatedBy(uc.getRelSysCreatedBy());
			ciRel.setUpdatedBy(uc.getRelSysUpdatedBy());
			
			return ciRel;
		}
		catch (ParseException ex)
		{
			logger.error("unable to parse data for ci relation "+uc.getParentUNumber()+" - "+uc.getChildUNumber());
			logger.error(ex);
		}
		return null;
	}

}
