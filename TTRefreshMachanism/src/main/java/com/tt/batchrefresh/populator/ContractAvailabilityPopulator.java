package com.tt.batchrefresh.populator;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.tt.batchrefresh.FetchRefreshData;
import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.batchrefresh.model.contarctAvailability.ContractAvailability;
import com.tt.batchrefresh.model.contarctAvailability.Xml;
import com.tt.common.RefreshUtil;

public class ContractAvailabilityPopulator {

	public List<ContractAvailability> populateModel(String url, String... params) throws JAXBException, IOException, ParseException
	{
		List<ContractAvailability> ContractAvailabilityList = new ArrayList<ContractAvailability>();
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(new FetchRefreshData().fetchRefreshDataStream(url, params));
		for (Xml.uContractAvailability availability : xml.getUContractAvailability())
		{
			if (availability!=null && createContractAvailability(availability) != null)
			{
				ContractAvailabilityList.add(createContractAvailability(availability));
				System.out.println("ContractAvailabilityList populated ---- "+ContractAvailabilityList.size());
				System.out.println("ContractAvailabilityList populated ---- "+ContractAvailabilityList.get(0).getContractedAvailability());
				
			}
		}
		return ContractAvailabilityList;
	}


	private ContractAvailability createContractAvailability(Xml.uContractAvailability availability) throws ParseException
	{

		ContractAvailability contractAvailability = new ContractAvailability();
		
		try
		{
			
			if(availability.getContractAvailability()!=null && availability.getContractAvailability().length()>0)
				contractAvailability.setContractedAvailability(Float.parseFloat(availability.getContractAvailability()));
			else
				contractAvailability.setContractedAvailability(0);
			
			contractAvailability.setCreatedBy(availability.getCreatedBy());
			contractAvailability.setCreatedOn(RefreshUtil.convertStringToDate(availability.getCreatedOn()));
			contractAvailability.setCustomerDisplayName(availability.getCustomerDisplayName());
			contractAvailability.setCustomerid("");
			contractAvailability.setProductType(availability.getService());
			contractAvailability.setServiceTier(availability.getServiceTier());
			contractAvailability.setStatus(availability.getStatus());
			contractAvailability.setUpdatedBy(availability.getUpdatedBy());
			contractAvailability.setUpdatedOn(RefreshUtil.convertStringToDate(availability.getUpdatedOn()));
				
			
			return contractAvailability;
		}
		catch (NumberFormatException ex)
		{
			System.out.println("Exception due to Availability is -----------"+ex.getMessage());
			
		}
		
		return null;
	}


}
