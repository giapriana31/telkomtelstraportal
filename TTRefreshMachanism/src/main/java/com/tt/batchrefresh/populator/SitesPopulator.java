/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tt.batchrefresh.populator;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.tt.batchrefresh.ApplicationProperties;
import com.tt.batchrefresh.FetchRefreshData;
import com.tt.batchrefresh.model.sites.Site;
import com.tt.batchrefresh.model.sites.Xml;
import com.tt.common.RefreshUtil;

/**
 *
 * @author Arijit_Dasgupta
 */
public class SitesPopulator implements ModelPopulator
{
	private static final Logger logger = Logger.getLogger(SitesPopulator.class);

	public List<Site> populateModel(String url, String... params) throws JAXBException, IOException
	{
		List<Site> siteList = new ArrayList<>();
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(new FetchRefreshData().fetchRefreshDataStream(url, params));
		for (Xml.CmnLocation cmn : xml.getCmnLocation())
		{
			siteList.add(createSite(cmn));
		}
		
		if(siteList!=null && siteList.size()>0)
			logger.warn("No. of Site records updated after Batch Refresh--->>"+siteList.size());
		return siteList;
	}

	@Override
	public List<Site> populateModel() throws JAXBException
	{
		List<Site> siteList = new ArrayList<>();
		Properties prop = ApplicationProperties.getInstance();
		File f = new File("/data1/IJV/site.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(f);
		for (Xml.CmnLocation cmn : xml.getCmnLocation())
		{
			siteList.add(createSite(cmn));
		}
		return siteList;
	}

	private Site createSite(Xml.CmnLocation cmn)
	{
		try
		{
			Site s = new Site();
			s.setCity(cmn.getCity());
			s.setCountry(cmn.getCountry());
			s.setCreatedby(cmn.getSysCreatedBy());
			s.setCreateddate(RefreshUtil.convertStringToDate(cmn.getSysCreatedOn()));
			s.setCustomersite(cmn.getUCustomerSite());
			s.setCustomeruniqueid(cmn.getUCustomerUniqueId());
			s.setLatitude(Double.valueOf(cmn.getLatitude()));
			s.setLongitude(Double.valueOf(cmn.getLongitude()));
			s.setServicetier(cmn.getUServiceTier());
			s.setSiteid(cmn.getUSiteId());
			s.setSiteoperationalhours(cmn.getUSiteOperationalHours());
			s.setStateprovince(cmn.getState());
			s.setSysZone(cmn.getUZone());
			s.setTimezone(cmn.getTimeZone());
			s.setUpdatedby(cmn.getSysUpdatedBy());
			s.setUpdatedon(RefreshUtil.convertStringToDate(cmn.getSysUpdatedOn()));
			s.setStatus(cmn.getuSiteStatus());
			s.setIsDataCenter(cmn.getIsDataCenter());
			s.setIsManaged(cmn.getIsManaged());
			s.setDataCenterName(cmn.getDataCenterName());
			s.setDataCenterType(cmn.getDataCenterType());
			s.setRootProductId(cmn.getRootProductId());
			
			logger.warn("SiteStatus from SNOW--->> "+cmn.getuSiteStatus()+" and SiteStatus set in Portal--->> "+s.getStatus());
			return s;
		}
		catch (NumberFormatException | ParseException ex)
		{
			logger.error("Unable to process data for site " + cmn.getUSiteId());
			logger.error(ex);
		}
		return null;
	}

}
