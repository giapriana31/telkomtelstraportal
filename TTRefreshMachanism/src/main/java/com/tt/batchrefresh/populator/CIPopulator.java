/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				30-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh.populator;

import com.tt.batchrefresh.ApplicationProperties;
import com.tt.batchrefresh.FetchRefreshData;
import com.tt.batchrefresh.model.ci.Xml;
import com.tt.common.RefreshUtil;
import com.tt.utils.PropertyReader;
import com.tt.batchrefresh.model.ci.Configuration;




import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

/**
 *
 * @author Infosys
 * @version 1,0
 */
public class CIPopulator implements ModelPopulator
{
	private static final Logger logger = Logger.getLogger(CIPopulator.class);
	
	Properties prop = PropertyReader.getProperties();

	public List<Configuration> populateModel(String url, String... params) throws JAXBException, IOException
	{
		List<Configuration> ciList = new ArrayList<>();
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(new FetchRefreshData().fetchRefreshDataStream(url, params));
		for (Object obj : xml.getUCis())
		{
			ciList.add(createConfiguration((Xml.UCis) obj));
		}

		return ciList;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tt.batchrefresh.populator.ModelPopulator#populateModel()
	 */
	@Override
	public List<Configuration> populateModel() throws JAXBException
	{
		List<Configuration> ciList = new ArrayList<>();
		Properties prop = ApplicationProperties.getInstance();
		File f = new File("/data1/IJV/ci/xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(f);

		for (Object obj : xml.getUCis())
		{
			ciList.add(createConfiguration((Xml.UCis) obj));
		}

		return ciList;
	}

	private Configuration createConfiguration(Xml.UCis obj)
	{
		logger.info("CONSTRUCT CONFIGURATION START ----");
		try
		{
			Configuration config = new Configuration();
			config.setCiid(obj.getCiunumber());
			config.setCiname(obj.getCiName());
			config.setCitype(obj.getCiucitype());
			switch(obj.getCiSysClassName())
			{
				case "u_cpe":
					config.setCmdblass("CPE");
					break;
				case "u_managed_network_service":
					config.setCmdblass("MNS");
					break;
				case "u_pe_interface":
					config.setCmdblass("Interface");
					break;
				case "u_pe":
					config.setCmdblass("PE");
					break;
				case "u_saas_component":
					config.setCmdblass("SaaS Component");
					break;
				case "u_saas_business_service":
					config.setCmdblass("SaaS Business Service");
					break;
				default: 
					config.setCmdblass(obj.getCiSysClassName());
					break;
			}
			
			switch(obj.getCiucideliverystatus())
			{
				case "Order Received":
					config.setDeliverystatus(obj.getCiucideliverystatus());
					config.setDeliverystage("1");
					config.setColor("black");
					break;
			
				case "Detailed Design":
					config.setDeliverystatus(obj.getCiucideliverystatus());
					config.setDeliverystage("2");
					config.setColor("#A40800");
					break;
				
				case "Procurement":
					config.setDeliverystatus(obj.getCiucideliverystatus());
					config.setDeliverystage("3");
					config.setColor("#7030A0");
					break;
				
				case "Delivery and Activation":
					config.setDeliverystatus(obj.getCiucideliverystatus());
					config.setDeliverystage("4");
					config.setColor("#00B0F0");
					break;
				
				case "Monitoring":
					config.setDeliverystatus(obj.getCiucideliverystatus());
					config.setDeliverystage("5");
					config.setColor("#FFBC00");
					break;
				
				case "Operational":
					config.setDeliverystatus(obj.getCiucideliverystatus());
					config.setDeliverystage("6");
					config.setColor("#92D050");
					break;
				
				default:
					config.setDeliverystatus("");
					config.setDeliverystage("");
					config.setColor("");
					break;
			}
			
			
			config.setCreatedby(obj.getCiSysCreatedBy());
			if(obj.getCiSysCreatedOn()!=null)
				config.setCreateddate(RefreshUtil.convertStringToDate(obj.getCiSysCreatedOn()));

			if (obj.getCpeucustomeredgedevice() != null && obj.getCpeucustomeredgedevice().trim().length() > 0 && "YES".equalsIgnoreCase(obj.getCpeucustomeredgedevice()))
				config.setCustomeredge(true);
			else
			config.setCustomeredge(false);
			config.setCustomeruniqueid(obj.getCiucustomeruniqueid());
			config.setSiteid(obj.getSiteusiteid());
			config.setUpdatedby(obj.getCiSysUpdatedBy());
			config.setUpdateddate(RefreshUtil.convertStringToDate(obj.getCiSysUpdatedOn()));
			config.setProductId(obj.getCiuproductid());
			config.setRootProductId(obj.getCiurootproductid());
			config.setCustomerServingNetworkInterface(obj.getPeinterfaceucustomerservingnetworkinterface());
			config.setSerreqnumber(obj.getReqnumber());
			config.setCiStatus(obj.getUsritype());
			config.setSrireferencenumber(obj.getCiusrireferencenumber());
			config.setLink(obj.getCiulink());
			config.setActivatedon(obj.getCiuactivatedon());
			config.setCustomercommiteddate(obj.getCiuccdcustomercommitteddate());
			config.setRitmnumber(obj.getRitmnumber());
			int statusValue=obj.getCiOperationalStatus();
			switch(statusValue){
			case 0:
				config.setStatus("Commissioning");
				break;
			case 1:
				config.setStatus("Decommissioned");
				break;
			case 2:
				config.setStatus("Infrastructure");
				break;
			case 3:
				config.setStatus("Maintenance");
				break;
			case 4:
				config.setStatus("Operational");
				break;
			case 7:
				config.setStatus("Monitoring");
				break;
			default:
				config.setStatus("");
				break;
			}
			config.setContractEndDate(RefreshUtil.convertStringToDate(obj.getCiucontractenddate()));
			config.setContractStartDate(RefreshUtil.convertStringToDate(obj.getCiucontractstartdate()));
			if((null != obj.getCiucontractstartdate() && obj.getCiucontractstartdate().length()>0) && (null != obj.getCiucontractenddate() && obj.getCiucontractenddate().length()>0))
				config.setContractDuration(RefreshUtil.getDateDifference(obj.getCiucontractstartdate(), obj.getCiucontractenddate()));
			else
				config.setContractDuration("");
			
			if(null != obj.getCpeuproductclassifications() && obj.getCpeuproductclassifications().trim().length()>0)
			{
				config.setProductclassification(obj.getCpeuproductclassifications());
			
			}else
				config.setProductclassification(prop.getProperty("prodClassAtPortal_Blank"));
			
			config.setProjectreferencenumber(obj.getCiureferenceprojectid());
			config.setSecuritytiers(obj.getMsscmpusecuritytiers());

			return config;
		}
		catch (ParseException ex)
		{
			logger.error("Unable to process data for customer " + obj.getCiucustomeruniqueid());
			logger.error(ex);
		}
		return null;
	}

}
