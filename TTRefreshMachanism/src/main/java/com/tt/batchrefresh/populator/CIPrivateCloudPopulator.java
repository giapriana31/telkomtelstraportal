package com.tt.batchrefresh.populator;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.tt.batchrefresh.ApplicationProperties;
import com.tt.batchrefresh.FetchRefreshData;
import com.tt.batchrefresh.model.ci.CloudBusiness;
import com.tt.batchrefresh.model.ci.Configuration;
import com.tt.batchrefresh.model.ci.VBlock;
import com.tt.batchrefresh.model.ci.VCenter;
import com.tt.batchrefresh.model.ci.VirtualMachine;
import com.tt.batchrefresh.model.ci.XmlPrivateCloud;
import com.tt.common.RefreshUtil;
import com.tt.utils.PropertyReader;
/*import org.apache.log4j.Logger;*/

public class CIPrivateCloudPopulator  implements ModelPopulator{
	
	private static Properties prop = PropertyReader.getProperties();

	/*private static final Logger logger = Logger.getLogger(CIPrivateCloudPopulator.class);*/

	public List<Configuration> populateModel(String url, String... params) throws JAXBException, IOException
	{
		List<Configuration> ciList = new ArrayList<>();
		JAXBContext jaxbContext = JAXBContext.newInstance(XmlPrivateCloud.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		XmlPrivateCloud xml = (XmlPrivateCloud) jaxbUnmarshaller.unmarshal(new FetchRefreshData().fetchRefreshDataStream(url, params));
		for (Object obj : xml.getUCis())
		{
			ciList.add(createConfiguration((XmlPrivateCloud.UCis) obj));
		}

		return ciList;
	}
	
	@Override
	public List<Configuration> populateModel() throws JAXBException
	{
		List<Configuration> ciList = new ArrayList<>();
		Properties prop = ApplicationProperties.getInstance();
		File f = new File("/data1/IJV/ci/xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(XmlPrivateCloud.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		XmlPrivateCloud xml = (XmlPrivateCloud) jaxbUnmarshaller.unmarshal(f);

		for (Object obj : xml.getUCis())
		{
			ciList.add(createConfiguration((XmlPrivateCloud.UCis) obj));
		}

		return ciList;
	}

	private Configuration createConfiguration(XmlPrivateCloud.UCis obj)
	{
		try
		{
			if(getValidVMCI(obj)){
				Configuration config = new Configuration();
				System.out.println("obj.getCiunumber()------------>>>"+obj.getCiunumber());
				config.setCiid(obj.getCiunumber());
				config.setCiname(obj.getCiName());
				config.setCitype(obj.getCiucitype());
				config.setCiStatus(obj.getUsritype());
				config.setProductclassification(obj.getCpeuproductclassification());
				switch(obj.getCiSysClassName())
				{
					case "u_cpe":
						config.setCmdblass("CPE");
						break;
					case "u_managed_network_service":
						config.setCmdblass("MNS");
						break;
					case "u_pe_interface":
						config.setCmdblass("Interface");
						break;
					case "u_pe":
						config.setCmdblass("PE");
						break;
					case "cmdb_ci_vcenter":
						config.setCmdblass("vCENTER");
						break;
					case "u_cmdb_ci_hardware_vblock":
						config.setCmdblass("vBLOCK");
						break;
					case "cmdb_ci_vmware_instance":
						config.setCmdblass("VM Instance");
						break;
					case "u_cloud_business_service":
						config.setCmdblass("Private Cloud");
						break;
					default: 
						config.setCmdblass(obj.getCiSysClassName());
						break;
				}

				switch(obj.getCiucideliverystatus())
				{
					case "Order Received":
						config.setDeliverystatus(obj.getCiucideliverystatus());
						config.setDeliverystage("1");
						config.setColor("black");
						break;
				
					case "Detailed Design":
						config.setDeliverystatus(obj.getCiucideliverystatus());
						config.setDeliverystage("2");
						config.setColor("#A40800");
						break;
					
					case "Procurement":
						config.setDeliverystatus(obj.getCiucideliverystatus());
						config.setDeliverystage("3");
						config.setColor("#7030A0");
						break;
					
					case "Delivery and Activation":
						config.setDeliverystatus(obj.getCiucideliverystatus());
						config.setDeliverystage("4");
						config.setColor("#00B0F0");
						break;
					
					case "Monitoring":
						config.setDeliverystatus(obj.getCiucideliverystatus());
						config.setDeliverystage("5");
						config.setColor("#FFBC00");
						break;
					
					case "Operational":
						config.setDeliverystatus(obj.getCiucideliverystatus());
						config.setDeliverystage("6");
						config.setColor("#92D050");
						break;
					
					default:
						config.setDeliverystatus("");
						config.setDeliverystage("");
						config.setColor("");
						break;
				}
				
				
				config.setCreatedby(obj.getCiSysCreatedBy());
				config.setCreateddate(RefreshUtil.convertStringToDate(obj.getCiSysCreatedOn()));

				if (obj.getCpeucustomeredgedevice() != null && obj.getCpeucustomeredgedevice().trim().length() > 0 && "YES".equalsIgnoreCase(obj.getCpeucustomeredgedevice()))
					config.setCustomeredge(true);
				else
				config.setCustomeredge(false);
				
				config.setCustomeruniqueid(obj.getCiucustomeruniqueid());
				config.setSiteid(obj.getSiteusiteid());
				config.setUpdatedby(obj.getCiSysUpdatedBy());
				config.setUpdateddate(RefreshUtil.convertStringToDate(obj.getCiSysUpdatedOn()));
				config.setProductId(obj.getCiuproductid());
				
				if("PRIVATE_CLOUD_IAAS".equalsIgnoreCase(obj.getCiurootproductid())){
					config.setRootProductId("Private Cloud");
				}else
					config.setRootProductId(obj.getCiurootproductid());
				
				config.setCustomerServingNetworkInterface(obj.getPeinterfaceucustomerservingnetworkinterface());
				config.setSerreqnumber(obj.getReqnumber());
				config.setRitmnumber(obj.getRitmnumber());
				config.setSrireferencenumber(obj.getCiusrireferencenumber());
				config.setLink(obj.getCiulink());
				config.setActivatedon(obj.getCiuactivatedon());
				config.setCustomercommiteddate(obj.getCiuccdcustomercommitteddate());
				config.setContractEndDate(RefreshUtil.convertStringToDate(obj.getCiucontractenddate()));
				config.setContractStartDate(RefreshUtil.convertStringToDate(obj.getCiucontractstartdate()));
				if((null != obj.getCiucontractstartdate() && obj.getCiucontractstartdate().length()>0) && (null != obj.getCiucontractenddate() && obj.getCiucontractenddate().length()>0))
					config.setContractDuration(RefreshUtil.getDateDifference(obj.getCiucontractstartdate(), obj.getCiucontractenddate()));
				else
					config.setContractDuration("");
				
				
				
				if("PRIVATE_CLOUD_IAAS_VMINSTANCE".equalsIgnoreCase(obj.getCiuproductid())){
					VirtualMachine virtualMachine = new VirtualMachine();
					virtualMachine.setCiid(config);
					virtualMachine.setDisks(obj.getVmidisks());
					virtualMachine.setDiskSize(obj.getVmidiskssize());
					virtualMachine.setLeaseEnd(obj.getVmileaseend());
					virtualMachine.setLeaseStart(obj.getVmileasestart());
					virtualMachine.setMemory(obj.getVmimemory());
					virtualMachine.setState(obj.getVmistate());
					virtualMachine.setCorrelationId(obj.getVmicorrelationid());
					virtualMachine.setCpus(obj.getVmicpus());
					virtualMachine.setDescription(obj.getVmishortdescription());
					virtualMachine.setImagePath(obj.getVmiimagepath());
					virtualMachine.setLink(obj.getVmiulink());
					virtualMachine.setModelId(obj.getVmimodelid());
					//virtualMachine.setNetworkAdapters(obj.getvmi);
					virtualMachine.setTemplate(obj.getVmitemplate());
					virtualMachine.setVmAlias(obj.getVmiuvmalias());
					
					
					config.setVirtualMachine(virtualMachine);
				}
				
				if("PRIVATE_CLOUD_IAAS_VCENTER".equalsIgnoreCase(obj.getCiuproductid())){
					VCenter vCenter = new VCenter();
					vCenter.setCiid(config);
					vCenter.setvCenterURL(obj.getVcurl());
					vCenter.setApiVersion(obj.getVcapiversion());
					vCenter.setDescription(obj.getVcshortdescription());
					vCenter.setFullName(obj.getVcfullname());
					config.setvCenter(vCenter);
				}
				
				if("PRIVATE_CLOUD_IAAS_VBLOCK".equalsIgnoreCase(obj.getCiuproductid())){
					VBlock vBlock = new VBlock();
					
					vBlock.setBoqURL(obj.getVbuboqurl());
					vBlock.setCiscoContractNumber(obj.getVbuciscocontractnumber());
					vBlock.setMaxMemory(obj.getVbumaxmemorygb());
					vBlock.setMaxNumberOfCPUs(obj.getVbumaxofcpus());
					vBlock.setMaxSizeOfStorage(obj.getVbumaxsizeofstoragegb());
					vBlock.setNumberOfRacks(obj.getVbunumberofracks());
					vBlock.setVceVisionURL(obj.getVbuvcevisionurl());
					vBlock.setVmwareContractNumber(obj.getVbuvmwarecontractnumber());
					vBlock.setTypeModel(obj.getVbutypemodel());
					
					vBlock.setBuilding(obj.getVbubuilding());
					vBlock.setDeliveryModel(obj.getVbudeliverymodel());
					vBlock.setDeviceOwnerShip(obj.getVbudeviceownership());
					vBlock.setEmcSerialNumber(obj.getVbuemcserialnumber());
					vBlock.setEmcSiteId(obj.getVbuemcsiteid());
					vBlock.setFinancialModel(obj.getVbufinancialmodel());
					vBlock.setFloor(obj.getVbufloor());
					vBlock.setLocalSiteBuilding(obj.getVbulocalsitebuidling());
					vBlock.setLocalSiteFloor(obj.getVbulocalsitefloor());
					vBlock.setMaintainanceOption(obj.getVbumaintenanceoption());
					vBlock.setRoom(obj.getVburoom());
					vBlock.setSerialNumber(obj.getVbserialnumber());
					vBlock.setStorageModel(obj.getVbusotragemodel());
					vBlock.setvBlockRCM(obj.getVbuvblockrcmreleaseconfigurationmatrix());
					vBlock.setvBlockSystemType(obj.getVbuvblocksystemtype());
					vBlock.setvBlockType(obj.getVbuvblocktype());
					vBlock.setVceContractNumber(obj.getVbuvcecontractnumber());
					vBlock.setNumberOfVcpu(obj.getVbunumberofvcpusforvblock());
					
					vBlock.setCiid(config);
					config.setvBlock(vBlock);
				}
				if("PRIVATE_CLOUD_IAAS_SERVICE".equalsIgnoreCase(obj.getCiuproductid())){
					CloudBusiness cloudBusiness = new CloudBusiness();
					cloudBusiness.setCiid(config);
					cloudBusiness.setDeploymentType(obj.getCbsudeploymenttype());
					cloudBusiness.setVendor(obj.getCbsudevicevendor());
					config.setCloudBusiness(cloudBusiness);
				}
				
				
				
				
				int statusValue=obj.getCiOperationalStatus();
				switch(statusValue){
				case 0:
					config.setStatus("Commissioning");
					break;
				case 1:
					config.setStatus("Decommissioned");
					break;
				case 2:
					config.setStatus("Infrastructure");
					break;
				case 3:
					config.setStatus("Maintenance");
					break;
				case 4:
					config.setStatus("Operational");
					break;
				case 7:
					config.setStatus("Monitoring");
					break;
				default:
					config.setStatus("");
					break;
				}
				
				config.setProductclassification(prop.getProperty("prodClassAtPortal_NA"));
					
				return config;
	
			}
			
		}
		catch (ParseException ex)
		{
			System.out.println("Unable to process data for customer " + obj.getCiucustomeruniqueid());
			System.out.println(ex);
		}
		return null;
	}
	
	private Boolean getValidVMCI(XmlPrivateCloud.UCis obj){
		
		if(obj.getCiSysClassName() != null && !"cmdb_ci_vmware_instance".equalsIgnoreCase(obj.getCiSysClassName())){
			return true;
		}else if((obj.getCiSysClassName() != null && "cmdb_ci_vmware_instance".equalsIgnoreCase(obj.getCiSysClassName())) && (null != obj.getVmistate() && (
				"Scheduled".equalsIgnoreCase(obj.getVmistate()) || "Retired".equalsIgnoreCase(obj.getVmistate()) || "Terminated".equalsIgnoreCase(obj.getVmistate()) || "Cancelled".equalsIgnoreCase(obj.getVmistate())))){
			return false;
		}else
			return true;
	}


}
