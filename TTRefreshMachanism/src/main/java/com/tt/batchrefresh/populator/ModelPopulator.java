/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh.populator;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

/**
 * @author Infosys
 *
 */
public interface ModelPopulator
{
	public List<?> populateModel() throws IOException, JAXBException;
}
