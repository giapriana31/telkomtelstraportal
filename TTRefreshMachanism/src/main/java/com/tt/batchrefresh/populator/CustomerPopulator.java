/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh.populator;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;




import com.tt.batchrefresh.ApplicationProperties;
import com.tt.batchrefresh.FetchRefreshData;
import com.tt.batchrefresh.model.customer.Xml;
import com.tt.batchrefresh.model.customer.Customer;
import com.tt.util.RefreshUtil;

/**
 * @author Infosys
 *
 */
public class CustomerPopulator implements ModelPopulator
{
	public List<Customer> populateModel(String url, String... params) throws JAXBException, IOException
	{
		List<Customer> custList = new ArrayList<>();
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(new FetchRefreshData().fetchRefreshDataStream(url, params));
		for (Xml.CoreCompany company : xml.getCoreCompany())
		{
			if (createCustomer(company) != null)
			{
				custList.add(createCustomer(company));
			}
		}
		return custList;
	}

	@Override
	public List<Customer> populateModel() throws JAXBException
	{
		List<Customer> custList = new ArrayList<>();
		Properties prop = ApplicationProperties.getInstance();
		File f = new File("D:/data1/IJV/customers.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Xml.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

		Xml xml = (Xml) jaxbUnmarshaller.unmarshal(f);
		for (Xml.CoreCompany company : xml.getCoreCompany())
		{
			if (createCustomer(company) != null)
			{
				custList.add(createCustomer(company));
			}
		}
		return custList;
	}

	private Customer createCustomer(Xml.CoreCompany company)
	{

		try
		{
			Customer cust = new Customer();
			cust.setAccountName(company.getName());
			if (company.getUBronzeContractedAvailability() != null && company.getUBronzeContractedAvailability().trim().length() > 0)
				cust.setBronzeAvailabilitySLA(new BigDecimal(company.getUBronzeContractedAvailability()));
			else
				cust.setBronzeAvailabilitySLA(new BigDecimal(0.0));
			
			if (company.getuManagedContractedAvailability() != null && company.getuManagedContractedAvailability().trim().length() > 0)
				cust.setManagedAvailability(new BigDecimal(company.getuManagedContractedAvailability()));
			else
				cust.setManagedAvailability(new BigDecimal(0.0));
			cust.setCountry(company.getCountry());
			cust.setCreatedBy(company.getSysCreatedBy());
			cust.setCreatedDate(RefreshUtil.convertStringToDate(company.getSysCreatedOn()));
			cust.setCustomeruniqueid(company.getUCustomerUniqueId());
			cust.setDescription(company.getUDescription());
			cust.setFiscalYear(company.getFiscalYear());
			if (company.getUGoldContractedAvailability() != null && company.getUGoldContractedAvailability().trim().length() > 0)
				cust.setGoldAvailabilitySLA(new BigDecimal(company.getUGoldContractedAvailability()));
			else
				cust.setGoldAvailabilitySLA(new BigDecimal(0.0));
			
			if (company.getUSilverContractedAvailability() != null && company.getUSilverContractedAvailability().trim().length() > 0)
				cust.setSilverAvailabilitySLA(new BigDecimal(company.getUSilverContractedAvailability()));
			else
				cust.setSilverAvailabilitySLA(new BigDecimal(0.0));
			cust.setState(company.getState());
			cust.setUpdatedBy(company.getSysUpdatedBy());
			cust.setUpdatedDate(RefreshUtil.convertStringToDate(company.getSysUpdatedOn()));
			cust.setCustomerType(company.getUType());
			cust.setStatus(company.getUCustomerStatus());
			cust.setActive(company.getUActive());
			
			if(company.getUNumberOfOktaUsers()!=null && company.getUNumberOfOktaUsers().length()>0)
				cust.setNumberOfOktaUsers(new Integer(company.getUNumberOfOktaUsers()));
			return cust;
		}
		catch (NumberFormatException | ParseException ex)
		{
			System.out.println(ex.getMessage());
			
		}
		return null;
	}

}
