/*
\ * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.batchrefresh.model.sites.Site;
import com.tt.batchrefresh.populator.SitesPopulator;
import com.tt.utils.PropertyReader;

/**
 *
 * @author Arijit_Dasgupta
 */
public class SitesBatchRefresh implements BatchRefresh
{
	private final static String PARAMS = "sysparm_query=sys_updated_on>=javascript:gs.dateGenerate(CREATED_DATE)";
	private static Properties prop = PropertyReader.getProperties();
	
	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws JAXBException, ParseException, IOException
	{
		//Properties prop = ApplicationProperties.getInstance();
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		String params = PARAMS.replaceAll("CREATED_DATE", date);
		System.out.println(params);

		
		String url = prop.getProperty("SITES_URL");
		List<Site> siteList = new SitesPopulator().populateModel(url, params);
	
		if(null != fromDate && "2015-01-01 00:00:00".equalsIgnoreCase(fromDate)){
			new BatchRefreshDAO().truncateSitesRecords();
		}
		new BatchRefreshDAO().refreshData(siteList);
		
		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		new BatchRefreshDAO().updateLastRefresh("Site", toDateFormatted, interval);
	}
	
	public static void main(String[] args)
	{
		//String date = new BatchRefreshDAO().retrieveLastRefreshDate("Site");
		String date = "2015-01-01 00:00:00";
		System.out.println(date);
		SitesBatchRefresh cb = new SitesBatchRefresh();
		try
		{
			cb.refreshData(date, null, "", "");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
