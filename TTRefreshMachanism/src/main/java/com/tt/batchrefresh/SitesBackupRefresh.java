/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				13-NOV-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;
import com.tt.batchrefresh.dao.BatchRefreshDAO;

/**
 * @author Infosys
 *
 */
public class SitesBackupRefresh
{
	public static void main(String[] args) {
		SitesBackupRefresh sitesBackupRefresh = new SitesBackupRefresh();
		sitesBackupRefresh.refresh();
	}
	public void refresh(){
		BatchRefreshDAO brdao = new BatchRefreshDAO();
		brdao.updatePVSiteDeliveryStatus();
		brdao.updateVBlockStatus();
	}
}