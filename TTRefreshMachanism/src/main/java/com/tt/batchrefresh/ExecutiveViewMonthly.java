package com.tt.batchrefresh;

import java.io.IOException;
import java.text.ParseException;

import javax.xml.bind.JAXBException;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tt.dao.HibernateUtil;

public class ExecutiveViewMonthly implements BatchRefresh 
{
	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType)
			throws ParseException, IOException, JAXBException 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		/* procedure for executive view incident data  */
		SQLQuery sqlQuery = session.createSQLQuery("CALL populate_ev_incmetadata(15)");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();
		txn.commit();
		
		/* procedure for executive view, delivery data  */
		SQLQuery sqlQuery1 = session.createSQLQuery("CALL populateDeliveryMetadata(1)");
		Transaction txn1 = session.beginTransaction();
		sqlQuery1.executeUpdate();
		txn1.commit();
		
		/* procedure for executive view, delivery - cloud data  */
		SQLQuery sqlQuery2 = session.createSQLQuery("CALL populateDeliveryCloudMetadata(1)");
		Transaction txn2 = session.beginTransaction();
		sqlQuery2.executeUpdate();
		txn2.commit();
		
		/* procedure for executive view, delivery - data  */
		SQLQuery sqlQuery3 = session.createSQLQuery("CALL deliverySpeedometerNew()");
		Transaction txn3 = session.beginTransaction();
		sqlQuery3.executeUpdate();
		txn3.commit();
		
		/* procedure for executive view, utilization - cloud data  */
		SQLQuery sqlQuery4 = session.createSQLQuery("CALL populate_utilization_pc_monthly()");
		Transaction txn4 = session.beginTransaction();
		sqlQuery4.executeUpdate();
		txn4.commit();
		
		/* procedure for executive view, delivery - mns data  */
		SQLQuery sqlQuery5 = session.createSQLQuery("CALL populate_utilization_mns()");
		Transaction txn5 = session.beginTransaction();
		sqlQuery5.executeUpdate();
		txn5.commit();
		
		/* procedure for executive view, delivery - SaaS data  */
		SQLQuery sqlQuery6 = session.createSQLQuery("CALL deliveryMetadataSaaS(1)");
		Transaction txn6 = session.beginTransaction();
		sqlQuery6.executeUpdate();
		txn5.commit();
		
		
		session.close();
	}
}
