/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				12-NOV-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;
import com.tt.batchrefresh.dao.BatchRefreshDAO;

/**
 * @author Infosys
 *
 */
public class DeleteSiteRefresh
{
	public static void main(String[] args) {
		DeleteSiteRefresh deleteSiteRefresh = new DeleteSiteRefresh();
		deleteSiteRefresh.refresh();
	}
	public void refresh(){
		BatchRefreshDAO brdao = new BatchRefreshDAO();
		brdao.deleteSitesRefresh();
	}
}