/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.batchrefresh.model.cirelation.CIRelationship;
import com.tt.batchrefresh.populator.CIRelationPopulator;
import com.tt.utils.PropertyReader;

/**
 * @author Infosys
 *
 */
public class CIRelationsBatchRefresh implements BatchRefresh
{
	private static final String PARAMS = "sysparm_query=parent_sys_class_name=u_managed_network_service^ORparent_sys_class_name=u_cpe^ORparent_sys_class_name=u_pe^ORparent_sys_class_name=u_pe_interface^rel_sys_created_on>=javascript:gs.dateGenerate(CREATED_ON)^ORrel_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
	private static Properties prop = PropertyReader.getProperties();
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.tt.batchrefresh.BatchRefresh#refreshData(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws IOException, JAXBException
	{
		
		String entityParam = getCIRefreshEntityParam(entityType);
		
		
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		String params = entityParam.replaceAll("CREATED_ON", date);
		date = "'" + toDate.substring(0, 10) + "','" + toDate.substring(11) + "'";
		params = params.replaceAll("UPDATED_ON", date);

		String url = prop.getProperty("CI_RELNS_URL");
		List<CIRelationship> ciList = new CIRelationPopulator().populateModel(url, params);
		

		if(null != fromDate && "2015-01-01 00:00:00".equalsIgnoreCase(fromDate) && entityType.equalsIgnoreCase("CIRelationship")){
			new BatchRefreshDAO().truncateCIRelationshipRecords();
		}
		
		new BatchRefreshDAO().deleteExistingCIRelationship(ciList);
		new BatchRefreshDAO().refreshData(ciList);
		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		new BatchRefreshDAO().updateLastRefresh("CIRelationship", toDateFormatted, interval);
	}
	
	private String getCIRefreshEntityParam(String entityType) {
		String entityParam = "sysparm_query=parent_sys_class_name=u_managed_network_service^ORparent_sys_class_name=u_cpe^ORparent_sys_class_name=u_pe^ORparent_sys_class_name=u_pe_interface^rel_sys_created_on>=javascript:gs.dateGenerate(CREATED_ON)^ORrel_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
		switch (entityType){
			case "CIRelationship":
				entityParam = "sysparm_query=parent_sys_class_name=u_managed_network_service^ORparent_sys_class_name=u_cpe^ORparent_sys_class_name=u_pe^ORparent_sys_class_name=u_pe_interface^rel_sys_created_on>=javascript:gs.dateGenerate(CREATED_ON)^ORrel_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
				break;
			case "CIRelationshipSaaS":
				entityParam = "sysparm_query=parent_sys_class_name=u_saas_business_service^rel_sys_created_on>=javascript:gs.dateGenerate(CREATED_ON)^ORrel_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
				break;
			default:
				System.out.println("UNKOWN entity Mode");
		}
		return entityParam;
	}

	
	public static void main(String[] args)
	{
		//String date = new BatchRefreshDAO().retrieveLastRefreshDate("CIRelationship");
		String date = "2015-01-01 00:00:00";

		System.out.println(date);
		try
		{
			CIRelationsBatchRefresh c = new CIRelationsBatchRefresh();
			c.refreshData(date, date, "", "");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
		}
	}
}
