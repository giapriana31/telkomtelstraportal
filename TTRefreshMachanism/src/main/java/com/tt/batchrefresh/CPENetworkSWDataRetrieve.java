package com.tt.batchrefresh;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Properties;

import javax.xml.bind.JAXBException;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

public class CPENetworkSWDataRetrieve implements BatchRefresh {
	private final static Logger log = Logger
			.getLogger(CPENetworkSWDataRetrieve.class);

	public static void main(String[] args) {
		try {
			new CPENetworkSWDataRetrieve().refreshData("", "", "", "");
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType)
			throws ParseException, IOException, JAXBException {
		 Properties properties = PropertyReader.getProperties(); 

		
		 final String dburlMYSQL = properties.getProperty("dburlMYSQL"); 
		 final String usernameMySQL = properties.getProperty("usernameMySQL"); 
		 final String passwordMySQL = properties.getProperty("passwordMySQL"); 
		 final String swDatabaseURL = properties.getProperty("swDatabaseURL");
		 
		 /* following values are according to sigma dev*/
		/*final String dburlMYSQL = "jdbc:mysql://172.31.2.3/ttportalappdatabase?zeroDateTimeBehavior=convertToNull";
		final String usernameMySQL = "root";
		final String passwordMySQL = "ttr00tprd_1";
		final String swDatabaseURL = "jdbc:sqlserver://172.31.2.34:1433;user=portaluser;password=portal@123;database=IncidentsDB";*/

		try {
			Connection mySQLConnection = DriverManager.getConnection(
					dburlMYSQL, usernameMySQL, passwordMySQL);
			Statement mySQLStatement = mySQLConnection.createStatement();

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection msSQLConnection = java.sql.DriverManager
					.getConnection(swDatabaseURL);
			Statement msSQLStatement = msSQLConnection.createStatement();

			log.debug("MS sql connection established :: " + msSQLConnection);

			ResultSet resultSetForCPE;
			ResultSet resultSetForNetwork;

			String swQueryForCPE = "SELECT *  FROM [IncidentsDB].[dbo].[CPULoadPercentileMonthly] where datetime = DateName(month, DATEADD(mm, DATEDIFF(mm, 0, getDate()) -1, 0)) + '-' + DateName(Year, getDate())";
			String swQueryForNetwork = "SELECT *  FROM [IncidentsDB].[dbo].[IntefacePercentileUtilizationMonthly] where datetime = (DateName(month, DATEADD(mm, DATEDIFF(mm, 0, getDate()) -1, 0)) + '-' + DateName(Year, getDate()))";

			/* Inserting data for cpe */
			resultSetForCPE = msSQLStatement.executeQuery(swQueryForCPE);

			while (resultSetForCPE.next()) {
				String ciName = resultSetForCPE.getString(1);
				String siteName = resultSetForCPE.getString(2);
				double percentileCPULoad = resultSetForCPE.getDouble(3);
				double peakCPULoad = resultSetForCPE.getDouble(4);
				String datetime = resultSetForCPE.getString(5);

				String cpeInsertQuery = "Insert into cpeutilization values("
						+ "'" + ciName + "','" + siteName + "',"
						+ percentileCPULoad + "," + peakCPULoad + ",'"
						+ datetime + "')";
				
				log.debug("Query to insert cpe data :: " + cpeInsertQuery);

				mySQLStatement.executeUpdate(cpeInsertQuery);
			}

			log.debug("Insertion for CPE Utilization done");

			/* Inserting data for Network */
			resultSetForNetwork = msSQLStatement
					.executeQuery(swQueryForNetwork);

			while (resultSetForNetwork.next()) {
				String interfaceName = resultSetForNetwork.getString(1);
				String ciName = resultSetForNetwork.getString(2);
				String siteName = resultSetForNetwork.getString(3);
				double percentile = resultSetForNetwork.getDouble(4);
				double peak = resultSetForNetwork.getDouble(5);
				String dateTime = resultSetForNetwork.getString(6);

				String networkInsertQuery = "Insert into networkutilization values("
						+ "'"
						+ interfaceName
						+ "','"
						+ ciName
						+ "','"
						+ siteName
						+ "',"
						+ percentile
						+ ","
						+ peak
						+ ",'"
						+ dateTime + "')";
				
				log.debug("Query to insert network data :: " + networkInsertQuery);
				
				mySQLStatement.executeUpdate(networkInsertQuery);
			}
			log.debug("Insertion for Network Utilization done");

			msSQLConnection.close();

			/* code for procedure */
			mySQLConnection.close();
			log.debug("Connection closed");

		} catch (Exception e) {
			log.error("Error occurred in CPENetworkSWDataRetrieve :: "
					+ e.getMessage());
			e.printStackTrace();
		}
	}
}
