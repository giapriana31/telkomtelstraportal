/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.batchrefresh.model.customer.Customer;
import com.tt.batchrefresh.populator.CustomerPopulator;
import com.tt.utils.PropertyReader;

/**
 * @author Infosys
 *
 */
public class CustomerBatchRefresh implements BatchRefresh
{

	private static final String PARAMS = "sysparm_query=sys_updated_on>=javascript:gs.dateGenerate(UPDATED_DATE)";
	private static Properties prop = PropertyReader.getProperties();	

	private String url = "";
	private String params = "";
	private List<Customer> custList;
	private List<Customer> customers;
	
	private String fromDate = "";
	private String toDate = "";
	private String interval = "";
	private String entityType = "";
	
	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws IOException, JAXBException
	{
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.interval = interval;
		this.entityType = entityType;
		
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		this.params = PARAMS.replaceAll("UPDATED_DATE", date);
		System.out.println(params);

		this.url = prop.getProperty("CUST_URL");
		
		refreshDataThread();
		
		//Properties prop = ApplicationProperties.getInstance();
		/**
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		String params = PARAMS.replaceAll("UPDATED_DATE", date);
		System.out.println(params);

		String url = prop.getProperty("CUST_URL");
		List<Customer> custList = new CustomerPopulator().populateModel(url, params);
		System.out.println(custList);
		
		
		List<Customer> customers = new BatchRefreshDAO().getCustomer(custList);

		if(null != fromDate && "2015-01-01 00:00:00".equalsIgnoreCase(fromDate)){
			new BatchRefreshDAO().truncateCustomersRecords();
		}
		
		
		new BatchRefreshDAO().refreshData(customers);

		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		new BatchRefreshDAO().updateLastRefresh("Customer", toDateFormatted, interval);
		**/
	}
	
	public void refreshDataThread(){
		
		//Properties prop = ApplicationProperties.getInstance();
		
		Thread t1 = new Thread(new Runnable(){

			public void run() {
				try {
					getCustomerList();
					
					System.out.println("custList : " + custList.toString());
					
				} catch (JAXBException | IOException e) {
					e.printStackTrace();
				}
				
			}	
		});
		
		Thread t2 = new Thread(new Runnable(){

			@Override
			public void run() {
				getCustomer();
			}
		});
		
		Thread t3 = new Thread(new Runnable(){

			@Override
			public void run() {
				if(null != fromDate && "2015-01-01 00:00:00".equalsIgnoreCase(fromDate)){
					truncateCustRecord();
				}				
			}
		});
		
		Thread t4 = new Thread(new Runnable(){

			@Override
			public void run() {
				refreshCustData();
			}
		});
		
		Thread t5 = new Thread(new Runnable(){

			@Override
			public void run() {
				Date todate = new Date();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				df.setTimeZone(TimeZone.getTimeZone("GMT"));
				String toDateFormatted = df.format(todate);
				new BatchRefreshDAO().updateLastRefresh("Customer", toDateFormatted, interval);
			}
		});
		
		t1.setName("Thread 1-custList");
		t2.setName("Thread 2-getCustomer");
		t3.setName("Thread 3-truncedCustomer");
		t4.setName("Thread 4-refreshCustData");
		t5.setName("Thread 5-updateLashRefresh");
		
		try {
			t1.start();
			t1.join();
			t2.start();
			t2.join();
			t3.start();
			t3.join();
			t4.start();
			t4.join();
			t5.start();
			t5.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void getCustomerList() throws JAXBException, IOException{
		
		custList = new CustomerPopulator().populateModel(url, params);
	}
	
	public synchronized void getCustomer(){
		customers = new BatchRefreshDAO().getCustomer(custList);
	}
	
	public synchronized void truncateCustRecord(){
		new BatchRefreshDAO().truncateCustomersRecords();
	}
	
	public synchronized void refreshCustData(){
		new BatchRefreshDAO().refreshData(customers);
	}

	public static void main(String[] args)
	{
		//String date = new BatchRefreshDAO().retrieveLastRefreshDate("Customer");
		String date = "2015-01-01 00:00:00";
		System.out.println(date);
		CustomerBatchRefresh cb = new CustomerBatchRefresh();
		try
		{
			cb.refreshData(date, null,"","");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
