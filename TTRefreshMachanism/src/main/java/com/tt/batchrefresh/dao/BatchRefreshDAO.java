/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.hibernate.criterion.Projections;

import com.tt.batchrefresh.model.ci.Configuration;
import com.tt.batchrefresh.model.ci.Incident;
import com.tt.batchrefresh.model.cirelation.CIRelationship;
import com.tt.batchrefresh.model.contarctAvailability.ContractAvailability;
import com.tt.batchrefresh.model.customer.Customer;
import com.tt.dao.HibernateUtil;
import com.tt.util.RefreshUtil;

/**
 * @author Infosys
 * @version 1.0
 */
public class BatchRefreshDAO
{
	private static final Logger logger = Logger.getLogger(BatchRefreshDAO.class);

	/**
	 * 
	 * @return
	 */
	public List<String> getCustomerIdList()
	{
		List<String> custIdList = new ArrayList<>();

		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Customer.class);
		criteria.setProjection(Projections.property("customeruniqueid"));
		custIdList = criteria.list();
		if(session!=null)
			session.close();
		return custIdList;
	}

	public List<Customer> getCustomer(List<Customer> custList)
	{

		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Customer> customers = new ArrayList<>();
		for (Customer customer : custList)
		{
			Query query = session.createQuery("SELECT id FROM Customer  WHERE customeruniqueid=:customeruniqueid");
			query.setString("customeruniqueid", customer.getCustomeruniqueid());
			if (query.list() != null && query.list().size() > 0)
				customer.setId((Long) query.list().get(0));

			customers.add(customer);
		}
		if(session!=null)
			session.close();
		
		if(customers!=null & customers.size()>0)
			logger.warn("No. of Customers' records updated in Batch Refresh -->>"+customers.size());
		return customers;

	}
	
	/**
	 * This method calls procedure calculate_netwrk_perform
	 */
	public void refreshNetworkPerformance()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();

		SQLQuery sqlQuery = session.createSQLQuery("CALL calculate_netwrk_perform()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();

	}
	
	/**
	 * This method calls procedure calculate_netwrk_perform_partial
	 */
	public void refreshNetworkPerformancePartial(String customerId)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();

		SQLQuery sqlQuery = session.createSQLQuery("CALL calculate_netwrk_perform_partial('"+customerId+"')");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();

	}

	
	
	/**
	 * @param objList
	 * @return
	 */
	public List<Configuration> getConfigurationItems(List<Configuration> objList)
	{

		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Configuration> configList = new ArrayList<>();

		for (Configuration obj : objList)
		{
			if(obj != null)
			{
				Query query = session.createQuery("SELECT id FROM Configuration  WHERE ciid=:ciid");
				
				query.setString("ciid", obj.getCiid());
				if (query.list() != null && !query.list().isEmpty())
					obj.setId((Long) query.list().get(0));
	
				configList.add(obj);
			}
		}
		
		if(session!=null)
			session.close();
		if(configList!=null & configList.size()>0)
			logger.warn("No. of CI records updated in Batch Refresh -->>"+configList.size());
		return configList;

	}
	
	public void deleteRefreshDependentPrivateCloudCI(List<Configuration> existingCI, List<Configuration> configListUp)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction txn = session.beginTransaction();
		for (Configuration ciid : existingCI)
		{
			session.createSQLQuery(" DELETE FROM VBlock WHERE ciid = '"+ciid.getCiid()+"'").executeUpdate();
			session.createSQLQuery(" DELETE FROM virtualmachine WHERE ciid = '"+ciid.getCiid()+"'").executeUpdate();
			session.createSQLQuery(" DELETE FROM vcenter WHERE ciid = '"+ciid.getCiid()+"'").executeUpdate();
			session.createSQLQuery(" DELETE FROM cloudbusiness WHERE ciid = '"+ciid.getCiid()+"'").executeUpdate();
			session.merge(ciid);
		}
		
		txn.commit();
		session.close();
	}
	
	/**
	 * @param modelList
	 */
	public void refreshData(List<?> modelList)
	{

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();

		for (Object obj : modelList){
			session.merge(obj);
		}
		transaction.commit();
		session.close();
	}
	
	public void insertRefreshData(Configuration configuration) {
		// Insert refresh data

		System.out.println("insertRefreshData...");
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.merge(configuration);
		transaction.commit();
		session.close();
	}


	public void updateLastRefresh(String entityType, String toDate, String interval)
	{

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.setFlushMode(FlushMode.MANUAL);
		ManagedSessionContext.bind(session);
		Transaction txn = session.beginTransaction();

		Query query = session.createSQLQuery("UPDATE lastrefreshtime l set l.status='X' WHERE l.entityname=:entityType and l.jobinterval = :interval");
		query.setString("entityType", entityType);
		query.setString("interval", interval);
		
		query.executeUpdate();

		Query insert = session.createSQLQuery("Insert into lastrefreshtime (status,customeruniqueid,customername,entityname,lastrefreshtime, jobinterval) values('A','','',:entityType,:lastrefreshtime, :interval)");
		insert.setString("entityType", entityType);
		insert.setString("lastrefreshtime", toDate);
		insert.setString("interval", interval);
		insert.executeUpdate();

		txn.commit();
		session.close();

	}

	/**
	 * @param entityType
	 * @return
	 */
	public String retrieveLastRefreshDate(String entityType, String interval)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createSQLQuery("SELECT max(lastrefreshtime) FROM lastrefreshtime l WHERE l.entityname=:entityType and l.jobinterval=:interval");
		query.setString("entityType", entityType);
		query.setString("interval", interval);
		if (query.list().isEmpty())
			return null;
		String date = RefreshUtil.convertDateToString((Date) query.list().get(0));
		if(session!=null)
			session.close();
		return date;
	}

	/** Retrieves the last refresh date for a customer. THis is used to determine when the incidents
	 * for this particular customer were last refreshed. 
	 * @param entityType This will be "Customer"
	 * @param customerUniqueId Customer ID
	 * @return Last refresh date. 
	 */
	public String retrieveLastRefreshDate(String entityType, String customerUniqueId, String interval)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		String sql = "SELECT max(lastrefreshtime) FROM lastrefreshtime l WHERE l.entityname=:entityType AND l.customerUniqueId=:customerUniqueId and l.jobinterval=:interval";
		Query query = session.createSQLQuery(sql);
		query.setString("entityType", entityType);
		query.setString("customerUniqueId", customerUniqueId);
		query.setString("interval", interval);
		if (query.list().isEmpty())
			return null;
		String date = RefreshUtil.convertDateToString((Date) query.list().get(0));
		if(session!=null)
			session.close();
		return date;
	}

	public void deleteSitesRefresh(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String sql = "DELETE from site WHERE siteid NOT IN (SELECT siteid from configurationitem)";
		Query query = session.createSQLQuery(sql);
		query.executeUpdate();
		txn.commit();
		session.close();
	}
	
	public void sitesBackupRefresh(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String delTable = "DELETE from sitesbackup";
		Query query = session.createSQLQuery(delTable);
		query.executeUpdate();
		String sql = "INSERT into sitesbackup SELECT * from site WHERE siteid NOT IN (SELECT siteid from configurationitem)";
		query = session.createSQLQuery(sql);
		query.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}
	
	public void updatePVSiteDeliveryStatus(){
		
		logger.info("BatchRefreshDao-updatePVSiteDeliveryStatus Start");
		Map<String,String> deliveryStageToStatus= new HashMap<String, String>();
		deliveryStageToStatus.put("6", "Operational");
		deliveryStageToStatus.put("5", "Monitoring");
		deliveryStageToStatus.put("4", "Delivery & Activation");
		deliveryStageToStatus.put("3", "Procurement");
		deliveryStageToStatus.put("2", "Detailed Design");
		deliveryStageToStatus.put("1", "Order Received");	
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		
		Query query = session.createSQLQuery("select distinct(c.siteid),min(c.delivery_stage) from configurationitem c ,site s  where c.siteid=s.siteid and c.cmdbclass in ('CPE', 'MNS') group by siteid;");
		if(null!=query.list() && query.list().size()>0){
			
			
			List<Object[]> siteIdList=query.list();
			String validSites="";
			for (Object[] objects : siteIdList) {
				
				String siteId=(String)objects[0];
				String status=deliveryStageToStatus.get((String)objects[1]);
				System.out.println("site ---------"+siteId +"-------status ---------"+status);
				
				if(null==status || (null != status && "".equalsIgnoreCase(status.trim()))){
					Query siteStatusQuery = session.createSQLQuery("select status from site where siteid='"+siteId+"'");
					Object siteStatus = siteStatusQuery.uniqueResult();
					
					if(null != siteStatus)
					{
						status= siteStatus.toString();
					}
					else
					{
						status = "";
					}
					
					System.out.println("Status :: " + status);
				}
				validSites=validSites+",'"+siteId+"'";
				String customerSiteStatus = "";
				if("operational".equalsIgnoreCase(status)){
					System.out.println("Operational status of sites");
					customerSiteStatus="Operational";
				}else{
					System.out.println("Non Operational status of sites---"+status);
					String sritypeSQL = "select distinct(cistatus) from  configurationitem where siteid ='"+siteId+"' and citype = 'Resource'";	
					Query sritypeQuery = session.createSQLQuery(sritypeSQL);
					if(sritypeQuery!=null && null!=sritypeQuery.list() && sritypeQuery.list().size()>0){
						List<String> sritypeList = (List<String>) sritypeQuery.list();
						if(sritypeList!=null && sritypeList.contains("MAC")){
							customerSiteStatus="Operational";
						}else{
							customerSiteStatus=status;
						}
					}else{
						customerSiteStatus=status;
					}
				}
				
				
				String sql = "Update site set pvsitedeliverystatus='"+status+"', status = '"+customerSiteStatus+"' where siteid='"+siteId+"'";
				Query updateSiteStatus = session.createSQLQuery(sql);
				updateSiteStatus.executeUpdate();
				
			}
			
			if(null!=validSites && !"".equalsIgnoreCase(validSites.trim())){
				// if no deliverys status is received
				String validSiteString=validSites.substring(1);				
				String sql = "Update site set pvsitedeliverystatus='' where siteid not in ("+validSiteString+")";
				Query validSitesQuery = session.createSQLQuery(sql);
				validSitesQuery.executeUpdate();
			}
			
		}
		txn.commit();
		if(session!=null)
			session.close();
		logger.info("BatchRefreshDao-UpdateSiteStatus END");

		
	}
	
	
	public void updateVBlockStatus(){
		
		logger.info("BatchRefreshDao-updateVBlockStatus start");

		
		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(6, "#92D050");
		deliveryStageToColor.put(5, "#FFC000");
		deliveryStageToColor.put(4, "#00B0F0");
		deliveryStageToColor.put(3, "#7030A0");
		deliveryStageToColor.put(2, "#A40800");
		deliveryStageToColor.put(1, "black");
		
		Map<String,String> deliveryStageToStatus= new HashMap<String, String>();
		deliveryStageToStatus.put("6", "Operational");
		deliveryStageToStatus.put("5", "Monitoring");
		deliveryStageToStatus.put("4", "Delivery & Activation");
		deliveryStageToStatus.put("3", "Procurement");
		deliveryStageToStatus.put("2", "Detailed Design");
		deliveryStageToStatus.put("1", "Order Received");
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		
		StringBuilder vblockSQL = new StringBuilder();
		vblockSQL.append("SELECT c.ciid, c.ciname, cr.dependentciid, c.status, c.deliverystatus, c.cistatus, c.delivery_stage");
		vblockSQL.append(" FROM configurationitem c, cirelationship cr WHERE c.ciid = cr.baseciid");
		vblockSQL.append(" AND c.rootproductid = 'Private Cloud' and c.cmdbclass = 'vBLOCK' order by c.delivery_stage");
		
		Query query = session.createSQLQuery(vblockSQL.toString());
		List<String> vBlockList = new ArrayList<String>();
		String vblockS = new String();
		String color = new String();
		
		if(null!=query.list() && query.list().size()>0){
			
			List<Object[]> vblockDataList=query.list();
			
			for (Object[] objects : vblockDataList) {
				String vblockciid = (String)objects[0];
				String dependentCiid = (String)objects[2];
				String vblockDeliveryStatus = (String)objects[4];
				String vblockSRIType = (String)objects[5];
				
				if(vblockDeliveryStatus!=null && !"Operational".equalsIgnoreCase(vblockDeliveryStatus) &&
						vblockSRIType!=null && "New Service".equalsIgnoreCase(vblockSRIType)){
					vblockS = vblockDeliveryStatus;
				} else{
					vblockS = "Operational";
				}
				
				color = deliveryStageToColor.get(vblockDeliveryStatus);
				
				if(!vBlockList.contains(vblockciid) && null!=vblockDeliveryStatus && null != vblockS){
					Query updateQuery=session.createSQLQuery("update configurationitem set status = '"+vblockS+"', deliverystatus='"+vblockDeliveryStatus+"' where ciid='"+vblockciid+"'");
					updateQuery.executeUpdate();
				}
				vBlockList.add(vblockciid);
				if(null!=vblockDeliveryStatus && null != vblockS){
					Query updateQuery=session.createSQLQuery("update configurationitem set status = '"+vblockS+"', deliverystatus='"+vblockDeliveryStatus+"' where ciid='"+dependentCiid+"'");
					updateQuery.executeUpdate();
				}
				
			}
			
			
		}
		txn.commit();
		if(session!=null)
			session.close();
		
	}
	
	public void sitesPopulationRefresh(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String sitesDelete = "DELETE from site";
		Query query = session.createSQLQuery(sitesDelete);
		query.executeUpdate();
		String populateSites = "INSERT into site SELECT * from allsites WHERE siteid IN(SELECT siteid from configurationitem WHERE status IN('Operational'))";
		query = session.createSQLQuery(populateSites);
		query.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}
	
	public static void main(String[] args)
	{
		System.out.println("*********** DATE IS 888888888888888  " + new BatchRefreshDAO().retrieveLastRefreshDate("customer", ""));
		//System.out.println(new BatchRefreshDAO().getCustomerIdList());
		
		new BatchRefreshDAO().updatePVSiteDeliveryStatus();
	}

	public void deleteExistingCIRelationship(List<CIRelationship> ciList) {

		for (CIRelationship ciRelationship : ciList) {

			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction txn = session.beginTransaction();
			String sitesDelete = "delete from cirelationship where baseciid =:baseciid  and dependentciid=:dependentciid and relationship =:relationship";
			Query query = session.createSQLQuery(sitesDelete);
			query.setParameter("baseciid", ciRelationship.getBaseCIId());
			query.setParameter("dependentciid",
					ciRelationship.getDependentCIId());
			query.setParameter("relationship", ciRelationship.getRelationShip());
			query.executeUpdate();
			txn.commit();
			if(session!=null)
				session.close();
		}
	}

	public void truncateCIRecords() {
		
		System.out.println("Testing truncate----------");
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String truncateCIQuery = "delete from configurationitem";
		Query query = session.createSQLQuery(truncateCIQuery);
		query.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}

	public void truncateCIRelationshipRecords() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String truncateCIQuery = "delete from cirelationship";
		Query query = session.createSQLQuery(truncateCIQuery);
		query.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}

	public void truncateCustomersRecords() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String truncateCIQuery = "delete from customers";
		Query query = session.createSQLQuery(truncateCIQuery);
		query.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}

	public void truncateContractAvailabilityRecords() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String truncateCIQuery = "delete from contractedavailability";
		Query query = session.createSQLQuery(truncateCIQuery);
		query.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}
	
	public void truncateSitesRecords() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String truncateCIQuery = "delete from site";
		Query query = session.createSQLQuery(truncateCIQuery);
		query.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}

	public void truncateOutageRecords() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String truncateOutageServQuery = "delete from affectedoutageservices";
		Query query = session.createSQLQuery(truncateOutageServQuery);
		query.executeUpdate();
		
		String truncateOutageQuery = "delete from serviceoutage";
		Query outageQuery = session.createSQLQuery(truncateOutageQuery);
		outageQuery.executeUpdate();
		txn.commit();
		if(session!=null)
			session.close();
	}

	public String getCustomerUniqueId(String customerDisplayName) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String customerUniqueIdQ = "select customeruniqueid from customers where accountname = '"+customerDisplayName+"'";
		Query query = session.createSQLQuery(customerUniqueIdQ);
		if(session!=null)
			session.close();
		if(query.list()!=null && query.list().size()>0)
			return (String) query.list().get(0);
		else 
			return null;
		
	}

	public List<ContractAvailability> getContractAvailability(
			List<ContractAvailability> contractAvailabilities) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		List<ContractAvailability> contractAvailabilityList = new ArrayList<>();
		for (ContractAvailability contractAvailability : contractAvailabilities) {
			Query query = session
					.createSQLQuery("select id from contractedavailability where producttype =:productType and servicetier =:serviceTier");
			query.setString("productType",
					contractAvailability.getProductType());
			query.setString("serviceTier",
					contractAvailability.getServiceTier());
			if (query.list() != null && query.list().size() > 0)
				contractAvailability.setId((Integer) query.list().get(0));

			contractAvailabilityList.add(contractAvailability);
		}

		if (contractAvailabilityList != null
				& contractAvailabilityList.size() > 0)
			logger.warn("No. of getContractAvailability' records updated in Batch Refresh -->>"
					+ contractAvailabilityList.size());

		if(session!=null)
			session.close();
		return contractAvailabilityList;
	}

	public void checkExitingCIAndUpdate(List<CIRelationship> ciList) {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		for(CIRelationship ciRelationship : ciList){
			String existId = checkCIExist(ciRelationship);
			if(existId!=null && existId.length()>0){
				String sitesDelete = "delete from cirelationship where baseciid =:baseciid  and dependentciid=:dependentciid and relationship =:relationship";
				Query query = session.createSQLQuery(sitesDelete);
				query.setParameter("baseciid", ciRelationship.getBaseCIId());
				query.setParameter("dependentciid", ciRelationship.getDependentCIId());
				query.setParameter("relationship", ciRelationship.getRelationShip());
				query.executeUpdate();
				session.merge(ciRelationship);
			}
		}
		txn.commit();
		if(session!=null)
			session.close();
	}

	private String checkCIExist(CIRelationship ciList) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		
		String sitesDelete = "select * from configurationitem where ciid in (:baseCiid , :dependentCiid)";
		Query query = session.createSQLQuery(sitesDelete);
		query.setParameter("baseCiid", ciList.getBaseCIId());
		query.setParameter("dependentCiid", ciList.getDependentCIId());
		
		/*if(session!=null)
			session.close();*/
		if(query.list()!=null && query.list().size()==2)
			return "exist";
		else
			return null;
	}
	
	public void updateLastRefresh(String entityType, String toDate, String interval, String customerId)
	{

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.setFlushMode(FlushMode.MANUAL);
		ManagedSessionContext.bind(session);
		Transaction txn = session.beginTransaction();

		Query query = session.createSQLQuery("UPDATE lastrefreshtime l set l.status='X' WHERE l.entityname=:entityType and l.jobinterval = :interval and l.customeruniqueid = :customerId");
		query.setString("entityType", entityType);
		query.setString("interval", interval);
		query.setString("customerId", customerId);
		
		query.executeUpdate();

		Query insert = session.createSQLQuery("Insert into lastrefreshtime (status,customeruniqueid,customername,entityname,lastrefreshtime, jobinterval) values('A',:customerId,'',:entityType,:lastrefreshtime, :interval)");
		insert.setString("entityType", entityType);
		insert.setString("lastrefreshtime", toDate);
		insert.setString("interval", interval);
		insert.setString("customerId", customerId);
		insert.executeUpdate();

		txn.commit();
		session.close();

	}

	
}
