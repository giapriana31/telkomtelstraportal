package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.batchrefresh.model.contarctAvailability.ContractAvailability;
import com.tt.batchrefresh.populator.ContractAvailabilityPopulator;
import com.tt.utils.PropertyReader;

public class ContractAvailabilityBatchRefresh implements BatchRefresh {


	private static final String PARAMS = "sysparm_query=sys_updated_on>=javascript:gs.dateGenerate(UPDATED_DATE)";
	private static Properties prop = PropertyReader.getProperties();	

	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws IOException, JAXBException, ParseException
	{
		//Properties prop = ApplicationProperties.getInstance();
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		String params = PARAMS.replaceAll("UPDATED_DATE", date);
		System.out.println(params);

		String url = prop.getProperty("ContractAvailability_URL");
		List<ContractAvailability> contractAvailabilities = new ContractAvailabilityPopulator().populateModel(url, params);
		System.out.println("getContractAvailability----"+ contractAvailabilities);
		List<ContractAvailability> contractAvailabilityList = new BatchRefreshDAO().getContractAvailability(contractAvailabilities);

		if(null != fromDate && "2015-01-01 00:00:00".equalsIgnoreCase(fromDate)){
			new BatchRefreshDAO().truncateContractAvailabilityRecords();
		}
		new BatchRefreshDAO().refreshData(contractAvailabilityList);

		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		new BatchRefreshDAO().updateLastRefresh("Contract Availability", toDateFormatted, interval);
	}

	public static void main(String[] args)
	{
		//String date = new BatchRefreshDAO().retrieveLastRefreshDate("Customer");
		String date = "2015-01-01 00:00:00";
		System.out.println(date);
		ContractAvailabilityBatchRefresh cb = new ContractAvailabilityBatchRefresh();
		try
		{
			cb.refreshData(date, null,"", "");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}


}
