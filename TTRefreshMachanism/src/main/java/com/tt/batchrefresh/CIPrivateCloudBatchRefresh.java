package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.batchrefresh.model.ci.Configuration;
import com.tt.batchrefresh.populator.CIPopulator;
import com.tt.batchrefresh.populator.CIPrivateCloudPopulator;
import com.tt.utils.PropertyReader;

public class CIPrivateCloudBatchRefresh implements BatchRefresh{
	private static Properties prop = PropertyReader.getProperties();
	private static final String PARAMS = "sysparm_query=ci_sys_class_name=u_cloud_business_service^ORci_sys_class_name=u_cmdb_ci_hardware_vblock^ORci_sys_class_name=cmdb_ci_vmware_instance^ORci_sys_class_name=cmdb_ci_vcenter^ci_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";

	private static final String CUSTPARAMS = "sysparm_query=ci_sys_class_name=u_cloud_business_service^ORci_sys_class_name=u_cmdb_ci_hardware_vblock^ORci_sys_class_name=cmdb_ci_vmware_instance^ORci_sys_class_name=cmdb_ci_vcenter^ci_u_customer_uniqueid=CUSTOMER_UNIQUE_ID^ci_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
	
	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws JAXBException, ParseException, IOException
	{
		//Properties prop = ApplicationProperties.getInstance();
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		String params = PARAMS.replaceAll("CREATED_ON", date);
		date = "'" + toDate.substring(0, 10) + "','" + toDate.substring(11) + "'";
		params = params.replaceAll("UPDATED_ON", date);

		String url = prop.getProperty("CI_URL");
		List<Configuration> configList = new CIPrivateCloudPopulator().populateModel(url, params);
		List<Configuration> configListUp = new BatchRefreshDAO().getConfigurationItems(configList);
		
		
		new BatchRefreshDAO().deleteRefreshDependentPrivateCloudCI(configListUp, configList);
		
		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		new BatchRefreshDAO().updateLastRefresh("PCConfiguration", toDateFormatted, interval);
	}
	
	public void refreshDataCustom(String fromDate, String toDate, String interval, String customerId) throws JAXBException, ParseException, IOException
	{
		//Properties prop = ApplicationProperties.getInstance();
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		String params = CUSTPARAMS.replaceAll("CREATED_ON", date);
		date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		params = params.replaceAll("UPDATED_ON", date);
		params = params.replaceAll("CUSTOMER_UNIQUE_ID", customerId);
		
		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		
		String url = prop.getProperty("CI_URL");
		List<Configuration> configList = new CIPrivateCloudPopulator().populateModel(url, params);
		List<Configuration> configListUp = new BatchRefreshDAO().getConfigurationItems(configList);
		
		new BatchRefreshDAO().deleteRefreshDependentPrivateCloudCI(configListUp, configList);
		new PrivateCloudCIRelationBatchRefresh().customRefreshData(fromDate, toDateFormatted, interval, customerId);
		
		new BatchRefreshDAO().updateLastRefresh("PCConfiguration", toDateFormatted, interval, customerId);
	}

	public static void main(String[] args)
	{
		String date = "2015-01-01 00:00:00";
		System.out.println(date);
		try
		{
			CIPrivateCloudBatchRefresh c = new CIPrivateCloudBatchRefresh();
			c.refreshData(date, date, "", "");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
		}
	}


}
