/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				28-DEC-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;
import com.tt.batchrefresh.dao.BatchRefreshDAO;

/**
 * @author Infosys
 *
 */
public class SitesPopulation
{
	public static void main(String[] args) {
		SitesPopulation sitesPopulation = new SitesPopulation();
		sitesPopulation.refresh();
	}
	public void refresh(){
		BatchRefreshDAO brdao = new BatchRefreshDAO();
		brdao.sitesPopulationRefresh();
	}
}