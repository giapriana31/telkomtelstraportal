/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.URL;

import org.apache.log4j.Logger;

import com.tt.auth.MyWSAuthenticator;
import com.tt.utils.RefreshUtil;



/** This class reads data from SNOW and writes it into a file
 * @author Infosys
 * @version 1.0
 */
public class FetchRefreshData
{
	private static final Logger logger = Logger.getLogger(FetchRefreshData.class);

	public InputStream fetchRefreshDataStream(String url, String... params) throws IOException
	{
		RefreshUtil.setProxyObject();
		//Properties prop = ApplicationProperties.getInstance();
		MyWSAuthenticator auth = new MyWSAuthenticator("portal_webservice", "portal");
		Authenticator.setDefault(auth);

		String link = url;
		for (String p : params)
		{
			link += "&" + p;
		}
		logger.info(link);
		URL dataLink = new URL(link);

		// Read data from the link
		InputStream in = new BufferedInputStream(dataLink.openStream());	
		return in;
	}
	
	/** Reads data from SNOW and writes it into a file
	 * @param filePath
	 * @param url
	 * @param params
	 * @throws IOException
	 */
	public void fetchRefreshData(String filePath, String url, String... params) throws IOException
	{
		RefreshUtil.setProxyObject();
		//Properties prop = ApplicationProperties.getInstance();
		MyWSAuthenticator auth = new MyWSAuthenticator("portal_webservice", "portal");
		Authenticator.setDefault(auth);

		String link = url;
		for (String p : params)
		{
			link += "&" + p;
		}
		logger.info(link);
		URL dataLink = new URL(link);

		// Read data from the link
		InputStream in = new BufferedInputStream(dataLink.openStream());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int n = 0;
		while (-1 != (n = in.read(buf)))
		{
			out.write(buf, 0, n);
		}

		out.close();
		in.close();
		byte[] response = out.toByteArray();
		logger.info("filePath-----" + filePath);
		// Write data from the buffer to a file
		FileOutputStream fos = new FileOutputStream(filePath);
		fos.write(response);
		fos.close();
	}

	
}
