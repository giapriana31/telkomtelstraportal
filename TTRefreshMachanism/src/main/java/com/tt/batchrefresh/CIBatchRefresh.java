/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.batchrefresh.model.ci.Configuration;
import com.tt.batchrefresh.populator.CIPopulator;
import com.tt.util.RefreshUtil;
import com.tt.utils.PropertyReader;

/**
 *
 * @author Arijit_Dasgupta
 */
public class CIBatchRefresh implements BatchRefresh
{
	private static Properties prop = PropertyReader.getProperties();
	private static final String PARAMS = "sysparm_query=ci_sys_class_name=u_managed_network_service^ORci_sys_class_name=u_cpe^ORci_sys_class_name=u_pe^ORci_sys_class_name=u_pe_interface^ci_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";

	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws JAXBException, ParseException, IOException
	{
		String entityParam = getCIRefreshEntityParam(entityType);
		
		String date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		String params = entityParam.replaceAll("CREATED_ON", date);
		date = "'" + fromDate.substring(0, 10) + "','" + fromDate.substring(11) + "'";
		params = params.replaceAll("UPDATED_ON", date);
			
		String url = prop.getProperty("CI_URL");
		System.out.println("url dba "+url);
		System.out.println("params dba "+params);
		List<Configuration> configList = new CIPopulator().populateModel(url, params);
		System.out.println("configList dba "+configList);
		System.out.println("from date : "+fromDate);
		System.out.println("entity type : "+ entityType);
		if(null != fromDate && "2015-01-01 00:00:00".equalsIgnoreCase(fromDate) && entityType.equalsIgnoreCase("Configuration")){
			new BatchRefreshDAO().truncateCIRecords();
		}
		
		List<Configuration> configListUp = new BatchRefreshDAO().getConfigurationItems(configList);
		System.out.println("config List Up "+configListUp);
		for (Configuration configuration : configListUp) {
			new BatchRefreshDAO().insertRefreshData(configuration);	
		}
		
		
		
		Date todate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String toDateFormatted = df.format(todate);
		new BatchRefreshDAO().updateLastRefresh(entityType, toDateFormatted, interval);
	}

	private String getCIRefreshEntityParam(String entityType) {
		String entityParam = "sysparm_query=ci_sys_class_name=u_managed_network_service^ORci_sys_class_name=u_cpe^ORci_sys_class_name=u_pe^ORci_sys_class_name=u_pe_interface^ci_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
		switch (entityType){
			case "Configuration":
				entityParam = "sysparm_query=ci_sys_class_name=u_managed_network_service^ORci_sys_class_name=u_cpe^ORci_sys_class_name=u_pe^ORci_sys_class_name=u_pe_interface^ci_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
				break;
			case "ConfigurationSaaS":
				entityParam = "sysparm_query=ci_sys_class_name=u_saas_business_service^ORci_sys_class_name=u_saas_component^ci_sys_updated_on>=javascript:gs.dateGenerate(UPDATED_ON)";
				break;
			default:
				System.out.println("UNKOWN entity Mode");
		}
		return entityParam;
	}

	public static void main(String[] args)
	{
		String date = "2015-01-01 00:00:00";
		System.out.println(date);
		try
		{
			CIBatchRefresh c = new CIBatchRefresh();
			c.refreshData(date, "", "", "");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
		}
	}
}
