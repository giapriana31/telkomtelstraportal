/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.io.IOException;
import java.text.ParseException;

import javax.xml.bind.JAXBException;

/**
 * @author Infosys
 *
 */
public interface BatchRefresh
{
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws ParseException, IOException, JAXBException;
}
