/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys				24-JUL-2015		Initial Version
 * 
 * **************************************************************************************************************
 */

package com.tt.batchrefresh;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import com.tt.RefreshServiceRequest;
import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.utils.PropertyReader;

/**
 * @author Infosys
 *
 */
public class SRBatchRefresh implements BatchRefresh
{
	private static final Logger logger = Logger.getLogger(SRBatchRefresh.class);
	private static Properties prop = PropertyReader.getProperties();
	
	/* (non-Javadoc)
	 * @see com.tt.batchrefresh.BatchRefresh#refreshData(java.lang.String, java.lang.String)
	 */
	
	RefreshServiceRequest rsr = new RefreshServiceRequest();
	
	private String fromDate = "";
	private String toDate = "";
	private String interval = "";
	private String entityType = "";
	private String customerId = "";
	
	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws ParseException, IOException, JAXBException {
		// Fetch all customer Ids
		List<String> custIdList = new BatchRefreshDAO().getCustomerIdList();
		logger.info("Cust Ids retrieved for SR : " + custIdList);

		// Fetch list of ids to be ignored
		String ids = prop.getProperty("SKIP_CUSTOMER_LIST");
		Set<String> skipSet = new HashSet<>();
		if (null != ids)
		{
			for (String id : ids.split(","))
			{
				skipSet.add(id);
			}
		}

		RefreshServiceRequest rsr = new RefreshServiceRequest();
		for (String custId : custIdList)
		{
			if(skipSet.contains(custId))
			{
				logger.info("Skipping customer "+custId);
				continue;
			}
			
			// If null date is passed, it means that we want the refresh to run for dates picked from the database, 
			if (null == fromDate)
				fromDate = new BatchRefreshDAO().retrieveLastRefreshDate("ServiceRequest", custId, interval);
			if (null == fromDate)
				fromDate = "2015-01-01 00:00:00";

			rsr.refreshServiceRequest(custId, toDate, fromDate, interval);
		}

	}
	
	public void refreshDataPartial(String fromDate, String toDate, String interval, String entityType, String customerId) throws ParseException, IOException, JAXBException {
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.interval = "HOURLY";
		this.entityType = entityType;
		this.customerId = customerId;
		
		refreshDataPartialThread();
	}
	
	public void refreshDataPartialThread(){
		Thread t1 = new Thread(new Runnable(){

			@Override
			public void run() {
				
				try {
					refreshServiceRequestPartialCust();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
		t1.setName("Thread 1-refreshServiceRequestPartialCust : " + customerId);
		
		t1.start();
		
		try {
			t1.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void refreshServiceRequestPartialCust() throws ParseException{
		if (null == fromDate)
			fromDate = new BatchRefreshDAO().retrieveLastRefreshDate("ServiceRequest", customerId, interval);
		if (null == fromDate)
			fromDate = "2015-01-01 00:00:00";
		
		rsr.refreshServiceRequest(customerId, toDate, fromDate, interval);
	}

}
