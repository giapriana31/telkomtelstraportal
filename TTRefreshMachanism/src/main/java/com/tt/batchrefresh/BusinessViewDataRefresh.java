package com.tt.batchrefresh;

import java.io.IOException;
import java.text.ParseException;

import javax.xml.bind.JAXBException;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tt.dao.HibernateUtil;

public class BusinessViewDataRefresh implements BatchRefresh 
{

	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType)
			throws ParseException, IOException, JAXBException 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL populatebvsubscribedservices()");
		SQLQuery sqlQuery2 = session.createSQLQuery("CALL bvsitemanagementtableproc()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();
		sqlQuery2.executeUpdate();
		txn.commit();
		session.close();
	}
	
	
	public void refreshDataDaily()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL ci_contractremainingtime_update_daily()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();
		txn.commit();
		session.close();
	}

}
