/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import com.tt.RefreshIncident;
import com.tt.batchrefresh.dao.BatchRefreshDAO;
import com.tt.utils.PropertyReader;


/** This class calls the Refresh Incident module for every customer present in the database
 * @author Infosys
 *
 */
public class IncidentBatchRefresh implements BatchRefresh
{
	private static final Logger logger = Logger.getLogger(IncidentBatchRefresh.class);
	private static Properties prop = PropertyReader.getProperties();
	
	/* (non-Javadoc)
	 * @see com.tt.batchrefresh.BatchRefresh#refreshData(java.util.Date, java.util.Date)
	 */
	
	private List<String> custIdList;
	private String ids = prop.getProperty("SKIP_CUSTOMER_LIST");
	private Set<String> skipSet = new HashSet<>();
	private RefreshIncident ri = new RefreshIncident();
	
	private String fromDate = "";
	private String toDate = "";
	private String interval = "";
	private String entityType = "";
	
	private String customerId = "";
	
	@Override
	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws ParseException
	{
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.interval = interval;
		this.entityType = entityType;
		
		refreshDataThread();
		
		/**
		// Fetch all customer Ids
		List<String> custIdList = new BatchRefreshDAO().getCustomerIdList();
		logger.info("CuSt Ids retrieved : " + custIdList);
		
		// Fetch list of ids to be ignored
		String ids = prop.getProperty("SKIP_CUSTOMER_LIST");
		Set<String> skipSet = new HashSet<>();
		if(null!=ids)
		{
			for(String id: ids.split(","))
			{
				skipSet.add(id);
			}
		}
		
		RefreshIncident ri = new RefreshIncident();
		// Refresh for each customer
		for(String custId: custIdList)
		{
			if(skipSet.contains(custId))
			{
				logger.info("Skipping customer "+custId);
				continue;
			}
			// If null date is passed, it means that we want the refresh to run for dates picked from the database, 
			fromDate = new BatchRefreshDAO().retrieveLastRefreshDate("Incident", custId, interval);
			if(null == fromDate)
				fromDate = "2015-01-01 00:00:00";
			ri.refreshIncident(custId, toDate, fromDate, interval);
		}
		
		new BatchRefreshDAO().refreshNetworkPerformance();
		logger.info("finished updating network performance");
		**/
		
	}
	
	public void refreshDataThread(){
		
		Thread t1 = new Thread(new Runnable(){

			@Override
			public void run() {
				getCustomerList();
			}
		});
		
		Thread t2 = new Thread(new Runnable(){

			@Override
			public void run() {
				getSkipList();
			}
		});
		
		Thread t3 = new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					refreshIncidentCust();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		});
		
		Thread t4 = new Thread(new Runnable(){

			@Override
			public void run() {
				refreshNetworkPerformance();
			}
		});
		
		t1.setName("Thread 1-getCustomerList");
		t2.setName("Thread 2-getSkipList");
		t3.setName("Thread 3-refreshIncidentCust");
		t4.setName("Thread 4-refreshNetworkPerformance");
		
		try {
			t1.start();
			t1.join();
			t2.start();
			t2.join();
			t3.start();
			t3.join();
			t4.start();
			t4.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void getCustomerList(){
		custIdList = new BatchRefreshDAO().getCustomerIdList();
	}
	
	public synchronized void getSkipList(){
		if(null!=ids)
		{
			for(String id: ids.split(","))
			{
				skipSet.add(id);
			}
		}
	}
	
	public synchronized void refreshIncidentCust() throws ParseException{
		for(String custId: custIdList)
		{
			if(skipSet.contains(custId))
			{
				logger.info("Skipping customer "+custId);
				continue;
			}
			// If null date is passed, it means that we want the refresh to run for dates picked from the database, 
			fromDate = new BatchRefreshDAO().retrieveLastRefreshDate("Incident", custId, interval);
			if(null == fromDate)
				fromDate = "2015-01-01 00:00:00";
			ri.refreshIncident(custId, toDate, fromDate, interval);
		}
	}
	
	public synchronized void refreshNetworkPerformance(){
		new BatchRefreshDAO().refreshNetworkPerformance();
		logger.info("finished updating network performance");
	}
	
	public void refreshDataPartial(String fromDate, String toDate, String interval, String entityType, String customerId) throws ParseException, IOException, JAXBException {
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.interval = "HOURLY";
		this.entityType = entityType;
		this.customerId = customerId;
		
		refreshDataPartialThread();
		
	}
	
	public void refreshDataPartialThread(){
		
		Thread t1 = new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					refreshIncidentPartialCust();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
		});
		
		Thread t2 = new Thread(new Runnable(){

			@Override
			public void run() {

				refreshNetworkPerformancePartial();
			}
			
		});
		
		t1.setName("Thread 1-refreshIncidentPartialCust : " + customerId);
		t2.setName("Thread 2-refreshNetworkPerformancePartial : " + customerId);
		
		try {
			t1.start();
			t1.join();
			t2.start();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public synchronized void refreshIncidentPartialCust() throws ParseException{

			// If null date is passed, it means that we want the refresh to run for dates picked from the database, 
			fromDate = new BatchRefreshDAO().retrieveLastRefreshDate("Incident", customerId, interval);
			if(null == fromDate){
				fromDate = "2015-01-01 00:00:00";
			}
				
			ri.refreshIncident(customerId, toDate, fromDate, interval);
	}
	
	public synchronized void refreshNetworkPerformancePartial(){
		new BatchRefreshDAO().refreshNetworkPerformancePartial(customerId);
		logger.info("finished updating network performance");
	}
	
	public static void main(String[] args){
		//String date = new BatchRefreshDAO().retrieveLastRefreshDate("Incident");
		String date = "2015-01-01 12:19:13";
		String customerId = "000151";
		System.out.println(date);
		
		try{
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			String toDate = df.format(dt);
			System.out.println(toDate);
			IncidentBatchRefresh cb = new IncidentBatchRefresh();
			//cb.refreshData(date, toDate, "", "");
			cb.refreshDataPartial(date, toDate, "", "", customerId);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
}
