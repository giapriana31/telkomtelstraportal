package com.tt.dao;

import java.text.ParseException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ChangeRequestCustomer;
import java.util.ArrayList;

public class ServiceRequestDao {

	/**
	 * This method does the following: <li>Change the status of all entries of
	 * that customer to 'S' <li>Make a new entry for this customer where last
	 * Refresh time = 'toDate' and status = 'X'
	 * 
	 * @param customerId
	 * @param toDate
	 * @throws ParseException
	 */
	public void createLastRefreshTimeEntry(String customerId, String toDate, String entityName, String interval)
			throws ParseException {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();

		Query query = session
				.createSQLQuery("UPDATE lastrefreshtime l set l.status='X' WHERE l.customeruniqueid=:customerId and l.entityname = :entityName and l.jobinterval = :interval");
		query.setString("customerId", customerId);
		query.setString("entityName", entityName);
		query.setString("interval", interval);
		query.executeUpdate();

		Query insert = session
				.createSQLQuery("Insert into lastrefreshtime (status,customeruniqueid,customername,entityname,lastrefreshtime,jobinterval) values ('A',?,'',?,?,?)");
		insert.setString(0, customerId);
		insert.setString(1, entityName);
		insert.setString(2, toDate);
		insert.setString(3, interval);
		insert.executeUpdate();

		txn.commit();
		session.close();
	}

	public void refreshServiceRequest(List<ServiceRequest> serviceRequestList) {

		deleteChildData(serviceRequestList);
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction txn = session.beginTransaction();
		for (ServiceRequest serviceRequest : serviceRequestList) {
			
			if(serviceRequest!=null && serviceRequest.getServiceRequestItem()!=null & serviceRequest.getServiceRequestItem().size()>0){
				for (ServiceRequestItem requestItem : serviceRequest.getServiceRequestItem()) {
					System.out.println("requestItem.getItemid()-----"+requestItem.getItemid());
					Query q = session.createQuery("delete from ServiceRequestItemDevices w where w.itemid='"+requestItem.getItemid()+"'");
					q.executeUpdate();
					Query q1 = session.createQuery("delete from ServiceRequestItemNotes w where w.itemid='"+requestItem.getItemid()+"'");
					q1.executeUpdate();
				}
			}
			
			session.merge(serviceRequest);
			// session.save(serviceRequest);
		}
		txn.commit();
		session.close();

	}

	private void deleteChildData(List<ServiceRequest> serviceRequestList) {
		
	}

	public void refreshChangeRequest (List<ChangeRequest> changeRequestList, String customerId){
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction txn = session.beginTransaction();
		for (ChangeRequest changeRequest : changeRequestList) {
			List<ChangeRequestCustomer> customerList = new ArrayList<ChangeRequestCustomer>();
			
			changeRequest.setChangeRequestCustomer(getChangeRequestCustomerList(changeRequest.getChangerequestid()));
			if(null != getChangeRequestCustomerList(changeRequest.getChangerequestid())){
				customerList.addAll(getChangeRequestCustomerList(changeRequest.getChangerequestid()));
			}else{
				ChangeRequestCustomer changeCustomer = new ChangeRequestCustomer();
				changeCustomer.setCustomeruniqueid(customerId);
				changeCustomer.setCustomername("");
				changeCustomer.setChangerequestid(changeRequest);
				customerList.add(changeCustomer);	
			}
			changeRequest.setChangeRequestCustomer(customerList);
			
			Query q = session.createQuery("delete from ChangeRequestDevices w where w.changerequestid='"+changeRequest.getChangerequestid()+"'");
			q.executeUpdate();
			Query q1 = session.createQuery("delete from ChangeRequestServices w where w.changerequestid='"+changeRequest.getChangerequestid()+"'");
			q1.executeUpdate();
			Query q3 = session.createQuery("delete from ChangeRequestNotes w where w.changerequestid='"+changeRequest.getChangerequestid()+"'");
			q3.executeUpdate();
			Query q2 = session.createQuery("delete from ChangeRequestCustomer w where w.changerequestid='"+changeRequest.getChangerequestid()+"'");
			q2.executeUpdate();
			Query q4 = session.createQuery("delete from ChangeRequestApprovals w where w.changerequestid='"+changeRequest.getChangerequestid()+"'");
			q4.executeUpdate();
			session.merge(changeRequest);
		}
		txn.commit();
		session.close();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<ChangeRequestCustomer> getChangeRequestCustomerList(String changeRequestId) {

		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction txn = session.beginTransaction();
		Query q = session.createQuery("From ChangeRequestCustomer w where w.changerequestid='"+changeRequestId+"'");
		List<ChangeRequestCustomer> results =  (List<ChangeRequestCustomer>)q.list();
		txn.commit();
		session.close();
		
		if(results!=null && results.size()>0){
			return (List<ChangeRequestCustomer>)results;
		}else {
			return null;
		}

		
	}
}
