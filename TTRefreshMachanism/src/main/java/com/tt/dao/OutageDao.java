package com.tt.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tt.model.Outage;

public class OutageDao 
{
	public void insertOutageData(List<Outage> outageList) 
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		
		for(Outage outage : outageList)
		{
			session.merge(outage);
		}
		txn.commit();
		session.close();
	}
}
