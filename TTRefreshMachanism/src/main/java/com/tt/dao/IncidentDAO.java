/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys			15-JUL-2015			Initial Version
 * 
 * 
 * **************************************************************************************************************
 *
 * Modified by TT
 */


package com.tt.dao;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

import com.tt.batchrefresh.model.ci.Incident;
import com.tt.batchrefresh.model.ci.IncidentToDevice;
import com.tt.batchrefresh.model.ci.IncidentToService;
import com.tt.common.RefreshUtil;

/** This class handles all database related operations for incidents data for batch jobs
 * @author Infosys
 * @version 1.0
 */
@Transactional
public class IncidentDAO {

	private static final Logger logger = Logger.getLogger(IncidentDAO.class.getName());
	
	public void refreshIncidents(List<Incident> incList) {
		/*
		for (Incident inc : incList){
			incPersistence(inc);
		}
		*/
		
		/*
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction txn = session.beginTransaction();
		for (Incident inc : incList)
		{
			session.createSQLQuery(" DELETE FROM incdnotes WHERE incidentid = '"+inc.getIncidentid()+"'").executeUpdate();
			session.createSQLQuery(" DELETE FROM incdtoservicemapping WHERE incidentid = '"+inc.getIncidentid()+"'").executeUpdate();
			session.createSQLQuery(" DELETE FROM incdtodevicemapping WHERE incidentid = '"+inc.getIncidentid()+"'").executeUpdate();
			session.merge(inc);
			
		}
		txn.commit();
		session.close();
		*/
		
		Session session = null;
		Transaction tx = null;
		SQLQuery sqlQuery = null;
		
		for (Incident inc : incList) {
			
			try{
				session = HibernateUtil.getSessionFactory().openSession();
				tx = session.beginTransaction();
				
				sqlQuery = session.createSQLQuery("call delete_inc_related_tables(:incidentId)");
				sqlQuery.setParameter("incidentId", inc.getIncidentid()).executeUpdate();
				session.flush();
				tx.commit();
				
				tx.begin();
				session.merge(inc);	
				session.flush();
				tx.commit();
			} catch(Exception e) {
				if (tx != null) tx.rollback();  
				logger.error("Error on transaction Incident Id : " + inc.getIncidentid() + " , Customer : " + inc.getCustomeruniqueid());
			    e.printStackTrace();
			} finally {
				session.close();
				
				session = null;
				tx = null;
				sqlQuery = null;
			}
		}
		
	}
	
	public void incPersistence(Incident inc){
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction tx = null;
		
		try{
			tx = session.beginTransaction();
		
			session.createSQLQuery(" DELETE FROM incdnotes WHERE incidentid = '"+inc.getIncidentid()+"'").executeUpdate();
			session.createSQLQuery(" DELETE FROM incdtoservicemapping WHERE incidentid = '"+inc.getIncidentid()+"'").executeUpdate();
			session.createSQLQuery(" DELETE FROM incdtodevicemapping WHERE incidentid = '"+inc.getIncidentid()+"'").executeUpdate();
			session.merge(inc);
			
			tx.commit();
		} catch(HibernateException e) {
			tx.rollback();  
			logger.error("Error on transaction Incident Id : " + inc.getIncidentid() + " , Customer : " + inc.getCustomeruniqueid());
		    throw e;
		    
		} finally {
			session.close();
		}
	}

	/**
	 *  This table calls the 'POPULATE_METRICS_DATA' stored procedure to refresh
	 * the metrics data.<BR>
	 * POPULATE_METRICS_DATA accepts a list of incidentIds as input
	 * @param incList List of incidents whose data is to be refreshed
	 */
	public void refreshMetrics(List<Incident> incList)
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL POPULATE_METRICS_DATA(:incidentId)");

		Transaction txn = session.beginTransaction();
		for (Incident inc : incList)
		{
			sqlQuery.setParameter("incidentId", inc.getIncidentid()).executeUpdate();
		}

		txn.commit();
		session.close();

	}

	/**
	 * This table calls the 'POPULATE_METRICS_DATA_BYLASTUPDATEDDATE' stored procedure to refresh
	 * the metrics data.<BR>
	 * POPULATE_METRICS_DATA_BYLASTUPDATEDDATE accepts the last updated date
	 * 
	 * @param fromDate  
	 *            
	 */
	public void refreshMetricsFromDate(List<Incident> incList, String customerId, String fromDateStr)  throws ParseException {

		Session session = HibernateUtil.getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery("CALL POPULATE_METRICS_DATA_BYDATE_OR_INCLIST(null,null,:fromDate,:customerId)");
		Transaction txn = session.beginTransaction();
		
		Date fromDate = RefreshUtil.convertStringToDate(fromDateStr);
		
		sqlQuery.setParameter("fromDate", fromDate);
		sqlQuery.setParameter("customerId", customerId);
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
		
		session = null;
		sqlQuery = null;
		txn = null;
		
	}

	/**
	 * This method calls procedure customernetworksites_populate
	 */
	public void refreshNetworkSites()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();

		SQLQuery sqlQuery = session.createSQLQuery("CALL customernetworksites_populate()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
		
		session = null;
		sqlQuery = null;
		txn = null;
	}
	
	/**
	 * This method calls procedure calculate_netwrk_perform
	 */
	public void calculateNetworkPerform()
	{
		Session session = HibernateUtil.getSessionFactory().openSession();

		SQLQuery sqlQuery = session.createSQLQuery("CALL calculate_netwrk_perform()");
		Transaction txn = session.beginTransaction();
		sqlQuery.executeUpdate();

		txn.commit();
		session.close();
		
		session = null;
		sqlQuery = null;
		txn = null;
	}



	/**
	 * This method does the following: <li>Change the status of all entries of
	 * that customer to 'S' <li>Make a new entry for this customer where last
	 * Refresh time = 'toDate' and status = 'X'
	 * 
	 * @param customerId
	 * @param toDate
	 * @throws ParseException
	 */
	public void createLastRefreshTimeEntry(String customerId, String toDate, String interval) throws ParseException
	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();

		Query query = session.createSQLQuery("UPDATE lastrefreshtime l set l.status='X' WHERE l.customeruniqueid=:customerId and l.entityname='Incident' and l.jobinterval=:interval");
		query.setString("customerId", customerId);
		query.setString("interval", interval);
		query.executeUpdate();

		Query insert = session.createSQLQuery("Insert into lastrefreshtime (status,customeruniqueid,customername,entityname,lastrefreshtime, jobinterval) values('A',:customerId,'','Incident',:lastrefreshtime, :interval)");
		insert.setString("customerId", customerId);
		insert.setString("lastrefreshtime", toDate);
		insert.setString("interval", interval);
		insert.executeUpdate();
		txn.commit();
		session.close();
	}

	public void saveDeviceServiceMapping(List<Incident> incidentList) {
		
		Session session = HibernateUtil.getSessionFactory().openSession();

		Transaction txn = session.beginTransaction();
		for (Incident inc : incidentList)
		{
			if(inc.getIncidentToDeviceList()!=null && inc.getIncidentToDeviceList().size() >0){
				for (IncidentToDevice device : inc.getIncidentToDeviceList()){
					session.merge(device);
				} 
			}
			if(inc.getIncidentToServiceList()!=null && inc.getIncidentToServiceList().size() >0){
				for (IncidentToService service : inc.getIncidentToServiceList()){
					session.merge(service);
				}
			}
		}
		txn.commit();
		session.close();

		
	}
}
