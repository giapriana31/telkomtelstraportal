package com.tt.dao;

import java.util.HashMap;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.tt.model.SWReportImages;
import com.tt.model.SWReports;

public class SWReportsDao {
	
	public String saveReports(SWReports reports){
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String	id=(String)session.save(reports);
		txn.commit();
		session.close();
		
		
		
		
		return id;
	}
	
	public void deleteCategory(String category ,String customerId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		int id=session.createSQLQuery("Delete From swreports Where LOWER(report_category)= LOWER('"+category+"') And customerId= '"+customerId+"'") .executeUpdate();
		System.out.println("How many files deleted "+id);
		txn.commit();
		session.close();
		
	}
	
	public void deleteReportImages(String category ,String customerId){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		int id=session.createSQLQuery("Delete From swreportimages Where LOWER(category)= LOWER('"+category+"') And customerId= '"+customerId+"'") .executeUpdate();
		System.out.println("How many image files deleted "+id);
		txn.commit();
		session.close();
		
		
	}
	
	public HashMap<String, String> getCustomerNameAndId(){
		
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();

		SQLQuery query=session.createSQLQuery("Select customerUniqueId,accountName from customers");
		List<Object[]> customersDetails=query.list();
		HashMap<String, String> customerMapping=new HashMap<String, String>();
		if (!customersDetails.isEmpty() &&  customersDetails!=null) {
			for (Object[] objects : customersDetails) {
				
			customerMapping.put((String)objects[1], (String)objects[0]);	

			}
		}
		txn.commit();
		session.close();
		
		return customerMapping;
		
		
	}
	
	
	public String saveImageToDB(SWReportImages swReportImages){
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txn = session.beginTransaction();
		String	id=(String)session.save(swReportImages);
		txn.commit();
		session.close();
		return id;
	}


}
