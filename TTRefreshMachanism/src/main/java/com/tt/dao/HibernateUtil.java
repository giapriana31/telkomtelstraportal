package com.tt.dao;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/** Singleton class to generate an instance of SessionFactory
 * @author Infosys
 *
 */
public class HibernateUtil
{
	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static final Logger logger = LogManager.getLogger(HibernateUtil.class);
	
    private static SessionFactory buildSessionFactory()
    {
        try
        {
            // Create the SessionFactory from hibernate.cfg.xml
        	Configuration configuration = new Configuration();
    		configuration.configure();

    		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().
    		applySettings(configuration.getProperties()).buildServiceRegistry();
    		SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    		return sessionFactory;

        }
        catch (Throwable ex)
        {
        	ex.printStackTrace();
            logger.error("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    /**
     * @return instance of the sessionFactory object
     */
    public static SessionFactory getSessionFactory()
    {
        if(sessionFactory == null)
            return buildSessionFactory();
        return sessionFactory;
    }
}
