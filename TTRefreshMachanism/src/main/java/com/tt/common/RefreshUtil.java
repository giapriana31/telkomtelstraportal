/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys			20-JUL-2015			Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.Period;

/**
 * Utility class
 * 
 * @author Infosys
 * @version 1.0
 */
public class RefreshUtil {
    private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Converts String to Date
     * 
     * @param dateString
     * @return
     * @throws ParseException
     */
    public static Date convertStringToDate(String dateString)  throws ParseException {
	Date date = dateString == null || dateString.equals("") ? null: FORMATTER.parse(dateString);
	return date;
    }
    
    public static Date convertStringToOnlyDate(String dateString)  throws ParseException {
	Date date = dateString == null || dateString.equals("") ? null: DATE_FORMATTER.parse(dateString);
	return date;
    }
    
    public static String getDateDifference(String fromDateString, String toDateString)  throws ParseException {
    	Date fromDate = fromDateString == null || fromDateString.equals("") ? null: DATE_FORMATTER.parse(fromDateString);
    	Date toDate = toDateString == null || toDateString.equals("") ? null: DATE_FORMATTER.parse(toDateString);
    	Period p = new Period(fromDate.getTime(), toDate.getTime());
    	System.out.println("You are " + p.getYears() + " years, " + p.getMonths() + " months, and " + p.getDays());
    	String duration =  p.getYears() + " Years, " + p.getMonths() + " Months, " + p.getDays() + " Days";
    	return duration;
    }
}
