package com.tt.common;



import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import com.tt.SWReportData;
import com.tt.SWReportList;
import com.tt.utils.PropertyReader;

public class FSFileRead {

	
	
	
	
	
		public  ArrayList<SWReportData> getReportData(String directoryNode){

			ArrayList<SWReportData>  swReportDatas= new ArrayList<SWReportData>();
			//File file =new File(directoryNode);
			Properties prop = PropertyReader.getProperties();

			File file = new File(directoryNode);
			File[] list=null;
			
			

			if(file.isDirectory()){
				
				 list= file.listFiles();
				
			
			}
			
			for (File fileRead : list) {
				
				SWReportData data= new SWReportData();
				data.setCustomerName(fileRead.getName());
				//data.setCustomerId(fileRead.getName());
				HashMap<String, File[]> map = new HashMap<String, File[]>();
				if(fileRead.isDirectory()){
		
					File[] tempList=fileRead.listFiles();
					
					for (File file2 : tempList) {
					

						map.put(file2.getName(), file2.listFiles());
						
					}
					data.setReportFile(map);
				}
				
				swReportDatas.add(data);
				
			}
			return swReportDatas;
		}
		
			
		

}
