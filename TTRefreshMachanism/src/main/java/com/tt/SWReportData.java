package com.tt;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;



public class SWReportData {
	
	
	private String customerName;
	private HashMap<String, File[]> reportFile;
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public HashMap<String, File[]> getReportFile() {
		return reportFile;
	}
	public void setReportFile(HashMap<String, File[]> reportFile) {
		this.reportFile = reportFile;
	}
	
	
	

}
