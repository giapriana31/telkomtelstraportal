/*window.SLA = window.SLA||{};
window.OfficeBuilding = window.OfficeBuilding||{};
window.IJVGraphs = window.IJVGraphs||{};


IJVGraphs.createCurve = function(container,startX,startY,radius,thickness,angle,color){
    var ax = startX;
    var ay = startY;
    var centerX = startX + radius;
    var shortRadius = radius - thickness;
    var centerY = startY;
    var anglePI = ((180-angle)*Math.PI)/180;
    var bx = centerX + radius * Math.cos(anglePI);
    var by = centerY - radius * Math.sin(anglePI);
    var cx = centerX + shortRadius * Math.cos(anglePI);
    var cy = centerY - shortRadius * Math.sin(anglePI);
    var dx = ax + thickness;
    var dy = ay;
    
    var curve = document.createElementNS("http://www.w3.org/2000/svg", "path");
    curve.setAttribute('d', "M "+dx+" "+dy+" L "+ax+" "+ay+" A "+radius+" "+radius+" 0 0,1 "+bx+","+by+" L "+cx+" "+cy+" A "+shortRadius+" "+shortRadius+" 0 0,0 "+dx+","+dy+" z");
    curve.setAttribute('style', "stroke:"+color+"; fill:"+color+";");
    container.append(curve);
}

IJVGraphs.createRectangle = function (container,x,y,w,h,fillColor,strokeColor){
    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
    rect.setAttribute('x', x);
    rect.setAttribute('y', y);
    rect.setAttribute('rx', 0);
    rect.setAttribute('ry', 0);
    rect.setAttribute('width', w);
    rect.setAttribute('height', h);
    rect.setAttribute('style', "stroke:"+strokeColor+"; fill:"+fillColor+";");
    container.append(rect);
}

IJVGraphs.wrapLine=function(textnode) {

    var x_pos = parseInt(textnode.getAttribute('x')),
    y_pos = parseInt(textnode.getAttribute('y')),
    boxwidth = parseInt(textnode.getAttribute('width')),
    fz = parseInt(window.getComputedStyle(textnode)['font-size']); 
    var line_height = fz;
    var wrapping = textnode.cloneNode(false);
    wrapping.setAttributeNS(null, 'x', x_pos);
    wrapping.setAttributeNS(null, 'y', y_pos);

    var testing = wrapping.cloneNode(false);
    testing.setAttributeNS(null, 'visibility', 'hidden');
    var tester = document.getElementsByTagName('svg')    [0].appendChild(testing);

    var words = textnode.textContent.split(" ");
    var line = line2 = "";
    var linecounter = 0;
    var testwidth;

    for (var n = 0; n < words.length; n++) {
      line2 = line + words[n] + " ";
      testing.textContent = line2;
     testwidth = testing.getBBox().width;

    if (testwidth > boxwidth)
    {
        testingTSPAN = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
        testingTSPAN.setAttributeNS(null, 'x', x_pos);
        testingTSPAN.setAttributeNS(null, 'dy', line_height);
        testingTEXTNODE = document.createTextNode(line.trim());
        testingTSPAN.appendChild(testingTEXTNODE);
        wrapping.appendChild(testingTSPAN);
        line = words[n] + " ";
        linecounter++;
    }
    else {
        line = line2;
    }
}
    var testingTSPAN = document.createElementNS('http://www.w3.org/2000/svg', 'tspan');
    testingTSPAN.setAttributeNS(null, 'x', x_pos);
    testingTSPAN.setAttributeNS(null, 'dy', line_height);

    var testingTEXTNODE = document.createTextNode(line);
    testingTSPAN.appendChild(testingTEXTNODE);

    wrapping.appendChild(testingTSPAN);

    testing.parentNode.removeChild(testing);
    textnode.parentNode.replaceChild(wrapping,textnode);
    wrapping.setAttributeNS(null, 'y', y_pos - ((linecounter+1) * fz));
    return linecounter+1;
}

IJVGraphs.createText = function(container,x,y,w,h,fillColor,color,text,fontSize,align,wrap){
    var id = "_id_"+Math.random();
    var textControl = document.createElementNS("http://www.w3.org/2000/svg", "text");
                textControl.setAttribute('id', id);
    textControl.setAttribute('x', x);
    textControl.setAttribute('y', y);
    textControl.setAttribute('width', w);
    textControl.setAttribute('height', h);
    textControl.setAttribute('rx', 0);
    textControl.setAttribute('ry', 0);
    textControl.setAttribute('style', "color:"+color+";cursor:default;font-size:"+fontSize+";fill:"+fillColor+";text-overflow:clip;text-anchor:"+align+";");
                textControl.textContent = text;
    container.append(textControl);
    if(wrap){
    	return IJVGraphs.wrapLine(document.getElementById(id)); 
    }
    else
    	return 1;
}

OfficeBuilding.draw =function (options){
	var container = $('#'+options.containerid);
	if(options.widthAutoAdjust)
	{
		container.empty();
		if(window.attachEvent) {
		    window.attachEvent('onresize', function() {
		        OfficeBuilding.draw(options);
		    });
		}
		else if(window.addEventListener) {
		    window.addEventListener('resize', function() {
		    	OfficeBuilding.draw(options);
		    }, true);
		}
	}
	var xaxis = options.xaxis;
	var yaxis = options.yaxis;
	var yAxisColors = options.yAxisColors;
	var data = options.data;
	var barAreaPct = options.barAreaPct;
	var xOffSet = options.xOffSet;
	var labelXAxis = options.labelXAxis;
	var labelXAxisSize = 13;
	var labelBarTotalSize = 15;
	var labelBarsSize = 13;
	var labelBarColor = '#777';
	if(options.labelBarColor)
		labelBarColor = options.labelBarColor;
	if(options.labelXAxisSize)
		labelXAxisSize = options.labelXAxisSize;
	if(options.labelBarTotalSize)
		labelBarTotalSize = options.labelBarTotalSize;
	if(options.labelBarsSize)
		labelBarsSize = options.labelBarsSize;
	var labelBarTotal = options.labelBarTotal;
	var labelBars = options.labelBars;
    var width = container.width();
    var height = container.height();
    var allBarWidth = width * barAreaPct / 100;
    var noOfBars = xaxis.length;
    var barWidth = parseFloat((allBarWidth/noOfBars).toFixed(2));
    var allGapWidth = width - (barWidth * noOfBars);
    var gapWidth = parseFloat((allGapWidth/(noOfBars+1)).toFixed(2));
    var barStartsX = Array();
    var x = gapWidth+xOffSet;
    for(var i = 0; i < xaxis.length; i++)
    {
        barStartsX[i] = x;
        x += barWidth + gapWidth;
    }
    var sum = new Array();
    for(var j = 0; j < xaxis.length; j++){
            sum[j]=0;
    }
    for(var i = 0; i < data.length; i++){
        var row = data[i];
        for(var j = 0; j < row.length; j++){
            var d = row[j];
            sum[j]+=d;
        }
    }
    var inverseData = Array();
    for(var i = 0; i < data.length; i++){
        inverseData[i] = data[data.length-i-1];
    }
    var inverseColors = Array();
    for(var i = 0; i < yaxis.length; i++){
        inverseColors[i] = yAxisColors[yaxis.length-i-1];
    }
    var pctData = new Array();
    data = inverseData;
    for(var i = 0; i < data.length; i++){
        var row = data[i];
        pctData[i] = new Array();
        for(var j = 0; j < row.length; j++){
            var d = row[j];
            pctData[i][j] = d/sum[j];
        }
    }
    var barStartsY = Array();
    for(var i = 0; i < pctData.length; i++){
        var row = pctData[i];
        barStartsY[i] = new Array();
        for(var j = 0; j < row.length; j++){
            barStartsY[i][j] = 0;
        }
    }
    for(var i = 1; i < pctData.length; i++){
        var row = pctData[i-1];
        for(var j = 0; j < row.length; j++){
            var d = row[j];
            barStartsY[i][j] = (barStartsY[i-1][j] + (height * d));
        }
    }
    var svg = '<svg version="1.1" style="font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="'+width+'" height="'+height+'"></svg>';
    container.append(svg);
    container = $('#'+options.containerid+' svg');
    for(var i = 0; i < data.length; i++){
        var row = data[i];
        
        for(var j = 0; j < row.length; j++){
            var x = barStartsX[j];
            var y = barStartsY[i][j];
            var w = barWidth;
            var h = pctData[i][j] * height;
            var color = inverseColors[i];
            IJVGraphs.createRectangle(container,x,y,w,h,color,'#FFFFFF');
            if(row[j]>0 && labelBars)
				IJVGraphs.createText(container,x+w+2,y+labelBarsSize,20,20,color,color,row[j],labelBarsSize+'px','start');
        }
    }
    IJVGraphs.createRectangle(container,0,height,width,1,'#aaa','#aaa');
    for(var i = 0; i < xaxis.length; i++)
    {
        var w = gapWidth-10;
        var x = barStartsX[i]-2
        var y = height-5;
        var h = 20;
        var text = xaxis[i];
        var color = labelBarColor;
        var count = sum[i];
        var lines = 2;
        if(labelXAxis)
        	lines = IJVGraphs.createText(container,x,y,w,h,labelBarColor,color,text,labelXAxisSize+'px','end',true);
        if(labelBarTotal)
			IJVGraphs.createText(container,x,y-(labelXAxisSize+3)*lines,w,h,labelBarColor,color,count,labelBarTotalSize+'px','end');
    }
}

SLA.draw = function(options){
	var container = $('#'+options.containerid);
    var width = container.width();
    var height = container.height();
    var radius = height - 20;
    var dia = width - 20;
    if(radius < dia/2)
        dia = 2 * radius;
    else if (dia < radius * 2)
        radius = dia/2;
    var cx = 10+radius;
    var cy = height - 10;
    var startX = cx - radius;
    var startY = cy;
    var angle = options.percent * 180 / 100;
    var backColor = '#FFFFFF';
    for(var i = 0; i < options.backColors.length;i++)
    {
        var tier = options.backColors[i];
        if(options.percent >= tier.min && options.percent <= tier.max)
        {
            backColor = tier.color;
        }
    }
    var svg = '<svg version="1.1" style="font-size:12px;" xmlns="http://www.w3.org/2000/svg" width="'+width+'" height="'+height+'"></svg>';
    container.append(svg);
    container = $('#'+options.containerid+' svg');
	IJVGraphs.createRectangle(container,0,0,width,height,backColor,backColor); 
	IJVGraphs.createCurve(container,startX,startY,radius,options.thickness,180,backColor);
	IJVGraphs.createCurve(container,startX,startY,radius,options.thickness,angle,options.foreColor);
	IJVGraphs.createText(container,cx,cy,20,20,options.foreColor,options.foreColor,options.percent+'%','20px','middle');
	IJVGraphs.createText(container,10,20,20,20,options.foreColor,options.foreColor,'SLA','15px','start');
}*/