
var colors=["blue","red","green","yellow","brown","darkGrey","pink","black","violet","indigo","orange","white"];
var sectionServices= {"tt-mnsmonthly":"MNSMon","tt-mnsquarterly":"MNSQtr","tt-mnsyearly":"MNSYr","tt-cloudmonthly":"CloudMon","tt-cloudquarterly":"CloudQtr","tt-cloudyearly":"CloudYr","tt-saasmonthly":"SaaSMon","tt-saasquarterly":"SaaSQtr","tt-saasyearly":"SaaSYr","tt-ucmonthly":"UCMon","tt-ucquarterly":"UCQtr","tt-ucyearly":"UCYr","tt-managesecuritymonthly":"ManageSecurityMon","tt-managesecurityquarterly":"ManageSecurityQtr","tt-managesecurityyearly":"ManageSecurityYr"};
var engBhsTranslation= {"MONTHLY":"BULANAN","INCIDENT":"INSIDEN","DELIVERY":"PENGIRIMAN","QUARTERLY":"TRIWULANAN","YEARLY":"TAHUNAN","Order Received":"Pesanan Diterima","Detailed Design":"Desain yang rinci","Procurement":"Pembelian","Delivery Activation":"Pengiriman Aktivasi","Monitoring":"Pemantauan","Operational":"Operasional","MNS":"MNS","Cloud":"Cloud","SaaS":"SaaS","UC":"UC","Network":"Network","CPE":"CPE","January":"Januari","February":"Februari",
		"March":"Maret",
		"April":"April",
		"May":"Mei",
		"June":"Juni",
		"July":"Juli",
		"August":"Agustus",
		"September":"September",
		"October":"Oktober",
		"November":"November",
		"December":"Desember",
		"Quarter1":"Kuartal1",
		"Quarter2":"Kuartal2",
		"Quarter3":"Kuartal3",
		"Quarter4":"Kuartal4"
		};

var selAll=false;
var selInc= false;

function renderDualBarGraph(dataset,section){
	var w = 150;
	var h = 150;
	
	

	var xScale = d3.scale.ordinal()
					.domain(d3.range(dataset.length))
					.rangeRoundBands([0, w], 0); 

	var yScale = d3.scale.linear()
					.domain([0, d3.max(dataset, function(d) {return +d.value;})])
					.range([0, h]);

	var key = function(d) {
		return d.key;
	};

	//Create SVG element
	var svg = d3.select("#"+section)
				.append("svg")
				.attr("width", w)
				.attr("height", h);

				
	svg.selectAll("text")
	   .data(dataset, key)
	   .enter()
	   .append("text")
	   .text(function(d) {
			if(dataset.indexOf(d)%2!=0){
				if(d.value=="0"){
					return "";
				}
				else{
					return d.value;
				}
			}
			else if(dataset.indexOf(d)%2==0){
				if(dataset[dataset.indexOf(d)+1].value=="0"){
					return "";
				}
				else{
					return d.value;
				}
			}
			else{
				return d.value;
			}
			
			
	   })
	   .attr("text-anchor", "middle")
	   .attr("x", function(d, i) {
			return (xScale(i)/1.2)+5;
	   })
	   .attr("y", function(d) {
			return h - yScale(d.value) - 5;
			
	   })
	   .attr("font-family", "sans-serif") 
	   .attr("font-size", "11px")
	   .attr("fill", "black");
	
	//Create bars
	svg.selectAll("rect")
	   .data(dataset, key)
	   .enter()
	   .append("rect")
	   .attr("x", function(d, i) {
			return xScale(i)/1.2;
	   })
	   .attr("y", function(d) {
			return h - yScale(d.value);
	   })
	   .attr("width", xScale.rangeBand()/1.2)
	   .attr("height", function(d) {
			return yScale(d.value);
	   })
	   
	   .attr("style", function(d,i) {
			if(dataset.indexOf(d)%2==0){
				return "fill:"+colors[(dataset.indexOf(d))/2]+";stroke-width:1;stroke:yellow";
			}
			else {
				return "fill:"+colors[(dataset.indexOf(d)-1)/2]+";stroke-width:1;stroke:yellow";
			}
	   })
	   

		//Tooltip
		.on("mouseover", function(d) {
			//Get this bar's x/y values, then augment for the tooltip
			var xPosition = parseFloat(d3.select(this).attr("x")) + xScale.rangeBand() / 2;
			var yPosition = parseFloat(d3.select(this).attr("y")) + 14;
			
			//Update Tooltip Position & value
			d3.select("#tooltip")
				.style("left", xPosition + "px")
				.style("top", yPosition + "px")
				.select("#value")
				.text(d.value);
			d3.select("#tooltip").classed("hidden", false)
		})
		.on("mouseout", function() {
			//Remove the tooltip
			d3.select("#tooltip").classed("hidden", true);
		})
		.append("title")
	   .append("text")
	   .text(function(d) {
			if(dataset.indexOf(d)%2==0){
				if(isBahasa){
					return 'Layanan: '+engBhsTranslation[d.key]+' Bacaan: '+d.value;			}
				else{
					return 'Service: '+d.key+' Reading: '+d.value;
				}
			}
	   })
}

function renderSingleBarGraph(dataset,section){


	var w = 150;
	var h = 150;

	var xScale = d3.scale.ordinal()
					.domain(d3.range(dataset.length))
					.rangeRoundBands([0, w], 0.5); 

	var yScale = d3.scale.linear()
					.domain([0, d3.max(dataset, function(d) {return +d.value;})])
					.range([0, h]);

	var key = function(d) {
		return d.key;
	};
	
	//Create SVG element
	var svg = d3.select("#"+section)
				.append("svg")
				.attr("width", w)
				.attr("height", h);

				
	svg.selectAll("text")
	   .data(dataset, key)
	   .enter()
	   .append("text")
	   .text(function(d) {
			return d.value;
			
	   })
	   .attr("text-anchor", "middle")
	   .attr("x", function(d, i) {
			return xScale(i)+5;
	   })
	   .attr("y", function(d) {
			return h - yScale(d.value) - 5;
			
	   })
	   .attr("font-family", "sans-serif") 
	   .attr("font-size", "11px")
	   .attr("fill", "black");
	
	//Create bars
	svg.selectAll("rect")
	   .data(dataset, key)
	   .enter()
	   .append("rect")
	   .attr("x", function(d, i) {
			return xScale(i);
	   })
	   .attr("y", function(d) {
			return h - yScale(d.value);
	   })
	   .attr("width", 10)
	   .attr("height", function(d) {
			return yScale(d.value);
	   })
	   .attr("style", function(d) {
			return "fill:"+colors[dataset.indexOf(d)];
	   })
	   

		//Tooltip
		.on("mouseover", function(d) {
			//Get this bar's x/y values, then augment for the tooltip
			var xPosition = parseFloat(d3.select(this).attr("x")) + xScale.rangeBand() / 2;
			var yPosition = parseFloat(d3.select(this).attr("y")) + 14;
			
			//Update Tooltip Position & value
			d3.select("#tooltip")
				.style("left", xPosition + "px")
				.style("top", yPosition + "px")
				.select("#value")
				.text(d.value);
			d3.select("#tooltip").classed("hidden", false)
		})
		.on("mouseout", function() {
			//Remove the tooltip
			d3.select("#tooltip").classed("hidden", true);
		})
		.append("title")
	   .append("text")
	   .text(function(d) {
		   if(isBahasa){
				return 'Layanan: '+engBhsTranslation[d.key]+' Bacaan: '+d.value;			}
			else{
				return 'Service: '+d.key+' Reading: '+d.value;
			}
			
	   })
}



	//ToDo
	//Call an ajax to get the actual ServDelvStatus values from server
	//if not null then set ServDelvStatus to actual else do noting with ServDelvStatus

	function drawSpeedometer(data){
		var ServDelvStatus = jQuery.parseJSON(JSON.stringify(data));
		var dsSpdList = new Array();
		dsSpdList = $("#tt-speedometer").children();
		
		//Iterating Divs id as item value where item=service name and item2= SpeedometerM/Q/Y
		
		$(dsSpdList).each(function(index, servicename){
		  
		var dsSpdList2 = $(servicename).children();
		//alert(servicename.id);
		var ServNameReceived = ServDelvStatus != null ? ServDelvStatus[servicename.id] : {};
		
		   $(dsSpdList2).each(function(index, snMQY) {
			   //alert(snMQY.id);
			 
			   if (!(($(this).hasClass('tt-servicetypelabel')) || this.id=='togglelabel')){
			   var PerMQYReceivedForServName = ServNameReceived[sectionServices[snMQY.id]] || null;
		        //alert(PerMQYReceivedForServName);
			   
			   var spData = {
			      percent: PerMQYReceivedForServName,
			      divId: snMQY.id
			    };
		
			    ttSpeedometer($, spData);
				}
		   });
	});
	};

	function ttSpeedometer($, spData, percent) {

	  var Needle, arc, arcEndRad, arcStartRad, barWidth, chart, chartInset, degToRad, el, endPadRad, height, margin, needle, numSections, padRad, percToDeg, percToRad, percent = spData.percent,
	    radius, sectionIndx, sectionPerc, startPadRad, svg, totalPercent, width, _i;
	  //console.log(spData.percent);

	  barWidth = 15;
	  numSections = 3;
	  sectionPerc = 1 / numSections / 2;
	  padRad = 0.00;
	  chartInset = 10;
	  totalPercent = .75;
	  
	  $('#' + spData.divId + " svg").remove();
	  el = d3.select('#' + spData.divId);
	  margin = {
	    top: 50,
	    right: 0,
	    bottom: 0,
	    left: 0
	  };

	  width = 85;
	  height = 85;
	  //width = el[0][0].offsetWidth + margin.left;
	  //height = el[0][0].offsetWidth + margin.top;
	  //radius = Math.min(width, height) / 2;
	  radius = 50;

	  percToDeg = function(perc) {
	    return perc * 360;
	  };

	  percToRad = function(perc) {
	    return degToRad(percToDeg(perc));
	  };

	  degToRad = function(deg) {
	    return deg * Math.PI / 180;
	  };

	  svg = el.append('svg').attr('width', width + margin.left + margin.right).attr('height', height);

	  chart = svg.append('g').attr('transform', "translate(" + ((width / 2) + margin.left) + ", " + margin.top + ")");

	  for (sectionIndx = _i = 1; 1 <= numSections ? _i <= numSections : _i >= numSections; sectionIndx = 1 <= numSections ? ++_i : --_i) {
	    arcStartRad = percToRad(totalPercent);
	    arcEndRad = arcStartRad + percToRad(sectionPerc);
	    totalPercent += sectionPerc;
	    startPadRad = sectionIndx === 0 ? 0 : padRad / 2;
	    endPadRad = sectionIndx === numSections ? 0 : padRad / 2;
	    arc = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth).startAngle(arcStartRad + startPadRad).endAngle(arcEndRad - endPadRad);
		
			chart.append('path').attr('class', "arc chart-color" + sectionIndx).attr('d', arc);
		
	  }

	  Needle = (function() {
	    function Needle(len, radius) {
	      this.len = len;
	      this.radius = radius;
	    }

	    Needle.prototype.drawOn = function(el, perc) {
	      el.append('circle').attr('class', 'needle-center').attr('cx', 0).attr('cy', 0).attr('r', this.radius);
	      return el.append('path').attr('class', 'needle').attr('d', this.mkCmd(perc));
	    };

	    Needle.prototype.animateOn = function(el, perc) {
	      var self;
	      self = this;
	      return el.transition().delay(0).ease('elastic').duration(4000).selectAll('.needle').tween('progress', function() {
	        return function(percentOfPercent) {
	          var progress;
	          progress = percentOfPercent * perc;
	          return d3.select(this).attr('d', self.mkCmd(progress));
	        };
	      });
	    };

	    Needle.prototype.mkCmd = function(perc) {
	      var centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;
	      thetaRad = percToRad(perc / 2);
	      centerX = 0;
	      centerY = 0;
	      topX = centerX - this.len * Math.cos(thetaRad);
	      topY = centerY - this.len * Math.sin(thetaRad);
	      leftX = centerX - this.radius * Math.cos(thetaRad - Math.PI / 2);
	      leftY = centerY - this.radius * Math.sin(thetaRad - Math.PI / 2);
	      rightX = centerX - this.radius * Math.cos(thetaRad + Math.PI / 2);
	      rightY = centerY - this.radius * Math.sin(thetaRad + Math.PI / 2);
	      return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;
	    };

	    return Needle;

	  })();
	  
	if(percent!=null){
	  needle = new Needle(40, 4);

	  needle.drawOn(chart, 0);

	  needle.animateOn(chart, percent);
	}
	else{
		needle = new Needle(1, 4);

	  needle.drawOn(chart, 0);
	}
	};