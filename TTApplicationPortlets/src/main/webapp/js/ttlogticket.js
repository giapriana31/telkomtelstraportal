	function addCommentNotes(element){
		if(element.checked){
			 var textbox = document.getElementById("dashTickNotesTextArea");
			 textbox.value =  "Multiple services are impacted. " + textbox.value;
		}
		else{
			var textbox = document.getElementById("dashTickNotesTextArea");
			textbox.value =  "";
		}
	}

	$(document).ready( function () {
		$('#LogIncidentModal').css('display', 'none');
		$('#logIncSubmitResponse').css('display', 'none');
		
		$('#logTicketButton').click(function(){
	       $('#LogIncidentModal').css('display', 'block');
	       $('#LogIncidentModal').css('z-index', 1041);
	       $('#LogIncidentModal').modal('show');
	       $('body').addClass('modal-open');
	       $('#_145_dockbar').css('z-index', 1039);
	    });
		
		
		$('#closeMarkID, #cancelButtonID').click(function(){
			$('body').removeClass('modal-open');
			$("label.error").hide();
			$(".error").removeClass("error"); 
			$('#serviceNameSelect option[value!="0"]').remove();
			$("#logIncForm").validate().resetForm();
			$("#logIncForm")[0].reset();
			$('#_145_dockbar').css('z-index', 1041);
			$('#LogIncidentModal').modal('hide');
			$('body').removeClass('modal-open');
		});
		
		$('#LogIncidentModal').on( 'shown', function ( event ) {
			$(this).css('display', 'block');
			$('#_145_dockbar').css('z-index', 1039);
			
			
		});
		
		$('#LogIncidentModal').on( 'hidden', function ( event ) {
			$(this).css('display', 'none');
			$('#_145_dockbar').css('z-index', 1041);
			$("label.error").hide();
			$(".error").removeClass("error"); 
			$('#serviceNameSelect option[value!="0"]').remove();
			$('#submitRequestButtonID').removeClass("disableButton");
			
			$("#logIncForm").validate().resetForm();
			$("#logIncForm")[0].reset();
			
		});
		
		$('#logIncSubmitResponse').on( 'shown', function ( event ) {
			$('#LogIncidentModal').css('z-index', 1039);
			$(this).css('z-index', 1042);
		});
		
		$('#logIncSubmitResponse').on( 'hidden', function ( event ) {
			$(this).css('display', 'none');
			$('#LogIncidentModal').css('z-index', 1041);
			$('#LogIncidentModal').modal('hide');
			$('.modal-backdrop').remove();
			$('body').removeClass('modal-open');
			
		});
	});