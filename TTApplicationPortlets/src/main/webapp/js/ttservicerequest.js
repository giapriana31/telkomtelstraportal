$(document).ready( function () {
	
	$('#tt-toggle-srlisting').click(function() {
		$("#tt-srlisting-container").css("display", "none");
		$("#tt-srd-container").slideToggle();
	});

	$('#tt-toggle-srdetails').click(function() {
		$("#tt-srd-container").css("display", "none");
		$("#tt-srlisting-container").slideToggle();
		$(".update-data-tab").slideToggle();
		$("#updateDataHolderDiv").slideToggle();
		$(".tt-morecrdetails-container").hide();
		$("#activeTab").removeClass("displayNoneImportant");
		$("#completedTab").removeClass("displayNoneImportant");
	});
	
	$('#ritmModal').css('display', "none");
	
	/*$('#ritmModal').on( 'shown', function ( event ) {
		$('#ritmModal').css('display', "block");
		$('#_145_dockbar').css('z-index', 1039);
		$('body').addClass('modal-open');
	});
*/	$('#ritmModal').on( 'hidden', function ( event ) {
		$('#ritmModal').css('display', "none");
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$('body').removeClass('modal-open');
	});

});

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

function showRITMModal(){
	$('#ritmModal').modal('show');
	$('#ritmModal').css('display', 'block');
	$('#_145_dockbar').css('z-index', 1039);
	$('body').addClass('modal-open');
}

function drawBarFillGraph(data,obj,height,section){
	
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	// Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
	var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
		// At least Safari 3+: "[object HTMLElementConstructor]"
	var isChrome = !!window.chrome && !isOpera; 
	var amt=0;
	var largest= Math.max.apply( Math, data.variantStatistics );
	var limit=Math.ceil(1.4*largest);
	if(limit<=1){
		limit=2;
	}
	else if(limit<10){
		if(limit%2!=0){
			limit++;
		}
	}
	else{
		if(limit%10>0){
			var res=limit%10;
			limit=(Math.floor(limit/10))*10;
			limit+=10;
		}
		else{
			limit=(Math.floor(limit/10))*10;
		}
		
	}
	var sum=0;
	
	var stats=[];
	
	for(var i=0;i<data.variantStatistics.length;i++){
		var instat=Math.floor(data.variantStatistics[i]/limit*height);
		stats.push(instat);
		sum+=data.variantStatistics[i];
	}
	
	
	var statLargest=Math.max.apply( Math, stats );
	
	var offset=height-statLargest;
	if(offset>height-15){
		offset=height-15;
	}
	
	if (isChrome || isFirefox) {
		amt=offset-25;
	}
	var variantTrendIdentifier="#"+section+obj;

	$(variantTrendIdentifier+" .variantcategorycount").html(sum);
	var str='<div class="basbarlayout">';
	for(var i=0;i<12;i++){
		str+='<div class="basebar" style="height:'+height+'px"></div>';
	}
	str+='</div>';
	str+='<div class="markbarlayout" style="top:'+offset+'px">';
	for(var i=0;i<12;i++){
		str+='<div class="markbar" style="height:'+stats[i]+'px;"></div>';
	}
	str+='</div>';
	$(variantTrendIdentifier+" .graphOverlay").html(str);
	var midpos=35-(Math.floor((height-90)/2));
	var bottompos=11-(height-90);
	var strstat='<div style="position:relative;top:'+amt+'px"><div class="uppermarker">&nbsp;'+limit+'</div><div class="midmarker" style="bottom:'+midpos+'px">&nbsp;'+limit/2+'</div><div class="bottommarker" style="bottom:'+bottompos+'px">&nbsp;0</div></div>';
	$(variantTrendIdentifier+" .graphStatistics").html(strstat);
	var xstrstat='<div style="position:relative;top:'+(amt-15)+'px"><div class="startXMarker">Wk-12</div><div class="midXMarker">Wk-6</div><div class="endXMarker"></div></div>';
	$(variantTrendIdentifier+" .xMarkersGraph").html(xstrstat);
	
}