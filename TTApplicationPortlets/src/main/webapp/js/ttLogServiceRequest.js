$(document).ready( function () {
	$('#logSRModal').css('display', "none");
	$('#logSRSubmitResponse').css('display', "none");
	/*$('#logSRModal').css('z-index', -10);*/
	$("input:radio").attr("checked", false);
	
	
	
	$('#closeMarkID, #cancelButtonID').click(function(){
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$("label.error").hide();
		$('#serviceNameSelect option[value!="0"]').remove();
		$("#logSRForm").validate().resetForm();
		$("#logSRModal").find('form')[0].reset();
		$("#logSRForm")[0].reset();
		$('#logSRModal').modal('hide');
	});
		
		
	$('#logServReqButton').click(function(){
		
		$('#_145_dockbar').css('z-index', 1039);
		$('#logSRModal').modal('show');
		$('#logSRModal').css('display', "block");
		$('body').addClass('modal-open');
		$("#categoryLabelID").addClass('error');
	});
	
	$('#logSRModal').on( 'shown', function ( event ) {
		$('#logSRModal').css('display', "block");
		$("#logSRForm")[0].reset();
		$("#logSRForm").validate().resetForm();
		$("#categoryLabelID").addClass('error');
	});
	
	$('#logSRModal').on( 'hidden', function ( event ) {
		$('#logSRModal').css('display', "none");
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$("label.error").hide();
		$('#submitRequestButtonID').removeClass("disableButton");
		document.getElementById("submitRequestButtonID").disabled = false;
		$("#logSRForm").validate().resetForm();
		$(this).find('form')[0].reset();
		$("#logSRForm")[0].reset();
		
	});
	
	$('#logSRSubmitResponse').on( 'shown', function ( event ) {
		$('#logSRModal').css('z-index', 1039);
		$('#logSRSubmitResponse').css('display', "block");
		$(this).css('z-index', 1042);
	});
	
	$('#logSRSubmitResponse').on( 'hidden', function ( event ) {
		$(this).css('display', 'none');
		$('#logSRModal').css('z-index', 1041);
		$('#logSRModal').modal('hide');
		$('.modal-backdrop').remove();
		$('body').removeClass('modal-open');
	});
});

	function addCommentNotes(element){
		if(element.checked){
			 /*var textbox = document.getElementById("customDescription");
			 textbox.value =  "Multiple services are impacted. " + textbox.value;*/
			 $('#deviceNameSelect').attr('disabled','disabled');
		     $('#deviceNameSelect').addClass("disableButton");
		}
		else{
			/*var textbox = document.getElementById("customDescription");
			textbox.value="";*/
			/*$("input:radio").attr("checked", false);
			$('#actionSelect option[value!="0"]').remove();
			$('#subCategorySelect option[value!="0"]').remove();*/
			/*$('#deviceNameSelect option[value!="0"]').remove();*/
			$('#deviceNameSelect').removeAttr('disabled','disabled');
	    	$('#deviceNameSelect').removeClass("disableButton");
			
		}
	}
	function addActionComment(){
		var selectedAction = document.getElementById("actionSelect");
		var textarea = document.getElementById("customDescription").value
		if(selectedAction.value!='0'){
			textarea += "This is "+selectedAction.value+" action.";
		}else{
			document.getElementById("customDescription").value="";
		}
	}
	
	
	function hideAll(){
		$('#submitRequestButtonID').attr('disabled','disabled');
    	$('#submitRequestButtonID').addClass("disableButton");
    	
    	$('#summary').attr('disabled','disabled');
    	$('#summary').addClass("disableButton");
    	
    	$('#regionSelect').attr('disabled','disabled');
    	$('#regionSelect').addClass("disableButton");
    	
    	$('#siteID').attr('disabled','disabled');
    	$('#siteID').addClass("disableButton");
    	
    	$('#serviceNameSelect').attr('disabled','disabled');
    	$('#serviceNameSelect').addClass("disableButton");
    	        	
    	$('#MACDRadio').attr('disabled','disabled');
    	$('#MACDRadio').addClass("disableButton");
    	
    	$('#GenericRadio').attr('disabled','disabled');
    	$('#GenericRadio').addClass("disableButton");
    	
    	$('#actionSelect').attr('disabled','disabled');
    	$('#actionSelect').addClass("disableButton");
    	
    	$('#subCategorySelect').attr('disabled','disabled');
    	$('#subCategorySelect').addClass("disableButton");
    	
    	$('#deviceNameSelect').attr('disabled','disabled');
    	$('#deviceNameSelect').addClass("disableButton");
    	
    	$('#productClassificationSelect').attr('disabled','disabled');
    	$('#productClassificationSelect').addClass("disableButton");
    	
    	$('#customDescription').attr('disabled','disabled');
    	$('#customDescription').addClass("disableButton");
    	
    	$('#cancelButtonID').attr('disabled','disabled');
    	$('#cancelButtonID').addClass("disableButton");
    	
    	$('#MultiServiceCheckBox').attr('disabled','disabled');
    	$('#MultiServiceCheckBox').addClass("disableButton");
    	
    	$('#closeMarkID').attr('disabled','disabled');
    	$('#closeMarkID').addClass("disableButton2");
	}
	function unhideAll(){
		$('#submitRequestButtonID').removeAttr('disabled','disabled');
    	$('#submitRequestButtonID').removeClass("disableButton");
    	
    	$('#regionSelect').removeAttr('disabled','disabled');
    	$('#regionSelect').removeClass("disableButton");
    	
    	$('#summary').removeAttr('disabled','disabled');
    	$('#summary').removeClass("disableButton");
    	
    	$('#siteID').removeAttr('disabled','disabled');
    	$('#siteID').removeClass("disableButton");
    	
    	$('#serviceNameSelect').removeAttr('disabled','disabled');
    	$('#serviceNameSelect').removeClass("disableButton");
    	        	
    	$('#MACDRadio').removeAttr('disabled','disabled');
    	$('#MACDRadio').removeClass("disableButton");
    	
    	$('#GenericRadio').removeAttr('disabled','disabled');
    	$('#GenericRadio').removeClass("disableButton");
    	
    	$('#actionSelect').removeAttr('disabled','disabled');
    	$('#actionSelect').removeClass("disableButton");
    	
    	$('#subCategorySelect').removeAttr('disabled','disabled');
    	$('#subCategorySelect').removeClass("disableButton");
    	
    	$('#deviceNameSelect').removeAttr('disabled','disabled');
    	$('#deviceNameSelect').removeClass("disableButton");
    	
    	$('#productClassificationSelect').attr('disabled','disabled');
    	$('#productClassificationSelect').addClass("disableButton");
    	
    	$('#customDescription').removeAttr('disabled','disabled');
    	$('#customDescription').removeClass("disableButton");
    	
    	$('#cancelButtonID').removeAttr('disabled','disabled');
    	$('#cancelButtonID').removeClass("disableButton");
    	
    	$('#MultiServiceCheckBox').removeAttr('disabled','disabled');
    	$('#MultiServiceCheckBox').removeClass("disableButton");
    	
    	$('#closeMarkID').removeAttr('disabled','disabled');
    	$('#closeMarkID').removeClass("disableButton2");
	}
	