function drawBarFillGraph(data,obj,height,section){
	
	var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	// Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
	var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
	var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
		// At least Safari 3+: "[object HTMLElementConstructor]"
	var isChrome = !!window.chrome && !isOpera; 
	var amt=0;
	var largest= Math.max.apply( Math, data.variantStatistics );
	var limit=Math.ceil(1.4*largest);
	if(limit<=1){
		limit=2;
	}
	else if(limit<10){
		if(limit%2!=0){
			limit++;
		}
	}
	else{
		if(limit%10>0){
			var res=limit%10;
			limit=(Math.floor(limit/10))*10;
			limit+=10;
		}
		else{
			limit=(Math.floor(limit/10))*10;
		}
		
	}
	var sum=0;
	
	var stats=[];
	
	for(var i=0;i<data.variantStatistics.length;i++){
		var instat=Math.floor(data.variantStatistics[i]/limit*height);
		stats.push(instat);
		sum+=data.variantStatistics[i];
	}
	
	
	var statLargest=Math.max.apply( Math, stats );
	
	var offset=height-statLargest;
	if(offset>height-15){
		offset=height-15;
	}
	
	if (isChrome || isFirefox) {
		amt=offset-25;
	}
	var variantTrendIdentifier="#"+section+obj;

	$(variantTrendIdentifier+" .variantcategorycount").html(sum);
	var str='<div class="basbarlayout">';
	for(var i=0;i<12;i++){
		str+='<div class="basebar" style="height:'+height+'px"></div>';
	}
	str+='</div>';
	str+='<div class="markbarlayout" style="top:'+offset+'px">';
	for(var i=0;i<12;i++){
		str+='<div class="markbar" style="height:'+stats[i]+'px;"></div>';
	}
	str+='</div>';
	$(variantTrendIdentifier+" .graphOverlay").html(str);
	var midpos=35-(Math.floor((height-90)/2));
	var bottompos=11-(height-90);
	var strstat='<div style="position:relative;top:'+amt+'px"><div class="uppermarker">&nbsp;'+limit+'</div><div class="midmarker" style="bottom:'+midpos+'px">&nbsp;'+limit/2+'</div><div class="bottommarker" style="bottom:'+bottompos+'px">&nbsp;0</div></div>';
	$(variantTrendIdentifier+" .graphStatistics").html(strstat);
	var xstrstat='<div style="position:relative;top:'+(amt-15)+'px"><div class="startXMarker">Wk-12</div><div class="midXMarker">Wk-6</div><div class="endXMarker"></div></div>';
	$(variantTrendIdentifier+" .xMarkersGraph").html(xstrstat);
}