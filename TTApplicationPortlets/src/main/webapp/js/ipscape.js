function activeCallbackTable() 
{      
       $('#ipscapeCompleteRequest-li').removeClass('selected');
       $('#ipscapeActiveRequest-li').addClass('selected');
       callbackDetailsIncompleteTableAjax();
       document.getElementById("completeCallbackDetailsDiv").style.display = 'none';
       document.getElementById("inCompleteCallbackDetailsDiv").style.display = 'block';
}
function completeCallbackTable() 
{      
       $('#ipscapeActiveRequest-li').removeClass('selected');
       $('#ipscapeCompleteRequest-li').addClass('selected');
       callbackDetailsCompleteTableAjax();
       document.getElementById("inCompleteCallbackDetailsDiv").style.display = 'none';
       document.getElementById("completeCallbackDetailsDiv").style.display = 'block';
}

var radioSelected;

function openIpscapePage(){
       $("#IPScapeId").modal('show');
          document.getElementById("intersupportScreenBahasa").style.display = "none";
              $("#ipScapeForm label").css("display","block");
              $("#ipScapeForm label").addClass("error");
              resetIpscapeForm();  
}

function processPhone() {
       phone = $('#telephone').val();
       
       var country = 'Indonesia';
       if (isValidNumber(phone, country)) {
       //alert("True");
              return true;
       } else {
       //alert("False");
              return false;
       }
}

function processEditPhone() {
       
       
       phone = $('#phoneEdit').val();
       
       var country = 'Indonesia';
       if (isValidNumber(phone, country)) {
       //alert("True");
              return true;
       } else {
       //alert("False");
              return false;
       }
}


function showIPScapeForm(){
       $("#IPScapeId").modal('show');
}

function resetIpscapeForm(){
       //var validator = $( "#ipScapeForm" ).validate();
       //validator.resetForm();
       $('#ipScapeForm').trigger("reset");
       $.fn.clearValidation = function(){var v = $(this).validate();$('[name]',this).each(function(){v.successList.push(this);v.showErrors();});v.resetForm();v.reset();};
       $("#ipScapeForm").clearValidation();
       //$("#ipScapeForm label").text("");
       //$("#ipScapeForm label").css("display","block");

       $('#pmradio').attr('disabled','disabled');
       $('#amradio').attr('disabled','disabled');
       $('#rangeTime').attr('disabled','disabled');
          
         
       
}

function resetIpscapeEditForm(){
       $('#ipscapeEditForm').trigger("reset");
       $.fn.clearValidation = function(){var v = $(this).validate();$('[name]',this).each(function(){v.successList.push(this);v.showErrors();});v.resetForm();v.reset();};
       $("#ipscapeEditForm").clearValidation();
          
           $('#pmradioEdit').attr('disabled','disabled');
       $('#amradioEdit').attr('disabled','disabled');
       $('#rangeTimeEdit').attr('disabled','disabled');
}

function resetMessages()
{
       $("#customerNameId").hide();
       $("#telephoneId").hide();
       $("#callbackDateId").hide();
       $("#specificTimeId").hide();
       $("#timeradioId").hide();
       $("#categoryId").hide();
       $("#rangeTimeId").hide();
       
}

function enableAMPMEdit(){
       var datepicker = $('#callbackDateEdit').val();
       var stringDatePicker = String(datepicker);
       var date = String($('#dateAsiaJakarta').val());
       var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
       var year = String($('#yearAsiaJakarta').val());
       if(date.length == 1){
              date = "0"+date;
       }
       if(month.length == 1){
              month = "0"+month;
       }      
       var stringFormattedDate =  date + "/" + month + "/" + year;
       //alert("current date::"+stringFormattedDate);
       $('#rangeTimeEdit').attr('disabled','disabled');
       $('#pmradioEdit').removeAttr('disabled','disabled');
       $('#amradioEdit').removeAttr('disabled','disabled');
       $("#rangeTimeEdit").empty();
       $('input[name="ampmradioEdit"]').attr('checked', false);
       $('input[name="ampmradioEdit"]').attr('checked', false);
       var asiaHour = $('#hoursAsiaJakarta').val();
       var asiaMin = $('#minutesAsiaJakarta').val();
       //alert(stringDatePicker+"--"+stringFormattedDate);
       if(((asiaHour >= 11 && asiaMin >=30) || asiaHour >= 12) && stringDatePicker == stringFormattedDate){
                     $('#amradioEdit').attr('disabled','disabled');
                     //alert("asdf");
       }
       if(asiaHour>=23 && asiaMin >=30 && stringDatePicker == stringFormattedDate){
                     $('#pmradioEdit').attr('disabled','disabled');
                     //alert("asdf");
       }
       
}




function populateTimeDropDownEdit(){
       $('#rangeTimeEdit').removeAttr('disabled','disabled');
       $("#rangeTimeEdit").empty();
       var datepicker = $('#callbackDateEdit').val();
       
       var stringDatePicker = String(datepicker);
       var date = String($('#dateAsiaJakarta').val());
       var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
       var year = String($('#yearAsiaJakarta').val());
       //alert("check::"+date.length);
       if(date.length == 1){
              date = "0"+date;
       }
       if(month.length == 1){
              month = "0"+month;
       }      
       var stringFormattedDate = date + "/" + month + "/" + year;
       //alert("formatted date::"+stringFormattedDate);
       //alert("date picker::"+stringDatePicker);
       var asiaHour = $('#hoursAsiaJakarta').val();
       var asiaMin = $('#minutesAsiaJakarta').val();
       var ampmValue = document.querySelector('input[name = "ampmradioEdit"]:checked').value;
       //alert("ampm value:"+ampmValue);
       //alert(asiaHour+":"+asiaMin);
       
       var hour = 0;
       var min = 0;
       if(stringDatePicker == stringFormattedDate){
              //alert("Same date is selected");
              if(asiaHour<12 && ampmValue=="pm"){
                     for(var i=0 ; i<24; i++){
                           var stringHour = String(hour);
                           var stringMin = String(min);
                           if(stringHour.length == 1){
                                  stringHour = "0"+stringHour;
                           }
                           if(stringMin.length == 1){
                                  stringMin = "0"+stringMin;
                           }
                           if(stringHour == "00"){
                                  stringHour = "12";
                           }
                           time = stringHour + ":" + stringMin;
                           //alert("Time asdf::"+time);
                           $("select[name='rangeTimeEdit']").append('<option value="'+time+'">'+time+'</option>');
                           if(min == 0){
                                  min = 30;
                           }else if(min == 30){
                                  min = 00;
                                  hour = hour + 1;
                           }
                     }
              }else{
                     if(parseInt(asiaMin) >= 30){
                           //alert("hours:::"+hour);
                           hour = parseInt(asiaHour) + 1;
                           //alert("hours after half an hour(+1):::"+hour);
                           if(hour >= 12){
                                  hour = hour - 12;
                                  //alert("hours after minus:::"+hour);
                           }
                     }else{
                           hour = parseInt(asiaHour);
                           //alert("hours:::"+hour);
                           if(hour >= 12){
                                  //alert("hours:::"+hour);
                                  hour = hour - 12;
                                  //alert("hours:::"+hour);
                           }
                           min = 30
                     }
                     while(hour < 12){
                           var stringHour = String(hour);
                           var stringMin = String(min);
                           if(stringHour.length == 1){
                                  stringHour = "0"+stringHour;
                           }
                           if(stringMin.length == 1){
                                  stringMin = "0"+stringMin;
                           }
                           if(stringHour == "00"){
                                  stringHour = "12";
                           }
                           time = stringHour + ":" + stringMin;
                           //alert("Time asdf::"+time);
                           $("select[name='rangeTimeEdit']").append('<option value="'+time+'">'+time+'</option>');
                           if(min == 0){
                                  min = 30;
                           }else if(min == 30){
                                  min = 00;
                                  hour = hour + 1;
                           }
                     }
              }
       }else{ 
       for(var i=0 ; i<24; i++){
              var stringHour = String(hour);
              var stringMin = String(min);
              if(stringHour.length == 1){
                     stringHour = "0"+stringHour;
              }
              if(stringMin.length == 1){
                     stringMin = "0"+stringMin;
              }
              if(stringHour == "00"){
                     stringHour = "12";
              }
              time = stringHour + ":" + stringMin;
              //alert("Time asdf::"+time);
              $("select[name='rangeTimeEdit']").append('<option value="'+time+'">'+time+'</option>');
              if(min == 0){
                     min = 30;
              }else if(min == 30){
                     min = 00;
                     hour = hour + 1;
              }
       }
       }
}




function enableAMPM(){
       var datepicker = $('#callbackDate').val();
       var stringDatePicker = String(datepicker);
       var date = String($('#dateAsiaJakarta').val());
       var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
       var year = String($('#yearAsiaJakarta').val());
       if(date.length == 1){
              date = "0"+date;
       }
       if(month.length == 1){
              month = "0"+month;
       }      
       var stringFormattedDate =  date + "/" + month + "/" + year;
       
       $('#rangeTime').attr('disabled','disabled');
       $('#pmradio').removeAttr('disabled','disabled');
       $('#amradio').removeAttr('disabled','disabled');
       $("#rangeTime").empty();
       $('input[name="ampmradio"]').attr('checked', false);
       $('input[name="ampmradio"]').attr('checked', false);
       var asiaHour = $('#hoursAsiaJakarta').val();
       var asiaMin = $('#minutesAsiaJakarta').val();
       
       if(((asiaHour >= 11 && asiaMin >=30) || asiaHour >= 12) && stringDatePicker == stringFormattedDate){
                     $('#amradio').attr('disabled','disabled');
       }
       if(asiaHour>=23 && asiaMin >=30 && stringDatePicker == stringFormattedDate){
                     $('#pmradio').attr('disabled','disabled');
       }
       
}

function populateTimeDropDown(){
       $('#rangeTime').removeAttr('disabled','disabled');
       $("#rangeTime").empty();
       var datepicker = $('#callbackDate').val();
       
       var stringDatePicker = String(datepicker);
       var date = String($('#dateAsiaJakarta').val());
       var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
       var year = String($('#yearAsiaJakarta').val());
       //alert("check::"+date.length);
       if(date.length == 1){
              date = "0"+date;
       }
       if(month.length == 1){
              month = "0"+month;
       }      
       var stringFormattedDate = date + "/" + month + "/" + year;
       //alert("formatted date::"+stringFormattedDate);
       //alert("date picker::"+stringDatePicker);
       var asiaHour = $('#hoursAsiaJakarta').val();
       var asiaMin = $('#minutesAsiaJakarta').val();
       var ampmValue = document.querySelector('input[name = "ampmradio"]:checked').value;
       //alert("ampm value:"+ampmValue);
       //alert(asiaHour+":"+asiaMin);
       
       var hour = 0;
       var min = 0;
       if(stringDatePicker == stringFormattedDate){
              //alert("Same date is selected");
              if(asiaHour<12 && ampmValue=="pm"){
                     for(var i=0 ; i<24; i++){
                           var stringHour = String(hour);
                           var stringMin = String(min);
                           if(stringHour.length == 1){
                                  stringHour = "0"+stringHour;
                           }
                           if(stringMin.length == 1){
                                  stringMin = "0"+stringMin;
                           }
                           if(stringHour == "00"){
                                  stringHour = "12";
                           }
                           time = stringHour + ":" + stringMin;
                           //alert("Time asdf::"+time);
                           $("select[name='rangeTime']").append('<option value="'+time+'">'+time+'</option>');
                           if(min == 0){
                                  min = 30;
                           }else if(min == 30){
                                  min = 00;
                                  hour = hour + 1;
                           }
                     }
              }else{
                     if(parseInt(asiaMin) >= 30){
                           //alert("hours:::"+hour);
                           hour = parseInt(asiaHour) + 1;
                           //alert("hours after half an hour(+1):::"+hour);
                           if(hour >= 12){
                                  hour = hour - 12;
                                  //alert("hours after minus:::"+hour);
                           }
                     }else{
                           hour = parseInt(asiaHour);
                           //alert("hours:::"+hour);
                           if(hour >= 12){
                                  //alert("hours:::"+hour);
                                  hour = hour - 12;
                                  //alert("hours:::"+hour);
                           }
                           min = 30
                     }
                     while(hour < 12){
                           var stringHour = String(hour);
                           var stringMin = String(min);
                           if(stringHour.length == 1){
                                  stringHour = "0"+stringHour;
                           }
                           if(stringMin.length == 1){
                                  stringMin = "0"+stringMin;
                           }
                           if(stringHour == "00"){
                                  stringHour = "12";
                           }
                           time = stringHour + ":" + stringMin;
                           //alert("Time asdf::"+time);
                           $("select[name='rangeTime']").append('<option value="'+time+'">'+time+'</option>');
                           if(min == 0){
                                  min = 30;
                           }else if(min == 30){
                                  min = 00;
                                  hour = hour + 1;
                           }
                     }
              }
       }else{ 
       for(var i=0 ; i<24; i++){
              var stringHour = String(hour);
              var stringMin = String(min);
              if(stringHour.length == 1){
                     stringHour = "0"+stringHour;
              }
              if(stringMin.length == 1){
                     stringMin = "0"+stringMin;
              }
              if(stringHour == "00"){
                     stringHour = "12";
              }
              time = stringHour + ":" + stringMin;
              //alert("Time asdf::"+time);
              $("select[name='rangeTime']").append('<option value="'+time+'">'+time+'</option>');
              if(min == 0){
                     min = 30;
              }else if(min == 30){
                     min = 00;
                     hour = hour + 1;
              }
       }
       }
}


var page = 1;
var campaignList = new Array();
var totalCampaigns = 0;
$(document).ready(function () 
{                          
	$('#createCallbckId').removeAttr('disabled');
	$('#createCallbckId').removeAttr("title");
	  $('#createCallbckId').removeClass("img-create-callback-disabled");
	   $('#createCallbckId').addClass("img-create-callback");
       
            $("#editCallbackModal").hide();
       		$("#IPScapeId").hide();
       		$("#IpscapeSubmitResponse").hide();
          	$("#confirmationBoxId").hide();
           	$("#deleteCallbackResponse").modal('hide'); 
 			$("#deleteCallbackConfirm").modal('hide'); 

           $("#editLeadResponseDiv").modal('hide');
           
           
       $('#pmradio').attr('disabled','disabled');
       $('#amradio').attr('disabled','disabled');
       $('#rangeTime').attr('disabled','disabled');
       
          activeCallbackTable();
          getCampaignListAjax(page);
          
       function getCampaignListAjax(page)
       {     
              var getCampaignListURL = "${getCampaignList}" + "&pageNo=" + page + "&perPage=100";
              
              $.ajax({
                     type: "POST",
                     url: getCampaignListURL,
                     dataType: "json",
                     success : function(data) 
                     {      
                           campaignList.push(data);
                                            
                           getFinalList(data);
                                                       
                     },
                     error : function()
                     {
                           
                     }
              });
       }
       
       function getFinalList(jsonData)
       {             
              totalCampaigns= totalCampaigns + jsonData.result.inPage;             
                     
              var total = jsonData.result.total;
                                  
              if(total >= totalCampaigns){
                     if(totalCampaigns == total){
                           totalCampaigns++; 
                     }
                     getCampaignListAjax(page = page + 1);
              }else{
                           
                     for(var i = 0;i < campaignList.length; i++){
                                  
                           var Json=campaignList[i];
                           var dataArray = Json.result.data;
                           for(var j=0 ; j<dataArray.length; j++){
                                  if(dataArray[j].campaignType == "Outbound"){
                                  $('#category').append($('<option>').text(dataArray[j].campaignTitle).val(dataArray[j].campaignId));
                                  }
                           }
                     }
              }
       }
       
       var date = String($('#dateAsiaJakarta').val());
       var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
       var year = String($('#yearAsiaJakarta').val());
       
       
       var todayDate = new Date(year + '/' + month + '/' + date);
       todayDate.setDate(todayDate.getDate() + 30);
       
      $(".selectDate").datepicker({minDate: 0, maxDate: new Date(todayDate.getFullYear(),todayDate.getMonth(),todayDate.getDate()), dateFormat: 'dd/mm/yy'});

       
       var submitResourceURLIPScape = "${submitResourceURLIPScape}";
         
              
       jQuery.validator.addMethod('timeformat',function(value,element,regexp){
              var re = new RegExp(regexp);
              return this.optional( element ) || re.test(value);
       }, "<liferay-ui:message key='Please insert time in proper format (HH:MM)'/>");    
       
       jQuery.validator.addMethod("namecheck", function(value, element,regexp) {
              var re = new RegExp(regexp);
           return this.optional(element) || re.test(value);
       },"<liferay-ui:message key='Please insert characters only'/>");
       
       jQuery.validator.addMethod("phonecheck", function(value, element,regexp) {
              var re = processPhone();
           return processPhone();
       },"<liferay-ui:message key='Invalid phone number'/>");
       
       $("#okbuttonid").click(function(){
           $("#confirmationBoxId").modal('hide');
           var dataFormIpscape = new FormData($('#ipScapeForm')[0]);
           $
           .ajax({
                         url : submitResourceURLIPScape,
                         type : "POST",
                         data : dataFormIpscape,
                         success : function(response) {
                                
                                var ipscapeResponse = JSON
                                       .parse(response);
                               
                                var status = ipscapeResponse.ipscape;
                                if (status == 'success') {
                                       document.getElementById("responseStatusIPScape").innerHTML = '<liferay-ui:message key="Your Callback request has been successfully submitted."/>';
                                }else{
                                       document
                                       .getElementById("responseStatusIPScape").innerHTML = '<liferay-ui:message key="Callback request failed.Please try again."/>';
                                }
                                $("#confirmationBoxId").modal('hide');
                                $("#IpscapeSubmitResponse").modal('show');
                         },
                         error : function(
                                       xhr) {
                                
                         },
                         processData: false,
                         contentType: false
                  });
     });               
                     
       $("#ipScapeForm").validate({
              onsubmit: true,
              rules:{
                     callbackDate:{
                           required:true                                   
                     },
                     firstName:{
                           required:true,
                           namecheck:"^[[a-zA-Z][a-zA-Z ]+$"
                     },
                     lastName:{
                           required:true,
                           namecheck:"^[[a-zA-Z][a-zA-Z ]+$"
                     },
                     telephone:{
                           required:true,
                           phonecheck:""                     
                     },
                     ampmradio:{
                           required:true
                     },
                     category: {
                           required: {
                                  depends: function(element) {
                                         return $("#category").val() == '';
                                  }
                           }
                     },
                     rangeTime: {
                           required: {
                                  depends: function(element) {
                                         return $("#rangeTime").val() == '';
                                  }
                           }
                     }
              },
                        errorClass:"validationError",
              messages:{
                     callbackDate : {
                           required:"<liferay-ui:message key='Please enter Callback date' />"
                            },
                       firstName:{
                            required:"<liferay-ui:message key='Please enter First Name' />",
                            namecheck:"<liferay-ui:message key='Please insert characters only' />"
                     },
                lastName:{
                            required:"<liferay-ui:message key='Please enter Last Name' />",
                            namecheck:"<liferay-ui:message key='Please insert characters only' />"
                     },
                     telephone:{
                            required:"<liferay-ui:message key='Please enter Phone Number' />",
                            countrycodecheck:"<liferay-ui:message key='Invalid phone number' />"
                 },
                 ampmradio:{
                            required:"<liferay-ui:message key='Please select either am or pm' />"
                 },
                 category:{
                           required:"<liferay-ui:message key='Please select a Category' />" ,
                     },
                     rangeTime:{
                           required:"<liferay-ui:message key='Please select an option from Range Time' />" 
                     }
                 
              },
              submitHandler : function(form) {
                     
                     phone = $('#telephone').val();
                     date =        $('#callbackDate').val();
                              
                     rangeTime = document.getElementById("rangeTime").value; 
                                   
                     var ampmValue = document.querySelector('input[name = "ampmradio"]:checked').value;                   
                     document.getElementById("confirmationString").innerHTML = '<liferay-ui:message key="Please verify the following Callback details:"/>';
                     document.getElementById("phoneVerify").innerHTML = '<liferay-ui:message key="Phone : "/>'+phone;
                     document.getElementById("dateVerify").innerHTML = '<liferay-ui:message key="Date : "/>'+date;
                                         
                     if(ampmValue == "am"){
                           document.getElementById("timeVerify").innerHTML = '<liferay-ui:message key="Time : "/>'+rangeTime+" AM JKT(GMT+7)";
                     }else{
                           document.getElementById("timeVerify").innerHTML = '<liferay-ui:message key="Time : "/>'+rangeTime+" PM JKT(GMT+7)";
                     }
                                  
                     $("#IPScapeId").modal('hide');
                     $("#confirmationBoxId").modal('show');          
                      
              }
       });
          
          jQuery.validator.addMethod("editphonecheck", function(value, element,regexp) {
              var re = processEditPhone();
           return processEditPhone();
       },"<liferay-ui:message key='Invalid phone number'/>");
          $("#ipscapeEditForm").validate({
              onsubmit: true,
              rules:{
                     phoneEdit:{
                           required:true,
                           editphonecheck:""                     
                     },
                           callbackDateEdit:{
                                     required:true                                   
                            },
                           ampmradioEdit:{
                           required:true
                 },
                 rangeTimeEdit: {
                                     required: {
                                                  depends: function(element) {
                                                              return $("#rangeTimeEdit").val() == '';
                                                  }
                                     }
                           },
              },
                       
              messages:{
                
                           phoneEdit:{
                                         required:"<liferay-ui:message key='Please enter Phone Number' />",
                        editphonecheck:"<liferay-ui:message key='Invalid phone number' />"
                                  },
                           callbackDateEdit:{
                           required:"<liferay-ui:message key='Please enter Callback date' />"
                    },
                           ampmradioEdit:{
                            required:"<liferay-ui:message key='Please select either am or pm' />"
                 },
                           rangeTimeEdit:{
                           required:"<liferay-ui:message key='Please select an option from Range Time' />" 
                 }
                           },
                           submitHandler : function(form) {
                                  var tempAMPMValue = document.querySelector('input[name = "ampmradioEdit"]:checked').value;
                                  var submitEditLeadIPScapeURL = "${submitEditLeadIPScapeURL}"+"&ampmradioEdit="+tempAMPMValue;
                                  var dataFormEditIpscape = new FormData($('#ipscapeEditForm')[0]);
                                                                 
                              $
                              .ajax({
                                                       url : submitEditLeadIPScapeURL,
                                                       type : "POST",
                                                       data : dataFormEditIpscape,
                                                       success : function(response) {
                                                                 //alert("Control in jsp");
                                                                 var updateLeadResponse = JSON
                                       .parse(response);
                               
                                              var updateStatus = updateLeadResponse.update;
                                              if (updateStatus == 'success') {
                                                
                                                $("#editCallbackModal").modal('hide'); 
                                                     document.getElementById("editLeadResponse").innerHTML = '<liferay-ui:message key="Callback updated successfully."/>';
                                                                              $("#editLeadResponseDiv").modal('show'); 
                                                                              callbackDetailsIncompleteTableAjax();
                                                     //alert("Success update");
                                              }else{
                                                $("#editCallbackModal").modal('hide');
                                                 document.getElementById("editLeadResponse").innerHTML = '<liferay-ui:message key="Callback update failed. Please try again."/>';
                                                                             $("#editLeadResponseDiv").modal('show');   
                                                //alert("Fail update");
                                              }
                                                                 
                                                        },
                                                       error : function(
                                                                              xhr) {
                                                                     
                                                       },
                                                       processData: false,
                                                       contentType: false
                                           });
                                                
                                         }
              });
              
              jQuery("#IpscapeButton").click(function()
                         {
                            
                          jQuery(this).next(".collapse3").slideToggle(15);
                          $("#IpscapeIcon").toggleClass("IpscapeMore IpscapeLess");
                          $("#IpscapeButton")
                             .toggleClass("ssry");
                          jQuery(".collapse1").hide();
                          jQuery(".collapse2").hide();
                          
                          $(".lsry")
                             .removeClass("lsry").add("lbry");
                          $(".sry")
                             .removeClass("sry").add("bry");
                          });
       
              
});

function showFloatingButton(){
              document.getElementById("intersupportScreenBahasa").style.display = "block";
}

var jsonLeadData;
              function callbackDetailsIncompleteTableAjax()
              {      
                     //alert("Function Called..");
                     var callbackDetailsURL = "${incompleteCallbackDetailsURL}";
                     //alert("URL:::"+callbackDetailsURL)
                     $.ajax({
                           url: callbackDetailsURL,
                           type: "POST",
                           dataType: 'json',
                           success : function(data)
                           {      //alert("Data is:"+data);
                                  jsonLeadData = data;
                                  table = $('#inCompleteCallbackDetailsTable').DataTable({
                               	  		 "aaData" : data,
                               	  		 "destroy": true,
                                         "bFilter" : false,
                                         "processing" : true,
                                         "bPaginate" : true,
                                         "pageLength": 10,
                                         "serverSide" : false,      
                                         "bLengthChange" : false,
                                         "bSortable" : false,
                                         "bInfo": false,
										 "columnDefs": [
							                {
                                                "defaultContent": "-",
                                                "targets": 7,
                                                "render": function (data, type, full, meta){
                                                       var isAdmin = document.getElementById("userAdminRole").value;
                                                       var isNOCManager = document.getElementById("isNOCManager").value;
                                                       var isExternal = document.getElementById("isExternalUser").value;
                                                       var loggedInUserEmailId = document.getElementById("emailId").value;
                                                       var isTTlabs = document.getElementById("isTTlabs").value;
                                                       var columnValue = full[7].split("@@@");
                                                       var deleteLeadId = columnValue[0];
                                                       var emailIdFromLead = columnValue[1];
                                                       if(isAdmin == "true"){//CustomerPortalAdmin OR EndCustomerAdmin
                                                    	   if(isExternal == "true"){//EndCustomerAdmin -- External Admin
                                                    		   // FIF VEC
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		   return '<a name="id[]" value="' 
                                                               + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                    	   }
                                                    	   else{//CustomerPortalAdmin --  Internal Admin
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		   return '<a name="id[]" value="' 
                                                               + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                    	   }
                                                       }
                                                       else if (isNOCManager == "true") {//NOCManager -- Internal
                                                    	   
                                                    	// See ALL
														// Edit Self created for ALl
									   								if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		 $('#createCallbckId').removeAttr('disabled');
		                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
		                                                                      $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                             + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }else{
                                                                    	 	 $('#createCallbckId').removeAttr('disabled');
                                                                    	 	$('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                    	 	$('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="Cannot cancel callback of other Users"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }
														
													   }
                                                       else if (isExternal == "true") {//Other External user
														// See All
														//loggedinuser
																	if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
	                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
	                                                                      $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                             + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                    	  $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="Cannot cancel callback of other Users"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }
														
														}
                                                       else if (isTTlabs ==  "true" && isExternal != "true"){//Other Internal and TTLabs
                                                   	   //Create and edit self
                                                   	   			if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
	                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
	                                                                      $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                             + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                    	  $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="Cannot cancel callback of other Users"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }
                                                    	}
                                                       else{//Internal not TTlabs
                                                    	   //ToDisable Create Callback
                                                    	   $('#createCallbckId').attr('disabled','disabled');
                                                    	   $('#createCallbckId').addClass("img-create-callback-disabled");
                                                    	   $('#createCallbckId').removeClass("img-create-callback");
                                                    	   $('#createCallbckId').attr("title", "Cannot Raise request for other companies.");
                                                    	   //All freeze All View
                                                    	   return '<a name="id[]" title="Cannot cancel other companies callbacks"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                    	  
                                                       }
                                                }
                                           },
                                           {
                                                "defaultContent": "-",
                                                "targets": 6,
                                                "render": function (data, type, full, meta){
                                                       var isAdmin = document.getElementById("userAdminRole").value;
                                                       var isNOCManager = document.getElementById("isNOCManager").value;
                                                       var isExternal = document.getElementById("isExternalUser").value;
                                                       var loggedInUserEmailId = document.getElementById("emailId").value;
                                                       var isTTlabs = document.getElementById("isTTlabs").value;
                                                       var columnValue = full[6].split("@@@");
                                                       var editLeadId = columnValue[0];
                                                       var emailIdFromLead = columnValue[1];
                                                       if(isAdmin == "true"){//CustomerPortalAdmin OR EndCustomerAdmin
                                                    	   if(isExternal == "true"){//EndCustomerAdmin -- External Admin
                                                    		   // FIF VEC
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		  return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                    	   }
                                                    	   else{//CustomerPortalAdmin --  Internal Admin
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		   return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                    	   }
                                                       }
                                                       else if (isNOCManager == "true") {//NOCManager -- Internal
                                                    	   
                                                    	// See ALL
														// Edit Self created for ALl
									   								if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		 $('#createCallbckId').removeAttr('disabled');
                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                           		   $('#createCallbckId').addClass("img-create-callback");
                                                                            return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                                      }else{
                                                                    	 	 $('#createCallbckId').removeAttr('disabled');
                                                                    	 	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                   		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="Cannot edit callback of other Users"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                                      }
														
													   }
                                                       else if (isExternal == "true") {//Other External user
														// See All
														//loggedinuser
																	if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                           		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                               		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="Cannot edit callback of other Users"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                                      }
														
														}
                                                       else if (isTTlabs ==  "true" && isExternal != "true"){//Other Internal and TTLabs
                                                   	   //Create and edit self
                                                   	   			if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                           		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                               		   $('#createCallbckId').addClass("img-create-callback");
                                                                            return '<a name="id[]" title="Cannot edit callback of other Users"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                                      }
                                                    	}
                                                       else{//Internal not TTlabs
                                                    	   
														   //ToDisable Create Callback
														   $('#createCallbckId').attr('disabled','disabled');
														   $('#createCallbckId').addClass("img-create-callback-disabled");
                                                		   $('#createCallbckId').removeClass("img-create-callback");
                                                		   $('#createCallbckId').attr("title", "Cannot Raise request for other companies.");
														   //All freeze All View
                                                    	   return '<a name="id[]" title="Cannot Edit other companies callbacks"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                    	   
                                                    	   
                                                       }
                                                }
                                           },
                                           {
                                                "defaultContent": "-",
                                                "targets": 7,
                                                "render": function (data, type, full, meta){
                                                       return '<div title="'+ $('<div/>').text(data).html() +'" >'+ $('<div/>').text(data).html() +'</div>'
                                                }
                                           },
                                           {
                                        	   "sClass": "comments", 
                                        	   "aTargets": [4]
                                           }
                                           ],
                                                                                                              
                                         'order': [[1, "asc"]]
                                         });
                           },
                           error: function()
                           {
                                  //alert("error");
                           }
                     });
              }
              
              function callbackDetailsCompleteTableAjax()
              {      
                     //alert("Function Called..");
                     var callbackDetailsURL = "${completeCallbackDetailsURL}";
                     //alert("URL:::"+callbackDetailsURL)
                     $.ajax({
                           url: callbackDetailsURL,
                           type: "POST",
                           dataType: 'json',
                           success : function(data)
                           {      //alert("Data is:"+data);
                                  table = $('#completeCallbackDetailsTable').DataTable({
                                         "aaData" : data,
                                         "destroy": true,
                                         "bFilter" : false,
                                         "processing" : true,
                                         "bPaginate" : true,
                                         "pageLength": 10,
                                         "serverSide" : false,      
                                         "bLengthChange" : false,
                                         "bSortable" : false,
                                         "bInfo": false,
                                         "columnDefs": [
                                         {
                                      	   "sClass": "comments", 
                                      	   "aTargets": [4]
                                         }],
                                         'order' : [[1,"asc"]]
                                         });
                           },
                           error: function()
                           {
                                  //alert("error");
                           }
                     });
              }
              
              function editCallback(link){
                     var leadId= link.getAttribute("value");
                     
                     for(var i=0;i<jsonLeadData.length;i++){
                    	 if(leadId==jsonLeadData[i][6]){
         					var temp = jsonLeadData[i][6];
         					var leadIdTemp = temp.split("@@@");
                                           //alert(jsonLeadData[i]);
                                           editLeadId = leadIdTemp[0] ;
                                           editName = jsonLeadData[i][0];
                                           editPhoneNumber = jsonLeadData[i][2];
                                           editCategory = jsonLeadData[i][5];
                                           editCallbackDate = jsonLeadData[i][1];
                                           editComments = jsonLeadData[i][4];
                                           editIpscapeForm();

                                  
                           }
                     }
                     $("#editCallbackModal").modal('show');
                     
              }
              
              function editIpscapeForm(){
                                  resetIpscapeEditForm();
                                  
                                  
                                  var dateElement = editCallbackDate.split(" ");
                                  var currentDate = dateElement[0];
                                  var currentTime = dateElement[1];
                                  //alert(dateElement);
                                  var currentDateArray = currentDate.split("/");
                                  var currentTimeArray = currentTime.split(":");
                                         
                                  var dateToSet = new Date(currentDateArray[2] + '/' + currentDateArray[1] + '/' + currentDateArray[0]);
                                  //alert(currentDate);
                                  var editHours = currentTimeArray[0];
                                  var editMin = currentTimeArray[1];
                                  
                                  if(parseInt(editHours) >= 12){
                                         $("#pmradioEdit").attr('checked', true);
                                         if(parseInt(editHours) != 12){
                                                editHours = parseInt(editHours) - 12;
                                         }
                                  }else{
                                         $("#amradioEdit").attr('checked', true);
                                  }
                                  
                                  
                                  //$('select option:contains('+editHours + ':' editMin+')').prop('selected',true);
                                  //$("#rangeTimeEdit option[text='07:00']").attr("selected","selected");
                                  seletedTime = editHours +":"+ editMin;
                                  selectItem = "selectTime";
                                  $("select[name='rangeTimeEdit']").append('<option value="'+selectItem+'">'+seletedTime+'</option>');
                                  $('#rangeTimeEdit [value='+selectItem+']').prop('selected', true);
                                  
                                  
                                  $("#leadIdEdit").val(editLeadId);
                                  $("#nameEdit").val(editName);
                                  $("#phoneEdit").val(editPhoneNumber);
                                  $("#categoryEdit").val(editCategory);
                                  $("#notesEdit").val(editComments);
                                  
                                  $("#callbackDateEdit").datepicker("setDate", dateToSet);
                                  
              }
	function confirmDeleteCallback(link){
			confirmdeleteleadId= link.getAttribute("value");
			$("#deleteCallbackConfirm").modal('show');
		}
        	
         function deleteCallback(){
                     
			$("#deleteCallbackConfirm").modal('hide');
			                     
		//	var confirm = document.getElementById("deleteCallbackConfirm").value;
		//	alert(confirm);
			 

                     var deleteCallbackURL = "${deleteCallbackURL}"+"&leadId="+confirmdeleteleadId;
                           
                           $.ajax({
                           url: deleteCallbackURL,
                           type: "POST",
                           dataType: 'json',
                           success : function(data)
                           {      //alert("one");
                                  var status = data.deleteCallback;
                                  //alert("status::"+status);
                                  if(status == "success"){
                                         //alert("SUCCESS");
                                         callbackDetailsIncompleteTableAjax();
                                         callbackDetailsCompleteTableAjax();
                                         document.getElementById("deleteLeadResponse").innerHTML = '<liferay-ui:message key="Callback Request is cancelled successfully"/>';
                                         $("#deleteCallbackResponse").modal('show'); 
                                  }else{
                                         //alert("FAIL");
                                         callbackDetailsIncompleteTableAjax();
                                         callbackDetailsCompleteTableAjax();
                                         document.getElementById("deleteLeadResponse").innerHTML = '<liferay-ui:message key="Callback Cancel request failed. Please try again."/>';
                                         $("#deleteCallbackResponse").modal('show'); 
                                  }                                 
                           },
                           error: function()
                           {
                                  //alert("error");
                           }
                     });
              }
              
    