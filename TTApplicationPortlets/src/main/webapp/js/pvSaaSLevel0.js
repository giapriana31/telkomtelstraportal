//new code//


var w = 280;
var h = 200;
var r = 90;
var ir = 80;
var textOffset = 14;
var tweenDuration = 500;

//OBJECTS TO BE POPULATED WITH DATA LATER
var lines = Array();
var valueLabels = Array();
var nameLabels = Array();
var pieData = [];
var oldPieData = [];
var filteredPieData = [];

//D3 helper function to populate pie slice parameters from array data
var donut = d3.layout.pie().value(function(d) {
  return d.octetTotalCount;
});


//D3 helper function to create colors from an ordinal scale

//var color = d3.scale.category20();
//var colors = ['#E32212', '#FF9C00', '#A3CF62', '#000000', '#8005D8', '#30BFFC'];

//var colors = ['#E32212', '#FF9C00', '#A3CF62'];

var siteColors = [colors.slice(0),colors.slice(0),colors.slice(0)];
var color = function(index,siteIndex) {
    if (index < siteColors[siteIndex].length)
      return siteColors[siteIndex][index];
    else
      return '#888888';
  }
  
  
  //D3 helper function to draw arcs, populates parameter "d" in path object
var arc = d3.svg.arc()
  .startAngle(function(d) {
    return d.startAngle;
  })
  .endAngle(function(d) {
    return d.endAngle;
  })
  .innerRadius(ir)
  .outerRadius(r);
  
  


///////////////////////////////////////////////////////////
//GENERATE FAKE DATA /////////////////////////////////////
///////////////////////////////////////////////////////////

var streakerDataAdded;

function findCoordinates(r, angle) {
  //alert(angle);
  angle = angle - Math.PI / 2;
  return {
    x: Math.cos(angle) * r,
    y: Math.sin(angle) * r
  };
}

function fillArray() {
  return {
    port: "",
    octetTotalCount: 10
  };
}



///////////////////////////////////////////////////////////
//CREATE VIS & GROUPS ////////////////////////////////////
///////////////////////////////////////////////////////////
//var sites = ['gold', 'silver', 'bronze'];
var sites = ['saas'];
var vis = Array();
var arc_group = Array();
var center_group = Array();
var paths = Array();
var label_group = Array();
var grayCircle = Array();
var whiteCircle = Array();
var totalLabel = Array();
var totalValue = Array();

var graphColorCode = [];

var count = 0;

//Temp Values
/*var ttOfficeBuildingSilverColorMap={Green:5};
var ttOfficeBuildingGoldColorMap={Red:0,Green:0,Amber:3};
var ttOfficeBuildingBronzeColorMap={Green:2};*/


//var ttOfficeBuildingSilverColorMap={Green:5};
//var ttOfficeBuildingGoldColorMap={Red:0,Green:0,Amber:3};
//var ttOfficeBuildingBronzeColorMap={Green:2};


		for(var graphColor in colors ){
		
			var clr = colors[graphColor];
			graphColorCode.push(graphColorData[clr]);
		

		
	}	
	
	


var graphJsonObject=[];


for (i = 0; i < graphColorCode.length; i++) { 
	graphJsonObject.push({octetTotalCount: graphColorCode[i]});
}




var totalData=[];

totalData.push(graphJsonObject);





    for(var p = 0;p<totalData.length;p++){
      var tempArray =  totalData[p];
      for(var q=tempArray.length-1;q>=0;q--){
        if(tempArray[q].octetTotalCount<1){
          tempArray.splice(q,1);
          siteColors[p].splice(q,1);
        }
      }
    }

for (var i = 0; i < sites.length; i++) {
  vis[i] = d3.select("#" + sites[i]).append("svg:svg")
    .attr("width", w)
    .attr("height", h);

  //	GROUP FOR ARCS/PATHS
  arc_group[i] = vis[i].append("svg:g")
    .attr("class", "arc")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

  //	GROUP FOR CENTER TEXT  
  center_group[i] = vis[i].append("svg:g")
    .attr("class", "center_group")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

  //	PLACEHOLDER GRAY CIRCLE
  paths[i] = arc_group[i].append("svg:circle")
    .attr("fill", "#EFEFEF")
    .attr("r", r);

  //	GROUP FOR LABELS
  label_group[i] = vis[i].append("svg:g")
    .attr("class", "label_group")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

  ///////////////////////////////////////////////////////////
  //	CENTER TEXT ////////////////////////////////////////////
  ///////////////////////////////////////////////////////////

  //	WHITE CIRCLE BEHIND LABELS
  grayCircle[i] = center_group[i].append("svg:circle")
    .attr("fill", "#eeeeee")
    .attr("r", ir);

  whiteCircle[i] = center_group[i].append("svg:circle")
    .attr("fill", "white")
    .attr("r", ir - 7);

  //	"TOTAL" LABEL
  totalLabel[i] = center_group[i].append("svg:text")
    .attr("class", "label")
    .attr("dy", 15)
    .attr("text-anchor", "middle") // text-align: right
    .text("Total");

  //	TOTAL TRAFFIC VALUE
  totalValue[i] = center_group[i].append("svg:text")
    .attr("class", "total")
    .attr("dy", -5)
    .attr("text-anchor", "middle") // text-align: right
    .text("0");

}
///////////////////////////////////////////////////////////
//STREAKER CONNECTION ////////////////////////////////////
///////////////////////////////////////////////////////////

for (var j = 0; j < sites.length; j++) {
  draw(j);
  draw(j);
  
}

//to run each time data is generated
function draw(siteIndex) {
    streakerDataAdded = totalData[siteIndex];
    oldPieData = filteredPieData;
    pieData = donut(streakerDataAdded);
    
    var totalOctets = 0;

    function filterData(element, index, array) {
      element.name = streakerDataAdded[index].port;
      element.value = streakerDataAdded[index].octetTotalCount;
      totalOctets += element.value;
      return (element.value >= 0);
    }
    filteredPieData = pieData.filter(filterData);
    if (filteredPieData.length > 0 && oldPieData.length > 0) {

      //REMOVE PLACEHOLDER CIRCLE
      arc_group[siteIndex].selectAll("circle").remove();

      totalValue[siteIndex].text(function() {

        return totalOctets;
        //return bchart.label.abbreviated(totalOctets*8);
      });

      //DRAW TICK MARK LINES FOR LABELS
      lines[siteIndex] = label_group[siteIndex].selectAll("line").data(filteredPieData);
      lines[siteIndex].enter().append("svg:line")
        .attr("x1", function(d) {
          return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).x;
        })
        .attr("x2", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
            return "-" + (r + 10);
          else
            return "" + (r + 10);
        })
        .attr("y1", function(d) {
          return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
        })
        .attr("y2", function(d) {
          return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
        })
        .attr("stroke", function(d, i) {
          return color(i,siteIndex);
        });
      lines[siteIndex].transition()
        .duration(tweenDuration);
      lines[siteIndex].exit().remove();

      //DRAW ARC PATHS

      paths[siteIndex] = arc_group[siteIndex].selectAll("path").data(filteredPieData);
      paths[siteIndex].enter().append("svg:path")
        .attr("stroke", "white")
        .attr("stroke-width", 0.5)
        .attr("fill", function(d, i) {
          return color(i,siteIndex);
        })
        .transition()
        .duration(tweenDuration)
        .attrTween("d", pieTween);
      paths[siteIndex]
        .transition()
        .duration(tweenDuration)
        .attrTween("d", pieTween);
      paths[siteIndex].exit()
        .transition()
        .duration(tweenDuration)
        .attrTween("d", removePieTween)
        .remove();

      //DRAW LABELS WITH PERCENTAGE VALUES
      valueLabels[siteIndex] = label_group[siteIndex].selectAll("text.value").data(filteredPieData)
        .attr("y", function(d) {
          return "" + (findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
        })
        .attr("x", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
            return "-" + (r + 10);
          else
            return "" + (r + 10);
        })
        .attr("fill", function(d, i) {
          return color(i,siteIndex);
        })
        .attr("text-anchor", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
            return "end";
          else
            return "begning";
        })
        .text(function(d) {
          return d.value;
        });

      valueLabels[siteIndex].enter().append("svg:text")
        .attr("class", "value")
        .attr("y", function(d) {
          return "" + (findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
        })
        .attr("x", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
            return "-" + (r + 10);
          else
            return "" + (r + 10);
        })
        .attr("fill", function(d, i) {
          return color(i,siteIndex);
        })
        .attr("text-anchor", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
            return "end";
          else
            return "begning";
        })
        .text(function(d) {
          return d.value;
        });

      valueLabels[siteIndex].exit().remove();

      //DRAW LABELS WITH ENTITY NAMES
      nameLabels[siteIndex] = label_group[siteIndex].selectAll("text.units").data(filteredPieData)
        .attr("dy", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2 && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
            return 17;
          } else {
            return 5;
          }
        })
        .attr("text-anchor", function(d) {
          if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
            return "beginning";
          } else {
            return "end";
          }
        }).text(function(d) {
          return d.name;
        });

      nameLabels[siteIndex].enter().append("svg:text")
        .attr("class", "units")
        .attr("transform", function(d) {
          return "translate(" + Math.cos(((d.startAngle + d.endAngle - Math.PI) / 2)) * (r + textOffset) + "," + Math.sin((d.startAngle + d.endAngle - Math.PI) / 2) * (r + textOffset) + ")";
        })
        .attr("dy", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2 && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
            return 17;
          } else {
            return 5;
          }
        })
        .attr("text-anchor", function(d) {
          if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
            return "beginning";
          } else {
            return "end";
          }
        }).text(function(d) {
          return d.name;
        });

      nameLabels[siteIndex].transition().duration(tweenDuration).attrTween("transform", textTween);

      nameLabels[siteIndex].exit().remove();
    }
  }
  ///////////////////////////////////////////////////////////
  //FUNCTIONS //////////////////////////////////////////////
  ///////////////////////////////////////////////////////////

//Interpolate the arcs in data space.
function pieTween(d, i) {
  var s0;
  var e0;
  if (oldPieData[i]) {
    s0 = oldPieData[i].startAngle;
    e0 = oldPieData[i].endAngle;
  } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
    s0 = oldPieData[i - 1].endAngle;
    e0 = oldPieData[i - 1].endAngle;
  } else if (!(oldPieData[i - 1]) && oldPieData.length > 0) {
    s0 = oldPieData[oldPieData.length - 1].endAngle;
    e0 = oldPieData[oldPieData.length - 1].endAngle;
  } else {
    s0 = 0;
    e0 = 0;
  }
  var i = d3.interpolate({
    startAngle: s0,
    endAngle: e0
  }, {
    startAngle: d.startAngle,
    endAngle: d.endAngle
  });
  return function(t) {
    var b = i(t);
    return arc(b);
  };
}

function removePieTween(d, i) {
  s0 = 2 * Math.PI;
  e0 = 2 * Math.PI;
  var i = d3.interpolate({
    startAngle: d.startAngle,
    endAngle: d.endAngle
  }, {
    startAngle: s0,
    endAngle: e0
  });
  return function(t) {
    var b = i(t);
    return arc(b);
  };
}

function textTween(d, i) {
  var a;
  if (oldPieData[i]) {
    a = (oldPieData[i].startAngle + oldPieData[i].endAngle - Math.PI) / 2;
  } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
    a = (oldPieData[i - 1].startAngle + oldPieData[i - 1].endAngle - Math.PI) / 2;
  } else if (!(oldPieData[i - 1]) && oldPieData.length > 0) {
    a = (oldPieData[oldPieData.length - 1].startAngle + oldPieData[oldPieData.length - 1].endAngle - Math.PI) / 2;
  } else {
    a = 0;
  }
  var b = (d.startAngle + d.endAngle - Math.PI) / 2;

  var fn = d3.interpolateNumber(a, b);
  return function(t) {
    var val = fn(t);
    return "translate(" + Math.cos(val) * (r + textOffset) + "," + Math.sin(val) * (r + textOffset) + ")";
  };
}