
map = new L.map('map',{
	   	    	 //worldCopyJump: true
				 zoomControl: false
	   	         }).setView([-4, 120], 5);    
	     	       
	         L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {    
	     	 attribution: '&copy; OpenStreetMap contributors',     
	     	 
	     	 }).addTo( map );

map.dragging.disable();
map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();
map.keyboard.disable();
if (map.tap) map.tap.disable();


  

function drawTable(data) {
    
    
    for (var i = 0; i < data.length; i++) {
      //  alert(data[i]);
        drawRow(data[i]);
    }
}


function drawTable(data) {
    
    
    for (var i = 0; i < data.length; i++) {
      //  alert(data[i]);
        drawRow(data[i]);
    }
}

function drawRow(rowData) {
   //alert(rowData.numberofGreenSite);
//    alert(rowData.latitude);
    //alert(rowData.longitude );
    var popupdata =  drawPopUpTableData(rowData);
   // alert (popupdata);

    // Assigning colour to hex values  
    if( rowData.networkProvidenceColour== "Red")
        rowData.networkProvidenceColour = "#B80028";
    else if( rowData.networkProvidenceColour== "Ambar")
        rowData.networkProvidenceColour ="#FFC200";
   else
        rowData.networkProvidenceColour = "#28A828";   
        
L.circle([rowData.latitude, rowData.longitude ], 50000, {
			color: rowData.networkProvidenceColour,
			fillColor: rowData.networkProvidenceColour,
			fillOpacity: 0.5
		}).bindPopup(popupdata, {noHide: true, className: "my-label", offset: [15, -35]}).addTo(map);

    
var canvasTiles = L.tileLayer.canvas();

canvasTiles.drawTile = function(canvas, tilePoint, zoom) {
	var ctx = canvas.getContext('2d');
      
};

canvasTiles.addTo(map);
    
}
function drawPopUpTableData(rowData) {
     var row = "<table bgcolor='#D8F0DA' background='transparent'>";
     if(rowData.numberofRedSite >0)
     row +="<tr><td align=" +"center"+" bgcolor="+ "#FF0000" +">" + rowData.numberofRedSite + "</td></tr>";
    if(rowData.numberofAmberSite >0)
     row +="<tr><td align=" +"center"+" bgcolor="+ "#FFC200" +">" + rowData.numberofAmberSite + "</td></tr>";
     if(rowData.numberofGreenSite >0)
     row +="<tr><td align=" +"center"+" bgcolor="+ "#28A828" +">" + rowData.numberofGreenSite + "</td></tr>";
     row += "</table>";   
 
     return row;
}

