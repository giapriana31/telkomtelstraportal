<%@page import="com.tt.constants.GenericConstants"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="java.util.Locale" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ include file="common.jsp" %>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="<%= request.getContextPath() %>/js/jquery-1.11.3.min.js"></script>
  <script src="<%= request.getContextPath() %>/js/bootstrap-3.3.4.min.js"></script>
</head>

<liferay-theme:defineObjects />
<portlet:defineObjects />


<c:set var="lang" scope="session" value="English"/>
<div class="container-fluid">

<%
	String lang = "en_US";	
%>
<c:choose>
    <c:when test="<%= themeDisplay.isSignedIn() %>">   
<%
	if(user.getExpandoBridge().getAttribute("Language")!=null)
	{
		lang = user.getLocale().toString();
	}
	String vartab = PortalUtil.getOriginalServletRequest(request).getParameter("tabvar");
	String selectedTab =null;
	PortletURL portletURL = renderResponse.createRenderURL();
	if(vartab==null)
	{
		String tabs1 = ParamUtil.getString(request,"tabs","tab1");
	    selectedTab = ParamUtil.getString(request, "tab", "0");
	    portletURL.setParameter("tabs", tabs1);
	    portletURL.setParameter("tab", selectedTab);
	}
	else
	{
		selectedTab = vartab;
		portletURL.setParameter("tab", vartab);
	}
	
	/* get view/page from some method */
	String currentView = "customerview";
	String currentPage  = "profiles";
	
	/* current logged in user's User object*/
	user = TTGenericUtils.getLoggedInUser(request);
	
	/* Forming a key for specific view,page and functionality */
	StringBuilder allowedTabs = new StringBuilder();
	String viewProfileSetingsFeature = currentView + "." + currentPage + "." + GenericConstants.PROFILESETTINGS_FEATURE;
	String viewSecuritySettingsFeature = currentView + "." + currentPage + "." + GenericConstants.SECURITYSETTINGS_FEATURE;
	String viewManagedUserFeature = currentView + "." + currentPage + "." + GenericConstants.MANAGEUSERS_FEATURE;
	
	if(TTGenericUtils.hasAccessToUser(user, viewProfileSetingsFeature))
		allowedTabs.append("Profile Settings,");
	
	if(TTGenericUtils.hasAccessToUser(user, viewSecuritySettingsFeature))
		allowedTabs.append("Security Settings,");
		
	if(TTGenericUtils.hasAccessToUser(user, viewManagedUserFeature))
		allowedTabs.append("Manage Users");
	
%>
<div id="settingsContainer"> 
<div id="settingsHeader"><div id="settingsIcon"></div><div id="settingsHeaderContent"><liferay-ui:message key="Settings"/></div></div>
<liferay-ui:tabs names="<%=allowedTabs.toString() %>" param="tab" url="<%=portletURL.toString()%>" tabsValues="<%=allowedTabs.toString() %>" value="<%=selectedTab%>" refresh="true">
    <liferay-ui:section>
        <%@ include file="/view/Profile.jsp" %>
    </liferay-ui:section>
    <liferay-ui:section>
        <%@ include file="/view/Security.jsp" %>
    </liferay-ui:section>
    
    <%	
    	if(TTGenericUtils.hasAccessToUser(user, viewManagedUserFeature))
    	{
    %>
	     <liferay-ui:section>
	        <%@ include file="/view/userManagment.jsp" %>
	     </liferay-ui:section>
    <%
    	}
    %>
</liferay-ui:tabs>  
</div>
 </c:when>
	<c:otherwise>
    	<liferay-ui:message key="please sign-in to view profile setting."/>
    </c:otherwise>
</c:choose>    


<%-- <script src="<%= request.getContextPath() %>/js/1.10.2-jquery.min.js"></script> --%>

<script src="<%= request.getContextPath() %>/js/jquery.form.validate-2.2.1.min.js"></script>
<script>
/* requiredFields: 'You have not answered all required fields', */
var en_US = {
				
		  badConfirmPassword:'The password does not match',
	    	selectQuestion:'Please Select a Question',
	    	validateAnswer:'Please select Question',
	    	newPassword:'Passwords must have at least 7 characters, a lowercase letter, an uppercase letter, a number, a symbol (@#$%!^&*),no parts of your username',
	    		passwordPattern:'The password does not match the required format',
	    		validateAnswer:'Please Select Question',
        errorTitle: 'Form submission failed!',
        requiredFields: 'Please enter data in required fields',
        badTime: 'You have not given a correct time',
        badEmail: 'You have not given a correct e-mail address',
        badTelephone: 'You have not given a correct phone number',
        badSecurityAnswer: 'You have not given a correct answer to the security question',
        badDate: 'You have not given a correct date',
        lengthBadStart: 'The input value must be between ',
        lengthBadEnd: ' characters',
        lengthTooLongStart: 'The input value is longer than ',
        lengthTooShortStart: 'The input value is shorter than ',
        notConfirmed: 'Input values could not be confirmed',
        badDomain: 'Incorrect domain value',
        badUrl: 'The input value is not a correct URL',
        badCustomVal: 'The input value is incorrect',
        andSpaces: ' and spaces ',
        badInt: 'The input value was not a correct number',
        badNumber : 'Only \'+\' and Digits are allowed',
        badSecurityNumber: 'Your social security number was incorrect',
        badUKVatAnswer: 'Incorrect UK VAT Number',
        badStrength: 'The password isn\'t strong enough',
        badNumberOfSelectedOptionsStart: 'You have to choose at least ',
        badNumberOfSelectedOptionsEnd: ' answers',
        badAlphaNumeric: 'The input value can only contain alphanumeric characters ',
        badAlphaNumericExtra: ' and ',
        wrongFileSize: 'The file you are trying to upload is too large (max %s)',
        wrongFileType: 'Only files of type %s is allowed',
        groupCheckedRangeStart: 'Please choose between ',
        groupCheckedTooFewStart: 'Please choose at least ',
        groupCheckedTooManyStart: 'Please choose a maximum of ',
        groupCheckedEnd: ' item(s)',
        badCreditCard: 'The credit card number is not correct',
        badCVV: 'The CVV number was not correct',
        	validateCustomQuestion:'Please provide a question',
        	validateCustomQuestionLength:'The question should be of minimum 6 digits'
    };
    
var in_ID = {
		errorTitle: 'Formulir pengajuan gagal!',
	    requiredFields: 'Masukkan data dalam bidang yang diperlukan',
	    badTime: 'Anda telah tidak diberi waktu yang tepat ',
	    badEmail: 'Anda belum memberikan alamat e -mail yang benar',
	    badTelephone: 'Anda belum memberikan nomor telepon yang benar',
	    badSecurityAnswer: 'Anda belum memberikan jawaban yang benar untuk pertanyaan keamanan',
	    badDate: 'Anda belum memberikan tanggal yang benar',
	    lengthBadStart: 'Nilai masukan harus antara',
	    lengthBadEnd: 'karakter',
	    lengthTooLongStart: 'Nilai masukan lebih panjang dari',
	    lengthTooShortStart: 'Nilai masukan lebih pendek dari',
	    notConfirmed: 'nilai-nilai input tidak dapat dikonfirmasi',
	    badDomain: 'Salah nilai domain',
	    BADURL: 'Nilai masukan bukan URL yang benar',
	    badCustomVal: 'Nilai masukan salah',
	    andSpaces: 'dan ruang',
	    badInt: 'Nilai masukan bukan nomor yang benar',
	    badNumber : ' Hanya \'+\' dan Digit diperbolehkan',
	    badSecurityNumber: 'nomor jaminan sosial Anda tidak benar',
	    badUKVatAnswer: 'Salah Inggris PPN Nomor',
	    badStrength: 'password isn \' t cukup kuat ',
	    badNumberOfSelectedOptionsStart: 'Anda harus memilih setidaknya',
	    badNumberOfSelectedOptionsEnd: 'jawaban',
	    badAlphaNumeric: 'Nilai masukan hanya dapat berisi karakter alfanumerik',
	    badAlphaNumericExtra: 'dan',
	    wrongFileSize: 'Berkas yang Anda mencoba untuk meng-upload terlalu besar (max% s)',
	    wrongFileType: 'Hanya file jenis% s diperbolehkan',
	    groupCheckedRangeStart: 'Silakan memilih antara',
	    groupCheckedTooFewStart: 'Silakan pilih setidaknya',
	    groupCheckedTooManyStart: 'Pilihlah maksimal',
	    groupCheckedEnd: 'item (s)',
	    badCreditCard: 'Jumlah kartu kredit tidak benar',
	    badCVV: 'Jumlah CVV tidak benar',
	    badConfirmPassword:'Sandi tidak cocok',
	    	selectQuestion:'silahkan pilih pertanyaan',
	    	validateAnswer:'Silakan Pilih Pertanyaan',
	    	newPassword:'Password harus memiliki minimal 7 karakter, huruf kecil, huruf besar, angka, simbol (@ # $% ^ & *), tidak ada bagian dari nama pengguna',
	    		passwordPattern:'Sandi tidak cocok format yang diperlukan',
	    		validateAnswer:'Silakan Pilih Pertanyaan',
	    			validateCustomQuestion:'Harap memberikan pertanyaan',
	    				validateCustomQuestionLength:'Pertanyaan harus minimal 6 digit'
		};
	    
	    
	    
	    
	$.formUtils.addValidator({
	  name : 'mobile_no',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {
		  	return (!isNaN(value.replace(new RegExp("\\+", 'g'), '')));		
	  },
	  errorMessage : 'Only \'+\' and Digits are allowed',
	  errorMessageKey: 'badNumber'
	});
	
  $.validate({
	  language : <%=lang%>
  });
  
  
  $.formUtils.addValidator({
	  name : 'password',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {	
		  var pattern=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{7,40}$/;  
		 //	 var pattern=/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,40}$/ 	
		 return (pattern.test(value));		
		  	
	  },
	  errorMessage : 'The password does not match the required format',
	  errorMessageKey: 'passwordPattern'
	});
  
  
  $.formUtils.addValidator({
	  name : 'newPassword',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {	
		 var pattern=/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{7,40}$/;
		 return (pattern.test(value));		
		  	
	  },
	  errorMessage : 'Passwords must have at least 7 characters, a lowercase letter, an uppercase letter, a number, a symbol (@#$%!^&*),no parts of your username',
	  errorMessageKey: 'newPassword'
	});
  
  

  $.formUtils.addValidator({
	  name : 'confirmPass',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {	
		  
		  var password=$('#newPassword');  
		  var confirmPassword=$('#confirmPassword')
		 
		 
		 
		  
		  return (confirmPassword.val()==password.val());	
		  
		  	
	  },
	  errorMessage : 'The password does not match',
	  errorMessageKey: 'badConfirmPassword'
	});
  
  
  $.formUtils.addValidator({
	  name : 'securityQuestion',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {	
		  
		  var question=value;
		 var answer=$('#answer').val();  
		
		 
		if( question =='#')	{
			
			return(false);
			
			
		}
		else{
			
			return(true);
			
		}
		  
		  
		  
		  	
	  },
	  errorMessage : 'Please Select a Question',
	  errorMessageKey: 'selectQuestion'
	});
  
  
  $.formUtils.addValidator({
	  name : 'validateAnswer',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {	
		  
		  
		 var question=$('#securityQuestion');  
		
		 
		 
		  
		  return (question==null || question=='');	
		  
		  	
	  },
	  errorMessage : 'Please Select Question',
	  errorMessageKey: 'validateAnswer'
	});
  
  
  $.formUtils.addValidator({
	  name : 'validateCustomQuestion',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {	
		  
		  
		  var question=$("#securityQuestionDropDown :selected").text(); 
		
		 if(question=='Add your own custom question'){
			
			 if((value==null || value=='')){
				 return false;
				 
			 }
			 
			
			
		 }
		 return true;
		 
		 
		  
		  
		  
		  	
	  },
	  errorMessage : 'Please provide a question',
	  errorMessageKey: 'validateCustomQuestion'
	});
  
  $.formUtils.addValidator({
	  name : 'validateCustomQuestionLength',
	  validatorFunction : function(value, $el, config, language, $form) 
	  {	
		  
		  
		  var question=$("#securityQuestionDropDown :selected").text();

		
		 if((value.length<6) && (question=='Add your own custom question')){
			 
			return false;
			
		 }
		 return true;
		 
		  
		  
		  
		  	
	  },
	  errorMessage : 'The question should be of minimum 6 digits',
	  errorMessageKey: 'validateCustomQuestionLength'
	});
  
  
  
  
</script>
</div>