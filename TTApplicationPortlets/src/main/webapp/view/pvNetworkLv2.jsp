<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="datatableURI" var="datatableURI"></portlet:resourceURL>
<portlet:resourceURL id="listDatatable" var="listDatatable"></portlet:resourceURL>
<portlet:resourceURL id="getServiceRequestDetails" var="getServiceRequestDetails"></portlet:resourceURL>
<portlet:resourceURL id="getServiceRequestItemDetails" var="getServiceRequestItemDetails"></portlet:resourceURL>

<html>
<head>

	<style>
		@media (max-width: 500px){
		
			button#srListSearchBtn {
				margin-bottom: 0px;
			}

			.tt-create-service-ctr{
				padding-left: 0px !important;
				padding-right: 0px !important;
			}

			.rwd{
				display:none !important;
			}
			
			td:nth-child(2){
				display:none;
			}
			
			th:nth-child(2){
				display:none;
			}
			
			td:nth-child(3){
				display:none;
			}
			
			th:nth-child(3){
				display:none;
			}
			
			td:nth-child(4){
				display:none;
			}
			
			th:nth-child(4){
				display:none;
			}
		}

		@media (min-width: 501px) and (max-width: 767px){
			
			button#srListSearchBtn {
		    	margin-bottom: 0px;
			}
			
			.tt-create-service-ctr{
				padding-left: 0px !important;
				padding-right: 0px !important;
			}
		
			.wrapper updateInfoHolder{
				display:none !important;
			}
			
			.tt-create-service-ctr {
			    width: 0% !important;
			    float: right !important;
			}

			tt-service-br-header{
				margin-top:0px !important;
			
			}
		
			img{
				margin-top:0px !important;	
			}
		}

		#srHeaderDiv {
			padding: 20px 0 20px 20px;
			background-color: #fff;
			border-bottom: solid 1px #ccc;
			margin-bottom: 20px;
		}

		#pvsr-tt-icon{
			display:inline;
		}

		#pvsr-tt-header {
			display: inline;
			font-size: 20px;
			color: #333;
			padding-left: 10px;
			font-weight: bold;
		}

		.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
			cursor: default;
			color: #555 !important;
			border: 1px solid transparent;
			background: transparent;
			box-shadow: none;
			border: solid transparent !important;
		}

		div#listingDatatableDiv {
			padding: 15px;
		}

		table#listingDatatable {
			background-color: rgb(204, 204, 204);
		}

		#listingDatatable td {
			background-color: #fff;
			height: 45px;
		}

		a.paginate_button.disabled:hover {
			border-bottom: solid transparent !important;
			background-color: transparent !important;
		}

		a.paginate_button:hover {
			border-bottom: solid 2px #f00 !important;
			background-color: transparent !important;
		}

		a.paginate_button.current {
			border-bottom: solid 2px red !important;
		}

		button.normalSearch {
			background-image: url("../images/srch.png");
			height: 30px;
			width: 30px;
			margin-bottom: 10px;
			border-style: none;
		}

		.srlSearchBox {
			float: right;
		}

		.filterbox{
			display:inline-block;
			padding:5px;
			margin: 0 5px 0 5px;
			border: solid 1px #ccc;
		}
	</style>

	<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"	type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/js/fnReloadAjax.js" type="text/javascript"></script>
	<!--<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.dataTables.css">-->
	<!--<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.dataTables.min.css">-->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/metro.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/docs.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ttservicerequest.css">	
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/serviceRequestDetails.css">

	<script type="text/javascript">
		function getURLParameter(name) {
  			return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
		}

		$(document).ready(function(){
			//var data = [{"task_status_tenoss":"Stage 1","ciid":"CI0000011018","carriageserviceid":"INTVPNTEL-0023043008","status_ticares":"NEW","sitename":"TTL-JB-BDOCORPU","ciname":"TTLJBCOR01RT01C28","tenid":192,"siteid":"SI-000000018","customeruniqueid":"000151","task_stage_tenoss":"1","customername":"telkomtelstra LAB"},{"task_status_tenoss":"Stage 2","ciid":"CI0000011015","carriageserviceid":"4700340-0022343369","status_ticares":"Installation","sitename":"TTL-JK-JKTKOKAS02","ciname":"TTLJKKOK35RT02C29","tenid":193,"siteid":"SI-000000016","customeruniqueid":"000151","task_stage_tenoss":"2","customername":"telkomtelstra LAB"},{"task_status_tenoss":"Stage 1","ciid":"CI0000011011","carriageserviceid":"4700340-0022349885","status_ticares":"NEW","sitename":"TTL-JK-JKTKOKAS01","ciname":"TTLJKKOK35RT01C29","tenid":194,"siteid":"SI-000000015","customeruniqueid":"000151","task_stage_tenoss":"1","customername":"telkomtelstra LAB"}]
		    var val = getURLParameter('ciname');
		    //alert("val : " + val);
		    //var regExSearch = '^\\s' + val +'\\s*$';
		    var data = eval('${listNetworkTenossDatatable}');
		    var table = $('#networkTenossDatatable').DataTable({
		    	"aaData": data,
		    	"aoColumns": [{ "mData": "sitename"},
							  { "mData": "ciname"},
							  { "mData": "carriageserviceid"},
							  { "mData": "status_ticares"},
							  { "mData": "task_name_ticares"},
							  { "mData": "task_status_tenoss"},
							  { "mData": "task_name_tenoss"},
							  { "mData": "modified_date"},
							  { "mData": "targetProvisioningDate"},
							  { "mData": "targetInstallDate"}],
				"paging":   false,
		        "ordering": false,
		        "info":     false
		    });
		    //table.column(columnNo).search(regExSearch, true, false).draw();
		    table.column(1).search( val ? '^'+val+'$' : '', true, false ).draw(); 
		});
	</script>
	
</head>
<body>

<div id="listingDatatableDiv">
	<div class="span6 srlSearchBox">
		<button type="button" onclick="window.history.back()" class="btn-primary-tt backButton" style="margin-top: -16px;">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
	</div>
	<table id="networkTenossDatatable" class="dataTable striped border bordered" data-role="datatable" data-searching="true" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th id="sitename"><liferay-ui:message key="Site Name" /></th>	
				<th id="ciname"><liferay-ui:message key="CPE" /></th>
				<th id="carriageserviceid"><liferay-ui:message key="Carriage Service ID" /></th>
				<th id="status_ticares"><liferay-ui:message key="Status Ticares" /></th>
				<th id="task_name_ticares"><liferay-ui:message key="Task Name Ticares" /></th>
				<th id="task_status_tenoss"><liferay-ui:message key="Task Status Tenoss" /></th>
				<th id="task_name_tenoss"><liferay-ui:message key="Task Name Tenoss" /></th>
				<th id="modified_date"><liferay-ui:message key="Modified Date" /></th>
				<th id="targetProvisioningDate"><liferay-ui:message key="Target Provisioning Date" /></th>
				<th id="targetInstallDate"><liferay-ui:message key="Target Install Date" /></th>				
			</tr>
		</thead>
	</table>
</div>
</body>
</html>
