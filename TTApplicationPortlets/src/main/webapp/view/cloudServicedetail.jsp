<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ include file="common.jsp" %>


<html>
<head>
<style>
button,html input[type="button"]{
border-radius:5px !important;
}
@font-face 
{
  font-family: 'MyWebFont';
  src: url('fonts/GothamRoundedBook.woff') format('woff'),
       url('fonts/GothamRoundedBook.ttf') format('truetype');
}
body
{
 font-family: 'MyWebFont';
}

	/* CSS Triangles - see Trevor's post */
	.wrapper .tooltip:after {
	  border-left: solid transparent 10px;
	  border-right: solid transparent 10px;
	  border-top: solid #D4D4D4 10px;
	  bottom: -10px;
	  content: " ";
	  height: 0;
	  left: 50%;
	  margin-left: -13px;
	  position: absolute;
	  width: 0;
	}
	  
	.wrapper:hover .tooltip {
	  font-size: 8px;
	    color: black;
	  opacity: 1;
	  pointer-events: auto;
	  -webkit-transform: translateY(0px);
	     -moz-transform: translateY(0px);
	      -ms-transform: translateY(0px);
	       -o-transform: translateY(0px);
	          transform: translateY(0px);
	}

	/* IE can just show/hide with no transition */
	.lte8 .wrapper .tooltip {
	  display: none;
	}
	
	.lte8 .wrapper:hover .tooltip {
	  display: block;
	}
}
@media  (min-width: 500px) {
	.wrapper{
			display:none !important;
	}
}

</style>
<script>
"use strict";jQuery.base64=(function($){var _PADCHAR="=",_ALPHA="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",_VERSION="1.0";function _getbyte64(s,i){var idx=_ALPHA.indexOf(s.charAt(i));if(idx===-1){throw"Cannot decode base64"}return idx}function _decode(s){var pads=0,i,b10,imax=s.length,x=[];s=String(s);if(imax===0){return s}if(imax%4!==0){throw"Cannot decode base64"}if(s.charAt(imax-1)===_PADCHAR){pads=1;if(s.charAt(imax-2)===_PADCHAR){pads=2}imax-=4}for(i=0;i<imax;i+=4){b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6)|_getbyte64(s,i+3);x.push(String.fromCharCode(b10>>16,(b10>>8)&255,b10&255))}switch(pads){case 1:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6);x.push(String.fromCharCode(b10>>16,(b10>>8)&255));break;case 2:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12);x.push(String.fromCharCode(b10>>16));break}return x.join("")}function _getbyte(s,i){var x=s.charCodeAt(i);if(x>255){throw"INVALID_CHARACTER_ERR: DOM Exception 5"}return x}function _encode(s){if(arguments.length!==1){throw"SyntaxError: exactly one argument required"}s=String(s);var i,b10,x=[],imax=s.length-s.length%3;if(s.length===0){return s}for(i=0;i<imax;i+=3){b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8)|_getbyte(s,i+2);x.push(_ALPHA.charAt(b10>>18));x.push(_ALPHA.charAt((b10>>12)&63));x.push(_ALPHA.charAt((b10>>6)&63));x.push(_ALPHA.charAt(b10&63))}switch(s.length-imax){case 1:b10=_getbyte(s,i)<<16;x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_PADCHAR+_PADCHAR);break;case 2:b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8);x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_ALPHA.charAt((b10>>6)&63)+_PADCHAR);break}return x.join("")}return{decode:_decode,encode:_encode,VERSION:_VERSION}}(jQuery));
</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/sites.css" ></link>


<portlet:defineObjects />

<portlet:actionURL name="refreshRealTimeVMs" var="refreshRealTimeVMs"></portlet:actionURL>

<script>
	function VMHealth() {
		$('#CapacityManagement-li').removeClass('selected');
		$('#VMHealth-li').addClass('selected');
		$('#data-capacity').addClass('hide');
		$('#data-VM').removeClass('hide');
		$('#VMCheckbox').removeClass('hide');
	}

	function CapacityManagement() {	
		$('#VMHealth-li').removeClass('selected');
		$('#CapacityManagement-li').addClass('selected');
		$('#data-capacity').removeClass('hide');
		$('#data-VM').addClass('hide');		
		$('#VMCheckbox').addClass('hide');
	}
	
	var VMs = [ {name:'VM-1',color:'background-color:rgb(163, 207, 98)',catagory:'vBLOCK-1',priority:'3'},{name:'VM-2',color:'background-color:rgb(163, 207, 98)',catagory:'vBLOCK-2',priority:'3'},
				{name:'VM-3',color:'background-color:#ff9c00',catagory:'vBLOCK-1',priority:'2'},{name:'VM-4',color:'background-color:rgb(163, 207, 98)',catagory:'vBLOCK-2',priority:'3'},
				{name:'VM-5',color:'background-color:#ff9c00',catagory:'vBLOCK-1',priority:'2'},{name:'VM-6',color:'background-color:#e32212',catagory:'vBLOCK-2',priority:'1'},
				{name:'VM-7',color:'background-color:#e32212',catagory:'vBLOCK-1',priority:'1'},{name:'VM-8',color:'background-color:#e32212',catagory:'vBLOCK-2',priority:'1'},
				{name:'VM-9',color:'background-color:#ff9c00',catagory:'vBLOCK-1',priority:'2'}];
	
	var vBlocks = [{vBlock:"vBLOCK-1",cpu_used:"5",cpu_total:"15",memory_used:"3",memory_total:"4",storage_used:"1",storage_total:"4",visionURLArray:"http://www.google.com",vCenterURLArray:"http://www.yahoo.com"},{vBlock:"vBLOCK-2",cpu_used:"15",cpu_total:"15",memory_used:"4",memory_total:"4",storage_used:"4",storage_total:"4",visionURLArray:"http://www.wikipedia.org",vCenterURLArray:"http://www.facebook.com"}];
	
	VMs = JSON.stringify('${VMArrayJSON}');
	VMs = eval('(' + '${VMArrayJSON}' + ')');
	
	vBlocks = JSON.stringify('${vBlockArrayJSON}');
	vBlocks = eval('(' + '${vBlockArrayJSON}' + ')');
	
	var temp = VMs.slice();
	
	$(document).ready(function(){
		
		var hash = document.location.hash;
		if(hash){
			if(hash === '#VMHealth'){
				VMHealth();
			}
			else if(hash === '#CapacityManagement'){
				CapacityManagement();
			}
		}
		else{
			VMHealth();
		}		

		VMs = VMs.sort(function IHaveAName(a, b) { return b.priority > a.priority ?  1 : b.priority < a.priority ? -1 : 0; });
		VMs = VMs.sort(function IHaveAName(a, b) { return b.name> a.name?  1 : b.name< a.name? -1 : 0; });

		populateData(VMs);	
		
		$("#linkTable").addClass('hide');
		
		for(i in vBlocks)
		{
			if(vBlocks[i].vBlock !== undefined && vBlocks[i].vBlock !== null)			
				$('#vBlockDropDown').append(new Option(vBlocks[i].vBlock,vBlocks[i].vBlock));
		}	
		
		// get catagory to be populated in dropdown
		var catagoryArray = [];				
		for(i in VMs)
		{
			if(VMs[i].catagory !== undefined && VMs[i].catagory !== null)			
				if($.inArray(VMs[i].catagory, catagoryArray) === -1) catagoryArray.push(VMs[i].catagory);
		}	
		catagoryArray.sort(function (a, b) {return a.toLowerCase().localeCompare(b.toLowerCase());});
		
		for(i=0; i<catagoryArray.length; i++)
			$('#catagoryDropDown').append(new Option(catagoryArray[i],catagoryArray[i]));
			
		$('#vBlockDropDown').trigger('change');
					
	})
	
	Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
		  return v[name] === value ? null : v;
	   });
	   this.length = 0;
	   this.push.apply(this, array);
	}
	
	
	$(function() {
	
	$("input[type='checkbox']").change(
	function()	{
		
			temp = VMs.slice();			
			var all = false;
			var high = false;
			var moderate = false;
			var normal = false;
			
			if($('#checkbox_all').is(':checked')) 
			{		
				all = true;
			}
			if($('#checkbox_high').is(':checked')) 
			{		
				high = true;
			}
			if($('#checkbox_moderate').is(':checked')) 
			{		
				moderate = true;
			}
			if($('#checkbox_normal').is(':checked')) 
			{		
				normal = true;
			}

			if(all == false)
			{
				if(high==false)
					temp.removeValue('priority', '1');
				if(moderate==false)
					temp.removeValue('priority', '2');
				if(normal==false)
					temp.removeValue('priority', '3');
			}
			
			$("select#catagoryDropDown").val('All vBLOCKS'); 
			$('#searchText').val('');
			
			populateData(temp);			
	});	
	});
	
	
	function updatevBlock()
	{
		var e = document.getElementById("vBlockDropDown");
		var text = e.options[e.selectedIndex].text;
		
		if(text == 'Select vBlock' || text == 'All vBLOCKS')
		{
			var cpu_used = 0, cpu_total = 0,  memory_used = 0 , memory_total = 0 , storage_used = 0; storage_total = 0;
			$('#linkTable').addClass('hide');
			
			var arr1 = "" ; 
			var arr2 = "" ; 
			var arr3 = "" ;
			
			for (i in vBlocks)
			{
				if(vBlocks[i].cpu_used !== undefined && vBlocks[i].cpu_used !== null)
					cpu_used += parseFloat(vBlocks[i].cpu_used);
				if(vBlocks[i].cpu_total !== undefined && vBlocks[i].cpu_total !== null)
					cpu_total += parseFloat(vBlocks[i].cpu_total);
				if(vBlocks[i].memory_used !== undefined && vBlocks[i].memory_used !== null)
					memory_used += parseFloat(vBlocks[i].memory_used);
				if(vBlocks[i].memory_total !== undefined && vBlocks[i].memory_total !== null)
					memory_total += parseFloat(vBlocks[i].memory_total);
				if(vBlocks[i].storage_used !== undefined && vBlocks[i].storage_used !== null)
					storage_used += parseFloat(vBlocks[i].storage_used);
				if(vBlocks[i].storage_total !== undefined && vBlocks[i].storage_total !== null)
					storage_total += parseFloat(vBlocks[i].storage_total);
					
				arr1 += vBlocks[i].visionURLArray + (",");
				arr2 += vBlocks[i].vCenterURLArray + (",");
				arr3 += vBlocks[i].vBlock + (",");
			}	
			
			$("#cpuBarText").html(parseFloat(cpu_used)+' / '+parseFloat(cpu_total));
			$("#memoryBarText").html(parseFloat(memory_used)+' / '+parseFloat(memory_total)+' GB');
			$("#storageBarText").html(parseFloat(storage_used)+' / '+parseFloat(storage_total)+' GB');
			
			$("#cpuBar").css('width', ((cpu_used*100)/cpu_total)+'%');
			$("#cpuPer").css('width', (100 - ((cpu_used*100)/cpu_total))+'%');
			$("#memoryBar").css('width', ((memory_used*100)/memory_total)+'%');
			$("#memoryPer").css('width', (100 - ((memory_used*100)/memory_total))+'%');
			$("#storageBar").css('width', ((storage_used*100)/storage_total)+'%');
			$("#storagePer").css('width', (100 - ((storage_used*100)/storage_total))+'%');
			
			$("#linkTable").find("tr:gt(0)").remove();
			$("#linkTable").removeClass('hide');
			arr1 = arr1.split(",");
			arr2 = arr2.split(",");
			arr3 = arr3.split(",");
			
			$('#vBlockColumn').removeClass('hide');
			for(i=0;i<arr1.length-2; i++)
			{
				var table = document.getElementById("linkTable"); 
				var row = table.insertRow(table.rows.length); 
				row.insertCell(0).innerHTML = '<span style="color:red;font-weight:bold;">' + arr3[i] + '</span>';
				row.insertCell(1).innerHTML = '<a href='+arr1[i]+' target="_blank">'+arr1[i]+'</a>';
				row.insertCell(2).innerHTML = '<a href='+arr2[i]+' target="_blank">'+arr2[i]+'</a>';
			}

		}
		else
		{
			for (i in vBlocks)
			{
				if(vBlocks[i].vBlock === text)
				{
					$("#cpuBarText").html(parseFloat(vBlocks[i].cpu_used)+' / '+parseFloat(vBlocks[i].cpu_total));
					$("#memoryBarText").html(parseFloat(vBlocks[i].memory_used)+' / '+parseFloat(vBlocks[i].memory_total)+' GB');
					$("#storageBarText").html(parseFloat(vBlocks[i].storage_used)+' / '+parseFloat(vBlocks[i].storage_total)+' GB');
					
					$("#cpuBar").css('width', ((vBlocks[i].cpu_used*100)/vBlocks[i].cpu_total)+'%');
					$("#cpuPer").css('width', (100 - ((vBlocks[i].cpu_used*100)/vBlocks[i].cpu_total))+'%');
					$("#memoryBar").css('width', ((vBlocks[i].memory_used*100)/vBlocks[i].memory_total)+'%');
					$("#memoryPer").css('width', (100 - ((vBlocks[i].memory_used*100)/vBlocks[i].memory_total))+'%');
					$("#storageBar").css('width', ((vBlocks[i].storage_used*100)/vBlocks[i].storage_total)+'%');
					$("#storagePer").css('width', (100 - ((vBlocks[i].storage_used*100)/vBlocks[i].storage_total))+'%');
					
					var arr1 = vBlocks[i].visionURLArray.split(",");
					var arr2 = vBlocks[i].vCenterURLArray.split(",");
					
					$("#linkTable").find("tr:gt(0)").remove();
					$("#linkTable").removeClass('hide');
					for(j=0;j<arr1.length; j++)
					{
						var table = document.getElementById("linkTable"); 
						var row = table.insertRow(table.rows.length); 						
						row.insertCell(0).innerHTML = '<span style="color:red;font-weight:bold;">' + vBlocks[i].vBlock + '</span>';
						row.insertCell(1).innerHTML = '<a href='+arr1[j]+' target="_blank">'+arr1[j]+'</a>';
						row.insertCell(2).innerHTML = '<a href='+arr2[j]+' target="_blank">'+arr2[j]+'</a>';
					}
					//$('#vBlockColumn').addClass('hide');
				}
					
			}	
		}
	}
	function filterVMs()
	{
		var e = document.getElementById("catagoryDropDown");
		var search = e.options[e.selectedIndex].text;
		$('#searchText').val('');
		
		temp = temp.sort(IHaveAName);
		temp = temp.sort(sortByName);
		
		if(temp.length==0)
		{
			document.getElementById("data-VM").innerHTML = "<div class='row-fluid'></div>";
			return;
		}
		else
		{
			var no = 0;	
			var str = "";
			str = "<div class='row-fluid'>";	
			for (i in temp)
			{
				if((search == 'All vBLOCKS' || temp[i].catagory == search ) && temp[i].ciid != null)
				{
					var URL = '/group/telkomtelstra/tickets?' + $.base64.encode(temp[i].ciid+'&'+temp[i].name);
					
					str += "<div class='span3'><div class='extra_style' style='"+temp[i].color+"'><table width='100%'><tr style='display: block;'><td style='word-break: break-all;width: 100px !important;position: relative;left: 84%;' align='center'><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].name+"</a></td></tr><tr style='display: block;'><td style='position: relative;left: 84%;' align='center'><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>("+temp[i].vmalias+")</a></td></tr></table></div></div>";
					no++;
					if(no==4)
					{
						 no=0;
						 str += "</div><div class='row-fluid'>";			 
					}
				}
			}
			str += "</div>";
			document.getElementById("data-VM").innerHTML = str;
		}
	}
	
	function searchVMs()
	{	
		var e = document.getElementById("catagoryDropDown");
		var dropdown = e.options[e.selectedIndex].text;
		
		var search = $('#searchText').val();		
		temp = temp.sort(IHaveAName);
		temp = temp.sort(sortByName);
		
		if(temp.length==0)
		{
			document.getElementById("data-VM").innerHTML = "<div class='row-fluid'></div>";
			return;
		}
		else
		{
			var no = 0;	
			var str = "";
			str = "<div class='row-fluid'>";	
			for (i in temp)
			{
				if(temp[i].name.indexOf(search) > -1 && (dropdown == 'All vBLOCKS' || temp[i].catagory == dropdown))
				{
					var URL = '/group/telkomtelstra/tickets?' + $.base64.encode(temp[i].ciid+'&'+temp[i].name);
					str += "<div class='span3'><div class='extra_style' style='"+temp[i].color+"'><table width='100%'><tr><td align='center'><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].name+"</a></td></tr><tr><td align='center'><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>("+temp[i].vmalias+")</a></td></tr></table></div></div>";
					no++;
					if(no==4)
					{
						 no=0;
						 str += "</div><div class='row-fluid'>";			 
					}
				}
			}
			str += "</div>";
			document.getElementById("data-VM").innerHTML = str;
		}
		
	}
	
	var IHaveAName = function IHaveAName(a, b) { 
		return b.priority < a.priority ?  1 : b.priority > a.priority ? -1 : 0;
	}

	var sortByName = function sortByName(a, b) { 
		return b.name < a.name ?  1 : b.name > a.name ? -1 : 0;
	}

	
	function populateData(temp)
	{
		temp = temp.sort(IHaveAName);		
		temp = temp.sort(sortByName);
		
		if(temp.length==0)
		{
			document.getElementById("data-VM").innerHTML = "<div class='row-fluid'></div>";
			return;
		}
		else
		{
			var no = 0;	
			var str = "";
			str = "<div class='row-fluid'>";	
			for (i in temp)
			{
				if(temp[i].ciid!=null){
				
					var URL = '/group/telkomtelstra/tickets?' + $.base64.encode(temp[i].ciid+'&'+temp[i].name);
					str += "<div class='span3'><div class='extra_style' style='"+temp[i].color+"'><table width='100%'><tr><td align='center'><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].name+"</a></td></tr><tr><td align='center'><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>("+temp[i].vmalias+")</a></td></tr></table></div></div>";
					no++;
					if(no==4)
					{
						no=0;
					 	str += "</div><div class='row-fluid'>";			 
					}
			 	}
			}
			str += "</div>";
			document.getElementById("data-VM").innerHTML = str;
		}
	}

$( document ).ready(function() {
$('#searchText').keypress(function (e) {
 var key = e.which;
 if(key == 13)
  {
	 searchVMs();
  }
});   
});

function submitFm() {

	document.getElementById("refreshButton").disabled = 'disabled';
	document.getElementById("refreshButton").setAttribute("class", "updateButtonDisable");

	var url = '<%=refreshRealTimeVMs.toString()%>';
		document.forms["fm"].action = url;
		document.forms["fm"].submit();
}
</script>

</head>
<body>
<div class="container-fluid tt-page-detail">
	<div class="tt-cloudlevel1" >
   		<div class="tt-cloudlevel1-header"> 
		   <img src="/TTApplicationPortlets/images/u91.png"> 
		   <font style="font-size:100%;vertical-align:middle;margin-left:1%;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</font>
		   <font style="font-size:125%;vertical-align:middle;"><liferay-ui:message key="Private Cloud"/></font> 
		   <div class="span4 tt-create-incident-breadcrumb" style="margin-bottom: 1px; float: right !important">
			<div id="updateDataHolderDiv">
				<div class="rwd updateInfoHolder">
					<liferay-ui:message key="Last updated on" />
				</div>
				<div class="wrapper updateInfoHolder">
					<liferay-ui:message key="Last upd.." />
					<div class="tooltip" style="margin-left:5px;text-align:center;"><liferay-ui:message key="Last updated on"/></div>
				</div>
			<div class="updatedTimeHolder">${lastRefreshDate}</div>
			<div class="updateButtonHolder">
				<form name="fm" method="Post">
				<%if(allowAccess)
					{
					%>
						<button type="button" id="refreshButton" style="font-family: 'MyWebFont';"class="btn-primary-tt refresh" onclick="submitFm()">
							<liferay-ui:message key="Update Data"/>
						</button>
						<%
					}
					else
					{
					%>
					<div class="updateButtonHolder">
						<button type="button" id="refreshButton" style="font-family: 'MyWebFont';background-color: #CCC !important; color: white !important;" class="btn-primary-tt refresh" title="Contact Administrator">
							<liferay-ui:message key="Update Data"/>
						</button>
					</div>
					<%
					}
					%>	
				</form>
			</div>
			</div>
		</div>
			<div class="span8 tt-backbutton" style="float: right;">
				<input type="button" value="&nbsp;&lt;&nbsp;<liferay-ui:message key='Back'/>" onclick="window.location='/group/telkomtelstra/dashboard'" class="tt-cloud-inputbutton" >
			</div>
		</div>
        
    </div>
	<div class="row-fluid tt-page-content">
		<ul id="tabc" class="nav nav-pills tt-site-ul">
			<li class="active tt-site-nav-pills selected" id="VMHealth-li">
				<a data-toggle="tab" href="#VMHealth" class="tt-site-nav-pills" id="basedOnTicket" onclick="VMHealth()"><liferay-ui:message key="VM Health"/></a>
			</li>
			<li class="tt-site-nav-pills" id="CapacityManagement-li">
				<a data-toggle="tab" href="#CapacityManagement" class="tt-site-nav-pills" id="basedOnSLA" onclick="CapacityManagement()"><liferay-ui:message key="Capacity & Cloud Management"/></a>
			</li>
		</ul>
	</div>
</div>

<div class="container-fluid tt-site-display" id="VMCheckbox">
	<div class="row-fluid">
		<div class="span12 tt-sitecheck">
			<div class="span2 tt-displaySite-title"><liferay-ui:message key="Display VMs:"/></div>
			<div class="span6">
				<div class="row-fluid tt-displaySite-check">
					<div class="span2" style="display: flex;"><input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/><liferay-ui:message key="All"/></div>
					<div class="span3" style="display: flex;"><input type="checkbox" id="checkbox_high" class="tt-displaysite-position" /><liferay-ui:message key="High Impact"/></div>
					<div class="span4" style="display: flex;"><input type="checkbox" id="checkbox_moderate" class="tt-displaysite-position" /><liferay-ui:message key="Moderate Impact"/></div>
					<div class="span3" style="display: flex;"><input type="checkbox" id="checkbox_normal" class="tt-displaysite-position" /><liferay-ui:message key="Normal"/></div>					
				</div>
			</div>
			<div class="span4" style="display: none;">
			<select id="catagoryDropDown" class="selectBoxLogInc"  onchange="filterVMs();" style="width:150px!important;height:34px!important;font-size: 88%;">
					<option><liferay-ui:message key="All vBLOCKS"/></option>
			</select>
			<!--<input type="text" style="height:26px!important;" id="searchText"><button type="submit" class="search" onclick="searchVMs();"></button>-->
			</div>
		</div>
	</div>
</div>

<div class="container-fluid tt-site-tab-container"><div id="data-VM" style="padding:10px;background-color: white;"></div> 
<div id="data-capacity" style="padding:10px;background-color: white;">

<div class="container-fluid" >

	<div align="right">
	    <button type="button" class="create-VM" style="box-shadow: 2px 2px 2px #777;background-color: #E32212;" onclick="window.open('<%=prop.getProperty("privateCloud.CreateVM")%>', '_blank');"><liferay-ui:message key="Create VM"/></button>
	    <button type="button" class="create-VM" style="box-shadow: 2px 2px 2px #777;background-color: #E32212;" onclick="window.open('<%=prop.getProperty("privateCloud.ModifyVM")%>', '_blank');"><liferay-ui:message key="View"/>/<liferay-ui:message key="Modify VM"/></button>
	</div>

	<div class="row-fluid">
		<div class='span2'><liferay-ui:message key="vBLOCK"/></div>
		<div class='span6' style="display: none;">
			<select id="vBlockDropDown" class="selectBoxLogInc" name="productType" onchange="updatevBlock();">
					<option>All vBLOCKS</option>
			</select>
		</div>		
	</div>
	
	<div title="<liferay-ui:message key="CPUs are calculated with 4:1 Over Subscription"/>" class="row-fluid" style="margin-bottom:15px;">
		<div class='span2' id='detailLabel'>#&nbsp;<liferay-ui:message key="of vCPU"/></div>
		<div class='span10'>
		<div class="row-fluid"><div class="fontsize" style="font-size: 12px;text-align: center;" id="cpuBarText">0/0</div></div>
			<div class="progress" style="margin-bottom:0px!important;height: 10px !important;">
				<div class="progress-bar progress-bar-success" style="width:0%;" id="cpuBar"></div>  				
			</div>
		</div>				
	</div>
	
	<div class="row-fluid" style="margin-bottom:15px;">
		<div class='span2' id='detailLabel'><liferay-ui:message key="Memory"/></div>
		<div class='span10'>
		<div class="row-fluid"><div class="fontsize" style="font-size: 12px;text-align: center;" id="memoryBarText">0/0 GB</div></div>
			<div class="progress" style="margin-bottom:0px!important;height: 10px !important;">
				<div class="progress-bar progress-bar-success" style="width:0%;" id="memoryBar"></div>  				
			</div>
		</div>				
	</div>
	
	<div class="row-fluid" style="margin-bottom:15px;">
		<div class='span2' id='detailLabel'><liferay-ui:message key="Storage"/></div>
		<div class='span10'>
		<div class="row-fluid"><div class="fontsize" style="font-size: 12px;text-align: center;" id="storageBarText">0/0 GB</div></div>
			<div class="progress" style="margin-bottom:0px!important;height: 10px !important;">
				<div class="progress-bar progress-bar-success" style="width:0%;" id="storageBar"></div>  				
			</div>
		</div>				
	</div>
		
	<div class="table-responsive">
		<table class="table table-bordered" id="linkTable">
			<tr>
				<td class="row-fluid span1 tt-cloudmobile" style="background-color: #ccc;" id="vBlockColumn"><liferay-ui:message key="vBLOCK"/></td>
				<td class="row-fluid span4 tt-cloudmobile" style="background-color: #ccc;"><liferay-ui:message key="Vision URL"/></td>
				<td class="row-fluid span4 tt-cloudmobile" style="background-color: #ccc;"><liferay-ui:message key="vCENTER URL"/></td>
			</tr>   
		</table>	
	</div>
	
</div>	
</div>
</div>
</body>
</html>