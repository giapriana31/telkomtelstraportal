<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL name="actionMethod1" var="sampleActionMethodURL"></portlet:actionURL>
<portlet:actionURL name="actionMethod3"
	var="sampleActionMethodURLSecurity"></portlet:actionURL>
<liferay-ui:error key="passwordError"
	message="Please Check The Password and Try Again !"></liferay-ui:error>
	

<liferay-ui:success key="passwordChangeSuccess"
	message="Password Has Been Changed Successfully !" />
<liferay-ui:error key="recoveryQuestionError"
	message="Please Check and Try Again !"></liferay-ui:error>
<liferay-ui:error key="contactAdmin"
	message="Contact Admin or Try again after sometime !"></liferay-ui:error>
<liferay-ui:success key="recoveryQuestionChangeSuccess"
	message="Security Question Has been Changed Successfully !" />
	
<c:set var="backDoorEntry" value='<%=isbackdoor%>'></c:set>
<c:if test="${backDoorEntry==true}">

<div>
<img src="<%=request.getContextPath()%>/images/alert1.png" class="oktaimg">
<div class="oktamsg"> Page is temporarily unavailable, Please contact Administrator!!! </div>
 </div>

</c:if>
<c:if test="${backDoorEntry==false}">
<div id="securityContainer" >

	<div class="row">

		<div class="span8" id="changePasswordHeader"><div id="backgroundPass" class="unclickedPass"><div id="changePasswordHeaderInside"><liferay-ui:message key="CHANGE PASSWORD"/></div><div id="moreLessIconPass" class="moreDetails"></div></div></div>

	</div>

	<div id="changePasswordContainer" >
	<div id="changePasswordContainerInside">
		<form action="${sampleActionMethodURL}" method="post" autocomplete="off">

			<div class="container-fluid">
				<INPUT TYPE="hidden" NAME="<portlet:namespace />Email"
					readonly="readonly"
					VALUE="<%=themeDisplay.getUser().getDisplayEmailAddress()%>">
					
				<INPUT TYPE="hidden" NAME="<portlet:namespace />CompanyID"
					readonly="readonly" VALUE="<%=themeDisplay.getCompanyId()+""%>">
			</div>
			<div class="row">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="User Name"/>:*</div>
				<div class="span3" id="firstRowProfile">
					<INPUT TYPE="TEXT" NAME="<portlet:namespace />name"
						VALUE="<%=user.getEmailAddress()%>" readonly="readonly" class="securityInputs">
				</div>
			</div>
			<div class="row">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="Existing Password"/>:*</div>
				<div class="span3" id="firstRowProfile">
					<INPUT TYPE="password" name="<portlet:namespace />password"
						VALUE="" data-validation="required length" data-validation-length="7-40"
						placeholder="*******" onfocus="this.placeholder=''" onblur="this.placeholder='*******'" class="securityInputs">
				</div>
			</div>
			<div class="row">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="New Password"/>:*</div>
				<div class="span3" id="firstRowProfile">
					<INPUT TYPE="password" name="<portlet:namespace />newPassword"
						id="newPassword" VALUE="" data-validation="newPassword"
						data-validation-optional="true" placeholder="*******" onfocus="this.placeholder=''" onblur="this.placeholder='*******'" class="securityInputs">
				</div>
			</div>

			<div class="row">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="Confirm Password"/>:*</div>
				<div class="span3" id="firstRowProfile">
					<INPUT TYPE="password" name="<portlet:namespace />confirmPassword"
						id="confirmPassword" VALUE="" data-validation="confirmPass"
						placeholder="*******" onfocus="this.placeholder=''" onblur="this.placeholder='*******'" class="securityInputs">
				</div>
			</div>

			<div class="profileSetBottom">
			<!-- 	<aui:button class="styled-button-1-save" type="submit" value="Save"
					id="submitBtn" />
				<aui:button class="styled-button-1-cancel" type="reset"
					value="Cancel" /> -->
					
					
					<%if(allowAccess)
						{
						%>
						<button  type="submit" id="savePassword"><div id="saveIcon"></div><div id="saveContent"><liferay-ui:message key="Save"/></div> </button>
						<%
						}
						else
						{
						%>
						<button  type="reset" id="savePassword" style="background-color:#CCC!important;color:white!important;" title="Contact Adminsitrator"><div id="saveIcon"></div><div id="saveContent"><liferay-ui:message key="Save"/></div> </button>
						<%
						}
						%>
					<button  type="reset" id="cancelPassword" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" ><div id="cancelIcon"></div> <div id="cancelContent"><liferay-ui:message key="Cancel"/></div> </button>
						
			</div>
		</form>
	</div>
	</div>

	<div class="row">
		<div class="span8" id="changeQuestionHeader"><div id="changeSecurityQuestionHeaderInside"><liferay-ui:message key="CHANGE SECURITY QUESTION"/>
			</div><div id="moreLessIconQues" class="moreDetails"></div></div>
	</div>

	
	<div id="changeQuestionContainer" style="display:none;">
	<div id="changeQuestionContainerInside">
		<form action="${sampleActionMethodURLSecurity}" method="post">

			<INPUT TYPE="hidden" NAME="<portlet:namespace />Email"
				readonly="readonly"
				VALUE="<%=themeDisplay.getUser().getDisplayEmailAddress()%>">
			<INPUT TYPE="hidden" NAME="<portlet:namespace />CompanyID"
				readonly="readonly" VALUE="<%=themeDisplay.getCompanyId()+""%>">

			<div class="row">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="Security Question"/>:*
					</div>
				<div class="span3" id="firstRowProfile">

					<select id="securityQuestionDropDown"
						name="<portlet:namespace />securityQuestion"
						data-validation="securityQuestion" class="securityInputs">
						<option value="#">--Select--</option>
						<c:forEach var="question" items="${securityQuestion}">
							<option value="${question.questionText}">${question.questionText}</option>

						</c:forEach>

					<option value="Add your own custom question"><liferay-ui:message key="Add your own custom question"/></option>
					</select>
				</div>
				
			</div>
			<div class="row" id="customQuestionDiv">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="Enter Question"/>:*</div>
				<div class="span3" id="firstRowProfile">
					<INPUT TYPE="TEXT" NAME="<portlet:namespace />customSecurityQuestion"
						value="" id="answer" data-validation="validateCustomQuestion validateCustomQuestionLength"  class="securityInputs">
				</div>
				
			</div>

			<div class="row" style="margin-top:10px;">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="Answer"/>:*</div>
				<div class="span3" id="firstRowProfile">
					<INPUT TYPE="TEXT" NAME="<portlet:namespace />answer" value=""
						id="answer" data-validation="required length" data-validation-length="min4" class="securityInputs">
				</div>
			</div>
			<div class="row">
				<div class="span2" id="firstRowProfile"><liferay-ui:message key="Password"/>:*</div>
				<div class="span3" id="firstRowProfile">
					<INPUT TYPE="password" name="<portlet:namespace />password"
						VALUE="" data-validation="required length" data-validation-length="7-40"
						placeholder="*******" onfocus="this.placeholder=''" onblur="this.placeholder='*******'" class="securityInputs">
				</div>
			</div>

			<div class="profileSetBottom">
				<!-- <aui:button class="styled-button-1-save" type="submit" value="Save"
					id="submitBtn" />
				<aui:button class="styled-button-1-cancel" type="reset"
					value="Cancel" /> -->
					<%if(allowAccess)
					{
					%>
					<button  type="submit" id="saveQuestion"><div id="saveIcon"></div><div id="saveContent"><liferay-ui:message key="Save"/></div></button>
					<%
					}
					else
					{
					%>
					<button  type="button" id="saveQuestion" style="background-color:#CCC!important;color:white!important;" title="Contact Adminsitrator"><div id="saveIcon"></div><div id="saveContent"><liferay-ui:message key="Save"/></div></button>
					<%
					}
					%>
					<button  type="reset" id="cancelQuestion" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" > <div id="cancelIcon"></div><div id="cancelContent"><liferay-ui:message key="Cancel"/></div> </button>
			</div>

		</form>
	</div>
</div>
</div>
</c:if>
<script>




	$(document).ready(function() {
	/* 	$("#changePasswordContainer").hide(); */
	
		var displayQuestionDiv=${renderQuestionDiv};
		var displayPasswordDiv=${renderPasswordDiv};
		if(displayPasswordDiv==false){
			
			$("#changePasswordContainer").hide();
			
		}
			if(displayPasswordDiv==true){
			
			$("#changePasswordContainer").show();
			$("#moreLessIconPass").removeClass("moreDetails");
			$("#moreLessIconPass").addClass("lessDetails");
			 $("#changePasswordHeader").css('backgroundColor',"#CCC");
			
			
		}
		
		if(displayQuestionDiv == false){
			$("#changeQuestionContainer").hide();
			$("#customQuestionDiv").hide();
			
			
		}else if(displayQuestionDiv == true){
			$("#changeQuestionContainer").show();
			$("#changePasswordContainer").hide();
			$("#customQuestionDiv").hide();
			$("#moreLessIconQues").removeClass("moreDetails");
			$("#moreLessIconQues").addClass("lessDetails");
			 $("#changeQuestionHeader").css('backgroundColor',"#CCC");
		}
		/* $("#changeQuestionContainer").hide();
		$("#customQuestionDiv").hide(); */
	});

	$("#changePasswordHeader").click(function() {
		$("#changePasswordContainer").slideToggle();
		$("#changeQuestionContainer").hide();
		$("#moreLessIconQues").addClass("moreDetails");
		$("#moreLessIconQues").removeClass("lessDetails");
		/* $("#changeQuestionContainer").hide(); */
	});

	$("#changeQuestionHeader").click(function() {
		$("#changeQuestionContainer").slideToggle();
		$("#changePasswordContainer").hide();
		$("#moreLessIconPass").addClass("moreDetails");
		$("#moreLessIconPass").removeClass("lessDetails");
		/* $("#changePasswordContainer").hide();
		$("#customQuestionDiv").hide(); */

	});

	/* $("#customQuestion").click(function() {
		$("#customQuestionDiv").show();
		$('#securityQuestionDropDown').attr({
			'disabled' : true
		});
	});

	$("#predefinedQuestion").click(function() {
		$("#customQuestionDiv").hide();
		$('#securityQuestionDropDown').attr({
			'disabled' : false
		});

	}); */
	
	
	
	
	
	
	$("#changePasswordHeader").click(function() {
		$("#moreLessIconPass").toggleClass("moreDetails lessDetails");
		
		 var x = $(this).css("backgroundColor");

		 if(x=="rgb(241, 241, 241)"){
	
			 $(this).css('backgroundColor',"#CCC");
			 $("#changeQuestionHeader").css('backgroundColor',"#f1f1f1");
		 }
		  if(x=="rgb(204, 204, 204)"){
			 $(this).css('backgroundColor',"#f1f1f1");
			 $("#changeQuestionHeader").css('backgroundColor',"#f1f1f1");
			 
		 }
 
 
 
/*  var y = $("#changeQuestionHeader").css("backgroundColor");

 if(y=="rgb(241, 241, 241)"){

	 $("#changeQuestionHeader").css('backgroundColor',"#CCC");
	 
 }
if(y=="rgb(204, 204, 204)"){
	 $("#changeQuestionHeader").css('backgroundColor',"#f1f1f1");
 
 
} */
	});
	
	
	
	$("#changeQuestionHeader").click(function() {
		$("#moreLessIconQues").toggleClass("moreDetails lessDetails");
		
		 var x = $(this).css("backgroundColor");

		 if(x=="rgb(241, 241, 241)"){
	
			 $(this).css('backgroundColor',"#CCC");
			 $("#changePasswordHeader").css('backgroundColor',"#f1f1f1");
			 
		 }
		  if(x=="rgb(204, 204, 204)"){
			 $(this).css('backgroundColor',"#f1f1f1");
			 $("#changePasswordHeader").css('backgroundColor',"#f1f1f1");
			 
		 }
 
 
 
 
 
/*  var y = $("#changePasswordHeader").css("backgroundColor");

 if(y=="rgb(241, 241, 241)"){

	 $("#changePasswordHeader").css('backgroundColor',"#CCC");
	 
 }
if(y=="rgb(204, 204, 204)"){
	 $("#changePasswordHeader").css('backgroundColor',"#f1f1f1");
	 
 }
  */
 
 
 
	});
	
	$("#securityQuestionDropDown").change(function() {
		var question=$("#securityQuestionDropDown :selected").text();
		if(question=="Add your own custom question"){
			
			$("#customQuestionDiv").show();
			/* $('#securityQuestionDropDown').attr({
				'disabled' : true
			}); */
		}
		
		else{
			
			$("#customQuestionDiv").hide();
			$('#securityQuestionDropDown').attr({
				'disabled' : false
			});
		}
	});
</script>

<!-- <script>


$('#newPassword').change(function(){
    $("#changePasswordContainer").toggle("changePasswordContainerHidden");
	if($('#newPassword').val()!=''){
		$('#submitBtn').attr({'disabled' : false});
		$('#submitBtn').removeClass('btn disabled btn-primary');
		$('#submitBtn').addClass('btn btn-primary');
		//$('#submitBtn').ancestor('.aui-button').addClass('aui-button-enabled');
	
		
    }
	
	if($('#newPassword').val()=='' && $('#answer').val()==''){
		$('#submitBtn').attr({'disabled' : true});
		$('#submitBtn').removeClass('btn btn-primary');
		$('#submitBtn').addClass('btn disabled btn-primary');
	
		//$('#submitBtn').ancestor('.aui-button').addClass('aui-button-enabled');
	
		
   }
});


$('#answer').change(function(){
	  
		if($('#answer').val()!=''){
			$('#submitBtn').attr({'disabled' : false});
			$('#submitBtn').removeClass('btn disabled btn-primary');
			$('#submitBtn').addClass('btn btn-primary');
			//$('#submitBtn').ancestor('.aui-button').addClass('aui-button-enabled');
		
			
	    }
		
		if($('#newPassword').val()=='' && $('#answer').val()==''){
			$('#submitBtn').attr({'disabled' : true});
			$('#submitBtn').removeClass('btn btn-primary');
			$('#submitBtn').addClass('btn disabled btn-primary');
		
			//$('#submitBtn').ancestor('.aui-button').addClass('aui-button-enabled');
		
			
	   }
	}); 

</script> -->
