<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="com.liferay.portal.kernel.util.Validator" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portlet.PortletPreferencesFactoryUtil" %>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.portal.model.User" %>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil" %>
<%@ page import="javax.portlet.PortletURL" %>
<%@page import="javax.portlet.WindowState" %>
<%@page import="java.text.Format"%>
<%@page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="java.util.*"%>
<%@page import="com.liferay.portal.kernel.util.OrderByComparator"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>


<%@ page import=" com.liferay.taglib.portlet.DefineObjectsTag" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="common.jsp" %>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajax-jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-1.11.1.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/additional-methods.min.js"></script>
</head>

<body>
<div class="container3" style="background-color: white;
	width: 925px;
	border: solid 1px #ccc; 
	border-bottom-right-radius:3px;
	border-bottom-left-radius:3px;  
	border-top-right-radius:3px;
	border-top-left-radius:3px;">

  <div id="button-set">
  
  <!--  <input type='button'  data-theme='b' value='SEND EMAIL' data-mini='true' data-inline='true' data-icon='gear' data-icon-pos='top' id="show-content"    style=" 
   background: #FFF;
  border-right-style: none;
  border-left-style: none;
  height: 46px;
  TEXT-ALIGN: LEFT;
  width: 100%;
  min-width: 150px;
  /* border-radius: 3px; */
  padding-left: 15px;
  padding-right: 15px;
  TEXT-DECORATION: NONE;
  color: #555555;
  font-size: 14px;
  border-bottom: solid 1px #ccc;"> -->
  
<button id="sendEmailButton"><div id="sendEmailHeader"><div id="sendEmailContent"><liferay-ui:message key="SEND EMAIL"/></div><div id="sendEmailIcon" class="sendEmailMore"></div></div></button>

<div class="collapse3" id="collapsei3" style="display: none;">
    
<portlet:actionURL var="sendMailMessageActionURL" windowState="<%=LiferayWindowState.NORMAL.toString()%>">
<portlet:param name="<%=ActionRequest.ACTION_NAME%>" value="sendMailMessage"/>
</portlet:actionURL>
    
<c:if test='<%= SessionMessages.contains(renderRequest.getPortletSession(),"mail-send-success")%>'>
	<liferay-ui:success key="mail-send-success" message="Mail has been sent successfully." />
</c:if>

<c:if test='<%= SessionMessages.contains(renderRequest.getPortletSession(),"mail-send-failure")%>'>
<liferay-ui:success key="mail-send-failure" message="Please fill all the required fields." />
</c:if>

<c:if test='<%= SessionMessages.contains(renderRequest.getPortletSession(),"file_type_invalid")%>'>
<liferay-ui:success key="file_type_invalid" message="Invalid File" />
</c:if>

<script>
function validateToAddress(value){
	
		/* alert('2'); */
		var emailRegexStr = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		/* alert('3');	 */
		var emailArray = value.split(';');
		var emailList = [];
		var j=0;
     	for (var i=0;i<emailArray.length;i++) {
     		/* alert('In For ' + i);
     		alert('Email Content ' + emailArray[i]); */
     		var email=emailArray[i];
     		/* alert('Length of email '+email.length); */
           	if(email!=null && email.trim().length>0){
           		/* alert('Segment of address '+ email.trim()); */
           		/* emailList.push(email); */
                emailList[j]=email.trim();
                /* alert('Test '+emailList[j]); */
                j++;
           	}else{
           		/* alert('Else'); */
           		continue;           		
           	}
     	}
		if(emailList.length==0){
			/* alert('4'); */
			return false;
		}
		/* alert('5 ' + emailList);
		
		alert('Length of reduced list '+ emailList.length); */
		for (var i=0;i<emailList.length;i++) {
			/* alert('Second for loop'); */
			var email=emailList[i];
			/* alert('Second for loop '+ email); */
        	if(!emailRegexStr.test(email)){
           		/* alert('6'); */
           		return false;
           	}else{
           		/* alert('Else before 7'); */
           	}
            /* alert('7'); */
     	}
		return true;
}

function copyFilePath()
{
	/*The pop() method removes the last element of an array, and returns that element.*/
	var filename = $('input[type=file]').val().split('\\').pop();
	document.getElementById('fileName').value = filename;
}

function resetMessages()
{
	$("#senderEmailAddessId").hide();
	$("#receiverEmailAddessId").hide();
	$("#mailSubjectId").hide();
	$("#messageTextAreaId").hide();
	$("#attachmentLabelId").hide();
}
</script>

<script> 

$(document).ready(function(){
	
	if(<%= SessionMessages.contains(renderRequest.getPortletSession(),"mail-send-success")%>)
		{jQuery(".collapse3").show();
		 $("#sendEmailIcon").removeClass("sendEmailMore");
		 $("#sendEmailIcon").addClass("sendEmailLess");
		
	}else if(<%= SessionMessages.contains(renderRequest.getPortletSession(),"mail-send-failure")%>){
		jQuery(".collapse3").show();
		 $("#sendEmailIcon").removeClass("sendEmailMore");
		 $("#sendEmailIcon").addClass("sendEmailLess");
	}else if(<%= SessionMessages.contains(renderRequest.getPortletSession(),"file_type_invalid")%>){
		jQuery(".collapse3").show();
		 $("#sendEmailIcon").removeClass("sendEmailMore");
		 $("#sendEmailIcon").addClass("sendEmailLess");
	}
	else
		{jQuery(".collapse3").hide();
		 $("#sendEmailIcon").addClass("sendEmailMore");
		 $("#sendEmailIcon").removeClass("sendEmailLess");
		
		}
	
	
	 jQuery("#sendEmailButton").click(function()
			    {
		 		 
			     jQuery(this).next(".collapse3").slideToggle(15);
			     $("#sendEmailIcon").toggleClass("sendEmailMore sendEmailLess");
			     $("#sendEmailButton")
			        .toggleClass("ssry");
			     jQuery(".collapse1").hide();
			     jQuery(".collapse2").hide();
			     
			     $(".lsry")
			        .removeClass("lsry").add("lbry");
			     $(".sry")
			        .removeClass("sry").add("bry");
			     });
	
	 
});
</script>

<form action="${sendMailMessageActionURL}" method="post" id="sendEmailForm" enctype="multipart/form-data">

 <div class="row">
      <div class="span2" id="emailLabel"><liferay-ui:message key="From:*"/></div>
      <div class="span9" id="fromInput">
		<INPUT TYPE="TEXT" NAME="senderEmailAddess"  VALUE="<%= themeDisplay.getUser().getDisplayEmailAddress() %>"  class="emailInputs" id="senderEmailAddess"/>
		<label for="senderEmailAddess" class="error" id="senderEmailAddessId"></label> 
	  </div>
   </div>
   <div class="row">
      <div class="span2" id="emailLabel"><liferay-ui:message key="To:*"/></div>
      <div class="span9" id="toInput">
		<INPUT TYPE="TEXT" NAME="receiverEmailAddess"  VALUE="servicemanager@telkomtelstra.co.id"  class="emailInputs" id="receiverEmailAddess" />
		<label for="receiverEmailAddess" class="error" id="receiverEmailAddessId"></label>
      </div>
   </div>
   
   <div class="row">
      <div class="span2" id="emailLabel"><liferay-ui:message key="Subject:*"/></div>
      <div class="span9" id="subjectInput">
        <INPUT TYPE="TEXT" NAME="mailSubject"  VALUE="[Executive View-Inquiries]"  class="emailInputs"  id="mailSubject"/>
        <label for="mailSubject" class="error" id="mailSubjectId"></label>
      </div>
   </div>
 
   <div class="row">
      <div class="span2" id="emailLabel"><liferay-ui:message key="Message:*" /></div>
      <div class="span9" id="messageInput">
        <textarea NAME="mailBody"   class="emailInputs" id="messageTextArea"></textarea>
        <label for="messageTextArea" class="error" id="messageTextAreaId"></label>
      </div>
   </div>
   
   <div class="row">
      <div class="span2" id="emailLabel">
      	<liferay-ui:message key="Attachment:" />
      	<div class="helpIconDiv">
			<a href="#" data-toggle="tooltip" data-placement="top"
				title="<liferay-ui:message key="${AttachmentDescription}"/>"> <img class="helpIconImg"
				src="<%=request.getContextPath()%>/images/Discription_G_16.png">
			</a>
		</div>	
      </div>
      <div id="attachmentInput">
		<input type="text" id="fileName" readonly="readonly" class="file_input_textbox"/> 
		<input type="button" id="fileInputButton" value='<liferay-ui:message key="Browse" />'class="file_input_button"/>
		<input type="file" id="attachment" name="attachment" onchange="copyFilePath()" class="file_input_hidden"
			onmouseover="document.getElementById('fileInputButton').className='file_input_button_hover';"
      		onmouseout="document.getElementById('fileInputButton').className='file_input_button';"/>
			<div>
				<label for="attachment" class="error" id="attachmentLabelId"></label>
			</div>
      </div>
   </div>

   <div class="row">
      <div id="emailButton">
			<button  type="reset" id="resetEmail" onclick="resetMessages()" > <div id="cancelIcon"></div><div id="cancelContent"><liferay-ui:message key="Reset"/></div> </button>
			<%if(allowAccess)
						{
						%>
			<button  type="submit" id="sendEmail"><div id="sendIcon"></div><div id="sendContent"><liferay-ui:message key="SEND EMAIL"/></div></button>
			<%
						}
						else
						{
						%>
			<button  type="reset" id="sendEmail" style="background-color:#CCC!important;color:white!important;" title="Contact Administrator"><div id="sendIcon"></div><div id="sendContent"><liferay-ui:message key="SEND EMAIL"/></div></button>
						<%
						}
						%>
      </div>
    </div>
      
</form>
</div>
</div>
</div>
</body>
</html>
<script type="text/javascript">

$(document).ready(function(){
    
	jQuery.validator.addMethod('fileSizeCheck', function(value, element, param) {
		 return this.optional(element) || (element.files[0].size <= param) 
		
	}, "<liferay-ui:message key='File Size should be less than 5 MB' />");
	
	   $("#sendEmailForm").validate({
		    
	        // Specify the validation rules
	        rules: {
	        	mailSubject: "required",
	        	mailBody: "required",
	            senderEmailAddess: {
	                required: true,
	                email: true
	            },
	            receiverEmailAddess: {
	                required: true,
	                email: true
	            },
	            attachment:{
	            	extension: "txt|doc|docx|xls|xlsx|pdf|jpg|jpeg|png|zip",
	            	fileSizeCheck:5242880
	            }
	        },
	        
	        // Specify the validation error messages
	        messages: {
	        	mailSubject: '<liferay-ui:message key="Please enter email subject"/>',
	            mailBody: '<liferay-ui:message key="Please enter email message"/>',
	            senderEmailAddess: {
	                required: '<liferay-ui:message key="Please enter email address"/>',
	                email: '<liferay-ui:message key="Please enter a valid email address"/>'
	            },
	            receiverEmailAddess: {
	                required: '<liferay-ui:message key="Please enter email address"/>',
	                email: '<liferay-ui:message key="Please enter a valid email address"/>'
	            },
	            attachment:{
	            	extension: '<liferay-ui:message key="This file type is not supported"/>'
	            }
	        },
	       
	        submitHandler: function(form) {
	            form.submit();
	        }
	    });
});
</script>

