<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"
	type="text/javascript"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/home.css" />

<%@ include file="common.jsp"%>
<portlet:defineObjects />

<script></script>

<style>
@media ( min-width :768px) and (max-width:838px) {
	.greyOut {
		position: relative;
		top: 1px;
	}
}
</style>


<div id="homeMainContainer"
	class="container-fluid span12 home-main-container">
	<div id="mns_grey" class="row-fluid service-container-grey span2">
		<a><img class="navArrowRightImg homeImage mns_grey"
			src="<%=request.getContextPath()%>/images/MNS-Grey.png" /></a>
		<div class="progress tt-progess" style="background: #969693;"></div>
	</div>
	<div id="mns_black" class="row-fluid service-container span2">
		<a
			href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardsites")%>">
			<img class="navArrowRightImg homeImage mns_black"
			src="<%=request.getContextPath()%>/images/MNS.png" />
		</a>
		<div class="progress tt-progess" id="mns_statusBar"></div>
	</div>
	<div id="network_grey" class="row-fluid service-container-grey span2">
		<a><img class="navArrowRightImg homeImage network_grey"
			src="<%=request.getContextPath()%>/images/Network-Grey.png" /></a>
		<div class="progress tt-progess" style="background: #969693;"></div>
	</div>
	<div id="network_black" class="row-fluid service-container span2">
		<a
			href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardnetwork")%>">
			<img class="navArrowRightImg homeImage network_black"
			src="<%=request.getContextPath()%>/images/Network.png" />
		</a>
		<div class="progress tt-progess" id="network_statusBar"></div>
	</div>
	<div id="cloud_grey" class="row-fluid service-container-grey span2">
		<a><img class="navArrowRightImg homeImage cloud_grey"
			src="<%=request.getContextPath()%>/images/Cloud Infrastructure-Grey.png" /></a>
		<div class="progress tt-progess" style="background: #969693;"></div>
	</div>
	<div id="cloud_black" class="row-fluid service-container span2">
		<a
			href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttcloudservices")%>">
			<img class="navArrowRightImg homeImage cloud_black"
			src="<%=request.getContextPath()%>/images/Cloud Infrastructure.png" />
		</a>
		<div class="progress tt-progess" id="cloud_statusBar"></div>
	</div>
	<div id="saas_grey" class="row-fluid service-container-grey span2">
		<a><img class="navArrowRightImg homeImage saas_grey"
			src="<%=request.getContextPath()%>/images/SAAS-Grey.png" /></a>
		<div class="progress tt-progess" style="background: #969693;"></div>
	</div>
	<div id="saas_black" class="row-fluid service-container span2">
		<!-- Add href to anchor tag for SaaS level one page when available -->
		<a href="/group/telkomtelstra/saaslevel1"> <img
			class="navArrowRightImg homeImage saas_black"
			src="<%=request.getContextPath()%>/images/SAAS.png" />
		</a>
		<div class="progress tt-progess" id="saas_statusBar"></div>
	</div>
	<div id="uc_grey" class="row-fluid service-container-grey span2">
		<a><img class="navArrowRightImg homeImage uc_grey"
			src="<%=request.getContextPath()%>/images/Unified Comm-Grey.png" /></a>
		<div class="progress tt-progess" style="background: #969693;"></div>
	</div>
	<div id="uc_black" class="row-fluid service-container span2">
		<!-- Add href to anchor tag for UC level one page when available -->
		<a> <img class="navArrowRightImg homeImage uc_black"
			src="<%=request.getContextPath()%>/images/Unified Comm.png" />
		</a>
		<div class="progress tt-progess" id="saas_statusBar"></div>
	</div>
	<div id="security_grey" class="row-fluid service-container-grey span2">
		<a><img class="navArrowRightImg homeImage security_grey"
			src="<%=request.getContextPath()%>/images/Security-Grey.png" /></a>
		<div class="progress tt-progess" style="background: #969693;"></div>
	</div>
	<div id="security_black" class="row-fluid service-container span2">
		<!-- Add href to anchor tag for UC level one page when available -->
		<a> <img class="navArrowRightImg homeImage security_black"
			src="<%=request.getContextPath()%>/images/Security.png" />
		</a>
		<div class="progress tt-progess" id="saas_statusBar"></div>
	</div>
</div>

<script>
// 	var stateOfBanner = "closedBanner";

// 	function displayBanner() {
// 		if (stateOfBanner == "openBanner") {
// 			$("#leftbannnerspan").css("display", "block");
// 		}
// 	}
	$(document).ready(function() {

// 		$("#_145_navSiteNavigationNavbarBtn").click(function() {
// 			if (stateOfBanner == "openBanner") {
// 				stateOfBanner = "closedBanner";
// 			} else {
// 				stateOfBanner = "openBanner";
// 			}
// 		});
// 		setInterval(function() {
// 			if (stateOfBanner == "openBanner") {
// 				$("#leftbannnerspan").css("display", "block");
// 			}
// 		}, 1);

		var statusBarData = ${statusBarMap};
		$(".tt-progess").addClass("greyOut");
		$(".homeImage").addClass("greyOut");
		for ( var key in statusBarData) {

			$("#" + key + "_grey").hide();
			$("#" + key + "_black").removeClass("service-container");
			$("#" + key + "_black").addClass("service-container_black");

			$("." + key + "_black").removeClass("greyOut");
			$("#" + key + "_statusBar").css("background", statusBarData[key]);
			$("#" + key + "_statusBar").removeClass("greyOut");
		}
	});
</script>

