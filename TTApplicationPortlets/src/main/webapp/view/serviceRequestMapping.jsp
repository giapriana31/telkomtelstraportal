<%@ page import="javax.portlet.PortletSession"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ page import="javax.portlet.ActionRequest"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ include file="logServiceRequest.jsp"%>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"
	type="text/javascript"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">	


<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="getSRListURI" var="getSRListURI"></portlet:resourceURL>
<portlet:resourceURL id="getSRGraph" var="getSRGraph"></portlet:resourceURL>
<portlet:resourceURL id="requestAdvSearchURI" var="requestAdvSearchURI"></portlet:resourceURL>
<portlet:resourceURL var="getStatusURL" id="getStatusURL" />
<portlet:resourceURL var="getServiceURL" id="getServiceURL" />

<portlet:resourceURL id="getRequestMetrics" var="getRequestMetrics"></portlet:resourceURL>
<portlet:resourceURL id="getRequestDetails" var="getRequestDetails"></portlet:resourceURL>
<portlet:resourceURL id="getServiceRequestItemDetails" var="getServiceRequestItemDetails"></portlet:resourceURL>
<%-- <portlet:resourceURL name="refreshSR" var="refreshSR"></portlet:resourceURL> --%>
<portlet:actionURL name="refreshSR" var="refreshSR"></portlet:actionURL>

<%-- <script src="<%=request.getContextPath()%>/js/ttLogServiceRequest.js"
	type="text/javascript"></script> --%>
<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/fnReloadAjax.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/ttservicerequest.js" type="text/javascript"></script>

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/jquery.dataTables.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/ttservicerequest.css" />

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/serviceRequestDetails.css" />
<style>
 @media (max-width: 1199px) and (min-width: 768px){
		.aui .row-fluid .span6 {
		  width: auto !important;
		}
	}
	
	#tt-sr-modal {
		width: 55% !important;
		min-width: 365px;
	}


	.displayNoneImportant{
		display:none !important;
	}

	button#logServReqButton {
	  position: relative;
	  bottom: 12px;
	  z-index: 1;
	  /* left: 9% !important; */
	}

	button#refreshButton {
	  position: absolute;
	  bottom: 30px;
	  left: 240px;
	 /*  margin-left: 2%; */
	}

	.updateButtonDisable{
		background: #CCC;
		border-top-style: none;
		border-bottom-style: none;
		border-right-style: none;
		border-left-style: none;
		height:36px;
		color: #ffffff !important;
		width: auto;
		min-width:100px;
		border-radius:3px;
		padding-left:15px;
		padding-right:15px;
		font-family:'Gotham-Rounded' !important;
		cursor:default !important;
	}

	#updateDataHolderDiv {
		width: 1000px;
		position: relative;
		right: 10%;
		padding-bottom: 18px;
	}
	
	.updateInfoHolder {
		display: inline;
	}
	
	.updatedTimeHolder {
		display: inline;
	}
	
	.updateButtonHolder {
		display: inline;
	}
	
	.tt-create-service-breadcrumb{
		width: 340px !important;
		margin-top: 0px !important;
	}
	
	.tt-page-icon{
		float: left;
	}
	
	.tt-service-br-header{
		float: left;
		padding-left: 20px;
		font-size: 20px;
		color: #333333;
		text-shadow: none;
		padding-top: 5px;
		font-weight: 600;
	}

.startXMarker {
	display: inline;
	position: relative;
	left: -18px;
}

.midXMarker {
	display: inline;
	position: relative;
	left: 48px;
}

.endXMarker {
	display: inline;
	position: relative;
	left: 191px;
}

.basbarlayout {
	position: absolute;
}

.basebar {
	display: inline-block;
	width: 10px;
	height: 90px;
	background-color: #ccc;
	margin-right: 8px;
}

.markbarlayout {
	position: relative;
}

.markbar {
	display: inline-block;
	width: 10px;
	background-color: #000;
	margin-right: 8px;
}

.uppermarker {
	position: relative;
	bottom: 60px;
}

.midmarker {
	position: relative;
}

.graphStatistics
{
	display: inline-block;
	height: 60px;
	position: relative;
	top: 4px;
	right: 12px;
}

@-moz-document url-prefix() {
    #graphStatisticssrCustInitiated
	{
		top: 0px;
}

#graphStatisticscrCustInitiated {
	top: 0px;
}

#graphStatisticscrTtInitiated {
	top: 0px;
}
	.graphStatistics
	{
	display: inline-table;
}

}

@media screen and (-webkit-min-device-pixel-ratio:0)
{ 
    #graphStatisticssrCustInitiated
	{
		top: 0px;
	}
	#graphStatisticscrCustInitiated {
		top: 0px;
	}
	#graphStatisticscrTtInitiated {
		top: 0px;
	}
	.graphStatistics
	{
		display: inline-table;
	}
}

.bottommarker {
	position: relative;
}

.variantcategory {
	display: inline;
}

.variantcategorycount {
	display: inline;
	float: right;
}

.varianttrends
{
	display: inline-block;
}

.varianttrendhead {
	padding: 10px 10px 10px 0;
	font-weight: bold;
}

.graphOverlay {
	display: inline-block;
}

#varianttrendbody {
	display: flex;
	justify-content: space-between;
}

#srTrendsHeading {
	padding: 10px 0 5px 0;
	font-weight: 600;
	color: #333;
}

#srTrendsShortDescription {
	color: #555;
}

.nospacediv {
	height: 0px !important;
}

div#scrTrendSection {
	height: 200px;
}

#srTrendsShortDescription {
	color: #555;
}

.nospacediv {
	height: 0px !important;
}

button#normalSearch {
	background-image: url("../images/srch.png");
	height: 30px;
	width: 30px;
	margin-bottom: 10px;
	border-style: none;
}

.srlSearchBox {
	float: right;
}

.dataTables_wrapper .dataTables_paginate .paginate_button.current,
	.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover
	{
	/* 	color: white !important; */
	/* 	border: 1px solid #e32212; */
	/* 	background-color: #f1f1f1; */
	/* 	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, white),
 		color-stop(100%, #e32212));
 	background: -webkit-linear-gradient(top, white 0%, #e32212 100%);
 	background: -moz-linear-gradient(top, white 0%, #e32212 100%);
 	background: -ms-linear-gradient(top, white 0%, #e32212 100%);
 	background: -o-linear-gradient(top, white 0%, #e32212 100%);
 	background: linear-gradient(to bottom, white 0%, #e32212 100%); */
	
}

.current:hover {
	background: #e32212 !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
	/*	color: white !important;
	border: 1px solid #111;
	background-color: #e32212; */
	/*	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #e32212),
		color-stop(100%, #111));
	background: -webkit-linear-gradient(top, #e32212 0%, #111 100%);
	background: -moz-linear-gradient(top, #e32212 0%, #111 100%);
	background: -ms-linear-gradient(top, #e32212 0%, #111 100%);
	background: -o-linear-gradient(top, #e32212 0%, #111 100%);
	background: linear-gradient(to bottom, #e32212 0%, #111 100%); */
	
}

.paginate_button.current, .paginate_button.current:hover {
	/* 	background-color: white; */
	border-bottom: 2px #e32212;
}
@media  (max-width: 500px) {
	#metricstable thead th{
		padding:0px !important;
	}
	.rwd{
		display:none;
	}
	.updateInfoHolder{
		margin-left:4% !important;
		position:relative;
		right:0% !important;
	}
	.updatedTimeHolder{
		position:relative;
		right:0% !important;
	}
	
	.wrapper {
		cursor: help;
		position: relative;
	  }
	
  .wrapper .tooltip {
	    background: #D4D4D4 ;
		bottom: 100%;
	  color: #fff;
	  display: block;
	  left: -25px;
	  margin-bottom: 15px;
	  opacity: 0;
	  padding: 20px;
	  pointer-events: none;
	  position: absolute;
	  width: 100%;
	  -webkit-transform: translateY(10px);
	     -moz-transform: translateY(10px);
	      -ms-transform: translateY(10px);
	       -o-transform: translateY(10px);
	          transform: translateY(10px);
	  -webkit-transition: all .25s ease-out;
	     -moz-transition: all .25s ease-out;
	      -ms-transition: all .25s ease-out;
	       -o-transition: all .25s ease-out;
	          transition: all .25s ease-out;
	  -webkit-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	     -moz-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	      -ms-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	       -o-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	          box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	}
	
	
	/* This bridges the gap so you can mouse into the tooltip without it disappearing */
	.wrapper .tooltip:before {
	  bottom: -20px;
	  content: " ";
	  display: block;
	  height: 20px;
	  left: 0;
	  position: absolute;
	  width: 100%;
	}  
	
	/* CSS Triangles - see Trevor's post */
	.wrapper .tooltip:after {
	  border-left: solid transparent 10px;
	  border-right: solid transparent 10px;
	  border-top: solid #D4D4D4 10px;
	  bottom: -10px;
	  content: " ";
	  height: 0;
	  left: 50%;
	  margin-left: -13px;
	  position: absolute;
	  width: 0;
	}
	  
	.wrapper:hover .tooltip {
	  font-size: 8px;
	    color: black;
	  opacity: 1;
	  pointer-events: auto;
	  -webkit-transform: translateY(0px);
	     -moz-transform: translateY(0px);
	      -ms-transform: translateY(0px);
	       -o-transform: translateY(0px);
	          transform: translateY(0px);
	}

	/* IE can just show/hide with no transition */
	.lte8 .wrapper .tooltip {
	  display: none;
	}
	
	.lte8 .wrapper:hover .tooltip {
	  display: block;
	}
}
@media  (min-width: 500px) {
	.wrapper{
			display:none !important;
	}
}
</style>
<script>
$(document).ready( function () {
	$('#logSRModal').css('display', "none");
	$('#logSRSubmitResponse').css('display', "none");
	/*$('#logSRModal').css('z-index', -10);*/
	$("input:radio").attr("checked", false);
	
	
	
	$('#closeMarkID, #cancelButtonID').click(function(){
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$("label.error").hide();
		$('#serviceNameSelect option[value!="0"]').remove();
		$("#logSRForm").validate().resetForm();
		$("#logSRModal").find('form')[0].reset();
		$("#logSRForm")[0].reset();
		$('#logSRModal').modal('hide');
	});
		
		
	$('#logServReqButton').click(function(){
		
		$('#_145_dockbar').css('z-index', 1039);
		$('#logSRModal').modal('show');
		$('#logSRModal').css('display', "block");
		$('body').addClass('modal-open');
		$("#categoryLabelID").addClass('error');
	});
	
	$('#logSRModal').on( 'shown', function ( event ) {
		$('#logSRModal').css('display', "block");
		$("#logSRForm")[0].reset();
		$("#logSRForm").validate().resetForm();
		$("#categoryLabelID").addClass('error');
	});
	
	$('#logSRModal').on( 'hidden', function ( event ) {
		$('#logSRModal').css('display', "none");
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$("label.error").hide();
		$('#submitRequestButtonID').removeClass("disableButton");
		document.getElementById("submitRequestButtonID").disabled = false;
		$("#logSRForm").validate().resetForm();
		$(this).find('form')[0].reset();
		$("#logSRForm")[0].reset();
		
	});
	
	$('#logSRSubmitResponse').on( 'shown', function ( event ) {
		$('#logSRModal').css('z-index', 1039);
		$('#logSRSubmitResponse').css('display', "block");
		$(this).css('z-index', 1042);
	});
	
	$('#logSRSubmitResponse').on( 'hidden', function ( event ) {
		$(this).css('display', 'none');
		$('#logSRModal').css('z-index', 1041);
		$('#logSRModal').modal('hide');
		$('.modal-backdrop').remove();
		$('body').removeClass('modal-open');
	});
});

	function addCommentNotes(element){
		if(element.checked){
			 /*var textbox = document.getElementById("customDescription");
			 textbox.value =  "Multiple services are impacted. " + textbox.value;*/
			 $('#deviceNameSelect').attr('disabled','disabled');
		     $('#deviceNameSelect').addClass("disableButton");
		}
		else{
			/*var textbox = document.getElementById("customDescription");
			textbox.value="";*/
			/*$("input:radio").attr("checked", false);
			$('#actionSelect option[value!="0"]').remove();
			$('#subCategorySelect option[value!="0"]').remove();*/
			/*$('#deviceNameSelect option[value!="0"]').remove();*/
			$('#deviceNameSelect').removeAttr('disabled','disabled');
	    	$('#deviceNameSelect').removeClass("disableButton");
			
		}
	}
	function addActionComment(){
		var selectedAction = document.getElementById("actionSelect");
		var textarea = document.getElementById("customDescription").value
		if(selectedAction.value!='0'){
			textarea += "This is "+selectedAction.value+" action.";
		}else{
			document.getElementById("customDescription").value="";
		}
	}
	
	
	function hideAll(){
		$('#submitRequestButtonID').attr('disabled','disabled');
    	$('#submitRequestButtonID').addClass("disableButton");
    	
    	$('#summary').attr('disabled','disabled');
    	$('#summary').addClass("disableButton");
    	
    	$('#regionSelect').attr('disabled','disabled');
    	$('#regionSelect').addClass("disableButton");
    	
    	$('#siteID').attr('disabled','disabled');
    	$('#siteID').addClass("disableButton");
    	
    	$('#serviceNameSelect').attr('disabled','disabled');
    	$('#serviceNameSelect').addClass("disableButton");
    	        	
    	$('#MACDRadio').attr('disabled','disabled');
    	$('#MACDRadio').addClass("disableButton");
    	
    	$('#GenericRadio').attr('disabled','disabled');
    	$('#GenericRadio').addClass("disableButton");
    	
    	$('#actionSelect').attr('disabled','disabled');
    	$('#actionSelect').addClass("disableButton");
    	
    	$('#subCategorySelect').attr('disabled','disabled');
    	$('#subCategorySelect').addClass("disableButton");
    	
    	$('#deviceNameSelect').attr('disabled','disabled');
    	$('#deviceNameSelect').addClass("disableButton");
    	
    	$('#productClassificationSelect').attr('disabled','disabled');
    	$('#productClassificationSelect').addClass("disableButton");
    	
    	$('#customDescription').attr('disabled','disabled');
    	$('#customDescription').addClass("disableButton");
    	
    	$('#cancelButtonID').attr('disabled','disabled');
    	$('#cancelButtonID').addClass("disableButton");
    	
    	$('#MultiServiceCheckBox').attr('disabled','disabled');
    	$('#MultiServiceCheckBox').addClass("disableButton");
    	
    	$('#closeMarkID').attr('disabled','disabled');
    	$('#closeMarkID').addClass("disableButton2");
	}
	function unhideAll(){
		$('#submitRequestButtonID').removeAttr('disabled','disabled');
    	$('#submitRequestButtonID').removeClass("disableButton");
    	
    	$('#regionSelect').removeAttr('disabled','disabled');
    	$('#regionSelect').removeClass("disableButton");
    	
    	$('#summary').removeAttr('disabled','disabled');
    	$('#summary').removeClass("disableButton");
    	
    	$('#siteID').removeAttr('disabled','disabled');
    	$('#siteID').removeClass("disableButton");
    	
    	$('#serviceNameSelect').removeAttr('disabled','disabled');
    	$('#serviceNameSelect').removeClass("disableButton");
    	        	
    	$('#MACDRadio').removeAttr('disabled','disabled');
    	$('#MACDRadio').removeClass("disableButton");
    	
    	$('#GenericRadio').removeAttr('disabled','disabled');
    	$('#GenericRadio').removeClass("disableButton");
    	
    	$('#actionSelect').removeAttr('disabled','disabled');
    	$('#actionSelect').removeClass("disableButton");
    	
    	$('#subCategorySelect').removeAttr('disabled','disabled');
    	$('#subCategorySelect').removeClass("disableButton");
    	
    	$('#deviceNameSelect').removeAttr('disabled','disabled');
    	$('#deviceNameSelect').removeClass("disableButton");
    	
    	$('#productClassificationSelect').removeAttr('disabled','disabled');
    	$('#productClassificationSelect').removeClass("disableButton");
    	
    	
    	$('#customDescription').removeAttr('disabled','disabled');
    	$('#customDescription').removeClass("disableButton");
    	
    	$('#cancelButtonID').removeAttr('disabled','disabled');
    	$('#cancelButtonID').removeClass("disableButton");
    	
    	$('#MultiServiceCheckBox').removeAttr('disabled','disabled');
    	$('#MultiServiceCheckBox').removeClass("disableButton");
    	
    	$('#closeMarkID').removeAttr('disabled','disabled');
    	$('#closeMarkID').removeClass("disableButton2");
	}
	
</script>

<script type="text/javascript">
	var getSRGraphURL="${getSRGraph}";

	$(document).ready( function () {

	var parameterReceived = getQueryString(window.location.href);
	if(parameterReceived!=-1){
		var parametersDecodedURL = Base64.decode(parameterReceived);
		var parametersFromURL = parametersDecodedURL.split('&');
		sendParametersForSRCRDetails(parametersFromURL[0],parametersFromURL[1]);
	}
	
	$('#tt-sr-modal').hide();
	$('#filterscol2').append('<liferay-ui:message key="No filters applied" />');
	$('#advSearchButton').on('click', function () {
		$('#_145_dockbar').css('z-index', 1039);
		$('#tt-sr-modal').css('display', 'block');	  
	    $('#tt-sr-modal').modal('show');
	    $(".srFilters").show();
		$(".crFilters").hide();
		$('#requestTypeSelect').html('');
		$('#requestTypeSelect').append($('<option>').text("Service Request").val("serviceRequest"));
		$('#requestTypeSelect').append($('<option>').text("Change Request").val("changeRequest"));
	});
	

	$('#tt-sr-modal').on('shown', function ( event ) {
		$(this).css('display', 'block');
		$('#_145_dockbar').css('z-index', 1039);
			
	});

	$('#tt-sr-modal').on('hidden', function ( event ) {
		$(this).css('display', 'none');		
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
	});
	   
	$("#requestTypeSelect").on('change', function(){
	   
		searchType = $("#requestTypeSelect").val();
		$(".commonFilters").show();
		if(searchType == "serviceRequest"){
			$(".crFilters").hide();
			$(".srFilters").show();
		}
		// CR Details--- IF Requesttype=change will display cr fields on details page.--------------------> Divya
		
		else if(searchType == "changeRequest"){
			$(".crFilters").show();
			$(".srFilters").hide();
		}
		
	});
	
	$("#advancedajaxform")[0].reset();
	
	/*ajax call to get the grid*/
	var getRequestMetricsURL = "${getRequestMetrics}";
	$.ajax({
			type : "POST",
			url : getRequestMetricsURL,
			dataType : 'json',
			success : function(data) 
			{
				var crsrJsonObj;
				try 
				{
					crsrJsonObj = jQuery.parseJSON(JSON.stringify(data));
					
					populateGridUsingJson(crsrJsonObj);
					
				} catch (e) 
				{
					//alert(e);
				}
			},
			error : function(){
				//alert("error");
			}
		});
	});

	function getQueryString(url){
		if (url.indexOf('?') > -1) {
			var pos = url.indexOf('?');
			var extract = url.substring(pos + 1,url.length);
			return extract;
		}
		else{
			return -1;
		}
	}

function getService() {
 	var selectedValue = $('#siteID').val();
 	var getServiceURL = "${getServiceURL}"
	var jsonSiteID = {"selectedSiteID" : selectedValue};
	var dataJson={selectedSiteIDJson : JSON.stringify(jsonSiteID)};
	$.ajax({
		url: getServiceURL,
        type: "POST",
        data: dataJson,
        success: function(response){
        	$('#serviceNameSelect option[value!="allServices"]').remove();
        	var x=1;
        	var arr = response.split("#####");
        	var len = arr.length;
        	var reslen = len-x;
        	for(var i=0; i<reslen;i++){
        		
        		$('#serviceNameSelect').append($('<option>').text(response.split("#####")[i].split('@@@@@')[1]).val(response.split("#####")[i].split('@@@@@')[1]));
        	}
        	$('#serviceNameSelect option[value=""]').remove();
      },
		error: function(xhr){
			
       // alert('Error' + xhr);
    }
	});
}

	function getAssociatedService(dropdown) {
	 	var selectedValue = dropdown.options[dropdown.selectedIndex].value;
	 	/* alert('Selected Value + ' + selectedValue); */
	 	var getServiceURL = "${getServiceURL}"
		var jsonSiteID = {"selectedSiteID" : selectedValue};
		var dataJson={selectedSiteIDJson : JSON.stringify(jsonSiteID)};
		$.ajax({
			url: getServiceURL,
	        type: "POST",
	        data: dataJson,
	        success: function(response){
	        	$('#serviceName option[value!="allServices"]').remove();
	        	var x=1;
	        	var arr = response.split("#####");
	        	var len = arr.length;
	        	var reslen = len-x;
	        	for(var i=0; i<reslen;i++){
	        		
	        		$('#serviceName').append($('<option>').text(response.split("#####")[i].split('@@@@@')[1]).val(response.split("#####")[i].split('@@@@@')[1]));
	        	}
	        	$('#serviceName option[value=""]').remove();
	      },
			error: function(xhr){
				
	        //alert('Error' + xhr);
	    }
		});
	}
	
	function getAllStatus(tabSelected) {
	 	
	 	var getStatusURL = "${getStatusURL}"
		var jsonStatusID = {"selectedStatusID" : tabSelected};
		var dataJson={selectedStatusIDJson : JSON.stringify(jsonStatusID)};
		$.ajax({
			url: getStatusURL,
	        type: "POST",
	        data: dataJson,
	        success: function(response){
	        	$('#status option[value!="allStatus"]').remove();
	        	var x=1;
	        	var arr = response.split("#####");
	        	var len = arr.length;
	        	var reslen = len-x;
	        	for(var i=0; i<reslen;i++){
	        		
	        		$('#status').append($('<option>').text(response.split("#####")[i].split('@@@@@')[1]).val(response.split("#####")[i].split('@@@@@')[1]));
	        	}
	        	$('#status option[value=""]').remove();
	      },
			error: function(xhr){
				
	        //alert('Error' + xhr);
	    }
		});
	}
	
	function populateGridUsingJson(crsrJsonObj)
	{
		$("#ServiceRequest").html(crsrJsonObj.ServiceRequest);
		$("#CustomerInitiatedPending").html(crsrJsonObj.CustomerInitiatedPending);
		$("#CustomerInitiatedApproved").html(crsrJsonObj.CustomerInitiatedApproved);
		$("#TTInitiatedPending").html(crsrJsonObj.TTInitiatedPending);
		$("#TTInitiatedApproved").html(crsrJsonObj.TTInitiatedApproved);
	}
	
	//
	//Pipelining function for DataTables. To be used to the `ajax` option of DataTables
	//
	$.fn.dataTable.pipeline = function(opts) {
		// Configuration options
		var conf = $.extend({
			pages : 3, // number of pages to cache
			url : '', // script url
			data : null, // function or object with parameters to send to the server
			// matching how `ajax.data` works in DataTables
			method : 'GET' // Ajax HTTP method
		}, opts);

		// Private variables for storing the cache
		var cacheLower = -1;
		var cacheUpper = null;
		var cacheLastRequest = null;
		var cacheLastJson = null;

		return function(request, drawCallback, settings) {
			var ajax = false;
			var requestStart = request.start;
			var drawStart = request.start;
			var requestLength = request.length;
			var requestEnd = requestStart + requestLength;

			if (settings.clearCache) {
				// API requested that the cache be cleared
				ajax = true;
				settings.clearCache = false;
			} else if ((cacheLower < 0) || (requestStart < cacheLower)
					|| (requestEnd > cacheUpper)) {
				// outside cached data - need to make a request
				ajax = true;
			} else if (JSON.stringify(request.order) !== JSON
					.stringify(cacheLastRequest.order)
					|| JSON.stringify(request.columns) !== JSON
							.stringify(cacheLastRequest.columns)
					|| JSON.stringify(request.search) !== JSON
							.stringify(cacheLastRequest.search)) {
				// properties changed (ordering, columns, searching)
				ajax = true;
			}

			// Store the request for checking next time around
			cacheLastRequest = $.extend(true, {}, request);

			if (ajax) {
				// Need data from the server
				if (requestStart < cacheLower) {
					requestStart = requestStart
							- (requestLength * (conf.pages - 1));

					if (requestStart < 0) {
						requestStart = 0;
					}
				}

				cacheLower = requestStart;
				cacheUpper = requestStart + (requestLength * conf.pages);

				request.start = requestStart;
				request.length = requestLength * conf.pages;

				// Provide the same `data` options as DataTables.
				if ($.isFunction(conf.data)) {
					// As a function it is executed with the data object as an arg
					// for manipulation. If an object is returned, it is used as the
					// data object to submit
					var d = conf.data(request);
					if (d) {
						$.extend(request, d);
					}
				} else if ($.isPlainObject(conf.data)) {
					// As an object, the data given extends the default
					$.extend(request, conf.data);
				}

				settings.jqXHR = $.ajax({
					"type" : conf.method,
					"url" : conf.url,
					"data" : request,
					"dataType" : "json",
					"cache" : false,
					"success" : function(json) {
						cacheLastJson = $.extend(true, {}, json);

						if (cacheLower != drawStart) {
							json.data.splice(0, drawStart - cacheLower);
						}

						json.data.splice(requestLength, json.data.length);

						drawCallback(json);
					}
				});
			} else {
				json = $.extend(true, {}, cacheLastJson);
				json.draw = request.draw; // Update the echo for each response
				json.data.splice(0, requestStart - cacheLower);
				json.data.splice(requestLength, json.data.length);

				drawCallback(json);
			}
		}
	};

	//Register an API method that will empty the pipelined data, forcing an Ajax
	//fetch on the next draw (i.e. `table.clearPipeline().draw()`)
	$.fn.dataTable.Api.register('clearPipeline()', function() {
		return this.iterator('table', function(settings) {
			settings.clearCache = true;
		});
	});
	
	//code for "completed SR Graph" starts------ 
	
	function graphMapperFnc(obj)
	    {
		  var jsonObj = {"initiateSource" : obj};
		  
		  var graphSRData = {graphSRDataIdentifier : JSON.stringify(jsonObj)};
		  
		  $.ajax({
					    type : "POST",
					    url : getSRGraphURL,
					    dataType : 'json',
						data : graphSRData,
						success : function(data) 
						{
							drawBarFillGraph(data,obj,90,"scrcategory");
						},
				        error : function(data) 
				        {
							log.error("Data recieving failed!!");
					    }
	     		});
		   return false;
	    } 

	//Service request table population and rendering at first time.
	function ServiceRequestPortlet_getSRList(srListURL) {	

		console.log(srListURL);

		//DataTables initialisation
		var oTable=$('#srListingTable').dataTable({
			"processing" : true,
			"serverSide" : true,
			"bDestroy": true,			
			'bInfo' : true,
			"oLanguage": {
			      "sInfo": '<liferay-ui:message key="Showing"/> _START_-_END_ <liferay-ui:message key="of"/> _TOTAL_',
			      "sInfoEmpty": '<liferay-ui:message key="Showing"/> 0-0 <liferay-ui:message key="of"/> 0',
			      "sEmptyTable": '<liferay-ui:message key="No data available in table"/>',
				  "oPaginate": {
			        "sPrevious": '&lt;&nbsp;<liferay-ui:message key="Previous"/>',
					"sNext": '<liferay-ui:message key="Next"/>&nbsp;&gt;'
			      }
				  
			    },
			'bFilter' : false,
			'bLengthChange' : false,
			"aoColumns" : [
				{
					"sClass": "tt-sr-adjCol-reqId","sWidth" : "13%"
				},				
				{
					"sClass": "tt-sr-adjCol-subject","sWidth" : "35%"
				},
				{
					"sClass": "tt-sr-adjCol-requestType","sWidth" : "12%"
				}, 
				{
					"sClass": "tt-sr-adjCol-lastUpdate","sWidth" : "15%"
				}, 
				{
					"sClass": "tt-sr-adjCol-requestor","sWidth" : "15%"
				},
				{
					"sClass": "tt-sr-adjCol-status","sWidth" : "10%"
				} 
			],

			"ajax" : $.fn.dataTable.pipeline({
				url : srListURL,
				pages : 3
			// number of pages to cache
			})
		});	
		oTable.fnSort( [ [3,'desc'] ] );
	}
	
	//Method to redraw a datatable for search
	function ServiceRequestPortlet_getSearchedSRList(srListURLSearch) {		

		 //console.log(srListURLSearch);
		 var datatable = $('#srListingTable').dataTable();
		 
		 datatable.fnReloadAjax( srListURLSearch );	
	}
			
	function ServiceRequestPortlet_getTabClickedSRList(srListURLTabClicked) {		

		//console.log(srListURLSearch);
		 var datatable = $('#srListingTable').dataTable();
		 
		 datatable.fnReloadAjax( srListURLTabClicked );	
	}
	
	
	// Code for function SRI list retrieve added by Sanjith Acharya----------------------- start
	function ServiceRequestPortlet_getSRIList(data) {

		
		var dataSet = data.datatableServiceRequestItems.data;

		//DataTables initialisation
		$('#sriListingTable').dataTable({
		
			"processing" : false,
			"serverSide" : false,
			'bInfo' : false,
			"bPaginate": false,
			"bSortable" : false,
			"bSort": false, 
            "aaSorting": [[0]], 
			'bFilter' : false,
			'bDestroy' : true,
			'bLengthChange' : false,
			"aoColumns" : [ {
				"sClass": "tt-sr-adjCol-itemid-th","sWidth" : "15%"
			}, // 1st column width 
			{
				"sClass": "tt-sr-adjCol-category-th","sWidth" : "10%"
			}, // 2nd column width 
			{
				"sClass": "tt-sr-adjCol-request-th","sWidth" : "25%"
			}, // 3rd column width and so on 
			{
				"sClass": "tt-sr-adjCol-type-th","sWidth" : "15%"
			}, {
				"sClass": "tt-sr-adjCol-status-th","sWidth" : "12%"
			}, {
				"sClass": "tt-sr-adjCol-lastupdatedon-th","sWidth" : "18%"
			}, {
				"sWidth" : "5%"
			} ],
			"data": dataSet
	
		});

		var table = $('#sriListingTable').DataTable();
		var info = table.page.info();
		$('#sriListingTable_info').html(
				'Currently showing page ' + (info.page + 1) + ' of '
						+ info.pages + ' pages.');
		
		//capture the click of first column ie service id  
		$('#sriListingTable tbody').on(
				'click',
				'td',
				function() {

					var colname = $('#sriListingTable thead tr th').eq(
							$(this).index()).html().trim();
					var data = $(this).html().trim();
					var row = $(this).parent().find('td').html().trim();
					var colIndex = $(this).index();

					//if selected index is for service id show the view ticket screen using the values.
					if (colIndex.eq(0)) {
						/* alert('Data:' + data);
						alert('Row:' + row);
						alert('Column:' + colname); */
					}
				});
	}
	// Code for function SRI list retrieve added by Sanjith Acharya----------------------- end

	String.prototype.ReplaceAll =function(stringToFind,stringToReplace){
		 
	    var temp = this;
	 
	    var index = temp.indexOf(stringToFind);
	 
	        while(index != -1){
	 
	            temp = temp.replace(stringToFind,stringToReplace);
	 
	            index = temp.indexOf(stringToFind);
	 
	        }
	 
	        return temp;
	 
	    }
	
	// Code added by Sanjith Acharya to implement retrieve SR and SRI list

	var getRequestDetailsURL = "${getRequestDetails}";
	
	$(function() {
		$('#advancedajaxform').submit(function(e) {
				
				e.preventDefault();
				$("#tt-sr-normalSearchForm")[0].reset();	
				var srListURL = "${getSRListURI}";				
				var srUserData = $('#advancedajaxform').serialize();
				var advSearchQueryUrl = srListURL + "&"+srUserData + "&maxResults=" + maxResults + "&tabSelected=" + tabSelected;
				ServiceRequestPortlet_getSearchedSRList(advSearchQueryUrl);
				$('#filterscol2').html('');
				$('#filterscol2').append('<liferay-ui:message key="Advanced Search filters applied" />');
				$('#tt-sr-modal').modal('hide');
				$('#requestTypeSelect').html('');
				$('#requestTypeSelect').append($('<option>').text("Service Request").val("serviceRequest"));
				$('#requestTypeSelect').append($('<option>').text("Change Request").val("changeRequest"));
				
		});
	
	});
	
	function clearAllRequestFilelds(){		
		var requestFields = new Array("Saab", "Volvo", "BMW");
		$("#srdetails-summary").html("");
		$("#tt-srd-srID").html("");
		$("#tt-srd-status").html("");
		$("#tt-srd-requestor").html("");
		$("#tt-srd-lastUpdated").html("");
		$("#tt-srd-createdOn").html("");
		$("#tt-srd-startDate").html("");
		$("#tt-srd-endDate").html("");
		$("#tt-srd-requestType").html("");
		$("#tt-srd-outagePresent").html("");
		$("#tt-srd-outageStartTime").html("");
		$("#tt-srd-outageEndTime").html("");		
		$("#tt-srd-description").html("");	
		
		$("#tt-srd-risk").html("");
		$("#tt-srd-priority").html("");
		$("#tt-srd-impact").html("");
		$("#tt-srd-caseManager").html("");
		$("#tt-srd-resolverGroup").html("");
		$("#tt-srd-comments").html("");
		$("#tt-sri-commentTable-td").html("");
		$("#tt-srd-siteName").html("");
		$("#tt-srd-category").html("");
		$("#tt-srd-changeSource").html("");
		$("#tt-srd-changeTypeTT").html("");
		$("#tt-srd-changeTypeCustomer").html("");
		$("#tt-srd-deviceName").html("");
		$("#tt-srd-serviceName").html("");
		
		$("#tt-srd-requestedByDate").html("");
		$("#tt-srd-telkomtelstraapprover").html("");
		$("#tt-srd-telkomtelstraapproverdate").html("");
		$("#tt-srd-telkomtelstraapprovergroup").html("");
		$("#tt-srd-customerapprover").html("");
		$("#tt-srd-customerapproverdate").html("");
		$("#tt-srd-outcomestatus").html("");
	}
	
	function PopulateAllRequestFilelds(jsonDATA,requestType){
		$("#advancedajaxform")[0].reset();
		$("#srdetails-summary").html(jsonDATA.summary);
		$("#tt-srd-srID").html(jsonDATA.serviceRequestId);
		$("#tt-srd-status").html(jsonDATA.status);
		if(jsonDATA.status=='Closed'||jsonDATA.status=='Cancelled'||jsonDATA.status=='Complete'||jsonDATA.status=='Completed'){
			selectedTab("completedTab");
		}
		$("#tt-srd-requestor").html(jsonDATA.requestor);
		$("#tt-srd-lastUpdated").html(jsonDATA.lastUpdated);
		$("#tt-srd-createdOn").html(jsonDATA.createdOn);
		$("#tt-srd-startDate").html(jsonDATA.startDate);
		$("#tt-srd-endDate").html(jsonDATA.endDate);
		$("#tt-srd-requestType").html(jsonDATA.requestType);
		
		if(requestType == 'Change')
		{
			$("#tt-srd-outagePresent").html(jsonDATA.outagePresent);
			$("#tt-srd-outageStartTime").html(jsonDATA.outageStartTime);
			$("#tt-srd-outageEndTime").html(jsonDATA.outageEndTime);		
			$("#tt-srd-description").html(jsonDATA.description);
			
			$("#tt-srd-risk").html(jsonDATA.risk);
			$("#tt-srd-priority").html(jsonDATA.priority);
			$("#tt-srd-impact").html(jsonDATA.impact);
			$("#tt-srd-caseManager").html(jsonDATA.casemanager);
			$("#tt-srd-resolverGroup").html(jsonDATA.resolverGroup);
			$("#tt-srd-siteName").html(jsonDATA.siteName);
			$("#tt-srd-category").html(jsonDATA.category);
			$("#tt-srd-changeSource").html(jsonDATA.changeSource);
			
			if(jsonDATA.changeSource == 'Customer')
			{
				$(".CustomerData").hide();
				$(".TTData").show();
			}
			else
			{
				$(".CustomerData").show();
				$(".TTData").hide();
			}
			
			$("#tt-srd-changeTypeTT").html(jsonDATA.changeType);
			$("#tt-srd-changeTypeCustomer").html(jsonDATA.changeType);
			var deviceNameList = jsonDATA.deviceName.replace(/,/g,'<br/>');
			$("#tt-srd-deviceName").html(deviceNameList);
			var serviceNameList = jsonDATA.serviceName.replace(/,/g,'<br/>');
			$("#tt-srd-serviceName").html(serviceNameList);
			
			var commentList = jsonDATA.comments;
			var commentAppendStr1='<table style="margin: 3px; width: 99%"><tr><td rowspan="2" style="width: 45px; padding:0px !important"><div><img src="<%=request.getContextPath()%>/images/tickets/u33.png" height="40" width="40"></img></div></td><td style="padding:0px !important"><p style="float: left; color: #e32212; margin-bottom: 0px;">';
			var commentAppendStr2 = '</p><p style="float: right; margin-bottom: 0px;">';
			var commentAppendStr3 = '</p></td></tr><tr><td style="padding: 0px !important">';
			var commentAppendStr4 = '</td></tr></table>';
			for (var i = 0; i < commentList.length; i++) {
				/* alert('Comments '+commentsList[i]); */
				
				document.getElementById("tt-sri-commentTable-td").innerHTML += commentAppendStr1
						+ commentList[i].userId
						+ commentAppendStr2
						+ commentList[i].modifiedDate
						+ commentAppendStr3
						+ commentList[i].notes
						+ commentAppendStr4;
			}
			
			$("#tt-srd-requestedByDate").html(jsonDATA.requestedByDate);
			var approverList = jsonDATA.approver.replace(/,/g,'<br/>');
			$("#tt-srd-telkomtelstraapprover").html(approverList);
			var approverDateList = jsonDATA.approvalDate.replace(/,/g,'<br/>');
			$("#tt-srd-telkomtelstraapproverdate").html(approverDateList);
			var approvalGroupList = jsonDATA.approvalGroup.replace(/,/g,'<br/>');
			$("#tt-srd-telkomtelstraapprovergroup").html(approvalGroupList);
			
			$("#tt-srd-customerapprover").html(jsonDATA.customerApprover);
			$("#tt-srd-customerapproverdate").html(jsonDATA.customerApprovalDate);
			$("#tt-srd-outcomestatus").html(jsonDATA.outcomeStatus);
		}
		
		manageCrSrFiledDisplay(requestType);	
	}
	
	//function to show hide the change or service fileds as per the request type
	function manageCrSrFiledDisplay(requestType){
		
		if(requestType == "Change"){
			$("div.crColumn").css('display','block');			 
			$("div.srColumn").css('display','none');			
		}
		else {			
			$("div.srColumn").css('display','block');			 
			$("div.crColumn").css('display','none');			
		}
	}
	
	function getSingleServiceRequestDetails(obj, requestType) {
		/* alert('Clicked ' + obj.getAttribute("id")); */
		var requestIdReceived = obj.getAttribute("id");
		sendParametersForSRCRDetails(requestIdReceived, requestType);
	}
	
	function sendParametersForSRCRDetails(requestIdParameter, requestType){
		
		clearAllRequestFilelds();
		
		var jsonObj = {
			"requestid" : requestIdParameter,
			"requestType": requestType
		};

		var serviceRequestData = {
			serviceRequestFilterIdentifier : JSON.stringify(jsonObj)
		};

		$.ajax({
					type : "POST",
					url : getRequestDetailsURL,
					dataType : 'json',
					data : serviceRequestData,
					success : function(data) {
					
						var jsonDATA;
						try {
							jsonDATA = jQuery.parseJSON(JSON.stringify(data));
						} catch (e) {
							/* alert(e); */
						}
						
						PopulateAllRequestFilelds(jsonDATA,requestType);						
						
						$("#tt-srlisting-container").css("display", "none");
						$("#updateDataHolderDiv").css("display", "none");
						$(".update-data-tab").css("display", "none");
						if(tabSelected=='activeTab'){
							$('#completedTab').addClass("displayNoneImportant");
						}
						else if(tabSelected=='completedTab'){
							$('#activeTab').addClass("displayNoneImportant");
						}
						$("#tt-srd-container").slideToggle();
						
						ServiceRequestPortlet_getSRIList(data);

					},
					error : function(data) {
					}

				});

	}

	//Code ended by Sanjith Acharya to implement retrieve SR and SRI list

	// Code added by Sanjith Acharya to implement retrieve SRI and comments list-------------------

	var getServiceRequestItemDetailsURL = "${getServiceRequestItemDetails}";

	function getSingleServiceRequestItemDetails(obj) {
		/* alert('Clicked ' + obj.getAttribute("id")); */
		var itemIdReceived = obj.getAttribute("id");

		var jsonObj = {
			"itemid" : itemIdReceived
		};

		var serviceRequestItemData = {
			serviceRequestItemFilterIdentifier : JSON.stringify(jsonObj)
		};
		var commentAppendStr1='<table style="margin: 3px; width: 99%"><tr><td rowspan="2" style="width: 45px; padding:0px !important"><div><img src="<%=request.getContextPath()%>/images/tickets/u33.png" height="40" width="40"></img></div></td><td style="padding:0px !important"><p style="float: left; color: #e32212; margin-bottom: 0px;">';
		var commentAppendStr2 = '</p><p class="mediasr" style="margin-bottom: 0px;">';
		var commentAppendStr3 = '</p></td></tr><tr><td style="padding: 0px !important">';
		var commentAppendStr4 = '</td></tr></table>';
		document.getElementById("tt-srd-ritmid").innerHTML = "";
		document.getElementById("tt-srd-serviceName-item").innerHTML = "";
		document.getElementById("tt-srd-siteName-item").innerHTML = "";
		document.getElementById("tt-srd-deviceName-item").innerHTML = "";
		document.getElementById("tt-sri-commentTable-td-item").innerHTML = "";

		$.ajax({
					type : "POST",
					url : getServiceRequestItemDetailsURL,
					dataType : 'json',
					data : serviceRequestItemData,
					success : function(data) {
						var jsonDATA = data;

						document.getElementById("tt-srd-ritmid").innerHTML = jsonDATA.ritmId;
						document.getElementById("tt-srd-serviceName-item").innerHTML = jsonDATA.serviceName;
						document.getElementById("tt-srd-siteName-item").innerHTML = jsonDATA.siteName;
						/* document.getElementById("tt-srd-deviceName").innerHTML = jsonDATA.requestor; */
						var deviceNameList = jsonDATA.deviceNameList;
						for (var i = 0; i < deviceNameList.length; i++) {
							/* alert('Device '+deviceNameList[i]); */
							document.getElementById("tt-srd-deviceName-item").innerHTML += deviceNameList[i]
									+ "<br>";
						}
						var commentList = jsonDATA.commentsList;
						for (var i = 0; i < commentList.length; i++) {
							/* alert('Comments '+commentsList[i]); */
							document.getElementById("tt-sri-commentTable-td-item").innerHTML += commentAppendStr1
									+ commentList[i].userId
									+ commentAppendStr2
									+ commentList[i].modifiedDate
									+ commentAppendStr3
									+ commentList[i].notes
									+ commentAppendStr4;
						}
						$('#ritmModal').modal('show');

					},
					error : function(data) {

					}

				});

	}
	function selectedTab(clicked_id){
		
		var srListURL = "${getSRListURI}";
			//append the search term and filter the results 
			
			tabSelected=clicked_id;
			
			$("#advancedajaxform")[0].reset();
			
			$('#filterscol2').html('');
			$('#filterscol2').append('<liferay-ui:message key="No filters applied"/>');
			
			if(tabSelected=="activeTab"){
				$("#activeTab").addClass("tt-sr-header-tabs-selected");
				$("#completedTab").removeClass("tt-sr-header-tabs-selected");
				$("#scrTrendSection").hide();
				$("#metricstablediv").show();
				
			}
			if(tabSelected=="completedTab"){
				$("#completedTab").addClass("tt-sr-header-tabs-selected");
				$("#activeTab").removeClass("tt-sr-header-tabs-selected");
				$("#scrTrendSection").show();
				$("#metricstablediv").hide();
				
			}
			getAllStatus(tabSelected);
		
			var srListURLTabClicked = srListURL + "&maxResults=" + maxResults +"&tabSelected=" +tabSelected + "&normalSearch=true";
				
			ServiceRequestPortlet_getTabClickedSRList(srListURLTabClicked);
			
		}
		var tabSelected='${tabStatusCheck}';
		var maxResults = 10;
	//Code ended by Sanjith Acharya to implement retrieve SRI and comments list
	$(document).ready(function() {
		
		$(".metricEX").click(function () 
		{		
			$("#advancedajaxform")[0].reset();
			//append the search term and filter the results 
			var srListURLSearch = srListURL + "&searchBySRCustTT=true" + "&clickedTdId=" + $(this)[0].id + "&maxResults=" + maxResults + "&tabSelected=" + tabSelected;
			ServiceRequestPortlet_getSearchedSRList(srListURLSearch);
			putFilterBox(this);
		});
		
		$(".update-data-tab").click(function(){
			$("#advancedajaxform")[0].reset();
		});
		

		var srListURL = "${getSRListURI}";
		getAllStatus(tabSelected);
		
		graphMapperFnc("srCustInitiated");
		graphMapperFnc("crCustInitiated");
		graphMapperFnc("crTtInitiated");
	
		ServiceRequestPortlet_getSRList(srListURL+ "&normalSearch=true" + "&tabSelected="+tabSelected);
		
		if(tabSelected=="activeTab"){
			$("#activeTab").addClass("tt-sr-header-tabs-selected");
			$("#completedTab").removeClass("tt-sr-header-tabs-selected");
			$("#scrTrendSection").hide();
			$("#metricstablediv").show();
				
		}
		if(tabSelected=="completedTab"){
			$("#completedTab").addClass("tt-sr-header-tabs-selected");
			$("#activeTab").removeClass("tt-sr-header-tabs-selected");
			$("#scrTrendSection").show();
			$("#metricstablediv").hide();
				
		}
		
		/* $("#completedTab").click(function () {
			alert("clickted Tab");
			
			//append the search term and filter the results 
			
			tabSelected=$(this).parent().attr("id");
			alert("tab selected "+tabSelected);
			var srListURLTabClicked = srListURL + "&tabSelected=" +tabSelected;
				
				ServiceRequestPortlet_getTabClickedSRList(srListURLTabClicked);
			}); */
		
		$("#srListSearchBtn").click(function () {
			
		 //TODO:needs to take from props.
		
		//append the search term and filter the results 
		var srListURLSearch = srListURL + "&searchTerm=" + $('#srListSearchTxt').val() + "&maxResults=" + maxResults + "&tabSelected=" + tabSelected + "&normalSearch=true";
			
			ServiceRequestPortlet_getSearchedSRList(srListURLSearch);
		});
	});
	
</script>
<script type="text/javascript">

function submitFm() {

	document.getElementById("refreshButton").disabled = 'disabled';
	document.getElementById("refreshButton").setAttribute("class", "updateButtonDisable");
	
	/* var refreshSRURL = "${refreshSR}";
	$.ajax({
		type : "POST",
		url : refreshSRURL,
		success : function(data) 
		{
			alert("success");
		},
		error : function(){
			alert("error");
		}
	}); */
	var url = '<%=refreshSR.toString()%>';
		document.forms["fm"].action = url;
		document.forms["fm"].submit();
}

$(function() {

	$('#tt-sr-normalSearchForm').submit(function(e) {
				e.preventDefault();
				$('#filterscol2').html('');
				$('#filterscol2').append('<liferay-ui:message key="No filters applied"/>');
				$("#advancedajaxform")[0].reset();
			});
});

$(function() {
	$(".datepicker").datepicker();
});

function resizeToMinimum(w,h){
    w=w>window.outerWidth?w:window.outerWidth;
    h=h>window.outerHeight?h:window.outerHeight;
    window.resizeTo(w, h);
};
window.addEventListener('resize', function(){resizeToMinimum(400,400)}, false)
</script>

<!-- TODO:needs to read the labels from language -->

<html>
<head>
</head>
<body>
<div class="container-fluid tt-service-breadcrumb">
	<div class="row-fluid span12" >
		<div class="span6 tt-create-service-breadcrumb" style="display: inline-flex;">
			<div class="tt-page-icon">
				<img src="<%=request.getContextPath()%>/images/ServiceRequest_G_32.png"></img>
			</div>
			<div class="tt-service-br-header"><liferay-ui:message key="Service"/>/<liferay-ui:message key="Change Request"/></div>
		</div>
		<div class="span6 tt-create-service-breadcrumb" style="margin-bottom: 1px; float: right !important">
		<div id="updateDataHolderDiv">
			<div class="rwd updateInfoHolder"><liferay-ui:message key="Last updated on" /></div>
			
			<div class="wrapper updateInfoHolder"><liferay-ui:message key="Last upd.." /><div class="tooltip" style="margin-left:5px;text-align:center;">Last updated on</div></div>
			<div class="updatedTimeHolder">${lastRefreshDate}</div>
			<div class="updateButtonHolder">
			<form name="fm" method="Post">
				<%if(allowAccess)
				{
				%>
					<button type="button" id="refreshButton" class="btn-primary-tt refresh" onclick="submitFm()">
						<liferay-ui:message key="Update Data"/>
					</button>
				<%
				}
				else
				{
				%>
				<div class="updateButtonHolder">
					<button type="button" id="refreshButton" style="background-color: #CCC !important; color: white !important;" class="btn-primary-tt refresh" title="Contact Administrator">
						<liferay-ui:message key="Update Data"/>
					</button>
				</div>
				<%
				}
				%>	
			</form>
			</div>
		</div>
		</div>
		
	</div>
	<div class="span8 tt-create-service-ctr" style=" float: right;">
			<button type="button" id="logServReqButton" class="img-create-servicerequest" data-toggle="modal"><liferay-ui:message key="Log a New Request" /></button>
		</div>
	<div class="row-fluid">
		<div class="span12 incHeaderPanel" style="left: 0px !important;">
			<div>
				<a href="javascript:void(0);" class="tt-sr-header-tabs tt-sr-header-tabs-selected" id="activeTab" onclick="return selectedTab(this.id)" style="outline: none!important"><liferay-ui:message key="Active Request" /></a>
				<a href="javascript:void(0);" class="tt-sr-header-tabs" id="completedTab" onclick="return selectedTab(this.id)" style="outline: none!important"><liferay-ui:message key="Completed Request" /></a>
			</div>
		</div>
	</div>
</div>

<div id="tt-srlisting-container">
	<div id="serviceRequestListBox">


		<div id="scrTrendSection">
			<div id="srTrendsHeading"><liferay-ui:message key="Service"/>/<liferay-ui:message key="Change Request Trend" /></div>
			<div id="srTrendsShortDescription"><liferay-ui:message key="Service"/>/<liferay-ui:message key="Change Requests against time, showing number of Service"/>/<liferay-ui:message key="Change Requests raised each week in a duration of last three months." /></div>

			<div id="varianttrendbody">

				<div id="scrcategorysrCustInitiated" class="varianttrends">
					<div class="varianttrendhead"><div class="variantcategory"><liferay-ui:message	key="Customer Initiated Service Request:" />&nbsp</div><div class="variantcategorycount"></div></div>
					<div class="graphOverlay">
			
					</div>

					<div class="graphStatistics" id="graphStatisticssrCustInitiated">
					</div>
					<div class="xMarkersGraph"></div>
				</div>

				<div id="scrcategorycrCustInitiated" class="varianttrends">
					<div class="varianttrendhead"><div class="variantcategory"><liferay-ui:message	key="Customer Initiated Change Request:" />&nbsp</div><div class="variantcategorycount"></div></div>
					<div class="graphOverlay">
			
					</div>

					<div class="graphStatistics" id="graphStatisticscrCustInitiated">
					</div>
					<div class="xMarkersGraph"></div>
				</div>

				<div id="scrcategorycrTtInitiated" class="varianttrends">
					<div class="varianttrendhead"><div class="variantcategory"><liferay-ui:message	key="telkomtelstra Initiated Change Request:" />&nbsp</div><div class="variantcategorycount"></div></div>
					<div class="graphOverlay">
			
					</div>

					<div class="graphStatistics" id="graphStatisticscrTtInitiated"></div>
					<div class="xMarkersGraph"></div>
				</div>
			</div>
		</div>

		<div class="listingTitle">
				<liferay-ui:message key="Service"/>/<liferay-ui:message key="Change Request Listing" />
		</div>

		<div id="metricstablediv">
			<table id="metricstable">
				<thead>
					<tr>
						<th class="gridLeftCols" rowspan="2">
							<liferay-ui:message key="Request Type"/>
						</th>
						<th class="rwd gridWidth18" rowspan="2">
								<liferay-ui:message key="Service Request"/>
							<div class="tooltip">ServiceRequest</div>
						</th>
							
							<th class="wrapper gridWidth18" rowspan="2">
							<liferay-ui:message key="SR..."/>
							<div class="tooltip">ServiceRequest</div>
						 </th>		
												
						<th class="gridWidth18" colspan="2">
								<liferay-ui:message key="Customer Initiated Change"/>
						</th>
						<th class="gridWidth18" colspan="2">
								<liferay-ui:message key="telkomtelstra Initiated Change"/>
						</th>
					</tr>
					
				
					
					<tr>
						<th class="rwd"><liferay-ui:message key="Submitted"/>-<liferay-ui:message key="pending approval"/></th>
						<th class="wrapper">
							<liferay-ui:message key="Submit.."/>
							<div class="tooltip">Submitted-pending approval</div>
						 </th>
						<th class="rwd" ><liferay-ui:message key="Approved"/>/<liferay-ui:message key="Acknowledged by telkomtelstra"/></th>
							<th class="wrapper">
							<liferay-ui:message key="Approv.."/>
							<div class="tooltip">Approved-Acknowledged by telkomtelstra</div>
						 </th>	
						<th class="rwd" ><liferay-ui:message key="Submitted"/>-<liferay-ui:message key="pending Customer approval"/></th>
						<th class="wrapper">
							<liferay-ui:message key="Submit.."/>
							<div class="tooltip">Submitted-pending Customer approval </div>
						 </th>	
						<th class="rwd"><liferay-ui:message key="Approved by Customer"/>-<liferay-ui:message key="pending execution"/></th>
						<th class="wrapper">
							<liferay-ui:message key="Approv.."/>
							<div class="tooltip">Approved by Customer-pending execution</div>
						 </th>	
					</tr></thead>
				<tr>
					<td class="gridLeftCols gridLeftColstd">
							<liferay-ui:message key="Total" />
					</td>
	                <td class="gridWidth18">
						<a id="ServiceRequest" class="metricEX" aria-label="ServiceRequest"></a>
					</td>
	                <td class="gridWidth18">
	                       <a id="CustomerInitiatedPending" class="metricEX Customer" aria-label="CustPending"></a>
	                </td>
	                <td class="gridWidth18">
	                       <a id="CustomerInitiatedApproved" class="metricEX Customer" aria-label="CustApproved"></a>
	                </td>
	                <td class="gridWidth18">
	                       <a id="TTInitiatedPending" class="metricEX TT" aria-label="TTPending"></a>
					</td>
	                <td class="gridWidth18">
	                       <a id="TTInitiatedApproved" class="metricEX TT" aria-label="TTApproved"></a>
					</td>
				</tr>
			</table>
		</div>


		<div class="srlSearchBox"><form id="tt-sr-normalSearchForm">
			<input id="srListSearchTxt"  
				type="search" class="" placeholder="" aria-controls="srListingTable">
				<button type="submit" id="srListSearchBtn" class="normalSearch"></button>
				<button type="button" id="advSearchButton" data-toggle="modal"></button>
			</form>
		</div>

		<div class="modal fade" id="tt-sr-modal" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<form method="post" name="advancedajaxform" id="advancedajaxform">

							<div class="modal-header">
								<button type="button" class="close" id="closeMarkID" data-dismiss="modal">
	          						<img class="closeMark" src="<%=request.getContextPath() %>/images/Close_G_24.png"/>
								</button>
	          					<div class="modal-title logTitle"><liferay-ui:message key="ADVANCED SEARCH"/></div>
							</div>
							<div class="modal-body">

								<table id="formTable1">

									<tr class="commonFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Request Type:"/></td>
										<td class="advSearchCustomSpan4">
											<select id="requestTypeSelect" class="selectBoxLogInc" name="requestType" >
												<option value="serviceRequest"><liferay-ui:message key="Service Request"/></option>
												<option value="changeRequest"><liferay-ui:message key="Change Request"/></option>
											</select>
										</td>	
									</tr>

									<tr class="srFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Requestor:" /></td>
										<td class="advSearchCustomSpan4">
										<input type="text" id="requestor" name="requestor" />
										</td>	
									</tr>
									<tr class="srFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Status:" /></td>
										<td class="advSearchCustomSpan4">
											<select  id="status" class="selectBoxLogInc" name="status" >
												<c:forEach var="item" items="${statusList}">
													<option value="${item.key}">${item.value}</option>
												</c:forEach>
											</select>
										</td>	
									</tr>

									<tr class="crFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Priority:" /></td>
										<td class="advSearchCustomSpan10">
											<input type="checkbox" class="checkBoxPriority" name="priority" value="All">
											<label class="checkbox-label"><liferay-ui:message key="All" /></label>
											
											<input type="checkbox" class="checkBoxPriority" name="priority" value="P1">
											<label class="checkbox-label"><liferay-ui:message key="P1" /></label>
											
											<input type="checkbox" class="checkBoxPriority" name="priority" value="P2"> 
											<label class="checkbox-label"><liferay-ui:message key="P2" /></label>
											 
											<input type="checkbox" class="checkBoxPriority" name="priority" value="P3"> 
											<label class="checkbox-label"><liferay-ui:message key="P3" /></label>
											 
											<input type="checkbox" class="checkBoxPriority" name="priority" value="P4"> 
											<label class="checkbox-label"><liferay-ui:message key="P4" /></label>											
										</td>	
									</tr>

									<tr class="crFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Site Name:" /></td>
										<td class="advSearchCustomSpan4">
										<select id="siteID" class="selectBoxLogInc" name="siteName" onchange="getAssociatedService(this)">
												<c:forEach var="item" items="${sitenamelist}">
													<option value="${item.key}">${item.value}</option>
												</c:forEach>
										</select></td>

									</tr>
									<tr class="crFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Service Name:" /></td>
										<td class="advSearchCustomSpan4">
											<select  id="serviceName"  class="selectBoxLogInc" name="serviceName">
													<option value="allServices">All Services</option>
												<c:forEach var="item" items="${servicenamelist}">
													<option value="${item.key}">${item.value}</option>
												</c:forEach>
											</select>
										</td>	
									</tr>
									<tr class="crFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Assigned to:" /></td>
										<td class="advSearchCustomSpan4">
										<input type="text" id="assignedTo" name="assignedTo" /></td>
									</tr>
									<tr class="crFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Change Type:" /></td>
										<td class="advSearchCustomSpan4">
											<select  id="changeType"  class="selectBoxLogInc" name="changeType">
												<c:forEach var="item" items="${changetypelist}">
													<option value="${item.key}">${item.value}</option>
												</c:forEach>
											</select>
										</td>	
									</tr>
									<tr class="crFilters">
										<td class="advSearchCustomSpan2"><liferay-ui:message key="Change Source:" /></td>
										<td class="advSearchCustomSpan4">
											<select  id="changeSource"  class="selectBoxLogInc" name="changeSource">
												<c:forEach var="item" items="${changesourcelist}">
													<option value="${item.key}">${item.value}</option>
												</c:forEach>
											</select>
										</td>	
									</tr>
								</table>
								<div class="commonFilters">

									<div class="row-fluid span12" id="advSearchRow commonFilters">
										<div class="span6">
											<table id="formTable1">
												<td class="advSearchCustomSpan2"><liferay-ui:message key="Date from:" /></td>
												<td class="advSearchCustomSpan4">
													<input class="datepicker" type="text" name="dateFrom" />
												</td>
											</table>
										
										</div>
										<div class="span6">
											<table  id="formTable1">
												<td class="advSearchCustomSpan2"><liferay-ui:message key="Date to:" /></td>
												<td class="advSearchCustomSpan4">
													<input class="datepicker" type="text" name="dateTo" />
												</td>
											</table>
										</div>
									</div>

								</div>


							</div>

							<div class="modal-footer commonFilters">
								<button type="button" data-dismiss="modal" class="cancelRequestButton">
									<div class="cancelButtonContent">
										<div class="buttonIcon">
		        							<img src="<%=request.getContextPath()%>/images/Cancel_W_16.png"/>
										</div>
										<div class="buttonText">
											<liferay-ui:message key="Cancel" />
										</div>
									</div>
								</button>
								<button type="submit" class="submitRequestButton">
									<div class="submitButtonContent">
										<div class="buttonIcon">
						        			<img src="<%=request.getContextPath()%>/images/Search_W_16.png"/>
										</div>
										<div class="buttonText">
											<liferay-ui:message key="Search" />
										</div>
									</div>
								</button>
							</div>

						</form>

					</div>

				</div>
			</div>
		</div>

		<div class="ttTableBox">
			<table id="filters" class="tt-filter-text">
				<tr>
					<td id="filterscol1"><liferay-ui:message key="Filters:"/></td>
					<td id="filterscol2"></td>
				</tr>
			</table>

			<table id="srListingTable" class="display" cellspacing="0"	width="100%">
				<thead>
					<tr>
						<th id="tt-sr-adj-reqId" class="tt-sr-adjCol-reqId"><liferay-ui:message key="Request Id" /></th>						
						<th id="tt-sr-adj-subject" class="tt-sr-adjCol-subject"><liferay-ui:message key="Subject" /></th>
						<th id="tt-sr-adj-requestType" class="tt-sr-adjCol-requestType"><liferay-ui:message key="Request Type" /></th>
						<th id="tt-sr-adj-lastUpdate" class="tt-sr-adjCol-lastUpdate"><liferay-ui:message key="Last Update" /></th>
						<th id="tt-sr-adj-requestor" class="tt-sr-adjCol-requestor"><liferay-ui:message key="Requestor" /></th>
						<th id="tt-sr-adj-status" class="tt-sr-adjCol-status"><liferay-ui:message key="Status" /></th>
					</tr>
				</thead>
			</table>
		</div>

	</div>
</div>


<!-- CODE for Service Request Details starts here-->

<!-- <script type="text/javascript">
	
	
	$(function() {

		
	});
</script> -->

<div id="tt-srd-container">
	<div class="tt-grayContainer">
		<!--Header Start-->
		<div class="tt-SRHeader srColumn">
			<span class="span12"> <liferay-ui:message key="Service Request Details" /></span>
		</div>

		<div class="tt-SRHeader crColumn">
			<span class="span12"> <liferay-ui:message key="Change Request Details" /></span>
		</div>

		<!--Header End-->
		<!--Back Button Start-->
		<div class="tt-buttonHolder">
			<button type="button" id="tt-toggle-srdetails"
				class="img-back-arrow btn-primary-tt" id="backButtonID">
				<liferay-ui:message key="Back to Request List" />
			</button>
		</div>
		<!--Back Button End-->
		<div class="tt-srdeatils-container-holder">
			<div class="tt-srdetails-container">
				<div class=" tt-srdetails-header span12">
					<span class="tt-srdetails-summary" id="srdetails-summary"></span>
				</div>
				
 				<!-- Main Details of Change Request and Service Request -->
				<div class="tt-lesssrcrdetails-container">
					<table class="table tt-table-overflow">
						<tbody>

							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle srColumn">
										<liferay-ui:message key="Service Request Id:" />
									</div>
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Change Request Id:" />
									</div>
									<div id="tt-srd-srID" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Status:" />
									</div>
									<div id="tt-srd-status" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Requestor:" />
									</div>
									<div id="tt-srd-requestor" class="tt-srd-dataStyle"></div>
								</td>
							</tr>

							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Last Updated On:" />
									</div>
									<div id="tt-srd-lastUpdated" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Request Created On:" />
									</div>
									<div id="tt-srd-createdOn" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Requested By Date:" />
									</div>
									<div id="tt-srd-requestedByDate" class="tt-srd-dataStyle"></div>
								</td>
							</tr>

							<tr>
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Start Date:" />
									</div>
									<div id="tt-srd-startDate" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Expected End Date:" />
									</div>
									<div id="tt-srd-endDate" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Request Type:" />
									</div>
									<div id="tt-srd-requestType" class="tt-srd-dataStyle"></div>
								</td>
							</tr>

							<tr>
								<td class="TTData" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="telkomtelstra Approver:" />
									</div>
									<div id="tt-srd-telkomtelstraapprover" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<!--  new added -->
								<td class="CustomerData" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Customer Approver:" />
									</div>
									<div id="tt-srd-customerapprover" class="tt-srd-dataStyle crColumn"></div>
								</td>
								
								<!--  new added -->
								<td class="TTData" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="telkomtelstra Approval Date:" />
									</div>
									<div id="tt-srd-telkomtelstraapproverdate" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<!--  new added -->
								<td class="CustomerData" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn ">
										<liferay-ui:message key="Customer Approval Date:" />
									</div>
									<div id="tt-srd-customerapproverdate" class="tt-srd-dataStyle crColumn"></div>
								</td>
								
								<!--  new added -->
								<td class="TTData" width="31%">
									<div class="tt-srd-dataHeaderStyle crColumn ">
										<liferay-ui:message key="telkomtelstra Approver Group" />
									</div>
									<div id="tt-srd-telkomtelstraapprovergroup" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="CustomerData" width="31%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Change Type:" />
									</div>
									<div id="tt-srd-changeTypeCustomer" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>
							
							<tr>
								<td class="TTData" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Change Type:" />
									</div>
									<div id="tt-srd-changeTypeTT" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>

							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Site Name:" />
									</div>
									<div id="tt-srd-siteName" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Category:" />
									</div>
									<div id="tt-srd-category" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<div class="span12">
											<button type="button" class="labelMore" id="moreLabel" onclick="hiddenToggle()">
												<div id="more-button-content">
												</div>
											</button>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="" colspan="3">
									<div class="tt-srd-sri-dataHeader srColumn">
										<span class="tt-srd-dataHeaderStyle tt-srdsri-HeaderStyle">
											<liferay-ui:message key="Service Request Items:" />
										</span>
									</div>
									<div class="sritmTableBox srColumn">
										<table id="sriListingTable" class="display" cellspacing="0"	width="100%">
											<thead>
												<tr>
													<th id="tt-sr-itemid-th" class="tt-sr-adjCol-itemid-th"><liferay-ui:message key="Item Id" /></th>
													<th id="tt-sr-category-th" class="tt-sr-adjCol-category-th"><liferay-ui:message key="Category" /></th>
													<th id="tt-sr-request-th" class="tt-sr-adjCol-request-th"><liferay-ui:message key="Request" /></th>
													<th id="tt-sr-type-th" class="tt-sr-adjCol-type-th"><liferay-ui:message key="Type" /></th>
													<th id="tt-sr-status-th" class="tt-sr-adjCol-status-th"><liferay-ui:message key="Status" /></th>
													<th id="tt-sr-lastupdatedon-th" class="tt-sr-adjCol-lastupdatedon-th"><liferay-ui:message key="Last Updated On" /></th>
													<th id="tt-sr-link-th"></th>
												</tr>
											</thead>
										</table>
									</div>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				
 				<!-- More Details Div -->
				<div class="tt-morecrdetails-container">
					<table class="table tt-table-overflow-ExtraCrDetails">
						<tbody>
							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Risk:" />
									</div>
									<div id="tt-srd-risk" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Priority:" />
									</div>
									<div id="tt-srd-priority" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Impact:" />
									</div>
									<div id="tt-srd-impact" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>

							<tr>
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Case Manager:" />
									</div>
									<div id="tt-srd-caseManager" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Resolver Group:" />
									</div>
									<div id="tt-srd-resolverGroup" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Change Source:" />
									</div>
									<div id="tt-srd-changeSource" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>

							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Outage Present:" />
									</div>
									<div id="tt-srd-outagePresent" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Outage Start Time:" />
									</div>
									<div id="tt-srd-outageStartTime" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Outage End Time:" />
									</div>
									<div id="tt-srd-outageEndTime" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>

							<tr>
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Change Request Devices:" />
									</div>
									<div id="tt-srd-deviceName" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Change Request Services:" />
									</div>
									<div id="tt-srd-serviceName" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td width="31%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Outcome Status:" />
									</div>
									<div id="tt-srd-outcomestatus" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				
 				<!-- Description Comments and comments Textbox -->
				<div class="tt-constantcrdetails-container crColumn">
					<table class="table tt-table-overflow-constantCrDetails">
						<tbody>
							<tr>
								<td class="" colspan="3" width="50%">
									<div class="span12 tt-srd-dataHeaderStyle crColumn">
										<liferay-ui:message key="Description:" />
									</div>
								<div id="tt-srd-description" class="tt-srd-dataStyle tt-srd-descriptionStyle"></div>
								</td>
								<td class="">
								 	<div class="tt-srd-dataHeaderStyle tt-sritem-commentHeaderStyle crColumn">
										<liferay-ui:message key="Comments:" />
									</div>
									<div class="tt-sritem-table-holder">
										<table class="table tt-sritem-table" id="sritem-table">
											<tr>
												<td rowspan="11" id="tt-sri-commentTable-td">
												   <text id="tt-srd-comments" class="tt-srd-dataStyle crColumn"></text>
												</td>
											</tr>
										</table>
									</div> 
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="ritmModal" data-backdrop="static"
	data-keyboard="false">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">
			<img class="closeMark"
				src="<%= request.getContextPath()%>/images/Close_G_24.png" />
		</button>
		<div id="tt-ritm-header">
			<liferay-ui:message key="ITEM DETAILS" />
		</div>
	</div>
	<div class="modal-body">
		<table class="table tt-table-overflow">
			<tr>
				<td>
					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Item Id:" />
					</div>
					<div id="tt-srd-ritmid" class="tt-srd-dataStyle"></div>

					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Service Name:" />
					</div>
					<div id="tt-srd-serviceName-item" class="tt-srd-dataStyle"></div>

					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Site Name:" />
					</div>
					<div id="tt-srd-siteName-item" class="tt-srd-dataStyle"></div>
				</td>
				<td>
					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Device Name:" />
					</div>
					<div id="tt-srd-deviceName-item" class="tt-srd-dataStyle tt-srd-sri-deviceNameStyle"></div>
				</td>
			</tr>
		</table>

		<div class="tt-srd-dataHeaderStyle tt-sritem-commentHeaderStyle">
			<liferay-ui:message key="Comments:" />
		</div>
		<div class="tt-sritem-table-holder">
			<table class="table tt-sritem-table" id="sritem-table">
				<tr>
					<td rowspan="11" id="tt-sri-commentTable-td-item">
						<%-- <table style="margin: 3px; width: 99%">
	                    	<tr>
                        		<td rowspan="2" style="width: 45px;">
	                        		<div>
                                		<img src="<%=request.getContextPath()%>/images/tickets/u33.png" height="40" width="40"></img>
                                	</div>
    	                       	</td>
                           		<td>
		                           	<p style="float: left; color: red;">${note.name}</p>
                            		<p style="float: right">${note.time}</p>
                           		</td>
                        	</tr>	                        
                        	<tr>
	                        	<td>${note.note}</td>
                        	</tr>
						</table> --%>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="img-OK btn-primary-tt" id="OKButtonID"
			data-dismiss="modal">
			<liferay-ui:message key="OK" />
		</button>
	</div>
</div>
</body>
</html>

<script>
var isClicked=0;
	$(document).ready(function() {
		
		$(".tt-morecrdetails-container").hide();

		$("#more-button-content").html('<span style="padding:5px;"><liferay-ui:message key="More Details"/></span><img height="30px" src="<%=request.getContextPath()%>/images/more.png"/>');

		$("#customerDetails").hide();
		 $("#moreLabel").click(function(){
		        $(".tt-morecrdetails-container").slideToggle();
		    });
	});

	$("#tt-toggle-srdetails").click(function(){
		isClicked=0;
		$("#more-button-content").html('<span style="padding:5px;"><liferay-ui:message key="More Details"/></span><img height="30px" src="<%=request.getContextPath()%>/images/more.png"/>');
	});
	
	function hiddenToggle() {

		$("#customerDetails").slideToggle();
		isClicked=isClicked+1;
		if((isClicked%2)!=0){
		$("#more-button-content").html('<span style="padding:5px;"><liferay-ui:message key="Less Details"/></span><img height="30px" src="<%=request.getContextPath()%>/images/less.png"/>');
		}
		else{
		$("#more-button-content").html('<span style="padding:5px;"><liferay-ui:message key="More Details"/></span><img height="30px" src="<%=request.getContextPath()%>/images/more.png"/>');
		}
	}
	
	function putFilterBox(obj)
	{
		appliedFilters = "";
		var labelOfClickedCell = obj.getAttribute("aria-label");
		//alert("labelOfClickedCell--"+labelOfClickedCell);
		
		var finalFilterHTML = '';
		//var idOfClickedCell = obj.getAttribute("id");
		
		var classFilters = obj.getAttribute("class");
		var requiredClass = classFilters.substring(9);
		//alert("requiredClass--" + requiredClass);
		
		if(labelOfClickedCell == "ServiceRequest")
		{
			finalFilterHTML = '<a href="javascript:void(0);" style="display:inline-flex;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc"'
				+ ' id="ServiceRequestFilter"'
				+ ' class="'+labelOfClickedCell+'" onclick="updateGridUsingFilter(this)"><liferay-ui:message key="Service Request"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
			
			// appliedFilters is a global variable to know the number of filters applied
			appliedFilters += labelOfClickedCell;
			//alert("appliedFilters ::" + appliedFilters);
		}
		else if(requiredClass == "Customer")
		{
			var CustInitiatedFilter = '<a href="javascript:void(0);" style="display:inline-flex;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc"'
				+  'id="CustomerInitiatedFilter"'
				+ ' class="Customer" onclick="updateGridUsingFilter(this)"><liferay-ui:message key="Customer Initiated"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
			
 			finalFilterHTML = CustInitiatedFilter;
			
			appliedFilters += requiredClass + "$";
			//alert("appliedFilters ::" + appliedFilters);
			
			if(labelOfClickedCell == "CustPending" || labelOfClickedCell == "Customer Initiated")
			{
				var PendingFilter = '<a href="javascript:void(0);" style="display:inline-flex;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc"'
				+ ' id="CustomerInitiatedPendingFilter"'
				+ ' class="Submitted" onclick="updateGridUsingFilter(this)"><liferay-ui:message key="Submitted"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"/></a>&nbsp;';	
			
				finalFilterHTML += PendingFilter;
				
				appliedFilters += "Submitted" + "$";
				//alert("appliedFilters ::" + appliedFilters);
			}
			if(labelOfClickedCell == "CustApproved" || labelOfClickedCell == "Customer Initiated")
			{
				var approvedFilter = '<a href="javascript:void(0);" style="display:inline-flex;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc"'
					+ ' id="CustomerInitiatedApprovedFilter"'
					+ ' class="Approved" onclick="updateGridUsingFilter(this)"><liferay-ui:message key="Approved"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"/></a>&nbsp;';
				
				finalFilterHTML += approvedFilter;
					
				appliedFilters += "Approved" + "$";
				//alert("appliedFilters ::" + appliedFilters);
			}
		}
		else if(requiredClass == "TT")
		{
			var TTInitiatedFilter = '<a href="javascript:void(0);"   style="display:inline-flex;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc"'
				+ ' id="OnlyTTInitiatedFilter"'
				+ ' class="TT" onclick="updateGridUsingFilter(this)"><liferay-ui:message key="telkomtelstra Initiated"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
			
 			finalFilterHTML = TTInitiatedFilter;
			
			appliedFilters += requiredClass + "$";
			//alert("appliedFilters ::" + appliedFilters);
			
			if(labelOfClickedCell == "TTPending" || labelOfClickedCell == "telkomtelstra Initiated")
			{
				var ttPendingFilter = '<a href="javascript:void(0);" style="display:inline-flex;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc"'
					+ ' id="TTInitiatedPendingFilter"'
					+ ' class="Submitted" onclick="updateGridUsingFilter(this)"><liferay-ui:message key="Submitted"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"/></a>&nbsp;';
				finalFilterHTML += ttPendingFilter;
				
				appliedFilters += "Submitted" + "$";
				//alert("appliedFilters ::" + appliedFilters);
			}
			if(labelOfClickedCell == "TTApproved" || labelOfClickedCell == "telkomtelstra Initiated")
			{
				var ttApprovedFilter = '<a href="javascript:void(0);" style="display:inline-flex;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc"'
					+ ' id="TTInitiatedApprovedFilter"'
					+ ' class="Approved" onclick="updateGridUsingFilter(this)"><liferay-ui:message key="Approved"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"/></a>&nbsp;';
				finalFilterHTML += ttApprovedFilter;
			
				appliedFilters += "Approved" + "$";
				//alert("appliedFilters ::" + appliedFilters);
			}
		}
		else
		{
			finalFilterHTML = "No Filters Applied";
		}

		$('#filterscol2').html('');
		$('#filterscol2').append('<div id="filterdiv" style="display:inline;">' + finalFilterHTML + '</div>');
	}

	function updateGridUsingFilter(obj)
	{
		var filterId = obj.getAttribute("id");
		//alert("id of the Filter cancelled ::  " + filterId);
		
		var removedFilter = obj.getAttribute("class");
		var finalId = "#" + filterId;
		$(finalId).remove();
		
		//alert("applied filters in here :: " + appliedFilters);
		//alert("removedFilterClass " + removedFilter);
		
		appliedFilters = remainingFiltersAfterRemoval(appliedFilters , removedFilter);
		
		//call the controller passing the required filters
		$("#advancedajaxform")[0].reset();
		
		//append the search term and filter the results 
		var srListURL = "${getSRListURI}";
		var srListURLSearch = srListURL + "&isFilterSearch=true" + "&appliedFilters=" + appliedFilters + "&maxResults=" + maxResults + "&tabSelected=" + tabSelected;
		ServiceRequestPortlet_getSearchedSRList(srListURLSearch);
	}
	
	function remainingFiltersAfterRemoval(appliedFilters , removedFilter)
	{		
		var appliedFiltersArray = appliedFilters.split("$");
		//alert("appliedFiltersArray  " + appliedFiltersArray);
		//alert("length of Remaining Filters ::" + appliedFiltersArray.length);
		
		var remainingFilters = "";
		for(i=0; i<appliedFiltersArray.length; i++)
		{
			var filter = appliedFiltersArray[i];
			//alert("filter in array @@@@@@@" + filter.trim());
			//alert("removedFilter :"+removedFilter.trim());
			
			if(filter.trim() != removedFilter.trim() && filter.trim() != "")
			{
				remainingFilters += filter + "$";
			}
		}
		//alert("remainingFilters %%%%%%%%%" + remainingFilters);
		
		if(remainingFilters == "")
		{
			$('#filterscol2').html('');
			$('#filterscol2').append('<liferay-ui:message key="No filters applied" />');
		}
		return remainingFilters;
	}

</script>

<!-- CODE for Service Request Details Ends here -->
