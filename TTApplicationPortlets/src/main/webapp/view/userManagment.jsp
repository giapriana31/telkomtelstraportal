<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:resourceURL id="userListPopulation" var="userListPopulation" />
<portlet:resourceURL id="disableUser" var="disableUser" />
<portlet:resourceURL id="enableUser" var="enableUser" />
<portlet:resourceURL id="addUser" var="addUser" />
<portlet:resourceURL id="renderAcquireLicense" var="renderAcquireLicense" />
<portlet:resourceURL id="addLicense" var="addLicense"></portlet:resourceURL>
<portlet:resourceURL id="editUser" var="editUser" />
<portlet:resourceURL id="updateUser" var="updateUser" />
<portlet:resourceURL id="editMobile" var="editMobile" />
<portlet:resourceURL id="getLanguage" var="getLanguage" />

<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
       type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js"
       type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
       type="text/javascript"></script>
       <script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/usermanagement.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/PhoneFormat.js" type="text/javascript"></script>
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/jquery.dataTables.css" />
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/usermanagment.css" />
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/profilesetting.css" />
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/ttlogticket.css" />


<style type="text/css">
              
              @media (max-width: 500px){
              table.dataTable thead th, table.dataTable thead td{
              padding: 10px 0px !important;
              }
              
              #settingsHeader{
              margin-top: 2%;
              }
              #disableUserButton {
              left: 25% !important;
              margin-top: 2%;
       }
       div#userListTable_wrapper {
           padding-right: 1%;
           padding-left: 1%;
              }
              #userheader{
           margin-right: 3px;
           margin-left: 3px;
           font-size: 14px;
              }
              .tablehomecontact{
              display:none !important;
              }
              .tableemail{
              display:none !important;
              }
              #logServReqButton  {
              position: relative;
              left: 1% !important;
              background-color: #e32212;
              border: 0;
              color: #fff;
              -moz-box-shadow: 2px 2px 2px #333;
              margin-bottom: 2%;
       }
       
       #buttons {
              margin-top: 2% ;
              margin-right: 0px !important;
       }
       .addUserButton {
              position: relative;
              left: 3% !important;
       }

       .enableUserButton {
              position: relative;
              left: 13% !important;
              
       }
}

@media (min-width: 501px) and (max-width: 767px){
              table.dataTable thead th, table.dataTable thead td {
           padding: 8px 8px !important;
              }
              #settingsHeader{
              margin-top: 2% !important;
              }
              .maincontainer{
              padding-left: 0px !important;
           padding-right: 0px !important;
              }
       /* #userListTable td{
              word-break: break-word;
              } */
              #logServReqButton {
              position: relative;
              left: 2% !important;
       }
       .addUserButton   {
              position: relative;
              left: 4% !important;
              
       }
       .enableUserButton {
              position: relative;
              left: 12% !important;
       }
       #disableUserButton {
              position: relative;
              left: 20% !important;
       }
}
              @media (min-width: 768px){
              .maincontainer{
              padding-left: 0px !important;
           padding-right: 0px !important;
              }
              }
              
              tbody {
              text-align: center;
              }
       #responseStatus {
              color: #333 !important;
              font-size: 16px !important;
              margin-top: 8px;
              margin-left: 15px;
       }
       .chrome, .ie,.firefox, .safari {
              .tt-modal-body{
                     padding:15px !important;
                     max-height: 82vh !important;
              }
       }
       #MessageString {
              margin-left: 15px;
       }
       .modal-footer {
              margin-bottom: -22px !important;
              background-color: #ffffff !important;
       }
       #responseMessage .modal-footer {
              margin-bottom: 0px !important;
       }
       .cancelButton, .closeButton {
              background: #777;
              border-top-style: none;
              border-bottom-style: none;
              border-right-style: none;
              border-left-style: none;
              height: 36px;
              color: #ffffff !important;
              width: auto;
              min-width: 100px;
              border-radius: 3px;
              padding-left: 15px;
              padding-right: 15px;
              font-family: 'Gotham-Rounded' !important;
       }
       .buttonIcon {
              position: relative !important;
              float: left;
              /* top: -2px; */
       }
       .buttonText {
              position: relative !important;
              color: #fff;
              font-size: 14px;
              display: inline;
              float: left;
              padding-left: 10px;
       }
       #disableUserButton{
       position: relative;
         left: 40%;
       background-color: #555;
       /* padding: 7px 7px 7px 10px; */
         border: 0;
         color: #fff;
         line-height: 1!important;
         box-shadow: 2px 2px 2px #333;
         -moz-box-shadow: 2px 2px 2px #333;
         -webkit-box-shadow: 2px 2px 2px #333;
       }
       #userListTable{
              border-left: 1px solid rgb(219, 218, 218);
           border-right: 1px solid rgb(219, 218, 218);
           border-top: 1px solid rgb(219, 218, 218);
           border-bottom: 1px solid rgb(219, 218, 218);
              
       }
       .current:hover {
              background: #e32212 !important;
       }
       
       
       .paginate_button.current, .paginate_button.current:hover {
              border-bottom: 2px #e32212;
       }
       #userListTable {
           border: solid 1px rgb(219, 219, 218);
           background: #fff;
       }
       
       #userListTable td {
           border-bottom: solid 1px #ccc;
           height: 30px;
           font-weight: 600;
       }
       #userListTable th {
           background: rgb(219,219,218) !important;
       }
       #userListTable_paginate {
           padding-right: 1%;
           padding-top: 2%;
       }
       .paginate_button:hover {
           border-radius: 5px;
       }
       
       #DisableIcon{
       
           height: 15px;
           width: 15px;
           margin-left: 15px;
           position: relative;
           top: 9px;
              
       }
       #DisableContent{
              color: #fff;
           margin-left: 40px;
           margin-bottom: 0;
           position: relative;
           bottom: 8px;
           padding-right: 15px;
           font-size: 14px;
       }
       #userheader{
              background-color:#555;
              color:#fff;
              font-size:16px;
              height:30px;
       }
       
       #registeredusers{
              color:#fff;   
              padding:4px;
       }
       .deacFail {
              color: #E32212;
       }
       .deacSucc {
              color: #63892A;
       }
       
.enableButton{
       position: relative;
       left: 40%;
       background-color: #555;
       /* padding: 7px 7px 7px 10px; */
       border: 0;
       color: #fff;
       line-height: 1!important;
       box-shadow: 2px 2px 2px #333;
       -moz-box-shadow: 2px 2px 2px #333;
       -webkit-box-shadow: 2px 2px 2px #333;
}
.disableButton{
       position: relative;
       left: 40%;
       background-color: #A7A7A7;
       /* padding: 7px 7px 7px 10px; */
       border: 0;
       color: #fff;
       line-height: 1!important;
       box-shadow: 2px 2px 2px #333;
       -moz-box-shadow: 2px 2px 2px #333;
       -webkit-box-shadow: 2px 2px 2px #333;
}

div#p_p_id_1_WAR_kaleodesignerportlet_ {
    min-height: 148px;
}
@font-face {
       font-family:'Gotham-Rounded';
       src: url("../fonts/GothamRounded-Book.woff");
       font-style: normal;
       font-weight: 400;

}
select,input,textarea{
       font-family: "Gotham-Rounded" !important;
       font-size: 14px !important;
}
.manageuser-input{
       margin-bottom:0px !important;
       padding:4px;
              text-indent: 5px;
              width:200px;
              font-size:10px;
}
.manageuser-input.watermark { color: #999; }
       
select{
       margin-bottom:0px !important;
}
/* .partfix {border:2px #ccc inset;width:20em;padding:0;margin:5px;}
.partfix textarea {resize:none;overflow:hidden; border:none;width:22em;margin:0} */
.closeMark{
       position:relative;
       margin-top: 4px;
}
.close{
       opacity:1 !important;
}
.disableButton,
.disableButton:hover{
       background-color:#ccc !important;
       cursor:pointer;
}
.disableButton2{
       background:#fff !important;
       cursor:pointer !important;
}
.dashTickRow{
       padding-bottom:10px !important;
}
.dashTickCheckBox,.checkBoxTextLogInc{
       display:inline !important;
}
.descriptionTextArea,#customDescription{
       resize:none;
       width: 90% !important;
}
.textBoxLogSR{
       width:90% !important;
}
.tt-radio-btn{
       margin-top: 0px !important;
}
.tt-radio-btn-sec{
       margin-left:20px;
} 
.img-create-servicerequest{
       background: url(../images/PostNewThread_W_16.png) no-repeat 10px 10px #E32212; 
       box-shadow: 2px 2px 2px #B52217;
       background-color: #E32212;
       padding: 10px 10px 10px 35px;
       border: 0;
       color: #ffffff;
}

.img-create-servicerequest:active, .img-create-servicerequest:hover, .img-create-servicerequest:visited, .img-create-servicerequest:focus {
       background-color: #B52217 !important;
       color: #ffffff;
       font-size: 14px; 
       outline:0;
}

.img-cancel{
       background-repeat:no-repeat;
       background-position:10px 10px;
       padding: 10px 10px 10px 35px;
       border: 0;
       color: #ffffff;
}
.img-close{
       background-image: url(../images/Close_W_16.png);
       background-repeat:no-repeat;
       background-position:10px 10px;
       /* background-color: #777; */
       padding: 10px 10px 10px 35px;
       border: 0;
       color: #ffffff;
}
.img-submit-servicerequest{
       
       background-repeat:no-repeat;
       background-color:#555;
       background-position:10px 10px;
       padding: 10px 10px 10px 35px;
       border: 0;
       color: #ffffff;
       background-color: #CCC !important; 
       color: white !important;
}
.img-submit-servicerequestenabled{
      
       background-repeat:no-repeat;
       background-color:#555;
       background-position:10px 10px;
       padding: 10px 10px 10px 35px;
       border: 0;
       color: #ffffff;
       color: white !important;
}
/* .img-submit-servicerequest:hover{
       background-color:#555;
} */
.error{
       color:#e32212 !important;
}
/*CSS for Modal Start  */
#logSRModal .modal-footer{
       margin-bottom: -22px !important;
       background-color: #ffffff !important;
}
#logSRModal .modal-header{
       border-bottom: 1px solid #CCC !important;
       height: 35px;
       padding: 2px 15px 2px 0px !important;
}
.logTitle {
    color: #333 !important;
    font-size: 16px !important;
    margin-top: 10px;
    margin-left: 18px;
}

.ui-autocomplete { 
            cursor:pointer; 
            height:auto;
                     max-height:150px;
            overflow-y:hidden;
                     background-color:#fff !important;
                     border-left:1px solid #ccc !important;
                     border-right:1px solid #ccc !important;
                     border-bottom:1px solid #ccc !important;
                     width:220px !important;
                     font-family: 'Gotham-Rounded';
                     src: url("../fonts/GothamRounded-Book.woff");
                     font-style: normal;
                     font-weight: 400;
                     
        }    
        .ui-autocomplete .ui-state-focus{
        background-color:#fff !important;
        font-style: normal !important;
                     font-weight: 400 !important;
        }





#logSRModal{
       position:absolute !important;
       width: 600px !important;
}
#logSRModal .modal-header{
       border-bottom: 1px solid #CCC !important;
       height: 35px;
       padding: 2px 15px 2px 0px !important;
}
/* #logSRModal .modal-body{
       padding-left: 0px !important;
       max-height:1000px !important;
} */
.modal-backdrop{background-color:#CCC !important;}
.modal-backdrop.fade{filter:alpha(opacity=0);opacity:0}
.modal-backdrop.in{filter:alpha(opacity=50);opacity:0.4 !important;}

#logSRSubmitResponse{
       z-index:-9;
       position:relative;
       width: 400px !important;
       /* top: 40% !important; */
       left:55% !important;
}
#logSRSubmitResponse .modal-footer{
       margin-bottom: 0px !important;
       padding: 10px 10px 10px !important;
       background-color: #fff;
}
#responseStatus{
       color:#333 !important;
       font-size:16px !important;
       margin-top: 8px;
       margin-left:15px;
}
.modal-open{
       overflow: hidden;
}
/*CSS for Modal End  */



.tt-submit-row{
       text-align: right;
}
.tt-form-error-row{
       margin-bottom: 20px;
}
.tt-form-row{
       margin-bottom: 15px;
}
.tt-form-last-row{
       margin-bottom: 10px;
}
.tt-form-container{
       padding-left:0 !important;
       padding-right:0 !important;
}
.tt-modal-form{
       margin-bottom: 0 ! important;
}

.tt-checkbox-optional{
       display:block;
       padding-left: 10px;
}

.tt-warning{
       display: inline-block;
       margin-top: 10px;
       position: relative;
       top: 0px;
}

.aui{
       
       .modal.fade.tt-SR-modal{
              
              display:none !important;
              >div{
                     >div{
                           >div{
                                  >.tt-modal-form{
                                         display: none;
                                  }
                           }
                     }
              }
       }
       
       .modal.fade.tt-SR-modal.in{
              
              display:table-cell !important;
              >div{
                     >div{
                           >div{
                                  >.tt-modal-form{
                                         display: block;
                                 }
                           }
                     }
              }
       }
}

@media (min-width: 1200px) {
       .tt-form-left-col{
              width: 14% ;
       }
       
       #logSRModal{
              position: fixed;
              /*width: 50% !important;*/
              overflow: auto;
              display: table-cell;
              vertical-align: middle;
              top: 2% !important;
              /*left: 45%;*/
              right: 0 !important;
              /* z-index: 1050 !important; */
       }
       
       #logSRModal{
              left: 46%;
              width: 50% !important;
       }
       #logServReqButton {
       position: relative;
       left: 83%;
       }
}

@media (min-width: 1025px) and (max-width: 1199px) {
       #logSRModal{
              left: 46% ! important;
              width: 60% !important;
              top: 2% !important;
       }
       #logServReqButton {
       position: relative;
       left: 83%;
       }
}

@media (min-width: 930px) and (max-width: 1024px) {
       #logSRModal{
              left: 48% ! important;
              width: 70% !important;
              top: 2% !important;
       }
       
}
@media (min-width: 768px) and (max-width: 929px) {
       #logSRModal{
              left: 44% ! important;
              width: 73% !important;
              top: 2% !important;
       }
       
}

@media (min-width: 768px) and (max-width: 929px) {
       #logServReqButton {
       position: relative;
       left: 83%;
       }
       #logSRModal{
              left: 49% ! important;
              top: 2%;
       }
}

@media (max-width: 767px){
       #logSRModal{
              left: 7%;
              width: 85% !important;
              top: 2% !important;
       }
       .tt-form-row{
              margin-bottom: 5px;
       }
       .tt-form-left-col{
              width: 100%;
       }
       .tt-submit{
              padding-top: 10px;
       }
       .tt-submit-row{
              text-align: left;
       }
       #dashTickNotesTextArea{
              width: 100%;
       }
}

.chrome, .ie, .firefox, .safari{
       .tt-modal-body{
              padding:15px !important;
              max-height: 82vh !important;
       }
}

@media screen and ( min-height: 600px ) and ( max-height: 641px ) {
       .chrome, .ie, .firefox, .safari {
              #logSRModal{
                     bottom: 2% !important;
                     overflow: hidden;
              }
              .modal-body{
                     /* background-color: yellow !important; */
                     max-height: 82vh !important;
              }
       }
}

@media screen and ( min-height: 465px ) and ( max-height: 599px ) {
       .chrome, .ie, .firefox, .safari {
              #logSRModal{
                     bottom: 2% !important;
                     overflow: hidden;
              }
              .modal-body{
                     /* background-color: yellow !important; */
                     max-height: 81vh !important;
              }
       }
}

@media screen and ( min-height: 445px ) and ( max-height: 466px ) {
       .chrome, .ie, .firefox, .safari {
              #logSRModal{
                     bottom: 2% !important;
                     overflow: hidden;
              }
              .modal-body{
                     /* background-color: yellow !important; */
                     max-height: 76vh !important;
              }
       }
}

@media screen and ( min-height: 400px ) and ( max-height: 445px ) {
       .chrome, .ie, .firefox, .safari {
              #logSRModal{
                     bottom: 2% !important;
                     overflow: hidden;
              }
              .modal-body{
                     /* background-color: yellow !important; */
                     max-height: 71vh !important;
              }
       }
}

@media screen and ( min-height: 320px ) and ( max-height: 399px ) {
       .chrome, .ie, .firefox, .safari {
              #logSRModal{
                     bottom: 2% !important;
                     overflow: hidden;
              }
              .modal-body{
                     /* background-color: yellow !important; */
                     max-height: 71vh !important;
              }
       }
}

@media screen and ( min-height: 250px ) and ( max-height: 319px ) {
       .chrome, .ie, .firefox, .safari {
              #logSRModal{
                     bottom: 2% !important;
                     overflow: hidden;
              }
              .modal-body{
                     /* background-color: yellow !important; */
                     max-height: 60vh !important;
              }
       }
}



.enableUserButton {
       position: relative;
       left: 40%;
       background-color: #555;
       /* padding: 7px 7px 7px 10px; */
       border: 0;
       color: #fff;
       line-height: 1 !important;
       box-shadow: 2px 2px 2px #333;
       -moz-box-shadow: 2px 2px 2px #333;
       -webkit-box-shadow: 2px 2px 2px #333;
       margin-right: 1%;
}

.addUserButton {
       position: relative;
       left: 40%;
       background-color: #555;
       /* padding: 7px 7px 7px 10px; */
       border: 0;
       color: #fff;
       line-height: 1 !important;
       box-shadow: 2px 2px 2px #333;
       -moz-box-shadow: 2px 2px 2px #333;
       -webkit-box-shadow: 2px 2px 2px #333;
           margin-right: 1%;
}

#EnableIcon {
   
       height: 15px;
       width: 15px;
       margin-left: 15px;
       position: relative;
       top: 9px;
}

#EnableContent {
       color: #fff;
       margin-left: 40px;
       margin-bottom: 0;
       position: relative;
       bottom: 8px;
       padding-right: 15px;
       font-size: 14px;
}

#AddLicense {
       color: #fff;
       margin-left: 40px;
       margin-bottom: 0;
       position: relative;
       bottom: 8px;
       padding-right: 15px;
       font-size: 14px;
}

#AddLicenseIcon {
    
       height: 15px;
       width: 15px;
       margin-left: 15px;
       position: relative;
       top: 9px;
}

#logServReqButton {
       position: relative;
       left: 83%;
       background-color: #e32212;
       /* padding: 7px 7px 7px 10px; */
       border: 0;
       color: #fff;
       /* line-height: 1 !important; */
       box-shadow: 2px 2px 2px #333;
       -moz-box-shadow: 2px 2px 2px #333;
       -webkit-box-shadow: 2px 2px 2px #333;
       margin-bottom: 1%;
}

.create-service-ctr {
       width: 58% !important;
       float: right !important;
       text-align: left !important;
       padding-top: 20px ! important;
       padding-right: 12px ! important;
}

.disableadduser{
    background-repeat: no-repeat;
    background-color: #555;
    background-position: 10px 10px;
    border: 0;
    color: #ffffff;
    background-color: #CCC !important;
    color: white !important;
}

#buttons{
       margin-top: 2%;
    margin-right: 20%;
}
.aui .modal{
       border:0px !important;
}
.wrapper {
              cursor: help;
              position: relative;
         }
       
  .wrapper .tooltip {
  
  color: #fff;
    font-size: 11px;
    border-radius: 4px;
   padding: 9px;
   background: #6F6B6B ;
              bottom: 100%;
         color: #fff;
         display: block;
         left: -25px;
         margin-bottom: 15px;
         opacity: 0;
         padding: 20px;
         pointer-events: none;
         position: absolute;
         width: 100%;
         -webkit-transform: translateY(10px);
            -moz-transform: translateY(10px);
             -ms-transform: translateY(10px);
              -o-transform: translateY(10px);
                 transform: translateY(10px);
         -webkit-transition: all .25s ease-out;
            -moz-transition: all .25s ease-out;
             -ms-transition: all .25s ease-out;
              -o-transition: all .25s ease-out;
                 transition: all .25s ease-out;
         -webkit-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
            -moz-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
             -ms-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
              -o-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
                 box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
       }
       
       
       /* This bridges the gap so you can mouse into the tooltip without it disappearing */
       .wrapper .tooltip:before {
         bottom: -20px;
         content: " ";
         display: block;
         height: 20px;
         left: 0;
         position: absolute;
         width: 100%;
       }  
       
       /* CSS Triangles - see Trevor's post */
       .wrapper .tooltip:after {
         border-left: solid transparent 10px;
         border-right: solid transparent 10px;
         border-top: solid #D4D4D4 10px;
         bottom: -10px;
         content: " ";
         height: 0;
         left: 50%;
         margin-left: -13px;
         position: absolute;
         width: 0;
       }
         
       .wrapper:hover .tooltip {
         font-size: 8px;
           color: black;
         opacity: 1;
         pointer-events: auto;
         -webkit-transform: translateY(0px);
            -moz-transform: translateY(0px);
             -ms-transform: translateY(0px);
              -o-transform: translateY(0px);
                 transform: translateY(0px);
       }

       /* IE can just show/hide with no transition */
       .lte8 .wrapper .tooltip {
         display: none;
       }
       
       .lte8 .wrapper:hover .tooltip {
         display: block;
       }
button,html input[type="button"] {
       border-radius: 5px !important;
}

/* .disableSubmitButtonID{
       background-image: url(../images/Cancel_W_16.png);
       background-repeat:no-repeat;
       background-position:10px 10px;
       padding: 10px 10px 10px 35px;
       border: 0;
       cursor: default;
    pointer-events: none;
    color: #c0c0c0;
    background-color: #ccc;
} */
              
              .wrapper {
              cursor: help;
              position: relative;
         }
       
  .wrapper .tooltip {
  
  color: #fff;
    font-size: 11px;
    border-radius: 4px;
   padding: 9px;
   background: #6F6B6B ;
              bottom: 100%;
         color: #fff;
         display: block;
         left: -25px;
         margin-bottom: 15px;
         opacity: 0;
         padding: 20px;
         pointer-events: none;
         position: absolute;
         width: 100%;
         -webkit-transform: translateY(10px);
            -moz-transform: translateY(10px);
             -ms-transform: translateY(10px);
              -o-transform: translateY(10px);
                 transform: translateY(10px);
         -webkit-transition: all .25s ease-out;
            -moz-transition: all .25s ease-out;
             -ms-transition: all .25s ease-out;
              -o-transition: all .25s ease-out;
                 transition: all .25s ease-out;
         -webkit-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
            -moz-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
             -ms-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
              -o-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
                 box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
       }
      
       
       /* This bridges the gap so you can mouse into the tooltip without it disappearing */
       .wrapper .tooltip:before {
         bottom: -20px;
         content: " ";
         display: block;
         height: 20px;
         left: 0;
         position: absolute;
         width: 100%;
       }  
       
       /* CSS Triangles - see Trevor's post */
       .wrapper .tooltip:after {
         border-left: solid transparent 10px;
         border-right: solid transparent 10px;
         border-top: solid #D4D4D4 10px;
         bottom: -10px;
         content: " ";
         height: 0;
         left: 50%;
         margin-left: -13px;
         position: absolute;
         width: 0;
       }
         
       .wrapper:hover .tooltip {
         font-size: 8px;
           color: black;
         opacity: 1;
         pointer-events: auto;
         -webkit-transform: translateY(0px);
            -moz-transform: translateY(0px);
             -ms-transform: translateY(0px);
              -o-transform: translateY(0px);
                 transform: translateY(0px);
       }

       /* IE can just show/hide with no transition */
       .lte8 .wrapper .tooltip {
         display: none;
       }
       
       .lte8 .wrapper:hover .tooltip {
         display: block;
       }
       
       input.watermark { color: #999; }
       input {
              padding:4px;
              text-indent: 5px;
              width:200px;
              font-size:10px;
       }
       
       .disableSubmitButton{
       background-image: url(../images/Cancel_W_16.png);
       background-repeat:no-repeat;
       background-position:10px 10px;
       padding: 10px 10px 10px 35px;
       border: 0;
       cursor: default;
    pointer-events: none;
    color: #c0c0c0;
    background-color: #ccc;
}
</style>

<script>
$(document).ready(function(){
       jQuery.validator.addMethod("phonecheck", function(value, element,regexp) {
        var re = processPhone();
     return processPhone();
},"<liferay-ui:message key='Invalid phone number'/>");

       jQuery.validator.addMethod("namecheck", function(value, element,regexp) {
        var re = new RegExp(regexp);
        
     return this.optional(element) || re.test(value);
},"<liferay-ui:message key='Please insert characters only'/>");

       
              $("#addUserFormModel").validate(
              {
                    
              rules : {
                     firstname:{
                required:true,
                namecheck:"^[[a-zA-Z][a-zA-Z ]+$"
          },

          lastname:{
              required:true,
              namecheck:"^[[a-zA-Z][a-zA-Z ]+$"
        },

                        username : {
                                    
                                    maxlength : 100,
                                    required: true,
                                 email: true
                        },
                        mobilenumber:{
                   required:true,
                   phonecheck:""                     
             }

                  },
                     messages : {
                       firstname : {
                             maxlength : "<liferay-ui:message key='Maximum characters allowed is '/>20",
                           	required : "<liferay-ui:message key='Please enter First Name'/>"
                        },
                       lastname : {
                          	 maxlength : "<liferay-ui:message key='Maximum characters allowed is '/>20",
                             required : "<liferay-ui:message key='Please enter Last Name'/>"
                        },
                        username : {
                             maxlength : "<liferay-ui:message key='Maximum characters allowed is '/>20"
                                  //required : "<liferay-ui:message key='Please enter User Name' />",
                        },
                       mobilenumber : {
                           
                             maxlength : "<liferay-ui:message key='Maximum characters allowed is '/>10",
                             required : "<liferay-ui:message key='Please enter Mobile Number'/>"
                            
                       },
                  }
                 
           });
});


</script>


<script type="text/javascript">

       $(document)
                     .ready(
                                  function() {
                                         var reqID;
                                         var renderAcquireLicense = "${renderAcquireLicense}";
                                         var errorClassForLabel = 'errorForLabel';
                                         
                                         
                                         jQuery.validator.addMethod('selectcheck', function(
                                                       value) {
                                                if (value != null && value != '0') {
                                                       return true;
                                                } else {
                                                       return false;
                                                }
                                                
                                         }, "<liferay-ui:message key='Please select a valid option' />");

                                         $("#acquireLicenseForm")
                                                       .validate(
                                                                     {
                                                                           rules : {
                                                                                  summary : {
                                                                                         required : true,
                                                                                         maxlength : 50
                                                                                  },
                                                                                  description : {
                                                                                         required : true,
                                                                                         maxlength : 200
                                                                                  },
                                                                                  
                                                                                  category : "required",
                                                                                  subCategory : {
                                                                                         selectcheck : true
                                                                                  }
                                                                           },
                                                                           messages : {
                                                                                  summary : {
                                                                                         required : "<liferay-ui:message key='Please enter request title of Service Request' />",
                                                                                         maxlength : "<liferay-ui:message key='Maximum characters allowed is ' />50",
                                                                                  },
                                                                                  description : {
                                                                                         required : "<liferay-ui:message key='Please enter description of Service Request' />",
                                                                                         maxlength : "<liferay-ui:message key='Maximum characters allowed is '/>200",
                                                                                  },
                                                                                  category : "<liferay-ui:message key='Please select category'/>",
                                                                           }
                                                                            
                                                              
                                                                     });
                                  });

</script>


<script>

var countActive = 0;
var countInactive = 0;
var noOfOktaUsers = "${numberOfOktaUsers}";
var siteData = new Array();
var statusActiveArr = new Array();
var statusInactiveArr = new Array();
var watermark ='Enter user&apos;s email address';
var watermarkMobile ='Include Country code';
var watermarkDesc ='Please enter number of licences or any other details.';
var remainingLicenses = 0;
var firstName = "";
var lastName = "";
var userName = "";
var mobileNum = "";
var customerPortalAdmin = "${customerPortalAdmin}";
var endCustomerAdmin = "${endCustomerAdmin}";
var nocManager = "${NOCManager}";
//var custId = "${customerId}";

       $(document)
                     .ready(
                                  function() {
                                      $('#addUserModal').modal('hide');
                                    /*   alert("customerPortalAdmin" +customerPortalAdmin);
                                      alert("endCustomerAdmin" +endCustomerAdmin);
                                      alert("nocManager" +nocManager);
                                      alert("custId" +custId); */
                                         $('#addUserModal').css('display', "none");
                                         $('#licenseModal').modal('hide');
                                         $('#licenseModal').css('display', "none");
                                         $('#editMobileNumberModal').modal('hide');
                                         $('#editMobileNumberModal').css('display', "none");
                                         $('#editUserModal').modal('hide');
                                         $('#editUserModal').css('display', "none");
                                         
                                         
                                         
                                         /* $('#addUserMoodal').modal('show');
                                         $('#addUserMoodal').modal('show'); */
                                         var userListURL = "${userListPopulation}";
                                         
                                         getUserList(userListURL);
                                         
                                         makeButtonEnable();
                                         
                                         $("#disableUserButton")
                                                       .click(
                                                                     function() {
                                                                            $("#disableUserButton").prop(
                                                                                         "disabled", true);
                                                                           makeButtonDisable();

                                                                           var userIdArray = $(
                                                                                         'table#userListTable input:checkbox')
                                                                                         .map(
                                                                                                       function() {
                                                                                                              return $(this)
                                                                                                                           .prop(
                                                                                                                                         'checked') ? $(
                                                                                                                           this)
                                                                                                                           .prop(
                                                                                                                                         'name')
                                                                                                                           : ""
                                                                                                       }).get().join();

                                                                           var userOktaIdArray = $(
                                                                                         'table#userListTable input:checkbox')
                                                                                         .map(
                                                                                                       function() {
                                                                                                              
                                                                                                              return $(this)
                                                                                                                           .prop(
                                                                                                                                         'checked') ? $(
                                                                                                                           this)
                                                                                                                           .attr(
                                                                                                                                         'oktaId')
                                                                                                                           : ""
                                                                                                       }).get().join();

                                                                           var userArray = userIdArray
                                                                                         .split(",");
                                                                           var finalUserArray = "";
                                                                           for (var i = 0; i < userArray.length; i++) {
                                                                                  if (userArray[i] == "") {
                                                                                         continue;
                                                                                  } else {
                                                                                         finalUserArray += userArray[i]
                                                                                                       + "  ,";
                                                                                  }
                                                                           }

                                                                           if (finalUserArray != "") {
                                                                                  var finalString = finalUserArray
                                                                                                .substring(
                                                                                                              0,
                                                                                                              finalUserArray.length - 1);
                                                                                  $("#confirmationString").html(
                                                                                                '<liferay-ui:message key="Do you want to Disable"/>&nbsp;'
                                                                                                              + finalString
                                                                                                              + '&nbsp;?');
                                                                           } else {
                                                                                  $("#confirmationString")
                                                                                                .html(
                                                                                                              '<liferay-ui:message key="Please select users to deactivate."/>');
                                                                           }

                                                                            $("#confirmationBoxId").modal(
                                                                                         'show');

                                                                            $("#okbuttonid").click(function() 
                                                                           {
                                                                                  
                                                                                  $("#confirmationBoxId").modal('hide');
                                                                                  if (finalUserArray != "") 
                                                                                  {
                                                                                         
                                                                                         callToDisableUsers(userIdArray,userOktaIdArray);
                                                                                  } 
                                                                                  else 
                                                                                  {
                                                                                         $("#disableUserButton").prop("disabled",false);
                                                                                         makeButtonEnable();
                                                                                  }
                                                                                  
                                                                           });

                                                                           $("#cancelbuttonid")
                                                                                         .click(
                                                                                                       function() {
                                                                                                              $(
                                                                                                                           "#confirmationBoxId")
                                                                                                                           .modal(
                                                                                                                                         'hide');
                                                                                                              $(
                                                                                                                           "#disableUserButton")
                                                                                                                           .prop(
                                                                                                                                         "disabled",
                                                                                                                                         false);
                                                                                                              makeButtonEnable();
                                                                                                       });
                                                                     });

                                         //Added by Niranjan: START
                                         $("#enableUserButton").click(
                                                                     function() {
                                                                            $("#enableUserButton").prop("disabled", true);
                                                                           makeButtonDisable();

                                                                           var userIdArray = $('table#userListTable input:checkbox').map(
                                                                                  function() 
                                                                                  {
                                                                                         return $(this).prop('checked') ? $(this).prop('name'): ""
                                                                                  }).get().join();

                                                                           var userOktaIdArray = $('table#userListTable input:checkbox').map(
                                                                                  function() 
                                                                                  {
                                                                                         
                                                                                         return $(this).prop('checked') ? 
                                                                                                       $(this).attr('oktaId'): ""
                                                                                  }).get().join();

                                                                           var userArray = userIdArray.split(",");
                                                                           
                                                                           var finalUserArray = "";
                                                                           for (var i = 0; i < userArray.length; i++) {
                                                                                  if (userArray[i] == "") {
                                                                                         continue;
                                                                                  } else {
                                                                                         finalUserArray += userArray[i]
                                                                                                       + "  ,";
                                                                                  }
                                                                           }

                                                                           if (finalUserArray != "") {
                                                                                  var finalString = finalUserArray
                                                                                                .substring(
                                                                                                              0,
                                                                                                              finalUserArray.length - 1);
                                                                                  $("#confirmationString").html(
                                                                                                '<liferay-ui:message key="Do you want to Enable"/>&nbsp;'
                                                                                                              + finalString
                                                                                                              + '&nbsp;?');
                                                                           } else {
                                                                                  $("#confirmationString")
                                                                                                .html(
                                                                                                              '<liferay-ui:message key="Please select users to Activate."/>');
                                                                           }

                                                                            $("#confirmationBoxId").modal('show');

                                                                            $("#okbuttonid").click(function() 
                                                                                         {
                                                                                                
                                                                                                $("#confirmationBoxId").modal('hide');
                                                                                                if (finalUserArray != "") 
                                                                                                {
                                                                                                       callEnableUsers(userIdArray,userOktaIdArray);
                                                                                                } 
                                                                                                else 
                                                                                                {
                                                                                                       $("#enableUserButton").prop("disabled",false);
                                                                                                       makeButtonEnable();
                                                                                                }
                                                                                         });

                                                                           $("#cancelbuttonid")
                                                                                         .click(
                                                                                                       function() {
                                                                                                              $(
                                                                                                                           "#confirmationBoxId")
                                                                                                                           .modal(
                                                                                                                                         'hide');
                                                                                                              $(
                                                                                                                           "#enableUserButton")
                                                                                                                           .prop(
                                                                                                                                         "disabled",
                                                                                                                                         false);
                                                                                                              makeButtonEnable();
                                                                                                       });
                                                                     });
                                         
                                         
                                         
                                         $("#addUserButton").click(
                                                       function() {
                                                              $("#addUserModal").modal(); 
                                         
                                         });
                                         
                                         $("#logServReqButton").click(
                                                       function() {
                                                              //resetLicenseData();
                                                              document.getElementById("acquireLicenseForm").reset();
                                                              $("#licenseModal").modal();
                                                              
                                                              
                                                              //Watermark for description
                                                              $('#customDescription').val(watermarkDesc).addClass('watermark');
                                                              
                                                              $('#customDescription').blur(function(){
                                                                     if ($(this).val().length == 0){
                                                                     $(this).val(watermarkDesc).addClass('watermark');
                                                                     }
                                                             });

                                                              $('#customDescription').focus(function(){
                                                                     if ($(this).val()!= null){
                                                                     $(this).val('').removeClass('watermark');
                                                                     }
                                                             }); 
                                                              
                                         });
                                                              
                                                              
                                         
                                         
                                         
                                         
                                         
                                         //Added by Niranjan: END

                                         
                                         $("#closeButtonID, #closeMarkId").click(function() {
                                                $("#confirmationBoxId").modal('hide');
                                                $("#responseMessage").hide();
                                                $("#disableUserButton").prop('disabled', false);
                                                $("#enableUserButton").prop("disabled", false);
                                                makeButtonEnable();
                                                location.reload();
                                         });
                                         
                                         
                                         /* $("#cancelButtonID, #closeMarkId").click(function() {
                                                location.reload();
                                         }); */
                                  });



       function processDisableUserResponce(data) {
              
              
              var emailAddress = data.name;

              var msg ="";
              var msg ="";
              msg += '<div class="deacFail"><liferay-ui:message key="Success"/></div> <div>'
                     + '&nbsp;<liferay-ui:message key="User successfully Disabled."/>';
              
              
                     $('#MessageString').html(msg);
                     $('#responseMessage').modal('show');
              
       }

       function processEnableUserResponse() {
              
              var msg ="";
              msg += '<div class="deacFail"><liferay-ui:message key="Success"/></div> <div>'
                     + '&nbsp;<liferay-ui:message key="User successfully Enabled."/>';
              
              
                     $('#MessageString').html(msg);
                     $('#responseMessage').modal('show');
       }

       
       function createDeactivationMsg(successUsrsMsg, failedUsrsMsg) {

              $('#responseStatus').html('<liferay-ui:message key="Manage Users"/>');
              var msg = "";

              if (successUsrsMsg.trim() != "") {
                     msg += '<div class="deacSucc"><liferay-ui:message key="Success"/></div> <div>'
                                  + successUsrsMsg
                                  + '&nbsp;<liferay-ui:message key="is"/>/<liferay-ui:message key="are successfully disabled"/> </div><br/>';
              }
              if (failedUsrsMsg.trim() != "") {
                     msg += '<div class="deacFail"><liferay-ui:message key="Failure"/></div> <div>'
                                  + failedUsrsMsg
                                  + '&nbsp;<liferay-ui:message key="is"/>/<liferay-ui:message key="could not be disabled. Please contact Administrator."/> </div>';
              }

              $('#MessageString').html(msg);
              $('#responseMessage').modal('show');
              
       }


       function successMsg(response){
              var json = JSON.parse(response);
              var user = (json.status);
              var msg ="";
              msg += '<div class="deacFail"><liferay-ui:message key="Success"/></div> <div>'
                     + user
                     + '&nbsp;<liferay-ui:message key="is successfully added."/>';
              
              
                     $('#MessageString').html(msg);
                     $('#responseMessage').modal('show');
                     var userList = "${userListPopulation}"
                           
                     $.ajax({
                           type : "POST",
                           url : userList,
                           dataType : 'json',
                           success : function(data) {
                                  
                                  $('#userListTable').dataTable().fnClearTable();
                                  $('#userListTable').dataTable().fnAddData(data);
                                  
                           },
                           error : function() {
                                  
                                  showAlert();
                           }
                     });
       }
       
       function callToDisableUsers(userIdArray, userOktaIdArray) {
              var check = "valid";
              
              $("tr td:nth-child(1)").each(function(i, tr) 
                           {
                           row = $(this);
                                  if (row.find('input[type="checkbox"]').is(':checked') )
                                  {

                                         statusInactiveArr.push( $(this).find('input[type="checkbox"]').attr('status'));
                                  }
                                                                           
                           });
              for(i = 0; i< statusInactiveArr.length; i++){
                     
                     if(statusInactiveArr[i] == "Inactive"){
                     document.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
                     document.getElementById("MessageString").innerHTML = '<liferay-ui:message key="Please select only Active users."/>';
       
                     $('#responseMessage').modal('show');
                     check = "invalid";
                     }
              }
              
              if(check =="valid"){
              var disableUserURL = "${disableUser}" + "&userList=" + userIdArray
                           + "&userOktaIdArray" + userOktaIdArray;
                     $.ajax({
                           type : "POST",
                           url : disableUserURL,
                           dataType : 'json',
                           success : function(data) {
                                  //alert("success :: " + data);
                                  var status = data.status;
                                  if(status == "success"){
                                  processDisableUserResponce(data);
                                  }
                                  else{
                                         showAlert();
                                  }
                                  
                           },
                           error : function() {
                                  
                                  showAlert();
                           }
                     });
              }
}

       // Call ProfileSecurityController to enable the selected users
       function callEnableUsers(userIdArray, userOktaIdArray) {
              
              var check = "valid";
              countInactive = 0;
              
              $("tr td:nth-child(1)").each(function(i, tr) 
                           {
                           row = $(this);
                                  if (row.find('input[type="checkbox"]').is(':checked') )
                                  {

                                         statusActiveArr.push( $(this).find('input[type="checkbox"]').attr('status'));
                                  }
                                                                           
                           });
              for(i = 0; i< statusActiveArr.length; i++){
                     
              if(statusActiveArr[i] == "Active"){
              document.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
              document.getElementById("MessageString").innerHTML = '<liferay-ui:message key="Please select only Inactive users."/>';

              $('#responseMessage').modal('show');
              check = "invalid";
              }
              }
              
              $("tr td:nth-child(1)").each(function(i, tr) 
                           {
                           row = $(this);
                                  if (row.find('input[type="checkbox"]').is(':checked') )
                                  {
                                         var status = $(this).find('input[type="checkbox"]').attr('status');
                           
                                         if( status == "Inactive" ){
                                                countInactive++;
                                         }
                                  }
                                                                           
                           });
              
              if(countInactive > remainingLicenses){
                     var msg ="";
                     msg += '<div class="deacFail"><liferay-ui:message key="Error"/></div> <div>'
                           + '&nbsp;<liferay-ui:message key="You have insufficient number of Licenses, Please try again."/>';
                           
                     
                     
                           $('#MessageString').html(msg);
                           $('#responseMessage').modal('show');
                           check = "invalid";
              }
              
              
                           
              
              
              if(check =="valid"){
                     userOktaIdArray = [];
                     $("tr td:nth-child(1)").each(function(i, tr) 
                                  {
                                  row = $(this);
                                         if (row.find('input[type="checkbox"]').is(':checked') )
                                         {

                                                userOktaIdArray.push( $(this).find('input[type="checkbox"]').attr('oktaid'));
                                         }
                                                                                  
                                  });
                     
                     
                     var enableUserURl = "${enableUser}" + "&userList=" + userIdArray
                     + "&userOktaIdArray=" + userOktaIdArray; //userOktaIdArray
                     
                     $.ajax({
                           type : "POST",
                           url : enableUserURl,
                           dataType : 'json',
                           success : function(data) {
                                  
                     var status = data.status;
                     if(status == "Success"){
                           processEnableUserResponse();
                     }
                     else if(status == "Failure"){
                           showAlert();
                     }
                     
                     },
                           error : function()
                                  {
                                  showAlert();
                                  }
                     });
              }
              
       }

       
       function getUserList(userListURL) {
              var checkboxIndex = 0;
              var checkboxIndexRoles = 0;
              var checkboxIndexMobile = 0;
              function getCheckboxID()
              {
                     return checkboxIndex++;
              }
              function getcheckBoxIDRoles(){
                     return checkboxIndexRoles++;
              }
              function getcheckBoxIDMobileNumber(){
                     return checkboxIndexMobile++;
              }
              
              $
                           .ajax({
                                  url : userListURL,
                                  type : "POST",
                                  dataType : 'json',
                                  success : function(data) {
                                         $('#userListTable')
                                                       .DataTable(
                                                                     {
                                                                           "aaData" : data,
                                                                           "bFilter" : false,
                                                                           "processing" : false,
                                                                           "bPaginate" : false,
                                                                           "serverSide" : false,
                                                                           "bLengthChange" : false,
                                                                           "bSortable" : false,
                                                                           "bInfo" : false,
                                                                           'columnDefs' : [ {
                                                                                  'targets' : 0,
                                                                                  'searchable' : false,
                                                                                  'orderable' : false,
                                                                                  'className' : 'dt-body-center',
                                                                                  'render' : function(data, type,
                                                                                                full, meta) {
                                                                                         
                                                                                         return '<input type="checkbox" id="checkBox'+getCheckboxID()+'" name="' + full[6] + '" oktaId="'+ full[7] +'" status="'+ full[1]+'">';
                                                                                         
                                                                                  }
                                                                           },
                                                                           {
                                                                                  'targets' : 7,
                                                                                  'searchable' : false,
                                                                                  'orderable' : false,
                                                                                  'className' : 'dt-body-center',
                                                                                  'render' : function(data, type,
                                                                                                full, meta) {
                                                                                         
                                                                                         return '<img src="<%=request.getContextPath()%>/images/Pencil.png" id="editRoles'+getcheckBoxIDRoles()+'" style="height: 20px; width: 20px;" onClick="openModal(this.id);">';
                                                                                         
                                                                                  }
                                                                           },
                                                                           
                                                                           {
                                                                                  'targets' : 8,
                                                                                  'searchable' : false,
                                                                                  'orderable' : false,
                                                                                  'className' : 'dt-body-center',
                                                                                  'render' : function(data, type,
                                                                                                full, meta) {
                                                                                         
                                                                                         return '<img src="<%=request.getContextPath()%>/images/Pencil.png" id="editMobileNumber'+getcheckBoxIDMobileNumber()+'"  style="height: 20px; width: 20px;" onClick="openEditDetailsModal(this.id);">';
                                                                                         
                                                                                  }
                                                                           }
                                                                           ]
                                                                     });
                                         
                                         $("tr td:nth-child(2)").each(function(i, tr) {
                                                if($(tr).text() == "Active"){
                                                       countActive++;
                                                       
                                                }
                                                
                                                if($(tr).text() == "Inactive")
                                                countInactive++;
                                                
                                                });
                                         
                                         remainingLicenses = noOfOktaUsers - countActive;
                                         if(remainingLicenses<=0){
                                                document.getElementById("enableUserButton").disabled = true;
                                                document.getElementById("addUserButton").disabled = true;
                                                
                                                $("#addUserButton").addClass('disableadduser');
                                                $("#enableUserButton").addClass('disableadduser');
                                         }
                                                                           
                                         if(countInactive == noOfOktaUsers){
                                                document.getElementById("disableUserButton").disabled = true;
                                                $("#disableUserButton").addClass('disableadduser');
                                         }
                                         
                                  if(remainingLicenses < 0){
                                         $("#logServReqButton").attr("title", '0 licenses left');
                                         $("#enableUserButton").attr("title", '0 licenses left');
                                         $("#addUserButton").attr("title", '0 licenses left');
                                         
                                  }
                                  else{
                                         $("#logServReqButton").attr("title", +remainingLicenses+ " " +'licenses left');
                                         $("#enableUserButton").attr("title", +remainingLicenses+ " " +'licenses left');
                                         $("#addUserButton").attr("title", +remainingLicenses+ " " +'licenses left');
                                         
                                  }
                                         
                                  },
                                  error : function() {
                                  
                                  }
                           });
       }

       function showAlert() {
              document.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
              document.getElementById("MessageString").innerHTML = '<liferay-ui:message key="Some error occurred. Contact Administrator."/>';

              $('#responseMessage').modal('show');
              $("#disableUserButton").prop('disabled', false);
              makeButtonEnable();
       }

       function makeButtonEnable() {
              //alert("enable button");
              $("#enableUserButton").addClass('enableUserButton');
              $("#enableUserButton").removeClass('disableButton');
              $("#disableUserButton").removeClass('disableButton');
              $("#disableUserButton").addClass('enableButton');
              

       }

       function makeButtonDisable() {
              //alert("disable button");
              $("#AddLicensebutton").removeClass('AddLicensebutton');
              $("#AddLicensebutton").addClass('disableButton');
              $("#enableUserButton").removeClass('enableUserButton');
              $("#enableUserButton").addClass('disableButton');
              $("#disableUserButton").removeClass('enableButton');
              $("#disableUserButton").addClass('disableButton');
       }
       

       
       
    function BindControls() {
       
       //$('#siteID').val(siteData);
        $('#siteID').autocomplete({
                   source: siteData,
                   minLength: 0,
                           appendTo: "#licenseModal",
                   scroll: true
               }).focus(function() {
                   $(this).autocomplete("search", "");
               });
           }
       


function processPhone() {
    
    
    phone = $('#mobilenumber').val();
    
    var country = 'Indonesia';
    if (isValidNumber(phone, country)) {
    //alert("True");
           return true;
    } else {
    //alert("False");
           return false;
    }
}

function getAddUserData(){
       $("#addUserFormModel").validate();
       if($("#addUserFormModel").valid()){
       var addUserURl = "${addUser}"
              $.ajax({
                     type : "POST",
                     url : addUserURl,
                     data : $("#addUserFormModel").serialize(),
                     success : function(data) {
                           
                           successMsg(data);
                           $("#addUserModal").modal('hide');
                            $("#addUserModal").css("display", "none");
                     },
                     error : function() {
                           $("#addUserModal").modal('hide');
                           $("#addUserModal").css("display", "none");
                           showAlert();
                     }
              });
       
       resetUserData();
       }

    
}

function resetUserData(){
       document.getElementById("addUserFormModel").reset();
}


function acquireLicense(){
       
       
       $("#acquireLicenseForm").validate();
       if($("#acquireLicenseForm").valid()){
              
              
              $.ajax({
                     type : "POST",
                     url: '<%=addLicense.toString()%>',
                     data : $('#acquireLicenseForm').serialize(),
                     dataType : 'json',
                     success : function(data){
                           //alert(data);
                           var status = data.requestID;
                           //alert("success" +status);
                           if(status == "Success"){
                           licenseSuccessMsg();
                           }
                           
                           else{
                                  $("#licenseModal").modal('hide');
                                  $("#licenseModal").css("display", "none");
                                  showAlert();
                                  
                           }
                           $("#licenseModal").modal('hide');
                           $("#licenseModal").css("display", "none");
                     },
                     error : function() {
                           //alert("error##########");
                           showAlert();
                     }
              });
       }
       
}

function licenseSuccessMsg(){
       
       var msg ="";
       msg += '<div class="deacFail"><liferay-ui:message key="Success"/></div> <div>'
              + '&nbsp;<liferay-ui:message key="Your Request to acquire License is successfully submitted"/>';
       
       
              $('#MessageString').html(msg);
              $('#responseMessage').modal('show');
              
       
}


function resetLicenseData(){
       //alert("Inside resetLicenseData");
       document.getElementById('summary').value = "";
       document.getElementById('customDescription').value = "";
}

function resetEditUserGroupFrom(){

document.getElementById("editUserFormModel").reset();

}
function openModal(id){
       $("tr td:nth-child(1)").each(function(i, tr) 
                     {
                     row = $(this);
                           if (row.find('input[type="checkbox"]').is(':checked') )
                           {
                                  var boxId = "editRoles"+i;
                                  //alert("Unchecking checkbox"+boxId);
                                  $("#checkBox"+i).prop('checked',false);
                     
                           }
                                                                     
                     });
       
                     
                     id = id.replace("editRoles","");
       
                     $("#userListTable tr").each(function() 
                     {
                           
                           row = $(this);       
                           var status = $(this).find('input[type="checkbox"]').attr('status');
                                             
                           $("#checkBox"+id).prop('checked',true);
       
                           if (row.find('input[type="checkbox"]').is(':checked')) //&& status == "Active" )
                           {
                                         firstName = $(this).find('td:nth-child(3)').html();
                                         //alert(firstName);  
                                         lastName = $(this).find('td:nth-child(4)').html();
                                         //alert(lastName);
                                         userName = $(this).find('td:nth-child(7)').html();
                                         //alert(userName);
                                  $('#editUserModal').modal('show'); 
                                  document.getElementById('fname').value = firstName;
                                  document.getElementById('lstname').value = lastName;
                                  document.getElementById('usrname').value = userName;   

                                  var editUserURl = "${editUser}";
                                  var usrname = "usrname="+document.getElementById("usrname").value;
                           
                                  $.ajax({
                                         type : "POST",
                                         url : editUserURl,
                                         data : usrname,
                                         dataType : 'json',
                                         success : function(data) {
                                                
                                                var roles = data.roles;
                                                
                                                for(i=0;i<roles.length;i++){
                                                	if(customerPortalAdmin == "true" || endCustomerAdmin == "true"|| nocManager == "true"){
                                                		$("#submitButtonRoles").addClass('img-submit-servicerequestenabled');
                                                        $("#submitButtonRoles").removeClass('disableSubmitButton');
        												$("#submitButtonRoles").addClass('btn-primary-tt');
        												document.getElementById("customerev").disabled = false;
        												  document.getElementById("customersv").disabled = false;
        												  document.getElementById("customerpv").disabled = false;
        												  document.getElementById("customerbv").disabled = false;
                                                	}
                  								
												  
												  else{
													  document.getElementById("customerev").disabled = true;
                                                      document.getElementById("customersv").disabled = true;
                                                      document.getElementById("customerpv").disabled = true;
                                                      document.getElementById("customerbv").disabled = true;
                                                      $("#submitButtonRoles").removeClass('img-submit-servicerequestenabled');
                                                      $("#submitButtonRoles").addClass('disableSubmitButton');
												  }

                                                       if(roles[i] == "CustomerExecutiveView"){
                                                              document.getElementById("customerev").checked = true;
                                                       }
                                 
                                                       if((roles[i] == "CustomerExecutiveView"  && status == "Inactive")|| !typeof(status) === "undefined"){
                                                              document.getElementById("customerev").checked = true;
                                                              document.getElementById("customerev").disabled = true;
                                                              document.getElementById("customersv").disabled = true;
                                                              document.getElementById("customerpv").disabled = true;
                                                              document.getElementById("customerbv").disabled = true;
                                                              $("#submitButtonRoles").removeClass('img-submit-servicerequestenabled');
                                                              $("#submitButtonRoles").addClass('disableSubmitButton');

                                                              
                                         
                                                       }
                                                       if(roles[i] == "Customer"){
                                                              document.getElementById("customersv").checked = true;
                                                              
                                                              
                                                       }
                                                       if((roles[i] == "Customer" && status == "Inactive")|| !typeof(status) === "undefined"){
       
                                                              document.getElementById("customersv").checked = true;
                                                              document.getElementById("customerev").disabled = true;
                                                              document.getElementById("customersv").disabled = true;
                                                              document.getElementById("customerpv").disabled = true;
                                                              document.getElementById("customerbv").disabled = true;
                                                             $("#submitButtonRoles").removeClass('img-submit-servicerequestenabled');
                                                              $("#submitButtonRoles").addClass('disableSubmitButton');

                                                              
                                                              
                                                       }
                                                       if(roles[i] == "CustomerProjectView"){
                                                              document.getElementById("customerpv").checked = true;
                                                              
                                                              
                                                       }
                                                       
                                                       if((roles[i] == "CustomerProjectView" && status == "Inactive")|| !typeof(status) === "undefined"){
                                                              document.getElementById("customerpv").checked = true;
                                                              document.getElementById("customerev").disabled = true;
                                                              document.getElementById("customersv").disabled = true;
                                                              document.getElementById("customerpv").disabled = true;
                                                              document.getElementById("customerbv").disabled = true;
                                                              $("#submitButtonRoles").removeClass('img-submit-servicerequestenabled');
                                                              $("#submitButtonRoles").addClass('disableSubmitButton');

                                                              
                                                              
                                                       }
                                                       if(roles[i] == "CustomerBusinessView"){
                                                              document.getElementById("customerbv").checked = true;
                                                              
                                                              
                                                       }
                                                       if((roles[i] == "CustomerBusinessView" && status == "Inactive")|| !typeof(status) === "undefined"){
                                                              document.getElementById("customerbv").checked = true;
                                                              document.getElementById("customerev").disabled = true;
                                                              document.getElementById("customersv").disabled = true;
                                                              document.getElementById("customerpv").disabled = true;
                                                              document.getElementById("customerbv").disabled = true;
                                                             $("#submitButtonRoles").removeClass('img-submit-servicerequestenabled');
                                                              $("#submitButtonRoles").addClass('disableSubmitButton');
                                                       }
  
                                                      if(roles[i] != "CustomerExecutiveView" && roles[i] != "Customer" && roles[i] != "CustomerProjectView" && roles[i] != "CustomerBusinessView" && status == "Inactive"){
												document.getElementById("customerev").disabled = true;
												document.getElementById("customersv").disabled = true;
												document.getElementById("customerpv").disabled = true;
												document.getElementById("customerbv").disabled = true;
												$("#submitButtonRoles").removeClass('img-submit-servicerequestenabled');
												$("#submitButtonRoles").addClass('disableSubmitButton');
								}
                                                                                                                                                                                                                 
                                                }
                                                
                                         },
                                         error : function() {
                                                showAlert();
                                         }
                                  });                               
                           }
       

              
       
                     });
                                                                                
                                                                                 
                                                                                
                                                                                 
}

function editRoles(){
       $('#editUserModal').modal('hide');
       var formdata = $('#editUserFormModel').serialize();
       var updateUserURl = "${updateUser}";
       $.ajax({
              type : "POST",
              url : updateUserURl,
              data : formdata,
              dataType : 'json',
              success : function(data) {
                     var roleStatus = data.status_roles;
                     var deleteStatus = data.status_deleteroles;
                     if(roleStatus == "Success" && deleteStatus == "Success"){
                     var msg ="";
                     msg += '<div class="deacFail"><liferay-ui:message key="Success"/></div> <div>'
                           + '&nbsp;<liferay-ui:message key="User Groups are successfully Updated."/>';
                     
                     
                           $('#MessageString').html(msg);
                           $('#responseMessage').modal('show');
                     }
                     else{
                           showAlert();
                     }
                     
              },
              error : function() {
                     showAlert();
              }
       }); 

}

function openEditDetailsModal(id){
       
       $("tr td:nth-child(1)").each(function(i, tr) 
                     {
                     row = $(this);
                           if (row.find('input[type="checkbox"]').is(':checked') )
                           {
                                  var boxId = "editRoles"+i;
                                  //alert("Unchecking checkbox"+boxId);
                                  $("#checkBox"+i).prop('checked',false);
                     
                           }
                                                                     
                     });
       
       id = id.replace("editMobileNumber","");
       
       $("#userListTable tr").each(function()  
                     {
                     row = $(this);
                     var status = $(this).find('input[type="checkbox"]').attr('status');
                     
                      $("#checkBox"+id).prop('checked',true);
                           if (row.find('input[type="checkbox"]').is(':checked')) //&& status == "Active" )
                           {
                                  firstName = $(this).find('td:nth-child(3)').html();
                                  //alert(firstName);  
                                  lastName = $(this).find('td:nth-child(4)').html();
                                  //alert(lastName);
                                  userName = $(this).find('td:nth-child(7)').html();
                                  //alert(userName);
                                  mobileNum = $(this).find('td:nth-child(5)').html();
                                  //alert(mobileNum);
                                  
                                  document.getElementById('frstname').value = firstName;
                                  document.getElementById('lname').value = lastName;
                                  document.getElementById('uname').value = userName;
                                  document.getElementById('mobilenum').value = mobileNum;
                                  
                                  var getLanguageURl = "${getLanguage}";
                            	  var formdata = $('#editMobileFormModel').serialize();
                               	  $.ajax({
					                   type : "POST",
					                   url : getLanguageURl,
					                   data : formdata,
					                   dataType : 'json',
					                   success : function(data) {
					                	   var lang = data.language;
					                	   
					                	   if(lang == "en_US" || lang == "English"){
					                		   document.getElementById("editlanguageEng").checked = true;
					                		   
					                	   }
					                	   if(lang == "in_ID" || lang == "Bahasa"){
					                		   
					                		   document.getElementById("editlanguageBaha").checked = true;
					                		 
					                	   }
					                	   
					                   }
                             
					          });
                                  $('#editMobileNumberModal').modal('show');
                                  if(customerPortalAdmin == "true" || endCustomerAdmin == "true"|| nocManager == "true"){
                                	  document.getElementById('mobilenum').disabled = false;
                                      
                                      document.getElementById('editlanguageEng').disabled = false;
                                      document.getElementById('editlanguageBaha').disabled = false;
                                      $("#submitButtonEditDetails").removeClass('disableSubmitButton');
                                      $("#submitButtonEditDetails").addClass('img-submit-servicerequestenabled');
                                  }
                                  else{
                                	  document.getElementById('mobilenum').disabled = true;
                                      document.getElementById('editlanguageEng').disabled = true;
                                      document.getElementById('editlanguageBaha').disabled = true;
                                      $("#submitButtonEditDetails").removeClass('img-submit-servicerequestenabled');
                                      $("#submitButtonEditDetails").addClass('disableSubmitButton');
                                  }
                                 
                                  
                                  $("#editlanguageEng").click(function() {
                                	  document.getElementById("editlanguageBaha").checked = false;
                               });
                                  $("#editlanguageBaha").click(function() {
                                	  document.getElementById("editlanguageEng").checked = false;
                               });
                                  
                                                                                                                                  
                                  if(status == "Inactive" || !typeof(status) === "undefined"){
                                                                                                                                                                
                                         document.getElementById('mobilenum').disabled = true;
                                         document.getElementById('editlanguageEng').disabled = true;
                                         document.getElementById('editlanguageBaha').disabled = true;
                                         $("#submitButtonEditDetails").removeClass('img-submit-servicerequestenabled');
                                         $("#submitButtonEditDetails").addClass('disableSubmitButton');
                                                                                                                                                                
                                  }
                                                                                         
                           }
              
       });

}

function updateDetails(){
       $('#editMobileNumberModal').modal('hide');
       var editMobileURl = "${editMobile}";
       var formdata = $('#editMobileFormModel').serialize();
       
              $.ajax({
                     type : "POST",
                     url : editMobileURl,
                     data : formdata,
                     dataType : 'json',
                     success : function(data) {
                           var update = data.status_mobile;
                           if(update == "Success"){
                           var msg ="";
                           msg += '<div class="deacFail"><liferay-ui:message key="Success"/></div> <div>'
                                  + '&nbsp;<liferay-ui:message key="User Details successfully Updated."/>';
                           
                           
                                  $('#MessageString').html(msg);
                                  $('#responseMessage').modal('show');
                           }
                           else{
                                  showAlert();
                           }
                           
                     },
                     error : function() {
                           showAlert();
                     }
              });
}


/* function reloadForm(){
       location.reload();
} */

</script>
<div class="supreme-container">
<button type="submit" id="logServReqButton" class="logServReqButton"
              name="license" data-toggle="modal" data-target="#licenseModal" title="">

              <div id="AddLicenseIcon"></div>
              <div id="AddLicense">
                     <liferay-ui:message key="Acquire License" />
              </div>
</button>



<div id="userheader">

       <div class="row-fluid" id="registeredusers">
              <liferay-ui:message key="Registered Users" />

       </div>
</div>

<div class="container-fluid maincontainer">
       <table id="userListTable" class="display" >
              <thead>
                     <tr>
                           <th class="tableSelect"><liferay-ui:message key="Select" /></th>
                           <th class="tablestatus"><liferay-ui:message key="Status" /></th>
                           <th class="tablefirstname"><liferay-ui:message key="First Name" /></th>
                           <th class="tablelastname"><liferay-ui:message key="Last Name" /></th>
                           <th class="tableworkcontact"><liferay-ui:message
                                         key="Work Contact" /></th>
                           <th class="tablehomecontact"><liferay-ui:message
                                         key="Home Contact" /></th>
                           <th class="tableemail"><liferay-ui:message key="Email" /></th>
                           <th class="tableeditroles"><liferay-ui:message key="Edit User Groups" /></th>
                           <th class="tableeditmobilenumber"><liferay-ui:message key="Edit Details" /></th>

                     </tr>
              </thead>
       </table>
</div>
<div id="buttons">

       <button type="submit" id="addUserButton" class="addUserButton"
              name="Add" data-toggle="modal" data-target="addUserModal">

              <div id="EnableIcon"></div>
              <div id="EnableContent">
                     <liferay-ui:message key="Add Users" />
              </div>
       </button>

       <button type="submit" id="enableUserButton" class="enableUserButton"
              name="Enable">

              <div id="EnableIcon"></div>
              <div id="EnableContent">
                     <liferay-ui:message key="Enable Users" />
              </div>
       </button>

      
       <button type="submit" id="disableUserButton" name="Disable">

              <div id="DisableIcon"></div>
              <div id="DisableContent">
                     <liferay-ui:message key="Disable Users" />
              </div>
       </button>
</div>


</div>

<!-- Disable user Modal Start -->
<div class="modal fade" id="responseMessage" data-backdrop="static" data-keyboard="false">
       <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="closeMarkID">
                     <img class="closeMark"
                           src="<%=request.getContextPath()%>/images/Close_G_24.png" />
              </button>
              <div class="modal-title" id="responseStatus"></div>
       </div>
       <div class="modal-body tt-modal-body">
              <div id="MessageString"></div>
       </div>
       <div class="modal-footer tt-submit-row">
              <button type="button" class="closeButton" class="btn" data-dismiss="modal" id="closeButtonID">
                     <div class="buttonIcon">
                           <img src="<%=request.getContextPath()%>/images/Close_W_16.png" />
                     </div>
                     <div class="buttonText">
                           <liferay-ui:message key="Close" />
                     </div>
              </button>
       </div>
</div>

<div class="modal fade" id="confirmationBoxId" data-backdrop="static" data-keyboard="false" aria-hidden="true">
       <div class="modal-header">
              <div class="modal-title" id="confirmheader"><liferay-ui:message key="Confirm" /></div>
       </div>
       <div class="modal-body tt-modal-body">
              <div id="confirmationString"></div>
       </div>
       <div class="modal-footer tt-submit-row">
              <button type="button" class="closeButton" class="btn" data-dismiss="modal" id="okbuttonid">
                     <div class="buttonTextOK">
                           <liferay-ui:message key="OK" />
                     </div>
              </button>
              
              <button type="button" class="closeButton" class="btn" data-dismiss="modal" id="cancelbuttonid">
                     <div class="buttonText">
                           <liferay-ui:message key="Cancel" />
                     </div>
              </button>
       </div>
</div>


<!-- Add user Modal Start -->
<div class="modal fade tt-SR-modal" id="addUserModal" role="dialog"
       data-backdrop="static" data-keyboard="false" aria-hidden="true"
       tabindex="-1">
       <div class="modal-dialog modal-lg">

              <!-- Modal content-->
              <div class="modal-content">
                     <div class="modal-header">
                           <button type="button" class="close" id="closeMarkID" 
                                  data-dismiss="modal">
                                  <img class="closeMark"
                                         src="<%=request.getContextPath()%>/images/Close_G_24.png" />
                           </button>
                           <div class="modal-title logTitle">
                                  <liferay-ui:message key="ADD USER" />
                           </div>
                     </div>
                     <div class="modal-body tt-modal-body">
                           <form:form id="addUserFormModel" name="addUserFormModel"
                                  class="form-horizontal tt-modal-form" method="post">
                                  <div class="container-fluid tt-form-container">
                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="First Name:*" />

                                                </div>
                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="firstname" name="firstname" required="required" tabindex="1" />
                                                </div>

                                         </div>

                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Last Name:*" />

                                                </div>

                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="lastname" name="lastname" required="required" tabindex="1" />
                                                </div>
                                         </div>

                                         <div class="row-fluid tt-form-row">
                                         <div class="span3 tt-form-left-col"> 
                                         <div class="wrapper gridWidth18"> <liferay-ui:message key="User Name:*"/>
                                                    <div class="tooltip"><liferay-ui:message key="Enter user&apos;s email address here"/></div>
                                                      
                                                      
                                         <img class="helpIconImg" src="/TTApplicationPortlets/images/Discription_G_16.png" > 
                                         
                                  
                                         </div> 
                                         </div>

                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="username" name="username" placeholder="Users email address " required="required"  tabindex="1" />
                                                </div>


                                         </div>

                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Phone Number:*" />

                                                </div>
                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="mobilenumber" name="mobilenumber" placeholder="Enter country code" required="required"
                                                              tabindex="1" />
                                                </div>
                                         </div>

                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Language:*" />

                                                </div>
                                                <div class="span4 tt-form-right-col">
                                                       <select name="language" id="language" class="selectBoxLogInc">

                                                              <option value="English">English</option>
                                                              <option value="Bahasa">Bahasa</option>
                                                       </select>
                                                </div>
                                         </div>


                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">
                                                       <liferay-ui:message key="User Group:*" />
                                                </div>


                                                <div class="span4 tt-form-right-col" style="display: inline-flex">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" value="customerview" tabindex="1" />
                                                       </div>
                                                       <div class="span2"><liferay-ui:message key="Standard View"/></div>

                                                </div>
                                                <div class="span4 tt-form-right-col" style="display: inline-flex">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" value="customerprojectview" tabindex="1" />
                                                       </div>
                                                       <div class="span2"><liferay-ui:message key="Project View"/></div>
                                                </div>
                                                <div class="span4 tt-form-right-col"
                                                       style="display: inline-flex; position: relative; left: 23%;">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" value="customerexecutiveview" tabindex="1" />
                                                       </div>

                                                       <div class="span2"><liferay-ui:message key="Executive View"/></div>
                                                </div>
                                                <div class="span4 tt-form-right-col"
                                                       style="display: inline-flex; position: relative; left: 23%;">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" value="customerbusinessview" tabindex="1" />
                                                       </div>
                                                       <div class="span2"><liferay-ui:message key="Business View"/></div>
                                                </div>
                                         </div>



                                         <div class="row-fluid tt-form-last-row pr-15 pt-30">
                                                <div class="span12 tt-submit-row">
                                                       <span class="tt-cancel pr-10">
                                                       
                                                       <button type="button" class="btn-secondary-tt img-cancel" class="btn" data-dismiss="modal" id="cancelbuttonid">
														                     <div class="buttonText">
														                           <liferay-ui:message key="Cancel" />
														                     </div>
														      </button> 
														      
                                                              <button type="button" class="btn-secondary-tt img-cancel"
                                                                     id="resetButtonID" tabindex="13" onclick="resetUserData()">
                                                                     <liferay-ui:message key="Reset" />
                                                              </button>
                                                              
															  
														      
                                                              <button type="button" class="btn-primary-tt img-submit-servicerequestenabled"
                                                                     id="submitButtonID" onclick="getAddUserData()" tabindex="13">
                                                                     <liferay-ui:message key="Submit" />
                                                              </button>
                                                       </span>
                                                </div>
                                         </div>
                                  </div>
                           </form:form>
                     </div>

              </div>
       </div>
</div>




<!-- Edit Roles Modal Start -->
<div class="modal fade tt-SR-modal" id="editUserModal" role="dialog"
       data-backdrop="static" data-keyboard="false" aria-hidden="true"
       tabindex="-1">
       <div class="modal-dialog modal-lg">

              <!-- Modal content-->
              <div class="modal-content">
                     <div class="modal-header">
                           <button type="button" class="close" id="closeMarkID" 
                                  data-dismiss="modal" onclick="resetEditUserGroupFrom()">
                                  <img class="closeMark"
                                         src="<%=request.getContextPath()%>/images/Close_G_24.png" />
                           </button>
                           <div class="modal-title logTitle">
                                  <liferay-ui:message key="EDIT USER GROUPS" />
                           </div>
                    </div>
                     <div class="modal-body tt-modal-body">
                           <form:form id="editUserFormModel" name="editUserFormModel"
                                  class="form-horizontal tt-modal-form" method="post">
                                  <div class="container-fluid tt-form-container">
                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="First Name:*" />

                                                </div>
                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="fname" name="fname" required="required" readOnly="readOnly"  tabindex="1" />
                                                </div>

                                         </div>

                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Last Name:*" />

                                                </div>

                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="lstname" name="lstname" required="required" readOnly="readOnly" tabindex="1" />
                                                </div>
                                         </div>

                                         <div class="row-fluid tt-form-row">
                                         <div class="span3 tt-form-left-col"> 
                                         <div class="wrapper gridWidth18"> <liferay-ui:message key="User Name:*" />
                                         </div> 
                                         </div>

                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="usrname" name="usrname" placeholder="Users email address " required="required" readOnly="readOnly"  tabindex="1" />
                                                </div>


                                         </div>

                                  
                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">
                                                       <liferay-ui:message key="User Group:*" />
                                                </div>


                                                <div class="span4 tt-form-right-col" style="display: inline-flex">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" id="customersv" value="customerview" tabindex="1" />
                                                       </div>
                                                       <div class="span2"><liferay-ui:message key="Standard View"/></div>

                                                </div>
                                                <div class="span4 tt-form-right-col" style="display: inline-flex">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" id="customerpv" value="customerprojectview" tabindex="1" />
                                                       </div>
                                                       <div class="span2"><liferay-ui:message key="Project View"/></div>
                                                </div>
                                                <div class="span4 tt-form-right-col"
                                                       style="display: inline-flex; position: relative; left: 23%;">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" id="customerev" value="customerexecutiveview" tabindex="1" />
                                                       </div>

                                                       <div class="span2"><liferay-ui:message key="Executive View"/></div>
                                                </div>
                                                <div class="span4 tt-form-right-col"
                                                       style="display: inline-flex; position: relative; left: 23%;">
                                                       <div class="span1">
                                                              <input type="checkbox" name="customer" id="customerbv" value="customerbusinessview" tabindex="1" />
                                                       </div>
                                                       <div class="span2"><liferay-ui:message key="Business View"/></div>
                                                </div>
                                         </div>

       
                                         <div class="row-fluid tt-form-last-row pr-15 pt-30">
                                                <div class="span12 tt-submit-row">
                                                       <span class="tt-cancel pr-10">
                                                       <button type="button" class="btn-secondary-tt img-cancel" data-dismiss="modal" onclick="resetEditUserGroupFrom()" id="cancelButtonID" tabindex="13">
                                                                     <liferay-ui:message key="Cancel" />
                                                              </button>
                                                       
                                                              <button type="button" class="btn-primary-tt img-submit-servicerequestenabled"
                                                                     id="submitButtonRoles" onclick="editRoles()" tabindex="13">
                                                                     <liferay-ui:message key="Update" />
                                                              </button>
                                                              
                                                              
                                                       </span>
                                                </div>
                                         </div>
                                  </div>
                           </form:form>
                     </div>

              </div>
       </div>
</div>


<!-- Edit Details Modal -->

<div class="modal fade tt-SR-modal" id="editMobileNumberModal" role="dialog"
       data-backdrop="static" data-keyboard="false" aria-hidden="true"
       tabindex="-1">
       <div class="modal-dialog modal-lg">

              <!-- Modal content-->
              <div class="modal-content">
                     <div class="modal-header">
                           <button type="button" class="close" id="closeMarkID" 
                                  data-dismiss="modal">
                                  <img class="closeMark"
                                         src="<%=request.getContextPath()%>/images/Close_G_24.png" />
                           </button>
                           <div class="modal-title logTitle">
                                  <liferay-ui:message key="EDIT DETAILS" />
                           </div>
                     </div>
                     <div class="modal-body tt-modal-body">
                           <form:form id="editMobileFormModel" name="editMobileFormModel"
                                  class="form-horizontal tt-modal-form" method="post">
                                  <div class="container-fluid tt-form-container">
                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="First Name:*" />

                                                </div>
                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="frstname" name="frstname" required="required" readOnly="readOnly"  tabindex="1" />
                                                </div>

                                         </div>

                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Last Name:*" />

                                                </div>

                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="lname" name="lname" required="required" readOnly="readOnly" tabindex="1" />
                                                </div>
                                         </div>


                                         <div class="row-fluid tt-form-row">
                                         <div class="span3 tt-form-left-col"> 
                                         <div class="wrapper gridWidth18"> <liferay-ui:message key="User Name:*" />
                                         </div> 
                                         </div>

                                                <div class="span9 tt-form-right-col">
                                                       <input type="text" id="uname" name="uname" placeholder="Users email address " required="required" readOnly="readOnly"  tabindex="1" />
                                                </div>


                                         </div>
                                         
                                         <div class="row-fluid tt-form-row">
                                         <div class="span3 tt-form-left-col">

                                                <liferay-ui:message key="Phone Number:*" />

                                         </div>
                                         <div class="span9 tt-form-right-col">
                                                <input type="text" id="mobilenum" name="mobilenum" placeholder="Enter country code" required="required"
                                                       tabindex="1" />
                                         </div>
                                  </div> 
                                  
                                  <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Language:*" />

                                                </div>
                                                <div class="span4 tt-form-right-col">
                                                       <input type="radio" id="editlanguageEng" name="editlanguage" value="en_US"> English
  													   <input type="radio" id="editlanguageBaha" name="editlanguage" value="in_ID"> Bahasa<br>
                                                </div>
                                         </div>
                                                
                                         <div class="row-fluid tt-form-last-row pr-15 pt-30">
                                                <div class="span12 tt-submit-row">
                                                       <span class="tt-cancel pr-10">
                                                       <button type="button" class="btn-secondary-tt img-cancel" data-dismiss="modal"
                                                                     id="cancelButtonID" tabindex="13">
                                                                     <liferay-ui:message key="Cancel" />
                                                              </button>
                                                       
                                                              <button type="button" class="btn-primary-tt img-submit-servicerequestenabled"
                                                                     id="submitButtonEditDetails" onclick="updateDetails()" tabindex="13">
                                                                     <liferay-ui:message key="Update" />
                                                              </button>
                                                              
                                                              
                                                       </span>
                                                </div>
                                         </div>
                                  </div>
                           </form:form>
                     </div>

              </div>
       </div>
</div>


<!-- Modal for Acquire License -->
<div class="modal fade tt-SR-modal" id="licenseModal" role="dialog"
data-backdrop="static" data-keyboard="false" aria-hidden="true" tabindex="-1">
       <div class="modal-dialog modal-lg">
              <!-- Modal content-->

              <div class="modal-content">
                     <div class="modal-header">
                           <button type="button" class="close" id="closeMarkID" onclick="resetLicenseData()"
                                  data-dismiss="modal">
                                  <img class="closeMark"
                                         src="<%=request.getContextPath()%>/images/Close_G_24.png" />
                           </button>
                           <div class="modal-title logTitle">
                                  <liferay-ui:message key="Acquire License" />
                           </div>
                     </div>

                     <div class="modal-body tt-modal-body">
                           <form:form id="acquireLicenseForm" name="acquireLicenseForm" class="form-horizontal tt-modal-form"
                                  modelAttribute="logSRFormModel" method="post">
                                  <div class="container-fluid tt-form-container">

                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Request Title:*" />

                                                </div>
                                                <div class="span9 tt-form-right-col">
                                                       <form:input path="summary" id="summary" name="summary"
                                                              type="text" class="textBoxLogSR" tabindex="1"/>
                                                </div>
                                         </div>

                                  
                                         <div class="row-fluid tt-form-row">
                                                <div class="span3 tt-form-left-col">

                                                       <liferay-ui:message key="Description:*" />

                                                </div>
                                                <div class="span9 tt-form-right-col">
                                                       <textarea id="customDescription"name="description" rows="4" cols="100"  class="customDescription" tabindex="11">
                                                       </textarea>
                                                </div>
                                         </div>


                                         <div class="row-fluid tt-form-last-row pr-15 pt-30">
                                                <div class="span12 tt-submit-row">
                                                       <span class="tt-cancel pr-10">
                                                              <button type="button" class="btn-secondary-tt img-cancel"
                                                                     id="cancelButtonID" data-dismiss="modal" tabindex="13">
                                                                     <liferay-ui:message key="Cancel" />
                                                              </button>
                                                       </span> 
                                                       <span class="tt-submit">
                                                       <%if(allowAccess)
                                                       {
                                                       %>
					 <button type="button" class="btn-primary-tt img-submit-servicerequestenabled" id="submitButtonIDAcquLicense" onclick="acquireLicense()" tabindex="13">
                           <liferay-ui:message key="Submit" />
                           </button>
					<%
				}
				else
				{
				%>
				<button type="button"  class="btn-primary-tt img-submit-servicerequest" id="submitButtonIDAcquLicense" tabindex="13" title="Contact Administrator">
                <liferay-ui:message key="Submit" />
                 </button>
				<%
				}
				%>	
                                                       
                                                       
                                                             
                                                                     
                                                                     
                                                </span>
                                                </div>
                                         </div>
                                  </div>
                           </form:form>
                     </div>
              </div>
       </div>
</div>





<div class="modal fade" id="logSRSubmitResponse" data-backdrop="static"
       data-keyboard="false">
       <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"
                     aria-hidden="true">
                     <img class="closeMark"
                           src="<%=request.getContextPath()%>/images/Close_G_24.png" />
              </button>
              <div id="responseStatus"></div>
       </div>
       <div class="modal-body">
              <div id="requestID"></div>
       </div>
       <div class="modal-footer">
              <button type="button" class="img-close btn-secondary-tt"
                     id="closeButtonID" data-dismiss="modal">
                     <liferay-ui:message key="Close" />
              </button>
       </div>
</div>