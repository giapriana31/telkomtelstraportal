<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="common.jsp" %>

<portlet:resourceURL id="getSlaDetails" var="getSlaDetails" >
</portlet:resourceURL> 

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/sites.css">
<div style="width:100%;height:75px;display:table;background:white;">		
	<div style="display:table-cell;vertical-align:middle;padding-left:20px; border-bottom-width:1px;border-bottom-color:#CCC;border-bottom-style: solid;">
		<img src="<%=request.getContextPath()%>/images/u91.png" />
		<font style="font-size:120%;vertical-align:middle;margin-left:1%;"><liferay-ui:message key="Dashboard"/> > </font>
		<font style="font-size:165%;vertical-align:middle;"><liferay-ui:message key="Availability"/></font>
		<input type="button" value=" <  <liferay-ui:message key="Back"/>" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" class="sla_back_button" style=" float:right;margin-right:10px; background-color: #555;    box-shadow: 0 0 0!important;border-radius: 5px;" />
	</div>
	<hr class="hr_style">
</div>	

<div id="mydiv" class="container-fluid tt-site-tab-container" style="margin:25px;">
<div style="background:white;">    
	<div id="set_height" style="position: relative; min-height:500px;border: solid 1px #CCCCCC;padding:2%;">
	<div style="font-size:150%;"><liferay-ui:message key="AVAILABILITY"/></div>
	
	<hr class="hr_style" style="margin-right: -2% ! important; margin-left: -2% ! important; margin-bottom: 0px;">
	
    <div class="first-column col" id="first-column"></div>
    <div class=" second-column col" id="second-column"></div>
    <div class=" third-column col" id="third-column"></div>
   <div class="fourth-column col" id="fourth-column"></div>
	</div>
</div>	
</div>

<script>

$(document).ready(function()
{
var response;
var cloudStatus;
	
$(".percentage_row").css("background-color","${networkAvailability.color}");
getCloudData();
	
 	if('${networkAvailability.avgAvail}'!='no data' || '${networkAvailability.customerId}'!='no data' )
 	{						
						
		document.getElementById("second-column").className="second-column col";					
		document.getElementById('second-column').innerHTML+='<div class="header_row_style"> <table width="100%" style="margin-top: 15px;"> <tr> <td align="left"  ><liferay-ui:message key="Networks"/></td> <td align="right" class="percentage_row">${networkAvailability.avgAvail}%</td> </tr> </table> </div> <hr class="hr_style">  </div>';
		
		var color = '${networkAvailability.color}'
		if(color.toLowerCase() == 'green')
		{
			document.getElementById('second-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#A3CF62;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Networks"/></td><td align="right">${networkAvailability.avgAvail}%</td></tr></table></div></a>';
		}
		if(color.toLowerCase()== 'red')
		{
			document.getElementById('second-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#E32212;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Networks"/></td><td align="right">${networkAvailability.avgAvail}%</td></tr></table></div></a>';
		}
		if(color.toLowerCase()== 'amber')
		{
			document.getElementById('second-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#FF9C00;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Networks"/></td><td align="right">${networkAvailability.avgAvail}%</td></tr></table></div></a>';
		}
	}
	else
	{
		document.getElementById("second-column").className="second-column col";					
		document.getElementById('second-column').innerHTML+='<div class="header_row_style"> <table width="100%" style="margin-top: 15px;"> <tr> <td align="left"  ><liferay-ui:message key="Networks"/></td> <td align="right" class="percentage_row">NA</td> </tr> </table> </div> <hr class="hr_style">  </div>';
		document.getElementById('second-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:gray;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Networks"/></td><td align="right">NA</td></tr></table></div></a>';
	}

function getCloudData()
{
	document.getElementById("first-column").classList.add("loading-animation");
	document.getElementById("second-column").classList.add("loading-animation");
	document.getElementById("third-column").classList.add("loading-animation");
	document.getElementById("fourth-column").classList.add("loading-animation");
	
	AUI().use('aui-io-request', function(A)
	{
		A.io.request("${getSlaDetails}", {	  
		   data: 
		   {
				userId : "<%=customerID%>",
		   },
	  		on: 
	  		{
	   			success: function() 
	   			{
					 response = this.get('responseData');							
					if(response.trim() != "no data"  )
					{
						var arr = response.split('#####');											
						document.getElementById("first-column").className="first-column col";					
						
						
						for (i = 0; i < arr.length; i++) 
						{ 
							if(i==0)
								document.getElementById('first-column').innerHTML+='<div class="header_row_style"><table width="100%" style=" margin-top: 15px;"><tr><td align="left"><liferay-ui:message key="Sites"/></td><td align="right" class="percentage_row">'+arr[i].split('@@@@@')[0]+'</td></tr></table></div><hr class="hr_style">';
							else
							{	
							  
								if(arr[i].split('@@@@@')[1] != "NULL")
								{
								
									if(parseInt(arr[i].split('@@@@@')[1]) == 0) 
										document.getElementById('first-column').innerHTML+='<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardsites")+"#basedOnSLATab"%>"><div class="extra_style" style="background-color:gray"><table width="100%"><tr><td align="left">'+arr[i].split('@@@@@')[0]+'</td><td align="right">NA</td></tr></table></div></a>';
									else
										document.getElementById('first-column').innerHTML+='<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardsites")+"#basedOnSLATab"%>"><div class="extra_style" style="background-color:'+arr[i].split('@@@@@')[2]+'"><table width="100%"><tr><td align="left">'+arr[i].split('@@@@@')[0]+'</td><td align="right">'+arr[i].split('@@@@@')[1]+'</td></tr></table></div></a>';
											
								}
							}
						}
					}
					else
					{
						document.getElementById("first-column").classList.remove("loading-animation");
						document.getElementById('first-column').innerHTML+='<div class="header_row_style"><table width="100%" style=" margin-top: 15px;"><tr><td align="left"><liferay-ui:message key="Sites"/></td><td align="right" class="percentage_row">NA</td></tr></table></div><hr class="hr_style">';
						document.getElementById('first-column').innerHTML+='<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardsites")+"#basedOnSLATab"%>"><div class="extra_style" style="background-color:gray"><table width="100%"><tr><td align="left">Gold Sites</td><td align="right">NA</td></tr></table></div></a>';
						document.getElementById('first-column').innerHTML+='<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardsites")+"#basedOnSLATab"%>"><div class="extra_style" style="background-color:gray"><table width="100%"><tr><td align="left">Silver Sites</td><td align="right">NA</td></tr></table></div></a>';
						document.getElementById('first-column').innerHTML+='<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardsites")+"#basedOnSLATab"%>"><div class="extra_style" style="background-color:gray"><table width="100%"><tr><td align="left">Bronze Sites</td><td align="right">NA</td></tr></table></div></a>';
					}
				checksData();
				getSaaSData();
				}
	  		}	
			});
		});

	var cloudColor='${IaaSColor}';
	
	if(cloudColor == 'grey' || cloudColor==null)
	{
	
		document.getElementById("third-column").className="third-column col";					
		document.getElementById('third-column').innerHTML+='<div class="header_row_style"> <table width="100%" style="margin-top: 15px;"> <tr> <td align="left"><liferay-ui:message key="Private Cloud"/></td> <td align="right" class="percentage_row">NA</td> </tr> </table> </div> <hr class="hr_style"> </div>';
		document.getElementById('third-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:gray;text-decoration:none !important;"><table width="100%"><tr><td align="left">NA</td><td align="right">NA</td></tr></table></div></a>';
		return;
	}
	
	document.getElementById("third-column").className="third-column col";					
	document.getElementById('third-column').innerHTML+='<div class="header_row_style"> <table width="100%" style="margin-top: 15px;"> <tr> <td align="left"><liferay-ui:message key="Private Cloud"/></td> <td align="right" class="percentage_row">${IaaSColorAverage}%</td> </tr> </table> </div> <hr class="hr_style"> </div>';

	if('${IaaSColor}' == 'green')
	{
		document.getElementById('third-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#A3CF62;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Private Cloud"/></td><td align="right">${IaaSColorAverage}%</td></tr></table></div></a>';
	}
	else if('${IaaSColor}'== 'red'){
		document.getElementById('third-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#E32212;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Private Cloud"/></td><td align="right">${IaaSColorAverage}%</td></tr></table></div></a>';
	}
	else if('${IaaSColor}'== 'amber')
	{
		document.getElementById('third-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#FF9C00;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Private Cloud"/></td><td align="right">${IaaSColorAverage}%</td></tr></table></div></a>';
	}
	else 
	{
		document.getElementById('third-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:gray;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="Private Cloud"/></td><td align="right">NA</td></tr></table></div></a>';
	}
 }
 

function checksData()
{
	var cloudColor='${IaaSColor}';
	var saasColor = '${SaaSColor}';
	if(response.trim() == "no data" && cloudColor =='grey' && '${networkAvailability.avgAvail}'=='no data' && saasColor=='grey')
	{
		document.getElementById("first-column").className="first-column col";										
		document.getElementById('first-column').innerHTML='<center style="font-size:150%;margin-top:5%;"><liferay-ui:message key="Data will be displayed from next month."/></center>';
		document.getElementById('first-column').style.width="100%";						
		document.getElementById("second-column").className="second-column col";		
		document.getElementById("third-column").className="third-column col";
		document.getElementById("fourth-column").className="fourth-column col";			
		$("#second-column").hide();
		$("#third-column").hide();
		$("fourth-column").hide();
	}
}

function getSaaSData()
{
	/*var jsonObject3 = '[{"name" : "ipscape","avail" : "99","color" : "Green"}, {"name" : "mandoe","avail" : "98","color" : "Amber"}, {"name" : "whispir","avail" : "95","color" : "Red"}]';*/
	
	var jsonvar = ${SaaSData};
	
	document.getElementById("fourth-column").className="fourth-column col";	
	document.getElementById("fourth-column").classList.remove("loading-animation");
		
	var totalAvail = '${SaaSAvailability}';
	
	if(totalAvail == 'NA')
	{
		document.getElementById('fourth-column').innerHTML+='<div class="header_row_style"><table width="100%" style=" margin-top: 15px;"><tr><td align="left"><liferay-ui:message key="SaaS"/></td><td align="right" class="percentage_row">NA</td></tr></table></div><hr class="hr_style">';
	}
	else
	{
		document.getElementById('fourth-column').innerHTML+='<div class="header_row_style"><table width="100%" style=" margin-top: 15px;"><tr><td align="left"><liferay-ui:message key="SaaS"/></td><td align="right" class="percentage_row">${SaaSAvailability}%</td></tr></table></div><hr class="hr_style">';
	}
	
	
	for(var i in jsonvar)
	{
		var name = jsonvar[i].name;
		var availability = jsonvar[i].avail;
		var color = jsonvar[i].color;
		
		/*alert("name :: " + name);
		alert("availability :: " + availability);
		alert("color :: " + color);*/
		
		if(color.toLowerCase() == 'green')
		{
			document.getElementById('fourth-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#A3CF62;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="'+name+'"/></td><td align="right">'+availability+'%</td></tr></table></div></a>';
		}
		if(color.toLowerCase()== 'red')
		{
			document.getElementById('fourth-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#E32212;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="'+name+'"/></td><td align="right">'+availability+'%</td></tr></table></div></a>';
		}
		if(color.toLowerCase()== 'amber')
		{
			document.getElementById('fourth-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:#FF9C00;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="'+name+'"/></td><td align="right">'+availability+'%</td></tr></table></div></a>';
		}
		if(color.toLowerCase()== 'grey')
		{
			document.getElementById('fourth-column').innerHTML+='<a href="javascript:void(0)"><div class="extra_style" style="background-color:gray;text-decoration:none !important;"><table width="100%"><tr><td align="left"><liferay-ui:message key="'+name+'"/></td><td align="right">NA</td></tr></table></div></a>';
		}
	}
}
});
</script>