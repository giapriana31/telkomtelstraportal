<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.tt.model.Notes"%>
<%@page import="java.util.List"%>
<%@page import="com.tt.model.Incident"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>

<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-modal.js"></script> 

<!-- <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->

<!-- <script  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> --> 


<script src="<%=request.getContextPath()%>/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">
<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/fnReloadAjax.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.dataTables.css">


<portlet:defineObjects />
<liferay-theme:defineObjects />


<portlet:resourceURL id="getTicketList" var="getTicketList"></portlet:resourceURL>
<portlet:resourceURL var="getServiceURL" id="getServiceURL"/>
<portlet:resourceURL var="getCategoryURL" id="getCategoryURL"/>
<portlet:resourceURL var="getDataURL" id="getDataURL"/>
<portlet:resourceURL var="getVMURL" id="getVMURL"/>
<portlet:actionURL var="viewListURL">
       <portlet:param name='action' value="tableFunction" />
</portlet:actionURL>
<portlet:actionURL name = "updateInc"  var = "updateTicket">
</portlet:actionURL>
<portlet:actionURL var="submitFormURL" name="handleLogIncident" />
<portlet:resourceURL var="submitRequestURL" id="submitRequestURL" />

<script type="text/javascript">
$(document).ready( function () {
       $('#myModal').css('display', 'none');
       $('#advSearchButton').on('click', function () {
              $('#_145_dockbar').css('z-index', 1039);
              $('#myModal').css('display', 'block');
            /*  $('#myModal').css('top', "20%"); */
             $('#myModal').modal('show');
                     
       });
       
       $('#myModal').on('shown', function ( event ) {
              $(this).css('display', 'block');
              $('#_145_dockbar').css('z-index', 1039);
                     
       });

       $('#myModal').on('hidden', function ( event ) {
              $(this).css('display', 'none');
              
              $('.modal-backdrop').remove();
              $('#_145_dockbar').css('z-index', 1041);
              
       });
});
</script>
<script type="text/javascript">

       $(document).ready(function() {
          
        //Hide the four drop downs V Block and VM initially:START
              
              $('#advSearchRowVM').hide();
              $('#advSearchRowSite').hide();
              $('#advSearchRowService').hide();

       //Hide the two drop downs V Block and VM initially:END
              $("#full_page").hide();
              
              $("#priority").addClass("TTHeaderSortableAscending");
              
              $("#ajaxform")[0].reset();
              $("#advancedajaxform")[0].reset();
              

                     var parameterReceived = getQueryString(window.location.href);
                     if(parameterReceived!=-1){
                           var vmSpecifics = Base64.decode(parameterReceived);
						   if(vmSpecifics.split("&")[0]=="saasToInc"){
								service=vmSpecifics.split("&")[1];
							}
							else{
							   vmIdentifier = vmSpecifics.split("&")[0];
							   vmIdentifierLabel = vmSpecifics.split("&")[1];
						   }
                           priority='';
                           impact='';
                           
                     }                      
                     
              procureTicketList();
       });

       var getTicketListURL = "${getTicketList}";

       var service = '';
       var priority = '';
       var impact = '';
       var siteName = '${siteNameCheck}';
       var assignedTo = '';
       var dateFrom = '';
       var dateTo = '';
       var searchQuery = '';
       var ciname='';
          var productType='';
          var vmIdentifier='';
          var vmIdentifierLabel='';
       var siteNameLabel='${siteNameLabelCheck}';
       var isAdvancedSearch=0;
       var onLoadIncListFlag=0;
       
       /* if(siteName==''){
              priority='P1&P2&P3&P4&';
       } */
          

       var sortDirection = 'asc';
       var sortCriterion = 'priority';
       var pageNumber = 1;
       var lastPage;
       
       
     //Hide or show drop downs based on product type and display data in drop downs respectively
       function check(elem){
              
              $('#VMID').prop('selectedIndex',0);
              
              var dropdown = document.getElementById("productTypeID");
              var currentValue = dropdown.options[dropdown.selectedIndex].value;
              
              if(currentValue == "allServices" || currentValue == "Whispir" || currentValue == "Mandoe" || currentValue == "IPScape" || currentValue == "SaaS"){
                     
                     $('#advSearchRowVM').hide();
                     $('#advSearchRowSite').hide();
                     $('#advSearchRowService').hide();
              }
              else if(currentValue == "Private Cloud"){
                     
                     $('#advSearchRowVM').show();
                     $('#advSearchRowSite').hide();
                     $('#advSearchRowService').hide();
              }
              else{
                     
                     $('#advSearchRowVM').hide();
                     $('#advSearchRowSite').show();
                     $('#advSearchRowService').show();
              }

       
              
              var category="";
              var selectedValue = dropdown.options[dropdown.selectedIndex].value;
              var getCategoryURL = "${getCategoryURL}"
              var jsonProductType = {"selectedProductType" : selectedValue};
              var dataJson={selectedProductTypeJson : JSON.stringify(jsonProductType)};
              document.getElementById("categoryID").options.length = 1;
       
              $.ajax({
                     url: getCategoryURL,
               type: "POST",
               data: dataJson,
               success: function(response){
                     category = response;
                     category = category.split(",");
                           for(var i=0; i<category.length; i++){
                            $('#categoryID').append($('<option>').text(category[i]).val(category[i]));
                     }
                           if(currentValue == "Private Cloud"){
                                  getDataVBlock();
               } 
                           
               },
               error: function(e){
                     }
               
               
              });
              
              
       }
       
   //Get the values for V block drop downs
       function getDataVBlock(){

              var vBlock="";
              var getDataURL = "${getDataURL}"
              document.getElementById("VBlockID").options.length = 1;
              $.ajax({
                     url: getDataURL,
               type: "POST",
               dataType: "text",
               success: function(response){
                     vBlock = response.split("&&&&");
                     
                     var len = vBlock.length;
                     var x=1;
                     var reslen = len-x;
                     for(var i=0; i<reslen;i++){
                           
                            $('#VBlockID').append($('<option>').text(vBlock[i].split('####')[1]).val(vBlock[i].split('####')[0]));
                     }                          
                           
               },
               error: function(e){
                     }

              });
              
       }
       
       //get Vvales for VM drop down
       function getVM(elem){
              var vm = "";
              var dropdown = document.getElementById("VBlockID");
              var selectedValue = dropdown.options[dropdown.selectedIndex].value;
              var getVMURL = "${getVMURL}"
                     var jsonVBlock = {"selectedVBlock" : selectedValue};
                     var dataJson={selectedVBlockJson : JSON.stringify(jsonVBlock)};
                     
                     $.ajax({
                           url: getVMURL,
                      type: "POST",
                      data: dataJson,
                      success: function(response){
                            vm = response.split("&&&&");
                            var len = vm.length;
                     var reslen = len-1;
                     for(var i=0; i<reslen;i++){
                            
                            $('#VMID').append($('<option>').text(response.split("&&&&")[i].split('####')[1]).val(response.split("&&&&")[i].split('####')[1]));
                     } 
                                  
                      },
                      error: function(e){
                           }
                      
                      
                     });
       }
       
       function generateTicketList(obj) {
              
              pageNumber=1;
              isAdvancedSearch=0;
              $("#ajaxform")[0].reset();
              $("#advancedajaxform")[0].reset();
              
              service = '';
              priority = '';
              impact = '';
              siteName = '';
              assignedTo = '';
              dateFrom = '';
              dateTo = '';
              searchQuery = '';
              ciname = '';
              siteNameLabel='';
                       productType='';
                       vmIdentifier='';
                       vmIdentifierLabel='';
              
              service = obj.getAttribute("id");
              
              $("a").removeClass("TTGridSelectedCount");
              
              $(obj).addClass("TTGridSelectedCount");

              priority = obj.getAttribute("aria-label");

              impact = obj.getAttribute("class");
              

                     
                       
              procureTicketList();

              return false;

       }

       function filterBoxFunction(obj) {
              pageNumber=1;
              
              $("a").removeClass("TTGridSelectedCount");
              $("#advancedajaxform")[0].reset();
              var idFilter = obj.getAttribute("id");
              var classFilter = obj.getAttribute("class");
              
              if(classFilter == "vmIdentifierClass")
                     {
                                  $("#filterService a[id='"+idFilter+"']").remove();
                                  
                                  vmIdentifier='';
                                  vmIdentifierLabel='';
                     }
              if(classFilter == "siteNameLabelClass")
                     {
                                  $("#filterService a[id='"+idFilter+"']").remove();
                                  
                                  siteNameLabel='';
                                  siteName='';
                     }
                           

              if(classFilter == "service")
                     {
                                  $("#filterService a[id='"+idFilter+"']").remove();
                                  
                                  service='';
                     }
              
              if(classFilter == "priority")
              {
                     if(idFilter == "P1")
                           {
                                  $('#P1').remove();
                                  priority=priority.replace("P1&", "");
                           }
                     else if(idFilter == "P2")
                     {
                           $('#P2').remove();
                           priority=priority.replace("P2&", "");
                     }
                     else if(idFilter == "P3")
                     {
                           $('#P3').remove();
                           priority=priority.replace("P3&", "");
                     }
                     else if(idFilter == "P4")
                     {
                           $('#P4').remove();
                           priority=priority.replace("P4&", "");
                     }/* 
					 else if(idFilter == "P5")
                     {
                           $('#P5').remove();
                           priority=priority.replace("P5&", "");
                     } */
              }
              
              /* if(classFilter == "impact"){
                     if(idFilter == "With Impacts")
                     {
                           $('#With Impacts').remove();
                           impact=impact.replace("With Impacts&", "");
                     }
                     else if(idFilter == "At Risk")
                     {
                           $('#At Risk').remove();
                           impact=impact.replace("At Risk&", "");
                     }
              } */
              
              procureTicketList();
              return false;
       }

       function changeOrder(obj) {
              var tempSortCriterion = obj.getAttribute("id");
              if (tempSortCriterion == sortCriterion && sortDirection == 'asc') {
                     sortDirection = 'desc';
                     sortCriterion = tempSortCriterion;
              } else {
                     sortDirection = 'asc';
                     sortCriterion = tempSortCriterion;
              }
              
              $("a").removeClass("TTHeaderSortableAscending");
              $("a").removeClass("TTHeaderSortableDescending");
              
              if(sortDirection=='asc'){
                     $(obj).addClass("TTHeaderSortableAscending");
              }
              else{
                     $(obj).addClass("TTHeaderSortableDescending");
              }
              procureTicketList();
              return false;
       }

       /* $(function() {

              $('#logIncForm').submit(function(e) {
                     e.preventDefault();

              });
       }); */

       function getService(dropdown) {
             var selectedValue = dropdown.options[dropdown.selectedIndex].value;
             /* alert('Selected Value + ' + selectedValue); */
             var getServiceURL = "${getServiceURL}"
              var jsonSiteID = {"selectedSiteID" : selectedValue};
              var dataJson={selectedSiteIDJson : JSON.stringify(jsonSiteID)};
              $.ajax({
                     url: getServiceURL,
            type: "POST",
            data: dataJson,
            success: function(response){
              $('#serviceName option[value!="allServices"]').remove();
              var x=1;
              var arr = response.split("#####");
              var len = arr.length;
              var reslen = len-x;
              for(var i=0; i<reslen;i++){
                    
                     $('#serviceName').append($('<option>').text(response.split("#####")[i].split('@@@@@')[1]).val(response.split("#####")[i].split('@@@@@')[1]));
              }
              $('#serviceName option[value=""]').remove();
          },
                     error: function(xhr){
                            
               alert('Error' + xhr);
           }
              });
       }
       
       function getServiceLI() {
              var selectedValue = $('#siteIDAuto').val();
              var getServiceURL = "${getServiceURL}"
              var jsonSiteID = {"selectedSiteID" : selectedValue};
              var dataJson={selectedSiteIDJson : JSON.stringify(jsonSiteID)};
              $.ajax({
                     url: getServiceURL,
            type: "POST",
            data: dataJson,
            success: function(response){
              $('#serviceNameSelect option[value!="0"]').remove();

              var x=1;
              var arr = response.split("#####");
              var len = arr.length;
              var reslen = len-x;
              for(var i=0; i<reslen;i++){
                     
                     $('#serviceNameSelect').append($('<option>').text(response.split("#####")[i].split('@@@@@')[1]).val(response.split("#####")[i].split('@@@@@')[1]));
              }
              $('#serviceNameSelect option[value=""]').remove();
          },
                     error: function(xhr){
                           
               alert('Error' + xhr);
           }
              });
       }
       
       String.prototype.ReplaceAll =function(stringToFind,stringToReplace){
              
           var temp = this;
       
           var index = temp.indexOf(stringToFind);
       
               while(index != -1){
       
                   temp = temp.replace(stringToFind,stringToReplace);
       
                   index = temp.indexOf(stringToFind);
       
               }
       
               return temp;
       
           }
       
       function toGetTicketDetails(obj)
       {
              var incidentIdForDetails=obj.getAttribute("id");
             $("#ticketgridbody").hide();
             $("#ticketlistbody").hide();
             $("#full_page").show();
             
              
              return false;
       }
       
       function changePage(obj){
              var pageInput=obj.getAttribute("id");
              if(pageInput=="previousPage"){
                     if(!(pageNumber==1)){
                           pageNumber--;
                     }
              }
              else if(pageInput=="nextPage"){
                     if(!(pageNumber==lastPage)){
                           pageNumber++;
                     }
              }
              else{
                     pageNumber=parseInt(pageInput);
              }
              procureTicketList();
       }     
        
       //incidents table population and rendering at first time.
       function loadTktListingDataTable(dataSet) {     
       
              onLoadIncListFlag++;
              //DataTables initialisation
              $('#ttticketListingTable').dataTable({
              
                     "processing" : false,
                     "serverSide" : false,
                     'bInfo' : false,
                     "bPaginate": false,
                     "bSortable" : false,
                     "bSort": false, 
            "aaSorting": [[0]], 
                     'bFilter' : false,
                     'bDestroy' : true,
                     'bLengthChange' : false,
                     'aoColumns':[
                           {"sClass": "tt-col-incident", "aTargets": [0]},
                           {"sClass": "tt-col-subject", "aTargets": [1]},
                           {"sClass": "tt-col-priority", "aTargets": [2]},
                           {"sClass": "tt-col-state", "aTargets": [3]},
                           {"sClass": "tt-col-assignee", "aTargets": [4]},
                           {"sClass": "tt-col-lastupdated", "aTargets": [5]},
                           {"sClass": "tt-col-status", "aTargets": [6]},
                           {"sClass": "tt-col-category", "aTargets": [7]},
                           {"sClass": "tt-col-dueby", "aTargets": [8]},
                           {"sClass": "tt-col-category", "aTargets": [9]},
                           {"sClass": "tt-col-dueby", "aTargets": [10]},
                           {"sClass": "tt-col-flagHeader", "aTargets": [11]},
                     ],
                     "data": dataSet      
              });    
       };
       
       function getPaginationHtml(totalPages,pageNumber){
              var pageTabDisplay='';
                                  if(totalPages<=6){
                                         for(var i=1;i<=totalPages;i++){
                                         pageTabDisplay+= '<a href="javascript:void(0)" class="tttabSelectorPages" onclick="changePage(this)" id="'+i+'">'+i+'</a>&nbsp;';
                                         }
                                  }
                                  else{
                                  
                                         var pageTabInner='';
                                         var continuityHtml=' .. ';
                                         
                                         var startPageHtml='<a href="javascript:void(0)" class="tttabSelectorPages" onclick="changePage(this)" id="1">1</a>&nbsp;';
                                         var endPageHtml='<a href="javascript:void(0)" class="tttabSelectorPages" onclick="changePage(this)" id="'+totalPages+'">'+totalPages+'</a>&nbsp;';
                                         
                                         if(pageNumber<5){
                                                for(var i=1;i<=5;i++){
                                                pageTabInner+= '<a href="javascript:void(0)" class="tttabSelectorPages" onclick="changePage(this)" id="'+i+'">'+i+'</a>&nbsp;';
                                                }
                                                pageTabDisplay=pageTabInner+continuityHtml+endPageHtml;
                                         }
                                         
                                         else if((totalPages-pageNumber)<4){
                                                for(var i=(totalPages-4);i<=totalPages;i++){
                                                pageTabInner+= '<a href="javascript:void(0)" class="tttabSelectorPages" onclick="changePage(this)" id="'+i+'">'+i+'</a>&nbsp;';
                                                }
                                                pageTabDisplay=startPageHtml+continuityHtml+pageTabInner;
                                         }
                                         
                                         else{
                                                for(var i=(pageNumber-2);i<=(pageNumber+2);i++){
                                                pageTabInner+= '<a href="javascript:void(0)" class="tttabSelectorPages" onclick="changePage(this)" id="'+i+'">'+i+'</a>&nbsp;';
                                                }
                                                pageTabDisplay=startPageHtml+continuityHtml+pageTabInner+continuityHtml+endPageHtml;
                                         }
                                  }
                     return pageTabDisplay;
                                  
       }
       
       function procureTicketList() {
              
              var serviceFilterHtml='';
              var impactFilterHtml='';
              var priorityFilterHtml='';
              var siteNameFilterHmtl='';
              var vmIdentifierFilterHmtl='';
              
              
              if(service=="allServices")
                     {
                           serviceFilterHtml='';
                     }
              else
                     {
                           serviceFilterHtml='<a href="javascript:void(0);" style="display:inline-block;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc" id="'
                           + service
                           + '" class="service" onclick="filterBoxFunction(this)">'
                           + service
                           + '&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
                     }
              if(service == '')
                     {
                     serviceFilterHtml='';
                     }
              
              var priorityList=priority.split("&");
              
              for(var i=0;i<priorityList.length-1;i++)
                     {
                           priorityFilterHtml+= '<a href="javascript:void(0);" style="display:inline-block;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc" id="'
                                  + priorityList[i]
                                  + '" class="priority" onclick="filterBoxFunction(this)">'
                                  + priorityList[i]
                                  + '&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
                     }
              
              /* var impactList=impact.split("&");
              
              for(var i=0;i<impactList.length-1;i++)
              {
                     if(impactList[i]=='With Impacts'){
                           impactFilterHtml += '<a href="javascript:void(0);" style="display:inline-block;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc" id="'
                                  + impactList[i]
                                  + '" class="impact" onclick="filterBoxFunction(this)"><liferay-ui:message key="With Impacts"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
                           }
                     else{
                           impactFilterHtml += '<a href="javascript:void(0);" style="display:inline-block;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc" id="'
                                  + impactList[i]
                                  + '" class="impact" onclick="filterBoxFunction(this)"><liferay-ui:message key="At Risk"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
                                  
                           }
              } */
              
              
              
              if(siteNameLabel == '')
                     {
                     siteNameFilterHmtl='';
                     }
                     
              else{
                     siteNameFilterHmtl='<a href="javascript:void(0);" style="display:inline-block;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc" id="'
                           + siteNameLabel
                           + '" class="siteNameLabelClass" onclick="filterBoxFunction(this)">'
                           + siteNameLabel
                           + '&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
              }
                       
                       if(vmIdentifier == '')
                     {
                     vmIdentifierFilterHmtl='';
                     }
                     
              else{
                     vmIdentifierFilterHmtl='<a href="javascript:void(0);" style="display:inline-block;background:#f1f1f1;text-decoration:none;color:#333333;padding:1%;border:solid 1px #ccc" id="'
                           + vmIdentifier
                           + '" class="vmIdentifierClass" onclick="filterBoxFunction(this)">'
                           + vmIdentifierLabel
                           + '&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/u8.png"></a>&nbsp;';
              }
              
              
              var resultantFilterHtml=serviceFilterHtml+impactFilterHtml+priorityFilterHtml+siteNameFilterHmtl+vmIdentifierFilterHmtl;
              
              $('#filterss').html("");
              $('#filterss').append('<div id="filterService">'+resultantFilterHtml+'</div>');
              
              if(isAdvancedSearch==1){
                     $('#filterss').html("Advanced Filters applied");
              }
              
              else if(resultantFilterHtml=='')
                     {
                     $('#filterss').html("No filters applied");
                     }
              
              var pageNumberString = pageNumber.toString();

              var jsonObj = {
                     "service" : service,
                     "priority" : priority,
                     "impact" : impact,
                    "siteName" : siteName,
                     "ciname" : ciname,
                     "dateFrom" : dateFrom,
                     "dateTo" : dateTo,
                     "assignedTo" : assignedTo,
                     "searchQuery" : searchQuery,
                     "sortCriterion" : sortCriterion,
                     "sortDirection" : sortDirection,
                     "pageNumber" : pageNumberString,
                     "sortCriterion" : sortCriterion,
                     "sortDirection" : sortDirection,
                                  "productType" : productType,
                                  "vmIdentifier" : vmIdentifier
              };
              
              

              var ticketData = {
                     ticketFilterIdentifier : JSON.stringify(jsonObj)
              };

              $.ajax({
                     type : "POST",
                     url : getTicketListURL,
                     dataType : 'json',
                     data : ticketData,
                     success : function(data) {
                           $('#myModal').modal('hide');
                           $('.modal-backdrop').remove();
                           $('#ttincidents').html("");
                           
                           if(data.length==1){
                                  var nullDataIdentifier= data[0].noDataIdentifier;
                                  if(nullDataIdentifier=="nullReturned"){
                                         $('#ttincidents').append('<tr><td colspan="12" style="background:#eee;height:80px;text-align:left"><liferay-ui:message key="No results found"/></td></tr>');
                                  }
                           }
                           else{
                           var dataSet = new Array();
                           var len = (data.length)-1;
                           for (var i = 0; i < len; i++) {
                                  var incidentIdTbl= data[i].incidentId;
                                  var shortDescriptionTbl = data[i].shortDescription;
                                  var longDescriptionTbl = data[i].longDescription;
                                  var priorityTbl = data[i].priority;
                                  var stateTbl = data[i].state;
                                  var assigneeTbl = data[i].assignee;
                                  var lastUpdateTbl = data[i].lastUpdated;
                                  var statusTbl = data[i].status;
                                  var dueByTbl = data[i].dueBy;
                                  var showEscalationFlagTbl = data[i].showEscalationFlag;
                                  var siteName = data[i].siteName;
                                  var siteServiceTier = data[i].siteServiceTier;
                                  var openDateTime = data[i].openDateTime;
                                  var categoryTbl = data[i].category;
                                  var resolutionDateTime = data[i].resolutionDateTime;
                                  var serviceIdTbl = data[i].serviceId;
                                  var bussinesDurationTbl = data[i].bussinesDuration;
                                  var headerTab = data[i].headerTab;
                                  
                                  
                                  var escalationFlagHtml='';
                                   
                                  if(showEscalationFlagTbl=="true"){
                                         escalationFlagHtml='<img title="Impacted" src="<%=request.getContextPath() %>/images/u1337.png"/>';
                                  }
                                         
                                  var updateTicket = "${updateTicket}";                                
                                  updateTicket = updateTicket+"&incident=" + incidentIdTbl;
                                	                      
                                  var dataSetRow = [
                                         /* '<a href="'+updateTicket+'" style="color:#e32212;text-decoration:none;display:inline-block;width:100%;text-align:center;position: relative;right: 5px;">'+incidentIdTbl+'</a>',
                                         '<span class="tt-shortdes">'+shortDescriptionTbl+'</span><br><span     style="color:#555555;font-size:14px">'+longDescriptionTbl+'</span>',
                                         priorityTbl,
                                         stateTbl,
                                         assigneeTbl,
                                         lastUpdateTbl.substring(0,10)+'<br>'+lastUpdateTbl.substring(11,lastUpdateTbl.length),
                                         statusTbl,
                                         categoryTbl,
                                         dueByTbl,
                                         escalationFlagHtml */
                                         '<a href="'+updateTicket+'" style="color:#e32212;text-decoration:none;display:inline-block;width:100%;text-align:center;position: relative;right: 5px;">'+incidentIdTbl+'</a>',
                                         siteName,
                                         serviceIdTbl,
                                         siteServiceTier,
                                         priorityTbl,
                                         '<span class="tt-shortdes">'+shortDescriptionTbl+'</span><br><span     style="color:#555555;font-size:14px">'+longDescriptionTbl+'</span>',
                                         openDateTime.substring(0,10)+'<br>'+openDateTime.substring(11,openDateTime.length),
                                         lastUpdateTbl.substring(0,10)+'<br>'+lastUpdateTbl.substring(11,lastUpdateTbl.length),
                                         resolutionDateTime.substring(0,10)+'<br>'+resolutionDateTime.substring(11,resolutionDateTime.length),
                                         bussinesDurationTbl,
                                         statusTbl,
                                         escalationFlagHtml
                                  ];
                                  
                                  dataSet.push(dataSetRow);                              
                                  
                           }
                                  if(onLoadIncListFlag==0){
                                         loadTktListingDataTable(dataSet);
                                  }
                                  else{
                                         var oTable= $('#ttticketListingTable').dataTable();
                                         oTable.fnClearTable();
                                         oTable.fnAddData(dataSet);
                                         oTable.fnDraw();
                                  }
                                  
                                  var temp= data.length-1;                               
                                  var actualCount = parseInt(data[temp].totalCount);                                 
                            
                                  var countTbl = len;
                                  var startCount=0;
                                  var endCount=0;
                                  if(countTbl>=1){
                                         startCount=((pageNumber*10)-10)+1;
                                  }
                                  if(countTbl<=10 && countTbl>=1){
                                         endCount=startCount+countTbl-1;
                                  }
                                  
                           
                                  var displayingPageRow='<liferay-ui:message key="Displaying"/>&nbsp;'+startCount+'-'+endCount+'&nbsp;<liferay-ui:message key="of"/>&nbsp;'+actualCount;
                                  var pageTabDisplay='';
                                  var totalPages=Math.floor(actualCount/10);
                                  
                                  if(actualCount%10 != 0){
                                         totalPages++;
                                  }
                     
                                  pageTabDisplay=getPaginationHtml(totalPages,pageNumber);
                                  
                                  /* var tablePaginationRow='<tr style="background:#eee;height:80px;"><td colspan="2" style="text-align:left">'+displayingPageRow+'</td><td colspan="7" style="text-align:right"><a href="javascript:void(0)" class="tttabSelectorVariant" id="previousPage" onclick="changePage(this)">&lt;&nbsp;Previous</a>&nbsp;'+pageTabDisplay+'<a href="javascript:void(0)" id="nextPage" class="tttabSelectorVariant" onclick="changePage(this)">Next&nbsp;&gt;</a></td></tr>';
                           
                                  $('#ttincidents').append(tablePaginationRow); */
                                                                     
                                  if(actualCount<10)
                                         {
                                         lastPage=1;
                                         }
                                  else if((actualCount%10)!=0)
                                         {
                                         lastPage=(Math.floor(actualCount/10))+1;
                                         }
                                  else if((actualCount%10)==0)
                                  {
                                  lastPage=(countTbl/10);
                                         }
                                  
                                  var disabledHtmlPreviousPage='';
                                  var disabledHtmlNextPage='';
                                  
                                  if(pageNumber==lastPage){
                                         disabledHtmlNextPage='disabled';
                                   }
                                  if(pageNumber==1){
                                         disabledHtmlPreviousPage='disabled';
                                  }
                                  
                                  var tablePaginationRow= '<tr><td colspan="12"><div class="container-fluid"><div class="row-fluid"><div class="span6">' + displayingPageRow + '</div>       <div class="span6" id="ttPaginationDiv"><a href="javascript:void(0)" class="tttabSelectorVariant '+disabledHtmlPreviousPage+'" id="previousPage" onclick="changePage(this)">&lt;&nbsp;<liferay-ui:message key="Previous"/></a>&nbsp;'+pageTabDisplay+'<a href="javascript:void(0)" id="nextPage" class="tttabSelectorVariant '+disabledHtmlNextPage+'" onclick="changePage(this)"><liferay-ui:message key="Next"/>&nbsp;&gt;</a></div></div></div></td></tr>';

                                  $('#ttincidents').append(tablePaginationRow);
                                  $("a").removeClass("selectedPageTab");
                                  $("a[id='"+pageNumber+"']").addClass("selectedPageTab");
                                  
                           
                     }
                     },
                     error : function(data) {
                           $('#myModal').modal('hide');
                           $('.modal-backdrop').remove();
                           $('#ttincidents').html("");
                           $('#ttincidents').append(row);
                     }

              });
              return false;
       }
       
       $(function() {
              $(".datepicker").datepicker();
       });

       $(function() {

              $('#ajaxform').submit(
                           function(e) {
                                  e.preventDefault();
                                  searchQuery = '';
                                  pageNumber = 1;
                                  
                                  var str = $('#ajaxform').serialize();

                                  var pos = str.indexOf('=');
                                  searchQuery = searchQuery.concat(str.substring(pos + 1,
                                                str.length));
                                  
                                  $("a").removeClass("TTGridSelectedCount");
                                  procureTicketList();
                                  

                           });
       });

       $(function() {

              $('#advancedajaxform').submit(
                                         function(e) {
                                                e.preventDefault();
                                                
                                                $("#ajaxform")[0].reset();
                                                
                                                service = '';
                                                priority = '';
                                                impact = '';
                                                siteName = '';
                                                assignedTo = '';
                                                dateFrom = '';
                                                dateTo = '';
                                                searchQuery = '';
                                                pageNumber = 1;
                                                ciname='';
                                                vmIdentifier = '';
                                                                                  productType=''; 

                                                var str = $('#advancedajaxform').serialize();
                                                
                                                var keyvaluelist = str.split("&");

                                                for (var i = 0; i < keyvaluelist.length; i++) {

                                                       if (keyvaluelist[i].indexOf('priority') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              priority = priority.concat(extract);
                                                              priority = priority.concat("&");

                                                       } else if (keyvaluelist[i].indexOf('impact') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              impact = impact.concat(extract);
                                                              impact = impact.concat("&");

                                                       } else if (keyvaluelist[i].indexOf('siteName') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              siteName = siteName.concat(extract);
                                                              
                                                       } else if (keyvaluelist[i]
                                                                    .indexOf('serviceName') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              ciname = ciname.concat(extract);
                                                       } else if (keyvaluelist[i].indexOf('dateFrom') > -1) {

                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              var rawDate = extract;
                                                              var rawDateList = rawDate.split('%2F');
                                                              for (var c = 0; c < rawDateList.length; c++) {
                                                                     dateFrom = dateFrom
                                                                                  .concat(rawDateList[c]);
                                                                     if(c<2 && rawDateList.length==3){
                                                                     dateFrom = dateFrom
                                                                                  .concat("/");
                                                                                  }
                                                              }
                                                       } else if (keyvaluelist[i].indexOf('dateTo') > -1) {

                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              var rawDate = extract;
                                                              var rawDateList = rawDate.split('%2F');
                                                              for (var c = 0; c < rawDateList.length; c++) {
                                                                     dateTo = dateTo.concat(rawDateList[c]);
                                                                     if(c<2 && rawDateList.length==3){
                                                                     dateTo = dateTo
                                                                                  .concat("/");
                                                                                  }
                                                              }
                                                       } else if (keyvaluelist[i]
                                                                     .indexOf('assignedTo') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              assignedTo = assignedTo.concat(extract);
                                                       } else if (keyvaluelist[i]
                                                                     .indexOf('advancedSearchQuery') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              searchQuery = searchQuery.concat(extract);
                                                       } else if (keyvaluelist[i]
                                                                     .indexOf('productType') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              productType = productType.concat(extract);
                                                       } else if (keyvaluelist[i]
                                                                     .indexOf('vm') > -1) {
                                                              var pos = keyvaluelist[i].indexOf('=');

                                                              var extract = keyvaluelist[i].substring(
                                                                           pos + 1, keyvaluelist[i].length);
                                                              vmIdentifier = vmIdentifier.concat(extract);
                                                       }

                                                }
                                                
                                                if(priority.indexOf('All') > -1)
                                                       {
                                                              priority='P1&P2&P3&P4&P5&';
                                                       }
                                                if(vmIdentifier.indexOf('allVM') > -1)
                                                       {
                                                              vmIdentifier='';
                                                       }
                                                if(service.indexOf('allServices') > -1)
                                                {
                                                       service='';
                                                }
                                                if(siteName.indexOf('allServices') > -1)
                                                {
                                                       siteName='';
                                                }
                                                
                                                
                                                impact=impact.ReplaceAll('+',' ');
                                                searchQuery=searchQuery.ReplaceAll('+',' ');
                                                assignedTo=assignedTo.ReplaceAll('+',' ');
                                                assignedTo=assignedTo.ReplaceAll('%40','@');
                                                ciname=ciname.ReplaceAll('+',' ');
                                                 productType=productType.ReplaceAll('+',' ');

                                                
                                                isAdvancedSearch=1;
                                                
                                                $("a").removeClass("TTGridSelectedCount");
                                                procureTicketList();
                                                
                                         });
       });

       /* $("#ajaxform").submit(); */
</script>

<style>
@media (min-width: 601px) and (max-width: 885px){
#ticketgridbody{
                     display: none;
              }
}

@media (min-width: 320px) and (max-width: 600px){

              .tt-col-subject{     
                     display: none;
              }
              
              .tt-col-state{
                     display: none;
              }
              
              .tt-col-assignee{
                     display: none;
              }
              .tt-col-lastupdated{
                     display: none;
              }
              .tt-col-flagHeader{
                     display: none;
              }
              
              #ticketgridbody{
                     display: none;
              }
              
              .tt-col-status{
                     display: none;
              }
              .tt-col-category{
                     display: none;
              }
              .tt-col-priority{
                     text-align: center !important;
              }
              .tt-col-incident{
                     padding-left: 5px !important;
              }
              
              .search-textbox{
                     width: 68% !important;
              }
              #advSearchButton{
                     display:none;
              }
}

@media (min-width: 320px) and (max-width: 970px){
       #advSearchButton{
                     display:none;
              }
}

@media (min-width: 320px) and (max-width: 1240px){
       .tt-col-state{
              display: none;
       }
       .tt-col-assignee{
              display: none;
       }
       .tt-col-lastupdated{
              display: none;
       }
       .tt-col-flagHeader{
              display: none;
       }
       

}
</style>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8;FF=3;OtherUA=4" />
</head>
<body>
<div class="container-fluid" id="ticketlistbody">
       <div class="row-fluid">
              <div class="span12 tt-incident-list-name"><liferay-ui:message key="Incidents Listing"/></div>
       </div>
       <div class="row-fluid">
              <div class="span6">
                     <span class="tt-filter-text"><liferay-ui:message key="Filters:"/>&nbsp;</span>
                     <div id="filterss" style="display: inline"><liferay-ui:message key="No filters applied"/></div>
              </div>
              <div class="span6 tt-search-filter">
                     <div>
                           <form method="post" name="ajaxform" id="ajaxform">
                                  <input id="u981_input" type="text" value="" name="normalSearchQuery" class="search-textbox">
                                  <button type="submit" id="normalSearch" class="search"></button>
                                  <button type="button" id="advSearchButton" class="" data-toggle="modal"></button>
                           </form>
                     </div>
              </div>
       </div>
       <div class="row-fluid">
              <div class="span12">
                     <table id="ttticketListingTable">
                           <thead>
                                  <tr style="background:#d4d4d4; height:50px;">
                                         <%-- <th class="incIDHeader"><a href="javascript:void(0)" onclick="changeOrder(this)" 
                                                id="incidentid" class="ttticketlistingheaders"><liferay-ui:message key="Incident Id" /></a></th>
                                         <th class="subHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="summary" class="ttticketlistingheaders"><liferay-ui:message key="Subject" /></a></th>
                                         <th class="priorityHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="priority" class="ttticketlistingheaders"><liferay-ui:message key="Priority" /></a></th>
                                         <th class="stateHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="customerimpacted" class="ttticketlistingheaders"><liferay-ui:message key="State" /></a></th>
                                         <th class="assigneeHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="userid" class="ttticketlistingheaders"><liferay-ui:message key="Assignee" /></a></th>
                                         <th class="lUpdHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="lastupdated" class="ttticketlistingheaders"><liferay-ui:message key="Last Updated" /></a></th>
                                         <th class="statusHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="incidentstatus" class="ttticketlistingheaders"><liferay-ui:message key="Status" /></a></th>
                                         <th class="statusHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="category" class="ttticketlistingheaders"><liferay-ui:message key="Product Type" /></a></th>
                                         <th class="dueByHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="serviceleveltarget" class="ttticketlistingheaders"><liferay-ui:message key="Due By" /></a></th>
                                         <th class="flagHeader"></th> --%>
                                         <th class="incIDHeader"><a href="javascript:void(0)" onclick="changeOrder(this)" 
                                                id="incidentid" class="ttticketlistingheaders"><liferay-ui:message key="Incident Id" /></a></th>
                                         <th class="subHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="summary" class="ttticketlistingheaders"><liferay-ui:message key="Site Name" /></a></th>
                                         <th class="priorityHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="priority" class="ttticketlistingheaders"><liferay-ui:message key="SID" /></a></th>
                                         <th class="stateHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="customerimpacted" class="ttticketlistingheaders"><liferay-ui:message key="Site Service Tiers" /></a></th>
                                         <th class="assigneeHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="userid" class="ttticketlistingheaders"><liferay-ui:message key="Priority" /></a></th>
                                         <th class="lUpdHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="lastupdated" class="ttticketlistingheaders"><liferay-ui:message key="Summary of Incident" /></a></th>
                                         <th class="statusHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="incidentstatus" class="ttticketlistingheaders"><liferay-ui:message key="Incident Start" /></a></th>
                                         <th class="statusHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="category" class="ttticketlistingheaders"><liferay-ui:message key="Last Updated" /></a></th>
                                         <th class="statusHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="category" class="ttticketlistingheaders"><liferay-ui:message key="Incident Resolved" /></a></th>
                                         <th class="statusHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="category" class="ttticketlistingheaders"><liferay-ui:message key="Contracted Hours of Outage" /></a></th>
                                         <th class="dueByHeader"><a href="javascript:void(0)" onclick="changeOrder(this)"
                                                id="serviceleveltarget" class="ttticketlistingheaders"><liferay-ui:message key="Ticket Status" /></a></th>
                                         <th class="flagHeader"></th>
                                  </tr>
                           </thead>
                           <tbody id="ttincidents"></tbody>
                     </table>
                     <div id="ticketlistdiv">
                     </div>
              </div>
       </div>
       
              <div class="modal fade" id="myModal" role="dialog"
              data-backdrop="static" data-keyboard="false">
              <div class="modal-dialog modal-lg">
                     <!-- Modal content-->
                     <div class="modal-dialog">

                           <!-- Modal content-->
                           <div class="modal-content">
                                  <form method="post" name="advancedajaxform" id="advancedajaxform">
                                         <div class="modal-header">
                                                <button type="button" class="close" id="closeMarkID" data-dismiss="modal">
                                                <img class="closeMark" src="<%=request.getContextPath() %>/images/Close_G_24.png"/>
                                         </button>
                                         <div class="modal-title logTitle"><liferay-ui:message key="ADVANCED SEARCH"/></div>
                                         </div>
                                         <div class="modal-body">


                                                <input type="text" placeholder="<liferay-ui:message key="Enter keyword for Search"/>"
                                                       name="advancedSearchQuery"></input>
                                                <table id="formTable1">
                                                <tr id="advSearchRow">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="Product Type:" /></td>
                                                              <td class="advSearchCustomSpan4">
                                                              <select id="productTypeID" class="selectBoxLogInc" name="productType" onchange="check(this)">
                                                                           <option value="allServices">All Services</option>
                                                                           <c:forEach var="item" items="${productType}">
                                                                                  <option value="${item.trim()}">${item.trim()}</option>
                                                                           </c:forEach> 
                                                                           

                                                              </select></td>

                                                       </tr>
                                                       <tr id="advSearchRow">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="Priority:" /></td>
                                                              <td class="advSearchCustomSpan10">
                                                                     <input type="checkbox" class="checkBoxPriority" name="priority" value="All">
                                                                     <label class="checkbox-label"><liferay-ui:message key="All" /></label>
                                                                     
                                                                     <input type="checkbox" class="checkBoxPriority" name="priority" value="P1">
                                                                     <label class="checkbox-label">P1</label>
                                                                     
                                                                     <input type="checkbox" class="checkBoxPriority" name="priority" value="P2"> 
                                                                     <label class="checkbox-label">P2</label>
                                                                     
                                                                     <input type="checkbox" class="checkBoxPriority" name="priority" value="P3"> 
                                                                     <label class="checkbox-label">P3</label>
                                                                     
                                                                     <input type="checkbox" class="checkBoxPriority" name="priority" value="P4"> 
                                                                     <label class="checkbox-label">P4</label>
                                                                     
																	 <input type="checkbox" class="checkBoxPriority" name="priority" value="P5"> 
                                                                     <label class="checkbox-label">P5</label>
                                                              </td>

                                                       </tr>
                                                       <tr id="advSearchRow">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="State:" /></td>
                                                              <td class="advSearchCustomSpan4">
                                                                     <input type="checkbox" name="impact" value="With Impacts"> 
                                                                     <label class="checkbox-label"><liferay-ui:message key="With Impacts" /></label>
                                                                     
                                                                     <input type="checkbox" name="impact" value="At Risk"> 
                                                                     <label class="checkbox-label"><liferay-ui:message key="At Risk" /></label>
                                                              </td>

                                                       </tr>
                               <%-- <tr id="advSearchRowVBlock">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="V Block:" /></td>
                                                              <td class="advSearchCustomSpan4">
                                                              <select id="VBlockID" class="selectBoxLogInc" name="vBlock" onchange="getVM(this)">
                                                                           <option value="allVBlock">All V-Block</option>
                                                                     
                                                              </select>
                                                              </td>

                                                       </tr> --%>
                                                       <tr id="advSearchRowVM">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="VM:" /></td>
                                                              <td class="advSearchCustomSpan4">
                                                              <select id="VMID" class="selectBoxLogInc" name="vm">
                                                                           <c:forEach var="item" items="${VMList}">
                                                                                  <option value="${item.key}">${item.value}</option>
                                                                            </c:forEach> 
                                                              </select>
                                                              </td>

                                                       </tr>
                                                       
                                                       <tr id="advSearchRowSite">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="Site Name:" /></td>
                                                              <td class="advSearchCustomSpan4">
                                                              <select id="siteIDAdv" class="selectBoxLogInc" name="siteName" onchange="getService(this)">
                                                                           <%-- <!-- <option value="allSites">All Sites</option> -->--%>
                                                                           <c:forEach var="item" items="${sitenamelist}">
                                                                                  <option value="${item.key}">${item.value}</option>
                                                                           </c:forEach> 
                                                              </select></td>

                                                       </tr>
                                                       <tr id="advSearchRowService">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="Service Name:" /></td>
                                                              <td class="advSearchCustomSpan4">
                                                              <select  id="serviceName"  class="selectBoxLogInc" name="serviceName">
                                                                           <%-- <option value="allServices">All Services</option>--%>
                                                                           <c:forEach var="item" items="${servicenamelist}">
                                                                                  <option value="${item.key}">${item.value}</option>
                                                                           </c:forEach>
                                                              </select></td>

                                                       </tr>
                                                </table>
                                                <table id="formTable2">
                                                       <tr id="advSearchRow">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="Date from:" /></td>
                                                              <td class="advSearchCustomTD advSearchCustomSpan4"><input class="datepicker" type="text" name="dateFrom" /></td>
                                                              <td class="advSearchCustomTD advSearchCustomSpan2"><liferay-ui:message key="Date to:" /></td>
                                                              <td class="advSearchCustomTD advSearchCustomSpan4"><input class="datepicker" type="text" name="dateTo" /></td>
                                                       </tr>
                                                       <tr id="advSearchRow">
                                                              <td class="advSearchCustomSpan2"><liferay-ui:message key="Assigned to:" /></td>
                                                              <td class="advSearchCustomTD advSearchCustomSpan10"><input type="text" id="assignedTo" name="assignedTo" /></td>
                                                       </tr>
                                                </table>
                                         </div>
                                         <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="cancelButton">
                                                       <div class="cancelButtonContent">
                                                              <div class="buttonIcon">
                                                              <img src="<%=request.getContextPath() %>/images/Cancel_W_16.png"/>
                                                       </div>
                                                       <div class="buttonText">
                                                              <liferay-ui:message key="Cancel"/>
                                                       </div>
                                                </div> 
                                                </button>
                                                <button type="submit" class="submitRequestButton">
                                                       <div class="submitButtonContent">
                                                       <div class="buttonIcon">
                                                             <img src="<%=request.getContextPath() %>/images/Search_W_16.png"/>
                                                       </div>
                                                       <div class="buttonText">
                                                              <liferay-ui:message key="Search" />
                                                       </div>
                                                </div>
                                                       
                                                </button>
                                         </div>
                                  </form>
                           </div>

                     </div>
              </div>
       </div>
</div>
</body>
</html>
