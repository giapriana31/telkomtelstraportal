<%@page import="javax.portlet.PortletSession"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<!-- <link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.7/css/jquery.dataTables.css">
<script
	src="https://cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script> -->


<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.validate-1.9.min.js"
	type="text/javascript"></script>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="logIncidentModal.jsp"%>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/ttlogticket.css">
<script src="<%=request.getContextPath()%>/js/ttlogticket.js"
	type="text/javascript"></script>
	
	<portlet:actionURL name="refreshInc" var="processURL"></portlet:actionURL>
<script>
	$(document).ready(function(){
		$('#activeIncidents').click(function(){
			/*alert('add');*/
		});
		
		$('#completedIncidents').click(function(){
			/*alert('addddddd');*/
		});
	});
	
	function submitFm() {

	document.getElementById("refreshButton").disabled = 'disabled';
	document.getElementById("refreshButton").setAttribute("class", "updateButtonDisable");
	
	var url = '<%=processURL.toString()%>';
	document.forms["fm"].action = url;
	document.forms["fm"].submit();
	}
</script>
<style>

.container-fluid.tt-incident-breadcrumb {
    margin-top: -40px !important;
}

.span6.tt-create-incident-breadcrumb {
    width: auto !important;
}

button#refreshButton {
    position: relative !important;
    bottom: 0px !important;
    left: 0px !important;
}

#updateDataHolderDiv {
    width: auto !important;
    position: relative !important;
    right: 0px !important;
    padding-bottom: 18px;
}

.updateInfoHolder {
    display: inline-block !important;
    position: relative !important;
    right: 0px !important;
    bottom: 0px !important;
}

.updatedTimeHolder {
    display: inline-block !important;
    position: relative !important;
    right: 0px !important;
    bottom: 0px !important;
}
.updateButtonHolder {
    display: inline-block !important;
}

@media (max-width: 979px){


.updatedTimeHolder {
    right: 3% !important;
    bottom: 13px !important;
}

.updateInfoHolder {
    right: 3% !important;
    bottom: 12px !important;
}

}


a#activeIncidents {
    padding-top: 14%!important;
    padding-bottom: 7px!important;
}

a#completedIncidents {
    padding-top: 10% !Important;
    padding-bottom: 7px !important;
}

	@media  (max-width: 500px) {
	.rwd{
		display:none !important;
	}
	.updateInfoHolder{
		margin-left:4% !important;
		position:relative;
		right:0% !important;
		bottom : 7px !important;
	}
	.updatedTimeHolder{
		position:relative;
		right:0% !important;
		bottom : 7px !important;
	}
	
	.wrapper {
		cursor: help;
		position: relative;
	  }
	
  .wrapper .tooltip {
	    background: #D4D4D4 ;
		bottom: 100%;
	  color: #fff;
	  display: block;
	  left: -25px;
	  margin-bottom: 15px;
	  opacity: 0;
	  padding: 20px;
	  pointer-events: none;
	  position: absolute;
	  width: 100%;
	  -webkit-transform: translateY(10px);
	     -moz-transform: translateY(10px);
	      -ms-transform: translateY(10px);
	       -o-transform: translateY(10px);
	          transform: translateY(10px);
	  -webkit-transition: all .25s ease-out;
	     -moz-transition: all .25s ease-out;
	      -ms-transition: all .25s ease-out;
	       -o-transition: all .25s ease-out;
	          transition: all .25s ease-out;
	  -webkit-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	     -moz-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	      -ms-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	       -o-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	          box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	}
	
	
	/* This bridges the gap so you can mouse into the tooltip without it disappearing */
	.wrapper .tooltip:before {
	  bottom: -20px;
	  content: " ";
	  display: block;
	  height: 20px;
	  left: 0;
	  position: absolute;
	  width: 100%;
	}  
	
	/* CSS Triangles - see Trevor's post */
	.wrapper .tooltip:after {
	  border-left: solid transparent 10px;
	  border-right: solid transparent 10px;
	  border-top: solid #D4D4D4 10px;
	  bottom: -10px;
	  content: " ";
	  height: 0;
	  left: 50%;
	  margin-left: -13px;
	  position: absolute;
	  width: 0;
	}
	  
	.wrapper:hover .tooltip {
	  font-size: 8px;
	    color: black;
	  opacity: 1;
	  pointer-events: auto;
	  -webkit-transform: translateY(0px);
	     -moz-transform: translateY(0px);
	      -ms-transform: translateY(0px);
	       -o-transform: translateY(0px);
	          transform: translateY(0px);
	}

	/* IE can just show/hide with no transition */
	.lte8 .wrapper .tooltip {
	  display: none;
	}
	
	.lte8 .wrapper:hover .tooltip {
	  display: block;
	}
}
@media  (min-width: 500px) {
	.wrapper{
			display:none !important;
	}
}
	button#logTicketButton {
	  position: relative;
	  bottom: 12px;
	  z-index: 1;
	}
	
	.span12.incHeaderPanel {
	  position: absolute;
	  top: 80px;
	}

	button#refreshButton {
	  position: absolute;
	  bottom: 50px;
	  left: 240px;
	}

	.updateButtonDisable{
		background: #CCC;
		border-top-style: none;
		border-bottom-style: none;
		border-right-style: none;
		border-left-style: none;
		height:36px;
		color: #ffffff !important;
		width: auto;
		min-width:100px;
		border-radius:3px;
		padding-left:15px;
		padding-right:15px;
		font-family:'Gotham-Rounded' !important;
		cursor:default !important;
	}

	#updateDataHolderDiv {
		width: 1000px;
		position: relative;
		right: 10%;
		padding-bottom: 18px;
	}
	
	.updateInfoHolder {
		display: inline;
		position: relative;
		right: 6%;
		bottom: 18px;
		
	}
	
	.updatedTimeHolder {
		display: inline;
		position: relative;
		right: 6%;
		bottom: 17px;
	}
	
	.updateButtonHolder {
		display: inline;
	}

	.tt-incident-breadcrumb{
		padding-top: 20px;
		background-color: #ffffff;
	}
	
	.tt-create-incident-breadcrumb{
		width: 340px !important;
		margin-top: 0px !important;
	}
	
	.tt-page-icon{
		float: left;
	}
	
	.tt-incident-br-header{
		float: left;
		padding-left: 20px;
		font-size: 20px;
		color: #333333;
		text-shadow: none;
		padding-top: 5px;
		font-weight: 600;
	}
	
	@media (min-width: 601px) and (max-width: 768px){
		.tt-search-filter{
			padding-top: 20px;
			text-align: left;
		}		
		.tt-create-incident-ctr{
			/*text-align: left ! important;
			padding-top: 20px;*/
		}
		.tt-incident-breadcrumb{
			padding-top: 20px !important;
			padding-left: 20px !important;
			padding-right: 20px !important;
		}
	}
	
	@media (min-width: 450px) and (max-width: 768px){
		.tt-create-incident-breadcrumb{
			width: 340px !important;
			float: left !important;
		}
		.tt-create-incident-ctr{
			width: 58% !important;
			float: right !important;
		}
	}
	@media (min-width: 320px) and (max-width: 449px){
		.tt-create-incident-ctr{
			text-align: left !important;
			padding-top: 20px ! important;
		}
		#updateDataHolderDiv{
			right: 20px;
		}
		button#logTicketButton{
			float: right;
			left: 12px;
			margin-top: -24px;
		}
		.span12.incHeaderPanel {
			position: inherit;
		}
	}
	@media (min-width: 320px) and (max-width: 600px){
		.tt-search-filter{
			padding-top: 20px;
			text-align: left;
		}		
		.tt-incident-breadcrumb{
			padding-top: 20px !important;
			padding-left: 20px !important;
			padding-right: 20px !important;
		}
		
		.tt-li-active-incident{
			width: 24% !important;
		}
		#activeIncidents{
			line-height: 1;
		}
		
		.tt-li-completed-incident{
			width: 24% !important
		}
		
		#completedIncidents{
			line-height: 1;
		}
	}
</style>
<div class="container-fluid tt-incident-breadcrumb">
	<div class="row-fluid span12">
		<div class="span6 tt-create-incident-breadcrumb">
			<div class="tt-page-icon">
				<img src="<%=request.getContextPath()%>/images/u841.png"></img>
			</div>
			<div class="tt-incident-br-header"><liferay-ui:message key="Incidents" /></div>
		</div>
		<div class="span6 tt-create-incident-breadcrumb" style="margin-bottom: 1px; float: right !important">
		<div id="updateDataHolderDiv">
			<div class="rwd updateInfoHolder">
				<liferay-ui:message key="Last updated on" />
			</div>
			<div class="wrapper updateInfoHolder"><liferay-ui:message key="Last upd.." /><div class="tooltip" style="margin-left:5px;text-align:center;">Last updated on</div></div>
			
			
			<div class="updatedTimeHolder">${lastRefreshDate}</div>
			<div class="updateButtonHolder">
			<form name="fm" method="Post">
			<%if(allowAccess)
				{
				%>
					<button type="button" id="refreshButton" class="btn-primary-tt refresh" onclick="submitFm()">
						<liferay-ui:message key="Update Data"/>
					</button>
					<%
				}
				else
				{
				%>
				<div class="updateButtonHolder">
					<button type="button" id="refreshButton" style="background-color: #CCC !important; color: white !important;" class="btn-primary-tt refresh" title="Contact Administrator">
						<liferay-ui:message key="Update Data"/>
					</button>
				</div>
				<%
				}
				%>	
			</form>
			</div>
		</div>
		</div>
		
	</div>
	<div class="span8 tt-create-incident-ctr" style=" float: right">
			<button type="button" id="logTicketButton" class="img-create-incident" data-toggle="modal"><liferay-ui:message key="Log a New Incident"/></button>
		</div>
	<div class="row-fluid pt-5">
		<div class="span12 incHeaderPanel">
			<ul class="nav nav-pills incident-ul">
				<li class="tt-nav-pills ${ttActiveIdentifier} tt-li-active-incident">
					<a id="activeIncidents" href='<portlet:actionURL name="incidentTypeSelector"><portlet:param name="status" value="active"/></portlet:actionURL>'
					class="${ttActiveIdentifier} "><liferay-ui:message key="Active Incidents"/></a>
				</li>
				<li class="tt-nav-pills ${ttCompletedIdentifer} tt-li-active-incident">
					<a id="completedIncidents" href='<portlet:actionURL name="incidentTypeSelector"><portlet:param name="status" value="completed"/></portlet:actionURL>'
					class="${ttCompletedIdentifer} "><liferay-ui:message key="Completed Incidents"/></a>
				</li>
			</ul>
		</div>
		
	</div>
</div>