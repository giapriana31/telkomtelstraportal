<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:resourceURL var="getSiteProvince" id="getSiteProvince" />

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/leaflet.css" />
<script
	src="<%=request.getContextPath()%>/js/leaflet.js"></script>
	
<!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css" /> -->
<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.js"></script> -->

<style>


.aui a:hover, .aui a:focus {
  text-decoration: none;
}
.leaflet-label {
	background: rgb(235, 235, 235);
	background: rgba(235, 235, 235, 0.81);
	background-clip: padding-box;
	border-color: #777;
	border-color: rgba(0, 0, 0, 0.25);
	border-radius: 4px;
	border-style: solid;
	border-width: 4px;
	color: #111;
	display: block;
	font: 12px/20px "Helvetica Neue", Arial, Helvetica, sans-serif;
	font-weight: bold;
	padding: 1px 6px;
	position: absolute;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	pointer-events: none;
	white-space: nowrap;
	z-index: 6;
}

.leaflet-label.leaflet-clickable {
	cursor: pointer;
	pointer-events: auto;
}

.leaflet-label:before,.leaflet-label:after {
	border-top: 6px solid transparent;
	border-bottom: 6px solid transparent;
	content: none;
	position: absolute;
	top: 5px;
}

.leaflet-label:before {
	border-right: 6px solid black;
	border-right-color: inherit;
	left: -10px;
}

.leaflet-label:after {
	border-left: 6px solid black;
	border-left-color: inherit;
	right: -10px;
}

.leaflet-label-right:before,.leaflet-label-left:after {
	content: "";
}

.my-label {
	position: absolute; //
	width: 1000px;
	font-size: 10px;
}

html,body,#map {
	height: 100%;
}
</style>

<script type="text/javascript">
	
/*
Leaflet.label, a plugin that adds labels to markers and vectors for Leaflet powered maps.
(c) 2012-2013, Jacob Toye, Smartrak

https://github.com/Leaflet/Leaflet.label
http://leafletjs.com
https://github.com/jacobtoye
*/
(function(t){var e=t.L;e.labelVersion="0.2.2-dev",e.Label=(e.Layer?e.Layer:e.Class).extend({includes:e.Mixin.Events,options:{className:"",clickable:!1,direction:"right",noHide:!1,offset:[12,-15],opacity:1,zoomAnimation:!0},initialize:function(t,i){e.setOptions(this,t),this._source=i,this._animated=e.Browser.any3d&&this.options.zoomAnimation,this._isOpen=!1},onAdd:function(t){this._map=t,this._pane=this.options.pane?t._panes[this.options.pane]:this._source instanceof e.Marker?t._panes.markerPane:t._panes.popupPane,this._container||this._initLayout(),this._pane.appendChild(this._container),this._initInteraction(),this._update(),this.setOpacity(this.options.opacity),t.on("moveend",this._onMoveEnd,this).on("viewreset",this._onViewReset,this),this._animated&&t.on("zoomanim",this._zoomAnimation,this),e.Browser.touch&&!this.options.noHide&&(e.DomEvent.on(this._container,"click",this.close,this),t.on("click",this.close,this))},onRemove:function(t){this._pane.removeChild(this._container),t.off({zoomanim:this._zoomAnimation,moveend:this._onMoveEnd,viewreset:this._onViewReset},this),this._removeInteraction(),this._map=null},setLatLng:function(t){return this._latlng=e.latLng(t),this._map&&this._updatePosition(),this},setContent:function(t){return this._previousContent=this._content,this._content=t,this._updateContent(),this},close:function(){var t=this._map;t&&(e.Browser.touch&&!this.options.noHide&&(e.DomEvent.off(this._container,"click",this.close),t.off("click",this.close,this)),t.removeLayer(this))},updateZIndex:function(t){this._zIndex=t,this._container&&this._zIndex&&(this._container.style.zIndex=t)},setOpacity:function(t){this.options.opacity=t,this._container&&e.DomUtil.setOpacity(this._container,t)},_initLayout:function(){this._container=e.DomUtil.create("div","leaflet-label "+this.options.className+" leaflet-zoom-animated"),this.updateZIndex(this._zIndex)},_update:function(){this._map&&(this._container.style.visibility="hidden",this._updateContent(),this._updatePosition(),this._container.style.visibility="")},_updateContent:function(){this._content&&this._map&&this._prevContent!==this._content&&"string"==typeof this._content&&(this._container.innerHTML=this._content,this._prevContent=this._content,this._labelWidth=this._container.offsetWidth)},_updatePosition:function(){var t=this._map.latLngToLayerPoint(this._latlng);this._setPosition(t)},_setPosition:function(t){var i=this._map,n=this._container,o=i.latLngToContainerPoint(i.getCenter()),s=i.layerPointToContainerPoint(t),a=this.options.direction,l=this._labelWidth,h=e.point(this.options.offset);"right"===a||"auto"===a&&s.x<o.x?(e.DomUtil.addClass(n,"leaflet-label-right"),e.DomUtil.removeClass(n,"leaflet-label-left"),t=t.add(h)):(e.DomUtil.addClass(n,"leaflet-label-left"),e.DomUtil.removeClass(n,"leaflet-label-right"),t=t.add(e.point(-h.x-l,h.y))),e.DomUtil.setPosition(n,t)},_zoomAnimation:function(t){var e=this._map._latLngToNewLayerPoint(this._latlng,t.zoom,t.center).round();this._setPosition(e)},_onMoveEnd:function(){this._animated&&"auto"!==this.options.direction||this._updatePosition()},_onViewReset:function(t){t&&t.hard&&this._update()},_initInteraction:function(){if(this.options.clickable){var t=this._container,i=["dblclick","mousedown","mouseover","mouseout","contextmenu"];e.DomUtil.addClass(t,"leaflet-clickable"),e.DomEvent.on(t,"click",this._onMouseClick,this);for(var n=0;i.length>n;n++)e.DomEvent.on(t,i[n],this._fireMouseEvent,this)}},_removeInteraction:function(){if(this.options.clickable){var t=this._container,i=["dblclick","mousedown","mouseover","mouseout","contextmenu"];e.DomUtil.removeClass(t,"leaflet-clickable"),e.DomEvent.off(t,"click",this._onMouseClick,this);for(var n=0;i.length>n;n++)e.DomEvent.off(t,i[n],this._fireMouseEvent,this)}},_onMouseClick:function(t){this.hasEventListeners(t.type)&&e.DomEvent.stopPropagation(t),this.fire(t.type,{originalEvent:t})},_fireMouseEvent:function(t){this.fire(t.type,{originalEvent:t}),"contextmenu"===t.type&&this.hasEventListeners(t.type)&&e.DomEvent.preventDefault(t),"mousedown"!==t.type?e.DomEvent.stopPropagation(t):e.DomEvent.preventDefault(t)}}),e.BaseMarkerMethods={showLabel:function(){return this.label&&this._map&&(this.label.setLatLng(this._latlng),this._map.showLabel(this.label)),this},hideLabel:function(){return this.label&&this.label.close(),this},setLabelNoHide:function(t){this._labelNoHide!==t&&(this._labelNoHide=t,t?(this._removeLabelRevealHandlers(),this.showLabel()):(this._addLabelRevealHandlers(),this.hideLabel()))},bindLabel:function(t,i){var n=this.options.icon?this.options.icon.options.labelAnchor:this.options.labelAnchor,o=e.point(n)||e.point(0,0);return o=o.add(e.Label.prototype.options.offset),i&&i.offset&&(o=o.add(i.offset)),i=e.Util.extend({offset:o},i),this._labelNoHide=i.noHide,this.label||(this._labelNoHide||this._addLabelRevealHandlers(),this.on("remove",this.hideLabel,this).on("move",this._moveLabel,this).on("add",this._onMarkerAdd,this),this._hasLabelHandlers=!0),this.label=new e.Label(i,this).setContent(t),this},unbindLabel:function(){return this.label&&(this.hideLabel(),this.label=null,this._hasLabelHandlers&&(this._labelNoHide||this._removeLabelRevealHandlers(),this.off("remove",this.hideLabel,this).off("move",this._moveLabel,this).off("add",this._onMarkerAdd,this)),this._hasLabelHandlers=!1),this},updateLabelContent:function(t){this.label&&this.label.setContent(t)},getLabel:function(){return this.label},_onMarkerAdd:function(){this._labelNoHide&&this.showLabel()},_addLabelRevealHandlers:function(){this.on("mouseover",this.showLabel,this).on("mouseout",this.hideLabel,this),e.Browser.touch&&this.on("click",this.showLabel,this)},_removeLabelRevealHandlers:function(){this.off("mouseover",this.showLabel,this).off("mouseout",this.hideLabel,this),e.Browser.touch&&this.off("click",this.showLabel,this)},_moveLabel:function(t){this.label.setLatLng(t.latlng)}},e.Icon.Default.mergeOptions({labelAnchor:new e.Point(9,-20)}),e.Marker.mergeOptions({icon:new e.Icon.Default}),e.Marker.include(e.BaseMarkerMethods),e.Marker.include({_originalUpdateZIndex:e.Marker.prototype._updateZIndex,_updateZIndex:function(t){var e=this._zIndex+t;this._originalUpdateZIndex(t),this.label&&this.label.updateZIndex(e)},_originalSetOpacity:e.Marker.prototype.setOpacity,setOpacity:function(t,e){this.options.labelHasSemiTransparency=e,this._originalSetOpacity(t)},_originalUpdateOpacity:e.Marker.prototype._updateOpacity,_updateOpacity:function(){var t=0===this.options.opacity?0:1;this._originalUpdateOpacity(),this.label&&this.label.setOpacity(this.options.labelHasSemiTransparency?this.options.opacity:t)},_originalSetLatLng:e.Marker.prototype.setLatLng,setLatLng:function(t){return this.label&&!this._labelNoHide&&this.hideLabel(),this._originalSetLatLng(t)}}),e.CircleMarker.mergeOptions({labelAnchor:new e.Point(0,0)}),e.CircleMarker.include(e.BaseMarkerMethods),e.Path.include({bindLabel:function(t,i){return this.label&&this.label.options===i||(this.label=new e.Label(i,this)),this.label.setContent(t),this._showLabelAdded||(this.on("mouseover",this._showLabel,this).on("mousemove",this._moveLabel,this).on("mouseout remove",this._hideLabel,this),e.Browser.touch&&this.on("click",this._showLabel,this),this._showLabelAdded=!0),this},unbindLabel:function(){return this.label&&(this._hideLabel(),this.label=null,this._showLabelAdded=!1,this.off("mouseover",this._showLabel,this).off("mousemove",this._moveLabel,this).off("mouseout remove",this._hideLabel,this)),this},updateLabelContent:function(t){this.label&&this.label.setContent(t)},_showLabel:function(t){this.label.setLatLng(t.latlng),this._map.showLabel(this.label)},_moveLabel:function(t){this.label.setLatLng(t.latlng)},_hideLabel:function(){this.label.close()}}),e.Map.include({showLabel:function(t){return this.addLayer(t)}}),e.FeatureGroup.include({clearLayers:function(){return this.unbindLabel(),this.eachLayer(this.removeLayer,this),this},bindLabel:function(t,e){return this.invoke("bindLabel",t,e)},unbindLabel:function(){return this.invoke("unbindLabel")},updateLabelContent:function(t){this.invoke("updateLabelContent",t)}})})(window,document);
</script>


<script type="text/javascript">
	
$(document).ready(function() {
		bindNetworkData();
		var maxsz='40px'; 
		$(".a_ttofycgreen").css('height', maxsz);
		$(".a_ttofycred").css('height', maxsz);
		$(".a_ttofycamber").css('height', maxsz);
});
</script>



<div id='map' style="top: 10px; left: 10px; width: 100%; height: 500px;"></div>

<script type="text/javascript">

function bindNetworkData(){
	
	var ttNetworkGraph = ${ttNetworkGraphRequest};
		

map = new L.map('map',{
    	 //worldCopyJump: true
	 zoomControl: false
         }).setView([-4, 120], 5);    
       
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {    
 attribution: '&copy; OpenStreetMap contributors',     
 
 }).addTo( map );


map.dragging.disable();
map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();
map.keyboard.disable();
if (map.tap) map.tap.disable();

drawMap(ttNetworkGraph)

}

function drawMap(data) {

	document.getElementsByClassName("leaflet-control")[0].innerHTML="<a href='http://leafletjs.com' target='_blank' title='A JS library for interactive maps'>Leaflet</a> | � OpenStreetMap contributors";
	for (var i = 0; i < data.length; i++) {	
	drawCircle(data[i]);
	}
}



function drawCircle(rowData) {
	var popupdata =  drawPopUpTableData(rowData);
	
	// Assigning colour to hex values  
	if( rowData.networkprovincecolor== "Red")
	rowData.networkprovincecolor = "#E32212";
	else if( rowData.networkprovincecolor== "Amber")
	rowData.networkprovincecolor ="#FF9C00";
	else
	rowData.networkprovincecolor = "#A3CF62";   
	
	L.circle([rowData.networksiteprovince.latitude, rowData.networksiteprovince.longitude ], 50000, {
	color: rowData.networkprovincecolor,
	fillColor: rowData.networkprovincecolor,
	fillOpacity: 0.5
	}).bindLabel(popupdata, {noHide: true, className: "my-label", offset: [15, -35]}).on('click', function () {getSiteProvince(rowData)}).addTo(map);
}

function drawPopUpTableData(rowData) {
	var row = "<table bgcolor='#D8F0DA' background='transparent'>";
	if(rowData.countredsite >0)
	row +="<tr><td align=" +"center"+"><font color="+ "#E32212" +">" + rowData.countredsite + "</td></tr>";
	if(rowData.countambersite >0)
	row +="<tr><td align=" +"center"+"><font color="+ "#FF9C00" +">" + rowData.countambersite + "</td></tr>";
	if(rowData.countgreensite >0)
	row +="<tr><td align=" +"center"+"><font color="+ "#A3CF62" +">" + rowData.countgreensite + "</td></tr>";
	row += "</table>";   
	return row;
}

	
	//  Ajax call to populate the site province details on click of circle on the map
	function getSiteProvince(rowData) {
		
		var getProvinceURL = "${getSiteProvince}";
		var jsonProvince   = {"selectedProvince" : rowData.networksiteprovince.networksiteprovince};
		var dataJson       = {selectedProvinceJson : JSON.stringify(jsonProvince)};
		var totalCount     =  rowData.countredsite + rowData.countambersite + rowData.countgreensite;
		var startDiv 	   = "<div class=\"span6 tt-site-column tt-site-column-second\" style=\"margin-left:0px !important;\">";
		var endDiv         = "</div>";
		var hrefRed        = "<a class=\"a_ttofycred\">";
		var hrefAmber      = "<a class=\"a_ttofycamber\">";
		var hrefGreen      = "<a class=\"a_ttofycgreen\">";
		var endHref 	   = "</a>";
		
		$.ajax({
			url: getProvinceURL,
            type: "POST",
            dataType : 'json',
            data: dataJson,
            success: function(response){
            	
            	$("#hd").text(rowData.networksiteprovince.networksiteprovince);
            	$("#hd1").text(totalCount);
            	
            	var finalResponse = "";
            	
            	if(response != null) {
	            	for(i=0; i< response.length; i++) {
	            		
	            		if((response[i].colorCode == 'red') || (response[i].colorCode == 'Red')) 
	            			finalResponse = finalResponse + startDiv + hrefRed   + response[i].siteName + endHref + endDiv;
	            		else if ((response[i].colorCode == 'amber') || (response[i].colorCode == 'Amber'))
	            			finalResponse = finalResponse + startDiv + hrefAmber + response[i].siteName + endHref + endDiv;
	            		else if ((response[i].colorCode == 'green') || (response[i].colorCode == 'Green'))
	            			finalResponse = finalResponse + startDiv + hrefGreen + response[i].siteName + endHref + endDiv;
	            	}
            	}
            	
            	$("#site-container").html(finalResponse);
          },
			error: function(xhr){
	       // alert('Error' + xhr);
	    }
		});	
	}

</script>

<br>
<br>
<div class="row-fluid tt-site-tab-incident-content" id="site-incident">
	<div class="span4 pt-20 tt-sites-gold-tab">
		<div class="row-fluid tt-site-tab-header">
			<div class="span12">
				<div class="hd" id="hd"></div>
				<div class="hd1" id="hd1"></div>
			</div>
		</div>
		<div class="row-fluid tt-site-gold-content-row">
			<div class="span12 tt-sites-gold-container" id="site-container">
			</div>
		</div>
	</div>
</div>
