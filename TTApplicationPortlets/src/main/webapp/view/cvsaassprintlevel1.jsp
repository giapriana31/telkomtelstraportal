 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.tt.utils.PropertyReader"%>
<%@ page import="java.util.Properties"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getUserData">
</portlet:resourceURL>

<%
	Properties properties = PropertyReader.getProperties();
%>


<%@ include file="common.jsp" %>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>

<script>

function RBHealth() {
	$('#RBHealth-li').addClass('selected');
	$('#RBCheckbox').removeClass('hide');
}

var VMs = [{priority:'2',productname:'MNS',ciname:'EC5GK000F3BAA',colour:'background-color:#ff9c00'}];


VMs = JSON.stringify('${CVSaasProductLevel1}');
VMs = eval('(' + '${CVSaasProductLevel1}' + ')');

var temp = VMs.slice();

$(document).ready(function(){
	
	var hash = document.location.hash;
	if(hash){
		if(hash === '#RBHealth'){
			RBHealth();
		}
	}
	else{
		RBHealth();
	}		
	
	//VMs = VMs.sort(function IHaveAName(a, b) { return b.priority> a.priority?  1 : b.priority< a.priority? -1 : 0; });

	populateData(VMs);	
	
	$("#linkTable").addClass('hide');
				
})

Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
		  return v[name] === value ? null : v;
	   });
	   this.length = 0;
	   this.push.apply(this, array);
	}
	
$(function() {
	
	$("input[type='checkbox']").change(
	function()	{
		
			temp = VMs.slice();	
			var all = false;
			var priority1 = false;
			var priority2 = false;
			var priority3 = false;
			var normal = false;
			
			if($('#checkbox_all').is(':checked')) 
			{		
				all = true;
			}
			if($('#checkbox_priority1').is(':checked')) 
			{		
				priority1 = true;
			}
			if($('#checkbox_priority2').is(':checked')) 
			{		
				priority2 = true;
			}
			if($('#checkbox_priority3').is(':checked')) 
			{		
				priority3 = true;
			}
			if($('#checkbox_normal').is(':checked')) 
			{		
				normal = true;
			}
			
			if(all == false)
			{
				if(priority1==false)
					temp.removeValue('priority', '1');
				if(priority2==false)
					temp.removeValue('priority', '2');
				if(priority3==false)
					temp.removeValue('priority', '3');
				if(normal==false)
					temp.removeValue('priority', '4');
			}
			
			$('#searchText').val('');
			
			populateData(temp);			
	});	
	});
	
var sortByName = function sortByName(a, b) { 
	return a.productname - b.productname;
}

function populateData(temp)
{	
	temp = temp.sort(sortByName);
	
	if(temp.length==0)
	{
		document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		return;
	}
	else
	{
		var no = 0;
		var cek = 0;
		var str = "";
		var cekProductName = "";
		
		str = "<div class='row-fluid'>";
		for (i in temp)
		{
			if(temp[i].priority=='1'||temp[i].priority=='2'||temp[i].priority=='3'||temp[i].priority=='4'){
				cek++;
				
				if(cekProductName==''){
					cekProductName=temp[i].productname;
				}
				
				
					if(cekProductName==temp[i].productname){
						no++;
						if(no==3)
						{
							no=0;
							str += "<div class='span4'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><br><tr><td height='10' align='center'><div class='span14'><a style='color:#000000!important;text-decoration:initial!important; font-size : 14px;'>"+temp[i].productname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#000000!important;text-decoration:initial!important; font-size : 18px;'>"+temp[i].ciname+"</a></div></td></tr></table></div></div>";
							str += "</div><div class='row-fluid'>";			 
						}else{
							str += "<div class='span4'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><br><tr><td height='10' align='center'><div class='span14'><a style='color:#000000!important;text-decoration:initial!important; font-size : 14px;'>"+temp[i].productname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#000000!important;text-decoration:initial!important; font-size : 18px;'>"+temp[i].ciname+"</a></div></td></tr></table></div></div>";
						}
					}else{
						no=0;
						no++;
						cekProductName=temp[i].productname;
						str += "</div><div class='row-fluid'>";	
						str += "<div class='span4'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><br><tr><td height='10' align='center'><div class='span14'><a style='color:#000000!important;text-decoration:initial!important; font-size : 14px;'>"+temp[i].productname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#000000!important;text-decoration:initial!important; font-size : 18px;'>"+temp[i].ciname+"</a></div></td></tr></table></div></div>";
						}
						
			}
			
		}
		str += "</div>";
		
		if(cek==0){
			document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-RB").innerHTML = str;
		}
	}
}

function getQueryString(url){
	if (url.indexOf('?') > -1) {
		var pos = url.indexOf('?');
		var extract = url.substring(pos + 1,url.length);
		return extract;
	}
	else{
		return -1;
	}
}

</script>

<style>

#saasPortletTitleBar{
       height:37px;
}

#saasTitleText{
       line-height:34px;
}
#saasNavigationArrow{
       line-height:34px;
}
.saas-title-dashboard {
    font-size: 15px;
    display: flex;
    justify-content: center;
}

#saasCountId{
       fill:#888 !important;
       font-size:20px !important;
}

#saasLabelId{
       fill:#888 !important;
       font-size:18px !important;
       font-weight: 100;
}

div#saas {
    display: flex;
   justify-content: center;
}

#markers-tt {
       display: flex;
       justify-content: center;
}

#tt-red {
       background-color: #e32212;
}

#tt-amber {
       background-color: #ff9c00;
}

#tt-green {
       background-color: #a3cf62;
}

#tt-yellow {
       background-color: #ffff00;
}

#tt-black-marker {
       color: #000000;
}

#tt-red-marker {
       color: #e32212;
}

#tt-amber-marker {
       color: #ff9c00;
}

#tt-green-marker {
       color: #a3cf62;
}

#tt-yellow-marker {
       color: #ffff00;
}

.tt-color-legends {
       display: inline-block;
       width: 10px;
       height: 10px;
       position: relative;
       top: 5px;
       margin: 0 5px;
}

.tt-markers {
       display: inline-block;
       font-size: 12px;
}

.legend-saas {
       font-size: 15px;
       line-height: 120%;
       padding-top: 5px;
       padding-bottom: 30px;
       height: 30px;
       text-align: center;
}

#saasblock {
       justify-content: center;
       background-color: #fff;
       height: 262px;
       /* border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; 
       margin-left: 50px;*/
}

.saasServices {
       width: 150px;
       margin: 0 40px 0 40px;
}

.value-saas {
       text-align: center;
       height: 50px;
       line-height: 50px;
       border: solid 1px black;
       border-radius: 8px;
}

#status-val-service {
       border-color: transparent;
}

.row-fluid {
    width: 100%;
    margin-bottom: 1%;
}

.tt-page-detail{
		background-color: #ffffff;
		border-bottom: 1px #cccccc solid;
		padding: 20px !important;
		}

.tt-back{
			width: 40%;
			float: right;
			text-align: right;
		}

.tt-page-breadcrumb{
			padding-top: 20px;
		}

.tt-img-dashboard{
			/*DashBoard_G_32.png*/
			background-image: url(../TTApplicationPortlets/images/DashBoard_G_32.png);
			display: inline-block;
			height: 35px;
			background-repeat: no-repeat;
			padding-left: 40px;
			padding-top: 5px;
			vertical-align: top;
		}	

.tt-current-page{
			font-size: 24px;
			color: #333333;
			vertical-align: top;
			padding-top: 5px;
			display: inline-block;
		}

.dashImageHolder{
	position:relative;
	float:left;
	margin-left: 20px;
}

.tt-previous-page{
			font-size: 14px;
			color: #333333;
			vertical-align: top;
			padding-top: 5px;
			display: inline-block;
		}
</style>

<div class="container-fluid tt-page-detail">
		<div class="row-fluid">
			<div class="span6 tt-breadcrumb">
				<div class="dashImageHolder">
					<img class=""
						src="<%=request.getContextPath()%>/images/DashBoard_G_32.png" />
				</div>&nbsp;
				<span class="tt-previous-page" style="margin-top: 1px;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
	
				<span class="tt-current-page" style="margin-top: 1px;"><liferay-ui:message key="SaaS"/></span>
			</div>
			<div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" class="btn-primary-tt backButton" style="margin-top: 1px;">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
		</div>
	</div>

<div class="row-fluid tt-displaySite-check">
 	<div id="markers-tt">
		     <div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="Display Service : " /></div>&nbsp;&nbsp;
				<input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/>
              <div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="All" /></div>&nbsp;&nbsp;
             <input type="checkbox" id="checkbox_priority1" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-red-marker"><liferay-ui:message key="Priority 1" /></div>&nbsp;&nbsp;
               <input type="checkbox" id="checkbox_priority2" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-amber-marker"><liferay-ui:message key="Priority 2" /></div>&nbsp;&nbsp;
               <input type="checkbox" id="checkbox_priority3" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-yellow-marker"><liferay-ui:message key="Priority 3" /></div>&nbsp;&nbsp;
               <input type="checkbox" id="checkbox_normal" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-green-marker"><liferay-ui:message key="Normal" /></div>
       	</div>
   </div>
       	
<div class="container-fluid tt-site-tab-container">

<div class="span12 tt-graph tt-graph-first tt-gold">
	<div style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
	<div class="row-fluid">
			<div id="data-RB" class="span12" style="padding:10px;background-color: white;">	
				</div>
		</div>
	
	</div>
</div>

</div>