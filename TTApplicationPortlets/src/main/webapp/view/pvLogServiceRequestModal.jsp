<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
       type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js"
       type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
       type="text/javascript"></script>
       <script src="<%=request.getContextPath()%>/js/jquery-ui.js" type="text/javascript"></script>
       
<!--  <script src="<%=request.getContextPath()%>/js/pvLogServiceRequestModal.js"
       type="text/javascript"></script>-->
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/pvLogServiceRequestModal.css" />
       
              
<script src="<%=request.getContextPath()%>/js/pvLogServiceRequest.js"
       type="text/javascript"></script>
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/pvLogServiceRequest.css" />

<%@ include file="common.jsp" %>
<portlet:defineObjects />

<portlet:actionURL var="submitFormURL" name="handleLogSR" />
<portlet:resourceURL var="getSiteURL" id="getSiteURL" />
<portlet:resourceURL var="getServiceForProductTypeURL" id="getServiceForProductTypeURL" />
<portlet:resourceURL var="getDeviceURL" id="getDeviceURL" />
<portlet:resourceURL var="getSubCategoryURL" id="getSubCategoryURL" />
<portlet:resourceURL var="submitRequestURL" id="submitRequestURL" />
<portlet:resourceURL id="pvprodClassforSelectedProductTypeURL" var="pvprodClassforSelectedProductTypeURL"/>

<script>
$(document).ready( function () {
	$('#logSRModal').css('display', "none");
	$('#logSRSubmitResponse').css('display', "none");
	/*$('#logSRModal').css('z-index', -10);*/
	$("input:radio").attr("checked", false);
	
	
	
	$('#closeMarkID, #cancelButtonID').click(function(){
		$('body').removeClass('modal-open');
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$("label.error").hide();
		$('#serviceNameSelect option[value!="0"]').remove();
		$("#logSRForm").validate().resetForm();
		$("#logSRModal").find('form')[0].reset();
		$("#logSRForm")[0].reset();
		$('#logSRModal').modal('hide');
	});
		
		
	$('#logServReqButton').click(function(){
		$('#_145_dockbar').css('z-index', 1039);
		$('#logSRModal').modal('show');
		$('#logSRModal').css('display', "block");
		$('body').addClass('modal-open');
		$("#categoryLabelID").addClass('error');
	});
	
	$('#logSRModal').on( 'shown', function ( event ) {
		$('#logSRModal').css('display', "block");
		$("#logSRForm")[0].reset();
		$("#logSRForm").validate().resetForm();
		$("#categoryLabelID").addClass('error');
	});
	
	$('#logSRModal').on( 'hidden', function ( event ) {
		$('#logSRModal').css('display', "none");
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$("label.error").hide();
		$('#submitRequestButtonID').removeClass("disableButton");
		document.getElementById("submitRequestButtonID").disabled = false;
		$("#logSRForm").validate().resetForm();
		$(this).find('form')[0].reset();
		$("#logSRForm")[0].reset();
		
	});
	
	$('#logSRSubmitResponse').on( 'shown', function ( event ) {
		$('#logSRModal').css('z-index', 1039);
		$('#logSRSubmitResponse').css('display', "block");
		$(this).css('z-index', 1042);
	});
	
	$('#logSRSubmitResponse').on( 'hidden', function ( event ) {
		$(this).css('display', 'none');
		$('#logSRModal').css('z-index', 1041);
		$('#logSRModal').modal('hide');
		$('.modal-backdrop').remove();
		$('body').removeClass('modal-open');
	});
});

	function addCommentNotes(element){
		if(element.checked){
			 /*var textbox = document.getElementById("customDescription");
			 textbox.value =  "Multiple services are impacted. " + textbox.value;*/
			 $('#deviceNameSelect').attr('disabled','disabled');
		     $('#deviceNameSelect').addClass("disableButton");
		}
		else{
			/*var textbox = document.getElementById("customDescription");
			textbox.value="";*/
			/*$("input:radio").attr("checked", false);
			$('#subCategorySelect option[value!="0"]').remove();*/
			/*$('#deviceNameSelect option[value!="0"]').remove();*/
			$('#deviceNameSelect').removeAttr('disabled','disabled');
	    	$('#deviceNameSelect').removeClass("disableButton");
			
		}
	}
	function addActionComment(){
		var selectedAction = document.getElementById("actionSelect");
		var textarea = document.getElementById("customDescription").value
		if(selectedAction.value!='0'){
			textarea += "This is "+selectedAction.value+" action.";
		}else{
			document.getElementById("customDescription").value="";
		}
	}
	
	
	function hideAll(){
		$('#submitRequestButtonID').attr('disabled','disabled');
    	$('#submitRequestButtonID').addClass("disableButton");
    	
    	$('#summary').attr('disabled','disabled');
    	$('#summary').addClass("disableButton");
    	
    	$('#regionSelect').attr('disabled','disabled');
    	$('#regionSelect').addClass("disableButton");
    	
    	$('#siteID').attr('disabled','disabled');
    	$('#siteID').addClass("disableButton");
    	
    	$('#serviceNameSelect').attr('disabled','disabled');
    	$('#serviceNameSelect').addClass("disableButton");
    
    	$('#subCategorySelect').attr('disabled','disabled');
    	$('#subCategorySelect').addClass("disableButton");
    	
    	$('#deviceNameSelect').attr('disabled','disabled');
    	$('#deviceNameSelect').addClass("disableButton");
    	
    	$('#productClassificationSelect').attr('disabled','disabled');
    	$('#productClassificationSelect').addClass("disableButton");
    	
    	
    	$('#customDescription').attr('disabled','disabled');
    	$('#customDescription').addClass("disableButton");
    	
    	$('#cancelButtonID').attr('disabled','disabled');
    	$('#cancelButtonID').addClass("disableButton");

    	$('#closeMarkID').attr('disabled','disabled');
    	$('#closeMarkID').addClass("disableButton2");
	}
	function unhideAll(){
		$('#submitRequestButtonID').removeAttr('disabled','disabled');
    	$('#submitRequestButtonID').removeClass("disableButton");
    	
    	$('#regionSelect').removeAttr('disabled','disabled');
    	$('#regionSelect').removeClass("disableButton");
    	
    	$('#summary').removeAttr('disabled','disabled');
    	$('#summary').removeClass("disableButton");
    	
    	$('#siteID').removeAttr('disabled','disabled');
    	$('#siteID').removeClass("disableButton");
    	
    	$('#serviceNameSelect').removeAttr('disabled','disabled');
    	$('#serviceNameSelect').removeClass("disableButton");
    	
    	$('#subCategorySelect').removeAttr('disabled','disabled');
    	$('#subCategorySelect').removeClass("disableButton");
    	
    	$('#deviceNameSelect').removeAttr('disabled','disabled');
    	$('#deviceNameSelect').removeClass("disableButton");
    	
    	$('#productClassificationSelect').attr('disabled','disabled');
    	$('#productClassificationSelect').addClass("disableButton");
    	
    	
    	$('#customDescription').removeAttr('disabled','disabled');
    	$('#customDescription').removeClass("disableButton");
    	
    	$('#cancelButtonID').removeAttr('disabled','disabled');
    	$('#cancelButtonID').removeClass("disableButton");
    	
    	$('#closeMarkID').removeAttr('disabled','disabled');
    	$('#closeMarkID').removeClass("disableButton2");
	}

</script>

<script type="text/javascript">
var selectedProductType = "";
var slectedServiceName = "";
var selectedSiteID = "";
var selectedSubCategory = "";

var siteData = "";
	
function getSelectedproductType(){
	 selectedProductType = $('#productSelect').val();
	 
	 //alert("before");
	 getProdClassforSelectedProductType();
	 //alert("after");
	
	 slectedServiceName = "";
	 selectedSiteID = "";
	 selectedSubCategory = "";
						
	      //alert("onChange of ProductType::selectedProductType::>>"+selectedProductType);
	      //alert("onChange of ProductType::slectedServiceName::>>"+slectedServiceName);	
	      //alert("onChange of ProductType::selectedSiteID::>>"+selectedSiteID);	
	      //alert("onChange of ProductType::selectedSubCategory::>>"+selectedSubCategory);	
								
			if (selectedProductType.toUpperCase()==="MNS"){
				$('#regionRow').show();
				$('#siteNameRow').show();
				$('#regionTitle').show();
				$('#siteNameTitle').show();
				$('#regionSelect').show();
				$('#siteID').show();
				$('#serviceNameRow').show();
				$('#deviceNameRow').show();
				$('#typeNameRow').show();
				
				//onChange
				$("#siteID").change(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				//onBlur
				$("#siteID").blur(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				//onSelect
				$("#siteID").select(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				//onKeyup
				$("#siteID").keyup(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				
			}else if (selectedProductType.toUpperCase()==="WHISPIR" || selectedProductType.toUpperCase()==="IPSCAPE" || selectedProductType.toUpperCase()==="MANDOE"){
				$('#regionRow').hide();
				$('#siteNameRow').hide();
				$('#serviceNameRow').show();
				$('#deviceNameRow').hide();
				$('#typeNameRow').hide();

					selectedSiteID = ""; 
					getServiceForProductType();
					getSaasSubCategories();
			}else{
				$('#regionRow').hide();
				$('#siteNameRow').hide();	
				$('#serviceNameRow').show();
				$('#deviceNameRow').show();
				$('#typeNameRow').show();
				
				    selectedSiteID = ""; 
					getServiceForProductType();
			}
							
	}
	
function getSite(dropdown) {
	siteData = new Array();
       var selectedValue = dropdown.options[dropdown.selectedIndex].value;
       var getSiteURL = "${getSiteURL}";
       var jsonCategory = {
              "selectedRegion" : selectedValue
       };
       var dataJson = {
              selectedRegionJson : JSON.stringify(jsonCategory)
       };
       $.ajax({
              url : getSiteURL,
              type : "POST",
              data : dataJson,
              success : function(response) {
                    $('#siteID option[value!="0"]').remove();
                    $('#serviceNameSelect option[value!="0"]').remove();
                    $('#deviceNameSelect option[value!="0"]').remove();

                    var x = 1;
                    var arr = response.split("#####");
                    var len = arr.length;
                    var reslen = len - x;
                    for (var i = 0; i < reslen; i++) {

                           
                    siteData.push(response.split("#####")[i].split('@@@@@')[1]);
                    }      
                    BindControls();
       
                    $('#siteID option[value=""]').remove();
                    
              },
              error : function(xhr) {
                    BindControls();
                    $('#siteID option[value!="0"]').remove();
              }
       });
}

function getServiceForProductType() {

    var getServiceForProductTypeURL = "${getServiceForProductTypeURL}";
    var jsonSiteID = {
    		"selectedProductID" : selectedProductType ,
			"selectedSiteID" : selectedSiteID	
		};
    
    //alert("selectedProductID in getServiceForProductType::"+selectedProductType);
    //alert("selectedSiteID in getServiceForProductType::"+selectedSiteID);

	var dataJson={selectedSiteIDJson : JSON.stringify(jsonSiteID)};
	$.ajax({
	    url : getServiceForProductTypeURL,
	    type : "POST",
	    data : dataJson,
	    success : function(response) {
	    	
	    	//alert('success '+response);
	          $('#serviceNameSelect option[value!="0"]').remove();
	          var x = 1;
	          var arr = response.split("#####");
	          var len = arr.length;
	          var reslen = len - x;
	          for (var i = 0; i < reslen; i++) {
	
	                 $('#serviceNameSelect').append(
	                               $('<option>')
	                                             .text(
	                                                    response.split("#####")[i]
	                                                                 .split('@@@@@')[1]).val(
	                                                    response.split("#####")[i]
	                                                                 .split('@@@@@')[1]));
	          }
	          $('#serviceNameSelect option[value=""]').remove();
	    },
	    error : function(xhr) {
	    	//alert("Error"+xhr);
	    }
	});
}

function getProdClassforSelectedProductType() {
	
    //alert("Yo Bro");
	var pvprodClassforSelectedProductTypeURLVar = "${pvprodClassforSelectedProductTypeURL}";
	var jsonSiteID = {
			"selectedProductID" : selectedProductType ,		
			};
	
     //alert("selectedProductID in getProdClassforSelectedProductType::"+selectedProductType);

	$.ajax({
		url : pvprodClassforSelectedProductTypeURLVar,
		type : "POST",
		data : jsonSiteID,
		success : function(response) {
		//alert('success '+response);
			$('#productClassificationSelect option[value!="0"]').remove();
			var x = 1;
			var arr = response.split("#####");
			var len = arr.length;
			var reslen = len - x;
			for (var i = 0; i < reslen; i++) {

				$('#productClassificationSelect').append(
						$('<option>')
								.text(
										response.split("#####")[i]
												.split('@@@@@')[1]).val(
										response.split("#####")[i]
												.split('@@@@@')[1]));
			}
			$('#productClassificationSelect option[value=""]').remove();
			},
		error : function(xhr) {
			//alert("Error"+xhr);
		}
	});
}
</script>

<script type="text/javascript">




	function getCategories() {
		var getSubCategoryURL = "${getSubCategoryURL}";

		var selectedValue = document.getElementById("serviceNameSelect").value;

		//alert("service");
		var category = "INFLIGHTMACD";
		var jsonService = {
			"selectedService" : selectedValue,
			"categoryValue" : category,
			"selectedProductID" : selectedProductType
		};
		var dataJson = {
			selectedServiceJson : JSON.stringify(jsonService)
		};

		
	
	$
				.ajax({
					url : getSubCategoryURL,
					type : "POST",
					data : dataJson,
					success : function(response) {

						//alert("service");

						$('#subCategorySelect option[value!="0"]').remove();
						var x = 1;

						var subCategoryArr = response.split("%%%%%")[1];

						len = subCategoryArr.split("#####").length;
						reslen = len - x;
						for (var i = 0; i < reslen; i++) {
							if (subCategoryArr.split("#####")[i].split('@@@@@')[0] == "") {
								continue;
							}
							$('#subCategorySelect').append(
									$('<option>').text(
											subCategoryArr.split("#####")[i]
													.split('@@@@@')[1]).val(
											subCategoryArr.split("#####")[i]
													.split('@@@@@')[0]));
						}
					},
					error : function(xhr) {
						$('#subCategorySelect option[value!="0"]').remove();

					}
				});
	}
	
	function getSaasSubCategories() {
		var getSubCategoryURL = "${getSubCategoryURL}";

		var selectedValue = "SaaS";

		//alert("service");
		var category = "INFLIGHTMACD";
		var jsonService = {
			"selectedService" : selectedValue,
			"categoryValue" : category,
			"selectedProductID" : selectedValue
		};
		var dataJson = {
			selectedServiceJson : JSON.stringify(jsonService)
		};
	
		$.ajax({
			url : getSubCategoryURL,
			type : "POST",
			data : dataJson,
			success : function(response) {
				//alert("service");

				$('#subCategorySelect option[value!="0"]').remove();
				var x = 1;

				var subCategoryArr = response.split("%%%%%")[1];

				len = subCategoryArr.split("#####").length;
				reslen = len - x;
				for (var i = 0; i < reslen; i++) {
					if (subCategoryArr.split("#####")[i].split('@@@@@')[0] == "") {
						continue;
					}
					$('#subCategorySelect').append(
							$('<option>').text(
									subCategoryArr.split("#####")[i]
											.split('@@@@@')[1]).val(
									subCategoryArr.split("#####")[i]
											.split('@@@@@')[0]));
				}
			},
			error : function(xhr) {
				$('#subCategorySelect option[value!="0"]').remove();
			}
		});
	}

	function getDevices() {

		selectedSubCategory = $('#subCategorySelect').val();
		//alert("::::::"+selectedSubCategory);
		slectedServiceName = $('#serviceNameSelect').val();
		;

		// $('#deviceNameSelect option[value!="0"]').remove();
		// $('#deviceNameSelect').removeAttr('disabled');
		// $('#deviceNameSelect').removeClass("disableButton");
		var getDeviceURL = "${getDeviceURL}";
		var serviceNameValue = document.getElementById("serviceNameSelect").value;

		var jsonService = {
			"selectedService" : serviceNameValue,
			"selectedProductID" : selectedProductType,
			"selectedSubCategory" : selectedSubCategory
		};
		var dataJson = {
			selectedServiceJson : JSON.stringify(jsonService)
		};
		$.ajax({
			url : getDeviceURL,
			type : "POST",
			data : dataJson,
			success : function(response) {
				$('#deviceNameSelect option[value!="0"]').remove();
				var x = 1;
				var arr = response.split("#####");
				var len = arr.length;
				var reslen = len - x;
				for (var i = 0; i < reslen; i++) {

					$('#deviceNameSelect').append(
							$('<option>')
									.text(
											response.split("#####")[i]
													.split('@@@@@')[1]).val(
											response.split("#####")[i]
													.split('@@@@@')[0]));
				}
				$('#deviceNameSelect option[value=""]').remove();
			},
			error : function(xhr) {
				$('#serviceNameSelect option[value!="0"]').remove();

			}
		});
	}
</script>

<script type="text/javascript">
       
</script>

<script type="text/javascript">

</script>
<script type="text/javascript">

	$(document)
			.ready(
					function() {

						//alert("In ready!!");

						$("#productSelect").change(function() {
							getSelectedproductType();
						});

						$('#regionRow').hide();
						$('#siteNameRow').hide();
						$('#serviceNameRow').show();
						$('#deviceNameRow').show();
						$('#typeNameRow').show();

						var reqID;
						var submitRequestURL = "${submitRequestURL}";
						var errorClassForLabel = 'errorForLabel';
						jQuery.validator
								.addMethod('selectcheck', function(value) {
									if (value != null && value != '0') {
										return true;
									} else {
										return false;
									}

								},
										"<liferay-ui:message key='Please select a valid option' />");

						$("#logSRForm")
								.validate(
										{
											rules : {
												summary : {
													required : true,
													maxlength : 200
												},
												description : {
													required : true,
													maxlength : 200
												},
												siteName : {
													required : true
												},
												serviceName : {
													selectcheck : true
												},
												region : {
													selectcheck : true
												},

												category : "required",
												subCategory : {
													selectcheck : true
												}
											},
											messages : {
												summary : {
													required : "<liferay-ui:message key='Please enter request title of Service Request' />",
													maxlength : "<liferay-ui:message key='Maximum characters allowed is 200' />",
												},
												description : {
													required : "<liferay-ui:message key='Please enter description of Service Request' />",
													maxlength : "<liferay-ui:message key='Maximum characters allowed is 200' />",
												},
												category : "<liferay-ui:message key='Please select category' />",
											},
											highlight : function(element,
													errorClass, validClass) {

												return false; // ensure this function stops
											},
											unhighlight : function(element,
													errorClass, validClass) {
												return false; // ensure this function stops
											},
											submitHandler : function(form) {
												var dataForm = $('#logSRForm')
														.serialize();
												hideAll();
												$
														.ajax({
															url : submitRequestURL,
															type : "POST",
															data : dataForm,
															success : function(
																	response) {
																reqID = JSON
																		.parse(response);
																var status = reqID.requestID
																		.split("@")[0];

																if (status == 'Success') {
																	document
																			.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="SUCCESSFUL"/>';
																	document
																			.getElementById("requestID").innerHTML = '<liferay-ui:message key="Your service request - "/>'
																			+ reqID.requestID
																					.split("@")[1]
																			+ '&nbsp;'
																			+ '<liferay-ui:message key="has been submitted successfully!"/>';
																} else {
																	document
																			.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
																	document
																			.getElementById("requestID").innerHTML = '<liferay-ui:message key="Service Request submission failed. Please try again."/>';
																}
																$(
																		'#logSRSubmitResponse')
																		.modal(
																				'show');
																unhideAll();

																$("#logSRForm")[0]
																		.reset();
																$(
																		'#serviceNameSelect option[value!="0"]')
																		.remove();

															},
															error : function(
																	xhr) {
																document
																		.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
																document
																		.getElementById("requestID").innerHTML = '<liferay-ui:message key="Service Request submission failed. Please try again."/>';
																$(
																		'#logSRSubmitResponse')
																		.modal(
																				'show');
																unhideAll();
																$("#logSRForm")[0]
																		.reset();
																$(
																		'#serviceNameSelect option[value!="0"]')
																		.remove();
															}
														});
											}
										});
					});
</script>
<script>
        $(document).ready(function() {
         //   BindControls();
        });

        function BindControls() {
        	
       $('#siteID').val('');
  	   $('#subCategorySelect option[value!="0"]').remove();
  	   $("label.error").hide();

       $('#siteID').autocomplete({
                source: siteData,
                minLength: 0,
                           appendTo: "#logSRModal",
                scroll: true
            }).focus(function() {
                $(this).autocomplete("search", "");
            });
        }
</script>

<div class="modal fade tt-SR-modal" id="logSRModal" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-lg">
      <!-- Modal content-->
 		 <div class="modal-content">
           	<div class="modal-header">
              	<button type="button" class="close" id="closeMarkID" data-dismiss="modal">
                     <img class="closeMark" src="<%=request.getContextPath()%>/images/Close_G_24.png" />
              	</button>
              		<div class="modal-title logTitle">
                      <liferay-ui:message key="LOG A NEW REQUEST" />
                </div>
          	</div>
			<div class="modal-body tt-modal-body">
               <form:form id="logSRForm" name="logSRForm" class="form-horizontal tt-modal-form" modelAttribute="logSRFormModel" method="post">
                     <div class="container-fluid tt-form-container">
							<div class="row-fluid tt-form-row">
                                <div class="span3 tt-form-left-col">
									<liferay-ui:message key="Request Title:*" />
							 	</div>
                               	<div class="span9 tt-form-right-col">
                                    <form:input path="summary" id="summary" name="summary" type="text" class="textBoxLogSR" tabindex="1"/>
                                </div>
                             </div>
                             <div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									 <liferay-ui:message key="Product Type:*" />
								</div>
								<div class="span9 tt-form-right-col">
									 <form:select path="productType" name="productType" id="productSelect" items="${productTypeList}" class="selectBoxLogInc" tabindex="2">
									 </form:select>
								</div>
							  </div>
                              <div class="row-fluid tt-form-row" id="regionRow">
                               	<div class="span3 tt-form-left-col" id="regionTitle">
									<liferay-ui:message key="Region:*" />
								</div>
                                <div class="span9 tt-form-right-col">
                                    <form:select path="region" name="region" id="regionSelect"
                                                              items="${regionList}" class="selectBoxLogInc"
                                                              onchange="getSite(this)" tabindex="2">
									</form:select>
                                </div>
                               </div>
							   <div class="row-fluid tt-form-row" id="siteNameRow">
                                   <div class="span3 tt-form-left-col" id="siteNameTitle">
										<liferay-ui:message key="Site Name:*" />
								   </div>
								   <div class="span9 tt-form-right-col">
                                       <div>
                                         <form:input path="siteName" name="siteName" tabindex="3" type="text" 
                                        									class="selectBoxLogInc" id="siteID"/>
                                       </div>
                                   </div>
                               	</div>
								<div class="row-fluid tt-form-row" id="serviceNameRow">
                                    <div class="span3 tt-form-left-col">
										<liferay-ui:message key="Service Name:*" />
									</div>
                                    <div class="span9 tt-form-right-col">
                                         <form:select path="serviceName" name="serviceName"
                                                      id="serviceNameSelect" items="${serviceIDList}"
                                                      class="selectBoxLogInc" onchange="getCategories()" tabindex="4">
                                         </form:select>
                                  	</div>
                              	</div>
                                <div class="row-fluid tt-form-row">
                                     <div class="span3 tt-form-left-col">
											<liferay-ui:message key="Category:*" />
									 </div>
                                     <div class="span9 tt-form-right-col">
                                           <form:select path="subCategory" name="subCategory" onchange="getDevices()" 
                                           					  onblur="getDevices()"
                                                              id="subCategorySelect" items="${subCateogryListMap}"
                                                              class="selectBoxLogInc" tabindex="9">
										   </form:select>
                                     </div>
                                 </div>
                                 <div class="row-fluid tt-form-row" id="deviceNameRow">
                                      <div class="span3 tt-form-left-col">
											<liferay-ui:message key="Device Name:" />
									  </div>
                                      <div class="span9 tt-form-right-col">
                                           	<form:select path="deviceName" name="deviceName" 
                                                         id="deviceNameSelect" items="${deviceNameListMap}"
                                                         class="selectBoxLogInc" tabindex="10">
											</form:select>
                                       </div>
                                  </div>
                                  <div class="row-fluid tt-form-row" id="typeNameRow">
										<div class="span3 tt-form-left-col">
											<liferay-ui:message key="Type:" />
										</div>
										<div class="span9 tt-form-right-col">
												<form:select path="productClassification" name="productClassification"
													id="productClassificationSelect" items="${productClassificationListMap}"
													class="selectBoxLogInc" tabindex="10">
												</form:select>
										</div>
								  </div>
                                  <div class="row-fluid tt-form-row">
                                       <div class="span3 tt-form-left-col">
											<liferay-ui:message key="Description:*" />
									   </div>
                                       <div class="span9 tt-form-right-col">
                                            <form:textarea path="description" id="customDescription"
                                                              name="description" rows="4" cols="100"
                                                              class="customDescription" tabindex="11"/>
                                       </div>
                                   </div>
								   <div class="row-fluid tt-form-last-row pr-15 pt-30">
                                        <div class="span12 tt-submit-row">
                                            <span class="tt-cancel pr-10">
                                                   <button type="button" class="btn-secondary-tt img-cancel"
                                                          id="cancelButtonID" data-dismiss="modal" tabindex="13">
                                                          <liferay-ui:message key="Cancel" />
                                                   </button>
                                            </span> 
                                            <span class="tt-submit">

<%
	if (allowAccess) {
%>
						                                               <button type="submit"
						                                                      class="btn-primary-tt img-submit-servicerequestenabled"
						                                                      id="submitRequestButtonID" tabindex="12">
						                                                      <liferay-ui:message key="Submit Request" />
						                                               </button> 
<%
	} else {
%>

		                                                              <button type="button"
		                                                                     class="img-submit-servicerequest btn-primary-tt "
		                                                                     id="submitRequestButtonID"
		                                                                     title="Contact Administrator" tabindex="12">
		                                                                     <liferay-ui:message key="Submit Request" />
		                                                              </button> 
<%
	}
%>
                                                </span>
                                            </div>
                                       	</div>
                             </div>
                           </form:form>
                     </div>
              </div>
       </div>
</div>
<div class="modal fade" id="logSRSubmitResponse" data-backdrop="static"
       data-keyboard="false">
       <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"
                     aria-hidden="true">
                     <img class="closeMark"
                           src="<%=request.getContextPath()%>/images/Close_G_24.png" />
              </button>
              <div id="responseStatus"></div>
       </div>
       <div class="modal-body">
              <div id="requestID"></div>
       </div>
       <div class="modal-footer">
              <button type="button" class="img-close btn-secondary-tt"
                     id="closeButtonID" data-dismiss="modal">
                     <liferay-ui:message key="Close" />
              </button>
       </div>
</div>