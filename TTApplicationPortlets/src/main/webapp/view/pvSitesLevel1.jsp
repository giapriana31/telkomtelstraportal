


<!--END SITE PAGE CONTAINER--> <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getUserData">
</portlet:resourceURL>
<portlet:resourceURL var="filterSitesURL" id="filterSitesURL" />

<%@ include file="common.jsp" %>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>

	
	<link href="${pageContext.request.contextPath}/css/pvSitesLevel0.css.css"
	rel="stylesheet"></link>
<!-- <style>

a{
color:#fff !important;
text-decoration:none !important;
}
</style>	
	 -->
<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/sites.js"></script>






<script>



$(document).ready(function(){

var filterList=["6","5","4","3","2","1"];

	$("input[type='checkbox']").change(function() {
		var selectedFilters="";
		 var selected = []; 
		$('#filterForm input:checked').each(function() {
			
			selected.push($(this).attr('id'));
		//	selectedFilters=selectedFilters+$(this).attr('name')+"&&&"; 
			
			
		});
		
		applyFilters(selected);

});

function applyFilters(selected){
				var goldCount=0;
				var silverCount=0;
				var bronzeCount=0;
		
				if(selected[0]=='all'){
				
				
					for (i = 0; i < filterList.length; i++) { 
				   
						var tempElement=document.getElementsByName(filterList[i]);
						
						for (j = 0; j < tempElement.length; j++) {
						tempElement[j].style.display = "block";
							var temp=tempElement[j].parentNode.parentNode;
				var id=temp.getAttribute("id");
				if(id=="boxbronze"){
				bronzeCount++;
				
				}else 
				if(id=="boxsilver"){
				
				silverCount++;
				
				}
				else if(id=="boxgold"){
				goldCount++;
				
				}
						
						
						}
				
					
				}
				
				
				
				}else{
				
			
				
				for (i = 0; i < filterList.length; i++) { 
				   
					var tempElement=document.getElementsByName(filterList[i]);
					
						for (j = 0; j < tempElement.length; j++) {
						tempElement[j].style.display = "none";
						}
				
					
					}
				
				
				
				
				for (i = 0; i < selected.length; i++) { 
					var tempElement=document.getElementsByName(selected[i]);
				
				for (j = 0; j < tempElement.length; j++) {
				
				tempElement[j].style.display = "block";
				var temp=tempElement[j].parentNode.parentNode;
				var id=temp.getAttribute("id");
				if(id=="boxbronze"){
				
				bronzeCount++;
				
				}else 
				if(id=="boxsilver"){
				
				silverCount++;
				
				}
				else if(id=="boxgold"){
				goldCount++;
				
				}
				

				
				}
				
					
				}
				}
					
		document.getElementById("bronzeCount").innerHTML = bronzeCount;
		document.getElementById("silverCount").innerHTML = silverCount;
		document.getElementById("goldCount").innerHTML = goldCount;
			
			
			}
			
			
});
	
	
	
	

	
	
	
	//var basedonincidentsjson = ${basedonincidentsjson}+"";
	//var basedonincidentsjson = [{"siteTier":"Gold","siteCount":"03","id":3,"officeBuildingSiteColorList":[{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"}]},{"siteTier":"Silver","siteCount":"02","id":2,"officeBuildingSiteColorList":[{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"}]},{"siteTier":"Bronze","siteCount":"01","id":1,"officeBuildingSiteColorList":[{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":1,"siteName":"INFINKARPUN","colorCode":"green"}]}];
	//console.log(JSON.stringify(basedonincidentsjson));
	//var basedonslajson = ${basedonslajson};	
	//var basedonslajson2 = [{"id":"x","siteTier":"Gold Sites","siteCount":"02","slaColorList":[{"id":"01","siteName":"testsite","colorCode":"green","sitePercentileAverage":"98"},{"id":"01","siteName":"testsite","colorCode":"green","sitePercentileAverage":"98"}]},{"id":"y","siteTier":"Silver Sites","siteCount":"06","slaColorList":[{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"}]},{"id":"z","siteTier":"Bronze Sites","siteCount":"04","slaColorList":[{"id":"03","siteName":"testsite3","colorCode":"red","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"amber","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"amber","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"green","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"green","sitePercentileAverage":"40"}]}];
	//console.log(JSON.stringify(basedonslajson2));
	
	var siteFilters = {
		   'a_ttofycred' : true,
		   'a_ttofycamber': true,
		   'a_ttofycgreen' : true,
		   'a_ttofycredsla' : true,
		   'a_ttofycambersla': true,
		   'a_ttofycgreensla' : true
	};
	
	
	//alert(JSON.stringify(basedonincidentsjson));
	//alert(JSON.stringify(basedonslajson2));
	//console.log(JSON.stringify(basedonslajson2));

	
	
	
	
	
	
	
	
	
</script>

<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name" style="margin-top: -13px;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
				<span class="tt-current-page" style="margin-top: -13px;"><liferay-ui:message key="Sites"/></span>
			</div>
		</div>
		<div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.pv.homeURL")%>/<%=prop.getProperty("ttPVDashboardPageName")%>'" class="btn-primary-tt backButton" style="margin-top: -16px;">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
	</div>
</div>

<!--START DISPLAY SITES CONTAINER-->
<div class="container-fluid tt-site-display">
	<div class="row-fluid">
		<div class="span12">
			<div class="span2 tt-displaySite-title"><liferay-ui:message key="Display Sites:"/></div>
			<div class="span6">
				<div class="row-fluid tt-displaySite-check" id="filterContainer">
				<form id="filterForm">
					<div class="span1" id="filterAll"><input type="checkbox" id="all"  class="tt-displaysite-position" checked/><liferay-ui:message key="All"/></div>
					<div class="span2" id="filterOperational"><input type="checkbox" id="6"  class="tt-displaysite-position" /><liferay-ui:message key="Operational"/></div>
					<div class="span2" id="filterMonitoring"><input type="checkbox" id="5"  class="tt-displaysite-position" /><liferay-ui:message key="Monitoring"/></div>
					<div class="span3" id="filterDelivery"><input type="checkbox" id="4"  class="tt-displaysite-position" /><liferay-ui:message key="Delivery & Activation"/></div>
					<div class="span2" id="filterProcurement"><input type="checkbox" id="3"  class="tt-displaysite-position" /><liferay-ui:message key="Procurement"/></div>
					<div class="span2" id="filterDetailed"><input type="checkbox" id="2"  class="tt-displaysite-position" /><liferay-ui:message key="Detailed Design"/></div>
					<div class="span2" id="filterOrder"><input type="checkbox" id="1"  class="tt-displaysite-position" /><liferay-ui:message key="Order Received"/></div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid tt-site-tab-container">
	<div class="row-fluid tt-site-tab-incident-content" id="site-incident">
		<div class="span4 pt-20 tt-sites-gold-tab">
			<div class="row-fluid tt-site-tab-header">
				<div id="hdgold" class="span12">
					
						<div class="hd"><liferay-ui:message key="${goldSiteTier.siteTier} Sites"/></div>
						<div class="hd1" id="goldCount"><liferay-ui:message key="${goldSiteTier.siteCount}"/></div>
					
				</div>
			</div>
			<div class="row-fluid tt-site-gold-content-row">
				<div id="boxgold"  class="span12 tt-sites-gold-container">
					<c:if test="${isGoldAdded}">
						<c:set var="rowcount" value="${1}" />
						<c:set var="lengthofloop" value="${fn:length(goldSiteTier.officeBuildingSiteColorList)}" />
						<c:forEach items="${goldSiteTier.officeBuildingSiteColorList}" var="sitelist" varStatus="loopcount">
							<c:if test="${rowcount == 1}">
								<div class="row-fluid"> 
							</c:if>
								<c:choose>
									<c:when test="${rowcount == 1}"><div class="span6 tt-site-column tt-site-column-first a_ttofycred wrapword" name="${sitelist.deliveryStage}"  style="background-color: ${sitelist.colorCode}"></c:when>
									<c:otherwise><div class="span6 tt-site-column tt-site-column-second a_ttofycred wrapword"  style="background-color: ${sitelist.colorCode}" name="${sitelist.deliveryStage}"></c:otherwise>
								</c:choose>
									<a href="<portlet:actionURL name="viewSite"><portlet:param name="siteName" value="${sitelist.siteName}"/></portlet:actionURL>"
									><liferay-ui:message key="${sitelist.siteName}"/></a></div>
						
							 <c:choose>
								<c:when test="${lengthofloop == 1}"></div></c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${lengthofloop-1 == loopcount.index}"></div></c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${rowcount == 2}"></div>
													<c:set var="rowcount" value="${1}" />
												</c:when>
												<c:otherwise><c:set var="rowcount" value="${rowcount+rowcount}" />
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							 </c:choose>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</div>
		<div class="span4 pt-20 tt-sites-silver-tab">
			<div class="row-fluid tt-site-tab-header">
				<div id="hdsilver" class="span12">
					<div class="hd"><liferay-ui:message key="${silverSiteTier.siteTier} Sites"/></div>
					<div class="hd1" id="silverCount"><liferay-ui:message key="${silverSiteTier.siteCount}"/></div>
				</div>
			</div>
			<div class="row-fluid tt-site-silver-content-row">
				<div id="boxsilver"  class="span12 tt-sites-silver-container">
					<c:if test="${isSilverAdded}">
						<c:set var="rowcount" value="${1}" />
						<c:set var="lengthofloop" value="${fn:length(silverSiteTier.officeBuildingSiteColorList)}" />
						<c:forEach items="${silverSiteTier.officeBuildingSiteColorList}" var="sitelist" varStatus="loopcount">
							<c:if test="${rowcount == 1}">
								<div class="row-fluid"> 
							</c:if>
								<c:choose>
									<c:when test="${rowcount == 1}"><div class="tt-classexp span6 tt-site-column tt-site-column-first a_ttofycred wrapword" name="${sitelist.deliveryStage}" style="background-color: ${sitelist.colorCode}"></c:when>
									<c:otherwise><div class="tt-classexp span6 tt-site-column tt-site-column-second a_ttofycred wrapword" name="${sitelist.deliveryStage}" style="background-color: ${sitelist.colorCode}"></c:otherwise>
								</c:choose>
									<a href="<portlet:actionURL name="viewSite"><portlet:param name="siteName" value="${sitelist.siteName}"/>
												</portlet:actionURL>"
											><liferay-ui:message key="${sitelist.siteName}"/></a></div>

							 <c:choose>
								<c:when test="${lengthofloop == 1}"></div></c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${lengthofloop -1 == loopcount.index }"></div></c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${rowcount == 2}"></div>
													<c:set var="rowcount" value="${1}" />
												</c:when>
												<c:otherwise><c:set var="rowcount" value="${rowcount+rowcount}" />
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							 </c:choose>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</div>	
		<div class="span4 pt-20 tt-sites-bronze-tab">
			<div class="row-fluid tt-site-tab-header">
				<div id="hdbronze" class="span12">
					<div class="hd"><liferay-ui:message key="${bronzeSiteTier.siteTier} Sites"/></div>
					<div class="hd1" id="bronzeCount" ><liferay-ui:message key="${bronzeSiteTier.siteCount}"/></div>
				</div>
			</div>
			<div class="row-fluid tt-site-bronze-content-row">
				<div id="boxbronze" class="span12 tt-sites-bronze-container">
					<c:if test="${isBronzeAdded}">
						<c:set var="rowcount" value="${1}" />
						<c:set var="lengthofloop" value="${fn:length(bronzeSiteTier.officeBuildingSiteColorList)}" />
						<c:forEach items="${bronzeSiteTier.officeBuildingSiteColorList}" var="sitelist" varStatus="loopcount">
							<c:if test="${rowcount == 1}">
								<div class="row-fluid"> 
							</c:if>
								<c:choose>
									<c:when test="${rowcount == 1}"><div class="span6 tt-site-column tt-site-column-first a_ttofycred wrapword" name="${sitelist.deliveryStage}" style="background-color: ${sitelist.colorCode}"></c:when>
									<c:otherwise><div class="span6 tt-site-column tt-site-column-second a_ttofycred wrapword" name="${sitelist.deliveryStage}" style="background-color: ${sitelist.colorCode}"></c:otherwise>
								</c:choose>
								
									<a href="<portlet:actionURL name="viewSite"><portlet:param name="siteName" value="${sitelist.siteName}"/>
												</portlet:actionURL>"
											><liferay-ui:message key="${sitelist.siteName}"/></a></div>

					
							 <c:choose>
								<c:when test="${lengthofloop == 1}"></div></c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${lengthofloop -1 == loopcount.index}"></div></c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${rowcount == 2}"></div>
													<c:set var="rowcount" value="${1}" />
												</c:when>
												<c:otherwise><c:set var="rowcount" value="${rowcount+rowcount}" />
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							 </c:choose>
						</c:forEach>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	

</div>


<!--END SITE PAGE CONTAINER-->