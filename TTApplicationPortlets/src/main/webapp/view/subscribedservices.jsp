<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="common.jsp"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>
	<link href="${pageContext.request.contextPath}/css/subscribedservices.css"
	rel="stylesheet"></link>
<div class="container-fluid tt-ss-container" id="subscribeds">
	<div class="portletTitleBar"style="BORDER-BOTTOM: SOLID 1PX #CCC; margin-bottom: 10px;">
		<div class="titleText"><liferay-ui:message key="SUBSCRIBED SERVICES"/></div>
		<div class="navigationArrow">
			<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttsubscribedservicesdetail")%>">
				<img class="navArrowRightImg"
				src="/TTApplicationPortlets/images/Expand_G_24.png">
			</a>
		</div>
	</div>
	<div class="content" id="table-content">
		<table id="ssTable">
			<tr id="tt-sstable-header">
				<th><div id="tableHeader"><liferay-ui:message key="Service"/></div></th>
				<th><div id="tableHeader"><liferay-ui:message key="Status"/></div></th>
				<th><div id="tableHeader"><liferay-ui:message key="Contract Term"/></div></th>
			</tr>
			<c:forEach items="${subscribedServicesData}"
				var="subscribedServicesData">

				<tr id="tt-sstable-body">
					<c:choose>
						<c:when test="${subscribedServicesData.status=='NA'}">

							<td><div id="tableColumnContentGray">${subscribedServicesData.productname}</div></td>
							<td><div id="tableColumnContentGray">${subscribedServicesData.status}</div></td>
							<td><div id="tableColumnContentGray">
									${subscribedServicesData.contractduration}</div></td>
						</c:when>
						<c:otherwise>
							<td><div id="tableColumnContent">${subscribedServicesData.productname}</div></td>
							<td><div id="tableColumnContent">${subscribedServicesData.status}</div></td>
							<td><div id="tableColumnContent">
									${subscribedServicesData.contractduration}</div></td>

						</c:otherwise>

					</c:choose>

				</tr>
			</c:forEach>

		</table>

	</div>
</div>
