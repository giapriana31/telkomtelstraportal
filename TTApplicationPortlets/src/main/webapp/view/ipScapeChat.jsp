<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>


<style>
@media (max-width: 768px){
 
iframe {
 
    display: none;
}
 
}

iframe{
z-index: 9999  !important;
}

.validationError {
        color: #000000;
 }
 
</style>

<form:form id="ipScapeForm" class="form-horizontal tt-modal-form"
	modelAttribute="ipscapeObj" method="post">
	<form:input type="hidden" id="companyName" path="companyName"
		name="companyName" value="${companyName}" />
	<form:input type="hidden" id="emailId" path="emailId" name="emailId"
		value="${emailId}" />
	<form:input type="hidden" id="firstName" path="firstName"
		name="firstName" value="${firstName}" />
	<form:input type="hidden" id="telephone" path="telephone"
		name="telephone" value="${telephone}" />
</form:form>

<div id="ipScapeChat">
	        <div id="chat_tjn6du5s">
            <script type="text/javascript">
                (function() {
                    var cs = document.createElement('script'); cs.type = 'text/javascript'; cs.async = true;
                    cs.src = 'https://tas.ccc.telkomtelstra.id/webchat/chat-uri.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cs, s);
                    cs.onload = function(){
                        Chat.init({
                            'container': 'chat_tjn6du5s',
                            'key': 'TjN6dU5sRFl2UGxSWC9pWldoWkJFQT09',
                            'src': 'tas.ccc.telkomtelstra.id/webchat/',
							'userData': {"name":$('#firstName').val() , "phone1":$('#telephone').val(), "Company_Name":$('#companyName').val(), "email":$('#emailId').val()}
                        });
                    }
                })();
            </script>
        </div>
</div>


