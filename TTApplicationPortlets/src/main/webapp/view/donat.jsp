<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
       type="text/javascript"></script>
<script
       src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
       type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"
       type="text/javascript"></script>

<link href="${pageContext.request.contextPath}/css/main.css"
       rel="stylesheet"></link>
       
<%@ include file="common.jsp"%>
<portlet:defineObjects />

<style>
#donatPortletTitleBar{
		height:40px;
}

#donatTitleText{
		line-Height:37px;
}

.donat-title-dashboard{
	font-size: 15px;
	display: flex;
	justify-content: center;
}

#donatCountId{
	fill:#888 !important;
	font-size:20px !important;
}
#donatLabelId{
       fill:#888 !important;
       font-size:18px !important;
       font-weight: 100;
}

div#donut-graph-donat {
    	display: flex;
   		justify-content: center;
}

#markers-tt{
		display:flex;
		justify-content: center;
}

#tt-brown{
		background-color: #a52a2A; 
}

#tt-blue{
		background-color: #0000ff;
}

#tt-brown-marker {
       color: #a52a2a;
}

#tt-amber-marker {
       color: #ff9c00;
}

#tt-blue-marker {
       color: #0000ff;
}
.tt-color-legends {
       display: inline-block;
       width: 10px;
       height: 10px;
       position: relative;
       top: 5px;
       margin: 0 5px;
}
.tt-markers {
       display: inline-block;
       font-size: 12px;
}
.Legend-donat{
		 font-size: 15px;
       line-height: 120%;
       padding-top: 5px;
       padding-bottom: 30px;
       height: 30px;
       text-align: center;
}

#donatblock{
		justify-content: center;
		bankground-color:#fff;
		height: 262px;
		border-left: solid 1px #ccc;
		border-right: solid 1px #ccc;
		border-bottom: solid 1px #ccc; 
}
.donatServices{
	width:150px;
	margin: 0 40px 0 40px;
}
.value.donat{
	text-align: center;
	height: 50px;
	line-height: 50px;
	border: solid 1px black;
	border-radius: 8px;
}

#status-val-service{
	border-color: transparent;
}
</style>
<div
	style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
	<div id="donutPortletTitleBar" class="portletTitleBar">
		<div id="donutTitleText" class="titleText">
				<liferay-ui:message key="DONUT's"></liferay-ui:message>
		</div>
		<div id="donatNavigationArrow" class="navigationArrow">
			<a class="navArrowRight" href="/group/telkomtelstra/test">
				<img class="navArrowRightImg" src="/TTAplicationPortlets/images/Expand_G_24.png">
			
			</a>
		
		</div>
	</div>
</div>
<div id="donutblock">
	<div class="donut-title-dashboard"><liferay-ui:message key="Overall Donut Health"></liferay-ui:message>
	<div id="donut-graph-donut">
		 <svg width="260" height="200">
                     <g class="arc" transform="translate(140,100)">
                     <path stroke="white" stroke-width="0.5"
                           fill="#${serviceStatusSpecs[1]}"
                           d="M0,90A90,90 0 1,1 0,-90A90,90 0 1,1 0,90M0,80A80,80 0 1,0 0,-80A80,80 0 1,0 0,80Z"></path></g>
                     <g class="center_group" transform="translate(140,100)">
                     <circle fill="#eeeeee" r="80"></circle>
                     <circle fill="white" r="73"></circle>
                     <text id="donutLabelId" class="label" dy="15" text-anchor="middle">
                     <liferay-ui:message key="Delicious Donut" /></text>
                     <text id="donutCountId" class="total" dy="-5" text-anchor="middle">${serviceStatusSpecs[0]}</text></g></svg>
	</div>
		<div id="makers-tt">
				 <div class="tt-color-legends" id="tt-brown"></div>
	              <div class="tt-markers" id="tt-brown-marker"><liferay-ui:message key="Fried" /></div>
	              <div class="tt-color-legends" id="tt-amber"></div>
	              <div class="tt-markers" id="tt-amber-marker"><liferay-ui:message key="Original" /></div>
	              <div class="tt-color-legends" id="tt-blue"></div>
	              <div class="tt-markers" id="tt-blue-marker"><liferay-ui:message key="With blue almond" /></div>
		</div>
	</div>
</div>
<script></script>