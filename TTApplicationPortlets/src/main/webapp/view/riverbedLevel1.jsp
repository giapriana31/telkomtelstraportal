<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ include file="common.jsp" %>


<html>
<head>
<style>
button,html input[type="button"]{
border-radius:5px !important;
}
@font-face 
{
  font-family: 'MyWebFont';
  src: url('fonts/GothamRoundedBook.woff') format('woff'),
       url('fonts/GothamRoundedBook.ttf') format('truetype');
}
body
{
 font-family: 'MyWebFont';
}

	/* CSS Triangles - see Trevor's post */
	.wrapper .tooltip:after {
	  border-left: solid transparent 10px;
	  border-right: solid transparent 10px;
	  border-top: solid #D4D4D4 10px;
	  bottom: -10px;
	  content: " ";
	  height: 0;
	  left: 50%;
	  margin-left: -13px;
	  position: absolute;
	  width: 0;
	}
	  
	.wrapper:hover .tooltip {
	  font-size: 8px;
	    color: black;
	  opacity: 1;
	  pointer-events: auto;
	  -webkit-transform: translateY(0px);
	     -moz-transform: translateY(0px);
	      -ms-transform: translateY(0px);
	       -o-transform: translateY(0px);
	          transform: translateY(0px);
	}

	/* IE can just show/hide with no transition */
	.lte8 .wrapper .tooltip {
	  display: none;
	}
	
	.lte8 .wrapper:hover .tooltip {
	  display: block;
	}
}
@media  (min-width: 500px) {
	.wrapper{
			display:none !important;
	}
}

</style>
<script>
"use strict";jQuery.base64=(function($){var _PADCHAR="=",_ALPHA="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",_VERSION="1.0";function _getbyte64(s,i){var idx=_ALPHA.indexOf(s.charAt(i));if(idx===-1){throw"Cannot decode base64"}return idx}function _decode(s){var pads=0,i,b10,imax=s.length,x=[];s=String(s);if(imax===0){return s}if(imax%4!==0){throw"Cannot decode base64"}if(s.charAt(imax-1)===_PADCHAR){pads=1;if(s.charAt(imax-2)===_PADCHAR){pads=2}imax-=4}for(i=0;i<imax;i+=4){b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6)|_getbyte64(s,i+3);x.push(String.fromCharCode(b10>>16,(b10>>8)&255,b10&255))}switch(pads){case 1:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6);x.push(String.fromCharCode(b10>>16,(b10>>8)&255));break;case 2:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12);x.push(String.fromCharCode(b10>>16));break}return x.join("")}function _getbyte(s,i){var x=s.charCodeAt(i);if(x>255){throw"INVALID_CHARACTER_ERR: DOM Exception 5"}return x}function _encode(s){if(arguments.length!==1){throw"SyntaxError: exactly one argument required"}s=String(s);var i,b10,x=[],imax=s.length-s.length%3;if(s.length===0){return s}for(i=0;i<imax;i+=3){b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8)|_getbyte(s,i+2);x.push(_ALPHA.charAt(b10>>18));x.push(_ALPHA.charAt((b10>>12)&63));x.push(_ALPHA.charAt((b10>>6)&63));x.push(_ALPHA.charAt(b10&63))}switch(s.length-imax){case 1:b10=_getbyte(s,i)<<16;x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_PADCHAR+_PADCHAR);break;case 2:b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8);x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_ALPHA.charAt((b10>>6)&63)+_PADCHAR);break}return x.join("")}return{decode:_decode,encode:_encode,VERSION:_VERSION}}(jQuery));
</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/sites.css" ></link>


<portlet:defineObjects />

<script>
function RBHealth() {
	$('#RBHealth-li').addClass('selected');
	$('#data-RB').removeClass('hide');
	$('#RBCheckbox').removeClass('hide');
	$('#riverbedCountId').removeClass('hide');
}

var VMs = [{appliance:'EC5GK000F3BAA',jml_application:'19',avg_reduction:'69',colour:'background-color:#ff9c00',priority:'2'},
           {appliance:'EC5GK000F3BAA',jml_application:'25',avg_reduction:'75',colour:'background-color:rgb(163, 207, 98)',priority:'3'},
           {appliance:'EC5GK000F3BAA',jml_application:'25',avg_reduction:'73',colour:'background-color:rgb(163, 207, 98)',priority:'3'},
           {appliance:'EC5GK000F3BAA',jml_application:'22',avg_reduction:'74',colour:'background-color:rgb(163, 207, 98)',priority:'3'}];           
			

VMs = JSON.stringify('${ApplianceArrayJSON}');
VMs = eval('(' + '${ApplianceArrayJSON}' + ')');

var temp = VMs.slice();

$(document).ready(function(){
	
	var hash = document.location.hash;
	if(hash){
		if(hash === '#RBHealth'){
			RBHealth();
		}
	}
	else{
		RBHealth();
	}		

	VMs = VMs.sort(function IHaveAName(a, b) { return b.avg_reduction> a.avg_reduction?  1 : b.avg_reduction< a.avg_reduction? -1 : 0; });

	populateData(VMs);	
	
	$("#linkTable").addClass('hide');
				
})

	Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
		  return v[name] === value ? null : v;
	   });
	   this.length = 0;
	   this.push.apply(this, array);
	}
	
$(function() {
	
	$("input[type='checkbox']").change(
	function()	{
		
			temp = VMs.slice();			
			var all = false;
			var high = false;
			var moderate = false;
			var normal = false;
			
			if($('#checkbox_all').is(':checked')) 
			{		
				all = true;
			}
			if($('#checkbox_high').is(':checked')) 
			{		
				high = true;
			}
			if($('#checkbox_moderate').is(':checked')) 
			{		
				moderate = true;
			}
			if($('#checkbox_normal').is(':checked')) 
			{		
				normal = true;
			}

			if(all == false)
			{
				if(high==false)
					temp.removeValue('priority', '1');
				if(moderate==false)
					temp.removeValue('priority', '2');
				if(normal==false)
					temp.removeValue('priority', '3');
			}
			
			$('#searchText').val('');
			
			populateData(temp);			
	});	
	});
	

/* var IHaveAName = function IHaveAName(a, b) { 
	return b.avg_reduction.replace('.', ' ') < a.avg_reduction.replace('.', ' ') ?  1 : b.avg_reduction.replace('.', ' ') > a.avg_reduction.replace('.', ' ') ? -1 : 0;
} */

/* var sortByName = function sortByName(a, b) { 
	return b.avg_reduction.replace('.', ' ') < a.avg_reduction.replace('.', ' ') ?  1 : b.avg_reduction.replace('.', ' ') > a.avg_reduction.replace('.', ' ') ? -1 : 0;
} */
var sortByName = function sortByName(a, b) { 
	return a.avg_reduction - b.avg_reduction;
}


function populateData(temp)
{
	/* temp = temp.sort(IHaveAName); */		
	temp = temp.sort(sortByName);
	
	if(temp.length==0)
	{
		document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		return;
	}
	else
	{
		var no = 0;
		var cek = 0;
		var str = "";
		str = "<div class='row-fluid'>";
		for (i in temp)
		{
			if(temp[i].appliance!=null){
				var URL = '/group/telkomtelstra/riverbedlevel2?' + $.base64.encode(temp[i].appliance+'&'+temp[i].avg_reduction);
				str += "<div class='span3'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td align='left'><div>"+temp[i].sitename+"</div></td></tr><tr><td align='left'><div class='span9'><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].appliance+"</a></div><div>"+temp[i].avg_reduction+"%</div></td></tr><tr><td align='center'><div class='progress tt-progess'><div id ='goldBar' class='bar' style='width: "+temp[i].avg_reduction+"%;background-image: -webkit-linear-gradient(top,#67B5EB 0,#67B5EB 100%);color:black'></div></div></td></tr></table></div></div>";
				no++;
				cek++;
				if(no==4)
				{
					no=0;
					str += "</div><div class='row-fluid'>";			 
				}
			}
		}
		str += "</div>";
		
		if(cek==0){
			document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-RB").innerHTML = str;
			document.getElementById("riverbedCountId").innerHTML = cek;
		}
	}
}

</script>

</head>
<body>
<div class="container-fluid tt-page-detail">
	<div class="tt-riverbedlevel1" >
   		<div class="tt-riverbedlevel1-header"> 
		   <img src="/TTApplicationPortlets/images/u91.png"> 
		   <font style="font-size:100%;vertical-align:middle;margin-left:1%;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</font>
		   <font style="font-size:125%;vertical-align:middle;"><liferay-ui:message key="Appliance Optimization"/></font> 
			<div class="span8 tt-backbutton" style="float: right;">
				<input type="button" value="&nbsp;&lt;&nbsp;<liferay-ui:message key='Back'/>" onclick="window.location='/group/telkomtelstra/dashboard'" class="tt-riverbed-inputbutton" >
			</div>
		</div>
        
    </div>
	<div class="row-fluid tt-page-content">
		<ul id="tabc" class="nav nav-pills tt-site-ul">
			<li class="active tt-site-nav-pills selected" id="RBHealth-li">
				<a data-toggle="tab" href="#RBHealth" class="tt-site-nav-pills" id="basedOnTicket" onclick="RBHealth()"><liferay-ui:message key="Base on Appliance Optimization"/></a>
			</li>
		</ul>
	</div>
</div>

<div class="container-fluid tt-site-display" id="RBCheckbox">
	<div class="row-fluid">
		<div class="span12 tt-sitecheck">
			<div class="span2 tt-displaySite-title"><liferay-ui:message key="Display"/></div>
			<div class="span6">
				<div class="row-fluid tt-displaySite-check">
					<div class="span1" style="display: flex;"><input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/><liferay-ui:message key="All"/></div>
					<div class="span3" style="display: flex;"><input type="checkbox" id="checkbox_high" class="tt-displaysite-position" /><liferay-ui:message key="Low Optimization"/></div>
					<div class="span4" style="display: flex;"><input type="checkbox" id="checkbox_moderate" class="tt-displaysite-position" /><liferay-ui:message key="Moderate Optimization"/></div>
					<div class="span4" style="display: flex;"><input type="checkbox" id="checkbox_normal" class="tt-displaysite-position" /><liferay-ui:message key="High Optimization"/></div>					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid tt-site-tab-container">

<div class="span12 tt-graph tt-graph-first tt-gold">
	<div style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
       <div id="riverbedGoldPortletTitleBar" class="portletTitleBar">
              <b><div id="riverbedTitleText" class="span3 titleText">
              			<liferay-ui:message key="Total Appliance" />        
              </div></b>
              <div>
                     <b><text id="riverbedCountId" class="total" dy="5" text-anchor="middle"></text></b></g></svg> 
              </div>
              
       </div>
	</div>
	<div
       style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
		<div id="data-RB" style="padding:10px;background-color: white;">
		</div>
	</div>
</div>
</div>

</body>
</html>