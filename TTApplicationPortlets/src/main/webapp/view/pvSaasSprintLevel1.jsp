


<!--END SITE PAGE CONTAINER--> <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getUserData">
</portlet:resourceURL>
<portlet:resourceURL var="filterSitesURL" id="filterSitesURL" />

<%@ include file="common.jsp" %>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>

	
	<%-- <link href="${pageContext.request.contextPath}/css/PVSiteSprintLevel2.css" --%>
	<link href="${pageContext.request.contextPath}/css/sites.css"
	rel="stylesheet"></link>
<!-- <style>

a{
color:#fff !important;
text-decoration:none !important;
}
</style>	
	 -->
<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/sites.js"></script>






<script>

function RBHealth() {
	$('#RBHealth-li').addClass('selected');
	$('#RBCheckbox').removeClass('hide');
}

var VMs = [{priority:'2',productname:'MNS',ciname:'EC5GK000F3BAA',colour:'background-color:#ff9c00'}];


VMs = JSON.stringify('${PVSaasProductLevel1}');
VMs = eval('(' + '${PVSaasProductLevel1}' + ')');

var temp = VMs.slice();

$(document).ready(function(){
	
	var hash = document.location.hash;
	if(hash){
		if(hash === '#RBHealth'){
			RBHealth();
		}
	}
	else{
		RBHealth();
	}		
	
	//VMs = VMs.sort(function IHaveAName(a, b) { return b.priority> a.priority?  1 : b.priority< a.priority? -1 : 0; });

	populateData(VMs);	
	
	$("#linkTable").addClass('hide');
				
})

Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
		  return v[name] === value ? null : v;
	   });
	   this.length = 0;
	   this.push.apply(this, array);
	}
	
$(function() {
	
	$("input[type='checkbox']").change(
	function()	{
		
			temp = VMs.slice();	
			var all = false;
			var deliverystage1 = false;
			var deliverystage2 = false;
			var deliverystage3 = false;
			var deliverystage4 = false;
			var deliverystage5 = false;
			var deliverystage6 = false;
			
			if($('#checkbox_all').is(':checked')) 
			{		
				all = true;
			}
			if($('#checkbox_1').is(':checked')) 
			{		
				deliverystage1 = true;
			}
			if($('#checkbox_2').is(':checked')) 
			{		
				deliverystage2 = true;
			}
			if($('#checkbox_3').is(':checked')) 
			{		
				deliverystage3 = true;
			}
			if($('#checkbox_4').is(':checked')) 
			{		
				deliverystage4 = true;
			}
			if($('#checkbox_5').is(':checked')) 
			{		
				deliverystage5 = true;
			}
			if($('#checkbox_6').is(':checked')) 
			{		
				deliverystage6 = true;
			}
			
			if(all == false)
			{
				if(deliverystage1==false)
					temp.removeValue('deliverystage', '1');
				if(deliverystage2==false)
					temp.removeValue('deliverystage', '2');
				if(deliverystage3==false)
					temp.removeValue('deliverystage', '3');
				if(deliverystage4==false)
					temp.removeValue('deliverystage', '4');
				if(deliverystage5==false)
					temp.removeValue('deliverystage', '5');
				if(deliverystage6==false)
					temp.removeValue('deliverystage', '6');
			}
			
			$('#searchText').val('');
			
			populateData(temp);			
	});	
	});
	
var sortByName = function sortByName(a, b) { 
	return a.productname - b.productname;
}

var sortByProduct = function sortByProduct(a, b) { 
	return a.deliverystage - b.deliverystage;
}

function populateData(temp)
{	
	temp = temp.sort(sortByName);
	temp = temp.sort(sortByProduct);
	
	if(temp.length==0)
	{
		document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		return;
	}
	else
	{
		var no = 0;
		var cek = 0;
		var str = "";
		var cekProductName = "";
		
		str = "<div class='row-fluid'>";
		for (i in temp)
		{
			if(temp[i].deliverystage=='1'||temp[i].deliverystage=='2'||temp[i].deliverystage=='3'||temp[i].deliverystage=='4'||temp[i].deliverystage=='5'||temp[i].deliverystage=='6'){
				cek++;

				if(cekProductName==''){
					cekProductName=temp[i].productname;
				}
				
				
					if(cekProductName==temp[i].productname){
						no++;
						if(no==3)
						{
							no=0;
							str += "<div class='span4'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><br><tr><td height='10' align='center'><div class='span14'><a style='color:#ffffff!important;text-decoration:initial!important; font-size : 14px;'>"+temp[i].productname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#ffffff!important;text-decoration:initial!important; font-size : 18px;'>"+temp[i].ciname+"</a></div></td></tr></table></div></div>";
							str += "</div><div class='row-fluid'>";			 
						}else{
							str += "<div class='span4'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><br><tr><td height='10' align='center'><div class='span14'><a style='color:#ffffff!important;text-decoration:initial!important; font-size : 14px;'>"+temp[i].productname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#ffffff!important;text-decoration:initial!important; font-size : 18px;'>"+temp[i].ciname+"</a></div></td></tr></table></div></div>";
						}
					}else{
						no=0;
						no++;
						cekProductName=temp[i].productname;
						str += "</div><div class='row-fluid'>";	
						str += "<div class='span4'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><br><tr><td height='10' align='center'><div class='span14'><a style='color:#ffffff!important;text-decoration:initial!important; font-size : 14px;'>"+temp[i].productname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#ffffff!important;text-decoration:initial!important; font-size : 18px;'>"+temp[i].ciname+"</a></div></td></tr></table></div></div>";
						}
						
			}
			
		}
		str += "</div>";
		
		if(cek==0){
			document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-RB").innerHTML = str;
		}
	}
}

function getQueryString(url){
	if (url.indexOf('?') > -1) {
		var pos = url.indexOf('?');
		var extract = url.substring(pos + 1,url.length);
		return extract;
	}
	else{
		return -1;
	}
}

</script>
<style>

#markers-tt {
       display: flex;
       justify-content: center;
}

#tt-red {
       background-color: #e32212;
}

#tt-amber {
       background-color: #ff9c00;
}

#tt-green {
       background-color: #a3cf62;
}

#tt-yellow {
       background-color: #ffff00;
}

#tt-black-marker {
       color: #000000;
}

#tt-red-marker {
       color: #e32212;
}

#tt-amber-marker {
       color: #ff9c00;
}

#tt-green-marker {
       color: #a3cf62;
}

#tt-yellow-marker {
       color: #ffff00;
}

#tt-order-received {
       color: #000;
}

#tt-detailed-design {
       color: #A40800;
}

#tt-procurement {
       color: #7030A0;
}

#tt-delivery-activation {
       color: #00B0F0;
}

#tt-monitoring {
       color: #FA8072;
}

#tt-operational {
       color: #89C35C;
}

.tt-color-legends {
       display: inline-block;
       width: 10px;
       height: 10px;
       position: relative;
       top: 5px;
       margin: 0 5px;
}

.tt-markers {
       display: inline-block;
       font-size: 12px;
}

.legend-site {
       font-size: 15px;
       line-height: 120%;
       padding-top: 5px;
       padding-bottom: 30px;
       height: 30px;
       text-align: center;
}

.tt-page-detail{
		background-color: #ffffff;
		border-bottom: 1px #cccccc solid;
		padding: 20px !important;
		}

.tt-back{
			width: 40%;
			float: right;
			text-align: right;
		}

.tt-page-breadcrumb{
			padding-top: 20px;
		}

.tt-img-dashboard{
			/*DashBoard_G_32.png*/
			background-image: url(http://localhost:8080/TTApplicationPortlets/images/DashBoard_G_32.png);
			display: inline-block;
			height: 35px;
			background-repeat: no-repeat;
			padding-left: 40px;
			padding-top: 5px;
			vertical-align: top;
		}	

.tt-previous-page{
			font-size: 14px;
			color: #333333;
			vertical-align: top;
			padding-top: 5px;
			display: inline-block;
		}
		
.tt-current-page{
			font-size: 24px;
			color: #333333;
			vertical-align: top;
			padding-top: 5px;
			display: inline-block;
		}
.dashImageHolder{
	position:relative;
	float:left;
	margin-left: 20px;
}
.dashboardTitleHolder{
	position:relative;
}
.dashboardText{
	position:relative;
	font-size:24px;
	float: left;
	padding-left: 20px;
	margin-top: 5px;
}	

#filterForm {
    display: flex;
    justify-content: center;
}				

.row-fluid {
    width: 100%;
    margin-bottom: 1%;
}
		
</style>

<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
		<div class="row-fluid">
			<div class="span6 tt-breadcrumb">
				<div class="dashImageHolder">
					<img class=""
						src="<%=request.getContextPath()%>/images/DashBoard_G_32.png" />
				</div>&nbsp;
				<span class="tt-previous-page" style="margin-top: 1px;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
	
				<span class="tt-current-page" style="margin-top: 1px;"><liferay-ui:message key="SaaS"/></span>
			</div>
			<div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.pv.homeURL")%>/<%=prop.getProperty("ttPVDashboardPageName")%>'" class="btn-primary-tt backButton" style="margin-top: 1px;">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
		</div>
	</div>

<!--START DISPLAY SITES CONTAINER-->
<div class="row-fluid tt-displaySite-check">
 	<div id="markers-tt">
		     			<div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="Display Service : " /></div>&nbsp;&nbsp;
						<input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/>
		              	<div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="All" /></div>&nbsp;&nbsp;
		               	<input type="checkbox" id="checkbox_1" class="tt-displaysite-position"/>
		              	<div class="tt-markers" id="tt-order-received"><liferay-ui:message key="Order Received" /></div>
		              	<input type="checkbox" id="checkbox_2" class="tt-displaysite-position"/>
		              	<div class="tt-markers" id="tt-detailed-design"><liferay-ui:message key="Detailed Design" /></div>&nbsp;&nbsp;
		              	<input type="checkbox" id="checkbox_3" class="tt-displaysite-position"/>
		              	<div class="tt-markers" id="tt-procurement"><liferay-ui:message key="Procurement" /></div>&nbsp;&nbsp;
		              	<input type="checkbox" id="checkbox_4" class="tt-displaysite-position"/>
		              	<div class="tt-markers" id="tt-delivery-activation"><liferay-ui:message key="Delivery & Activation" /></div>&nbsp;&nbsp;
		              	<input type="checkbox" id="checkbox_5" class="tt-displaysite-position"/>
		              	<div class="tt-markers" id="tt-monitoring"><liferay-ui:message key="Monitoring" /></div>&nbsp;&nbsp;
		              	<input type="checkbox" id="checkbox_6" class="tt-displaysite-position"/>
		             	<div class="tt-markers" id="tt-operational"><liferay-ui:message key="Operational" /></div>&nbsp;&nbsp;
       	</div>
   </div>

<div class="container-fluid tt-site-tab-container">

<div class="span12 tt-graph tt-graph-first tt-gold">
	<div style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
	<div class="row-fluid">
			<div id="data-RB" class="span12" style="padding:10px;background-color: white;">	
				</div>
		</div>
	
	</div>
</div>

</div>
<!--END SITE PAGE CONTAINER-->