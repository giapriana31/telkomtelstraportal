
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<style>
@media screen and (max-width: 1268px) and (min-width: 800px){
#column-5 {
    margin-right: 11px !important;
}
}

@media screen and (min-width: 1269px){
#column-6 {
    width: 55% !important;
    margin-top: -160px !important;
    margin-left: 2% !important;
}
}


@media screen and (max-width: 389px) and (min-width: 320px){
#column-6 {
    margin-left: 5% !important;
    width: 94% !important;
}
}
@media screen and (max-width: 768px) and (min-width: 390px){
#column-6 {
    margin-left: 3% !important;
    width: 94% !important;
}
}
@media screen and (max-width: 799px) and (min-width: 768px){
#column-6 {
    margin-left: 5% !important;
    width: 95% !important;
}
}
@media screen and (max-width: 1268px) and (min-width: 800px){ 
#column-6 {
      width: 48% !important;
	margin-left: 2% !important;
}
}

@media screen and (min-width: 1269px) and (max-width: 1500px){
#column-6 {
	
    margin-top: -145px !important;
    margin-left: 2% !important;
    width: 54% !important;
}
}

@media screen and (min-width: 1501px) and (max-width: 1700px)
{
#column-6 {
    margin-left: 2% !important;
    margin-top: -135px !important;
    width: 54% !important;
}
}

@media screen and (min-width: 1701px) and (max-width: 2100px)
{
#column-6 {
    margin-left: 2% !important;
    margin-top: -9%;
    width: 54% !important;
}

}

.tt-div-highlight-yellow {
    background-color: #ffff00;
    border-radius: 3px;
    padding: 10px 15px;
    border: 0;
    color: #fff;
    display: inline-block;
    font-size: 14px;
}

.tt-div-highlight-green {
    background-color: #a3cf62;
    border-radius: 3px;
    padding: 10px 15px;
    border: 0;
    color: #fff;
    display: inline-block;
    font-size: 14px;
}

.dashTickSpan1 {
    /* width: auto; */
    width: 1px !important;
    cursor: default;
    text-align: center !important;
}
   

</style>



<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js" type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/js/jquery.validate-1.9.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/ttlogticket.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ttlogticket.css">

<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js" type="text/javascript"></script>
	
<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="logIncidentModal.jsp"%>

<!--START TICKETS CONTAINER-->
<div class="container-fluid tt-container tt-logtickets">
	<!--START TICKETS HEADER-->
	<div class="row-fluid tt-header">
		<div class="span6 tt-page-title"><liferay-ui:message key="INCIDENTS"/></div>
		<div class="span6 tt-detail">
			<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttIncidentPageName") %>">
				<img class="navArrowRightImg" src="<%=request.getContextPath() %>/images/Link_G_24.png"/>
			</a>
		</div>
	</div>
	<!--END TICKETS HEADER-->
	<!--START TICKETS CONTENT---->
	<!--START COUNT 1-->
	<%-- <div class="row-fluid tt-logticket-content">
		<div class="span12 tt-summary1">
			<div>
				<div class="tt-div-highlight-red tt-summary1-count">${countP1P2}</div>
				<div style="display:inline-block" class="tt-summary1-text"><liferay-ui:message key="Incidents with High Impact"/></div>
			</div>
		</div>
		
	</div> --%>
	<!--END COUNT 1-->
	<!--START COUNT 2-->
	<div class="row-fluid tt-logticket-content">
		<div class="span12 tt-summary2">
			<div class="span8">
				<div class="tt-div-highlight-red tt-summary1-count">P1</div>
				<div class="tt-div-highlight-orange tt-summary1-count">P2</div>
				<div class="tt-div-highlight-yellow tt-summary1-count">P3</div>
				<div class="tt-div-highlight-green tt-summary1-count">P4</div>
				<%-- <div class="tt-summary2-text"><liferay-ui:message key="Incidents with Moderate Impact"/></div> --%>
			</div>
			<div>
				<button type="button" id="logTicketButton" class="img-create-incident" data-toggle="modal"><liferay-ui:message key="Log a New Incident"/></button>
			</div>
		</div>		
	</div>
	<!--END COUNT 2-->
	<!--START TICKET TABLE-->
		<div class="dashTicketsCont">
 		   	<div class="table-responsive">
				<table class="table" id="ticketTableOverflow">
				<c:forEach var = "listValue" items = "${incidentRowDetailsList}">
				<tr class="customTD">
					<td class="dashTickSpan1">P${listValue.priority}
					<%-- <c:choose>
						<c:when test="${listValue.customerimpacted == true}">			
							<td class="dashTickSpan1 priorityColorRed">P${listValue.priority}
							
						</c:when>
						<c:otherwise>
							<td class="dashTickSpan1 priorityColorAmber">P${listValue.priority}
						</c:otherwise>
					</c:choose> --%>
					<c:if test="${listValue.showEscalationFlag == true}">
						<br>
						<div class="escFlagIcon">
							<img src="<%=request.getContextPath() %>/images/u550.png"/>
						</div>
					</c:if>
					</td>
	      			<td class="dashTickSpan5">
						<div class="container-fluid">
								<div class="row-fluid">
								<div class="span3">
									<div class="statusClass">${listValue.incidentid}</div>
								</div>
								<div class="span7">
									<div class="escFlagClass">
										<div class="elipsTicketSummary" title="${listValue.summary}">${listValue.summary}</div>
									</div>
								</div>
								<div class="span2">
									<div class="statusClass">${listValue.incidentstatus}</div>
								</div>
								<%-- <div class="span2">
									<div class="statusClass">${listValue.category}</div>
								</div> --%>
							</div>
						</div>
					</td>
    				
    			</tr>
 			 </c:forEach>
    		</table>
    	</div>
 	</div>
	<!--END TICKET TABLE-->
	<!--END TICKETS CONTENT---->
</div>
<!--END TICKETS CONTAINER-->