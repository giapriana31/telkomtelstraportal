
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
	type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/js/jquery-ui.js" type="text/javascript"></script>
	

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ttLogServiceRequest.css" />

<%@ include file="common.jsp" %>
<portlet:defineObjects />

<portlet:actionURL var="submitFormURL" name="handleLogSR" />
<portlet:resourceURL var="getSiteURL" id="getSiteURL" />
<portlet:resourceURL var="getServiceForProductTypeURL" id="getServiceForProductTypeURL" />
<portlet:resourceURL var="getDeviceURL" id="getDeviceURL" />
<portlet:resourceURL var="getSubCategoryURL" id="getSubCategoryURL" />
<portlet:resourceURL var="submitRequestURL" id="submitRequestURL" />
<portlet:resourceURL id="prodClassforSelectedProductTypeURL" var="prodClassforSelectedProductTypeURL"/>

<script type="text/javascript">
    var selectedProductType = "";
    var slectedServiceName = "";
    var selectedSiteID = "";
	var selectedSubCategory = "";

	var siteData = "";


function getSelectedproductType(){
	 selectedProductType = $('#productSelect').val();
    //alert("before");
	 getProdClassforSelectedProductType();
	//alert("after");
	
	 slectedServiceName = "";
	 selectedSiteID = "";
	 selectedSubCategory = "";
						
       //alert("onChange of ProductType::selectedProductType::>>"+selectedProductType);
       //alert("onChange of ProductType::slectedServiceName::>>"+slectedServiceName);	
       //alert("onChange of ProductType::selectedSiteID::>>"+selectedSiteID);	
       //alert("onChange of ProductType::selectedSubCategory::>>"+selectedSubCategory);	
								
			if (selectedProductType.toUpperCase()==="MNS"){
				
				$('#regionRow').show();
				$('#siteNameRow').show();
				$('#regionTitle').show();
				$('#siteNameTitle').show();
				$('#regionSelect').show();
				$('#siteID').show();
				$('#MSI_MNS').show();
                $('#MSI_PC').hide();
                $('#serviceNameRow').show();
				$('#categoryRow').show();
				$('#deviceNameRow').show();
				$('#typeRow').show();

				//onChange
				$("#siteID").change(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				//onBlur
				$("#siteID").blur(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				//onSelect
				$("#siteID").select(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				//onKeyup
				$("#siteID").keyup(function(){
					selectedSiteID = $('#siteID').val();
					getServiceForProductType();
					
				});
				
			}else if(selectedProductType.toUpperCase()==="PRIVATE CLOUD"){
				$('#regionRow').hide();
				$('#siteNameRow').hide();	
				$('#MSI_MNS').hide();
                $('#MSI_PC').show();
                $('#serviceNameRow').show();
				$('#categoryRow').show();
				$('#deviceNameRow').show();
				$('#typeRow').show();
				
				    selectedSiteID = ""; 
					getServiceForProductType();
			}else if(selectedProductType.toUpperCase()==="WHISPIR" || selectedProductType.toUpperCase()==="IPSCAPE" || selectedProductType.toUpperCase()==="MANDOE"){
				$('#regionRow').hide();
				$('#siteNameRow').hide();	
				$('#MSI_MNS').hide();
                $('#MSI_PC').hide();
                $('#serviceNameRow').show();
				$('#categoryRow').show();
				$('#deviceNameRow').hide();
				$('#typeRow').hide();

				selectedSiteID = ""; 
				getSaasServiceForProductType();
			}
			else{
              	$('#regionRow').hide();
				$('#siteNameRow').hide();	
				$('#MSI_MNS').hide();
               	$('#MSI_PC').hide();
               	$('#serviceNameRow').show();
				$('#categoryRow').show();
				$('#deviceNameRow').show();
				$('#typeRow').show();
                $('#deviceNameSelect option[value!="0"]').remove();
                $('#actionSelect option[value!="0"]').remove();
                $('#subCategorySelect option[value!="0"]').remove();
                $('#actionSelect').removeAttr('disabled');
				$('#actionSelect').removeClass("disableButton");
				}
							
	}

	function getSaasServiceForProductType() {
		
		var getServiceForProductTypeURL = "${getServiceForProductTypeURL}";
		var jsonSiteID = {
				"selectedProductID" : selectedProductType ,
				"selectedSiteID" : selectedSiteID			
				};
		
         //alert("selectedProductID in getServiceForProductType::"+selectedProductType);
         //alert("selectedSiteID in getServiceForProductType::"+selectedSiteID);
         
		$.ajax({
			url : getServiceForProductTypeURL,
			type : "POST",
			data : jsonSiteID,
			success : function(response) {
			//alert('success '+response);
				$('#serviceNameSelect option[value!="0"]').remove();
				var x = 1;
				var arr = response.split("#####");
				var len = arr.length;
				var reslen = len - x;
				for (var i = 0; i < reslen; i++) {

					$('#serviceNameSelect').append(
							$('<option>')
									.text(
											response.split("#####")[i]
													.split('@@@@@')[1]).val(
											response.split("#####")[i]
													.split('@@@@@')[1]));
				}
				$('#serviceNameSelect option[value=""]').remove();
			},
			error : function(xhr) {
				//alert("Error"+xhr);
			}
		});
	}
	
	function getSite(dropdown) {
	siteData = new Array();
		var selectedValue = dropdown.options[dropdown.selectedIndex].value;
		/* alert('Selected Value + ' + selectedValue); */
		var getSiteURL = "${getSiteURL}";
		var jsonCategory = {
			"selectedRegion" : selectedValue
		};
		var dataJson = {
			selectedRegionJson : JSON.stringify(jsonCategory)
		};
		$.ajax({
			url : getSiteURL,
			type : "POST",
			data : dataJson,
			success : function(response) {
				$('#siteID option[value!="0"]').remove();
				$('#serviceNameSelect option[value!="0"]').remove();
				$('#deviceNameSelect option[value!="0"]').remove();

				var x = 1;
				var arr = response.split("#####");
				var len = arr.length;
				var reslen = len - x;
				for (var i = 0; i < reslen; i++) {

					
				siteData.push(response.split("#####")[i].split('@@@@@')[1]);
				}
				BindControls();
				//siteData=siteData+"]";
				//alert("hhhh" +siteData );
				$('#siteID option[value=""]').remove();
				
			},
			error : function(xhr) {
				BindControls();
				$('#siteID option[value!="0"]').remove();
			}
		});
	}

function getServiceForProductType() {
		
		var getServiceForProductTypeURL = "${getServiceForProductTypeURL}";
		var jsonSiteID = {
				"selectedProductID" : selectedProductType ,
				"selectedSiteID" : selectedSiteID			
				};
		
         //alert("selectedProductID in getServiceForProductType::"+selectedProductType);
         //alert("selectedSiteID in getServiceForProductType::"+selectedSiteID);
         
		$.ajax({
			url : getServiceForProductTypeURL,
			type : "POST",
			data : jsonSiteID,
			success : function(response) {
			//alert('success '+response);
				$('#serviceNameSelect option[value!="0"]').remove();
				var x = 1;
				var arr = response.split("#####");
				var len = arr.length;
				var reslen = len - x;
				for (var i = 0; i < reslen; i++) {

					$('#serviceNameSelect').append(
							$('<option>')
									.text(
											response.split("#####")[i]
													.split('@@@@@')[1]).val(
											response.split("#####")[i]
													.split('@@@@@')[1]));
				}
				$('#serviceNameSelect option[value=""]').remove();
			},
			error : function(xhr) {
				//alert("Error"+xhr);
			}
		});
	}
	
function getProdClassforSelectedProductType() {
	
    //alert("Yo Bro");
	var prodClassforSelectedProductTypeURLVar = "${prodClassforSelectedProductTypeURL}";
	var jsonSiteID = {
			"selectedProductID" : selectedProductType ,		
			};
	
     //alert("selectedProductID in getProdClassforSelectedProductType::"+selectedProductType);

	$.ajax({
		url : prodClassforSelectedProductTypeURLVar,
		type : "POST",
		data : jsonSiteID,
		success : function(response) {
		//alert('success '+response);
			$('#productClassificationSelect option[value!="0"]').remove();
			var x = 1;
			var arr = response.split("#####");
			var len = arr.length;
			var reslen = len - x;
			for (var i = 0; i < reslen; i++) {

				$('#productClassificationSelect').append(
						$('<option>')
								.text(
										response.split("#####")[i]
												.split('@@@@@')[1]).val(
										response.split("#####")[i]
												.split('@@@@@')[1]));
			}
			$('#productClassificationSelect option[value=""]').remove();
		
		
		},
		error : function(xhr) {
			//alert("Error"+xhr);
		}
	});
}
	
</script>
<script type="text/javascript">
	$(document)
			.ready(function() {
						var getSubCategoryURL = "${getSubCategoryURL}";
						$('#GenericRadio')
								.click(
										function() {

											$('#actionSelect').attr('disabled',
													'disabled');
											$('#actionSelect').addClass(
													"disableButton");
											/* $('#deviceNameSelect option[value!="0"]').remove(); */
											/* $('#deviceNameSelect').attr(
													'disabled', 'disabled');
											$('#deviceNameSelect').addClass(
													"disableButton"); */
											document.getElementById("customDescription").value = "";
											/* $("#MultiServiceCheckBox").attr(
													"checked", false); */

											var selectedValue = document.getElementById("serviceNameSelect").value;
											var category = "Generic";
											var jsonService = {
												"selectedService" : selectedValue,
												"categoryValue" : category,
												"selectedProductID" : selectedProductType 
											};
											var dataJson = {
												selectedServiceJson : JSON
														.stringify(jsonService)
											};

											$
													.ajax({
														url : getSubCategoryURL,
														type : "POST",
														data : dataJson,
														success : function(
																response) {
															$(
																	'#actionSelect option[value!="0"]')
																	.remove();
															$(
																	'#subCategorySelect option[value!="0"]')
																	.remove();
															var x = 1;

															var actionArr = response
																	.split("%%%%%")[0];
															var subCategoryArr = response
																	.split("%%%%%")[1];
														
															var len = subCategoryArr
																	.split("#####").length;
															var reslen = len
																	- x;
															for (var i = 0; i < reslen; i++) {
																if (subCategoryArr
																		.split("#####")[i]
																		.split('@@@@@')[0] == "") {
																	continue;
																}
																$(
																		'#subCategorySelect')
																		.append(
																				$(
																						'<option>')
																						.text(
																								subCategoryArr
																										.split("#####")[i]
																										.split('@@@@@')[1])
																						.val(
																								subCategoryArr
																										.split("#####")[i]
																										.split('@@@@@')[0]));
															}
															
															$(
																	'#subCategorySelect option[value=""]')
																	.remove();
														},
														error : function(xhr) {
															
															$(
																	'#subCategorySelect option[value!="0"]')
																	.remove();
															
														}
													});

										});
						$('#MACDRadio')
								.click(
										function() {
											$('#actionSelect').removeAttr(
													'disabled');
											$('#actionSelect').removeClass(
													"disableButton");

											
											var selectedValue = document.getElementById("serviceNameSelect").value;

											var category = "MACD";
											var jsonService = {
												"selectedService" : selectedValue,
												"categoryValue" : category,
												"selectedProductID" : selectedProductType ,
											};
											var dataJson = {
												selectedServiceJson : JSON
														.stringify(jsonService)
											};

											$
													.ajax({
														url : getSubCategoryURL,
														type : "POST",
														data : dataJson,
														success : function(
																response) {

															$(
																	'#actionSelect option[value!="0"]')
																	.remove();
															$(
																	'#subCategorySelect option[value!="0"]')
																	.remove();
															var x = 1;
															
															var actionArr = response
																	.split("%%%%%")[0];
															var subCategoryArr = response
																	.split("%%%%%")[1];
															
															var len = actionArr
																	.split("#####").length;
															var reslen = len
																	- x;
															for (var i = 0; i < reslen; i++) {
																if (actionArr
																		.split("#####")[i]
																		.split('@@@@@')[0] == "") {
																	continue;
																}
																$(
																		'#actionSelect')
																		.append(
																				$(
																						'<option>')
																						.text(
																								actionArr
																										.split("#####")[i]
																										.split('@@@@@')[1])
																						.val(
																								actionArr
																										.split("#####")[i]
																										.split('@@@@@')[0]));
															}
															len = subCategoryArr
																	.split("#####").length;
															reslen = len - x;
															for (var i = 0; i < reslen; i++) {
																if (subCategoryArr
																		.split("#####")[i]
																		.split('@@@@@')[0] == "") {
																	continue;
																}
																$(
																		'#subCategorySelect')
																		.append(
																				$(
																						'<option>')
																						.text(
																								subCategoryArr
																										.split("#####")[i]
																										.split('@@@@@')[1])
																						.val(
																								subCategoryArr
																										.split("#####")[i]
																										.split('@@@@@')[0]));
															}
														},
														error : function(xhr) {
															$(
																	'#actionSelect option[value!="0"]')
																	.remove();
															$(
																	'#subCategorySelect option[value!="0"]')
																	.remove();
															/* alert('Error' + xhr); */
														}
													});
										});
					});
</script>
<script type="text/javascript">
	function getDevices(dropdown) {
	
	  selectedSubCategory = $('#subCategorySelect').val();
	   //alert("::::::"+selectedSubCategory);
	  slectedServiceName = $('#serviceNameSelect').val();;
		var getDeviceURL = "${getDeviceURL}";
		var jsonService = {
			"selectedService" : slectedServiceName,
			"selectedProductID" : selectedProductType,
			"selectedSubCategory" : selectedSubCategory
		};
		var dataJson = {
			selectedServiceJson : JSON.stringify(jsonService)
		};
		$.ajax({
			url : getDeviceURL,
			type : "POST",
			data : dataJson,
			success : function(response) {
				$('#deviceNameSelect option[value!="0"]').remove();
				var x = 1;
				var arr = response.split("#####");
				var len = arr.length;
				var reslen = len - x;
				for (var i = 0; i < reslen; i++) {

					$('#deviceNameSelect').append(
							$('<option>')
									.text(
											response.split("#####")[i]
													.split('@@@@@')[1]).val(
											response.split("#####")[i]
													.split('@@@@@')[0]));
				}
				$('#deviceNameSelect option[value=""]').remove();
			},
			error : function(xhr) {
				$('#serviceNameSelect option[value!="0"]').remove();
				/* alert('Error' + xhr); */
			}
		});
	}
</script>


<script type="text/javascript">
	$(document)
			.ready(
					function() {
								
								$("#productSelect").change(function(){
									getSelectedproductType();
								});
								
								$('#regionRow').hide();
								$('#siteNameRow').hide();
                                $('#MSI_MNS').hide();
                           		$('#MSI_PC').hide();
                           		$('#serviceNameRow').show();
								$('#categoryRow').show();
								$('#deviceNameRow').show();
								$('#typeRow').show();	
					
						var reqID;
						var submitRequestURL = "${submitRequestURL}";
						var errorClassForLabel = 'errorForLabel';
						jQuery.validator.addMethod('selectcheck', function(
								value) {
							if (value != null && value != '0') {
								return true;
							} else {
								return false;
							}
							
						}, "<liferay-ui:message key='Please select a valid option' />");

						$("#logSRForm")
								.validate(
										{
											rules : {
												summary : {
													required : true,
													maxlength : 200
												},
												description : {
													required : true,
													maxlength : 200
												},
												productType : {
													selectcheck : true
												},
												siteName : {
													required : true
												},
												serviceName : {
													selectcheck : true
												},
												region : {
													selectcheck : true
												},
												action : {
													selectcheck : true
												},
												category : "required",
												subCategory : {
													selectcheck : true
												}
											},
											messages : {
												summary : {
													required : "<liferay-ui:message key='Please enter request title of Service Request' />",
													maxlength : "<liferay-ui:message key='Maximum characters allowed is 200' />",
												},
												description : {
													required : "<liferay-ui:message key='Please enter description of Service Request' />",
													maxlength : "<liferay-ui:message key='Maximum characters allowed is 200' />",
												},
												category : "<liferay-ui:message key='Please select category' />",
											},
											highlight : function(element,
													errorClass, validClass) {

												return false; // ensure this function stops
											},
											unhighlight : function(element,
													errorClass, validClass) {
												return false; // ensure this function stops
											},
											submitHandler : function(form) {
												var dataForm = $('#logSRForm')
														.serialize();
												hideAll();
												$
														.ajax({
															url : submitRequestURL,
															type : "POST",
															data : dataForm,
															success : function(
																	response) {
																reqID = JSON
																		.parse(response);
																var status = reqID.requestID
																		.split("@")[0];

																if (status == 'Success') {
																	document
																			.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="SUCCESSFUL"/>';
																	document
																			.getElementById("requestID").innerHTML = '<liferay-ui:message key="Your service request - "/>'
																			+ reqID.requestID
																					.split("@")[1]
																			+ '&nbsp;'
																			+ '<liferay-ui:message key="has been submitted successfully!"/>';
																} else {
																	document
																			.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
																	document
																			.getElementById("requestID").innerHTML = '<liferay-ui:message key="Service Request submission failed. Please try again."/>';
																}
																$('#logSRSubmitResponse').modal('show');
																unhideAll();

																$("#logSRForm")[0]
																		.reset();
																$(
																		'#serviceNameSelect option[value!="0"]')
																		.remove();

															},
															error : function(
																	xhr) {
																document
																		.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
																document
																		.getElementById("requestID").innerHTML = '<liferay-ui:message key="Service Request submission failed. Please try again."/>';
																$(
																		'#logSRSubmitResponse')
																		.modal(
																				'show');
																unhideAll();
																$("#logSRForm")[0]
																		.reset();
																$(
																		'#serviceNameSelect option[value!="0"]')
																		.remove();
															}
														});
											}
										});
					});
</script>
<script>
        $(document).ready(function() {
         //   BindControls();
        });

        function BindControls() {
//		alert('in bind '+siteData);
	 $('#siteID').autocomplete({
                source: siteData,
                minLength: 0,
				appendTo: "#logSRModal",
                scroll: true
            }).focus(function() {
                $(this).autocomplete("search", "");
            });
        }
</script>
	
	
<div class="modal fade tt-SR-modal" id="logSRModal" role="dialog"
	data-backdrop="static" data-keyboard="false" aria-hidden="true"
	tabindex="-1">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->

		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="closeMarkID"
					data-dismiss="modal">
					<img class="closeMark"
						src="<%=request.getContextPath()%>/images/Close_G_24.png" />
				</button>
				<div class="modal-title logTitle">
					<liferay-ui:message key="LOG A NEW REQUEST" />
				</div>
			</div>

			<div class="modal-body tt-modal-body">
				<form:form id="logSRForm" name="logSRForm" class="form-horizontal tt-modal-form"
					modelAttribute="logSRFormModel" method="post">
					<div class="container-fluid tt-form-container">

						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">

								<liferay-ui:message key="Request Title:*" />

							</div>
							<div class="span9 tt-form-right-col">
								<form:input path="summary" id="summary" name="summary"
									type="text" class="textBoxLogSR" tabindex="1"/>
							</div>
						</div>
						
						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">

								<liferay-ui:message key="Product Type:*" />

							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="productType" name="productType" id="productSelect"
									items="${productTypeList}" class="selectBoxLogInc" tabindex="2">

								</form:select>
							</div>
						</div>
						

						<div class="row-fluid tt-form-row" id="regionRow">
							<div class="span3 tt-form-left-col" id="regionTitle">

								<liferay-ui:message key="Region:*" />

							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="region" name="region" id="regionSelect"
									items="${regionList}" class="selectBoxLogInc"
									onchange="getSite(this)" tabindex="2">

								</form:select>
							</div>
						</div>

						<div class="row-fluid tt-form-row" id="siteNameRow">
							<div class="span3 tt-form-left-col" id="siteNameTitle">

								<liferay-ui:message key="Site Name:*" />

							</div>
							<!--<div class="span9 tt-form-right-col">
								<form:select path="siteName" name="siteName" id="siteID"
									items="${siteNameList}" class="selectBoxLogInc"
									onchange="getService(this)" tabindex="3">

								</form:select>
							</div>-->
							
								<div class="span9 tt-form-right-col">
								 <div>
								<form:input path="siteName" name="siteName" tabindex="3" type="text" class="selectBoxLogInc" id="siteID" />
								
								</div>
							</div>
						</div>

						<div class="row-fluid tt-form-row" id="serviceNameRow">
							<div class="span3 tt-form-left-col">

								<liferay-ui:message key="Service Name:*" />

							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="serviceName" name="serviceName"
									id="serviceNameSelect" items="${serviceIDList}"
									class="selectBoxLogInc" tabindex="4">
								</form:select>
							</div>
						</div>

						<div class="row-fluid tt-form-row" id="MSI_MNS">

							<div class="span9 offset3 tt-form-right-col"
								id="dashTickCheckBox">

								<form:checkbox path="MSI" id="MultiServiceCheckBox" tabindex="5"/>
									<%-- <form:input path="mimCheck" id="MultiServiceCheckBox" type="checkbox"
									onchange="addCommentNotes(this)" /> --%>
								<div class="checkBoxTextLogInc">
									<span id="MSI"> 
										<liferay-ui:message	key="Multiple services are impacted" /></span> 
									<span class="tt-checkbox-optional"> 
										<liferay-ui:message	key="(Add details in description)" />
									</span>

								</div>
							</div>
						</div>

						<div class="row-fluid tt-form-row" id="categoryRow">
							<div class="span3 tt-form-left-col">
								<liferay-ui:message key="Category:*" />
							</div>
							<div class="span9 tt-form-right-col">
								<fieldset id="categoryMACD">
									<form:input path="category" id="MACDRadio" type="radio"	class="tt-radio-btn" name="category" value="MACD" tabindex="6"/>
									<liferay-ui:message key="MACD" />

									<form:input path="category" id="GenericRadio" type="radio" class="tt-radio-btn tt-radio-btn-sec" name="category" value="Generic" tabindex="7"/>
									<liferay-ui:message key="Generic" />
								</fieldset>
								<div>
									<label for="category" id="categoryLabelID" class="error"></label>
								</div>
							</div>

						</div>

						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">
								<liferay-ui:message key="Action:*" />
							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="action" name="action" id="actionSelect" items="${actionListMap}" class="selectBoxLogInc" tabindex="8">
								
								<%-- <form:select path="action" name="action" id="actionSelect"
									items="${actionListMap}" class="selectBoxLogInc"
									onchange="addActionComment()"> --%>
									
								</form:select>
							</div>
						</div>

						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">

								<liferay-ui:message key="Sub-Category:*" />

							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="subCategory" name="subCategory"
									id="subCategorySelect" items="${subCateogryListMap}"
									class="selectBoxLogInc" onchange="getDevices(this)" onblur="getDevices(this)" tabindex="9">

								</form:select>
							</div>
						</div>
                         
						<div class="row-fluid tt-form-row" id="deviceNameRow">
							<div class="span3 tt-form-left-col">

								<liferay-ui:message key="Device Name:" />

							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="deviceName" name="deviceName"
									id="deviceNameSelect" items="${deviceNameListMap}"
									class="selectBoxLogInc" tabindex="10">

								</form:select>
							</div>
						</div>
						<div class="row-fluid tt-form-row" id="typeRow">
							<div class="span3 tt-form-left-col">
								<liferay-ui:message key="Type:" />
							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="productClassification" name="productClassification"
									id="productClassificationSelect" items="${productClassificationListMap}"
									class="selectBoxLogInc" tabindex="10">
								</form:select>
							</div>
						</div>
						
						
						<div class="row-fluid tt-form-row" id="MSI_PC">

							<div class="span9 offset3 tt-form-right-col"
								id="dashTickCheckBox">

								<form:checkbox path="MSI" id="MultiServiceCheckBox" tabindex="5"/>
									<%-- <form:input path="mimCheck" id="MultiServiceCheckBox" type="checkbox"
									onchange="addCommentNotes(this)" /> --%>
								<div class="checkBoxTextLogInc">
									<span id="MSI"> <liferay-ui:message key="Multiple devices are impacted" />
									</span> <span class="tt-checkbox-optional"> <liferay-ui:message key="(Add details in description)" />
									</span>
								</div>
							</div>
						</div>
						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">

								<liferay-ui:message key="Description:*" />

							</div>
							<div class="span9 tt-form-right-col">
								<form:textarea path="description" id="customDescription"
									name="description" rows="4" cols="100"
									class="customDescription" tabindex="11"/>
							</div>
						</div>


						<div class="row-fluid tt-form-last-row pr-15 pt-30">
							<div class="span12 tt-submit-row">
								<span class="tt-cancel pr-10">
									<button type="button" class="btn-secondary-tt img-cancel"
										id="cancelButtonID" data-dismiss="modal" tabindex="13">
										<liferay-ui:message key="Cancel" />
									</button>
								</span> <span class="tt-submit"  style="margin-right: 2%;">
<%
	if (allowAccess) {
%>
									<button type="submit"
										class="btn-primary-tt img-submit-servicerequestenabled"
										id="submitRequestButtonID" tabindex="12">
										<liferay-ui:message key="Submit Request" />
									</button> 
<%
 	} else {
%>
									<button type="button"
										class="img-submit-servicerequest btn-primary-tt "
										id="submitRequestButtonID"
										
										title="Contact Administrator" tabindex="12">
										<liferay-ui:message key="Submit Request" />
									</button> 
<%
 	}
%>
								</span>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="logSRSubmitResponse" data-backdrop="static"
	data-keyboard="false">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">
			<img class="closeMark"
				src="<%=request.getContextPath()%>/images/Close_G_24.png" />
		</button>
		<div id="responseStatus"></div>
	</div>
	<div class="modal-body">
		<div id="requestID"></div>
	</div>
	<div class="modal-footer">
		<button type="button" class="img-close btn-secondary-tt"
			id="closeButtonID" data-dismiss="modal">
			<liferay-ui:message key="Close" />
		</button>
	</div>
</div>