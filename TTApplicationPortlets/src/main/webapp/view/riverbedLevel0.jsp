<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
       type="text/javascript"></script>
<script
       src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
       type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"
       type="text/javascript"></script>

<link href="${pageContext.request.contextPath}/css/main.css"
       rel="stylesheet"></link>
<link href="${pageContext.request.contextPath}/css/riverbedLevel0.css"
	rel="stylesheet"></link>       

<%@ include file="common.jsp"%>
<portlet:defineObjects />

<script>
var style = document.documentElement.appendChild(document.createElement("style")),
rule = " html {\
     to {\
        stroke-dashoffset: "+${riverbedOptimizationStatus[3]}+";}\
}";
if (CSSRule.KEYFRAMES_RULE) { // W3C
    style.sheet.insertRule("@keyframes" + rule, 0);
} else if (CSSRule.WEBKIT_KEYFRAMES_RULE) { // WebKit
    style.sheet.insertRule("@-webkit-keyframes" + rule, 0);
}

</script>

<style>

#riverbedPortletTitleBar{
       height:37px;
}

#riverbedTitleText{
       line-height:34px;
}
#riverbedNavigationArrow{
       line-height:34px;
}
.riverbed-title-dashboard {
    font-size: 15px;
    display: flex;
    justify-content: center;
}

#riverbedCountId{
       fill:#888 !important;
       font-size:50px !important;
}

#riverbedLabelId{
       fill:#888 !important;
       font-size:18px !important;
       font-weight: 100;
}

div#donut-graph-riverbed {
    display: flex;
   justify-content: center;
}

#markers-tt {
       display: flex;
       justify-content: center;
}

#tt-red {
       background-color: #e32212;
}

#tt-amber {
       background-color: #ff9c00;
}

#tt-green {
       background-color: #a3cf62;
}

#tt-red-marker {
       color: #e32212;
}

#tt-amber-marker {
       color: #ff9c00;
}

#tt-green-marker {
       color: #a3cf62;
}

.tt-color-legends {
       display: inline-block;
       width: 10px;
       height: 10px;
       position: relative;
       top: 5px;
       margin: 0 5px;
}

.tt-markers {
       display: inline-block;
       font-size: 12px;
}

.legend-riverbed {
       font-size: 15px;
       line-height: 120%;
       padding-top: 5px;
       padding-bottom: 30px;
       height: 30px;
       text-align: center;
}

#riverbedblock {
       justify-content: center;
       background-color: #fff;
       height: 262px;
       border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc;
}

.riverbedServices {
       width: 150px;
       margin: 0 40px 0 40px;
}

.value-riverbed {
       text-align: center;
       height: 50px;
       line-height: 50px;
       border: solid 1px black;
       border-radius: 8px;
}

#status-val-service {
       border-color: transparent;
}
</style>

<div
       style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
       <div id="riverbedPortletTitleBar" class="portletTitleBar">
              <div id="riverbedTitleText" class="titleText">
                     <liferay-ui:message key="OPTIMIZATION" />
              </div>
              <div id="riverbedNavigationArrow" class="navigationArrow">
                     <a class="navArrowRight" href="/group/telkomtelstra/riverbedlevel1">
                           <img class="navArrowRightImg"
                           src="/TTApplicationPortlets/images/Expand_G_24.png">
                     </a>
              </div>
       </div>
</div>
<div id="riverbedblock">
       <%-- <div class="riverbed-title-dashboard"><liferay-ui:message key="Overall Riverbed Health" /></div> --%>
       <div class="riverbed-title-dashboard"><liferay-ui:message key="Overall Bi-Directional Optimization" /></div>
       <div id="rb">
              <svg width="390" height="210" >
              		 <g class="arc" transform="translate(140,100)" id="yui_patched_v3_11_0_1_1491990343266_1220">
              		 <circle fill="#EFEFEF" r="89" id="yui_patched_v3_11_0_1_1491990343266_1219"></circle>
              		 </g>
                     <g class="item html" transform="translate(140,100) rotate(-90)">
                     <circle id="circle" class="circle_animation" r="85" cy="0" cx="0" stroke-width="9" stroke="#${riverbedOptimizationStatus[2]}" fill="#efefef"></circle>
                     </g>    
                     <g class="center_group" transform="translate(140,100)">
                     <circle fill="#eeeeee" r="80"></circle>
                     <circle fill="white" r="73"></circle>
                     <text id="riverbedCountId" class="total" dy="15" text-anchor="middle">${riverbedOptimizationStatus[1]}%</text>
                     </g>
                     <g class="label_group" transform="translate(140,100)"><line x1="5.204748896376251e-15" x2="100" y1="85" y2="85" stroke="#${riverbedOptimizationStatus[2]}"></line>
                     <text class="value" y="90" x="100" fill="#${riverbedOptimizationStatus[2]}" text-anchor="begning" id="yui_patched_v3_11_0_1_1489913337768_1106">${riverbedOptimizationStatus[0]} Appliance</text>
					 <text class="units" transform="translate(6.368163355566236e-15,104)" dy="17" text-anchor="end"></text>
					 </g>
                     </svg>
       </div>
       <div id="markers-tt">
              <div class="tt-color-legends" id="tt-red"></div>
              <div class="tt-markers" id="tt-red-marker"><liferay-ui:message key="Low Optimization" /></div>
              <div class="tt-color-legends" id="tt-amber"></div>
              <div class="tt-markers" id="tt-amber-marker"><liferay-ui:message key="Moderate Optimization" /></div>
              <div class="tt-color-legends" id="tt-green"></div>
              <div class="tt-markers" id="tt-green-marker"><liferay-ui:message key="High Optimization" /></div>
       </div>

</div>


<script>
       
</script>






