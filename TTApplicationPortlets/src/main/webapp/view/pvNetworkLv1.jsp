<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ include file="common.jsp" %>
    
    
<portlet:resourceURL id="getNetworkDrillDownDetail" var="getNetworkDrillDownDetail">
	<portlet:param name="action" value="getNetworkDrillDownDetail"/> 
</portlet:resourceURL>

<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet"></link>
<link href="${pageContext.request.contextPath}/css/pvNetwork1.css" rel="stylesheet"></link>


<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js" type="text/javascript"></script>
<script src="<%= request.getContextPath() %>/js/jquery.form-validator.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<style>
	.progress {
		height: 20px;
		margin-bottom: 20px;
		overflow: hidden;
		background-color: #f5f5f5;
		border-radius: 4px;
		-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
		box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
	}
	.progress {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#ebebeb),to(#f5f5f5));
		background-image: -webkit-linear-gradient(top,#ebebeb 0,#f5f5f5 100%);
		background-image: -moz-linear-gradient(top,#ebebeb 0,#f5f5f5 100%);
		background-image: linear-gradient(to bottom,#ebebeb 0,#f5f5f5 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffebebeb',endColorstr='#fff5f5f5',GradientType=0);
	}
	.progress {
		height: 12px;
		background-color: #ebeef1;
		background-image: none;
		box-shadow: none;
	}
	.progress-bar {
		float: left;
		width: 0;
		height: 100%;
		font-size: 12px;
		line-height: 20px;
		color: #fff;
		text-align: center;
		background-color: #428bca;
		-webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,0.15);
		box-shadow: inset 0 -1px 0 rgba(0,0,0,0.15);
		-webkit-transition: width .6s ease;
		transition: width .6s ease;
	}
	.progress-bar {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#428bca),to(#3071a9));
		background-image: -webkit-linear-gradient(top,#428bca 0,#3071a9 100%);
		background-image: -moz-linear-gradient(top,#428bca 0,#3071a9 100%);
		background-image: linear-gradient(to bottom,#428bca 0,#3071a9 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff428bca',endColorstr='#ff3071a9',GradientType=0);
	}
	.progress-bar {
		box-shadow: none;
		border-radius: 3px;
		background-color: #0090D9;
		background-image: none;
		-webkit-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
		-moz-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
		-ms-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
		-o-transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
		transition: all 1000ms cubic-bezier(0.785, 0.135, 0.150, 0.860);
		-webkit-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
		-moz-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
		-ms-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
		-o-transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
		transition-timing-function: cubic-bezier(0.785, 0.135, 0.150, 0.860);
	}
	.progress-bar-success {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#5cb85c),to(#449d44));
		background-image: -webkit-linear-gradient(top,#5cb85c 0,#449d44 100%);
		background-image: -moz-linear-gradient(top,#5cb85c 0,#449d44 100%);
		background-image: linear-gradient(to bottom,#5cb85c 0,#449d44 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5cb85c',endColorstr='#ff449d44',GradientType=0);
	}
	.progress-bar-success {
		background-color: #0AA699;
		background-image: none;
	}
	.progress-bar-info {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#5bc0de),to(#31b0d5));
		background-image: -webkit-linear-gradient(top,#5bc0de 0,#31b0d5 100%);
		background-image: -moz-linear-gradient(top,#5bc0de 0,#31b0d5 100%);
		background-image: linear-gradient(to bottom,#5bc0de 0,#31b0d5 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de',endColorstr='#ff31b0d5',GradientType=0);
	}
	.progress-bar-info {
		background-color: #0090D9;
		background-image: none;
	}
	.progress-bar-warning {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#f0ad4e),to(#ec971f));
		background-image: -webkit-linear-gradient(top,#f0ad4e 0,#ec971f 100%);
		background-image: -moz-linear-gradient(top,#f0ad4e 0,#ec971f 100%);
		background-image: linear-gradient(to bottom,#f0ad4e 0,#ec971f 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff0ad4e',endColorstr='#ffec971f',GradientType=0);
	}
	.progress-bar-warning {
		background-color: #FDD01C;
		background-image: none;
	}
	.progress-bar-danger {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-danger {
		background-color: #F35958;
		background-image: none;
	}
	/*********************/
	.progress-bar-1 {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-1 {
		background-color: #000;
		background-image: none;
	}
	.progress-bar-2 {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-2 {
		background-color: #A40800;
		background-image: none;
	}
	.progress-bar-3 {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-3 {
		background-color: #7030A0;
		background-image: none;
	}
	.progress-bar-4 {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-4 {
		background-color: #00B0F0;
		background-image: none;
	}
	.progress-bar-5 {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-5 {
		background-color: #FF0600;
		background-image: none;
	}
	.progress-bar-6 {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-6 {
		background-color: #F0990D;
		background-image: none;
	}
	.progress-bar-7 {
		background-image: -webkit-gradient(linear,left 0,left 100%,from(#d9534f),to(#c9302c));
		background-image: -webkit-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: -moz-linear-gradient(top,#d9534f 0,#c9302c 100%);
		background-image: linear-gradient(to bottom,#d9534f 0,#c9302c 100%);
		background-repeat: repeat-x;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffd9534f',endColorstr='#ffc9302c',GradientType=0);
	}
	.progress-bar-7 {
		background-color: #89C35C;
		background-image: none;
	}
	/*********************/

	.progress-bar-container {
		width: 200px;
		height: 60px;
		background-color: #D7D7D7;
		padding-right: 25px;
		padding-left: 25px;
		padding-top: 12px;
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
		border-top-right-radius: 5px;
		border-bottom-right-radius: 5px;
	}

	.progress-bar-container1 {
		width: 200px;
		height: 80px;
		background-color: #D7D7D7;
		padding-top: 20px;
		padding-right: 25px;
		padding-left: 25px;
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
		border-top-right-radius: 5px;
		border-bottom-right-radius: 5px;
		float: left;
		overflow: auto;
	}

	.progress-bar-title-button {
		float: left;
		height: 50px;
		width: 120px;
		background-color: #545966;
		color: white;
		border-top-left-radius: 5px;
		border-bottom-left-radius: 5px;
		margin-left: 30px;

	}

	.progress-bar-text-button {
		margin-top: 15px;
		margin-left: 15px;
		font-size: 13px;

	}

	.progress-bar-title {
		margin-top: 10px;
		margin-left: 10px;
		font-size: 13px;

	}

	.tenoss-legend{
		width: 15px;
		/**background-color: #e32212;**/
		height: 15px;
		display: inline-block;
		margin-right: 5px;
		vertical-align: middle;
</style>

<script>
	jQuery.base64=(function($){var _PADCHAR="=",_ALPHA="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",_VERSION="1.0";function _getbyte64(s,i){var idx=_ALPHA.indexOf(s.charAt(i));if(idx===-1){throw"Cannot decode base64"}return idx}function _decode(s){var pads=0,i,b10,imax=s.length,x=[];s=String(s);if(imax===0){return s}if(imax%4!==0){throw"Cannot decode base64"}if(s.charAt(imax-1)===_PADCHAR){pads=1;if(s.charAt(imax-2)===_PADCHAR){pads=2}imax-=4}for(i=0;i<imax;i+=4){b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6)|_getbyte64(s,i+3);x.push(String.fromCharCode(b10>>16,(b10>>8)&255,b10&255))}switch(pads){case 1:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6);x.push(String.fromCharCode(b10>>16,(b10>>8)&255));break;case 2:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12);x.push(String.fromCharCode(b10>>16));break}return x.join("")}function _getbyte(s,i){var x=s.charCodeAt(i);if(x>255){throw"INVALID_CHARACTER_ERR: DOM Exception 5"}return x}function _encode(s){if(arguments.length!==1){throw"SyntaxError: exactly one argument required"}s=String(s);var i,b10,x=[],imax=s.length-s.length%3;if(s.length===0){return s}for(i=0;i<imax;i+=3){b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8)|_getbyte(s,i+2);x.push(_ALPHA.charAt(b10>>18));x.push(_ALPHA.charAt((b10>>12)&63));x.push(_ALPHA.charAt((b10>>6)&63));x.push(_ALPHA.charAt(b10&63))}switch(s.length-imax){case 1:b10=_getbyte(s,i)<<16;x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_PADCHAR+_PADCHAR);break;case 2:b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8);x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_ALPHA.charAt((b10>>6)&63)+_PADCHAR);break}return x.join("")}return{decode:_decode,encode:_encode,VERSION:_VERSION}}(jQuery));
	
	$(document).ready(function(){
		var searchTerm=null;
		getDrillDowndata();

		function getDrillDowndata(){
			var getDrillDownDetails="${getNetworkDrillDownDetail}";
			
			$.ajax({type : "POST",
					url : getDrillDownDetails,
					dataType : 'json',

					success : function(data) {
						var count=0;	 
						$("#data-Network").empty();
					  
						var htmlString="";
						for (var i=0;i<data.length; i++){
							var tempData=data[i];
							//alert(tempData);

							for (var key in tempData) {
								count++;
								if (tempData.hasOwnProperty(key)) {
									if(count%4==0){
										//htmlString=htmlString+'<div class="span3"><div class="extra_style" style="background-color:'+tempData[key]+'"><table width="100%"><tbody><tr><td align="center"><a class="networkHeaders" href="/group/project-view/project_view?'+$.base64.encode(key.split("***")[1]+"&"+key.split("***")[0])+'">'+key.split("***")[0]+'</a></td></tr></tbody></table></div></div>';       
										//htmlString=htmlString+'</div>';   	
										htmlString=htmlString+'<div class="span3 progress-bar-container">';
										htmlString=htmlString+'    <span class="extra_network_style progress-bar-title"><a href="<%=prop.getProperty("tt.pv.homeURL")%>/<%=prop.getProperty("pvNetworkLv2")%>?ciname='+key.split("***")[0]+'">'+key.split("***")[0]+'</a></span>';
										htmlString=htmlString+'    <div class ="progress">';
										htmlString=htmlString+'        <div style="width: '+key.split("***")[2]+'%;" class="progress-bar progress-bar-'+key.split("***")[1]+'" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>';
										htmlString=htmlString+'    </div>';
										htmlString=htmlString+'</div>';
										htmlString=htmlString+'<div style="clear:both; height: 20px;"></div>';
										htmlString=htmlString+'</div>';
									}else if(count==1 || (count-1)%4==0){
										htmlString=htmlString+'<div class="row-fluid" >';
										//htmlString=htmlString+'<div class="span3"><div class="extra_style" style="background-color:'+tempData[key]+'"><table width="100%"><tbody><tr><td align="center"><a class="networkHeaders" href="/group/project-view/project_view?'+$.base64.encode(key.split("***")[1]+"&"+key.split("***")[0])+'">'+key.split("***")[0]+'</a></td></tr></tbody></table></div></div>';       
										htmlString=htmlString+'<div class="span3 progress-bar-container">';
										htmlString=htmlString+'    <span class="extra_network_style progress-bar-title"><a href="<%=prop.getProperty("tt.pv.homeURL")%>/<%=prop.getProperty("pvNetworkLv2")%>?ciname='+key.split("***")[0]+'">'+key.split("***")[0]+'</a></span>';
										htmlString=htmlString+'    <div class ="progress">';
										htmlString=htmlString+'        <div style="width: '+key.split("***")[2]+'%;" class="progress-bar progress-bar-'+key.split("***")[1]+'" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>';
										htmlString=htmlString+'    </div>';
										htmlString=htmlString+'</div>';
						  			}else{
										//htmlString=htmlString+'<div class="span3"><div class="extra_style" style="background-color:'+tempData[key]+'"><table width="100%"><tbody><tr><td align="center"><a class="networkHeaders" href="/group/project-view/project_view?'+$.base64.encode(key.split("***")[1]+"&"+key.split("***")[0])+'">'+key.split("***")[0]+'</a></td></tr></tbody></table></div></div>';  
										htmlString=htmlString+'<div class="span3 progress-bar-container">';
										htmlString=htmlString+'    <span class="extra_network_style progress-bar-title"><a href="<%=prop.getProperty("tt.pv.homeURL")%>/<%=prop.getProperty("pvNetworkLv2")%>?ciname='+key.split("***")[0]+'">'+key.split("***")[0]+'</a></span>';
										htmlString=htmlString+'    <div class ="progress">';
										htmlString=htmlString+'        <div style="width: '+key.split("***")[2]+'%;" class="progress-bar progress-bar-'+key.split("***")[1]+'" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>';
										htmlString=htmlString+'    </div>';
										htmlString=htmlString+'</div>';
									}
								}
							}
						}
						
						$("#data-Network").append(htmlString); 
					},
					error : function(data) {
					
					}
			});  
		}
	});
</script>

<title>Network Details</title>
</head>
<body>

<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name" style="margin-top: -13px;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
				<span class="tt-current-page" style="margin-top: -13px;"><liferay-ui:message key="Network Details"/></span>
			</div>
		</div>
		<div class="span6 tt-back">
			<button type="button" onclick="window.location='<%=prop.getProperty("tt.pv.homeURL")%>/<%=prop.getProperty("ttPVDashboardPageName")%>'" class="btn-primary-tt backButton" style="margin-top: -16px;">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
   </div>
</div>
<div id="tabContainer">
	<div id="networkTab">Network</div>
	<div style="clear:both; height: 20px;"></div>
</div>
<!--START DISPLAY SITES CONTAINER-->



<div class="container-fluid">
<!-- START OFFICE PORTLET GRAPH LEGEND -->
	<div class="row-fluid tt-graph-legend-container tt-centermodified">
		<div class="span12 tt-graph-legend">

			<c:forEach var="graphBottomDetails" items="${graphBottomList}">
			<c:set var="statusDetails" value="${fn:split(graphBottomDetails, '-')}" />
				<div class="span2" style="margin-left: 0px;">
					<div class="tenoss-legend" style="background-color:${statusDetails[1]}"></div>
					<span class="tt-legend-text-red"  style="color:${statusDetails[1]}"><liferay-ui:message key="${statusDetails[0]}"/></span>
				</div>
			</c:forEach>

		</div>
	</div>
	<div style="clear:both; height: 20px;"></div>
<!-- END OFFICE PORTLET GRAPH LEGEND -->
	<div class="row-fluid tt-site-tab-incident-content" id="site-incident">
		<div id="data-Network" style="background-color: white;" class="col-md-4">
			
		</div>
	</div>
	<div style="clear:both; height: 20px;"></div>
</div>

</body>
</html>



