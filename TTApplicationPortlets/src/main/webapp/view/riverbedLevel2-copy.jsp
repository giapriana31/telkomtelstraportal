<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ include file="common.jsp" %>

<html>
<head>
<style>
button,html input[type="button"]{
border-radius:5px !important;
}
@font-face 
{
  font-family: 'MyWebFont';
  src: url('fonts/GothamRoundedBook.woff') format('woff'),
       url('fonts/GothamRoundedBook.ttf') format('truetype');
}
body
{
 font-family: 'MyWebFont';
}

	/* CSS Triangles - see Trevor's post */
	.wrapper .tooltip:after {
	  border-left: solid transparent 10px;
	  border-right: solid transparent 10px;
	  border-top: solid #D4D4D4 10px;
	  bottom: -10px;
	  content: " ";
	  height: 0;
	  left: 50%;
	  margin-left: -13px;
	  position: absolute;
	  width: 0;
	}
	  
	.wrapper:hover .tooltip {
	  font-size: 8px;
	    color: black;
	  opacity: 1;
	  pointer-events: auto;
	  -webkit-transform: translateY(0px);
	     -moz-transform: translateY(0px);
	      -ms-transform: translateY(0px);
	       -o-transform: translateY(0px);
	          transform: translateY(0px);
	}

	/* IE can just show/hide with no transition */
	.lte8 .wrapper .tooltip {
	  display: none;
	}
	
	.lte8 .wrapper:hover .tooltip {
	  display: block;
	}
}
@media  (min-width: 500px) {
	.wrapper{
			display:none !important;
	}
}
</style>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/sites.css" ></link>


<portlet:defineObjects />

<script type="text/javascript">
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

function APPHealth() {
	$('#APPHealth-li').addClass('selected');
	$('#data-RBApp').removeClass('hide');
	$('#APPCheckbox').removeClass('hide');
	$('#riverbedAppCountId').removeClass('hide');
	$('#riverbedApplianceTitleText').removeClass('hide');
}

var VMs = [{application:'Solarwinds',appliance:'EC5GK000F3BAA',optimization:'69',colour:'background-color:#ff9c00',priority:'2'},
           {application:'SalesForce.com',appliance:'EC5GK000F3BAA',optimization:'75',colour:'background-color:rgb(163, 207, 98)',priority:'3'},
           {application:'Office365',appliance:'EC5GK000F3BAA',optimization:'73',colour:'background-color:rgb(163, 207, 98)',priority:'3'},
           {application:'ServiceNOW',appliance:'EC5GK000F3BAA',optimization:'74',colour:'background-color:rgb(163, 207, 98)',priority:'3'}];           
			
VMs = JSON.stringify('${ApplicationArrayJSON}');
VMs = eval('(' + '${ApplicationArrayJSON}' + ')');

var temp = VMs.slice();
//var appliances = '';

$(document).ready(function(){
	
	var hash = document.location.hash;
	if(hash){
		if(hash === '#APPHealth'){
			APPHealth();
		}
	}
	else{
		APPHealth();
	}
	
	/* var parameterReceived = getQueryString(window.location.href);
    if(parameterReceived!=-1){
          var applianceSpecifics = Base64.decode(parameterReceived);
          appliances=applianceSpecifics.split("&")[0];
    } */
    //document.getElementById("riverbedApplianceTitleText").innerHTML = appliances;
    
	VMs = VMs.sort(function IHaveAName(a, b) { return b.application> a.application?  1 : b.application< a.application? -1 : 0; });
	VMs = VMs.add();

	populateData(VMs);	
	
	$("#linkTable").addClass('hide');
				
})


Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
		  return v[name] === value ? null : v;
	   });
	   this.length = 0;
	   this.push.apply(this, array);
	}
	
$(function() {
	
	$("input[type='checkbox']").change(
	function()	{
		
			temp = VMs.slice();			
			var all = false;
			var high = false;
			var moderate = false;
			var normal = false;
			
			if($('#checkbox_all').is(':checked')) 
			{		
				all = true;
			}
			if($('#checkbox_high').is(':checked')) 
			{		
				high = true;
			}
			if($('#checkbox_moderate').is(':checked')) 
			{		
				moderate = true;
			}
			if($('#checkbox_normal').is(':checked')) 
			{		
				normal = true;
			}

			if(all == false)
			{
				if(high==false)
					temp.removeValue('priority', '1');
				if(moderate==false)
					temp.removeValue('priority', '2');
				if(normal==false)
					temp.removeValue('priority', '3');
			}
			
			$('#searchText').val('');
			
			populateData(temp);			
	});	
	});	
	
var IHaveAName = function IHaveAName(a, b) { 
	return b.application < a.application ?  1 : b.application > a.application ? -1 : 0;
}

var sortByName = function sortByName(a, b) { 
	return b.application < a.application ?  1 : b.application > a.application ? -1 : 0;
}	

function populateData(temp)
{
	temp = temp.sort(IHaveAName);		
	temp = temp.sort(sortByName);
	
	if(temp.length==0)
	{
		document.getElementById("data-RBApp").innerHTML = "<div class='row-fluid'></div>";
		return;
	}
	else
	{
		var noApp = 0;
		var cekApp = 0;
		var strApp = "";
		strApp = "<div class='row-fluid'>";
		for (i in temp)
		{
			if(temp[i].appliance!=null){
				strApp += "<div class='span3'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td align='left'><div class='span9'><a style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].application+"</a></div><div>"+temp[i].optimization+"%</div></td></tr><tr><td align='center'><div class='progress tt-progess'><div id ='appBar' class='bar' style='width: "+temp[i].optimization+"%;background-image: -webkit-linear-gradient(top,#67B5EB 0,#67B5EB 100%);color:black'></div></div></td></tr></table></div></div>";
				noApp++;
				cekApp++;
				if(noApp==4)
				{
					noApp=0;
					strApp += "</div><div class='row-fluid'>";			 
				}
			}	
		}
		strApp += "</div>";
		
		if(cekApp==0){
			document.getElementById("data-RBApp").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-RBApp").innerHTML = strApp;
			document.getElementById("riverbedAppCountId").innerHTML = cekApp;
			//document.getElementById("riverbedApplianceTitleText").innerHTML = appliances;
		}
	}
}

function getQueryString(url){
	if (url.indexOf('?') > -1) {
		var pos = url.indexOf('?');
		var extract = url.substring(pos + 1,url.length);
		return extract;
	}
	else{
		return -1;
	}
}


</script>

</head>
<body>
<div class="container-fluid tt-page-detail">
	<div class="tt-riverbedlevel1" >
   		<div class="tt-riverbedlevel1-header"> 
		   <img src="/TTApplicationPortlets/images/u91.png"> 
		   <font style="font-size:100%;vertical-align:middle;margin-left:1%;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</font>
		   <font style="font-size:125%;vertical-align:middle;"><liferay-ui:message key="Riverbed"/></font> 
			<div class="span8 tt-backbutton" style="float: right;">
				<input type="button" value="&nbsp;&lt;&nbsp;<liferay-ui:message key='Back'/>" onclick="window.location='/group/telkomtelstra/riverbedlevel1'" class="tt-riverbed-inputbutton" >
			</div>
		</div>
        
    </div>
	<div class="row-fluid tt-page-content">
		<ul id="tabc" class="nav nav-pills tt-site-ul">
			<li class="active tt-site-nav-pills selected" id="APPHealth-li">
				<a data-toggle="tab" href="#APPHealth" class="tt-site-nav-pills" id="basedOnTicket" onclick="APPHealth()"><liferay-ui:message key="Base on Port / Application Optimization"/></a>
			</li>
		</ul>
	</div>
</div>

<div class="container-fluid tt-site-display" id="APPCheckbox">
	<div class="row-fluid">
		<div class="span12 tt-sitecheck">
			<div class="span2 tt-displaySite-title"><liferay-ui:message key="Display"/></div>
			<div class="span6">
				<div class="row-fluid tt-displaySite-check">
					<div class="span2" style="display: flex;"><input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/><liferay-ui:message key="All"/></div>
					<div class="span3" style="display: flex;"><input type="checkbox" id="checkbox_high" class="tt-displaysite-position" /><liferay-ui:message key="High Impact"/></div>
					<div class="span4" style="display: flex;"><input type="checkbox" id="checkbox_moderate" class="tt-displaysite-position" /><liferay-ui:message key="Moderate Impact"/></div>
					<div class="span3" style="display: flex;"><input type="checkbox" id="checkbox_normal" class="tt-displaysite-position" /><liferay-ui:message key="Normal"/></div>					
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid tt-site-tab-container">

<div class="span12 tt-graph tt-graph-first tt-gold">
	<div style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
       <div id="riverbedAppPortletTitleBar" class="portletTitleBar">
              <div id="riverbedApplianceTitleText" class="span10 titleText">
              			<liferay-ui:message key="No Appliance applied" />        
              </div>
              <div>
                     <text id="riverbedAppCountId" class="total" dy="5" text-anchor="middle"></text></g></svg> 
              </div>
              
       </div>
	</div>
	<div
       style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
		<div id="data-RBApp" style="padding:10px;background-color: white;">
		</div>
	</div>
</div>

</div>

</body>
</html>