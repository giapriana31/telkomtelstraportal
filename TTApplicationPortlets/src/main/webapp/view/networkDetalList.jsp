<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/networkgraph.css">

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="common.jsp" %>

<script type="text/javascript">
	var ttNetworkDetailMap = ${ttNetworkDetailMap};
	for(var property in ttNetworkDetailMap) {
			
			
		}
</script>

<style>

div#officePortel {
    background-color: #fff;
    height: 307px;
}

</style>

<script type="text/javascript">

$(document).ready(function(){
var redSitesCount = ${redSitesCount}; 
var amberSitesCount = ${amberSitesCount}; 
var greennSitesCount = ${greenSitesCount};
var total= redSitesCount + amberSitesCount + greennSitesCount;
    document.getElementById("red").style.width = redSitesCount/total*100 + "%";
    document.getElementById("ambar").style.width = amberSitesCount/total*100 + "%";
    document.getElementById("green").style.width = greennSitesCount/total*100 + "%";
	});
</script>

<div class="container-fluid tt-container" id="officePortel">
	<!-- START NETWORK PORTLET HEADER-->
	<div class="row-fluid tt-header">
		<div class="span6 tt-page-title"><liferay-ui:message key="NETWORK"/></div>
		<div class="span6 tt-detail">
			<a href="<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardnetwork")%>">
				<img class="navArrowRightImg" style="float:right;" src="<%=request.getContextPath() %>/images/Expand_G_24.png"/>
		   </a>
			
		</div>
	</div>
	<!-- END NETWORK PORTLET HEADER-->
	<!-- START NETWORK PORTLET MAP CONTENT-->
	<div class="row-fluid tt-content-map">
		<div class="span12 pt-5 pl-5 pr-5">
				<!--START PROGRESS BAR  -->
				<div class="row-fluid">
					<div class="span12" >
						<div class="progress tt-progess">
							<div id ="green" class="bar tt-progress-bar-success" ></div>
							<div id ="ambar"  class="bar tt-progress-bar-warning" ></div>
							<div id ="red" class="bar tt-progress-bar-danger" ></div>
						</div>
					</div>
				</div>
				<!--END PROGRESS BAR  -->
				
		</div>
	</div>
	<!--START INCIDENT AND COUNT  -->
	<div class="row-fluid tt-content-map tt-ntw-impact-incident  ">
		<div class="span9 tt-impact-incident-text pl-15"><liferay-ui:message key="Impact Incidents"/></div>
		<div class="span3 tt-impact-incident-count pr-15">${customerImpactedIncidentsCount}</div>
	</div>
	
	<div class="row-fluid tt-content-map tt-ntw-risk-incident">
		<div class="span6 tt-risk-incident-text pl-15"><liferay-ui:message key="At Risk Incidents"/></div>
		<div class="span6 tt-risk-incident-count pr-15">${atRiskIncidentsCount} </div>
	</div>
	<!--END INCIDENT AND COUNT  -->
	<!--START MAP -->
	<div class="row-fluid tt-content-map tt-ntw-map ">
		<div class="span12" style="margin-bottom: 8px;">
			<div>
				<span class="tt-domestic-map"></span>
				<span class="tt-domestic-green-marker"> ${greenSitesCount}</span>
				<span class="tt-domestic-amber-marker">${amberSitesCount}</span>
				<span class="tt-domestic-red-marker">${redSitesCount}</span>
			</div>
		</div>
	</div>
	<!--END MAP -->
	<!-- END NETWORK PORTLET MAP CONTENT-->
</div>