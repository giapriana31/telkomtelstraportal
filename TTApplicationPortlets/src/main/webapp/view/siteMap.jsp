<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link
	href="${pageContext.request.contextPath}/webapp/css/bootstrap.min.css"
	rel="stylesheet"></link>
<script
	src="${pageContext.request.contextPath}/webapp/css/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/webapp/css/bootstrap.min.js"></script>
<div class="container">
	<h2><liferay-ui:message key="Office Buildings"/></h2>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#home"><liferay-ui:message key="Home"/></a></li>
		<li><a data-toggle="tab" href="#menu1"><liferay-ui:message key="Menu 1"/></a></li>
	</ul>

	<div class="tab-content">
		<div id="home" class="tab-pane fade in active">
			<table id="mainTable" border="0"
				style="width: 100%; border-collapse: separate; border-spacing: 5px; margin-top: 10px;">
				<tr>
					<td colspan="2" style="vertical-align: top;">
					
						<table id="GTable" border="1"
							style="width: 100%; border-collapse: collapse;">
							
							  <c:if test="${not empty incidentList}">
							
							<c:forEach items="${incidentList.incidents}" var="incident"  >
						    
						     
                             <c:if test="${incident.siteServiceTier eq 'Gold Sites' and incident.colorcode eq 'red'}">
							 
							 <tr>
								<c:out value="${incident.siteServiceTier}"/>
								
							 </tr>
							<tr>
								<td bgcolor="#FF0000">11 <c:out value="${incident.siteName}"/></td>
								<td bgcolor="#FF0000">11 <c:out value="${incident.siteName}"/></td>
						   </tr>
							</c:if>
							</c:forEach>
							
							<c:forEach items="${incidentList.incidents}" var="incident"  >
						   
                             <c:if test="${incident.siteServiceTier eq 'Gold Sites' and incident.colorcode eq 'yellow'}">
							<tr>
								<td bgcolor="#FFFF00">11 <c:out value="${incident.siteName}"/></td>
								<td bgcolor="#FFFF00">11 <c:out value="${incident.siteName}"/></td>
						   </tr>
							</c:if>
							</c:forEach>
							
							<c:forEach items="${incidentList.incidents}" var="incident"  >
						   
                             <c:if test="${incident.siteServiceTier eq 'Gold Sites' and incident.colorcode eq 'green'}">
							<tr>
								<td bgcolor="#00FF00">11 <c:out value="${incident.siteName}"/></td>
								<td bgcolor="#00FF00">11 <c:out value="${incident.siteName}"/></td>
						   </tr>
							</c:if>
							</c:forEach>
							</c:if>
						</table>
					</td>
					<td></td>
					<!-- <td colspan="2" style="vertical-align: top;">
						<table id="STable" border="1"
							style="width: 100%; border-collapse: collapse;">
							<tr>
								<td colspan="2">Silver Sities(2)</td>
							</tr>
							<tr>
								<td>11</td>
								<td>ww</td>
							</tr>
						</table>
					</td>
					<td colspan="2" style="vertical-align: top;">
						<table id="BTable" border="1"
							style="width: 100%; border-collapse: collapse;">
							<tr>
								<td colspan="2">Bronze Sities(5)</td>
							</tr>
							<tr>
								<td bgcolor="#FF0000">11</td>
								<td bgcolor="#FF0000">ww</td>
							</tr>
						</table>
					</td>
				</tr> -->
			</table>
		</div>
		<!-- <div id="menu1" class="tab-pane fade">
			<h3>Menu 1</h3>
			<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco
				laboris nisi ut aliquip ex ea commodo consequat.</p>
		</div> -->
	</div>
</div>
