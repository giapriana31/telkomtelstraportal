<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>




<div class="container-fluid">
   <div class="row">
      <div class="span2" id="firstRowProfile"><liferay-ui:message key="Notify for"/>:</div>
      <div class="span3" id="firstRowProfile">
 <select name="lang">
            <option value="Priority1" selected="selected"><liferay-ui:message key="Priority1"/></option>
            <option value="Priority2"><liferay-ui:message key="Priority2"/></option>
            <option value="Priority3"><liferay-ui:message key="Priority3"/></option>
            <option value="Priority4"><liferay-ui:message key="Priority4"/></option>
         </select>      </div>
   </div>
   <div class="row">
      <div class="span2" id="firstRowProfile"><liferay-ui:message key="Inform via"/>:</div>
      <div class="span3" id="firstRowProfile">
         <% if(user.getExpandoBridge().getAttribute( "NotificationPreference").equals( "Email")) { %>
         <label>
         <input type="radio" value="1" name="radio" /><liferay-ui:message key="SMS"/>
         <br>
         <input type="radio" value="2" name="radio" checked/><liferay-ui:message key="Email"/>
         <br>
         </label>
         <% } else { %>
         <label>
         <input type="radio" value="1" name="radio" checked/><liferay-ui:message key="SMS"/>
         <br>
         <input type="radio" value="2" name="radio" /><liferay-ui:message key="Email"/>
         <br>
         </label>
         <% } %>
      </div>
   </div>
   <div class="row">
      <div class="span2" id="firstRowProfile"><liferay-ui:message key="Profile Id"/>:</div>
      <div class="span4" id="firstRowProfile">
       <select name="lang1">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3" selected="selected">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
         </select>
      </div>
       <div class="span2" id="thirdColHrs" ><liferay-ui:message key="Hours"/></div>
   </div>
  
 
  </div>


