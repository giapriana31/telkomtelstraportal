<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script src="<%=request.getContextPath() %>/js/d3.js"></script>
<!-- <script src="<%=request.getContextPath() %>/js/jquery.min.js"></script> -->
<script src="<%=request.getContextPath() %>/js/main.js"></script>
<script src="<%=request.getContextPath() %>/js/bvdashboard.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.dataTables.min.js"></script>  


<link href="<%=request.getContextPath() %>/css/main.css" rel="stylesheet"></link>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.dataTables.css"/>
<link href="${pageContext.request.contextPath}/css/bvdashboard.css" rel="stylesheet"/>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="common.jsp" %>
<style>
button {
	border-radius: 5px;
}
.tt-red-legend{
	  width: 15px;
	  height: 15px;
	  display: inline-block;
	  margin-right: 5px;
	  vertical-align: middle;
}

</style>
<script type="text/javascript">
/*$.getScript("${pageContext.request.contextPath}/js/d3.js").done(
function(script, textStatus) {
	$.getScript("${pageContext.request.contextPath}/js/bvdashboard.js")
			.done(function(script, textStatus) {
			}).fail(function(jqxhr, settings, exception) {
			});
}).fail(function(jqxhr, settings, exception) {
});
*/
var ttOfficeBuildingGoldColorMap = ${subscribedServicesMap};
var ttOfficeBuildingSilverColorMap = ${sitesManagementMap};
var colors=${colorList};
var colorsSorted=[];
for (var pair in ttOfficeBuildingSilverColorMap)
      colorsSorted.push([pair, ttOfficeBuildingSilverColorMap[pair]])
colorsSorted.sort(function(a, b) {return a[1] - b[1]})
colors=[];
for(var i=0;i<colorsSorted.length;i++){
colors.push(colorsSorted[i][0]);
}

	
</script>


<script type="text/javascript">
$(document).ready(function()
{

subscribedServicesTable();
	
var URLbv = window.location.href;
subscribedServicesTable();	

if(URLbv.includes("bvdetails"))
{
	siteManagementTableAjax2();
}
else
{
	siteManagementTableAjax();
}

var w = 280;
var h = 200;
var r = 90;
var ir = 80;
var textOffset = 14;
var tweenDuration = 500;

//OBJECTS TO BE POPULATED WITH DATA LATER
var lines = Array();
var valueLabels = Array();
var nameLabels = Array();
var pieData = [];
var oldPieData = [];
var filteredPieData = [];

//D3 helper function to populate pie slice parameters from array data
var donut = d3.layout.pie().value(function(d) {
  return d.octetTotalCount;
});

//D3 helper function to create colors from an ordinal scale

//var color = d3.scale.category20();
//var colors = ['#E32212', '#FF9C00', '#A3CF62', '#000000', '#8005D8', '#30BFFC'];
//var colors = graphColorList;
var siteColors = [colors.slice(0),colors.slice(0),colors.slice(0)];
//alert(colors.slice(0));
var color = function(index,siteIndex) {
    if (index < siteColors[siteIndex].length)
      return siteColors[siteIndex][index];
    else
      return '#888888';
  }
  //D3 helper function to draw arcs, populates parameter "d" in path object
var arc = d3.svg.arc()
  .startAngle(function(d) {
    return d.startAngle;
  })
  .endAngle(function(d) {
    return d.endAngle;
  })
  .innerRadius(ir)
  .outerRadius(r);

///////////////////////////////////////////////////////////
//GENERATE FAKE DATA /////////////////////////////////////
///////////////////////////////////////////////////////////

var streakerDataAdded;

function findCoordinates(r, angle) {
//  alert(angle);
  angle = angle - Math.PI / 2;
  return {
    x: Math.cos(angle) * r,
    y: Math.sin(angle) * r
  };
}

function fillArray() {
  return {
    port: "",
    octetTotalCount: 10
  };
}

///////////////////////////////////////////////////////////
//CREATE VIS & GROUPS ////////////////////////////////////
///////////////////////////////////////////////////////////
var sites = ['subscribedservices', 'sitemanagement'];
var vis = Array();
var arc_group = Array();
var center_group = Array();
var paths = Array();
var label_group = Array();
var grayCircle = Array();
var whiteCircle = Array();
var totalLabel = Array();
var totalValue = Array();

//var ttOfficeSilverColorCode = [0, 0, 0, 0, 0, 0];
var ttOfficeSilverColorCode = [];
//var ttOfficeGoldColorCode = [0, 0, 0, 0, 0, 0];
var ttOfficeGoldColorCode = [];
//var ttOfficeBronzeColorCode = [0, 0, 0, 0, 0, 0];
var ttOfficeBronzeColorCode = [];
var count = 0;

//Temp Values
/*var ttOfficeBuildingSilverColorMap={Green:5};
var ttOfficeBuildingGoldColorMap={Red:0,Green:0,Amber:3};
var ttOfficeBuildingBronzeColorMap={Green:2};*/

/*for (var colorKey in ttOfficeBuildingSilverColorMap) {
       for(var graphColor in colors ){
              if(colorKey.toUpperCase()==colors[graphColor].toUpperCase()){
                     ttOfficeSilverColorCode.push(ttOfficeBuildingSilverColorMap[graphColor]);
                     
              }
       }*/
              for(var graphColor in colors ){
       
                     var clr = colors[graphColor];
                     //alert("clr ::: " + clr);
                     ttOfficeSilverColorCode.push(ttOfficeBuildingSilverColorMap[clr]);
                     ttOfficeGoldColorCode.push(ttOfficeBuildingGoldColorMap[clr]);                    
                     //ttOfficeBronzeColorCode.push( ttOfficeBuildingBronzeColorMap[clr]);
       }      
       //alert(ttOfficeSilverColorCode);
       
/* if (colorKey == 'Red') {
    ttOfficeSilverColorCode[0] = ttOfficeBuildingSilverColorMap[colorKey];
  } else if (colorKey == 'Amber') {
    ttOfficeSilverColorCode[1] = ttOfficeBuildingSilverColorMap[colorKey];
  } else if (colorKey == 'Green') {
    ttOfficeSilverColorCode[2] = ttOfficeBuildingSilverColorMap[colorKey];
  } else if (colorKey == 'Black') {
    ttOfficeSilverColorCode[3] = ttOfficeBuildingSilverColorMap[colorKey];
  } else if (colorKey == 'Brown') {
   ttOfficeSilverColorCode[4] = ttOfficeBuildingSilverColorMap[colorKey];
  } else if (colorKey == 'Yellow') {
    ttOfficeSilverColorCode[5] = ttOfficeBuildingSilverColorMap[colorKey];
  }*/
       
       //ttOfficeSilverColorCode.push(ttOfficeBuildingSilverColorMap[colorKey]);
//}
count = 0;
/*for (var colorKey in ttOfficeBuildingGoldColorMap) {
for(var graphColor in colors ){

              if(colorKey.toUpperCase()==colors[graphColor].toUpperCase()){
                     ttOfficeGoldColorCode.push(ttOfficeBuildingGoldColorMap[colorKey]);               
              }
       }*/
/* if (colorKey == 'Red') {
    ttOfficeGoldColorCode[0] = ttOfficeBuildingGoldColorMap[colorKey];
  } else if (colorKey == 'Amber') {
    ttOfficeGoldColorCode[1] = ttOfficeBuildingGoldColorMap[colorKey];
  } else if (colorKey == 'Green') {
    ttOfficeGoldColorCode[2] = ttOfficeBuildingGoldColorMap[colorKey];
  }else if (colorKey == 'Black') {
    ttOfficeGoldColorCode[3] = ttOfficeBuildingGoldColorMap[colorKey];
  } else if (colorKey == 'Brown') {
    ttOfficeGoldColorCode[4] = ttOfficeBuildingGoldColorMap[colorKey];
  } else if (colorKey == 'Yellow') {
    ttOfficeGoldColorCode[5] = ttOfficeBuildingGoldColorMap[colorKey];
  }*/
       //ttOfficeGoldColorCode.push(ttOfficeBuildingGoldColorMap[colorKey]);
//}
count = 0;
/*for (var colorKey in ttOfficeBuildingBronzeColorMap) {
       
for(var graphColor in colors ){

              if(colorKey.toUpperCase()==colors[graphColor].toUpperCase()){
                     
                     ttOfficeBronzeColorCode.push( ttOfficeBuildingBronzeColorMap[colorKey]);
              }
       }*/
  /*if (colorKey == 'Red') {
    ttOfficeBronzeColorCode[0] = ttOfficeBuildingBronzeColorMap[colorKey];
} else if (colorKey == 'Amber') {
    ttOfficeBronzeColorCode[1] = ttOfficeBuildingBronzeColorMap[colorKey];
  } else if (colorKey == 'Green') {
    ttOfficeBronzeColorCode[2] = ttOfficeBuildingBronzeColorMap[colorKey];
  }else if (colorKey == 'Black') {
    ttOfficeBronzeColorCode[3] = ttOfficeBuildingBronzeColorMap[colorKey];
  } else if (colorKey == 'Brown') {
    ttOfficeBronzeColorCode[4] = ttOfficeBuildingBronzeColorMap[colorKey];
  } else if (colorKey == 'Yellow') {
    ttOfficeBronzeColorCode[5] = ttOfficeBuildingBronzeColorMap[colorKey];
  }*/
       //ttOfficeBronzeColorCode.push( ttOfficeBuildingBronzeColorMap[colorKey]);
//}


var goldJsonObject=[];
var silverJsonObject=[];
var bronzeJsonObject=[];




for (i = 0; i < ttOfficeGoldColorCode.length; i++) { 
       goldJsonObject.push({octetTotalCount: ttOfficeGoldColorCode[i]});
}


for (i = 0; i < ttOfficeSilverColorCode.length; i++) { 
       silverJsonObject.push({octetTotalCount: ttOfficeSilverColorCode[i]});
}

for (i = 0; i < ttOfficeBronzeColorCode.length; i++) { 
       bronzeJsonObject.push({octetTotalCount: ttOfficeBronzeColorCode[i]});
}



var totalData=[];

totalData.push(goldJsonObject);
totalData.push(silverJsonObject);
totalData.push(bronzeJsonObject);





/*var totalData = [
      [{
        octetTotalCount: ttOfficeGoldColorCode[0]
      },{
        octetTotalCount: ttOfficeGoldColorCode[1]
      }, {
        octetTotalCount: ttOfficeGoldColorCode[2]
      }, {
        octetTotalCount: ttOfficeGoldColorCode[3]
      }, {
        octetTotalCount: ttOfficeGoldColorCode[4]
      }, {
        octetTotalCount: ttOfficeGoldColorCode[5]
      }],
      [{
        octetTotalCount: ttOfficeSilverColorCode[0]
      }, {
        octetTotalCount: ttOfficeSilverColorCode[1]
      }, {
        octetTotalCount: ttOfficeSilverColorCode[2]
      }, {
        octetTotalCount: ttOfficeSilverColorCode[3]
      }, {
        octetTotalCount: ttOfficeSilverColorCode[4]
      }, {
        octetTotalCount: ttOfficeSilverColorCode[5]
      }],
      [{
        octetTotalCount: ttOfficeBronzeColorCode[0]
      }, {
        octetTotalCount: ttOfficeBronzeColorCode[1]
      }, {
        octetTotalCount: ttOfficeBronzeColorCode[2]
      }, {
        octetTotalCount: ttOfficeBronzeColorCode[3]
      }, {
        octetTotalCount: ttOfficeBronzeColorCode[4]
      }, {
        octetTotalCount: ttOfficeBronzeColorCode[5]
      }]
    ];*/
    for(var p = 0;p<totalData.length;p++){
      var tempArray =  totalData[p];
      for(var q=tempArray.length-1;q>=0;q--){
       if(tempArray[q].octetTotalCount<1){
          tempArray.splice(q,1);
          siteColors[p].splice(q,1);
        }
      }
    }

for (var i = 0; i < sites.length; i++) {
  vis[i] = d3.select("#" + sites[i]).append("svg:svg")
    .attr("width", w)
    .attr("height", h);

  //   GROUP FOR ARCS/PATHS
  arc_group[i] = vis[i].append("svg:g")
    .attr("class", "arc")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

  //   GROUP FOR CENTER TEXT  
  center_group[i] = vis[i].append("svg:g")
    .attr("class", "center_group")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

  //   PLACEHOLDER GRAY CIRCLE
  paths[i] = arc_group[i].append("svg:circle")
    .attr("fill", "#EFEFEF")
    .attr("r", r);

  //   GROUP FOR LABELS
  label_group[i] = vis[i].append("svg:g")
    .attr("class", "label_group")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

  ///////////////////////////////////////////////////////////
  //   CENTER TEXT ////////////////////////////////////////////
  ///////////////////////////////////////////////////////////

  //   WHITE CIRCLE BEHIND LABELS
  grayCircle[i] = center_group[i].append("svg:circle")
    .attr("fill", "#eeeeee")
    .attr("r", ir);

  whiteCircle[i] = center_group[i].append("svg:circle")
    .attr("fill", "white")
    .attr("r", ir - 7);

  //   "TOTAL" LABEL
  totalLabel[i] = center_group[i].append("svg:text")
    .attr("class", "label")
    .attr("dy", 15)
    .attr("text-anchor", "middle") // text-align: right
    .text("Total");

  //   TOTAL TRAFFIC VALUE
  totalValue[i] = center_group[i].append("svg:text")
    .attr("class", "total")
    .attr("dy", -5)
    .attr("text-anchor", "middle") // text-align: right
    .text("0");

}
///////////////////////////////////////////////////////////
//STREAKER CONNECTION ////////////////////////////////////
///////////////////////////////////////////////////////////

for (var j = 0; j < sites.length; j++) {
  draw(j);
  draw(j);
  
}

//to run each time data is generated
function draw(siteIndex) {
    streakerDataAdded = totalData[siteIndex];
    oldPieData = filteredPieData;
    pieData = donut(streakerDataAdded);
    
    var totalOctets = 0;

    function filterData(element, index, array) {
      element.name = streakerDataAdded[index].port;
      element.value = streakerDataAdded[index].octetTotalCount;
      totalOctets += element.value;
      return (element.value >= 0);
    }
    filteredPieData = pieData.filter(filterData);
    if (filteredPieData.length > 0 && oldPieData.length > 0) {

      //REMOVE PLACEHOLDER CIRCLE
      arc_group[siteIndex].selectAll("circle").remove();

      totalValue[siteIndex].text(function() {

        return totalOctets;
        //return bchart.label.abbreviated(totalOctets*8);
      });

      //DRAW TICK MARK LINES FOR LABELS
      lines[siteIndex] = label_group[siteIndex].selectAll("line").data(filteredPieData);
       var genCoordsX=[];
       var genCoordsY=[];
var firstLineX=0;
var latestLineX=0;
var firstLineY=0;
var latestLineY=0;
      lines[siteIndex].enter().append("svg:line")
        .attr("x1", function(d) {
          return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).x;
        })
        .attr("x2", function(d) {
          if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
            return "-" + (r + 10);
          else
            return "" + (r + 10);
        })
        .attr("y1", function(d) {
          return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
        })
        .attr("y2", function(d) 
   {

                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {
                                                                                           
                                                                                            var flagDiff = 0;
                                                                                            for (var i = 0; i < genCoordsX.length; i++) {
                                                                                                   var diffcoords = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y- genCoordsX[i]);

                                                                                                   if (diffcoords<10 && diffcoords>0) {

                                                                                                          flagDiff = 1;
                                                                                                          break;
                                                                                                   } else {
                                                                                                          continue;
                                                                                                   }
                                                                                            }
                                                                                            if (flagDiff == 0) {
														 genCoordsX.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y);
                                                                                                   return ""+ findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                                                                            } else {
                                                                                                   if (firstLineX == 0) {
                                                                                                          var incres = findCoordinates(
                                                                                                                       r - 5,
                                                                                                                       (d.startAngle + d.endAngle) / 2).y;
                                                                                                          incres += 20;
															   incres=findIsolation(incres,genCoordsX);
                                                                                                          firstLineX = 1;
                                                                                                          latestLineX = incres;
															   genCoordsX.push(latestLineX);
                                                                                                          return ""+ incres;
                                                                                                   } else {
                                                                                                          latestLineX += 20;
                                                                                                          genCoordsX.push(latestLineX);
                                                                                                          return ""+ latestLineX;
                                                                                                   }
                                                                                            }
                                                                                     }

                                                                                     else {
                                                                                            
                                                                                            var flagDiff = 0;
                                                                                            for (var i = 0; i < genCoordsY.length; i++) {
                                                                                                   var diffcoords = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y - genCoordsY[i]);

                                                                                                   if (diffcoords<10 && diffcoords>0) {

                                                                                                          flagDiff = 1;
                                                                                                          break;
                                                                                                   } else {
                                                                                                          continue;
                                                                                                   }
                                                                                            }
                                                                                            if (flagDiff == 0) {
														   genCoordsY.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y);
                                                                                                   return ""+ findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                                                                            } else {
                                                                                                   if (firstLineY == 0) {
                                                                                                          var incres = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                                                                                          incres += 20;
															   incres=findIsolation(incres,genCoordsY);
                                                                                                          firstLineY = 1;
                                                                                                          latestLineY = incres;
															   genCoordsY.push(latestLineY);
                                                                                                          return ""+ incres;
                                                                                                   } else {
                                                                                                          latestLineY += 20;
                                                                                                          genCoordsY.push(latestLineY);
                                                                                                          return "" + latestLineY;
                                                                                                   }
                                                                                            }

                                                                                     }
                                                                              }).attr("stroke",
                                                                              function(d, i) {
                                                                                     return color(i, siteIndex);
                                                                              });
                                                   lines[siteIndex].transition().duration(
                                                                 tweenDuration);
                                                   lines[siteIndex].exit().remove();
function findIsolation(coord,coordArray){
var coordFirst;
for(var i=0;i<coordArray.length;i++){
	var diff=Math.abs(coord-coordArray[i]);
	if(diff<15 && diff>0){
		coord+=20;
	}
}
coordFirst=coord;
for(var i=0;i<coordArray.length;i++){
	var diff=Math.abs(coord-coordArray[i]);
	if(diff<15 && diff>0){
		coord+=20;
	}
}
if(coord==coordFirst){
return coord;
}
else{findIsolation(coord,coordArray);}
}

  //DRAW ARC PATHS

  paths[siteIndex] = arc_group[siteIndex].selectAll("path").data(filteredPieData);
  paths[siteIndex].enter().append("svg:path")
    .attr("stroke", "white")
    .attr("stroke-width", 0.5)
    .attr("fill", function(d, i) {
      return color(i,siteIndex);
    })
    .transition()
    .duration(tweenDuration)
    .attrTween("d", pieTween);
  paths[siteIndex]
    .transition()
    .duration(tweenDuration)
    .attrTween("d", pieTween);
  paths[siteIndex].exit()
    .transition()
    .duration(tweenDuration)
    .attrTween("d", removePieTween)
    .remove();

  //DRAW LABELS WITH PERCENTAGE VALUES

  							   var genCoordsTextX = [];
                                                   var genCoordsTextY = [];
                                                   var latestTextX = 0;
                                                   var firstTextX = 0;
                                                   var latestTextY = 0;
                                                   var firstTextY = 0;

                                                   valueLabels[siteIndex] = label_group[siteIndex].selectAll("text.value").data(filteredPieData).attr("y",function(d) {
                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {

                                                                                            var flagDiffText = 0;
                                                                                            for (var i = 0; i < genCoordsTextX.length; i++) {
                                                                                                   var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5 - genCoordsTextX[i]);

                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

                                                                                                          flagDiffText = 1;
                                                                                                          break;
                                                                                                   } else {
                                                                                                          continue;
                                                                                                   }
                                                                                            }
                                                                                            if (flagDiffText == 0) {
														   genCoordsTextX.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                                   return ""+ (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                            } else {

                                                                                                   if (firstTextX == 0) {
                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                                                                                          incresText += 25;
															   incresText = findIsolaton(incresText,genCoordsTextX);
                                                                                                          firstTextX = 1;
                                                                                                          latestTextX = incresText;
                                                                                                          genCoordsTextX.push(incresText);
                                                                                                          return ""+ incresText;
                                                                                                   } else {
                                                                                                          latestTextX += 20;
                                                                                                          genCoordsTextX.push(latestTextX);
                                                                                                          return ""+ latestTextX;
                                                                                                   }

                                                                                            }

                                                                                     } else {

                                                                                            var flagDiffText = 0;
                                                                                            for (var i = 0; i < genCoordsTextY.length; i++) {
                                                                                                   var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5- genCoordsTextY[i]);

                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

                                                                                                          flagDiffText = 1;
                                                                                                          break;
                                                                                                   } else {
                                                                                                          continue;
                                                                                                   }
                                                                                            }
                                                                                            if (flagDiffText == 0) {
   														   genCoordsTextY.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                                   return "" + (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                            } else {

                                                                                                   if (firstText == 0) {
                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                                                                                          incresText += 25;
															   incresText = findIsolation(incresText,genCoordsTextY);
                                                                                                          firstTextY = 1;
                                                                                                          latestTextY = incresText;
                                                                                                          genCoordsTextY.push(incresText);
                                                                                                          return "" + incresText;
                                                                                                   } else {
                                                                                                          latestTextY += 20;
                                                                                                          genCoordsTextY.push(latestTextY);
                                                                                                          return ""+ latestTextY;
                                                                                                   }

                                                                                            }

                                                                                     }

                                                                              })
                                                                 .attr(
                                                                              "x",
                                                                              function(d) {
                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                                                                                            return "-" + (r + 10);
                                                                                     else
                                                                                            return "" + (r + 10);
                                                                              })
                                                                 .attr("fill", function(d, i) {
                                                                       return color(i, siteIndex);
                                                                 })
                                                                 .attr(
                                                                              "text-anchor",
                                                                              function(d) {
                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                                                                                            return "end";
                                                                                     else
                                                                                            return "begning";
                                                                              }).text(function(d) {
                                                                       return d.value;
                                                                 });
 							 genCoordsTextX = [];
                                                 genCoordsTextY = [];
                                                 latestTextX = 0;
                                                 firstTextX = 0;
                                                 latestTextY = 0;
                                                 firstTextY = 0;

                                                   valueLabels[siteIndex]
                                                                 .enter()
                                                                 .append("svg:text")
                                                                 .attr("class", "value")
                                                                 .attr(
                                                                              "y",
                                                                              function(d) {
                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {

                                                                                            
                                                                                            var flagDiffText = 0;
                                                                                            for (var i = 0; i < genCoordsTextX.length; i++) {
                                                                                            var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5- genCoordsTextX[i]);

                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

                                                                                                          flagDiffText = 1;
                                                                                                          break;
                                                                                                   } else {
                                                                                                          continue;
                                                                                                   }
                                                                                            }
                                                                                            if (flagDiffText == 0) {
														genCoordsTextX.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                                return ""+ (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                            } else {

                                                                                                   if (firstTextX == 0) {
                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                                                                                          incresText += 25;
                                                                                                          firstTextX = 1;
															   incresText=findIsolation(incresText,genCoordsTextX);
                                                                                                          latestTextX = incresText;
                                                                                                          genCoordsTextX.push(incresText);
                                                                                                          return "" + incresText;
                                                                                                   } else {
                                                                                                          latestTextX += 20;
                                                                                                          genCoordsTextX.push(latestTextX);
                                                                                                          return "" + latestTextX;
                                                                                                   }

                                                                                            }

                                                                                     } else {

                                                                                            var flagDiffText = 0;
                                                                                            for (var i = 0; i < genCoordsTextY.length; i++) {
                                                                                                   var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5- genCoordsTextY[i]);

                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

                                                                                                          flagDiffText = 1;
                                                                                                          break;
                                                                                                   } else {
                                                                                                          continue;
                                                                                                   }
                                                                                            }
                                                                                            if (flagDiffText == 0) {
														   genCoordsTextY.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                                   return ""+ (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
                                                                                            } else {

                                                                                                   if (firstText == 0) {
                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                                                                                          incresText += 25;
                                                                                                          firstTextY = 1;
															   incresText=findIsolation(incresText,genCoordsTextY);
                                                                                                          latestTextY = incresText;
                                                                                                          genCoordsTextY.push(incresText);
                                                                                                          return "" + incresText;
                                                                                                   } else {
                                                                                                          latestTextY += 20;
                                                                                                          genCoordsTextY.push(latestTextY);
                                                                                                          return ""+ latestTextY;
                                                                                                   }

                                                                                            }

                                                                                     }

                                                                              })

                                                                 .attr(
                                                                              "x",
                                                                              function(d) {
                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                                                                                            return "-" + (r + 10);
                                                                                     else
                                                                                            return "" + (r + 10);
                                                                              })
                                                                 .attr("fill", function(d, i) {
                                                                       return color(i, siteIndex);
                                                                })
                                                                 .attr(
                                                                              "text-anchor",
                                                                              function(d) {
                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                                                                                            return "end";
                                                                                     else
                                                                                            return "begning";
                                                                              }).text(function(d) {
                                                                       return d.value;
                                                                 });

                                                   valueLabels[siteIndex].exit().remove();

                                                       //DRAW LABELS WITH ENTITY NAMES
                                                       nameLabels[siteIndex] = label_group[siteIndex]
                                                                     .selectAll("text.units")
                                                                     .data(filteredPieData)
                                                                     .attr(
                                                                                  "dy",
                                                                                  function(d) {
                                                                                         if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2
                                                                                                       && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
                                                                                                return 17;
                                                                                         } else {
                                                                                                return 5;
                                                                                         }
                                                                                  })
                                                                     .attr(
                                                                                  "text-anchor",
                                                                                  function(d) {
                                                                                         if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
                                                                                                return "beginning";
                                                                                         } else {
                                                                                                return "end";
                                                                                         }
                                                                                  }).text(function(d) {
                                                                           return d.name;
                                                                     });

                                                       nameLabels[siteIndex]
                                                                     .enter()
                                                                     .append("svg:text")
                                                                     .attr("class", "units")
                                                                     .attr(
                                                                                  "transform",
                                                                                  function(d) {
                                                                                         return "translate("
                                                                                                       + Math
                                                                                                                     .cos(((d.startAngle
                                                                                                                                  + d.endAngle - Math.PI) / 2))
                                                                                                       * (r + textOffset)
                                                                                                       + ","
                                                                                                       + Math
                                                                                                                     .sin((d.startAngle
                                                                                                                                  + d.endAngle - Math.PI) / 2)
                                                                                                       * (r + textOffset)
                                                                                                       + ")";
                                                                                  })
                                                                     .attr(
                                                                                  "dy",
                                                                                  function(d) {
                                                                                         if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2
                                                                                                       && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
                                                                                                return 17;
                                                                                         } else {
                                                                                                return 5;
                                                                                         }
                                                                                  })
                                                                     .attr(
                                                                                  "text-anchor",
                                                                                  function(d) {
                                                                                         if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
                                                                                                return "beginning";
                                                                                         } else {
                                                                                                return "end";
                                                                                         }
                                                                                  }).text(function(d) {
                                                                           return d.name;
                                                                     });

                                                       nameLabels[siteIndex].transition().duration(
                                                                     tweenDuration).attrTween("transform",
                                                                     textTween);

                                                       nameLabels[siteIndex].exit().remove();
                                                }
                                         }
                                         ///////////////////////////////////////////////////////////
                                         //FUNCTIONS //////////////////////////////////////////////
                                         ///////////////////////////////////////////////////////////

                                         //Interpolate the arcs in data space.
                                         function pieTween(d, i) {
                                                var s0;
                                                var e0;
                                                if (oldPieData[i]) {
                                                       s0 = oldPieData[i].startAngle;
                                                       e0 = oldPieData[i].endAngle;
                                                } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
                                                       s0 = oldPieData[i - 1].endAngle;
                                                       e0 = oldPieData[i - 1].endAngle;
                                                } else if (!(oldPieData[i - 1])
                                                              && oldPieData.length > 0) {
                                                       s0 = oldPieData[oldPieData.length - 1].endAngle;
                                                       e0 = oldPieData[oldPieData.length - 1].endAngle;
                                                } else {
                                                       s0 = 0;
                                                       e0 = 0;
                                                }
                                                var i = d3.interpolate({
                                                       startAngle : s0,
                                                       endAngle : e0
                                                }, {
                                                       startAngle : d.startAngle,
                                                       endAngle : d.endAngle
                                                });
                                                return function(t) {
                                                       var b = i(t);
                                                       return arc(b);
                                                };
                                         }

                                         function removePieTween(d, i) {
                                                s0 = 2 * Math.PI;
                                                e0 = 2 * Math.PI;
                                                var i = d3.interpolate({
                                                       startAngle : d.startAngle,
                                                       endAngle : d.endAngle
                                                }, {
                                                       startAngle : s0,
                                                       endAngle : e0
                                                });
                                                return function(t) {
                                                       var b = i(t);
                                                       return arc(b);
                                                };
                                         }

                                         function textTween(d, i) {
                                                var a;
                                                if (oldPieData[i]) {
                                                       a = (oldPieData[i].startAngle
                                                                     + oldPieData[i].endAngle - Math.PI) / 2;
                                                } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
                                                       a = (oldPieData[i - 1].startAngle
                                                                     + oldPieData[i - 1].endAngle - Math.PI) / 2;
                                                } else if (!(oldPieData[i - 1])
                                                              && oldPieData.length > 0) {
                                                       a = (oldPieData[oldPieData.length - 1].startAngle
                                                                     + oldPieData[oldPieData.length - 1].endAngle - Math.PI) / 2;
                                                } else {
                                                       a = 0;
                                                }
                                                var b = (d.startAngle + d.endAngle - Math.PI) / 2;

                                                var fn = d3.interpolateNumber(a, b);
                                                return function(t) {
                                                       var val = fn(t);
                                                       return "translate(" + Math.cos(val)
                                                                     * (r + textOffset) + ","
                                                                     + Math.sin(val) * (r + textOffset)
                                                                     + ")";
                                                };
                                         }






		
});

</script>
<div class="container-fluid tt-container" id="officePortel">
	<!-- START OFFICE PORTLET HEADER-->
	<div class="row-fluid tt-bvheader">
		<div class="span6 tt-bvpage-title"><liferay-ui:message key="Services and Sites"/></div>
		<div class="span6 tt-detail">
			<a href="<%=prop.getProperty("tt.bv.homeURL")%>/<%=prop.getProperty("ttBVDetails")%>">
			<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
		</div>
	</div>
	<!-- END OFFICE PORTLET HEADER-->
	<!-- START OFFICE PORTLET GRAPH CONTENT-->
	<div class="row-fluid tt-content">
		<div class="span4 tt-bvgraph tt-bvgraph-first tt-gold">
			<div id="subscribedservices"></div>
			<div class="tt-bvgraph-title "><liferay-ui:message key="Subscribed Services"/></div>

			<!-- START GRAPH LEGEND -->
			<div class="row-fluid tt-bvgraph-legend-container tt-bvcenter-container">
				<div class="span12 tt-bvgraph-legend">
					<div class="row-fluid">
					<c:forEach var="legendListDetailsSS" items="${legendListForSS}">
						<c:set var="statusDetailsSS" value="${fn:split(legendListDetailsSS, '-')}"/>
						<div class="span4">
							<div class="tt-red-legend" style="background-color:${statusDetailsSS[1]}"></div>
							<span class="tt-legend-text-red"  style="color:${statusDetailsSS[1]}"><liferay-ui:message key="${statusDetailsSS[0]}"/></span>
						</div>
					</c:forEach>
					</div> 
				</div> 
		 	</div> 
		 	<!-- END GRAPH LEGEND -->
		</div>
		<div class="span4 tt-bvgraph tt-silver" >
			<div id="sitemanagement"></div>
			<div class="tt-bvgraph-title "><liferay-ui:message key="Sites Management"/></div>

			<div class="row-fluid tt-bvgraph-legend-container">
				<div class="span12 tt-bvgraph-legend">
					<div class="row-fluid">
					<c:forEach var="legendListDetailsSM" items="${legendListForSM}">
						<c:set var="statusDetailsSM" value="${fn:split(legendListDetailsSM, '-')}"/>
						<div class="span4">
							<div class="tt-red-legend" style="background-color:${statusDetailsSM[1]}"></div>
							<span class="tt-legend-text-red"  style="color:${statusDetailsSM[1]}"><liferay-ui:message key="${statusDetailsSM[0]}"/></span>
						</div>
					</c:forEach>
					</div> 
				</div> 
		 	</div> 
		 	<!-- END OFFICE PORTLET GRAPH LEGEND -->
		</div>
	</div>
	<!-- END OFFICE PORTLET GRAPH CONTENT-->
	
	<%
		String currentURL = themeDisplay.getURLCurrent();
		String bvDetailsURL = TTGenericUtils.prop.getProperty("ttBVDetails");
	%>
	<!-- START SUBSCRIBED SERVICES TABLE-->
	<div id="userheader">
		<div class="row-fluid" id="subscribedservices"><liferay-ui:message key="Subscribed Services"/></div>
	</div>
	<%@ include file="subscribedservicestable.jsp" %>
	
	<!-- END SUBSCRIBED SERVICES TABLE-->
	
	<!-- START SITE MANAGEMENT TABLE-->
	<div id="userheader">
		<div class="row-fluid" id="sitemanagement"><liferay-ui:message key="Site Management"/></div>
	</div>
	<%@ include file="sitemanagementtable.jsp" %>
	
	<!-- END SITE MANAGEMENT TABLE-->
</div>