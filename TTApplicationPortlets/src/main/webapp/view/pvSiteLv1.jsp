<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.tt.utils.PropertyReader"%>
<%@ page import="java.util.Properties"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getUserData">
</portlet:resourceURL>

<%
	Properties properties = PropertyReader.getProperties();
%>


<%@ include file="common.jsp" %>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>
	<link href="${pageContext.request.contextPath}/css/sites.css"
	rel="stylesheet"></link>
	
<script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
<!-- <style>
a{
color:#fff !important;
text-decoration:none !important;
}
</style> -->	
<script>
"use strict";jQuery.base64=(function($){var _PADCHAR="=",_ALPHA="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",_VERSION="1.0";function _getbyte64(s,i){var idx=_ALPHA.indexOf(s.charAt(i));if(idx===-1){throw"Cannot decode base64"}return idx}function _decode(s){var pads=0,i,b10,imax=s.length,x=[];s=String(s);if(imax===0){return s}if(imax%4!==0){throw"Cannot decode base64"}if(s.charAt(imax-1)===_PADCHAR){pads=1;if(s.charAt(imax-2)===_PADCHAR){pads=2}imax-=4}for(i=0;i<imax;i+=4){b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6)|_getbyte64(s,i+3);x.push(String.fromCharCode(b10>>16,(b10>>8)&255,b10&255))}switch(pads){case 1:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6);x.push(String.fromCharCode(b10>>16,(b10>>8)&255));break;case 2:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12);x.push(String.fromCharCode(b10>>16));break}return x.join("")}function _getbyte(s,i){var x=s.charCodeAt(i);if(x>255){throw"INVALID_CHARACTER_ERR: DOM Exception 5"}return x}function _encode(s){if(arguments.length!==1){throw"SyntaxError: exactly one argument required"}s=String(s);var i,b10,x=[],imax=s.length-s.length%3;if(s.length===0){return s}for(i=0;i<imax;i+=3){b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8)|_getbyte(s,i+2);x.push(_ALPHA.charAt(b10>>18));x.push(_ALPHA.charAt((b10>>12)&63));x.push(_ALPHA.charAt((b10>>6)&63));x.push(_ALPHA.charAt(b10&63))}switch(s.length-imax){case 1:b10=_getbyte(s,i)<<16;x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_PADCHAR+_PADCHAR);break;case 2:b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8);x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_ALPHA.charAt((b10>>6)&63)+_PADCHAR);break}return x.join("")}return{decode:_decode,encode:_encode,VERSION:_VERSION}}(jQuery));
</script>
<script type="text/javascript">
	var graphColorDataMWAN = ${pvSiteStatusGraphDataMWAN};
	var colorsMWAN=${pvGraphColorListSiteStatusMWAN};

	var graphColorDataMWANOPTI = ${pvSiteStatusGraphDataMWANOPTI};
	var colorsMWANOPTI=${pvGraphColorListSiteStatusMWANOPTI};

	var graphColorDataMWLAN = ${pvSiteStatusGraphDataMWLAN};
	var colorsMWLAN=${pvGraphColorListSiteStatusMWLAN};	

	var graphColorDataMLAN = ${pvSiteStatusGraphDataMLAN};
	var colorsMLAN=${pvGraphColorListSiteStatusMLAN};	

	var graphColorDataMFIREWALL = ${pvSiteStatusGraphDataMFIREWALL};
	var colorsMFIREWALL=${pvGraphColorListSiteStatusMFIREWALL};		
    
    $(document).ready(function(){
        do_stuff( graphColorDataMWAN,colorsMWAN,'mwan');
        do_stuff( graphColorDataMWANOPTI,colorsMWANOPTI,'mwanopti');
        do_stuff(graphColorDataMWLAN,colorsMWLAN,'mwlan');
        do_stuff(graphColorDataMLAN,colorsMLAN,'mlan');
        do_stuff(graphColorDataMFIREWALL,colorsMFIREWALL,'mfirewall');
        /* do_mwanopti(graphColorDataMWANOPTI,colorsMWANOPTI);
        do_mwlan(graphColorDataMWLAN,colorsMWLAN);
        do_mlan(graphColorDataMLAN,colorsMLAN);
        do_mfirewall(graphColorDataMFIREWALL,colorsMFIREWALL); */
    });	
	
</script>
	
<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/sites.js"></script>
<script>
/**
	function basedOnTicket() {
		$('#basedOnSLATab-li').removeClass('selected');
		$('#basedOnTicket-li').addClass('selected');
		$('#site-sla').addClass('hide');
		$('#site-incident').removeClass('hide');
	}

	function basedOnSLA() {	
		$('#basedOnTicket-li').removeClass('selected');
		$('#basedOnSLATab-li').addClass('selected');
		$('#site-sla').removeClass('hide');
		$('#site-incident').addClass('hide');
		
	}
**/	
	var VMs = [{sitename:'EC5GK000F3BAA',colour:'background-color:#ff9c00',priority:'2'},
	           {sitename:'EC5GK000F3BAA',colour:'background-color:rgb(163, 207, 98)',priority:'3'},
	           {sitename:'EC5GK000F3BAA',colour:'background-color:rgb(163, 207, 98)',priority:'3'},
	           {sitename:'EC5GK000F3BAA',colour:'background-color:rgb(163, 207, 98)',priority:'3'}];           
				

	VMs = JSON.stringify('${bysitejson}');
	VMs = eval('(' + '${bysitejson}' + ')');

	var temp = VMs.slice();
	
	$(document).ready(function(){
		
		var hash = document.location.hash;
		if(hash){
			if(hash === '#basedOnTicketTab'){
				basedOnTicket();
			}
		}
		else{
			basedOnTicket();
		}		

		VMs = VMs.sort(function IHaveAName(a, b) { return b.priority> a.priority?  1 : b.priority< a.priority? -1 : 0; });

		populateData(VMs);	
		
		$("#linkTable").addClass('hide');
					
	})
	
	Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
		  return v[name] === value ? null : v;
	   });
	   this.length = 0;
	   this.push.apply(this, array);
	}
	
	$(function() {
		
		$("input[type='checkbox']").change(
		function()	{
			
				temp = VMs.slice();			
				var all = false;
				var priority1 = false;
				var priority2 = false;
				var priority3 = false;
				var normal = false;
				
				if($('#checkbox_all').is(':checked')) 
				{		
					all = true;
				}
				if($('#checkbox_priority1').is(':checked')) 
				{		
					priority1 = true;
				}
				if($('#checkbox_priority2').is(':checked')) 
				{		
					priority2 = true;
				}
				if($('#checkbox_priority3').is(':checked')) 
				{		
					priority3 = true;
				}
				if($('#checkbox_normal').is(':checked')) 
				{		
					normal = true;
				}

				if(all == false)
				{
					if(priority1==false)
						temp.removeValue('priority', '1');
					if(priority2==false)
						temp.removeValue('priority', '2');
					if(priority3==false)
						temp.removeValue('priority', '3');
					if(normal==false)
						temp.removeValue('priority', '4');
				}
				
				$('#searchText').val('');
				
				populateData(temp);			
		});	
		});
	
	var sortByName = function sortByName(a, b) { 
		return a.priority - b.priority;
	}
	
	function populateData(temp)
	{
		/* temp = temp.sort(IHaveAName); */		
		temp = temp.sort(sortByName);
		
		if(temp.length==0)
		{
			document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
			return;
		}
		else
		{
			var no = 0;
			var cek = 0;
			var str = "";
			str = "<div class='row-fluid'>";
			for (i in temp)
			{
				if(temp[i].sitename!=null){
					/* var URL = '/group/telkomtelstra/sitedetaillevel2?' + $.base64.encode('device'+'&'+temp[i].sitename); */
					var URL = '/group/telkomtelstra/sitedetaillevel2?' + 'DEVICE'+'&'+temp[i].sitename;
					str += "<div class='span2'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td height='50' align='center'><div><a href='"+URL+"' style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].sitename+"</a></div></td><td></td></tr></table></div></div>";
					no++;
					cek++;
					if(no==6)
					{
						no=0;
						str += "</div><div class='row-fluid'>";			 
					}
				}
			}
			str += "</div>";
			
			if(cek==0){
				document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
			}else{
				document.getElementById("data-RB").innerHTML = str;
			}
		}
	}

</script>

<style>

#sitePortletTitleBar{
       height:37px;
}

#siteTitleText{
       line-height:34px;
}
#siteNavigationArrow{
       line-height:34px;
}
.site-title-dashboard {
    font-size: 15px;
    display: flex;
    justify-content: center;
}

#siteCountId{
       fill:#888 !important;
       font-size:20px !important;
}

#siteLabelId{
       fill:#888 !important;
       font-size:18px !important;
       font-weight: 100;
}

div#site {
    display: flex;
   justify-content: center;
}

#markers-tt {
       display: flex;
       justify-content: center;
}

#tt-red {
       background-color: #e32212;
}

#tt-black {
       background-color: #000000;
}

#tt-maroon {
       background-color: #A40800;
}

#tt-purple {
       background-color: #7030A0;
}

#tt-blue {
       background-color: #00B0F0;
}

#tt-orange {
       background-color: #FA8072;
}

#tt-amber {
       background-color: #ff9c00;
}

#tt-green {
       background-color: #a3cf62;
}

#tt-yellow {
       background-color: #ffff00;
}

#tt-black-marker {
       color: #000000;
}

#tt-red-marker {
       color: #e32212;
}

#tt-maroon-marker {
       color: #A40800;
}

#tt-purple-marker {
       color: #7030A0;
}

#tt-blue-marker {
       color: #00B0F0;
}

#tt-orange-marker {
       color: #FA8072;
}       

#tt-amber-marker {
       color: #ff9c00;
}

#tt-green-marker {
       color: #a3cf62;
}

#tt-yellow-marker {
       color: #ffff00;
}

.tt-color-legends {
       display: inline-block;
       width: 10px;
       height: 10px;
       position: relative;
       top: 5px;
       margin: 0 5px;
}

.tt-markers {
       display: inline-block;
       font-size: 12px;
}

.legend-site {
       font-size: 15px;
       line-height: 120%;
       padding-top: 5px;
       padding-bottom: 30px;
       height: 30px;
       text-align: center;
}

#siteblock {
       justify-content: center;
       background-color: #fff;
       height: 262px;
       /* border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; 
       margin-left: 50px;*/
}

#mwan {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mwanopti {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mwlan {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mlan {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mfirewall {
       display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

.siteServices {
       width: 150px;
       margin: 0 40px 0 40px;
}

.value-site {
       text-align: center;
       height: 50px;
       line-height: 50px;
       border: solid 1px black;
       border-radius: 8px;
}

#status-val-service {
       border-color: transparent;
}

.row-fluid {
    width: 100%;
    margin-bottom: 1%;
}

@media (max-width: 500px){
	#markers-tt {
    	display: block;
	}
}

@media ( min-width :768px){
	#buttonPvDashboard{
		float:right;
	}
}


</style>

<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
				<span class="tt-current-page"><liferay-ui:message key="Site Information"/></span>
			</div>
		</div>
		<div class="span6 tt-back"><button id="buttonPvDashboard" type="button" onclick="window.location='pvdashboardlv0'" class="btn-primary-tt backButton">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
	</div>
</div>


<div class="container-fluid tt-site-tab-container">	
	<div id="site-sla" class="row-fluid tt-site-tab-incident-content">
	
	<div class="span12 tt-graph tt-graph-first tt-gold">
	<div style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
		<div id="markers-tt">
     
			  <div class="tt-color-legends" id="tt-black"></div>
              <div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="Order Received" /></div>
              <div class="tt-color-legends" id="tt-maroon"></div>
              <div class="tt-markers" id="tt-maroon-marker"><liferay-ui:message key="Detailed Design" /></div>
              <div class="tt-color-legends" id="tt-purple"></div>
              <div class="tt-markers" id="tt-purple-marker"><liferay-ui:message key="Procurement" /></div>
              <div class="tt-color-legends" id="tt-blue"></div>
              <div class="tt-markers" id="tt-blue-marker"><liferay-ui:message key="Delivery & Activation" /></div>
              <div class="tt-color-legends" id="tt-orange"></div>
              <div class="tt-markers" id="tt-orange-marker"><liferay-ui:message key="Monitoring" /></div>
              <div class="tt-color-legends" id="tt-green"></div>
              <div class="tt-markers" id="tt-green-marker"><liferay-ui:message key="Operational" /></div>
              
       </div>
	</div>
	<div
       style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
		<!-- <div id="data-RB" style="padding:10px;background-color: white;">
		</div> -->
		
		<div class="row-fluid">
		<c:choose>
			<c:when test="${pvSubscribedMWAN gt 0}">
				<div class="span4">
			</c:when>
			<c:otherwise>
				<div class="span4" style="opacity:.5;">
			</c:otherwise>
		</c:choose>
			<div class="container-fluid tt-container" id="officePortel">
				<!-- START OFFICE PORTLET HEADER-->
				<div class="row-fluid tt-header">
					<div class="span8 tt-page-title"><liferay-ui:message key="mns-wan"/></div>
					<div class="span4 tt-detail">
				<c:choose>
				    <c:when test="${pvSubscribedMWAN gt 0}">
				        <a href="pvdashboardlv2?MNS_WAN">
								<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:when>
				    <c:otherwise>
				    	<a><img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:otherwise>
				</c:choose>
						
					</div>
				</div>
				<!-- END OFFICE PORTLET HEADER-->
				<!-- START OFFICE PORTLET GRAPH CONTENT-->
					<!-- <div class="row-fluid tt-content">
					<div class="span4 tt-graph tt-graph-first tt-gold pv-gold">
	
						<div id="mwan">
						</div>
					</div>
					
				</div> -->
				<div id="siteblock">
				<br>
					<!-- <div id="mwan">
						</div> -->
				<c:choose>
				    <c:when test="${pvSubscribedMWAN gt 0}">
				        <div id=mwan>
						</div>
				    </c:when>
				    <c:otherwise>
				        <div class="row-fluid tt-content"
							style="text-align: center; margin-top: 10%; font-size: 16px;line-height: 40px;" id="msgContent">
							<img class="imgalign"src="<%=request.getContextPath()%>/images/no mns.png"style="width: 40px; margin-right: 17px;" />
							<liferay-ui:message key="You currently have not purchased MNS-WAN services. To enquire about MNS-WAN Services, please"/> <a
								href="<%= properties.getProperty("unsubscribedservicelink")%>" target="_blank">&nbsp;<liferay-ui:message key="click here"/></a>.
						</div>
				    </c:otherwise>
				</c:choose>
				</div>
				<!-- END OFFICE PORTLET GRAPH CONTENT-->
				<!-- START OFFICE PORTLET GRAPH LEGEND -->
				<!-- END OFFICE PORTLET GRAPH LEGEND -->
			</div>
		</div>
		
		<c:choose>
			<c:when test="${pvSubscribedMWAN gt 0}">
				<div class="span4">
			</c:when>
			<c:otherwise>
				<div class="span4" style="opacity:.5;">
			</c:otherwise>
		</c:choose>
			<div class="container-fluid tt-container " id="officePortel">
				<!-- START OFFICE PORTLET HEADER-->
				<div class="row-fluid tt-header">
					<div class="span8 tt-page-title"><liferay-ui:message key="mns-wan opti"/></div>
					<div class="span4 tt-detail">
					<c:choose>
				    <c:when test="${pvSubscribedMWANOPTI gt 0}">
				        <a href="pvdashboardlv2?MNS_WANOPT">
								<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:when>
				    <c:otherwise>
				    	<a><img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:otherwise>
				</c:choose>
							
					</div>
				</div>
				<!-- END OFFICE PORTLET HEADER-->
				<!-- START OFFICE PORTLET GRAPH CONTENT-->
					<!-- <div class="row-fluid tt-content">
					<div class="span4 tt-graph tt-graph-first tt-gold pv-gold">
	
						<div id="mwanopti">
						</div>
					</div>
					
				</div> -->
				<div id="siteblock">
				<br>
					<!-- <div id="mwanopti">
						</div> -->
						<c:choose>
				    <c:when test="${pvSubscribedMWANOPTI gt 0}">
				        <div id=mwanopti>
						</div>
				    </c:when>
				    <c:otherwise>
				        <div class="row-fluid tt-content"
							style="text-align: center; margin-top: 10%; font-size: 16px;line-height: 40px;" id="msgContent">
							<img class="imgalign"src="<%=request.getContextPath()%>/images/no mns.png"style="width: 40px; margin-right: 17px;" />
							<liferay-ui:message key="You currently have not purchased MNS-WAN OPTI services. To enquire about MNS-WAN OPTI Services, please"/> <a
								href="<%= properties.getProperty("unsubscribedservicelink")%>" target="_blank">&nbsp;<liferay-ui:message key="click here"/></a>.
						</div>
				    </c:otherwise>
				</c:choose>
				</div>
				<!-- END OFFICE PORTLET GRAPH CONTENT-->
				<!-- START OFFICE PORTLET GRAPH LEGEND -->
				<!-- END OFFICE PORTLET GRAPH LEGEND -->
			</div>
		</div>
		
		<c:choose>
			<c:when test="${pvSubscribedMWLAN gt 0}">
				<div class="span4">
			</c:when>
			<c:otherwise>
				<div class="span4" style="opacity:.5;">
			</c:otherwise>
		</c:choose>
			<div class="container-fluid tt-container " id="officePortel">
				<!-- START OFFICE PORTLET HEADER-->
				<div class="row-fluid tt-header">
					<div class="span8 tt-page-title"><liferay-ui:message key="mns-wlan"/></div>
					<div class="span4 tt-detail">
					<c:choose>
				    <c:when test="${pvSubscribedMWLAN gt 0}">
				        <a href="pvdashboardlv2?MNS_WLAN">
								<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:when>
				    <c:otherwise>
				    	<a><img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:otherwise>
				</c:choose>
							
					</div>
				</div>
				<!-- END OFFICE PORTLET HEADER-->
				<!-- START OFFICE PORTLET GRAPH CONTENT-->
					<!-- <div class="row-fluid tt-content">
					<div class="span4 tt-graph tt-graph-first tt-gold pv-gold">
	
						<div id="mwlan">
						</div>
					</div>
					
				</div> -->
				<div id="siteblock">
				<br>
					<!-- <div id="mwlan">
						</div> -->
						<c:choose>
				    <c:when test="${pvSubscribedMWLAN gt 0}">
				        <div id=mwlan>
						</div>
				    </c:when>
				    <c:otherwise>
				        <div class="row-fluid tt-content"
							style="text-align: center; margin-top: 10%; font-size: 16px;line-height: 40px;" id="msgContent">
							<img class="imgalign"src="<%=request.getContextPath()%>/images/no mns.png"style="width: 40px; margin-right: 17px;" />
							<liferay-ui:message key="You currently have not purchased MNS-WLAN services. To enquire about MNS-WLAN Services, please"/> <a
								href="<%= properties.getProperty("unsubscribedservicelink")%>" target="_blank">&nbsp;<liferay-ui:message key="click here"/></a>.
						</div>
				    </c:otherwise>
				</c:choose>
				</div>
				<!-- END OFFICE PORTLET GRAPH CONTENT-->
				<!-- START OFFICE PORTLET GRAPH LEGEND -->
				<!-- END OFFICE PORTLET GRAPH LEGEND -->
			</div>
		</div>		
	</div>
	<!-- <div style="clear:both; height: 20px;"></div> -->
	<div class="row-fluid">
		<c:choose>
			<c:when test="${pvSubscribedMLAN gt 0}">
				<div class="span4">
			</c:when>
			<c:otherwise>
				<div class="span4" style="opacity:.5;">
			</c:otherwise>
		</c:choose>
			<div class="container-fluid tt-container" id="officePortel">
				<!-- START OFFICE PORTLET HEADER-->
				<div class="row-fluid tt-header">
					<div class="span8 tt-page-title"><liferay-ui:message key="mns-lan"/></div>
					<div class="span4 tt-detail">
					<c:choose>
				    <c:when test="${subcsribedMLAN eq '1'}">
				        <a href="pvdashboardlv2?MNS_LAN">
								<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:when>
				    <c:otherwise>
				    	<a><img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
				    </c:otherwise>
				</c:choose>
							
					</div>
				</div>
				<!-- END OFFICE PORTLET HEADER-->
				<!-- START OFFICE PORTLET GRAPH CONTENT-->
					<!-- <div class="row-fluid tt-content">
					<div class="span4 tt-graph tt-graph-first tt-gold pv-gold">
	
						<div id="mlan">
						</div>
					</div>
					
				</div> -->
				<div id="siteblock">
				<br>
				<c:choose>
				    <c:when test="${pvSubcsribedMLAN gt 0}">
				        <div id=mlan>
						</div>
				    </c:when>
				    <c:otherwise>
				        <div class="row-fluid tt-content"
							style="text-align: center; margin-top: 10%; font-size: 16px;line-height: 40px;" id="msgContent">
							<img class="imgalign"src="<%=request.getContextPath()%>/images/no mns.png"style="width: 40px; margin-right: 17px;" />
							<liferay-ui:message key="You currently have not purchased MNS-LAN services. To enquire about MNS-LAN Services, please"/> <a
								href="<%= properties.getProperty("unsubscribedservicelink")%>" target="_blank">&nbsp;<liferay-ui:message key="click here"/></a>.
						</div>
				    </c:otherwise>
				</c:choose>
				
				</div>
				<!-- END OFFICE PORTLET GRAPH CONTENT-->
				<!-- START OFFICE PORTLET GRAPH LEGEND -->
				<!-- END OFFICE PORTLET GRAPH LEGEND -->
			</div>
		</div>
		
		<c:choose>
			<c:when test="${pvSubscribedMFIREWALL gt 0}">
				<div class="span4">
			</c:when>
			<c:otherwise>
				<div class="span4" style="opacity:.5;">
			</c:otherwise>
		</c:choose>
			<div class="container-fluid tt-container " id="officePortel">
				<!-- START OFFICE PORTLET HEADER-->
				<div class="row-fluid tt-header">
					<div class="span8 tt-page-title"><liferay-ui:message key="mss firewall"/></div>
					<div class="span4 tt-detail">
					<c:choose>
				    <c:when test="${pvSubscribedMFIREWALL gt 0}">
				        <a href="pvdashboardlv2?NGFW">
								<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>	
				    </c:when>
				    <c:otherwise>
				    	<a>
								<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>	
				    </c:otherwise>
				</c:choose>
				</div>
				</div>
				<!-- END OFFICE PORTLET HEADER-->
				<!-- START OFFICE PORTLET GRAPH CONTENT-->
					<!-- <div class="row-fluid tt-content">
					<div class="span4 tt-graph tt-graph-first tt-gold pv-gold">
	
						<div id="mfirewall">
						</div>
					</div>
					
				</div> -->
				<div id="siteblock">
				<br>
					<!-- <div id="mfirewall">
						</div> -->
						<c:choose>
				    <c:when test="${pvSubscribedMFIREWALL gt 0}">
				        <div id=mfirewall>
						</div>
				    </c:when>
				    <c:otherwise>
				        <div class="row-fluid tt-content"
							style="text-align: center; margin-top: 10%; font-size: 16px;line-height: 40px;" id="msgContent">
							<img class="imgalign"src="<%=request.getContextPath()%>/images/no mns.png"style="width: 40px; margin-right: 17px;" />
							<liferay-ui:message key="You currently have not purchased MSS FIREWALL services. To enquire about MSS FIREWALL Services, please"/> <a
								href="<%= properties.getProperty("unsubscribedservicelink")%>" target="_blank">&nbsp;<liferay-ui:message key="click here"/></a>.
						</div>
				    </c:otherwise>
				</c:choose>
				</div>
				<!-- END OFFICE PORTLET GRAPH CONTENT-->
				<!-- START OFFICE PORTLET GRAPH LEGEND -->
				<!-- END OFFICE PORTLET GRAPH LEGEND -->
			</div>
		</div>
		
		<div class="span4">
	
		</div>		
	</div>
	
	
	</div>
</div>
	</div>
</div>

<script>
function do_stuff( graphColorDataSaaS,colors, productClassification ) {
	//new code//
	var w = 280;
	var h = 200;
	var r = 90;
	var ir = 80;
	var textOffset = 14;
	var tweenDuration = 500;

	//OBJECTS TO BE POPULATED WITH DATA LATER
	var lines = Array();
	var valueLabels = Array();
	var nameLabels = Array();
	var pieData = [];
	var oldPieData = [];
	var filteredPieData = [];

	//D3 helper function to populate pie slice parameters from array data
	var donut = d3.layout.pie().value(function(d) {
	  return d.octetTotalCount;
	});

	//D3 helper function to create colors from an ordinal scale

	//var color = d3.scale.category20();
	//var colors = ['#E32212', '#FF9C00', '#A3CF62', '#000000', '#8005D8', '#30BFFC'];

	//var colors = ['#E32212', '#FF9C00', '#A3CF62'];

	var siteColors = [colors.slice(0),colors.slice(0),colors.slice(0)];
	var color = function(index,siteIndex) {
	    if (index < siteColors[siteIndex].length)
	      return siteColors[siteIndex][index];
	    else
	      return '#888888';
	  }
	  
	  
	  //D3 helper function to draw arcs, populates parameter "d" in path object
	var arc = d3.svg.arc()
	  .startAngle(function(d) {
	    return d.startAngle;
	  })
	  .endAngle(function(d) {
	    return d.endAngle;
	  })
	  .innerRadius(ir)
	  .outerRadius(r);
	  
	  


	///////////////////////////////////////////////////////////
	//GENERATE FAKE DATA /////////////////////////////////////
	///////////////////////////////////////////////////////////

	var streakerDataAdded;

	function findCoordinates(r, angle) {
	  //alert(angle);
	  angle = angle - Math.PI / 2;
	  return {
	    x: Math.cos(angle) * r,
	    y: Math.sin(angle) * r
	  };
	}

	function fillArray() {
	  return {
	    port: "",
	    octetTotalCount: 10
	  };
	}



	///////////////////////////////////////////////////////////
	//CREATE VIS & GROUPS ////////////////////////////////////
	///////////////////////////////////////////////////////////
	//var sites = ['gold', 'silver', 'bronze'];
	var sites = [productClassification];
	var vis = Array();
	var arc_group = Array();
	var center_group = Array();
	var paths = Array();
	var label_group = Array();
	var grayCircle = Array();
	var whiteCircle = Array();
	var totalLabel = Array();
	var totalValue = Array();

	var graphColorCode = [];

	var count = 0;

	//Temp Values
	/*var ttOfficeBuildingSilverColorMap={Green:5};
	var ttOfficeBuildingGoldColorMap={Red:0,Green:0,Amber:3};
	var ttOfficeBuildingBronzeColorMap={Green:2};*/


	//var ttOfficeBuildingSilverColorMap={Green:5};
	//var ttOfficeBuildingGoldColorMap={Red:0,Green:0,Amber:3};
	//var ttOfficeBuildingBronzeColorMap={Green:2};


			for(var graphColor in colors ){
			
				var clr = colors[graphColor];
				graphColorCode.push(graphColorDataSaaS[clr]);
			

			
		}	
		
		


	var graphJsonObject=[];


	for (i = 0; i < graphColorCode.length; i++) { 
		graphJsonObject.push({octetTotalCount: graphColorCode[i]});
	}




	var totalData=[];

	totalData.push(graphJsonObject);





	    for(var p = 0;p<totalData.length;p++){
	      var tempArray =  totalData[p];
	      for(var q=tempArray.length-1;q>=0;q--){
	        if(tempArray[q].octetTotalCount<1){
	          tempArray.splice(q,1);
	          siteColors[p].splice(q,1);
	        }
	      }
	    }

	for (var i = 0; i < sites.length; i++) {
	  vis[i] = d3.select("#" + sites[i]).append("svg:svg")
	    .attr("width", w)
	    .attr("height", h);

	  //	GROUP FOR ARCS/PATHS
	  arc_group[i] = vis[i].append("svg:g")
	    .attr("class", "arc")
	    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

	  //	GROUP FOR CENTER TEXT  
	  center_group[i] = vis[i].append("svg:g")
	    .attr("class", "center_group")
	    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

	  //	PLACEHOLDER GRAY CIRCLE
	  paths[i] = arc_group[i].append("svg:circle")
	    .attr("fill", "#EFEFEF")
	    .attr("r", r);

	  //	GROUP FOR LABELS
	  label_group[i] = vis[i].append("svg:g")
	    .attr("class", "label_group")
	    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

	  ///////////////////////////////////////////////////////////
	  //	CENTER TEXT ////////////////////////////////////////////
	  ///////////////////////////////////////////////////////////

	  //	WHITE CIRCLE BEHIND LABELS
	  grayCircle[i] = center_group[i].append("svg:circle")
	    .attr("fill", "#eeeeee")
	    .attr("r", ir);

	  whiteCircle[i] = center_group[i].append("svg:circle")
	    .attr("fill", "white")
	    .attr("r", ir - 7);

	  //	"TOTAL" LABEL
	  totalLabel[i] = center_group[i].append("svg:text")
	    .attr("class", "label")
	    .attr("dy", 15)
	    .attr("text-anchor", "middle") // text-align: right
	    .text("Total");

	  //	TOTAL TRAFFIC VALUE
	  totalValue[i] = center_group[i].append("svg:text")
	    .attr("class", "total")
	    .attr("dy", -5)
	    .attr("text-anchor", "middle") // text-align: right
	    .text("0");
	    
/*	  whiteCircle[i] = center_group[i].append("svg:circle")
	    .attr("fill", "white")
	    .attr("r", ir - 7);*/

	}
	///////////////////////////////////////////////////////////
	//STREAKER CONNECTION ////////////////////////////////////
	///////////////////////////////////////////////////////////

	for (var j = 0; j < sites.length; j++) {
	  draw(j);
	  draw(j);
	  
	}

	//to run each time data is generated
	function draw(siteIndex) {
	    streakerDataAdded = totalData[siteIndex];
	    oldPieData = filteredPieData;
	    pieData = donut(streakerDataAdded);
	    
	    var totalOctets = 0;

	    function filterData(element, index, array) {
	      element.name = streakerDataAdded[index].port;
	      element.value = streakerDataAdded[index].octetTotalCount;
	      totalOctets += element.value;
	      return (element.value >= 0);
	    }
	    filteredPieData = pieData.filter(filterData);
	    if (filteredPieData.length > 0 && oldPieData.length > 0) {

	      //REMOVE PLACEHOLDER CIRCLE
	      arc_group[siteIndex].selectAll("circle").remove();

	      totalValue[siteIndex].text(function() {

	        return totalOctets;
	        //return bchart.label.abbreviated(totalOctets*8);
	      });

	      //DRAW TICK MARK LINES FOR LABELS
	lines[siteIndex] = label_group[siteIndex].selectAll("line").data(filteredPieData);
	   var genCoordsX=[];
	   var genCoordsY=[];
	   var firstLineX=0;
	   var latestLineX=0;
	   var firstLineY=0;
	   var latestLineY=0;
	  lines[siteIndex].enter().append("svg:line")
	    .attr("x1", function(d) {
	      return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).x;
	    })
	    .attr("x2", function(d) {
	      if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
	        return "-" + (r + 10);
	      else
	        return "" + (r + 10);
	    })
	    .attr("y1", function(d) {
	      return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
	    })
	    .attr("y2", function(d) 
	   {



	                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
	                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {
	                                                                                           
	                                                                                            var flagDiff = 0;
	                                                                                            for (var i = 0; i < genCoordsX.length; i++) {
	                                                                                                   var diffcoords = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y- genCoordsX[i]);

	                                                                                                   if (diffcoords<10 && diffcoords>0) {

	                                                                                                          flagDiff = 1;
	                                                                                                          break;
	                                                                                                   } else {
	                                                                                                          continue;
	                                                                                                   }
	                                                                                            }
	                                                                                            if (flagDiff == 0) {
															 genCoordsX.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y);
	                                                                                                   return ""+ findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
	                                                                                            } else {
	                                                                                                   if (firstLineX == 0) {
	                                                                                                          var incres = findCoordinates(
	                                                                                                                       r - 5,
	                                                                                                                       (d.startAngle + d.endAngle) / 2).y;
	                                                                                                          incres += 20;
																   incres=findIsolation(incres,genCoordsX);
	                                                                                                          firstLineX = 1;
	                                                                                                          latestLineX = incres;
																   genCoordsX.push(latestLineX);
	                                                                                                          return ""+ incres;
	                                                                                                   } else {
	                                                                                                          latestLineX += 20;
	                                                                                                          genCoordsX.push(latestLineX);
	                                                                                                          return ""+ latestLineX;
	                                                                                                   }
	                                                                                            }
	                                                                                     }

	                                                                                     else {
	                                                                                            
	                                                                                            var flagDiff = 0;
	                                                                                            for (var i = 0; i < genCoordsY.length; i++) {
	                                                                                                   var diffcoords = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y - genCoordsY[i]);

	                                                                                                   if (diffcoords<10 && diffcoords>0) {

	                                                                                                          flagDiff = 1;
	                                                                                                          break;
	                                                                                                   } else {
	                                                                                                          continue;
	                                                                                                   }
	                                                                                            }
	                                                                                            if (flagDiff == 0) {
															   genCoordsY.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y);
	                                                                                                   return ""+ findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
	                                                                                            } else {
	                                                                                                   if (firstLineY == 0) {
	                                                                                                          var incres = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
	                                                                                                          incres += 20;
																   incres=findIsolation(incres,genCoordsY);
	                                                                                                          firstLineY = 1;
	                                                                                                          latestLineY = incres;
																   genCoordsY.push(latestLineY);
	                                                                                                          return ""+ incres;
	                                                                                                   } else {
	                                                                                                          latestLineY += 20;
	                                                                                                          genCoordsY.push(latestLineY);
	                                                                                                          return "" + latestLineY;
	                                                                                                   }
	                                                                                            }

	                                                                                     }
	                                                                              }).attr("stroke",
	                                                                              function(d, i) {
	                                                                                     return color(i, siteIndex);
	                                                                              });
	                                                   lines[siteIndex].transition().duration(
	                                                                 tweenDuration);
	                                                   lines[siteIndex].exit().remove();
	function findIsolation(coord,coordArray){
	var coordFirst;
	for(var i=0;i<coordArray.length;i++){
		var diff=Math.abs(coord-coordArray[i]);
		if(diff<15 && diff>0){
			coord+=20;
		}
	}
	coordFirst=coord;
	for(var i=0;i<coordArray.length;i++){
		var diff=Math.abs(coord-coordArray[i]);
		if(diff<15 && diff>0){
			coord+=20;
		}
	}
	if(coord==coordFirst){
	return coord;
	}
	else{findIsolation(coord,coordArray);}
	}

	  //DRAW ARC PATHS

	  paths[siteIndex] = arc_group[siteIndex].selectAll("path").data(filteredPieData);
	  paths[siteIndex].enter().append("svg:path")
	    .attr("stroke", "white")
	    .attr("stroke-width", 0.5)
	    .attr("fill", function(d, i) {
	      return color(i,siteIndex);
	    })
	    .transition()
	    .duration(tweenDuration)
	    .attrTween("d", pieTween);
	  paths[siteIndex]
	    .transition()
	    .duration(tweenDuration)
	    .attrTween("d", pieTween);
	  paths[siteIndex].exit()
	    .transition()
	    .duration(tweenDuration)
	    .attrTween("d", removePieTween)
	    .remove();

	  //DRAW LABELS WITH PERCENTAGE VALUES

	  							   var genCoordsTextX = [];
	                                                   var genCoordsTextY = [];
	                                                   var latestTextX = 0;
	                                                   var firstTextX = 0;
	                                                   var latestTextY = 0;
	                                                   var firstTextY = 0;

	                                                   valueLabels[siteIndex] = label_group[siteIndex].selectAll("text.value").data(filteredPieData).attr("y",function(d) {
	                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
	                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {

	                                                                                            var flagDiffText = 0;
	                                                                                            for (var i = 0; i < genCoordsTextX.length; i++) {
	                                                                                                   var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5 - genCoordsTextX[i]);

	                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

	                                                                                                          flagDiffText = 1;
	                                                                                                          break;
	                                                                                                   } else {
	                                                                                                          continue;
	                                                                                                   }
	                                                                                            }
	                                                                                            if (flagDiffText == 0) {
															   genCoordsTextX.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                                   return ""+ (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                            } else {

	                                                                                                   if (firstTextX == 0) {
	                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
	                                                                                                          incresText += 25;
																   incresText = findIsolaton(incresText,genCoordsTextX);
	                                                                                                          firstTextX = 1;
	                                                                                                          latestTextX = incresText;
	                                                                                                          genCoordsTextX.push(incresText);
	                                                                                                          return ""+ incresText;
	                                                                                                   } else {
	                                                                                                          latestTextX += 20;
	                                                                                                          genCoordsTextX.push(latestTextX);
	                                                                                                          return ""+ latestTextX;
	                                                                                                   }

	                                                                                            }

	                                                                                     } else {

	                                                                                            var flagDiffText = 0;
	                                                                                            for (var i = 0; i < genCoordsTextY.length; i++) {
	                                                                                                   var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5- genCoordsTextY[i]);

	                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

	                                                                                                          flagDiffText = 1;
	                                                                                                          break;
	                                                                                                   } else {
	                                                                                                          continue;
	                                                                                                   }
	                                                                                            }
	                                                                                            if (flagDiffText == 0) {
	   														   genCoordsTextY.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                                   return "" + (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                            } else {

	                                                                                                   if (firstText == 0) {
	                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
	                                                                                                          incresText += 25;
																   incresText = findIsolation(incresText,genCoordsTextY);
	                                                                                                          firstTextY = 1;
	                                                                                                          latestTextY = incresText;
	                                                                                                          genCoordsTextY.push(incresText);
	                                                                                                          return "" + incresText;
	                                                                                                   } else {
	                                                                                                          latestTextY += 20;
	                                                                                                          genCoordsTextY.push(latestTextY);
	                                                                                                          return ""+ latestTextY;
	                                                                                                   }

	                                                                                            }

	                                                                                     }

	                                                                              })
	                                                                 .attr(
	                                                                              "x",
	                                                                              function(d) {
	                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
	                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
	                                                                                            return "-" + (r + 10);
	                                                                                     else
	                                                                                            return "" + (r + 10);
	                                                                              })
	                                                                 .attr("fill", function(d, i) {
	                                                                       return color(i, siteIndex);
	                                                                 })
	                                                                 .attr(
	                                                                              "text-anchor",
	                                                                              function(d) {
	                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
	                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
	                                                                                            return "end";
	                                                                                     else
	                                                                                            return "begning";
	                                                                              }).text(function(d) {
	                                                                       return d.value;
	                                                                 });
	 							 genCoordsTextX = [];
	                                                 genCoordsTextY = [];
	                                                 latestTextX = 0;
	                                                 firstTextX = 0;
	                                                 latestTextY = 0;
	                                                 firstTextY = 0;

	                                                   valueLabels[siteIndex]
	                                                                 .enter()
	                                                                 .append("svg:text")
	                                                                 .attr("class", "value")
	                                                                 .attr(
	                                                                              "y",
	                                                                              function(d) {
	                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
	                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {

	                                                                                            
	                                                                                            var flagDiffText = 0;
	                                                                                            for (var i = 0; i < genCoordsTextX.length; i++) {
	                                                                                            var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5- genCoordsTextX[i]);

	                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

	                                                                                                          flagDiffText = 1;
	                                                                                                          break;
	                                                                                                   } else {
	                                                                                                          continue;
	                                                                                                   }
	                                                                                            }
	                                                                                            if (flagDiffText == 0) {
															genCoordsTextX.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                                return ""+ (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                            } else {

	                                                                                                   if (firstTextX == 0) {
	                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
	                                                                                                          incresText += 25;
	                                                                                                          firstTextX = 1;
																   incresText=findIsolation(incresText,genCoordsTextX);
	                                                                                                          latestTextX = incresText;
	                                                                                                          genCoordsTextX.push(incresText);
	                                                                                                          return "" + incresText;
	                                                                                                   } else {
	                                                                                                          latestTextX += 20;
	                                                                                                          genCoordsTextX.push(latestTextX);
	                                                                                                          return "" + latestTextX;
	                                                                                                   }

	                                                                                            }

	                                                                                     } else {

	                                                                                            var flagDiffText = 0;
	                                                                                            for (var i = 0; i < genCoordsTextY.length; i++) {
	                                                                                                   var diffcoordsText = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y+5- genCoordsTextY[i]);

	                                                                                                   if (diffcoordsText<10 && diffcoordsText>0) {

	                                                                                                          flagDiffText = 1;
	                                                                                                          break;
	                                                                                                   } else {
	                                                                                                          continue;
	                                                                                                   }
	                                                                                            }
	                                                                                            if (flagDiffText == 0) {
															   genCoordsTextY.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                                   return ""+ (findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y + 5);
	                                                                                            } else {

	                                                                                                   if (firstText == 0) {
	                                                                                                          var incresText = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
	                                                                                                          incresText += 25;
	                                                                                                          firstTextY = 1;
																   incresText=findIsolation(incresText,genCoordsTextY);
	                                                                                                          latestTextY = incresText;
	                                                                                                          genCoordsTextY.push(incresText);
	                                                                                                          return "" + incresText;
	                                                                                                   } else {
	                                                                                                          latestTextY += 20;
	                                                                                                          genCoordsTextY.push(latestTextY);
	                                                                                                          return ""+ latestTextY;
	                                                                                                   }

	                                                                                            }

	                                                                                     }

	                                                                              })

	                                                                 .attr(
	                                                                              "x",
	                                                                              function(d) {
	                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
	                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
	                                                                                            return "-" + (r + 10);
	                                                                                     else
	                                                                                            return "" + (r + 10);
	                                                                              })
	                                                                 .attr("fill", function(d, i) {
	                                                                       return color(i, siteIndex);
	                                                                })
	                                                                 .attr(
	                                                                              "text-anchor",
	                                                                              function(d) {
	                                                                                     if ((d.startAngle + d.endAngle) / 2 > Math.PI
	                                                                                                   && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
	                                                                                            return "end";
	                                                                                     else
	                                                                                            return "begning";
	                                                                              }).text(function(d) {
	                                                                       return d.value;
	                                                                 });

	                                                   valueLabels[siteIndex].exit().remove();


	      //DRAW LABELS WITH ENTITY NAMES
	      nameLabels[siteIndex] = label_group[siteIndex].selectAll("text.units").data(filteredPieData)
	        .attr("dy", function(d) {
	          if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2 && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
	            return 17;
	          } else {
	            return 5;
	          }
	        })
	        .attr("text-anchor", function(d) {
	          if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
	            return "beginning";
	          } else {
	            return "end";
	          }
	        }).text(function(d) {
	          return d.name;
	        });

	      nameLabels[siteIndex].enter().append("svg:text")
	        .attr("class", "units")
	        .attr("transform", function(d) {
	          return "translate(" + Math.cos(((d.startAngle + d.endAngle - Math.PI) / 2)) * (r + textOffset) + "," + Math.sin((d.startAngle + d.endAngle - Math.PI) / 2) * (r + textOffset) + ")";
	        })
	        .attr("dy", function(d) {
	          if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2 && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
	            return 17;
	          } else {
	            return 5;
	          }
	        })
	        .attr("text-anchor", function(d) {
	          if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
	            return "beginning";
	          } else {
	            return "end";
	          }
	        }).text(function(d) {
	          return d.name;
	        });

	      nameLabels[siteIndex].transition().duration(tweenDuration).attrTween("transform", textTween);

	      nameLabels[siteIndex].exit().remove();
	    }
	  }
	  ///////////////////////////////////////////////////////////
	  //FUNCTIONS //////////////////////////////////////////////
	  ///////////////////////////////////////////////////////////

	//Interpolate the arcs in data space.
	function pieTween(d, i) {
	  var s0;
	  var e0;
	  if (oldPieData[i]) {
	    s0 = oldPieData[i].startAngle;
	    e0 = oldPieData[i].endAngle;
	  } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
	    s0 = oldPieData[i - 1].endAngle;
	    e0 = oldPieData[i - 1].endAngle;
	  } else if (!(oldPieData[i - 1]) && oldPieData.length > 0) {
	    s0 = oldPieData[oldPieData.length - 1].endAngle;
	    e0 = oldPieData[oldPieData.length - 1].endAngle;
	  } else {
	    s0 = 0;
	    e0 = 0;
	  }
	  var i = d3.interpolate({
	    startAngle: s0,
	    endAngle: e0
	  }, {
	    startAngle: d.startAngle,
	    endAngle: d.endAngle
	  });
	  return function(t) {
	    var b = i(t);
	    return arc(b);
	  };
	}

	function removePieTween(d, i) {
	  s0 = 2 * Math.PI;
	  e0 = 2 * Math.PI;
	  var i = d3.interpolate({
	    startAngle: d.startAngle,
	    endAngle: d.endAngle
	  }, {
	    startAngle: s0,
	    endAngle: e0
	  });
	  return function(t) {
	    var b = i(t);
	    return arc(b);
	  };
	}

	function textTween(d, i) {
	  var a;
	  if (oldPieData[i]) {
	    a = (oldPieData[i].startAngle + oldPieData[i].endAngle - Math.PI) / 2;
	  } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
	    a = (oldPieData[i - 1].startAngle + oldPieData[i - 1].endAngle - Math.PI) / 2;
	  } else if (!(oldPieData[i - 1]) && oldPieData.length > 0) {
	    a = (oldPieData[oldPieData.length - 1].startAngle + oldPieData[oldPieData.length - 1].endAngle - Math.PI) / 2;
	  } else {
	    a = 0;
	  }
	  var b = (d.startAngle + d.endAngle - Math.PI) / 2;

	  var fn = d3.interpolateNumber(a, b);
	  return function(t) {
	    var val = fn(t);
	    return "translate(" + Math.cos(val) * (r + textOffset) + "," + Math.sin(val) * (r + textOffset) + ")";
	  };
	}
	
}
</script>
<!--END SITE PAGE CONTAINER-->