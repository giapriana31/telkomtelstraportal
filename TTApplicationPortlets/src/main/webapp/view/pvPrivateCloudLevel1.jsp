<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ include file="common.jsp" %>
    
    
<portlet:resourceURL id="getDrillDownDetail" var="getDrillDownDetail">
<portlet:param name="action" value="getDrillDownDetail"/> 

</portlet:resourceURL>
    
    

<html>


<head>
<style>
.vblockHeaders{
       text-decoration: none !important;
       color: #fff !important;
}
</style>

<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js" type="text/javascript"></script>
<script src="<%= request.getContextPath() %>/js/jquery.form-validator.min.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<div class="container-fluid tt-page-detail">
       <div class="row-fluid tt-page-breadcrumb">
              <div class="span6">
                     <div>
                           <span class="tt-img-dashboard tt-main-page-name" style="margin-top: -13px;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
                           <span class="tt-current-page" style="margin-top: -13px;"><liferay-ui:message key="Private Cloud"/></span>
                     </div>
              </div>
              <div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.pv.homeURL")%>/<%=prop.getProperty("ttPVDashboardPageName")%>'" class="btn-primary-tt backButton" style="margin-top: -16px;">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
              </div>
       </div>
</div>
<div id="tabContainer"><div id="vBlockTab">V Block</div></div>
  <form id="searchForm"><div id="searchBoxContainer"><input type="text" name="searchBox" style="width:150px!important;height:26px!important;" id="searchText" class=""><button type="" class="search"  id="searchBtn"></button></div></form>
<!--START DISPLAY SITES CONTAINER-->
<div class="container-fluid tt-site-display">
       <div class="row-fluid">
              <div class="span12" style="margin-top: 10px;">
                     <div class="span2 tt-displaySite-title" style="font-size: 10px;"><liferay-ui:message key="Display vBLOCKS:"/></div>
                     <div class="span8">
                           <div class="row-fluid tt-displaySite-check" id="filterContainer">
                           <form id="filterForm">
                                  <div class="span2" id="filterAll"><input type="checkbox" name="All" id="All"  class="tt-displaysite-position" checked/><liferay-ui:message key="All"/></div>
                                  <div class="span3" id="filterOperational"><input type="checkbox" name="Operational" id="6"  class="tt-displaysite-position" /><liferay-ui:message key="Operational"/></div>
                                  <div class="span3" id="filterMonitoring"><input type="checkbox" name="Monitoring" id="5"  class="tt-displaysite-position" /><liferay-ui:message key="Monitoring"/></div>
                                  <div class="span3" id="filterDelivery"><input type="checkbox" name="DeliveryActivation" id="4"  class="tt-displaysite-position" /><liferay-ui:message key="Delivery & Activation"/></div>
                                  <div class="span3" id="filterProcurement"><input type="checkbox" name="Procurement" id="3"  class="tt-displaysite-position" /><liferay-ui:message key="Procurement"/></div>
                                  <div class="span3" id="filterDetailed"><input type="checkbox" name="Detailed Design" id="2"  class="tt-displaysite-position" /><liferay-ui:message key="Detailed Design"/></div>
                                  <div class="span3" id="filterOrder"><input type="checkbox" name="Order Received" id="1"  class="tt-displaysite-position" /><liferay-ui:message key="Order Received"/></div>
                                  </form>
                           </div>
                         
                     </div>
              </div>
       </div>
</div>


<div class="container-fluid tt-site-tab-container">
       <div class="row-fluid tt-site-tab-incident-content" id="site-incident">
       <div id="data-VM" style="background-color: white;">
       <c:set var="count" value="0"></c:set>
       <c:forEach items="${drillDownList}" var="details" >
       <c:set var="count" value="${count+1}"></c:set>
       
       
       <c:choose>
       <c:when test="${count==1 || (count-1)%4==0}">
       <div class="row-fluid" >
       <div class="span3"><div class="extra_style" style="background-color:${details.color}"><table width="100%"><tbody><tr><td align="center">${details.vBlockName}</td></tr></tbody></table></div></div>
       </c:when>
       <c:when test="${count%4==0}">
       
       <div class="span3"><div class="extra_style" style="background-color:${details.color}"><table width="100%"><tbody><tr><td align="center">${details.vBlockName}</td></tr></tbody></table></div></div>
       </div>
       </c:when>
       <c:otherwise>
                     <div class="span3"><div class="extra_style" style="background-color:${details.color}"><table width="100%"><tbody><tr><td align="center">${details.vBlockName}</td></tr></tbody></table></div></div>
       
       
       
       </c:otherwise>       
       
       </c:choose>

       
       
       </c:forEach>
       </div>
       

</div>
</div>

</body>


</html>

<script>

jQuery.base64=(function($){var _PADCHAR="=",_ALPHA="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",_VERSION="1.0";function _getbyte64(s,i){var idx=_ALPHA.indexOf(s.charAt(i));if(idx===-1){throw"Cannot decode base64"}return idx}function _decode(s){var pads=0,i,b10,imax=s.length,x=[];s=String(s);if(imax===0){return s}if(imax%4!==0){throw"Cannot decode base64"}if(s.charAt(imax-1)===_PADCHAR){pads=1;if(s.charAt(imax-2)===_PADCHAR){pads=2}imax-=4}for(i=0;i<imax;i+=4){b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6)|_getbyte64(s,i+3);x.push(String.fromCharCode(b10>>16,(b10>>8)&255,b10&255))}switch(pads){case 1:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12)|(_getbyte64(s,i+2)<<6);x.push(String.fromCharCode(b10>>16,(b10>>8)&255));break;case 2:b10=(_getbyte64(s,i)<<18)|(_getbyte64(s,i+1)<<12);x.push(String.fromCharCode(b10>>16));break}return x.join("")}function _getbyte(s,i){var x=s.charCodeAt(i);if(x>255){throw"INVALID_CHARACTER_ERR: DOM Exception 5"}return x}function _encode(s){if(arguments.length!==1){throw"SyntaxError: exactly one argument required"}s=String(s);var i,b10,x=[],imax=s.length-s.length%3;if(s.length===0){return s}for(i=0;i<imax;i+=3){b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8)|_getbyte(s,i+2);x.push(_ALPHA.charAt(b10>>18));x.push(_ALPHA.charAt((b10>>12)&63));x.push(_ALPHA.charAt((b10>>6)&63));x.push(_ALPHA.charAt(b10&63))}switch(s.length-imax){case 1:b10=_getbyte(s,i)<<16;x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_PADCHAR+_PADCHAR);break;case 2:b10=(_getbyte(s,i)<<16)|(_getbyte(s,i+1)<<8);x.push(_ALPHA.charAt(b10>>18)+_ALPHA.charAt((b10>>12)&63)+_ALPHA.charAt((b10>>6)&63)+_PADCHAR);break}return x.join("")}return{decode:_decode,encode:_encode,VERSION:_VERSION}}(jQuery));



$(document).ready(function(){
       

var searchTerm=null;
getDrillDowndata();

              $("#searchBtn").click(function(e){
              e.preventDefault();
              searchTerm=$("#searchText").val();
                                  getDrillDowndata();

       
              });

              $("input[type='checkbox']").change(function() {
              
                     getDrillDowndata();

              });


function getDrillDowndata(){

       
       
       var filters="";
       $('#filterForm input:checked').each(function() {
              
              filters=filters+$(this).attr('name')+"-"; 
              
              
       });
       var getDrillDownDetails="${getDrillDownDetail}&filters="+filters+"&searchTerm="+searchTerm;
       $.ajax({
              type : "POST",
              url : getDrillDownDetails,
              dataType : 'json',

              success : function(data) {
		
                     var count=0;
					 
					 
              $("#data-VM").empty();
              
              var htmlString="";
			 for (var i=0;i<data.length; i++){
			  var tempData=data[i];
			  	//		 alert(tempData);

              for (var key in tempData) {
			
              count++;
         if (tempData.hasOwnProperty(key)) {
              if(count%4==0){
              htmlString=htmlString+'<div class="span3"><div class="extra_style" style="background-color:'+tempData[key]+'"><table width="100%"><tbody><tr><td align="center"><a class="vblockHeaders" href="/group/project-view/project_view?'+$.base64.encode(key.split("***")[1]+"&"+key.split("***")[0])+'">'+key.split("***")[0]+'</a></td></tr></tbody></table></div></div>';       
              htmlString=htmlString+'</div>';   
                           
              }
                     else if(count==1 || (count-1)%4==0){

                                  htmlString=htmlString+'<div class="row-fluid" >';
                            htmlString=htmlString+'<div class="span3"><div class="extra_style" style="background-color:'+tempData[key]+'"><table width="100%"><tbody><tr><td align="center"><a class="vblockHeaders" href="/group/project-view/project_view?'+$.base64.encode(key.split("***")[1]+"&"+key.split("***")[0])+'">'+key.split("***")[0]+'</a></td></tr></tbody></table></div></div>';       

              
              } 
              else{

                                         htmlString=htmlString+'<div class="span3"><div class="extra_style" style="background-color:'+tempData[key]+'"><table width="100%"><tbody><tr><td align="center"><a class="vblockHeaders" href="/group/project-view/project_view?'+$.base64.encode(key.split("***")[1]+"&"+key.split("***")[0])+'">'+key.split("***")[0]+'</a></td></tr></tbody></table></div></div>';       
                    
              }
         }
       }
	   }
       $("#data-VM").append(htmlString);
                     

       
                     
              },
              error : function(data) {
              }

       });

       
       
}






              });

</script>

