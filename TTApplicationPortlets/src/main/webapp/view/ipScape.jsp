<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />


<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/PhoneFormat.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js" type="text/javascript"></script>
<script src="<%= request.getContextPath()%>/js/jquery.dataTables.min.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.dataTables.css"/>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ipscape.css">
<link href="${pageContext.request.contextPath}/css/sites.css" rel="stylesheet" />

<portlet:resourceURL id="getCampaignList" var="getCampaignList" />
<portlet:resourceURL id="submitResourceURLIPScape" var="submitResourceURLIPScape" />
<portlet:resourceURL id="submitEditLeadIPScapeURL" var="submitEditLeadIPScapeURL" />
<portlet:resourceURL id="incompleteCallbackDetailsURL" var="incompleteCallbackDetailsURL" />
<portlet:resourceURL id="completeCallbackDetailsURL" var="completeCallbackDetailsURL" />
<portlet:resourceURL id="deleteCallbackURL" var="deleteCallbackURL" />

<style>
.100pxSquare
{
  width: 100px;
  height: 100px;
}
#submitRequestEdit{
       background-image: url(<%=request.getContextPath()%>/images/SubmitRequest_W_16.png);
    background-repeat: no-repeat;
    background-position: 10px 10px;
    padding: 10px 10px 10px 41px;
    border: 0;
    color: #ffffff;
}
iframe{
z-index: 999  !important;
}
.img-create-callback {
       background: url(<%=request.getContextPath()%>/images/PostNewThread_W_16.png) no-repeat 10px 10px
              #E32212;
       box-shadow: 2px 2px 2px #B52217;
       background-color: #E32212;
       padding: 10px 10px 10px 35px;
       border: 0;
       color: #ffffff;
       float: right;
}
.img-create-callback-disabled {
       background: url(<%=request.getContextPath()%>/images/PostNewThread_W_16.png) no-repeat 10px 10px #777;
       box-shadow: 2px 2px 2px #777;
       background-color: #777;
       padding: 10px 10px 10px 35px;
       border: 0;
       color: #ffffff;
       float: right;
}
</style>
<script type="text/javascript">
function activeCallbackTable() 
{      
 $('#ipscapeCompleteRequest-li').removeClass('selected');
 $('#ipscapeActiveRequest-li').addClass('selected');
 callbackDetailsIncompleteTableAjax();
 document.getElementById("completeCallbackDetailsDiv").style.display = 'none';
 document.getElementById("inCompleteCallbackDetailsDiv").style.display = 'block';
}
function completeCallbackTable() 
{      
 $('#ipscapeActiveRequest-li').removeClass('selected');
 $('#ipscapeCompleteRequest-li').addClass('selected');
 callbackDetailsCompleteTableAjax();
 document.getElementById("inCompleteCallbackDetailsDiv").style.display = 'none';
 document.getElementById("completeCallbackDetailsDiv").style.display = 'block';
}
var radioSelected;
function openIpscapePage(){
  $("#IPScapeId").modal('show');
  document.getElementById("intersupportScreenBahasa").style.display = "none";
  $("#ipScapeForm label").css("display","block");
  $("#ipScapeForm label").addClass("error");
  resetIpscapeForm();  
}
function processPhone() {
  phone = $('#telephone').val();
  var country = 'Indonesia';
  if (isValidNumber(phone, country)) {
  //alert("True");
         return true;
  } else {
  //alert("False");
         return false;
  }
}
function processEditPhone() {
	 phone = $('#phoneEdit').val();
	 
	 var country = 'Indonesia';
	 if (isValidNumber(phone, country)) {
	 //alert("True");
	        return true;
	 } else {
	 //alert("False");
	        return false;
	 }
}

function currentdatetimecheck(){

  var tempAMPMValue = document.querySelector('input[name = "ampmradioEdit"]:checked').value;
  var stringtempAMPMValue = String(tempAMPMValue);
  //alert(tempAMPMValue+"::tempAMPMValue");
  var e = document.getElementById("rangeTimeEdit");
  var tempRangeTimeEditValue = e.options[e.selectedIndex].text; 
  var tempe = tempRangeTimeEditValue.split(":");
  var hh = tempe[0];
  var mm = tempe[1];
  //alert(hh+":hh"+mm+"::mm");
 if(hh.length == 1){
		//alert("hh.length == 1");
		hh = "0"+hh;
		//alert("append 0"+hh);
	}
  if(mm.length == 1){
		 //alert("mm.length == 1");
		 mm = "0"+mm;
		 //alert("append 0"+mm);
  }
  if(stringtempAMPMValue == "pm" && hh!="12" && hh!="00"){
	//alert("stringtempAMPMValue == pm && hh!=12 & hh!=00");
	hh = parseInt(hh) + 12 ;
	//alert("parseInt plus 12 hh"+hh);
	hh = String(hh);
	//alert("string again hh"+hh);
	}
  if(stringtempAMPMValue == "am" && hh=="12"){
  //alert("stringtempAMPMValue == am && hh==12");
	hh = "00";
	}
	//alert(hh+":hh"+mm+"::mm after conversion");
	time = hh + ":" + mm;
	//alert(time+":::24 hour formate time");
  
  var dateInForm = document.getElementById("callbackDateEdit").value;
  var dateInFormString = String(dateInForm);
  var temp = dateInFormString.split("/");
  var dd = temp[0];
  var mm = temp[1];
  var yy = temp[2];
  //alert(dd+mm+yy+":::::ddmmyy");
  formattedDateInFormString = mm + "/" + dd + "/" +yy+" "+time; 
  //alert(formattedDateInFormString+"::::::::::formattedDateInFormString");
  var dateInFormDate = new Date(formattedDateInFormString);
  //alert (dateInFormDate+":::dateInFormDate");
  var currDate = new Date();
  //alert(currDate + "  :: current date");
  
  if(dateInFormDate<=currDate){
	//alert("don't submit");
	return false;
  }
  else{
  //alert("submit");
  return true;
  }
  
 }


function showIPScapeForm(){
  $("#IPScapeId").modal('show');
}
function resetIpscapeForm(){
  //var validator = $( "#ipScapeForm" ).validate();
  //validator.resetForm();
  $('#ipScapeForm').trigger("reset");
  $.fn.clearValidation = function(){var v = $(this).validate();$('[name]',this).each(function(){v.successList.push(this);v.showErrors();});v.resetForm();v.reset();};
  $("#ipScapeForm").clearValidation();
  //$("#ipScapeForm label").text("");
  //$("#ipScapeForm label").css("display","block");
  $('#pmradio').attr('disabled','disabled');
  $('#amradio').attr('disabled','disabled');
  $('#rangeTime').attr('disabled','disabled');       
}
function resetIpscapeEditForm(){
 $('#ipscapeEditForm').trigger("reset");
 $.fn.clearValidation = function(){var v = $(this).validate();$('[name]',this).each(function(){v.successList.push(this);v.showErrors();});v.resetForm();v.reset();};
 $("#ipscapeEditForm").clearValidation();
    
     $('#pmradioEdit').attr('disabled','disabled');
 $('#amradioEdit').attr('disabled','disabled');
 $('#rangeTimeEdit').attr('disabled','disabled');
}
function resetMessages()
{
 $("#customerNameId").hide();
 $("#telephoneId").hide();
 $("#callbackDateId").hide();
 $("#specificTimeId").hide();
 $("#timeradioId").hide();
 $("#categoryId").hide();
 $("#rangeTimeId").hide();
}
function enableAMPMEdit(){
   var datepicker = $('#callbackDateEdit').val();
   var stringDatePicker = String(datepicker);
   var date = String($('#dateAsiaJakarta').val());
   var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
   var year = String($('#yearAsiaJakarta').val());
   if(date.length == 1){
          date = "0"+date;
   }
   if(month.length == 1){
          month = "0"+month;
   }      
   var stringFormattedDate =  date + "/" + month + "/" + year;
   //alert("current date::"+stringFormattedDate);
   $('#rangeTimeEdit').attr('disabled','disabled');
   $('#pmradioEdit').removeAttr('disabled','disabled');
   $('#amradioEdit').removeAttr('disabled','disabled');
   $("#rangeTimeEdit").empty();
   $('input[name="ampmradioEdit"]').attr('checked', false);
   $('input[name="ampmradioEdit"]').attr('checked', false);
   var asiaHour = $('#hoursAsiaJakarta').val();
   var asiaMin = $('#minutesAsiaJakarta').val();
   //alert(stringDatePicker+"--"+stringFormattedDate);
   if(((asiaHour >= 11 && asiaMin >=30) || asiaHour >= 12) && stringDatePicker == stringFormattedDate){
                 $('#amradioEdit').attr('disabled','disabled');
                 //alert("asdf");
   }
   if(asiaHour>=23 && asiaMin >=30 && stringDatePicker == stringFormattedDate){
                 $('#pmradioEdit').attr('disabled','disabled');
                 //alert("asdf");
   }
}
function populateTimeDropDownEdit(){
   $('#rangeTimeEdit').removeAttr('disabled','disabled');
   $("#rangeTimeEdit").empty();
   var datepicker = $('#callbackDateEdit').val();
   var stringDatePicker = String(datepicker);
   var date = String($('#dateAsiaJakarta').val());
   var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
   var year = String($('#yearAsiaJakarta').val());
   //alert("check::"+date.length);
   if(date.length == 1){
          date = "0"+date;
   }
   if(month.length == 1){
          month = "0"+month;
   }      
   var stringFormattedDate = date + "/" + month + "/" + year;
   //alert("formatted date::"+stringFormattedDate);
   //alert("date picker::"+stringDatePicker);
   var asiaHour = $('#hoursAsiaJakarta').val();
   var asiaMin = $('#minutesAsiaJakarta').val();
   var ampmValue = document.querySelector('input[name = "ampmradioEdit"]:checked').value;
   //alert("ampm value:"+ampmValue);
   //alert(asiaHour+":"+asiaMin);
   var hour = 0;
   var min = 0;
   if(stringDatePicker == stringFormattedDate){
     //alert("Same date is selected");
     if(asiaHour<12 && ampmValue=="pm"){
        for(var i=0 ; i<24; i++){
          var stringHour = String(hour);
          var stringMin = String(min);
          if(stringHour.length == 1){
                 stringHour = "0"+stringHour;
          }
          if(stringMin.length == 1){
                 stringMin = "0"+stringMin;
          }
          if(stringHour == "00"){
                 stringHour = "12";
          }
          time = stringHour + ":" + stringMin;
          //alert("Time asdf::"+time);
          $("select[name='rangeTimeEdit']").append('<option value="'+time+'">'+time+'</option>');
          if(min == 0){
                 min = 30;
          }else if(min == 30){
                 min = 00;
                 hour = hour + 1;
          }
        }
     }
     else{
       if(parseInt(asiaMin) >= 30){
             //alert("hours:::"+hour);
             hour = parseInt(asiaHour) + 1;
             //alert("hours after half an hour(+1):::"+hour);
             if(hour >= 12){
                    hour = hour - 12;
                    //alert("hours after minus:::"+hour);
             }
       }else{
             hour = parseInt(asiaHour);
             //alert("hours:::"+hour);
             if(hour >= 12){
                    //alert("hours:::"+hour);
                    hour = hour - 12;
                    //alert("hours:::"+hour);
             }
             min = 30
       }
       while(hour < 12){
             var stringHour = String(hour);
             var stringMin = String(min);
             if(stringHour.length == 1){
                    stringHour = "0"+stringHour;
             }
             if(stringMin.length == 1){
                    stringMin = "0"+stringMin;
             }
             if(stringHour == "00"){
                    stringHour = "12";
             }
             time = stringHour + ":" + stringMin;
             //alert("Time asdf::"+time);
             $("select[name='rangeTimeEdit']").append('<option value="'+time+'">'+time+'</option>');
             if(min == 0){
                    min = 30;
             }else if(min == 30){
                    min = 00;
                    hour = hour + 1;
             }
       }
     }
   }
   else{ 
	    for(var i=0 ; i<24; i++){
	         var stringHour = String(hour);
	         var stringMin = String(min);
	         if(stringHour.length == 1){
	                stringHour = "0"+stringHour;
	         }
	         if(stringMin.length == 1){
	                stringMin = "0"+stringMin;
	         }
	         if(stringHour == "00"){
	                stringHour = "12";
	         }
	         time = stringHour + ":" + stringMin;
	         //alert("Time asdf::"+time);
	         $("select[name='rangeTimeEdit']").append('<option value="'+time+'">'+time+'</option>');
	         if(min == 0){
	                min = 30;
	         }else if(min == 30){
	                min = 00;
	                hour = hour + 1;
	         }
	    }
   }
}
function enableAMPM(){
    var datepicker = $('#callbackDate').val();
    var stringDatePicker = String(datepicker);
    var date = String($('#dateAsiaJakarta').val());
    var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
    var year = String($('#yearAsiaJakarta').val());
    if(date.length == 1){
           date = "0"+date;
    }
    if(month.length == 1){
           month = "0"+month;
    }      
    var stringFormattedDate =  date + "/" + month + "/" + year;
    
    $('#rangeTime').attr('disabled','disabled');
    $('#pmradio').removeAttr('disabled','disabled');
    $('#amradio').removeAttr('disabled','disabled');
    $("#rangeTime").empty();
    $('input[name="ampmradio"]').attr('checked', false);
    $('input[name="ampmradio"]').attr('checked', false);
    var asiaHour = $('#hoursAsiaJakarta').val();
    var asiaMin = $('#minutesAsiaJakarta').val();
    
    if(((asiaHour >= 11 && asiaMin >=30) || asiaHour >= 12) && stringDatePicker == stringFormattedDate){
                  $('#amradio').attr('disabled','disabled');
    }
    if(asiaHour>=23 && asiaMin >=30 && stringDatePicker == stringFormattedDate){
                  $('#pmradio').attr('disabled','disabled');
    }       
}
function populateTimeDropDown(){
   $('#rangeTime').removeAttr('disabled','disabled');
   $("#rangeTime").empty();
   var datepicker = $('#callbackDate').val();
   var stringDatePicker = String(datepicker);
   var date = String($('#dateAsiaJakarta').val());
   var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
   var year = String($('#yearAsiaJakarta').val());
   //alert("check::"+date.length);
   if(date.length == 1){
          date = "0"+date;
   }
   if(month.length == 1){
          month = "0"+month;
   }      
   var stringFormattedDate = date + "/" + month + "/" + year;
   //alert("formatted date::"+stringFormattedDate);
   //alert("date picker::"+stringDatePicker);
   var asiaHour = $('#hoursAsiaJakarta').val();
   var asiaMin = $('#minutesAsiaJakarta').val();
   var ampmValue = document.querySelector('input[name = "ampmradio"]:checked').value;
   //alert("ampm value:"+ampmValue);
   //alert(asiaHour+":"+asiaMin);
   var hour = 0;
   var min = 0;
   if(stringDatePicker == stringFormattedDate){
      //alert("Same date is selected");
      if(asiaHour<12 && ampmValue=="pm"){
        for(var i=0 ; i<24; i++){
              var stringHour = String(hour);
              var stringMin = String(min);
              if(stringHour.length == 1){
                     stringHour = "0"+stringHour;
              }
              if(stringMin.length == 1){
                     stringMin = "0"+stringMin;
              }
              if(stringHour == "00"){
                     stringHour = "12";
              }
              time = stringHour + ":" + stringMin;
              //alert("Time asdf::"+time);
              $("select[name='rangeTime']").append('<option value="'+time+'">'+time+'</option>');
              if(min == 0){
                     min = 30;
              }else if(min == 30){
                     min = 00;
                     hour = hour + 1;
              }
		 }
      }
      else{
	        if(parseInt(asiaMin) >= 30){
	             //alert("hours:::"+hour);
	             hour = parseInt(asiaHour) + 1;
	             //alert("hours after half an hour(+1):::"+hour);
	             if(hour >= 12){
	                    hour = hour - 12;
	                    //alert("hours after minus:::"+hour);
	             }
	        }
	        else{
	             hour = parseInt(asiaHour);
	             //alert("hours:::"+hour);
	             if(hour >= 12){
	                    //alert("hours:::"+hour);
	                    hour = hour - 12;
	                    //alert("hours:::"+hour);
	             }
	             min = 30
	        }
            while(hour < 12){
              var stringHour = String(hour);
              var stringMin = String(min);
              if(stringHour.length == 1){
                     stringHour = "0"+stringHour;
              }
              if(stringMin.length == 1){
                     stringMin = "0"+stringMin;
              }
              if(stringHour == "00"){
                     stringHour = "12";
              }
              time = stringHour + ":" + stringMin;
              //alert("Time asdf::"+time);
              $("select[name='rangeTime']").append('<option value="'+time+'">'+time+'</option>');
              if(min == 0){
                     min = 30;
              }else if(min == 30){
                     min = 00;
                     hour = hour + 1;
              }
            }
          }
   }
   else{ 
	   for(var i=0 ; i<24; i++){
         var stringHour = String(hour);
         var stringMin = String(min);
         if(stringHour.length == 1){
                stringHour = "0"+stringHour;
         }
         if(stringMin.length == 1){
                stringMin = "0"+stringMin;
         }
         if(stringHour == "00"){
                stringHour = "12";
         }
         time = stringHour + ":" + stringMin;
         //alert("Time asdf::"+time);
         $("select[name='rangeTime']").append('<option value="'+time+'">'+time+'</option>');
         if(min == 0){
                min = 30;
         }else if(min == 30){
                min = 00;
                hour = hour + 1;
         }
	   }
   }
}
var page = 1;
var campaignList = new Array();
var totalCampaigns = 0;

/* $(document).click(function(event) { 
    if(!$(event.target).closest('.nameText').length) {
        $(".hoverTextDivs").removeClass("displayHoverText");
    }        
}) */
$(document).ready(function () 
{      

   
	$(".hoverTextDivs").removeClass("displayHoverText");
            
	$('#createCallbckId').removeAttr('disabled');
	$('#createCallbckId').removeAttr("title");
    $('#createCallbckId').removeClass("img-create-callback-disabled");
    $('#createCallbckId').addClass("img-create-callback");
      
    $("#editCallbackModal").hide();
	$("#IPScapeId").hide();
	$("#IpscapeSubmitResponse").hide();
  	$("#confirmationBoxId").hide();
   	$("#deleteCallbackResponse").modal('hide'); 
	$("#deleteCallbackConfirm").modal('hide'); 
	$("#editLeadResponseDiv").modal('hide');
   
    $('#pmradio').attr('disabled','disabled');
    $('#amradio').attr('disabled','disabled');
    $('#rangeTime').attr('disabled','disabled');
    
    activeCallbackTable();
    getCampaignListAjax(page);
    
    function getCampaignListAjax(page){     
         var getCampaignListURL = "${getCampaignList}" + "&pageNo=" + page + "&perPage=100";
         $.ajax({
           type: "POST",
           url: getCampaignListURL,
           dataType: "json",
           success : function(data) 
           {     
    	   	 campaignList.push(data);
             getFinalList(data);
           },
           error : function()
           {
           }
         });
       }
     function getFinalList(jsonData){             
        totalCampaigns= totalCampaigns + jsonData.result.inPage;             
        var total = jsonData.result.total;
        if(total >= totalCampaigns){
           if(totalCampaigns == total){
                 totalCampaigns++; 
           }
           getCampaignListAjax(page = page + 1);
        }
        else{
            for(var i = 0;i < campaignList.length; i++){
	            var Json=campaignList[i];
	            var dataArray = Json.result.data;
	            for(var j=0 ; j<dataArray.length; j++){
	                if(dataArray[j].campaignType == "Outbound"){
	                $('#category').append($('<option>').text(dataArray[j].campaignTitle).val(dataArray[j].campaignId));
	            	}
	    		 }
   			}
   		}
  	   }
       var date = String($('#dateAsiaJakarta').val());
       var month = String(parseInt($('#monthsAsiaJakarta').val())+1);
       var year = String($('#yearAsiaJakarta').val());
       var todayDate = new Date(year + '/' + month + '/' + date);
       todayDate.setDate(todayDate.getDate() + 30);
       $(".selectDate").datepicker({minDate: 0, maxDate: new Date(todayDate.getFullYear(),todayDate.getMonth(),todayDate.getDate()), dateFormat: 'dd/mm/yy'});
	   var submitResourceURLIPScape = "${submitResourceURLIPScape}";
       jQuery.validator.addMethod('timeformat',function(value,element,regexp){
              var re = new RegExp(regexp);
              return this.optional( element ) || re.test(value);
       }, "<liferay-ui:message key='Please insert time in proper format (HH:MM)'/>");    
       /* jQuery.validator.addMethod("namecheck", function(value, element,regexp) {
              var re = new RegExp(regexp);
           return this.optional(element) || re.test(value);
       },"<liferay-ui:message key='Please insert characters only'/>"); */
       jQuery.validator.addMethod("phonecheck", function(value, element,regexp) {
              var re = processPhone();
           return processPhone();
       },"<liferay-ui:message key='Invalid phone number'/>");
       $("#okbuttonid").click(function(){
           $("#confirmationBoxId").modal('hide');
           var dataFormIpscape = new FormData($('#ipScapeForm')[0]);
           $
           .ajax({
                         url : submitResourceURLIPScape,
                         type : "POST",
                         data : dataFormIpscape,
                         success : function(response) {
                                
                                var ipscapeResponse = JSON
                                       .parse(response);
                               
                                var status = ipscapeResponse.ipscape;
                                if (status == 'success') {
                                       document.getElementById("responseStatusIPScape").innerHTML = '<liferay-ui:message key="Your Callback request has been successfully submitted."/>';
                                }else{
                                       document
                                       .getElementById("responseStatusIPScape").innerHTML = '<liferay-ui:message key="Callback request failed.Please try again."/>';
                                }
                                $("#confirmationBoxId").modal('hide');
                                $("#IpscapeSubmitResponse").modal('show');
                         },
                         error : function(
                                       xhr) {
                                
                         },
                         processData: false,
                         contentType: false
                  });
		 });               
       $("#ipScapeForm").validate({
          onsubmit: true,
          rules:{
                 callbackDate:{
                       required:true                                   
                 },
                 firstName:{
                       required:true,
                       //namecheck:"^[[a-zA-Z][a-zA-Z ]+$"
                 },
                 lastName:{
                       required:true,
                       //namecheck:"^[[a-zA-Z][a-zA-Z ]+$"
                 },
                 telephone:{
                       required:true,
                       phonecheck:""                     
                 },
                 ampmradio:{
                       required:true
                 },
                 category: {
                       required: {
                              depends: function(element) {
                                     return $("#category").val() == '';
                              }
                       }
                 },
                 rangeTime: {
                       required: {
                              depends: function(element) {
                                     return $("#rangeTime").val() == '';
                              }
                       }
                 }
          },
                    errorClass:"validationError",
          messages:{
                 callbackDate : {
                       required:"<liferay-ui:message key='Please enter Callback date' />"
                        },
                   firstName:{
                        required:"<liferay-ui:message key='Please enter First Name' />",
                        //namecheck:"<liferay-ui:message key='Please insert characters only' />"
                 },
            lastName:{
                        required:"<liferay-ui:message key='Please enter Last Name' />",
                        //namecheck:"<liferay-ui:message key='Please insert characters only' />"
                 },
                 telephone:{
                        required:"<liferay-ui:message key='Please enter Phone Number' />",
                        countrycodecheck:"<liferay-ui:message key='Invalid phone number' />"
             },
             ampmradio:{
                        required:"<liferay-ui:message key='Please select either am or pm' />"
             },
             category:{
                       required:"<liferay-ui:message key='Please select a Category' />" ,
                 },
                 rangeTime:{
                       required:"<liferay-ui:message key='Please select an option from Range Time' />" 
                 }
             
          },
          submitHandler : function(form) {
                 
                 phone = $('#telephone').val();
                 date =        $('#callbackDate').val();
                          
                 rangeTime = document.getElementById("rangeTime").value; 
                               
                 var ampmValue = document.querySelector('input[name = "ampmradio"]:checked').value;                   
                 document.getElementById("confirmationString").innerHTML = '<liferay-ui:message key="Please verify the following Callback details:"/>';
                 document.getElementById("phoneVerify").innerHTML = '<liferay-ui:message key="Phone Number: "/>'+phone;
                 document.getElementById("dateVerify").innerHTML = '<liferay-ui:message key="Date : "/>'+date;
                                     
                 if(ampmValue == "am"){
                       document.getElementById("timeVerify").innerHTML = '<liferay-ui:message key="Time : "/>'+rangeTime+" AM JKT(GMT+7)";
                 }else{
                       document.getElementById("timeVerify").innerHTML = '<liferay-ui:message key="Time : "/>'+rangeTime+" PM JKT(GMT+7)";
                 }
                              
                 $("#IPScapeId").modal('hide');
                 $("#confirmationBoxId").modal('show');          
          }
       });
       jQuery.validator.addMethod("editphonecheck", function(value, element,regexp) {
        var re = processEditPhone();
        return processEditPhone();
       },"<liferay-ui:message key='Invalid phone number'/>");
	   
	    jQuery.validator.addMethod("currdatetimecheck", function(value, element,param) {
        var resp = currentdatetimecheck();
		//alert("respnse rec"+resp);
        return resp;
		
       },"<liferay-ui:message key='Callback date expired, <br>  Please update to <br> future date.' />");
	   
          $("#ipscapeEditForm").validate({
              onsubmit: true,
              rules:{
                    phoneEdit:{
					   required:true,
					   editphonecheck:""                     
                    },
					callbackDateEdit:{
						required:true, 
						currdatetimecheck : ""
					},
					ampmradioEdit:{
                        required:true
					},
					rangeTimeEdit: {
						required: {
							  depends: function(element) {
										  return $("#rangeTimeEdit").val() == '';
							  }
						 }
					},
              },
              messages:{
                
                           phoneEdit:{
                                        required:"<liferay-ui:message key='Please enter Phone Number' />",
										editphonecheck:"<liferay-ui:message key='Invalid phone number' />"
							},
                           callbackDateEdit:{
										required:"<liferay-ui:message key='Please enter Callback date' />"
							},
                           ampmradioEdit:{
										required:"<liferay-ui:message key='Please select either am or pm' />"
							},
                           rangeTimeEdit:{
										required:"<liferay-ui:message key='Please select an option from Range Time' />"
										
						}
                           },
                           submitHandler : function(form) {
                                  var tempAMPMValue = document.querySelector('input[name = "ampmradioEdit"]:checked').value;
								  var stringtempAMPMValue = String(tempAMPMValue);
								  //alert(tempAMPMValue+"::tempAMPMValue");
                                  var e = document.getElementById("rangeTimeEdit");
								  var tempRangeTimeEditValue = e.options[e.selectedIndex].text; 
                                  var submitEditLeadIPScapeURL = "${submitEditLeadIPScapeURL}"+"&ampmradioEdit="+tempAMPMValue+"&rangeTimeEdit="+tempRangeTimeEditValue;
                                  var dataFormEditIpscape = new FormData($('#ipscapeEditForm')[0]);
                                                                 
                              $
                              .ajax({
                                                       url : submitEditLeadIPScapeURL,
                                                       type : "POST",
                                                       data : dataFormEditIpscape,
                                                       success : function(response) {
                                                                 //alert("Control in jsp");
                                                                 var updateLeadResponse = JSON
                                       .parse(response);
                               
                                              var updateStatus = updateLeadResponse.update;
                                              if (updateStatus == 'success') {
                                                
                                                $("#editCallbackModal").modal('hide'); 
                                                     document.getElementById("editLeadResponse").innerHTML = '<liferay-ui:message key="Callback updated successfully."/>';
                                                                              $("#editLeadResponseDiv").modal('show'); 
                                                                              callbackDetailsIncompleteTableAjax();
                                                     //alert("Success update");
                                              }else{
                                                $("#editCallbackModal").modal('hide');
                                                 document.getElementById("editLeadResponse").innerHTML = '<liferay-ui:message key="Callback update failed. Please try again."/>';
                                                                             $("#editLeadResponseDiv").modal('show');   
                                                //alert("Fail update");
                                              }
                                                                 
                                                        },
                                                       error : function(
                                                                              xhr) {
                                                                     
                                                       },
                                                       processData: false,
                                                       contentType: false
                                           });
                                                
                                         }
              });
              
              jQuery("#IpscapeButton").click(function()
                         {
                            
                          jQuery(this).next(".collapse3").slideToggle(15);
                          $("#IpscapeIcon").toggleClass("IpscapeMore IpscapeLess");
                          $("#IpscapeButton")
                             .toggleClass("ssry");
                          jQuery(".collapse1").hide();
                          jQuery(".collapse2").hide();
                          
                          $(".lsry")
                             .removeClass("lsry").add("lbry");
                          $(".sry")
                             .removeClass("sry").add("bry");
                          });              
});
function showFloatingButton(){
  document.getElementById("intersupportScreenBahasa").style.display = "block";
}
var jsonLeadData;
              function callbackDetailsIncompleteTableAjax()
              {      
                     //alert("Function Called..");
                     var callbackDetailsURL = "${incompleteCallbackDetailsURL}";
                     //alert("URL:::"+callbackDetailsURL)
                     $.ajax({
                           url: callbackDetailsURL,
                           type: "POST",
                           dataType: 'json',
                           success : function(data)
                           {      //alert("Data is:"+data);
                                  jsonLeadData = data;
                                  table = $('#inCompleteCallbackDetailsTable').DataTable({
                               	  		 "aaData" : data,
                               	  		 "destroy": true,
                                         "bFilter" : false,
                                         "processing" : true,
                                         "bPaginate" : true,
                                         "pageLength": 10,
                                         "serverSide" : false,      
                                         "bLengthChange" : false,
                                         "bSortable" : false,
                                         "bInfo": false,
										 "columnDefs": [
							                {
                                                "defaultContent": "-",
                                                "targets": 7,
                                                "render": function (data, type, full, meta){
                                                       var isAdmin = document.getElementById("userAdminRole").value;
                                                       var isNOCManager = document.getElementById("isNOCManager").value;
                                                       var isExternal = document.getElementById("isExternalUser").value;
                                                       var loggedInUserEmailId = document.getElementById("emailId").value;
                                                       var isTTlabs = document.getElementById("isTTlabs").value;
                                                       var columnValue = full[7].split("@@@");
                                                       var deleteLeadId = columnValue[0];
                                                       var emailIdFromLead = columnValue[1];
                                                       if(isAdmin == "true"){//CustomerPortalAdmin OR EndCustomerAdmin
                                                    	   if(isExternal == "true"){//EndCustomerAdmin -- External Admin
                                                    		   // FIF VEC
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		   return '<a name="id[]" value="' 
                                                               + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                    	   }
                                                    	   else{//CustomerPortalAdmin --  Internal Admin
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		   return '<a name="id[]" value="' 
                                                               + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                    	   }
                                                       }
                                                       else if (isNOCManager == "true") {//NOCManager -- Internal
                                                    	   
                                                    	// See ALL
														// Edit Self created for ALl
														//Edit TT for all
														
																	if(isTTlabs ==  "true"){
																		 $('#createCallbckId').removeAttr('disabled');
	                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
	                                                                      $('#createCallbckId').addClass("img-create-callback");
                                                                        return '<a name="id[]" value="' 
                                                                        + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
																	}
																	else if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		 $('#createCallbckId').removeAttr('disabled');
		                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
		                                                                      $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                             + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }else{
                                                                    	 	 $('#createCallbckId').removeAttr('disabled');
                                                                    	 	$('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                    	 	$('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="<liferay-ui:message key='Cannot cancel callback of other users.'/>"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }
														
													   }
                                                       else if (isExternal == "true") {//Other External user
														// See All
														//loggedinuser
																	if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
	                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
	                                                                      $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                             + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                    	  $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="<liferay-ui:message key='Cannot cancel callback of other users.'/>"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }
														
														}
                                                       else if (isTTlabs ==  "true" && isExternal != "true"){//Other Internal and TTLabs
                                                   	   //Create and edit self
                                                   	   			if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
	                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
	                                                                      $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                             + $('<div/>').text(data).html() + '" onclick="confirmDeleteCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                    	  $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="<liferay-ui:message key='Cannot cancel callback of other users.'/>"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                                      }
                                                    	}
                                                       else{//Internal not TTlabs
                                                    	   //ToDisable Create Callback
                                                    	   $('#createCallbckId').attr('disabled','disabled');
                                                    	   $('#createCallbckId').addClass("img-create-callback-disabled");
                                                    	   $('#createCallbckId').removeClass("img-create-callback");
                                                    	   $('#createCallbckId').attr("title", "<liferay-ui:message key='Cannot raise request for other companies.'/>");
                                                    	   //All freeze All View
                                                    	   return '<a name="id[]" title="<liferay-ui:message key='Cannot cancel other companie\u0027s callbacks.'/>"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Close_G_24.png" /> </a>';
                                                    	  
                                                       }
                                                }
                                           },
                                           {
                                                "defaultContent": "-",
                                                "targets": 6,
                                                "render": function (data, type, full, meta){
                                                       var isAdmin = document.getElementById("userAdminRole").value;
                                                       var isNOCManager = document.getElementById("isNOCManager").value;
                                                       var isExternal = document.getElementById("isExternalUser").value;
                                                       var loggedInUserEmailId = document.getElementById("emailId").value;
                                                       var isTTlabs = document.getElementById("isTTlabs").value;
                                                       var columnValue = full[6].split("@@@");
                                                       var editLeadId = columnValue[0];
                                                       var emailIdFromLead = columnValue[1];
                                                       if(isAdmin == "true"){//CustomerPortalAdmin OR EndCustomerAdmin
                                                    	   if(isExternal == "true"){//EndCustomerAdmin -- External Admin
                                                    		   // FIF VEC
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		  return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                    	   }
                                                    	   else{//CustomerPortalAdmin --  Internal Admin
                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                    		   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                    		   $('#createCallbckId').addClass("img-create-callback");
                                                    		   return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                    	   }
                                                       }
                                                       else if (isNOCManager == "true") {//NOCManager -- Internal
                                                    	   
                                                    	// See ALL
														// Edit Self created for ALl
														
	                                                    	   if(isTTlabs ==  "true"){
	                                                    		   $('#createCallbckId').removeAttr('disabled');
                                                                   $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                        		   $('#createCallbckId').addClass("img-create-callback");
                                                                         return '<a name="id[]" value="' 
                                                                  + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
																}
																else if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		 $('#createCallbckId').removeAttr('disabled');
                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                           		   $('#createCallbckId').addClass("img-create-callback");
                                                                            return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                                      }else{
                                                                    	 	 $('#createCallbckId').removeAttr('disabled');
                                                                    	 	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                                   		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="<liferay-ui:message key='Cannot edit callback of other users.'/>"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                                      }
														
													   }
                                                       else if (isExternal == "true") {//Other External user
														// See All
														//loggedinuser
																	if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                           		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                               		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" title="<liferay-ui:message key='Cannot edit callback of other users.'/>"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                                      }
														
														}
                                                       else if (isTTlabs ==  "true" && isExternal != "true"){//Other Internal and TTLabs
                                                   	   //Create and edit self
                                                   	   			if(emailIdFromLead ==  loggedInUserEmailId)
                                                                      {		$('#createCallbckId').removeAttr('disabled');
                                                                      $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                           		   $('#createCallbckId').addClass("img-create-callback");
                                                                             return '<a name="id[]" value="' 
                                                                     + $('<div/>').text(data).html() + '" onclick="editCallback(this)"> <img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /> </a>';
                                                                      }else{
                                                                    	  $('#createCallbckId').removeAttr('disabled');
                                                                    	  $('#createCallbckId').removeClass("img-create-callback-disabled");
                                                               		   $('#createCallbckId').addClass("img-create-callback");
                                                                            return '<a name="id[]" title="<liferay-ui:message key='Cannot edit callback of other users.'/>"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                                      }
                                                    	}
                                                       else{//Internal not TTlabs
                                                    	   
														   //ToDisable Create Callback
														   $('#createCallbckId').attr('disabled','disabled');
														   $('#createCallbckId').addClass("img-create-callback-disabled");
                                                		   $('#createCallbckId').removeClass("img-create-callback");
                                                		   $('#createCallbckId').attr("title", "<liferay-ui:message key='Cannot raise request for other companies.'/>");
														   //All freeze All View
                                                    	   return '<a name="id[]" title="<liferay-ui:message key='Cannot edit other companie\u0027s callbacks.'/>"><img style="height:18px;cursor: pointer;" src="<%=request.getContextPath()%>/images/Pencil.png" /></a>';
                                                    	   
                                                    	   
                                                       }
                                                }
                                           },/* {
                                                "defaultContent": "-",
                                                "targets": 0,
                                                "render": function (data, type, full, meta){
								
                                                    return '<div class="nameText" aria-label="'+full[0]+'" onclick="showHoverText(this)" style="display:inline-block;">'+full[0]+'</div><div class="hoverTextDivs">'+full[0]+'</div>';
                                                    	   }
                                           }, */
                                           {
                                                "defaultContent": "-",
                                                "targets": 7,
                                                "render": function (data, type, full, meta){
                                                       return '<div title="'+ $('<div/>').text(data).html() +'" >'+ $('<div/>').text(data).html() +'</div>'
                                                }
                                           },
                                           {
                                        	   "sClass": "comments", 
                                        	   "aTargets": [4]
                                           },
                                           {
                                        	   "sClass": "hidemob", 
                                        	   "aTargets": [1]
                                           },
                                           {
                                        	   "sClass": "hidemob", 
                                        	   "aTargets": [3]
                                           },
                                           {
                                        	   "sClass": "hidemobtab", 
                                        	   "aTargets": [5]
                                           },
                                           {
                                        	   "sClass": "name", 
                                        	   "aTargets": [0]
                                           }
                                           ],
                                                                                                              
                                         'order': [[1, "asc"]]
                                         });
                           },
                           error: function()
                           {
                                  //alert("error");
                           }
                     });
              }

		function showHoverText(obj){
			$(".hoverTextDivs").removeClass("displayHoverText");
			$(obj).next().addClass("displayHoverText");
		}

          
              function callbackDetailsCompleteTableAjax()
              {      
                     //alert("Function Called..");
                     var callbackDetailsURL = "${completeCallbackDetailsURL}";
                     //alert("URL:::"+callbackDetailsURL)
                     $.ajax({
                           url: callbackDetailsURL,
                           type: "POST",
                           dataType: 'json',
                           success : function(data)
                           {      //alert("Data is:"+data);
                                  table = $('#completeCallbackDetailsTable').DataTable({
                                         "aaData" : data,
                                         "destroy": true,
                                         "bFilter" : false,
                                         "processing" : true,
                                         "bPaginate" : true,
                                         "pageLength": 10,
                                         "serverSide" : false,      
                                         "bLengthChange" : false,
                                         "bSortable" : false,
                                         "bInfo": false,
                                         "columnDefs": [
                                         {
                                      	   "sClass": "comments", 
                                      	   "aTargets": [4]
                                         },
                                         {
                                      	   "sClass": "hidemob", 
                                      	   "aTargets": [1]
                                         },
                                         {
                                      	   "sClass": "hidemob", 
                                      	   "aTargets": [3]
                                         },
                                         {
                                      	   "sClass": "hidemobtab", 
                                      	   "aTargets": [5]
                                         },
                                         {
                                      	   "sClass": "name", 
                                      	   "aTargets": [0]
                                         }
                                         ],
                                         'order' : [[1,"asc"]]
                                         });
                           },
                           error: function()
                           {
                                  //alert("error");
                           }
                     });
              }
              
              function editCallback(link){
                     var leadId= link.getAttribute("value");
                     
                     for(var i=0;i<jsonLeadData.length;i++){
                    	 if(leadId==jsonLeadData[i][6]){
         					var temp = jsonLeadData[i][6];
         					var leadIdTemp = temp.split("@@@");
                                           //alert(jsonLeadData[i]);
                                           editLeadId = leadIdTemp[0] ;
                                           var editNameTemp = jsonLeadData[i][0];
                                           var editName = editNameTemp.split(" ");
                                           firstNameEdit = editName[0];
                                           lastNameEdit = editName[1];
                                           editPhoneNumber = jsonLeadData[i][2];
                                           editCategory = jsonLeadData[i][5];
                                           editCallbackDate = jsonLeadData[i][1];
                                           editComments = jsonLeadData[i][4];
                                           editIpscapeForm();

                                  
                           }
                     }
                     $("#editCallbackModal").modal('show');
                     
              }
              
              function editIpscapeForm(){
                                  resetIpscapeEditForm();
                                  
                                  
                                  var dateElement = editCallbackDate.split(" ");
                                  var currentDate = dateElement[0];
                                  var currentTime = dateElement[1];
                                  //alert(dateElement);
                                  var currentDateArray = currentDate.split("/");
                                  var currentTimeArray = currentTime.split(":");
                                         
                                  var dateToSet = new Date(currentDateArray[2] + '/' + currentDateArray[1] + '/' + currentDateArray[0]);
                                  //alert(currentDate);
                                  var editHours = currentTimeArray[0];
                                  var editMin = currentTimeArray[1];
                                  
                                  if(parseInt(editHours) >= 12){
                                         $("#pmradioEdit").attr('checked', true);
                                         if(parseInt(editHours) != 12){
                                                editHours = parseInt(editHours) - 12;
                                         }
                                  }else{
                                         $("#amradioEdit").attr('checked', true);
                                  }
                                  
                                  
                                  //$('select option:contains('+editHours + ':' editMin+')').prop('selected',true);
                                  //$("#rangeTimeEdit option[text='07:00']").attr("selected","selected");
                                  seletedTime = editHours +":"+ editMin;
                                  selectItem = "selectTime";
                                  $("select[name='rangeTimeEdit']").append('<option value="'+selectItem+'">'+seletedTime+'</option>');
                                  $('#rangeTimeEdit [value='+selectItem+']').prop('selected', true);
                                  
                                  
                                  $("#leadIdEdit").val(editLeadId);
                                  //$("#nameEdit").val(editName);
                                  $("#firstNameEdit").val(firstNameEdit);
                                  $("#lastNameEdit").val(lastNameEdit);
                                  $("#phoneEdit").val(editPhoneNumber);
                                  $("#categoryEdit").val(editCategory);
                                  $("#notesEdit").val(editComments);
                                  
                                  $("#callbackDateEdit").datepicker("setDate", dateToSet);
                                  
              }
	function confirmDeleteCallback(link){
			confirmdeleteleadId= link.getAttribute("value");
			$("#deleteCallbackConfirm").modal('show');
		}
        	
         function deleteCallback(){
                     
			$("#deleteCallbackConfirm").modal('hide');
			                     
		//	var confirm = document.getElementById("deleteCallbackConfirm").value;
		//	alert(confirm);
			 

                     var deleteCallbackURL = "${deleteCallbackURL}"+"&leadId="+confirmdeleteleadId;
                           
                           $.ajax({
                           url: deleteCallbackURL,
                           type: "POST",
                           dataType: 'json',
                           success : function(data)
                           {      //alert("one");
                                  var status = data.deleteCallback;
                                  //alert("status::"+status);
                                  if(status == "success"){
                                         //alert("SUCCESS");
                                         callbackDetailsIncompleteTableAjax();
                                         callbackDetailsCompleteTableAjax();
                                         document.getElementById("deleteLeadResponse").innerHTML = '<liferay-ui:message key="Callback Request is cancelled successfully"/>';
                                         $("#deleteCallbackResponse").modal('show'); 
                                  }else{
                                         //alert("FAIL");
                                         callbackDetailsIncompleteTableAjax();
                                         callbackDetailsCompleteTableAjax();
                                         document.getElementById("deleteLeadResponse").innerHTML = '<liferay-ui:message key="Callback Cancel request failed. Please try again."/>';
                                         $("#deleteCallbackResponse").modal('show'); 
                                  }                                 
                           },
                           error: function()
                           {
                                  //alert("error");
                           }
                     });
              }
              
              
</script>
<html>
<body>
<div class="container3" style="background-color: white;
       width: 125%;
       border: solid 1px #ccc; 
       border-bottom-right-radius:3px;
       border-bottom-left-radius:3px;  
       border-top-right-radius:3px;
       border-top-left-radius:3px;margin-top: 17px;" >
		<div id="button-set">
  			<button id="IpscapeButton" class=""><div id="IpscapeHeader"><div id="IpscapeContent"><liferay-ui:message key="CALLBACK REQUEST"/></div><div id="IpscapeIcon" class="IpscapeMore"></div></div></button>
			<div class="collapse3" id="ipscapeMainPageId" style="display: none;">
       				<div>
	                     <div class="row-fluid tt-page-content">
	                           <ul id="tabc" class="nav nav-pills tt-site-ul">
	                                  <li class="active tt-site-nav-pills selected" id="ipscapeActiveRequest-li">
	                                         <a data-toggle="tab" href="" class="tt-site-nav-pills" id="ipscapeRequest" onclick="activeCallbackTable()" style="font-size: 14px;"><liferay-ui:message key="ACTIVE CALLBACK REQUESTS"/></a>
	                                  </li>
	                                  <li class="tt-site-nav-pills" id="ipscapeCompleteRequest-li">
	                                         <a data-toggle="tab" href="" class="tt-site-nav-pills" id="ipscapeEdit" onclick="completeCallbackTable()" style="font-size: 14px;" ><liferay-ui:message key="COMPLETED CALLBACK REQUESTS"/></a>
	                                  </li>
	                           </ul>
	                     </div>
           				 <button type="button" id="createCallbckId" class="img-create-callback" onclick="openIpscapePage()" data-toggle="modal"><liferay-ui:message key="Request a Callback"/></button>
                     	 <div id="inCompleteCallbackDetailsDiv" class="row-fluid tt-site-tab-incident-content">
			                     <table id="inCompleteCallbackDetailsTable" class="display tt-siteManagmentTable-main" >
			                           <thead class="siteManagmentTable-head">
			                                  <tr>   
	                                       			<th class="tablefirstname"><liferay-ui:message key="Name"/></th>
	                                                <th class="tablefirstname"><liferay-ui:message key="Callback Date"/></th>
	                                                <th class="tablefirstname"><liferay-ui:message key="Contact"/></th>
	                                                <th class="tablefirstname"><liferay-ui:message key="Status"/></th>
	                                                <th class="tablelastname"><liferay-ui:message key="Comments"/></th>
	                                          		<th class="tablehomecontact"><liferay-ui:message key="Category"/></th>
	                                           		<th class="tableSelect" style="width: 53px;"><liferay-ui:message key="Edit"/></th>
	                                           		<th class="tableSelect"><liferay-ui:message key="Cancel"/></th>
			                                  </tr>
			                           </thead>
			                     </table>
                   		  </div>
                     	  <div id="completeCallbackDetailsDiv" class="row-fluid tt-site-tab-incident-content">
			                     <table id="completeCallbackDetailsTable" class="display tt-siteManagmentTable-main" >
			                           <thead class="siteManagmentTable-head">
			                                  <tr>   
	                                                <th class="tablefirstname"><liferay-ui:message key="Name"/></th>
	                                                <th class="tablefirstname"><liferay-ui:message key="Callback Date"/></th>
	                                                <th class="tablefirstname"><liferay-ui:message key="Contact"/></th>
	                                                <th class="tablefirstname"><liferay-ui:message key="Status"/></th>
	                                                <th class="tablelastname"><liferay-ui:message key="Comments"/></th>
	                                          		<th class="tablehomecontact"><liferay-ui:message key="Category"/></th>
			                                  </tr>
			                           </thead>
			                     </table>
           				  </div>
       				</div>
			  </div>
		</div>
</div>
<div class="modal fade" id="IPScapeId" data-backdrop="static" data-keyboard="false" aria-hidden="true">
     <div class="modal-header">
            <button type="button" class="close" id="closeMarkID" onclick="showFloatingButton(); resetMessages(); resetIpscapeForm(); " data-dismiss="modal">
                  <img class="closeMark" src="/TTApplicationPortlets/images/Close_G_24.png"> 
            </button> 
            <div class="modal-title logTitle">
                  <liferay-ui:message key="REQUEST CALLBACK"/>
            </div> 
     </div>
     <div class="modal-body tt-modal-body">
          <div id="ipscapeRequestDiv" class="row-fluid tt-site-tab-incident-content">
               <form:form id="ipScapeForm" class="form-horizontal tt-modal-form" modelAttribute="ipscapeObj" method="post">
                   		<form:input type="hidden" id="hoursAsiaJakarta" path="hoursAsiaJakarta"  name="hoursAsiaJakarta" value="${hoursAsiaJakarta}"/>
                        <form:input type="hidden" id="minutesAsiaJakarta" path="minutesAsiaJakarta"  name="minutesAsiaJakarta" value="${minutesAsiaJakarta}"/>
                        <form:input type="hidden" id="dateAsiaJakarta" path="dateAsiaJakarta"  name="dateAsiaJakarta" value="${dateAsiaJakarta}"/>
                        <form:input type="hidden" id="monthsAsiaJakarta" path="monthsAsiaJakarta"  name="monthsAsiaJakarta" value="${monthsAsiaJakarta}"/>
                        <form:input type="hidden" id="yearAsiaJakarta" path="yearAsiaJakarta"  name="yearAsiaJakarta" value="${yearAsiaJakarta}"/>
                        <form:input type="hidden" id="companyName" path="companyName"  name="companyName" value="${companyName}"/>
                        <form:input type="hidden" id="emailId" path="emailId"  name="emailId" value="${emailId}"/>
                        <form:input type="hidden" id="isExternalUser" path="isExternalUser"  name="isExternalUser" value="${isExternalUser}"/>
                        <form:input type="hidden" id="userAdminRole" path="userAdminRole"  name="userAdminRole" value="${userAdminRole}"/>
                        <form:input type="hidden" id="isTTlabs" path="isTTlabs"  name="isTTlabs" value="${isTTlabs}"/>
                        <form:input type="hidden" id="isNOCManager" path="isNOCManager"  name="isNOCManager" value="${isNOCManager}"/>
                                         
                        <div class="container-fluid tt-container">
                             <div class="row-fluid tt-ipscape-contains">
                                  <div class="span4 tt-form-left-col">
                                       <liferay-ui:message key="First Name:" />
                                  </div>
                                  <div class="span3 tt-form-right-col">
                                       <form:input type="text" id="firstName" path="firstName" class="textBoxLogInc" name="firstName" value="${firstName}" readonly="true" />
                                  </div>
                                  <div class="row-fluid ">
                                        <label for="firstName" class="validationError" id="firstNameId"></label>
                                  </div>
                              </div>
                              <div class="row-fluid tt-ipscape-contains">
                                   <div class="span4 tt-form-left-col">
                                        <liferay-ui:message key="Last Name:" />
                                   </div>
                                   <div class="span3 tt-form-right-col">
                                        <form:input type="text" id="lastName" path="lastName" class="textBoxLogInc" name="lastName" value="${lastName}" readonly="true" />
                                   </div>
                                   <div class="row-fluid ">
                                        <label for="lastName" class="validationError" id="lastNameId"></label>
                                   </div>
                           	  </div>
                              <div class="row-fluid tt-ipscape-contains ">
                                   <div class="span4 tt-form-left-col">
                                        <liferay-ui:message key="Phone Number:" />
                                        <a href="#" data-toggle="tooltip" data-placement="top"
                                               title="<liferay-ui:message key="Please enter Country Code followed by phone number."/> &#013; Example: +62 8146662548">
                                               <img src="<%=request.getContextPath()%>/images/Discription_G_16.png">
                                        </a>
                                   </div>
                                   <div class="span3 tt-form-right-col">
                                        <form:input type="text" id="telephone" name="telephone" path="telephone" value="${workContact}" />
                                   </div>
                               </div>
                               <div>
                                    <label for="telephone" class="validationError" id="telephoneId"></label>
                               </div>
                               <div id="categoryDiv" class="row-fluid tt-ipscape-contains">
                                  	<div class="span4 tt-form-left-col">
                                         <liferay-ui:message key="Category:" />
                                    </div>
                                    <div class="span3 tt-form-right-col">
                                         <form:select id="category" name="category" path="category">
                                                <option value=""><liferay-ui:message key="--Select--" /></option>                       
                                         </form:select>
                                    </div>
                                </div>
                                <div>
                                     <label for="category" class="validationError" id="categoryId"></label>
                                </div>
                                <div class="row-fluid tt-ipscape-contains">
                                     <div class="span4 tt-form-left-col" >
                                            <liferay-ui:message key="Callback Date:" />
                                            <a href="#" data-toggle="tooltip" data-placement="top"
                                                   title="<liferay-ui:message key="Callback can be requested only for next 30 days"/>">
                                                   <img class="helpIconImg"
                                                   src="<%=request.getContextPath()%>/images/Discription_G_16.png">
                                            </a>
                                     </div>
                                     <div class="span3 tt-form-right-col">
                                            <form:input id="callbackDate" name="callbackDate" path="callbackDate" class="selectDate" type="text" onchange="enableAMPM()" placeholder="Click to select Date" />
                                     		<label for="callbackDate" class="validationError" id="callbackDateId"></label>
                                     </div>
                                </div>
                                <div class="row-fluid tt-ipscape-contains ">
                                     <div class="span4 tt-form-left-col" >
                                          <liferay-ui:message key="Time of call:" />
                                     </div>
                               		 <div class="span8 tt-form-right-col">
                                          <div class="row-fluid span4">
                                               <form:input type="radio" id="amradio" name="ampmradio" path="ampmradio" value="am" onchange="populateTimeDropDown()" />AM 
                                               <form:input type="radio" id="pmradio" name="ampmradio" path="ampmradio" value="pm" onchange="populateTimeDropDown()" />PM
                                          </div>
                                          <div id="rangeTimeDiv" class="row-fluid span9" >
                                               <form:select id="rangeTime" name="rangeTime" path="rangeTime">
                                                      <option value=""><liferay-ui:message key="--Select--"/></option>
                                               </form:select> JKT(GMT +7)
                                           </div>
                                      </div>
                                 </div>
                                 <div  class="row-fluid ">
                                       <label for="ampmradio" class="validationError" id="ampmradioId"></label>
                                 </div>
	                             <div>
	                                  <label for="rangeTime" class="validationError" id="rangeTimeId"></label>
	                             </div>
                                 <div class="row-fluid tt-ipscape-contains ">
                                      <div class="span4 tt-form-left-col">
                                           <liferay-ui:message key="Notes:" />
                                      </div>
                                      <div class="span8 tt-form-right-col"> 
                                           <form:textarea path="comments" id="comments" name="comments" tabindex="9" rows="4" cols="100" maxlength="100"/> 
                                      </div>
                                  </div>
                                  <div class="row-fluid ">
                                       <label for="comments" class="validationError" id="commentsId"></label>
                                  </div>
                                  <div class="row-fluid tt-form-last-row pr-15 pt-30">
                                       <div class="span12 tt-submit-row" > 
	                                      <span class="tt-cancel pr-10 ipscape-cancel" > 
	                                            <button type="reset" class="btn-secondary-tt img-cancel" onclick="resetIpscapeForm();" id="resetButtonID"  tabindex="11"> <liferay-ui:message key="Reset" /> 
	                                            </button>
	                                      </span> 
	                                      <span class="tt-submit">
	                                             <button type="submit" class="btn-primary-tt img-submitrequestenabled" id="submitRequest"  tabindex="10"> 
	                                             <liferay-ui:message key="Submit" /> 
	                                             </button> 
	                                      </span> 
                                       </div>
                                   </div>
                              </div>
                  </form:form>
          	</div>
  	  </div>
</div>
<div class="modal fade" id="IpscapeSubmitResponse" data-backdrop="static" data-keyboard="false" aria-hidden="true">
       <div class="modal-header">
              <div class="row-fluid ">
                     <div class="span12 tt-page-title">
                           <liferay-ui:message key="Request Callback" />
                     </div>
              </div>
       </div>
       <div class="modal-body tt-modal-body">
              <div class="modal-title" id="responseStatusIPScape"></div>
       </div>
       <div class="modal-footer tt-submit-row">
            <button type="button" class="btn-secondary-tt img-cancel" id="resetButtonID" onclick="resetIpscapeForm(); activeCallbackTable();"  data-dismiss="modal" tabindex="10">
                   <liferay-ui:message key="Close" />
            </button>
       </div>
</div>
<div class="modal fade" id="confirmationBoxId" data-backdrop="static" data-keyboard="false" aria-hidden="true">
       <div class="modal-header" >
            <div class="modal-title" id="confirmheader"><liferay-ui:message key="Confirm Callback Request Details" /></div>
       </div>
       <div class="modal-body tt-modal-body">
              <div id="confirmationString"></div>
              <div id="phoneVerify"></div>
              <div id="dateVerify"></div>
              <div id="timeVerify"></div>
       </div>
       <div class="modal-footer tt-submit-row">
            <button type="button" id="cancelRequest" class="btn-secondary-tt" onclick="showIPScapeForm()" data-dismiss="modal" tabindex="10"><liferay-ui:message key="Cancel" /></button>
            <button type="button" id="okbuttonid" class="btn-secondary-tt" tabindex="10"><liferay-ui:message key="Confirm" /></button>  
       </div>
</div>
<div class="modal fade" id="deleteCallbackConfirm" data-backdrop="static" data-keyboard="false" aria-hidden="true" style="display:none;">
     <div class="modal-header" >
          <div class="modal-title" id="confirmheader"><liferay-ui:message key="Cancel Callback Request" /></div>
     </div>
     <div class="modal-body tt-modal-body">
          <div id="deleteLeadConfirm"><liferay-ui:message key="Please confirm if you want to cancel this callback request?" /></div>
     </div>
     <div class="modal-footer tt-submit-row">
         <button type="button" id="confirmDeleteLead" class="btn-secondary-tt" onclick="deleteCallback()" data-dismiss="modal" tabindex="10">
                 <div id="YesConfirm" style="position: relative; margin-right: 30px;"><liferay-ui:message key="Yes" /></div>
         </button> 
  		 <button type="button" id="cancelDeleteLead" class="btn-secondary-tt" data-dismiss="modal" tabindex="10">
                <div id="NoConfirm" style="position: relative; margin-right: 30px;"><liferay-ui:message key="No" /></div>
          </button>
     </div>
</div>
<div class="modal fade" id="deleteCallbackResponse" data-backdrop="static" data-keyboard="false" aria-hidden="true" style="display:none;">
       <div class="modal-header" >
            <div class="modal-title" id="confirmheader"><liferay-ui:message key="Cancel Callback Request" /></div>
       </div>
       <div class="modal-body tt-modal-body">
            <div id="deleteLeadResponse"></div>
       </div>
       <div class="modal-footer tt-submit-row">
           <button type="button" id="cancelRequest" class="btn-secondary-tt" data-dismiss="modal" tabindex="10">
                   <liferay-ui:message key="Ok" />
           </button> 
       </div>
</div>
<%-- <div class="modal fade" id="editConfirmationBoxId" data-backdrop="static" data-keyboard="false" aria-hidden="true">
       <div class="modal-header" >
            <div class="modal-title" id="confirmheader"><liferay-ui:message key="Confirm Update Callback Request Details" /></div>
       </div>
       <div class="modal-body tt-modal-body">
              <div id="editConfirmationString"></div>
              <div id="editPhoneVerify"></div>
              <div id="editDateVerify"></div>
              <div id="editTimeVerify"></div>
       </div>
       <div class="modal-footer tt-submit-row">
            <button type="button" id="cancelEditRequest" class="btn-secondary-tt" onclick="showIPScapeForm()" data-dismiss="modal" tabindex="10"><liferay-ui:message key="Cancel" /></button>
            <button type="button" id="editOkbuttonid" class="btn-secondary-tt" tabindex="10"><liferay-ui:message key="Confirm" /></button>  
       </div>
</div> --%>
<div class="modal fade" id="editLeadResponseDiv" data-backdrop="static" data-keyboard="false" aria-hidden="true" style="display:none;">
     <div class="modal-header" >
          <div class="modal-title" id="confirmheader"><liferay-ui:message key="Edit Callback" /></div>
     </div>
     <div class="modal-body tt-modal-body">
       	  <div id="editLeadResponse"></div>
     </div>
     <div class="modal-footer tt-submit-row">
       	  <button type="button" id="cancelRequest" class="btn-secondary-tt" data-dismiss="modal" tabindex="10">
                  <liferay-ui:message key="Ok" />
          </button> 
     </div>
</div>
<div class="modal fade" id="editCallbackModal" data-backdrop="static" data-keyboard="false" aria-hidden="true" style="display:none;">
     <div class="modal-header">
          <div class="row-fluid ">
                <div class="span12 tt-page-title">
                     <liferay-ui:message key="Edit Callback" />
                                        <button type="button" class="close" id="closeMarkID"  data-dismiss="modal">
                                                <img class="closeMark" src="/TTApplicationPortlets/images/Close_G_24.png"> 
                                        </button> 
                 </div>
           </div>
       </div>
       <div class="modal-body tt-modal-body">
            <form:form id="ipscapeEditForm" class="form-horizontal tt-modal-form" method="post">
                       <div class="row-fluid tt-ipscape-contains ">
                            <div class="span4 tt-form-left-col">
                                 <liferay-ui:message key="Callback Id:" />
                            </div>
                            <div class="span8 tt-form-right-col"> 
                                 <input type="text"  id="leadIdEdit" name="leadIdEdit" readonly />  
                            </div>
                        </div>
                        <div class="row-fluid tt-ipscape-contains">
                                  <div class="span4 tt-form-left-col">
                                       <liferay-ui:message key="First Name:" />
                                  </div>
                                  <div class="span3 tt-form-right-col">
                                       <input type="text" id="firstNameEdit" name="firstName" readonly/>
                                  </div>
                                  <!-- <div class="row-fluid ">
                                        <label for="firstName" class="validationError" id="firstNameId"></label>
                                  </div> -->
                         </div>
                         <div class="row-fluid tt-ipscape-contains">
                                   <div class="span4 tt-form-left-col">
                                        <liferay-ui:message key="Last Name:" />
                                   </div>
                                   <div class="span3 tt-form-right-col">
                                        <input type="text" id="lastNameEdit" name="lastName" readonly/>
                                   </div>
                                  <!--  <div class="row-fluid ">
                                        <label for="lastName" class="validationError" id="lastNameId"></label>
                                   </div> -->
                         </div>
                        <%-- <div class="row-fluid tt-ipscape-contains ">
                             <div class="span4 tt-form-left-col">
                                  <liferay-ui:message key="Name:" />
                             </div>
                             <div class="span8 tt-form-right-col"> 
                                  <input type="text"  id="nameEdit" name="nameEdit" readonly />  
                             </div>
                         </div> --%>
                         <div class="row-fluid tt-ipscape-contains ">
                              <div class="span4 tt-form-left-col">
                                   <liferay-ui:message key="Phone Number:" />
                              </div>
                              <div class="span8 tt-form-right-col"> 
                                                <input type="text"  id="phoneEdit" name="phoneEdit" /> 
                              </div>
                              <label for="phoneEdit" class="error" id="phoneEditId"></label>
                          </div>
                          <div class="row-fluid tt-ipscape-contains ">
                               <div class="span4 tt-form-left-col">
                                    <liferay-ui:message key="Category:" />
                               </div>
                               <div class="span8 tt-form-right-col"> 
                                    <input type="text"  id="categoryEdit" name="categoryEdit" readonly /> 
                               </div>
                           </div>
                           <div class="row-fluid tt-ipscape-contains">
                                <div class="span4 tt-form-left-col" >
                                     <liferay-ui:message key="Callback Date:" />
                                                  <a href="#" data-toggle="tooltip" data-placement="top"
                                                              title="<liferay-ui:message key="Callback can be requested only for next 30 days"/>">
                                                              <img class="helpIconImg"
                                                              src="<%=request.getContextPath()%>/images/Discription_G_16.png">
                                                  </a>
                                </div>
                                <div class="span3 tt-form-right-col">
                                     <input id="callbackDateEdit" name="callbackDateEdit" onchange="enableAMPMEdit()" class="selectDate" type="text"  placeholder="Click to select Date" />
                                	 <label for="callbackDateEdit" class="error" id="callbackDateEditId"></label>
                                </div> 
                            </div>
							<div class="row-fluid tt-ipscape-contains ">
                                 <div class="span4 tt-form-left-col" >
                                      <liferay-ui:message key="Time of call:" />
                                 </div>
  								 <div class="span8 tt-form-right-col">
                                   	  <div class="row-fluid span4">
                                           <input type="radio" id="amradioEdit" name="ampmradioEdit"  value="am" onchange="populateTimeDropDownEdit()" />AM 
                                           <input type="radio" id="pmradioEdit" name="ampmradioEdit"  value="pm" onchange="populateTimeDropDownEdit()" />PM
                                     </div>
                                     <div class="row-fluid span9" id="rangeTimeEditDiv">
                                          <select id="rangeTimeEdit" name="rangeTimeEdit" >
                                                  <option value=""><liferay-ui:message key="--Select--"/></option>
                                          </select> JKT(GMT +7)
                               		 </div>
                           		  </div>
                         	  </div>
                         	  <div class="row-fluid ">
                                   <label for="ampmradioEdit" class="error" id="ampmradioEditId"></label>
                              </div>
   							  <div  class="row-fluid ">
                                  	<label for="rangeTimeEdit" class="error" id="rangeTimeEditId"></label>
                       		  </div>
                       		  <div class="row-fluid tt-ipscape-contains ">
                                  <div class="span4 tt-form-left-col">
                                       <liferay-ui:message key="Notes:" />
                                  </div>
                                  <div class="span8 tt-form-right-col"> 
                                       <textarea  id="notesEdit" name="notesEdit" tabindex="9" rows="4" cols="100" maxlength="100" > </textarea> 
                                  </div>
                      		  </div>
                       		  <div class="row-fluid tt-form-last-row pr-15 pt-30">
	                               <div class="span12 tt-submit-row" > 
                                       <span class="tt-cancel pr-10 ipscape-cancel" > 
                                              <button type="button" class="btn-secondary-tt img-cancel" id="resetButtonIDEdit" onclick="editIpscapeForm()"  tabindex="11"> 
                                                     <liferay-ui:message key="Reset" /> 
                                              </button>
                                       </span> 
                                       <span class="tt-submit">
                                              <button type="submit" class="btn-secondary-tt img-submitrequestenabled img-cancel" id="submitRequestEdit"  tabindex="10"> 
                                                     <liferay-ui:message key="Submit" /> 
                                              </button> 
                                       </span> 
	                                </div>
                         	  </div>
                </form:form>
   		</div>
</div>
</body>
</html>