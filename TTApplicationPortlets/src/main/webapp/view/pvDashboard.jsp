<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<!-- 
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-modal.js"></script>
<script
	src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> -->

<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.validate-1.9.min.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/ttrefresh.css">	

<%@ include file="common.jsp" %>

	<div class="container-fluid tt-dashboard-header">
		<div class="row-fluid">
			<div class="span6 tt-breadcrumb">
				<div class="dashImageHolder">
					<img class=""
						src="<%=request.getContextPath()%>/images/DashBoard_G_32.png" />
				</div>
				<div class="dashboardTitleHolder">
					<div class="dashboardText">
						<liferay-ui:message key="Dashboard" />
					</div>
				</div>
			</div>
		</div>
	</div>