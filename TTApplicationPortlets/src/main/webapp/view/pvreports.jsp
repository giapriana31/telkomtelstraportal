<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="common.jsp" %>
<style>

@media (max-width: 767px){
	#mobile_msg{
	/* font-size:11px;
	float:right; */
	display:none;
	}
	#mobile_msg1{
	display:none;
	}
	.aui a {
	text-decoration:none ;
	}
	.aui a:hover{
	text-decoration:none ;
	}
	
  }
  
	@media (min-width: 768px){
	#mobile_msg{
	display:none;
	}
	
}

	@media (min-width: 1200px) {
	#mobile_msg1{
	float:right;
	font-size:11px;
	}
	.aui a:hover{
	text-decoration:none ;
	}
	.aui a {
	text-decoration:none ;
	}
	
  }
@media (min-width: 768px) and (max-width: 1199px){
	#mobile_msg1{
	float:right;
	font-size:11px;
	}
	.aui a:hover{
	text-decoration:none ;
	}
	.aui a {
	text-decoration:none ;
	}
	
  }
}
@media (max-width: 700px){
.reportsmsg{
min-height:835px !important;
}

}
</style>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/reports.css" />
<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.validate-1.9.min.js"
	type="text/javascript"></script>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<portlet:defineObjects />
<liferay-theme:defineObjects />
<%-- <portlet:resourceURL var="openReportURL"  >
 <portlet:param name="action" value="openSWReports"/> 
</portlet:resourceURL>
<portlet:resourceURL var="getImageUrl"  >
 <portlet:param name="action" value="getImage"/> 
</portlet:resourceURL> --%>

<portlet:resourceURL id="openSWReports" var="openSWReports">
 <portlet:param name="action" value="openSWReports"/> 

</portlet:resourceURL>
<portlet:resourceURL id="getImage" var="getImage">
 <portlet:param name="action" value="getImage"/> 

</portlet:resourceURL>
<html> 
<body>
<c:set var="backDoorEntry" value='<%=isbackdoor%>'></c:set>
<c:if test="${backDoorEntry==true}">

<div>
<img src="<%=request.getContextPath()%>/images/alert1.png" style="position: relative;left: 43%;margin-top: 13%;">
<div style="font-size: 18px;color:#FF9C00;margin-left: 40px;margin-top: 42px;text-align:center;"><liferay-ui:message key="Page is temporarily unavailable, Please contact Administrator!!!"/></div>
 </div>
</c:if>
<c:if test="${backDoorEntry==false}">
	<div style="width:100%;height:75px;display:table;background:white;" class="reportsheader">		
		<div style="display:table-cell;vertical-align:middle;padding-left:20px; border-bottom-width:1px;border-bottom-color:#CCC;border-bottom-style: solid;">
			<img src="<%=request.getContextPath()%>/images/Reports_G_32.png" />
			<font style="font-size:15px;vertical-align:middle;margin-left:1%;font-weight:200"><liferay-ui:message key="Reports"/></font>
			<%-- <img  id="reportNameImage" src="<%=request.getContextPath()%>/images/ReadFull_G_16_right.png" /> --%>
		<%-- 	<font id="reportName" style="font-size:20px;vertical-align:middle;margin-left:1%;font-weight:200;position: relative;top: 2px;"><liferay-ui:message key="Report Name"/></font> --%>
			<%-- <div id="backToReportsDiv"><button id="backToReports"><img id="backImage" src="<%=request.getContextPath()%>/images/Back_W_16.png" /><div id="backToReportsContent"><liferay-ui:message key="Back"/></div></button></div> --%>
		</div>
		<hr style="border-bottom: 1px solid #CCC !important;
	  border-top: none !important;">
	</div>



	<div id="mydiv" style="margin:25px;">
	<div style="background:white;">    
	<div id="set_height" class="reportsmsg"style="position: relative; min-height:500px;border: solid 1px #CCCCCC;padding:3%;">
	<div>
	<div style="font-size:127%; background-color: #ccc; height: 35px;">
	<div style="  padding-top: 0.5%;padding-left: 0.5%;font-weight: bold">
	<liferay-ui:message key="Report Name"/>
	</div>
	</div>


						
 						
	<div id="mobile_msg">
	<liferay-ui:message key="ACCESS PORTAL IN DESKTOP FOR MORE REPORT"/>
	</div>
						
	<div id="mobile_msg1">
	<a href="#" class="reportContent" onclick="window.open('<%=prop.getProperty("PVReports.URL")%>','','height:400;width:400;')" style="float:right;color:#555 !important;font-size:11px;"><liferay-ui:message key="CLICK HERE FOR MORE REPORT"/></a>
	</div>
	
	</div>	
	</div>
	</div>
	 </div>
	</c:if>	
	</body>
	</html>


