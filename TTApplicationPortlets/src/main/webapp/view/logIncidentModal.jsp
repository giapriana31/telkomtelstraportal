<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="common.jsp" %>

<portlet:actionURL var="submitFormURL" name="handleLogIncident" />
<portlet:resourceURL var="submitRequestURLLI" id="submitRequestURLLI" />
<portlet:resourceURL var="getServiceURLLI" id="getServiceURLLI" />
<portlet:resourceURL var="getSiteURL" id="getSiteURL" />
<portlet:resourceURL var="getVMURL" id="getVMURL" />
<portlet:resourceURL var="getSaasWhispirResources" id="getSaasWhispirResources" />
<portlet:resourceURL var="getSaasIPScapeResources" id="getSaasIPScapeResources" />
<portlet:resourceURL var="getSaasMandoeResources" id="getSaasMandoeResources" />


<style>
#LogIncidentModalBody{
	max-height: 400px !important;
}
</style>
<script type="text/javascript">
	var siteData = "";
	function getSite(dropdown) {
		siteData = new Array();
		var selectedValue = dropdown.options[dropdown.selectedIndex].value;
		/* alert('Selected Value + ' + selectedValue); */
		var getSiteURL = "${getSiteURL}"
		var jsonCategory = {
			"selectedRegion" : selectedValue
		};
		var dataJson = {
			selectedRegionJson : JSON.stringify(jsonCategory)
		};
		$.ajax({
			url : getSiteURL,
			type : "POST",
			data : dataJson,
			success : function(response) {
				$('#siteID option[value!="0"]').remove();
				$('#serviceNameSelect option[value!="0"]').remove();
				var x = 1;
				var arr = response.split("#####");
				var len = arr.length;
				var reslen = len - x;
				for (var i = 0; i < reslen; i++) {
				//alert(response.split("#####")[i].split('@@@@@')[1]);
				//var temp = response.split("#####")[i].split('@@@@@')[1];
				siteData.push(response.split("#####")[i].split('@@@@@')[1]);
				
				}
				BindControls();
				//siteData=siteData+"]";
				//alert("hhhh" +siteData );
				$('#siteID option[value=""]').remove();
			},
			error : function(xhr) {
				BindControls();
				$('#siteID option[value!="0"]').remove();
			}
		});
	}
</script>

<script type="text/javascript">

	$(document)
			.ready(
					function() {
						var incID;
						var submitRequestURLLI = "${submitRequestURLLI}";
						jQuery.validator.addMethod('selectcheck', function(
								value) {
							return (value != '0');
						}, "<liferay-ui:message key='Please select a valid option' />");
						
						jQuery.validator.addMethod('fileSizeCheck', function(value, element, param) {
							 return this.optional(element) || (element.files[0].size <= param) 
							
						}, "<liferay-ui:message key='File Size should be less than 5 MB' />");

						var validator = $("#logIncForm")
								.validate(
										{
											rules : {
												summary : {
													required : true,
													maxlength : 200
												},
												worklognotes : {
													required : true,
													maxlength : 200
												},
												region : {
													selectcheck : true
												},
												
												serviceid : {
													selectcheck : true
												},
												impact : {
													selectcheck : true
												},
												urgency : {
													selectcheck : true
												},
												siteid: {
													required : true
												}
											},
											messages : {
												summary : {required:"<liferay-ui:message key='Please enter summary of incident' />"
													/* "Please enter summary of incident" */,
															maxlength:"<liferay-ui:message key='Maximum characters allowed is 200' />"
														   },
												worklognotes : {required:"<liferay-ui:message key='Please enter description of incident' />",
																maxlength:"<liferay-ui:message key='Maximum characters allowed is 200' />"
												   			},
												siteid : { required:"<liferay-ui:message key='Please select a valid site name' />",
														}
											},
											highlight : function(element,
													errorClass, validClass) {
												return false; // ensure this function stops
											},
											unhighlight : function(element,
													errorClass, validClass) {
												return false; // ensure this function stops
											},
											submitHandler : function(form) {

												//var dataForm = $('#logIncForm').serialize();
													
												var dataForm = new FormData($('#logIncForm')[0]);
												
												$('#submitRequestButtonID')
														.attr('disabled',
																'disabled');
												$('#submitRequestButtonID')
														.addClass(
																"disableButton");

												$('#regionID').attr('disabled',
														'disabled');
												$('#regionID').addClass(
														"disableButton");

												$('#siteIDAuto').attr('disabled',
														'disabled');
												$('#siteIDAuto').addClass(
														"disableButton");

												$('#serviceNameSelect').attr(
														'disabled', 'disabled');
												$('#serviceNameSelect')
														.addClass(
																"disableButton");

												$('#MultiServiceCheckBox')
														.attr('disabled',
																'disabled');
												$('#MultiServiceCheckBox')
														.addClass(
																"disableButton");

												$('#dashTickNotesTextArea')
														.attr('disabled',
																'disabled');
												$('#dashTickNotesTextArea')
														.addClass(
																"disableButton");

												$('.textBoxLogInc').attr(
														'disabled', 'disabled');
												$('.textBoxLogInc').addClass(
														"disableButton");

												$('.selectBoxLogInc').attr(
														'disabled', 'disabled');
												$('.selectBoxLogInc').addClass(
														"disableButton");

												$('#cancelButtonID').attr(
														'disabled', 'disabled');
												$('#cancelButtonID').addClass(
														"disableButton");

												$('#closeMarkID').attr(
														'disabled', 'disabled');
												$('#closeMarkID').addClass(
														"disableButton2");

												$
														.ajax({
															url : submitRequestURLLI,
															type : "POST",
															data : dataForm,
															success : function(
																	response) {
																/* alert(response); */
																incID = JSON
																		.parse(response)
																var status = incID.incidentID
																		.split("@@@")[0];
																/* alert('Response - ' + status); */
																if (status == 'Success') {
																	document
																			.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="SUCCESSFUL"/>';
																	document
																			.getElementById("incidentID").innerHTML = '<liferay-ui:message key="Your incident - "/>'
																			+ incID.incidentID
																					.split("@@@")[1]
																	+ '&nbsp;'+'<liferay-ui:message key="has been submitted successfully!"/>';
																} else {
																	document
																			.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
																	document
																			.getElementById("incidentID").innerHTML = '<liferay-ui:message key="Incident submission failed. Please try again."/>';
																}
																$(
																		'#logIncSubmitResponse')
																		.modal(
																				'show');

																$(
																		'#submitRequestButtonID')
																		.removeAttr(
																				'disabled');
																$(
																		'#submitRequestButtonID')
																		.removeClass(
																				"disableButton");

																$('#siteIDAuto')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$('#siteIDAuto')
																		.removeClass(
																				"disableButton");

																$(
																		'#serviceNameSelect')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#serviceNameSelect')
																		.removeClass(
																				"disableButton");

																$(
																		'#MultiServiceCheckBox')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#MultiServiceCheckBox')
																		.removeClass(
																				"disableButton");

																$(
																		'#dashTickNotesTextArea')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#dashTickNotesTextArea')
																		.removeClass(
																				"disableButton");

																$(
																		'.textBoxLogInc')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'.textBoxLogInc')
																		.removeClass(
																				"disableButton");

																$(
																		'.selectBoxLogInc')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'.selectBoxLogInc')
																		.removeClass(
																				"disableButton");

																$(
																		'#cancelButtonID')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#cancelButtonID')
																		.removeClass(
																				"disableButton");

																$(
																		'#closeMarkID')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#closeMarkID')
																		.removeClass(
																				"disableButton2");

																$("#logIncForm")[0]
																		.reset();
																$(
																		'#serviceNameSelect option[value!="0"]')
																		.remove();
																/* $('.submitRequestButton').prop('disabled', false); */
															},
															error : function(
																	xhr) {
																/* alert('error ' + xhr); */
																document
																		.getElementById("responseStatus").innerHTML = '<liferay-ui:message key="ERROR"/>';
																document
																		.getElementById("incidentID").innerHTML = '<liferay-ui:message key="Incident submission failed. Please try again."/>';
																$(
																		'#logIncSubmitResponse')
																		.modal(
																				'show');
																$(
																		'#submitRequestButtonID')
																		.removeAttr(
																				'disabled');
																$(
																		'#submitRequestButtonID')
																		.removeClass(
																				"disableButton");

																$('#regionID')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$('#regionID')
																		.removeClass(
																				"disableButton");

																$('#siteIDAuto')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$('#siteIDAuto')
																		.removeClass(
																				"disableButton");

																$(
																		'#serviceNameSelect')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#serviceNameSelect')
																		.removeClass(
																				"disableButton");

																$(
																		'#MultiServiceCheckBox')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#MultiServiceCheckBox')
																		.removeClass(
																				"disableButton");

																$(
																		'#dashTickNotesTextArea')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#dashTickNotesTextArea')
																		.removeClass(
																				"disableButton");

																$(
																		'.textBoxLogInc')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'.textBoxLogInc')
																		.removeClass(
																				"disableButton");

																$(
																		'.selectBoxLogInc')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'.selectBoxLogInc')
																		.removeClass(
																				"disableButton");

																$(
																		'#cancelButtonID')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#cancelButtonID')
																		.removeClass(
																				"disableButton");

																$(
																		'#closeMarkID')
																		.removeAttr(
																				'disabled',
																				'disabled');
																$(
																		'#closeMarkID')
																		.removeClass(
																				"disableButton2");

																$("#logIncForm")[0]
																		.reset();
																$(
																		'#serviceNameSelect option[value!="0"]')
																		.remove();
															},
															processData: false,
														    contentType: false
														});
											}
										});
					});
</script>

<script type="text/javascript">
	function getServiceLI() {
			var selectedValue = $('#siteIDAuto').val();
			var getServiceURLLI = "${getServiceURLLI}"
			var jsonSiteID = {"selectedSiteID" : selectedValue};
		var dataJson = {
			selectedSiteIDJson : JSON.stringify(jsonSiteID)
		};
		$.ajax({
			url : getServiceURLLI,
			type : "POST",
			data : dataJson,
			success : function(response) {
			//alert('success '+response);
				$('#serviceNameSelect option[value!="0"]').remove();
				var x = 1;
				var arr = response.split("#####");
				var len = arr.length;
				var reslen = len - x;
				for (var i = 0; i < reslen; i++) {

					$('#serviceNameSelect').append(
							$('<option>')
									.text(
											response.split("#####")[i]
													.split('@@@@@')[1]).val(
											response.split("#####")[i]
													.split('@@@@@')[1]));
				}
				$('#serviceNameSelect option[value=""]').remove();
			},
			error : function(xhr) {
					
			}
		});
	}
</script>


 <script>
 
        $(document).ready(function() {
        	$("#LogIncidentModal").on('shown', function(){
        		$('#mnsID').hide();
        		$('#privateCloudFieldsID').hide();
        		$('#forMNSServices').hide();
        		$('#forPrivateCloud').hide();
				$('#saasWhispirFieldsID').hide();
				$('#saasMandoeFieldsID').hide();
				$('#saasIPScapeFieldsID').hide();
        	});
        });

        function BindControls() {
		 	$('#siteIDAuto').autocomplete({
                source: siteData,
                minLength: 0,
				appendTo: "#LogIncidentModal",
                scroll: true
            }).focus(function() {
                $(this).autocomplete("search", "");
            });
        }
        
function toggleFieldsForMNSandPrivateCloud(dropdown)
{
	var selectedValue = dropdown.options[dropdown.selectedIndex].value;
	if(selectedValue == "PrivateCloud")
	{
		$('#mnsID').hide();
		$('#privateCloudFieldsID').show();
		$('#forMNSServices').hide();
		$('#forPrivateCloud').show();
		$('#saasWhispirFieldsID').hide();
		$('#saasMandoeFieldsID').hide();
		$('#saasIPScapeFieldsID').hide();
	}	
	else if(selectedValue == "MNS")
	{
		$('#mnsID').show();
		$('#privateCloudFieldsID').hide();
		$('#forMNSServices').show();
		$('#forPrivateCloud').hide();
		$('#saasWhispirFieldsID').hide();
		$('#saasMandoeFieldsID').hide();
		$('#saasIPScapeFieldsID').hide();
	}
	else if(selectedValue == "Whispir")
	{
		$('#saasWhispirFieldsID').show();
		$('#mnsID').hide();
		$('#privateCloudFieldsID').hide();
		$('#forMNSServices').hide();
		$('#forPrivateCloud').hide();
		$('#saasMandoeFieldsID').hide();
		$('#saasIPScapeFieldsID').hide();
	}
	else if(selectedValue == "Mandoe")
	{
		$('#saasMandoeFieldsID').show();
		$('#mnsID').hide();
		$('#privateCloudFieldsID').hide();
		$('#forMNSServices').hide();
		$('#forPrivateCloud').hide();
		$('#saasWhispirFieldsID').hide();
		$('#saasIPScapeFieldsID').hide();
	}
	else if(selectedValue == "IPScape")
	{
		$('#saasIPScapeFieldsID').show();
		$('#mnsID').hide();
		$('#privateCloudFieldsID').hide();
		$('#forMNSServices').hide();
		$('#forPrivateCloud').hide();
		$('#saasWhispirFieldsID').hide()
		$('#saasMandoeFieldsID').hide();
	}
	else
	{
		$('#mnsID').hide();
		$('#privateCloudFieldsID').hide();
		$('#forMNSServices').hide();
		$('#forPrivateCloud').hide();
	}
}

function getVMList(dropdown)
{
	var selectedpcbs = dropdown.options[dropdown.selectedIndex].value;
	
	/* ajax call to get region list / business service */
	var VMURL = "${getVMURL}";
	var modifiedURL = VMURL + "&pcbs=" + selectedpcbs;
	
	$.ajax({
		type: "POST",
		url: modifiedURL,
		dataType: "json",
		success: function(data)
		{
			//alert("success");
			
			populateVMDropdown(data);
		},
		error: function()
		{
			//alert("error");
		}
	});
}

function getSaasWhispirComList(dropdown)
{
	var selectedSaasWhispirBs = dropdown.options[dropdown.selectedIndex].value;

	
	/* ajax call to get SaaS Resources */
	var saasWhispirResources = "${getSaasWhispirResources}";
	var modifiedURL = saasWhispirResources + "&saasWhispirBs=" + selectedSaasWhispirBs;
	
	$.ajax({
		type: "POST",
		url: modifiedURL,
		dataType: "json",
		success: function(data)
		{
			//alert("success");
			populateSaasWhispirResource(data);
		},
		error: function()
		{
			//alert("error");
		}
	});
}

function getSaasIPScapeComList(dropdown)
{
	var selectedSaasIPScapeBs = dropdown.options[dropdown.selectedIndex].value;

	
	/* ajax call to get SaaS Resources */
	var saasIPScapeResources = "${getSaasIPScapeResources}";
	var modifiedURL = saasIPScapeResources + "&saasIPScapeBs=" + selectedSaasIPScapeBs;
	
	$.ajax({
		type: "POST",
		url: modifiedURL,
		dataType: "json",
		success: function(data)
		{
			//alert("success");
			populateSaasIPScapeResource(data);

		},
		error: function()
		{
			//alert("error");
		}
	});
}

function getSaasMandoeComList(dropdown)
{
	var selectedSaasMandoeBs = dropdown.options[dropdown.selectedIndex].value;

	
	/* ajax call to get SaaS Resources */
	var saasMandoeResources = "${getSaasMandoeResources}";
	var modifiedURL = saasMandoeResources + "&saasMandoeBs=" + selectedSaasMandoeBs;

	$.ajax({
		type: "POST",
		url: modifiedURL,
		dataType: "json",
		success: function(data)
		{
			//alert("success");
			populateSaasMandoeResource(data);
		},
		error: function()
		{
			//alert("error");
		}
	});
}
	
function populateVMDropdown(jsonDataArray)
{
	//$('#category').append($('<option>').text(dataArray[j].campaignTitle).val(dataArray[j].campaignId));
	$('#virtualmachinesID option[value!="0"]').remove();
	//$('#virtualmachinesID').append($('<option>').text("--Select--").val('0'));
	
	for(var i=0; i<jsonDataArray.length; i++)
	{
		var jsonData = jsonDataArray[i];
		$('#virtualmachinesID').append($('<option>').text(jsonData.ciname).val(jsonData.ciid));
	}
}  

function populateSaasWhispirResource(jsonDataArray)
{
	//$('#category').append($('<option>').text(dataArray[j].campaignTitle).val(dataArray[j].campaignId));
	$('#saasWhispirComponentID option[value!="0"]').remove();
	//$('#virtualmachinesID').append($('<option>').text("--Select--").val('0'));
	
	for(var i=0; i<jsonDataArray.length; i++)
	{
		var jsonData = jsonDataArray[i];
		$('#saasWhispirComponentID').append($('<option>').text(jsonData.ciname).val(jsonData.ciid));
	}
}    

function populateSaasIPScapeResource(jsonDataArray)
{
	//$('#category').append($('<option>').text(dataArray[j].campaignTitle).val(dataArray[j].campaignId));
	$('#saasIPScapeComponentID option[value!="0"]').remove();
	//$('#virtualmachinesID').append($('<option>').text("--Select--").val('0'));
	
	for(var i=0; i<jsonDataArray.length; i++)
	{
		var jsonData = jsonDataArray[i];
		$('#saasIPScapeComponentID').append($('<option>').text(jsonData.ciname).val(jsonData.ciid));
	}
} 

function populateSaasMandoeResource(jsonDataArray)
{
	//$('#category').append($('<option>').text(dataArray[j].campaignTitle).val(dataArray[j].campaignId));
	$('#saasMandoeComponentID option[value!="0"]').remove();
	//$('#virtualmachinesID').append($('<option>').text("--Select--").val('0'));
	
	for(var i=0; i<jsonDataArray.length; i++)
	{
		var jsonData = jsonDataArray[i];
		$('#saasMandoeComponentID').append($('<option>').text(jsonData.ciname).val(jsonData.ciid));
	}
} 


</script>

<div class="modal fade tt-incident-modal" id="LogIncidentModal"
	role="dialog" data-backdrop="static" data-keyboard="false"
	aria-hidden="true" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" id="closeMarkID"
					data-dismiss="modal">

					<img class="closeMark"
						src="<%=request.getContextPath()%>/images/Close_G_24.png" />
				</button>
				<div class="modal-title logTitle">
					<liferay-ui:message key="LOG A NEW INCIDENT" />
				</div>
			</div>
			<div class="modal-body tt-modal-body" id="LogIncidentModalBody" >
				<form:form id="logIncForm" class="form-horizontal tt-modal-form"
					modelAttribute="logIncident" method="post">
					<div class="container-fluid tt-form-container">
						<div class="row-fluid tt-form-error-row">
							<div class="P1P2Disclaimer">
								<span class="pr-10 pull-left tt-warning"> <img
									class="disclaimerIcon"
									src="<%=request.getContextPath()%>/images/Discription_G_24.png" />
								</span>
								<p class="disclaimerText tt-warning-text">
								 	<liferay-ui:message key="${LogIncidentDisclaimerMsg}"/>
								</p>
							</div>
							
						</div>
						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">
								<liferay-ui:message key="Request Summary:" />
							</div>
							<div class="span9 tt-form-right-col">
								<form:input path="summary" name="summary" type="text"
									class="textBoxLogInc" tabindex="1"/>
							</div>
							
						</div>
						
						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">
								<liferay-ui:message key="Product Type:" />
							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="productType" name="producttype" id="producttypeId" 
									class="textBoxLogInc" onchange="toggleFieldsForMNSandPrivateCloud(this)" tabindex="2">
									<form:option value="0" label="--Select--"/>
									<form:option value="MNS" label="MNS"/>
									<form:option value="PrivateCloud" label="Private Cloud"/>
									<form:option value="Whispir" label="Whispir"/>
									<form:option value="IPScape" label="IPScape"/>
									<form:option value="Mandoe" label="Mandoe"/>									
								</form:select>
							</div>
							
						</div>

						<div id="mnsID" style="display:none">
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="Region:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="region" name="region" id="regionID"
										items="${regionList}" class="selectBoxLogInc"
										onchange="getSite(this)" tabindex="3">
										<form:option value="NONE" label="--- Select ---" />
									</form:select>
								</div>
							</div>
	
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="Site Name:" />
								</div>
								<div class="span9 tt-form-right-col">
									 <div>
									<form:input path="siteid" name="siteid" tabindex="4" type="text" class="siteSelectAutoComplete" id="siteIDAuto" onblur="getServiceLI()" onkeyup="getServiceLI()"/>
									<%--		
									<div>
										<select  id="siteID" name="siteid" onchange="getServiceLI(this)" style="display: none;"><option value="0"></option></select>
									</div>	
									--%>
									</div>
								</div>
							</div>
							
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="Service Name:" />
								</div>
								<div class="span9  tt-form-right-col">
									<form:select path="serviceid" name="serviceid"
										id="serviceNameSelect" items="${serviceIDList}"
										class="selectBoxLogInc" tabindex="5">
										<%-- <form:option value="NONE" label="--- Select ---"/> --%>
									</form:select>
								</div>
							</div>
						</div>
						
						<div id="privateCloudFieldsID" style="display:none">
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="Private Cloud Business Service:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="privateCloudBusinessService" name="pcbs" id="pcbs"
										items="${pcbsList}" class="selectBoxLogInc"
										onchange="getVMList(this)" tabindex="3">
											<form:options items="${pcbsList}" itemValue="${pcbsList.value}" itemLabel="${pcbsList.key}" />
									</form:select>
								</div>
							</div>
	
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="Virtual Machines:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="virtualMachines" name="virtualmachines" id="virtualmachinesID"
										class="selectBoxLogInc"
										onchange="" tabindex="4">
										<form:option value="0" label="--Select--"/>
									</form:select>
								</div>
							</div>
						</div>
						
						<!-- start Additional field for SaaS Whispir product -->						
						<div id="saasWhispirFieldsID" style="display:none">
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="SaaS Business Service:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="privateCloudBusinessService" name="saasWhispirBs" id="saasWhispirBs" items="${saasWhispirBsList}" class="selectBoxLogInc" onchange="getSaasWhispirComList(this)" tabindex="3">
											<form:options items="${saasMandoeBs}" itemValue="${saasWhispirBsList.value}" itemLabel="${saasWhispirBsList.key}" />
									</form:select>
								</div>
							</div>
							
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="SaaS Component:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="virtualMachines" name="saasWhispirComponents" id="saasWhispirComponentID" class="selectBoxLogInc" onchange="" tabindex="4">
										<form:option value="0" label="--Select--"/>
									</form:select>
								</div>
							</div>
						</div>						
						<!-- end Additional field for SaaS Whispir product -->
						<!-- start Additional field for SaaS IPScape product -->						
						<div id="saasIPScapeFieldsID" style="display:none">
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="SaaS Business Service:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="privateCloudBusinessService" name="saasIPScapeBs" id="saasIPScapeBs" items="${saasIPScapeBsList}" class="selectBoxLogInc" onchange="getSaasIPScapeComList(this)" tabindex="3">
											<form:options items="${saasIPScapeBsList}" itemValue="${saasIPScapeBsList.value}" itemLabel="${saasIPScapeBsList.key}" />
									</form:select>
								</div>
							</div>
							
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="SaaS Component:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="virtualMachines" name="saasIPScapeComponents" id="saasIPScapeComponentID" class="selectBoxLogInc" onchange="" tabindex="4">
										<form:option value="0" label="--Select--"/>
									</form:select>
								</div>
							</div>
						</div>						
						<!-- end Additional field for SaaS IPScape product -->
						<!-- start Additional field for SaaS Mandoe product -->						
						<div id="saasMandoeFieldsID" style="display:none">
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="SaaS Business Service:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="privateCloudBusinessService" name="saasMandoeBs" id="saasMandoeBs" items="${saasMandoeBsList}" class="selectBoxLogInc" onchange="getSaasMandoeComList(this)" tabindex="3">
											<form:options items="${saasMandoeBsList}" itemValue="${saasMandoeBsList.value}" itemLabel="${saasMandoeBsList.key}" />
									</form:select>
								</div>
							</div>
							
							<div class="row-fluid tt-form-row">
								<div class="span3 tt-form-left-col">
									<liferay-ui:message key="SaaS Component:" />
								</div>
								<div class="span9 tt-form-right-col">
									<form:select path="virtualMachines" name="saasMandoeComponents" id="saasMandoeComponentID" class="selectBoxLogInc" onchange="" tabindex="4">
										<form:option value="0" label="--Select--"/>
									</form:select>
								</div>
							</div>
						</div>						
						<!-- end Additional field for SaaS Mandoe product -->
						
						<div class="row-fluid tt-form-row" id="forMNSServices" style="display:none">
							<div class="span9 offset3 tt-form-right-col"
								id="dashTickCheckBox">
								<%-- <form:checkbox path="MIM" value=""/> --%>
								<form:checkbox path="mimCheck" id="MultiServiceCheckBox" tabindex="6"/>
									
								<div class="checkBoxTextLogInc">
									<span><liferay-ui:message key="Multiple services are impacted" /></span> 
									<span class="tt-checkbox-optional">
										<liferay-ui:message key="(Add details in description)" />
									</span>
								</div>
							</div>
						</div>
						
						<div class="row-fluid tt-form-row" id="forPrivateCloud" style="display:none">
							<div class="span9 offset3 tt-form-right-col"
								id="dashTickCheckBox">
								<%-- <form:checkbox path="MIM" value=""/> --%>
								<form:checkbox path="mimCheck" id="MultiServiceCheckBox" tabindex="6"/>
									
								<div class="checkBoxTextLogInc">
									<span><liferay-ui:message key="Multiple VMs are impacted" /></span> 
									<span class="tt-checkbox-optional">
										<liferay-ui:message key="(Add details in description)" />
									</span>
								</div>
							</div>
						</div>
						
						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">
								<div>
									<liferay-ui:message key="Impact:" />
									<div class="helpIconDiv">
										<a href="#" data-toggle="tooltip" data-placement="top"
											title="<liferay-ui:message key="${ImpactDescription}"/>"> <%-- data-toggle="tooltip" title="${ImpactDescription}" --%>
											<%-- data-placement='bottom' rel="popover" data-original-title="Impact Description" data-content="${ImpactDescription}" data-container="body" --%>
											<img class="helpIconImg"
											src="<%=request.getContextPath()%>/images/Discription_G_16.png">
										</a>
									</div>
								</div>
							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="impact" name="impact"
									items="${impactListMap}" class="selectBoxLogInc" tabindex="7">
									<form:option value="NONE" label="--- Select ---"/>
								</form:select>
							</div>
						</div>
						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">
								<div>
									<liferay-ui:message key="Urgency:" />
									<div class="helpIconDiv">
										<a href="#" data-toggle="tooltip" data-placement="top"
											title="<liferay-ui:message key="${UrgencyDescription}"/>"> <img class="helpIconImg"
											src="<%=request.getContextPath()%>/images/Discription_G_16.png">
										</a>
									</div>
								</div>
							</div>
							<div class="span9 tt-form-right-col">
								<form:select path="urgency" name="urgency"
									items="${urgencyListMap}" class="selectBoxLogInc" tabindex="8">
									<form:option value="NONE" label="--- Select ---" />
								</form:select>
							</div>
						</div>
						<div class="row-fluid tt-form-row">
							<div class="span3 tt-form-left-col">
								<liferay-ui:message key="Description:" />
							</div>
							<div class="span9 tt-form-right-col">
								<form:textarea path="worklognotes" name="worklognotes" rows="4"
									cols="100" id="dashTickNotesTextArea" tabindex="9"/>
							</div>
						</div>
						
						<div class="row-fluid tt-form-last-row pr-15 pt-30">
							<div class="span12 tt-submit-row">
								<span class="tt-cancel pr-10">
									<button type="button" class="btn-secondary-tt img-cancel"
										id="cancelButtonID" data-dismiss="modal" tabindex="11">
										<liferay-ui:message key="Cancel" />
									</button>
								</span> 
								<span class="tt-submit">
<%
 	if (allowAccess) {
%>

									<button type="submit" class="btn-primary-tt img-submitrequestenabled"
										id="submitRequestButtonID" tabindex="10">
										<liferay-ui:message key="Submit Request" />
									</button> 
								
<%
 	} else {
%>

									<button type="button" class="btn-primary-tt img-submitrequest"
										id="submitRequestButtonID"
										title="Contact Administrator" tabindex="10">
										<liferay-ui:message key="Submit Request" />
									</button> 
									
<%
 	}
%>
								</span>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="logIncSubmitResponse" data-backdrop="static"
	data-keyboard="false">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">
			<img class="closeMark"
				src="<%=request.getContextPath()%>/images/Close_G_24.png" />
		</button>
		<div class="modal-title" id="responseStatus"></div>
	</div>
	<div class="modal-body tt-modal-body">
		<div id="incidentID"></div>
	</div>
	<div class="modal-footer tt-submit-row">
		<button type="button" class="closeButton" class="btn"
			data-dismiss="modal">
			<div class="buttonIcon">
				<img src="<%=request.getContextPath()%>/images/Close_W_16.png" />
			</div>
			<div class="buttonText">
				<liferay-ui:message key="Close" />
			</div>
		</button>
	</div>
</div>
