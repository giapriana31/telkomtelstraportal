<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="datatableURI" var="datatableURI"></portlet:resourceURL>
<portlet:resourceURL id="getServiceRequestDetails" var="getServiceRequestDetails"></portlet:resourceURL>
<portlet:resourceURL id="getServiceRequestItemDetails" var="getServiceRequestItemDetails"></portlet:resourceURL>

<html>
<head>

<style>
@media (max-width: 500px){
button#srListSearchBtn {
    margin-bottom: 0px;
}
.tt-create-service-ctr{
padding-left: 0px !important;
padding-right: 0px !important;
}
.rwd{
		display:none !important;
	}
td:nth-child(2){
display:none;
}
th:nth-child(2){
display:none;
}
td:nth-child(3){
display:none;
}
th:nth-child(3){
display:none;
}
td:nth-child(4){
display:none;
}
th:nth-child(4){
display:none;
}
}
@media (min-width: 501px) and (max-width: 767px){
	button#srListSearchBtn {
    margin-bottom: 0px;
}
.tt-create-service-ctr{
padding-left: 0px !important;
padding-right: 0px !important;
}
.wrapper updateInfoHolder{
display:none !important;
	}
	.tt-create-service-ctr {
    width: 0% !important;
    float: right !important;
}
tt-service-br-header{
	margin-top:0px !important;
	
}
img{
	margin-top:0px !important;
	
}
}

#srHeaderDiv {
  padding: 20px 0 20px 20px;
  background-color: #fff;
  border-bottom: solid 1px #ccc;
  margin-bottom: 20px;
}
#pvsr-tt-icon{
	display:inline;
}
#pvsr-tt-header {
  display: inline;
  font-size: 20px;
  color: #333;
  padding-left: 10px;
  font-weight: bold;
}

.dataTables_wrapper .dataTables_paginate .paginate_button.disabled, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
  cursor: default;
  color: #555 !important;
  border: 1px solid transparent;
  background: transparent;
  box-shadow: none;
  border: solid transparent !important;
}

div#listingDatatableDiv {
  padding: 15px;
}

table#listingDatatable {
  background-color: rgb(204, 204, 204);
}

#listingDatatable td {
  background-color: #fff;
  height: 45px;
}

a.paginate_button.disabled:hover {
  border-bottom: solid transparent !important;
  background-color: transparent !important;
}

a.paginate_button:hover {
  border-bottom: solid 2px #f00 !important;
  background-color: transparent !important;
}

a.paginate_button.current {
  border-bottom: solid 2px red !important;
}

button.normalSearch {
	background-image: url("../images/srch.png");
	height: 30px;
	width: 30px;
	margin-bottom: 10px;
	border-style: none;
}

.srlSearchBox {
	float: right;
}

.filterbox{
	display:inline-block;
	padding:5px;
	margin: 0 5px 0 5px;
	border: solid 1px #ccc;
}

</style>

<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/fnReloadAjax.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/jquery.dataTables.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/ttservicerequest.css">	
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/serviceRequestDetails.css">	
<script>

$.fn.dataTable.pipeline = function(opts) {
		// Configuration options
		var conf = $.extend({
			pages : 3, // number of pages to cache
			url : '', // script url
			data : null, // function or object with parameters to send to the server
			// matching how `ajax.data` works in DataTables
			method : 'GET' // Ajax HTTP method
		}, opts);

		// Private variables for storing the cache
		var cacheLower = -1;
		var cacheUpper = null;
		var cacheLastRequest = null;
		var cacheLastJson = null;

		return function(request, drawCallback, settings) {
			var ajax = false;
			var requestStart = request.start;
			var drawStart = request.start;
			var requestLength = request.length;
			var requestEnd = requestStart + requestLength;

			if (settings.clearCache) {
				// API requested that the cache be cleared
				ajax = true;
				settings.clearCache = false;
			} else if ((cacheLower < 0) || (requestStart < cacheLower)
					|| (requestEnd > cacheUpper)) {
				// outside cached data - need to make a request
				ajax = true;
			} else if (JSON.stringify(request.order) !== JSON
					.stringify(cacheLastRequest.order)
					|| JSON.stringify(request.columns) !== JSON
							.stringify(cacheLastRequest.columns)
					|| JSON.stringify(request.search) !== JSON
							.stringify(cacheLastRequest.search)) {
				// properties changed (ordering, columns, searching)
				ajax = true;
			}

			// Store the request for checking next time around
			cacheLastRequest = $.extend(true, {}, request);

			if (ajax) {
				// Need data from the server
				if (requestStart < cacheLower) {
					requestStart = requestStart
							- (requestLength * (conf.pages - 1));

					if (requestStart < 0) {
						requestStart = 0;
					}
				}

				cacheLower = requestStart;
				cacheUpper = requestStart + (requestLength * conf.pages);

				request.start = requestStart;
				request.length = requestLength * conf.pages;

				// Provide the same `data` options as DataTables.
				if ($.isFunction(conf.data)) {
					// As a function it is executed with the data object as an arg
					// for manipulation. If an object is returned, it is used as the
					// data object to submit
					var d = conf.data(request);
					if (d) {
						$.extend(request, d);
					}
				} else if ($.isPlainObject(conf.data)) {
					// As an object, the data given extends the default
					$.extend(request, conf.data);
				}

				settings.jqXHR = $.ajax({
					"type" : conf.method,
					"url" : conf.url,
					"data" : request,
					"dataType" : "json",
					"cache" : false,
					"success" : function(json) {
						cacheLastJson = $.extend(true, {}, json);

						if (cacheLower != drawStart) {
							json.data.splice(0, drawStart - cacheLower);
						}

						json.data.splice(requestLength, json.data.length);

						drawCallback(json);
					}
				});
			} else {
				json = $.extend(true, {}, cacheLastJson);
				json.draw = request.draw; // Update the echo for each response
				json.data.splice(0, requestStart - cacheLower);
				json.data.splice(requestLength, json.data.length);

				drawCallback(json);
			}
		}
	};

	//Register an API method that will empty the pipelined data, forcing an Ajax
	//fetch on the next draw (i.e. `table.clearPipeline().draw()`)
	$.fn.dataTable.Api.register('clearPipeline()', function() {
		return this.iterator('table', function(settings) {
			settings.clearCache = true;
		});
	});
	

function populateTable(datatableURL) {	

	console.log(datatableURL);

	//DataTables initialisation
	var oTable=$('#listingDatatable').dataTable({
		"processing" : true,
		"serverSide" : true,
		"bDestroy": true,			
		'bInfo' : true,
		"oLanguage": {
		      "sInfo": '<liferay-ui:message key="Showing"/> _START_-_END_ <liferay-ui:message key="of"/> _TOTAL_',
		      "sInfoEmpty": '<liferay-ui:message key="Showing"/> 0-0 <liferay-ui:message key="of"/> 0',
		      "sEmptyTable": '<liferay-ui:message key="No data available in table"/>',
			  "oPaginate": {
		        "sPrevious": '&lt;&nbsp;<liferay-ui:message key="Previous"/>',
				"sNext": '<liferay-ui:message key="Next"/>&nbsp;&gt;'
		      }
			  
		    },
		'bFilter' : false,
		'bLengthChange' : false,

		"ajax" : $.fn.dataTable.pipeline({
			url : datatableURL,
			pages : 3
		// number of pages to cache
		})
	});	
	oTable.fnSort( [ [3,'desc'] ] );
}

	function getSingleServiceRequestDetails(obj) {
		/* alert('Clicked ' + obj.getAttribute("id")); */
		var requestIdReceived = obj.getAttribute("id");
		sendParametersForSRDetails(requestIdReceived);
	}
	
	function ServiceRequestPortlet_getSRIList(data) {

		
		var dataSet = data.datatableServiceRequestItems.data;

		//DataTables initialisation
		$('#sriListingTable').dataTable({
		
			"processing" : false,
			"serverSide" : false,
			'bInfo' : false,
			"bPaginate": false,
			"bSortable" : false,
			"bSort": false, 
            "aaSorting": [[0]], 
			'bFilter' : false,
			'bDestroy' : true,
			'bLengthChange' : false,
			"aoColumns" : [ {
				"sClass": "tt-sr-adjCol-itemid-th","sWidth" : "15%"
			}, // 1st column width 
			{
				"sClass": "tt-sr-adjCol-category-th","sWidth" : "10%"
			}, // 2nd column width 
			{
				"sClass": "tt-sr-adjCol-request-th","sWidth" : "25%"
			}, // 3rd column width and so on 
			{
				"sClass": "tt-sr-adjCol-type-th","sWidth" : "15%"
			}, {
				"sClass": "tt-sr-adjCol-status-th","sWidth" : "12%"
			}, {
				"sClass": "tt-sr-adjCol-lastupdatedon-th","sWidth" : "18%"
			}, {
				"sWidth" : "5%"
			} ],
			"data": dataSet
	
		});
		}
	
	function sendParametersForSRDetails(requestIdParameter){
		
		var getRequestDetailsURL = "${getServiceRequestDetails}";
		clearAllRequestFilelds();
		
		var jsonObj = {
			"requestid" : requestIdParameter,
		};

		var serviceRequestData = {
			serviceRequestFilterIdentifier : JSON.stringify(jsonObj)
		};

		$.ajax({
					type : "POST",
					url : getRequestDetailsURL,
					dataType : 'json',
					data : serviceRequestData,
					success : function(data) {
					
						var jsonDATA;
						try {
							jsonDATA = jQuery.parseJSON(JSON.stringify(data));
						} catch (e) {
							/* alert(e); */
						}
						
						PopulateAllRequestFilelds(jsonDATA);						
						
						$("#tt-srd-container").slideToggle();
						$("#listingDatatableDiv").css("display","none");
						
						ServiceRequestPortlet_getSRIList(data);

					},
					error : function(data) {
					}

				});

	}
	
	function clearAllRequestFilelds(){		
		$("#srdetails-summary").html("");
		$("#tt-srd-srID").html("");
		$("#tt-srd-status").html("");
		$("#tt-srd-requestor").html("");
		$("#tt-srd-lastUpdated").html("");
		$("#tt-srd-createdOn").html("");
		$("#tt-srd-startDate").html("");
		$("#tt-srd-endDate").html("");
		$("#tt-srd-requestType").html("");
		$("#tt-srd-outagePresent").html("");
		$("#tt-srd-outageStartTime").html("");
		$("#tt-srd-outageEndTime").html("");		
		$("#tt-srd-description").html("");	
		
		$("#tt-srd-risk").html("");
		$("#tt-srd-priority").html("");
		$("#tt-srd-impact").html("");
		$("#tt-srd-caseManager").html("");
		$("#tt-srd-resolverGroup").html("");
		$("#tt-srd-comments").html("");
		$("#tt-sri-commentTable-td").html("");
		$("#tt-srd-siteName").html("");
		$("#tt-srd-category").html("");
		$("#tt-srd-changeSource").html("");
		$("#tt-srd-changeTypeTT").html("");
		$("#tt-srd-changeTypeCustomer").html("");
		$("#tt-srd-deviceName").html("");
		$("#tt-srd-serviceName").html("");
		
		$("#tt-srd-requestedByDate").html("");
		$("#tt-srd-telkomtelstraapprover").html("");
		$("#tt-srd-telkomtelstraapproverdate").html("");
		$("#tt-srd-telkomtelstraapprovergroup").html("");
		$("#tt-srd-customerapprover").html("");
		$("#tt-srd-customerapproverdate").html("");
		$("#tt-srd-outcomestatus").html("");
	}
	
	function PopulateAllRequestFilelds(jsonDATA,requestType){
		$("#srdetails-summary").html(jsonDATA.summary);
		$("#tt-srd-srID").html(jsonDATA.serviceRequestId);
		$("#tt-srd-status").html(jsonDATA.status);
		
		$("#tt-srd-requestor").html(jsonDATA.requestor);
		$("#tt-srd-lastUpdated").html(jsonDATA.lastUpdated);
		$("#tt-srd-createdOn").html(jsonDATA.createdOn);
		$("#tt-srd-startDate").html(jsonDATA.startDate);
		$("#tt-srd-endDate").html(jsonDATA.endDate);
		$("#tt-srd-requestType").html(jsonDATA.requestType);
		
	
	}
	
	var getServiceRequestItemDetailsURL = "${getServiceRequestItemDetails}";

	function getSingleServiceRequestItemDetails(obj) {
		/* alert('Clicked ' + obj.getAttribute("id")); */
		var getServiceRequestItemDetailsURL = "${getServiceRequestItemDetails}";
		var itemIdReceived = obj.getAttribute("id");

		var jsonObj = {
			"itemid" : itemIdReceived
		};

		var serviceRequestItemData = {
			serviceRequestItemFilterIdentifier : JSON.stringify(jsonObj)
		};
		var commentAppendStr1='<table style="margin: 3px; width: 99%"><tr><td rowspan="2" style="width: 45px; padding:0px !important"><div><img src="<%=request.getContextPath()%>/images/tickets/u33.png" height="40" width="40"></img></div></td><td style="padding:0px !important"><p style="float: left; color: #e32212; margin-bottom: 0px;">';
		var commentAppendStr2 = '</p><p class="mediasr" style="margin-bottom: 0px;">';
		var commentAppendStr3 = '</p></td></tr><tr><td style="padding: 0px !important">';
		var commentAppendStr4 = '</td></tr></table>';
		document.getElementById("tt-srd-ritmid").innerHTML = "";
		document.getElementById("tt-srd-serviceName-item").innerHTML = "";
		document.getElementById("tt-srd-siteName-item").innerHTML = "";
		document.getElementById("tt-srd-deviceName-item").innerHTML = "";
		document.getElementById("tt-sri-commentTable-td-item").innerHTML = "";

		$.ajax({
					type : "POST",
					url : getServiceRequestItemDetailsURL,
					dataType : 'json',
					data : serviceRequestItemData,
					success : function(data) {
						var jsonDATA = data;

						document.getElementById("tt-srd-ritmid").innerHTML = jsonDATA.ritmId;
						document.getElementById("tt-srd-serviceName-item").innerHTML = jsonDATA.serviceName;
						document.getElementById("tt-srd-siteName-item").innerHTML = jsonDATA.siteName;
						/* document.getElementById("tt-srd-deviceName").innerHTML = jsonDATA.requestor; */
						var deviceNameList = jsonDATA.deviceNameList;
						for (var i = 0; i < deviceNameList.length; i++) {
							/* alert('Device '+deviceNameList[i]); */
							document.getElementById("tt-srd-deviceName-item").innerHTML += deviceNameList[i]
									+ "<br>";
						}
						var commentList = jsonDATA.commentsList;
						for (var i = 0; i < commentList.length; i++) {
							/* alert('Comments '+commentsList[i]); */
							document.getElementById("tt-sri-commentTable-td-item").innerHTML += commentAppendStr1
									+ commentList[i].userId
									+ commentAppendStr2
									+ commentList[i].modifiedDate
									+ commentAppendStr3
									+ commentList[i].notes
									+ commentAppendStr4;
						}
						$('#ritmModal').modal('show');

					},
					error : function(data) {

					}

				});

	}

$(function() {

	$('#tt-sr-normalSearchForm').submit(function(e) {
				e.preventDefault();
				
			});
});

$("#srListSearchBtn").click(function () {
			
		 //TODO:needs to take from props.
		//append the search term and filter the results 
		populateTable(datatableURL+"&searchTerm=" + $('#srListSearchTxt').val());
});

$(document).ready(function() {
	var datatableURL="${datatableURI}";

	
	$('#tt-toggle-srlisting').click(function() {
		$("#tt-srlisting-container").css("display", "none");
		$("#tt-srd-container").slideToggle();
	});

	$('#tt-toggle-srdetails').click(function() {
		$("#tt-srd-container").css("display", "none");
		$("#listingDatatableDiv").slideToggle();
		$(".update-data-tab").slideToggle();
		$("#updateDataHolderDiv").slideToggle();
		$(".tt-morecrdetails-container").hide();
		$("#activeTab").removeClass("displayNoneImportant");
		$("#completedTab").removeClass("displayNoneImportant");
	});
	
	$('#ritmModal').css('display', "none");
	
	/*$('#ritmModal').on( 'shown', function ( event ) {
		$('#ritmModal').css('display', "block");
		$('#_145_dockbar').css('z-index', 1039);
		$('body').addClass('modal-open');
	});
*/	$('#ritmModal').on( 'hidden', function ( event ) {
		$('#ritmModal').css('display', "none");
		$('.modal-backdrop').remove();
		$('#_145_dockbar').css('z-index', 1041);
		$('body').removeClass('modal-open');
	});

	
	$("#srListSearchBtn").click(function () {
			
		 //TODO:needs to take from props.
		//append the search term and filter the results 
		populateTable(datatableURL+"&searchTerm=" + $('#srListSearchTxt').val());
	});
	populateTable(datatableURL);

});

</script>
</head>
<body>
<div id="listingDatatableDiv">

	<div class="srlSearchBox"><form id="tt-sr-normalSearchForm">
				<input id="srListSearchTxt"  
					type="search" class="" placeholder="" aria-controls="listingDatatable">
					<button type="submit" id="srListSearchBtn" class="normalSearch"></button>
					<button type="button" id="srListExportBtn" class="btn-primary-tt backButton"><liferay-ui:message key="Export" /></button>
				</form>
	</div>

<%
        String exportToExcel = request.getParameter("exportToExcel");
        if (exportToExcel != null
                && exportToExcel.toString().equalsIgnoreCase("YES")) {
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "inline; filename="
                    + "excel.xls");
 
        }
    %>
    <table align="left" border="2">
        <thead>
            <tr bgcolor="lightgreen">
                <th>Sr. No.</th>
                <th>Text Data</th>
                <th>Number Data</th>
            </tr>
        </thead>
        <tbody>
            <%
                for (int i = 0; i < 10; i++) {
            %>
            <tr bgcolor="lightblue">
                <td align="center"><%=i + 1%></td>
                <td align="center">This is text data <%=i%></td>
                <td align="center"><%=i * i%></td>
            </tr>
            <%
                }
            %>
        </tbody>
    </table>
    <%
        if (exportToExcel == null) {
    %>
    <a href="/group/project-view/pvservice-request?exportToExcel=YES">Export to Excel</a>
    <%
        }
    %>
	<table id="listingDatatable" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th id="tt-dev-adj-requestid" class="tt-dev-adjCol-requestid"><liferay-ui:message key="Request Id" /></th>						
						<th id="tt-dev-adj-subject" class="tt-dev-adjCol-subject"><liferay-ui:message key="Request Title" /></th>
						<th id="tt-dev-adj-lastupdate" class="tt-dev-adjCol-lastupdate"><liferay-ui:message key="Last Update" /></th>
						<th id="tt-dev-adj-requestor" class="tt-dev-adjCol-requestor"><liferay-ui:message key="Requestor" /></th>
						<th id="tt-dev-adj-status" class="tt-dev-adjCol-status"><liferay-ui:message key="Status" /></th>
						
					</tr>
				</thead>
	</table>
	
</div>
<div id="tt-srd-container">
	<div class="tt-grayContainer">
		<!--Header Start-->
		<div class="tt-SRHeader srColumn">
			<span class="span12"> <liferay-ui:message key="Service Request Details" /></span>
		</div>


		<!--Header End-->
		<!--Back Button Start-->
		<div class="tt-buttonHolder">
			<button type="button" id="tt-toggle-srdetails"
				class="img-back-arrow btn-primary-tt" id="backButtonID">
				<liferay-ui:message key="Back to Request List" />
			</button>
		</div>
		<!--Back Button End-->
		<div class="tt-srdeatils-container-holder">
			<div class="tt-srdetails-container">
				<div class=" tt-srdetails-header span12">
					<span class="tt-srdetails-summary" id="srdetails-summary"></span>
				</div>
				
 				<!-- Main Details of Change Request and Service Request -->
				<div class="tt-lesssrcrdetails-container">
					<table class="table tt-table-overflow">
						<tbody>

							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle srColumn">
										<liferay-ui:message key="Service Request Id:" />
									</div>
									<div id="tt-srd-srID" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Status:" />
									</div>
									<div id="tt-srd-status" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Requestor:" />
									</div>
									<div id="tt-srd-requestor" class="tt-srd-dataStyle"></div>
								</td>
							</tr>

							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Last Updated On:" />
									</div>
									<div id="tt-srd-lastUpdated" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle">
										<liferay-ui:message key="Request Created On:" />
									</div>
									<div id="tt-srd-createdOn" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Requested By Date:" />
									</div>
									<div id="tt-srd-requestedByDate" class="tt-srd-dataStyle"></div>
								</td>
							</tr>

							<tr>
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Start Date:" />
									</div>
									<div id="tt-srd-startDate" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Expected End Date:" />
									</div>
									<div id="tt-srd-endDate" class="tt-srd-dataStyle"></div>
								</td>
								<td class="" width="31%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="Request Type:" />
									</div>
									<div id="tt-srd-requestType" class="tt-srd-dataStyle"></div>
								</td>
							</tr>

							<tr>
								<td class="TTData" width="34%">
									<div class="tt-srd-dataHeaderStyle ">
										<liferay-ui:message key="telkomtelstra Approver:" />
									</div>
									<div id="tt-srd-telkomtelstraapprover" class="tt-srd-dataStyle"></div>
								</td>
								<!--  new added -->
								<td class="CustomerData" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
										
									</div>
									<div id="tt-srd-customerapprover" class="tt-srd-dataStyle crColumn"></div>
								</td>
								
								<!--  new added -->
								<td class="TTData" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
									</div>
									<div id="tt-srd-telkomtelstraapproverdate" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<!--  new added -->
								<td class="CustomerData" width="35%">
									<div class="tt-srd-dataHeaderStyle ">
									</div>
									<div id="tt-srd-customerapproverdate" class="tt-srd-dataStyle"></div>
								</td>
								
								<!--  new added -->
								<td class="TTData" width="31%">
									<div class="tt-srd-dataHeaderStyle ">
									</div>
									<div id="tt-srd-telkomtelstraapprovergroup" class="tt-srd-dataStyle"></div>
								</td>
								<td class="CustomerData" width="31%">
									<div class="tt-srd-dataHeaderStyle crColumn">
									</div>
									<div id="tt-srd-changeTypeCustomer" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>
							
							<tr>
								<td class="TTData" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
									</div>
									<div id="tt-srd-changeTypeTT" class="tt-srd-dataStyle crColumn"></div>
								</td>
							</tr>

							<tr class="">
								<td class="" width="34%">
									<div class="tt-srd-dataHeaderStyle crColumn">
									</div>
									<div id="tt-srd-siteName" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="" width="35%">
									<div class="tt-srd-dataHeaderStyle crColumn">
									</div>
									<div id="tt-srd-category" class="tt-srd-dataStyle crColumn"></div>
								</td>
								<td class="">
									<div class="tt-srd-dataHeaderStyle crColumn">
										<div class="span12">
											
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td class="" colspan="3">
									<div class="tt-srd-sri-dataHeader srColumn">
										<span class="tt-srd-dataHeaderStyle tt-srdsri-HeaderStyle">
										</span>
									</div>
									<div class="sritmTableBox srColumn">
										<table id="sriListingTable" class="display" cellspacing="0"	width="100%">
											<thead>
												<tr>
													<th id="tt-sr-itemid-th" class="tt-sr-adjCol-itemid-th"><liferay-ui:message key="Item Id" /></th>
													<th id="tt-sr-category-th" class="tt-sr-adjCol-category-th"><liferay-ui:message key="Category" /></th>
													<th id="tt-sr-request-th" class="tt-sr-adjCol-request-th"><liferay-ui:message key="Request" /></th>
													<th id="tt-sr-type-th" class="tt-sr-adjCol-type-th"><liferay-ui:message key="Type" /></th>
													<th id="tt-sr-status-th" class="tt-sr-adjCol-status-th"><liferay-ui:message key="Status" /></th>
													<th id="tt-sr-lastupdatedon-th" class="tt-sr-adjCol-lastupdatedon-th"><liferay-ui:message key="Last Updated On" /></th>
													<th id="tt-sr-link-th"></th>
												</tr>
											</thead>
										</table>
									</div>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				
 				
			</div>
		</div>
	</div>
	<div class="modal fade" id="ritmModal" data-backdrop="static"
	data-keyboard="false">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">
			<img class="closeMark"
				src="<%= request.getContextPath()%>/images/Close_G_24.png" />
		</button>
		<div id="tt-ritm-header">
			<liferay-ui:message key="ITEM DETAILS" />
		</div>
	</div>
	<div class="modal-body">
		<table class="table tt-table-overflow">
			<tr>
				<td>
					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Item Id:" />
					</div>
					<div id="tt-srd-ritmid" class="tt-srd-dataStyle"></div>

					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Service Name:" />
					</div>
					<div id="tt-srd-serviceName-item" class="tt-srd-dataStyle"></div>

					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Site Name:" />
					</div>
					<div id="tt-srd-siteName-item" class="tt-srd-dataStyle"></div>
				</td>
				<td>
					<div class="tt-srd-dataHeaderStyle">
						<liferay-ui:message key="Device Name:" />
					</div>
					<div id="tt-srd-deviceName-item" class="tt-srd-dataStyle tt-srd-sri-deviceNameStyle"></div>
				</td>
			</tr>
		</table>

		<div class="tt-srd-dataHeaderStyle tt-sritem-commentHeaderStyle">
			<liferay-ui:message key="Comments:" />
		</div>
		<div class="tt-sritem-table-holder">
			<table class="table tt-sritem-table" id="sritem-table">
				<tr>
					<td rowspan="11" id="tt-sri-commentTable-td-item">
						
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="img-OK btn-primary-tt" id="OKButtonID"
			data-dismiss="modal">
			<liferay-ui:message key="OK" />
		</button>
	</div>
</div>
</div>
</body>
</html>