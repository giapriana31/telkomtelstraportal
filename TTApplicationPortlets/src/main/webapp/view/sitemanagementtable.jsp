<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link rel="stylesheet" href="<%=request.getContextPath() %>/css/sitemanagementtable.css" />

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="siteManagementTableURL" var="siteManagementTableURL"/>
<style>
@media ( max-width : 500px) {
	td+td+td+td+td {
		display: none;
	}
	th+th+th+th+th {
		display: none;
	}
	.tt-subtitle{
	display:none !important;
	}
	.search{
	width: 29px!important;
    height: 29px!important;
	}
	
	#displayRegionFilter{
	margin-left:10px !important;
	}
}
@media (min-width: 501px) and (max-width: 767px){

 #displayRegionFilter{
	margin-left:10px !important;
	}
}
.header expand{
margin: 5px;
display: flex;
}
/* #displayRegionFilter_sitemg{
float: right;
margin-top: 5px;
margin-left: 15%;
padding-left: 68%;
} */

</style>

<script>
var selectedRegion = ['all'];
var searchTerm = null;
$(document).ready(function() {
	
	var bvUrl = window.location.href;
	
	$("input[type='checkbox']").change(function() {
		//alert("start");
		selectedRegion = [];
		$('div#displayRegionFilter input[type=checkbox]').each(function() {
		   if ($(this).is(":checked")) {
			   selectedRegion.push($(this).attr('id'));
		   }
		});
		//alert("servicesIdArray is::"+selectedRegion);
		
		if(bvUrl.includes('bvdetails'))
		{
			siteManagementTableAjax2();
		}
		else
		{	
			siteManagementTableAjax();
		}
	});
	
	
	$("#normalSearchTerm" ).on( "keydown", function(event) {
		if(event.which == 13) 
		{
			searchTerm = $('#normalSearchTerm').val();
			if(bvUrl.includes('bvdetails'))
			{
				siteManagementTableAjax2();
			}
			else
			{
				siteManagementTableAjax();
			}
		}
	});
	
	$('#normalSearchButton').click(function(){
		searchTerm = $('#normalSearchTerm').val();
		if(bvUrl.includes('bvdetails'))
		{
			siteManagementTableAjax2();
		}
		else
		{
			siteManagementTableAjax();
		}
	});
	
	$('.header').click(function(){
		$('.togglecontent').slideToggle(100);
	});
});





/* Define two custom functions (asc and desc) for string sorting */
jQuery.fn.dataTableExt.oSort['string-case-asc']  = function(x,y) {

	var a= parseFloat(x.split('%')[0]);
	var b= parseFloat(y.split('%')[0]);
	return ((a < b) ? -1 : ((a > b) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['string-case-desc'] = function(x,y) {
	var a= parseFloat(x.split('%')[0]);
	var b= parseFloat(y.split('%')[0]);
	return ((a < b) ?  1 : ((a > b) ? -1 : 0));
};

function siteManagementTableAjax2()
{
	var siteManagementTableURL = "${siteManagementTableURL}"+"&regionFilters="+selectedRegion+"&searchTerm="+searchTerm;
	//alert("URL:::"+siteManagementTableURL)
	$.ajax({
		url: siteManagementTableURL,
		type: "POST",
		dataType: 'json',
		success : function(data)
		{
			$('#siteManagmentTable').DataTable({
				"aaData" : data,
				"destroy": true,
				"bFilter" : false,
				"processing" : true,
				"bPaginate" : true,
				"pageLength": 7,
				"serverSide" : false,	
				"bLengthChange" : false,
				"bSortable" : false,
				"bInfo": false,
				"aoColumns": [
									null,
									null,
									null,
									null,
									{ "sType": 'string-case' },
									null,
									null,
									null,
									null
								],
				'columnDefs': [{
					 'targets': 0,
					 'className': 'dt-body-center',
					 'render': function (data, type, full, meta){
							return data;
						}
					}],
					'createdRow': function(row, data, dataIndex) {
						
						
						var performanceData=data[4].split("%");
						 if(performanceData[0] >= 98.9)
						 {
							$(row).css('color','#a3cf62').css('font-style','bold');
						 }
						 else if(performanceData[0] > 98.5 && performanceData[0] < 98.9)
						 {
							 $(row).css('color','#ff9c00').css('font-style','bold');
						 }
						 else if(performanceData[0] >= 0 && performanceData[0] <= 98.5)
						 {
							 $(row).css('color','#e32212').css('font-style','bold');
						 }
						 else
						 {
							 $(row).css('color','#555').css('font-style','bold');
						 }
					 },
					'order' : [[4,"asc"]]
				});
		},
		error: function()
		{
			//alert("error");
		}
	});
}



function siteManagementTableAjax()
{
	var siteManagementTableURL = "${siteManagementTableURL}"+"&regionFilters="+selectedRegion+"&searchTerm="+searchTerm;
	//alert("URL:::"+siteManagementTableURL)
	$.ajax({
		url: siteManagementTableURL,
		type: "POST",
		dataType: 'json',
		success : function(data)
		{
			$('#siteManagmentTable').DataTable({
				"aaData" : data,
				"destroy": true,
				"bFilter" : false,
				"processing" : true,
				"bPaginate" : true,
				"pageLength": 7,
				"serverSide" : false,	
				"bLengthChange" : false,
				"bSortable" : false,
				"bInfo": false,
    				"aoColumns": [
									null,
									null,
									null,
									null,
									{ "sType": 'string-case' }
								],
				'columnDefs': [{
					 'targets': 0,
					 'className': 'dt-body-center',
					 'render': function (data, type, full, meta){
							return data;
						}
					}],
					'createdRow': function(row, data, dataIndex) {
						
						
						var performanceData=data[4].split("%");
						 if(performanceData[0] >= 98.9)
						 {
							$(row).css('color','#a3cf62').css('font-style','bold');
						 }
						 else if(performanceData[0] > 98.5 && performanceData[0] < 98.9)
						 {
							 $(row).css('color','#ff9c00').css('font-style','bold');
						 }
						 else if(performanceData[0] >= 0 && performanceData[0] <= 98.5)
						 {
							 $(row).css('color','#e32212').css('font-style','bold');
						 }
						 else
						 {
							 $(row).css('color','#555').css('font-style','bold');
						 }
					 },
					'order' : [[4,"asc"]]
				});
		},
		error: function()
		{
			//alert("error");
		}
	});
}


</script>

<!--  Start of Sites Management Data Table -->
<div class="container-fluid maincontainer sitemanagementcontainer">
	
	<% 
		if(currentURL.contains(bvDetailsURL))
		{
	%>
	<table  class="span12" style=" background-color: #eee;border:0px">
  		<tr  class= "span12" style="margin: 5px;">
      		<th class="header expand" colspan="2" style="font-size: 15px;float: left;cursor: pointer;"><liferay-ui:message key="Site Management"/><span class="sign"></span></th>
  		<th class="span2" id="displayRegionFilter_sitemg" > <span id="normalSearchDiv" style="float: right;"> <input id="normalSearchTerm" type="text" name="normalSearchTerm" class=""> <button type="submit" id="normalSearchButton" class="search"></button> </span> </th>
  		</tr>
  		
  		<tr class="togglecontent" style=" background-color: rgb(255, 255, 255);">
 			<td class="togglecoloumn1">
				<div class="span12" id="displayRegionFilter" >
					<div class="span3">
						<input type="checkbox" id="all" name="serviceId" class="serviceClass" checked>
						<liferay-ui:message key="All"/>
					</div>
					<c:forEach var="region" items="${regionFilter}">
					<div class="span3" style="margin-left:0px !important;display: inline-flex;">
						<input type="checkbox" id="${region}" name="regionId" name="regionId">
						<liferay-ui:message key="${region}"/>
					</div>
					</c:forEach>
				</div>
			</td>
			<!--  <td class="togglecoloumn2">
				<div class="span3" id="displayRegionFilter">
			
				<div id="normalSearchDiv">
					<input id="normalSearchTerm" type="text" name="normalSearchTerm">
					<button type="submit" id="normalSearchButton" class="search"></button>	
				</div>
				
				</div>
			</td> -->	
 		</tr>
	</table>
	<%  
		} 
	%>
	<div class="sitemanagementlevel0-table">
	<table id="siteManagmentTable" class="display tt-siteManagmentTable-main" cellspacing="0" width="100%">
	<thead class="siteManagmentTable-head">

	<% 
		if(currentURL.contains(bvDetailsURL))
		{
	%>
			<tr>
				<th class="tableSelect" rowspan="2"><liferay-ui:message key="Province"/></th>
		        <th class="tablefirstname" rowspan="2"><liferay-ui:message key="Site Name"/></th>
		        <th class="tableworkcontact" rowspan="2"><liferay-ui:message key="Service Tier"/></th>
		        <th class="tablelastname" rowspan="2"><liferay-ui:message key="Status"/></th>
		        <th class="tablehomecontact" rowspan="2"><liferay-ui:message key="Performance"/></th>
		        <th class="tableNetUti" colspan="2" style="padding:0px !important"><liferay-ui:message key="Network Utilization"/></th>
		        <th class="tableCPUUti" colspan="2" style="padding:0px !important"><liferay-ui:message key="CPU Utilization"/></th>
		    </tr>
		    <tr class="tt-subtitle" >
		    	<th><liferay-ui:message key="Average"/></th>
				<th><liferay-ui:message key="Peak"/></th>
				<th><liferay-ui:message key="Average"/></th>
				<th><liferay-ui:message key="Peak"/></th>
			</tr>
	<%  
		}
		else 
		{
	%>
		    <tr>	
				<th class="tableSelect"><liferay-ui:message key="Province"/></th>
		        <th class="tablefirstname"><liferay-ui:message key="Site Name"/></th>
		        <th class="tableworkcontact"><liferay-ui:message key="Service Tier"/></th>
		        <th class="tablelastname"><liferay-ui:message key="Status"/></th>
		        <th class="tablehomecontact"><liferay-ui:message key="Performance"/></th>
		    </tr>
	<%
		}
	%>
		</thead>
	</table>
	</div>
</div>
<!--  End of Sites Management Data Table -->