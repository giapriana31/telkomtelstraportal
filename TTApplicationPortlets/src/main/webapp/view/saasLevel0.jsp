<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
       type="text/javascript"></script>
<script
       src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
       type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"
       type="text/javascript"></script>

<link href="${pageContext.request.contextPath}/css/main.css"
       rel="stylesheet"></link>

<%@ include file="common.jsp"%>
<portlet:defineObjects />

<script></script>

<style>

#saasPortletTitleBar{
       height:37px;
}

#saasTitleText{
       line-height:34px;
}
#saasNavigationArrow{
       line-height:34px;
}
.saas-title-dashboard {
    font-size: 15px;
    display: flex;
    justify-content: center;
}

#saasCountId{
       fill:#888 !important;
       font-size:20px !important;
}

#saasLabelId{
       fill:#888 !important;
       font-size:18px !important;
       font-weight: 100;
}

div#donut-graph-saas {
    display: flex;
   justify-content: center;
}

#markers-tt {
       display: flex;
       justify-content: center;
}

#tt-red {
       background-color: #e32212;
}

#tt-amber {
       background-color: #ff9c00;
}

#tt-green {
       background-color: #a3cf62;
}

#tt-red-marker {
       color: #e32212;
}

#tt-amber-marker {
       color: #ff9c00;
}

#tt-green-marker {
       color: #a3cf62;
}

.tt-color-legends {
       display: inline-block;
       width: 10px;
       height: 10px;
       position: relative;
       top: 5px;
       margin: 0 5px;
}

.tt-markers {
       display: inline-block;
       font-size: 12px;
}

.legend-saas {
       font-size: 15px;
       line-height: 120%;
       padding-top: 5px;
       padding-bottom: 30px;
       height: 30px;
       text-align: center;
}

#saasblock {
       justify-content: center;
       background-color: #fff;
       height: 262px;
       border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc;
}

.saasServices {
       width: 150px;
       margin: 0 40px 0 40px;
}

.value-saas {
       text-align: center;
       height: 50px;
       line-height: 50px;
       border: solid 1px black;
       border-radius: 8px;
}

#status-val-service {
       border-color: transparent;
}
</style>

<div
       style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
       <div id="saasPortletTitleBar" class="portletTitleBar">
              <div id="saasTitleText" class="titleText">
                     <liferay-ui:message key="SOFTWARE AS A SERVICE" />
              </div>
              <div id="saasNavigationArrow" class="navigationArrow">
                     <a class="navArrowRight" href="/group/telkomtelstra/saaslevel1">
                           <img class="navArrowRightImg"
                           src="/TTApplicationPortlets/images/Expand_G_24.png">
                     </a>
              </div>
       </div>
</div>
<div id="saasblock">
       <div class="saas-title-dashboard"><liferay-ui:message key="Overall SaaS Health" /></div>
       <div id="donut-graph-saas">
              <svg width="280" height="200">
                     <g class="arc" transform="translate(140,100)">
                     <path stroke="white" stroke-width="0.5"
                           fill="#${serviceStatusSpecs[1]}"
                           d="M0,90A90,90 0 1,1 0,-90A90,90 0 1,1 0,90M0,80A80,80 0 1,0 0,-80A80,80 0 1,0 0,80Z"></path></g>
                     <g class="center_group" transform="translate(140,100)">
                     <circle fill="#eeeeee" r="80"></circle>
                     <circle fill="white" r="73"></circle>
                     <text id="saasLabelId" class="label" dy="15" text-anchor="middle">
                     <liferay-ui:message key="Total Service" /></text>
                     <text id="saasCountId" class="total" dy="-5" text-anchor="middle">${serviceStatusSpecs[0]}</text></g></svg>
       </div>
       <div id="markers-tt">
              <div class="tt-color-legends" id="tt-red"></div>
              <div class="tt-markers" id="tt-red-marker"><liferay-ui:message key="High Impact" /></div>
              <div class="tt-color-legends" id="tt-amber"></div>
              <div class="tt-markers" id="tt-amber-marker"><liferay-ui:message key="Moderate Impact" /></div>
              <div class="tt-color-legends" id="tt-green"></div>
              <div class="tt-markers" id="tt-green-marker"><liferay-ui:message key="Normal" /></div>
       </div>

</div>


<script>
       
</script>






