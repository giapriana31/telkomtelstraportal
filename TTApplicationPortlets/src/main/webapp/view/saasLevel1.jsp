<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"
	type="text/javascript"></script>
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/saasLevel1.css" />



<%@ include file="common.jsp"%>
<portlet:defineObjects />

<style>

div#notSubscribed {
    border: solid 1px #ccc;
    color: #333;
    margin: 0 1%;
    padding: 1% 2%;
}

.blocks{
	cursor:pointer !important;
	color:#FFF;
}
</style>

<script>

	function redirectToAvailability(){
		window.location.href = "/group/telkomtelstra/sladetails";
	}

	function sendParam(param) {

		var typeOfRedirectVal = Base64.encode("saasToInc&" + param);
		window.location.href = "/group/telkomtelstra/tickets?"
				+ typeOfRedirectVal;
	}

	var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


$(document).ready(function() {

	});
</script>

<div
	style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
	<div class="portletTitleBar">
		<div class="titleText"><liferay-ui:message key="Software as a Service" />
			<div id="backButtonDiv">
				<input type="button" value="&nbsp;&lt;&nbsp;<liferay-ui:message key='Back'/>" class="btn-primary-tt backButton" onclick="window.location='/group/telkomtelstra/dashboard'" /> 
			</div>
		</div>
	</div>
</div>

<div id="serviceSpecsTableDiv">
	<table id="serviceSpecsTable">
		<tr>
			<td class="serviceTypeTd"><liferay-ui:message key="Service Type" /></td>
			<td class="sos"><liferay-ui:message key="Status of Services" /></td>
			<td class="avail"><liferay-ui:message key="Availability" /></td>
		</tr>
		<c:forEach items="${serviceSpecs}" var="inServiceSpecs">
			<c:if test="${inServiceSpecs[1] == 'greyOut'}"> 
 				 <c:set var="needToSubscribe" value="yes"/>
			</c:if>
			<tr>
				<td class="link1">${inServiceSpecs[0]}</td>
				<td class="link2"><div id="${inServiceSpecs[0]}"
						class="blocks redirectBlocks ${inServiceSpecs[1]}"
						onclick="sendParam(this.id)"></div></td>
				<td class="link3"><div class="blocks ${inServiceSpecs[3]}" onclick="redirectToAvailability()">${inServiceSpecs[2]}</div></td>
			</tr>
		</c:forEach>
	</table>
<c:if test="${needToSubscribe == 'yes'}">
<div id="notSubscribed"><liferay-ui:message key="You currently have not subscribed to all of the SaaS products. To enquire about any SaaS products, please" />&nbsp;<a href="http://www.telkomtelstra.co.id/en/contact-us.html" target="_blank"><liferay-ui:message key="click here" /></a>.</div>
</c:if>
</div>


