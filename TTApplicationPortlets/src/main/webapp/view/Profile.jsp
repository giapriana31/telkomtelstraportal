<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="<%= request.getContextPath()%>/js/jquery-1.11.3.min.js"></script>
  <style>
  .container-fluid
  {
  background-color: #fff;
  }
  .span10,.rightspan,.navigation_span10,.col{
   background-color: #fff;
 }
  </style>
</head>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<liferay-ui:error key="profileUpdateError" message="Please Check The Details and Try again !"></liferay-ui:error>
<liferay-ui:success key="profileUpdateSuccess" message="Profile has been successfully updated !" />
	<liferay-ui:error key="secondEmailSameError"
	message="The primary email and secondary email cannot be same."></liferay-ui:error>

<c:set var="backDoorEntry" value='<%=isbackdoor%>'></c:set>
<c:if test="${backDoorEntry==true}">
<div>

<img src="/TTApplicationPortlets/images/alert1.png" class="oktaimg">
<div class="oktamsg"> Page is temporarily unavailable, Please contact Administrator!!! </div>
 </div>

</c:if>
<c:if test="${backDoorEntry==false}">
<div  class="container-fluid tt-profile-container" style="background-color:#fff;">
<portlet:actionURL name="actionMethod2" var="sampleActionMethodURL"></portlet:actionURL>
<form action="${sampleActionMethodURL}" method="post">
<div class="Profilepagehead"><liferay-ui:message key="Update your profile details"/></div>
<br>
   <div class="row">
      <div class="span2" id="firstRowProfile"><liferay-ui:message key="First Name"/>:</div>
       <c:set var="firstName" value='${userDetails.firstName} '></c:set>
          <c:if test="${firstName=='null'|| firstName==''}">
           <c:set var="firstName" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
<INPUT TYPE="TEXT" NAME="<portlet:namespace />ProfileId" class="profileSettingsInput" VALUE="${firstName}"  readonly="readonly" >      </div>
   </div>
   <div class="row">
      <div class="span2" id="firstRowProfile"><liferay-ui:message key="Last Name"/>:</div>
       <c:set var="lastName" value='${userDetails.lastName}'></c:set>
          <c:if test="${lastName=='null'|| lastName==''}">
           <c:set var="lastName" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
	<INPUT TYPE="TEXT" NAME="<portlet:namespace />LastName" class="profileSettingsInput" VALUE="${lastName}"  readonly="readonly">
      </div>
   </div>
   
   <div class="row">
      <div class="span2" id="firstRowProfile"><liferay-ui:message key="Customer Id"/>:</div>
       <c:set var="customerId" value='${userDetails.customerID}'></c:set>
          <c:if test="${customerId=='null'|| customerId==''}">
           <c:set var="customerId" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
        <INPUT TYPE="TEXT" NAME="<portlet:namespace />companyIDCustom" class="profileSettingsInput" VALUE="${customerId}" readonly="readonly"  >
      </div>
   </div>
 
   <div class="row">
      <div class="span2" id="firstRowProfile"><liferay-ui:message key="Customer Name"/>:</div>
      <%-- <c:set var="customerName" value='<%= user.getExpandoBridge().getAttribute("customerName") %>'></c:set> --%>
          <c:set var="customerName" value='${userDetails.customerName}'></c:set>
          <c:if test="${customerName=='null'|| customerName==''}">
           <c:set var="customerName" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
        <INPUT TYPE="TEXT" NAME="<portlet:namespace />customerName" class="profileSettingsInput" VALUE='${customerName}' readonly="readonly" >
      </div>
   </div>
   <div class="row">
      <div class="span2" id="firstRowProfile">
          <liferay-ui:message key="Location"/>:</div>
          <c:set var="location" value='${userDetails.location}'></c:set>
          <c:if test="${location=='null'|| location==''}">
           <c:set var="location" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
         <INPUT TYPE="TEXT" NAME="<portlet:namespace />Location" class="profileSettingsInput"  VALUE="${location}"  readonly="readonly">
   </div></div>
     <div class="row">
      <div class="span2" id="firstRowProfile">
         <liferay-ui:message key="Work Contact"/>:</div>
          <c:set var="workContact" value='${userDetails.workContact}'></c:set>
          <c:if test="${workContact=='null'|| workContact==''}">
           <c:set var="workContact" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
<INPUT TYPE="TEXT" NAME="<portlet:namespace />WorkContact" class="profileSettingsInput"   VALUE="${workContact}"  data-validation="required length mobile_no" data-validation-length="10-15">   </div></div>
   
     <div class="row">
      <div class="span2" id="firstRowProfile">
          <liferay-ui:message key="Home Contact"/>:</div>
          <c:set var="homeContact" value='${userDetails.homeContact}'></c:set>
          <c:if test="${homeContact=='null'|| homeContact==''}">
           <c:set var="homeContact" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
 			<INPUT TYPE="TEXT" NAME="<portlet:namespace />HomeContact"  class="profileSettingsInput" VALUE="${homeContact}"   data-validation="required length mobile_no" data-validation-length="10-15">
		 <INPUT TYPE="hidden" NAME="<portlet:namespace />Email" readonly="readonly"  VALUE="<%= user.getDisplayEmailAddress() %>"  >
		 <INPUT TYPE="hidden" NAME="<portlet:namespace />CompanyID" readonly="readonly"  VALUE="<%= user.getCompanyId()+"" %>"  >   </div></div>
   
     <div class="row">
      <div class="span2" id="firstRowProfile">
          <liferay-ui:message key="Email"/>:</div>
           <c:set var="secondEmail" value='${userDetails.email}'></c:set>
          <c:if test="${secondEmail=='null'|| secondEmail==''}">
           <c:set var="secondEmail" value=""></c:set>
          </c:if>
      <div class="span3" id="firstRowProfile">
<INPUT TYPE="TEXT" NAME="<portlet:namespace />secondMail"  class="profileSettingsInput"VALUE="${secondEmail}"  data-validation="required email">   </div></div>
   
     <div class="row">
      <div class="span2" id="firstRowProfile">
          <liferay-ui:message key="Language"/>:</div>
      <div class="span3" id="firstRowProfile">
          <c:if test="${user.getLocale().toString() eq 'en_US'}">
	       	<select name="<portlet:namespace />lang">
	               <option value="en_US" selected>English</option>
	               <option value="in_ID" >Bahasa Indonesia</option>
	        </select>
		 </c:if>
		 <c:if test="${user.getLocale().toString() eq 'in_ID'}">
	       	<select name="<portlet:namespace />lang" class="profileSettingsInput">
	               <option value="en_US" ><liferay-ui:message key="English"/></option>
	               <option value="in_ID" selected><liferay-ui:message key="Bahasa Indonesia"/></option>
	        </select>
		 </c:if>    
   </div></div>
   <div class="profileSetBottomProfileSettings">
<!-- <aui:button class="styled-button-1-save" type="submit" value="Save" />
<aui:button class="styled-button-1-cancel" type="reset" value="Cancel" /> -->
<%if(allowAccess)
{
%>
<button  type="submit" id="saveQuestion"><div id="saveIcon"></div><div id="saveContent"><liferay-ui:message key="Save"/></div></button>
<%
}
else
{
%>
<button  type="button" id="saveQuestion" style="background-color:#CCC!important;color:white!important;" title="Contact Adminsitrator"><div id="saveIcon"></div><div id="saveContent"><liferay-ui:message key="Save"/></div></button>
<%
}
%>
<button  type="reset" id="cancelQuestion" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'"> <div id="cancelIcon"></div><div id="cancelContent"><liferay-ui:message key="Cancel"/></div> </button>
</div>
</form> 
</div>
</c:if>




