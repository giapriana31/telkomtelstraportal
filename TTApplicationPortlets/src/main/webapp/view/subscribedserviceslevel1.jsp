<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>


<script src="<%=request.getContextPath() %>/js/bvdashboard.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.dataTables.min.js"></script>  

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.dataTables.css"/>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/subscribedserviceslevel1.css"/>

<link href="${pageContext.request.contextPath}/css/bvdashboard.css" rel="stylesheet"/>



<portlet:defineObjects />
<liferay-theme:defineObjects />
<script type="text/javascript">

$(document).ready(function()
{
	subscribedServicesTable();
	

});
</script>


<%@ include file="common.jsp" %>
<%
	String currentURL = themeDisplay.getURLCurrent();
	String bvDetailsURL = TTGenericUtils.prop.getProperty("ttBVDetails");
%>
<div class="container-fluid tt-page-detail">
       <div class="row-fluid tt-page-breadcrumb">
              <div class="span6">
                     <div>
                           <span class="tt-img-dashboard tt-main-page-name" style="margin-top: -5px;"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
                           <span class="tt-current-page" style="margin-top: -13px;"><liferay-ui:message key="Subscribed Services"/></span>
                     </div>
              </div>
              <div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" class="btn-primary-tt backButton" style="margin-top: -16px;">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
              </div>
       </div>
</div>
<%@ include file="subscribedservicestable.jsp"%>
