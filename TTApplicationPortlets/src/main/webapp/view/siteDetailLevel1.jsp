 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getUserData">
</portlet:resourceURL>

<%@ include file="common.jsp" %>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>
	<link href="${pageContext.request.contextPath}/css/sites.css"
	rel="stylesheet"></link>
<!-- <style>
a{
color:#fff !important;
text-decoration:none !important;
}
</style> -->	
	
<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/sites.js"></script>

<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
				<span class="tt-current-page"><liferay-ui:message key="Sites"/></span>
			</div>
		</div>
		<div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" class="btn-primary-tt backButton">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
	</div>
	<div class="row-fluid tt-page-content">
		<ul id="tabc" class="nav nav-pills tt-site-ul">
			<li class="active tt-site-nav-pills selected" id="basedOnTicket-li">
				<a data-toggle="tab" href="#basedOnTicketTab" class="tt-site-nav-pills" id="basedOnTicket" onclick="basedOnTicket()"><liferay-ui:message key="Based on Incidents"/></a>
			</li>
			<li class="tt-site-nav-pills" id="basedOnSLATab-li">
				<a data-toggle="tab" href="#basedOnSLATab" class="tt-site-nav-pills" id="basedOnSLA" onclick="basedOnSLA()"><liferay-ui:message key="Based on Availability"/></a>
			</li>
		</ul>
	</div>
</div>

<!--START DISPLAY SITES CONTAINER-->
<div class="container-fluid tt-site-display">
	<div class="row-fluid">
		<div class="span12">
			<div class="span2 tt-displaySite-title"><liferay-ui:message key="Display Sites:"/></div>
			<div class="span6">
				<div class="row-fluid tt-displaySite-check">
					<div class="span2"><input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/><liferay-ui:message key=" All"/></div>
					<div class="span3"><input type="checkbox" id="checkbox_high" class="tt-displaysite-position" /><liferay-ui:message key=" High Impact"/></div>
					<div class="span4"><input type="checkbox" id="checkbox_moderate" class="tt-displaysite-position" /><liferay-ui:message key=" Moderate Impact"/></div>
					<div class="span3"><input type="checkbox" id="checkbox_normal" class="tt-displaysite-position" /><liferay-ui:message key=" Normal"/></div>
				</div>
			</div>
		</div>
	</div>
</div>