
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:resourceURL id="getSpeedoMeterGraphData" var="getSpeedoMeterGraphData"></portlet:resourceURL>
<portlet:resourceURL id="getSummaryBarGraphData" var="getSummaryBarGraphData"></portlet:resourceURL>


<html>
<head>

<link href="${pageContext.request.contextPath}/css/main.css"
       rel="stylesheet"></link>
<link href="${pageContext.request.contextPath}/css/evSpeedometer.css"
       rel="stylesheet"></link>
       <link href="${pageContext.request.contextPath}/css/sites.css"
       rel="stylesheet" />


<style>
.colorIndicatorDual {
    display: inline-block;
    height: 10px;
    width: 10px;
    margin-right: 10px;
    position: relative;
    left: -15px;
    top: 5px;
}

button#summaryButton {
    float: right;
    margin: 5px;
    background: url(/TTApplicationPortlets/images/u5.png) no-repeat;
    color: white;
}
#unsubscribedService {
    text-align: center;
   width: 100%;
    padding-top: 100px;
    padding-bottom: 100px
       }
#unsubscribedHeader{
display:none;
}
.row-fluid.cls {
    display: flex;
    justify-content: center;
}

.tt-legenddisplay{
       display:flex;
}

</style>
<script src="${pageContext.request.contextPath}/js/d3.js"></script>
<script src="${pageContext.request.contextPath}/js/d3.v3.min.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/evSpeedometer.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
.serviceIndicator {
    display: inline-block;
    margin-right: 10px;
    font-size: 11px;
}
.tt-displaysite-header{
       text-align:center;
}

@media ( min-width : 768px) {
       #barGraphDiv1>svg>text.thresholdsText {
              display: none !important;
       }
       #barGraphDiv2>svg>text.thresholdsText {
              display: none !important;
       }
}
</style>

<script>

var colors=["blue","red","green","yellow","brown","darkGrey","pink","black","violet","indigo","orange","white"];
var divDescriptors= {"tt-mnsmonthly":"MONTHLY MNS","tt-mnsquarterly":"QUARTERLY MNS","tt-mnsyearly":"YEARLY MNS","tt-cloudmonthly":"MONTHLY CLOUD","tt-cloudquarterly":"QUARTERLY CLOUD","tt-cloudyearly":"YEARLY CLOUD","tt-saasmonthly":"MONTHLY SAAS","tt-saasquarterly":"QUARTERLY SAAS","tt-saasyearly":"YEARLY SAAS","tt-ucmonthly":"MONTHLY UC","tt-ucquarterly":"QUARTERLY UC","tt-ucyearly":"YEARLY UC","tt-managesecuritymonthly":"MONTHLY MANAGESECURITY","tt-managesecurityquarterly":"QUARTERLY MANAGESECURITY","tt-managesecurityyearly":"YEARLY MANAGESECURITY"};
var engBhsTranslation= {"MONTHLY":"BULANAN","INCIDENT":"INSIDEN","DELIVERY":"PENGIRIMAN","UTILIZATION":"PEMANFAATAN","QUARTERLY":"TRIWULANAN","YEARLY":"TAHUNAN","Order Received":"Pesanan Diterima","Detailed Design":"Desain yang rinci","Procurement":"Pembelian","Delivery Activation":"Pengiriman Aktivasi","Monitoring":"Pemantauan","Operational":"Operasional","MNS":"MNS","Cloud":"Private Cloud","SaaS":"SaaS","UC":"UC","Network":"Network","CPE":"CPE","January":"Januari","February":"Februari",
"March":"Maret",
"April":"April",
"May":"Mei",
"June":"Juni",
"July":"Juli",
"August":"Agustus",
"September":"September",
"October":"Oktober",
"November":"November",
"December":"Desember",
"Quarter1":"Kuartal1",
"Quarter2":"Kuartal2",
"Quarter3":"Kuartal3",
"Quarter4":"Kuartal4",
"PERFORMANCE":"KINERJA",
"Delivery and Activation":"Pengiriman dan Pengaktifan",
"Sites":"Situs",
"Operational":"Operasional",
"Total":"Total",
"%":"%",
"Hot":"Panas",
"Completed Incidents":"Lengkap Insiden",
"month":"bulan",
"quarter":"perempat",
"year":"tahun",
"Incident":"Insiden","Delivery":"Pengiriman","Utilization":"Pemanfaatan","Performance":"Kinerja",
"vBlocks":"vBlocks",
"New":"Baru"
};

var serviceSelected='';
var qtyTypeFiltered='';
var qtyTypeTotal='';
var measuredQty='';
var isLevel2=false;

var currentLanguage = "<%=themeDisplay.getUser().getLocale()%>";
       var isBahasa = false;
       if (currentLanguage == 'in_ID') {
              isBahasa = true;
       }

       var selAll = false;
       var selInc = false;
       var nullAllStatus = {
              "MNS" : {
                     "MNSMon" : "null",
                     "MNSQtr" : "null",
                     "MNSYr" : "null"
              },
              "Cloud" : {
                     "CloudMon" : "null",
                     "CloudQtr" : "null",
                     "CloudYr" : "null"
              },
              "SaaS" : {
                     "SaaSMon" : "null",
                     "SaaSQtr" : "null",
                     "SaaSYr" : "null"
              },
              "UC" : {
                     "UCMon" : "null",
                     "UCQtr" : "null",
                     "UCYr" : "null"

              },
              "ManageSecurity" : {
                     "ManageSecurityMon" : "null",
                     "ManageSecurityQtr" : "null",
                     "ManageSecurityYr" : "null"

              }
       };


function ColorLuminance(hex, lum) {

       // validate hex string
       hex = String(hex).replace(/[^0-9a-f]/gi, '');
       if (hex.length < 6) {
              hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
       }
       lum = lum || 0;

       // convert to decimal and change luminosity
       var rgb = "#", c, i;
       for (i = 0; i < 3; i++) {
              c = parseInt(hex.substr(i*2,2), 16);
              c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
              rgb += ("00"+c).substr(c.length);
       }

       return rgb;
}

function renderSingleBarGraph(dataset,section){


       var w = 150;
       var h = 150;

       var xScale = d3.scale.ordinal()
                                  .domain(d3.range(dataset.length))
                                  .rangeRoundBands([0, w], 0.5); 

       var yScale = d3.scale.linear()
                                  .domain([0, d3.max(dataset, function(d) {return +d.value;})])
                                  .range([0, h]);

       var key = function(d) {
              return d.key;
       };
       
       //Create SVG element
       var svg = d3.select("#"+section)
                           .append("svg")
                           .attr("width", w)
                           .attr("height", h);

                           
       svg.selectAll("text")
          .data(dataset, key)
          .enter()
          .append("text")
          .text(function(d) {
                    return d.value;
                     
          })
          .attr("text-anchor", "middle")
          .attr("x", function(d, i) {
                     return xScale(i)+5;
          })
          .attr("y", function(d) {
                     return h - yScale(d.value) - 5;
                     
          })
          .attr("font-family", "sans-serif") 
          .attr("font-size", "11px")
          .attr("fill", "black");
       
       //Create bars
       svg.selectAll("rect")
          .data(dataset, key)
          .enter()
          .append("rect")
          .attr("x", function(d, i) {
                     return xScale(i);
          })
          .attr("y", function(d) {
                     return h - yScale(d.value);
          })
          .attr("width", 10)
          .attr("height", function(d) {
                     return yScale(d.value);
          })
          .attr("style", function(d) {
                     return "fill:"+colors[dataset.indexOf(d)];
          })
          

              //Tooltip
              .on("mouseover", function(d) {
                     //Get this bar's x/y values, then augment for the tooltip
                     var xPosition = parseFloat(d3.select(this).attr("x")) + xScale.rangeBand() / 2;
                     var yPosition = parseFloat(d3.select(this).attr("y")) + 14;
                     
                     //Update Tooltip Position & value
                     d3.select("#tooltip")
                           .style("left", xPosition + "px")
                           .style("top", yPosition + "px")
                           .select("#value")
                           .text(d.value);
                     d3.select("#tooltip").classed("hidden", false)
              })
              .on("mouseout", function() {
                     //Remove the tooltip
                     d3.select("#tooltip").classed("hidden", true);
              })
              .append("title")
          .append("text")
          .text(function(d) {
                     var displayText='';
                     if(isLevel2){
                           displayText+=serviceSelected+' - ';
                     }
                            if(isBahasa){
                                   if(selectedEntities=='Delivery'){
                                         displayText+=engBhsTranslation[d.key];
                                  }else{
                                         displayText+=d.key;
                                  }
                                  displayText+=' : '+d.value+' '+engBhsTranslation[measuredQty]; 
                                  return displayText;                    }
                           else{
                                  displayText+=d.key+' : '+d.value+' '+measuredQty; 
                                  return displayText;     
                           }
                     
          })
}


       function renderDualThresholdBarGraphForCloud(dataset, section) {


var w = (dataset.length/3)*50;
              var h = 150;

var x2Th=+160+((dataset.length*25)/3);
var x1Th=-158+((dataset.length*25)/3);


var spacingJson={"key":"","value":""};

for(var i=3;i<dataset.length;i+=5){
       dataset.splice(i,0,spacingJson);
      dataset.splice(i,0,spacingJson);
}
              

              var xScale = d3.scale.ordinal().domain(d3.range(dataset.length))
                           .rangeRoundBands([ 0, w ], 0.5);

              var yScale;          
                            
              if(dataset.length>0){             
              
                     yScale = d3.scale.linear().domain([ 0, d3.max(dataset, function(d) {
                           return +100.0;
                     }) ]).range([ 0, h ]);
              
              }
              
              else{
                     
                     yScale = d3.scale.linear().domain([ 0, 100 ]).range([ 0, h ]);
                     
              }

              var key = function(d) {
                     return d.key;
              };

              //Create SVG element
              var svg = d3.select("#" + section).append("svg").attr("width", w).attr(
                           "height", h);

              svg.selectAll("text").data(dataset, key).enter().append("text").text(
                           function(d) {if(d.value!=''){
                                         return Math.round(d.value);
                                  }
                           }).attr("text-anchor", "middle").attr("x", function(d, i) {
                     return xScale(i) + 4;
              }).attr("y", function(d) {
                     var valText=d.value;
                     if(parseFloat(valText)>100){valText="105";}
                     return h - yScale(valText) - 5;

              }).attr("font-family", "sans-serif").attr("font-size", "8px").attr(
                           "fill", "black");

              //Create bars
              svg.selectAll("rect").data(dataset, key).enter().append("rect").attr(
                           "x", function(d, i) {
                                  return xScale(i);
                           }).attr("y", function(d) {
                                  var valRect=d.value;
                                  if(parseFloat(valRect)>100){valRect="105";}
                                  return h - yScale(valRect);
              }).attr("width", 10).attr("height", function(d) {
                     var valHeight=d.value;
                     if(parseFloat(valHeight)>100){valHeight="105";}

                     return yScale(valHeight);
              }).attr("style", function(d) {
var result = (dataset.indexOf(d)/5);
var integerPart = Math.floor(result);
var floatingPointPart = dataset.indexOf(d)- (integerPart*5);
                     return "fill:" + ColorLuminance(colors[integerPart],-(3*floatingPointPart*0.1));
              })

              //Tooltip
              .on(
                           "mouseover",
                           function(d) {
                                  //Get this bar's x/y values, then augment for the tooltip
                                  var xPosition = parseFloat(d3.select(this).attr("x"))
                                                + xScale.rangeBand() / 2;
                                  var yPosition = parseFloat(d3.select(this).attr("y")) + 14;

                                  //Update Tooltip Position & value
                                  d3.select("#tooltip").style("left", xPosition + "px")
                                                .style("top", yPosition + "px").select("#value")
                                                .text(d.value);
                                  d3.select("#tooltip").classed("hidden", false)
                           }).on("mouseout", function() {
                     //Remove the tooltip
                     d3.select("#tooltip").classed("hidden", true);
              }).append("title").append("text").text(
                           function(d) {if(d.value!=''){
                                         var displayText='';
                                         if(isLevel2){
                                                displayText+=serviceSelected+' - ';
                                         }
                                          displayText+=d.key+' : '+Math.round(d.value)+' '+measuredQty; 
                                         return displayText;
                                  }
                           });

              svg.append("line").attr("x1", x1Th).attr("x2", x2Th).attr("y1", 0).attr(
                           "y2", 0).attr("style", "stroke:grey;stroke-width:1");
              svg.append("text").text("100").attr(
                           "x",x2Th+2).attr("y", 5).attr("class",
                           "thresholdsText");
              svg.append("line").attr("x1", x1Th).attr("x2", x2Th).attr("y1", h-yScale(75)).attr(
                           "y2", h-yScale(75)).attr("style", "stroke:green;stroke-width:1");
              svg.append("text").text("75").attr(
                           "x",x2Th+2).attr("y", h-yScale(75)+5).attr("class",
                           "thresholdsText");
       }


       
       function renderDualThresholdBarGraph(dataset, section) {


var w = (dataset.length)*30;
              var h = 150;

var x2Th=+160+((dataset.length*15));
var x1Th=-158+((dataset.length*15));

              var xScale = d3.scale.ordinal().domain(d3.range(dataset.length))
                           .rangeRoundBands([ 0, w ], 0.5);

              var yScale;          
                           
              if(dataset.length>0){             
              
                     yScale = d3.scale.linear().domain([ 0, d3.max(dataset, function(d) {
                           return +100.0;
                     }) ]).range([ 0, h ]);
              
              }
              
              else{
                     
                     yScale = d3.scale.linear().domain([ 0, 100 ]).range([ 0, h ]);
                     
              }

              var key = function(d) {
                     return d.key;
              };

              //Create SVG element
              var svg = d3.select("#" + section).append("svg").attr("width", w).attr(
                           "height", h);

              svg.selectAll("text").data(dataset, key).enter().append("text").text(
                           function(d) {
                                  return Math.round(d.value*100)/100;

                           }).attr("text-anchor", "middle").attr("x", function(d, i) {
                     return xScale(i) + 5;
              }).attr("y", function(d) {
                     return h - yScale(d.value) - 5;

              }).attr("font-family", "sans-serif").attr("font-size", "11px").attr(
                           "fill", "black");

              //Create bars
              svg.selectAll("rect").data(dataset, key).enter().append("rect").attr(
                           "x", function(d, i) {
                                  return xScale(i);
                           }).attr("y", function(d) {
                     return h - yScale(d.value);
              }).attr("width", 10).attr("height", function(d) {
                     return yScale(d.value);
              }).attr("style", function(d) {
                     return "fill:" + colors[dataset.indexOf(d)];
              })

              //Tooltip
              .on(
                           "mouseover",
                           function(d) {
                                   //Get this bar's x/y values, then augment for the tooltip
                                  var xPosition = parseFloat(d3.select(this).attr("x"))
                                                + xScale.rangeBand() / 2;
                                  var yPosition = parseFloat(d3.select(this).attr("y")) + 14;

                                  //Update Tooltip Position & value
                                  d3.select("#tooltip").style("left", xPosition + "px")
                                                .style("top", yPosition + "px").select("#value")
                                                .text(d.value);
                                  d3.select("#tooltip").classed("hidden", false)
                           }).on("mouseout", function() {
                     //Remove the tooltip
                     d3.select("#tooltip").classed("hidden", true);
              }).append("title").append("text").text(
                           function(d) {
                                  
                                         var displayText='';
                                         if(isLevel2){
                                                displayText+=serviceSelected+' - ';
                                         }
                                         displayText+=d.key+' : '+Math.round(d.value*100)/100+' '+measuredQty; 
                                         return displayText;
                                  

                           });

              svg.append("line").attr("x1", x1Th).attr("x2", x2Th).attr("y1", 0).attr(
                           "y2", 0).attr("style", "stroke:grey;stroke-width:1");
              svg.append("text").text("100").attr(
                           "x",x2Th+2).attr("y", 5).attr("class",
                           "thresholdsText");
              svg.append("line").attr("x1", x1Th).attr("x2", x2Th).attr("y1", h-yScale(75)).attr(
                           "y2", h-yScale(75)).attr("style", "stroke:green;stroke-width:1");
              svg.append("text").text("75").attr(
                           "x",x2Th+2).attr("y", h-yScale(75)+5).attr("class",
                           "thresholdsText");
       }

       function renderThresholdBarGraph(dataset, section) {

              var w = 150;
              var h = 150;

              var xScale = d3.scale.ordinal().domain(d3.range(dataset.length))
                           .rangeRoundBands([ 0, w ], 0.5);

              var yScale;                 
              if(dataset.length>0){             
              
                     yScale = d3.scale.linear().domain([ 0, d3.max(dataset, function(d) {
                           return +100.0;
                     }) ]).range([ 0, h ]);
              
              }
              
              else{
                     
                     yScale = d3.scale.linear().domain([ 0, 100 ]).range([ 0, h ]);
                     
              }

              var key = function(d) {
                     return d.key;
              };

              //Create SVG element
              var svg = d3.select("#" + section).append("svg").attr("width", w).attr(
                           "height", h);

              svg.selectAll("text").data(dataset, key).enter().append("text").text(
                           function(d) {
                                  if(d.value==-1){d.value=0;}
                                  return (Math.round(d.value*100)/100);

                           }).attr("text-anchor", "middle").attr("x", function(d, i) {
                     return xScale(i) + 5;
              }).attr("y", function(d) {
                     return h - yScale(d.value) - 5;

              }).attr("font-family", "sans-serif").attr("font-size", "11px").attr(
                           "fill", "black");

              //Create bars
              svg.selectAll("rect").data(dataset, key).enter().append("rect").attr(
                           "x", function(d, i) {
                                  return xScale(i);
                           }).attr("y", function(d) {
                     return h - yScale(d.value);
              }).attr("width", 10).attr("height", function(d) {
                     return yScale(d.value);
              }).attr("style", function(d) {
                     return "fill:" + colors[dataset.indexOf(d)];
              })

              //Tooltip
              .on(
                            "mouseover",
                           function(d) {
                                  //Get this bar's x/y values, then augment for the tooltip
                                  var xPosition = parseFloat(d3.select(this).attr("x"))
                                                + xScale.rangeBand() / 2;
                                  var yPosition = parseFloat(d3.select(this).attr("y")) + 14;

                                  //Update Tooltip Position & value
                                  d3.select("#tooltip").style("left", xPosition + "px")
                                                .style("top", yPosition + "px").select("#value")
                                                .text(d.value);
                                  d3.select("#tooltip").classed("hidden", false)
                           }).on("mouseout", function() {
                     //Remove the tooltip
                     d3.select("#tooltip").classed("hidden", true);
              }).append("title").append("text").text(
                           function(d) {
                                  var displayText='';
                                  if(isLevel2){
                                         displayText+=serviceSelected+' - ';
                                  }
                           displayText+=d.key+' : '+(Math.round(d.value*100)/100)+' '+measuredQty; 
                           return displayText;

                           });

              svg.append("line").attr("x1", -81).attr("x2", 231).attr("y1", h-yScale(75)).attr(
                           "y2", h-yScale(75)).attr("style", "stroke:green;stroke-width:1");
              svg.append("text").text("80").attr(
                           "x",233).attr("y", h-yScale(75)+5).attr("class",
                           "thresholdsText");
       }

function drawSpeedometer(data){
              var ServDelvStatus = jQuery.parseJSON(JSON.stringify(data));
              var dsSpdList = new Array();
              dsSpdList = $("#tt-speedometer").children();
              
              //Iterating Divs id as item value where item=service name and item2= SpeedometerM/Q/Y
              
              $(dsSpdList).each(function(index, servicename){
                
              var dsSpdList2 = $(servicename).children();
              //alert(servicename.id);
              var ServNameReceived = ServDelvStatus != null ? ServDelvStatus[servicename.id] : {};
              
                 $(dsSpdList2).each(function(index, snMQY) {
                        //alert(snMQY.id);
                     
                        if (!(($(this).hasClass('tt-servicetypelabel')) || this.id=='togglelabel')){
                        var PerMQYReceivedForServName = ServNameReceived[sectionServices[snMQY.id]] || null;
                      //alert(PerMQYReceivedForServName);
                        
                        var spData = {
                           percent: PerMQYReceivedForServName,
                           divId: snMQY.id
                         };
              
                         ttSpeedometer($, spData);
                           }
                 });
       });
       };


function ttSpeedometer($, spData, percent) {

         var Needle, arc, arcEndRad, arcStartRad, barWidth, chart, chartInset, degToRad, el, endPadRad, height, margin, needle, numSections, padRad, percToDeg, percToRad, percent = spData.percent,
           radius, sectionIndx, sectionPerc, startPadRad, svg, totalPercent, width, _i;
         //console.log(spData.percent);

         barWidth = 15;
         numSections = 3;
         sectionPerc = 1 / numSections / 2;
         padRad = 0.00;
         chartInset = 10;
         totalPercent = .75;
         
         $('#' + spData.divId + " svg").remove();
         el = d3.select('#' + spData.divId);
         margin = {
           top: 50,
           right: 0,
           bottom: 0,
           left: 0
         };

         width = 85;
         height = 85;
         //width = el[0][0].offsetWidth + margin.left;
         //height = el[0][0].offsetWidth + margin.top;
         //radius = Math.min(width, height) / 2;
         radius = 50;

         percToDeg = function(perc) {
           return perc * 360;
         };

         percToRad = function(perc) {
           return degToRad(percToDeg(perc));
         };

         degToRad = function(deg) {
           return deg * Math.PI / 180;
         };

         svg = el.append('svg').attr('width', width + margin.left + margin.right).attr('height', height);

         chart = svg.append('g').attr('transform', "translate(" + ((width / 2) + margin.left) + ", " + margin.top + ")");
              var isValidService=(spData.divId).indexOf("mns")>-1||(spData.divId).indexOf("cloud")>-1||(spData.divId).indexOf("saas")>-1;
              var wdw;
              if((spData.divId).indexOf("monthly")>-1){wdw='month';}
              else if((spData.divId).indexOf("quarterly")>-1){wdw='quarter';}
             else if((spData.divId).indexOf("yearly")>-1){wdw='year';}
              if(percent=="null"){
                     if(isValidService){
                           if(isBahasa){
                                  chart.append("title").append("text").text("Tidak "+engBhsTranslation[selectedEntities]+" data di sebelumnya "+engBhsTranslation[wdw]);
                           }else{
                                  chart.append("title").append("text").text("No "+selectedEntities+" data in previous "+wdw);
                           }
                     }
                     else{
                           if(isBahasa){
                                  chart.append("title").append("text").text("Layanan tidak berlangganan");
                           }else{
                                  chart.append("title").append("text").text("Service not subscribed");
                           }
                     }
              }

         for (sectionIndx = _i = 1; 1 <= numSections ? _i <= numSections : _i >= numSections; sectionIndx = 1 <= numSections ? ++_i : --_i) {
           arcStartRad = percToRad(totalPercent);
           arcEndRad = arcStartRad + percToRad(sectionPerc);
           totalPercent += sectionPerc;
           startPadRad = sectionIndx === 0 ? 0 : padRad / 2;
           endPadRad = sectionIndx === numSections ? 0 : padRad / 2;
           arc = d3.svg.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth).startAngle(arcStartRad + startPadRad).endAngle(arcEndRad - endPadRad);
              var appendedpath=chart.append('path').attr('class', "arc chart-color" + sectionIndx).attr('d', arc);
              
              if(sectionIndx==1){
                     if(isValidService){
                           appendedpath.attr('style','fill:#a3cf62 !important');
                     }else{
                           appendedpath.attr('style','fill:#d3d3d3 !important');
                     }
              }
              else if(sectionIndx==2){
                     if(isValidService){
                           appendedpath.attr('style','fill:#ff9c00 !important');
                     }else{
                           appendedpath.attr('style','fill:#c1baba !important');
                     }
              }
              else if(sectionIndx==3){
                     if(isValidService){
                           appendedpath.attr('style','fill:#e32212 !important');
                     }else{
                           appendedpath.attr('style','fill:#989292 !important');
                     }
              }
         }

         Needle = (function() {
           function Needle(len, radius) {
             this.len = len;
            this.radius = radius;
           }

           Needle.prototype.drawOn = function(el, perc) {
              var appendedNeedleCenter= el.append('circle').attr('class', 'needle-center');
             appendedNeedleCenter.attr('cx', 0).attr('cy', 0).attr('r', this.radius);
                if(!((spData.divId).indexOf("mns")>-1||(spData.divId).indexOf("cloud")>-1||(spData.divId).indexOf("saas")>-1)     ){
                     appendedNeedleCenter.attr('style', 'fill:#726b6b !important');
                }
             return el.append('path').attr('class', 'needle').attr('d', this.mkCmd(perc));
           };

           Needle.prototype.animateOn = function(el, perc) {
             var self;
             self = this;
             return el.transition().delay(0).ease('elastic').duration(4000).selectAll('.needle').tween('progress', function() {
               return function(percentOfPercent) {
                 var progress;
                 progress = percentOfPercent * perc;
                 return d3.select(this).attr('d', self.mkCmd(progress));
               };
             });
           };

           Needle.prototype.mkCmd = function(perc) {
             var centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;
             thetaRad = percToRad(perc / 2);
             centerX = 0;
             centerY = 0;
             topX = centerX - this.len * Math.cos(thetaRad);
             topY = centerY - this.len * Math.sin(thetaRad);
             leftX = centerX - this.radius * Math.cos(thetaRad - Math.PI / 2);
             leftY = centerY - this.radius * Math.sin(thetaRad - Math.PI / 2);
             rightX = centerX - this.radius * Math.cos(thetaRad + Math.PI / 2);
             rightY = centerY - this.radius * Math.sin(thetaRad + Math.PI / 2);
             return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;
           };

           return Needle;

         })();
         
       if(percent!=null){
         needle = new Needle(40, 4);

         needle.drawOn(chart, 0);

         needle.animateOn(chart, percent);
       }
       else{
         needle = new Needle(1, 4);

         needle.drawOn(chart, 0);
         
         
       }
       };

       String.prototype.ReplaceAll =function(stringToFind,stringToReplace){
              
           var temp = this;
       
           var index = temp.indexOf(stringToFind);
       
               while(index != -1){
       
                   temp = temp.replace(stringToFind,stringToReplace);
       
                   index = temp.indexOf(stringToFind);
       
               }
       
               return temp;
       
           }


       function renderSelectiveGraph(serviceParam, windowParam) {
              $('#graphIncdicatorDiv').removeClass('tt-legenddisplay');
              $('#summaryButtonDiv').css("display","block");
              var getSummaryBarGraphDataURL = "${getSummaryBarGraphData}";
              isLevel2=true;
              serviceSelected=windowParam;
              if(windowParam=='CLOUD'){serviceSelected='Private Cloud';}
              var summaryBarData = {
                     serviceIdentifier : serviceParam,
                     windowIdentifier : windowParam,
                     currentSelectedEntity : selectedEntities
              };
              if((selectedEntities == 'Utilization')&&(serviceParam=='MONTHLY'||serviceParam=='QUARTERLY')&&(windowParam=='SAAS')){
                                  $('#summarySection').show();
                                  $("#unsubscribedServiceCloud").hide();
                                  $("#unsubscribedServiceMns").hide();
                                  $("#unsubscribedServiceSaas").hide();



                                  $('#showGraphDiv').hide();
                                  $('#contactAdminDiv').show();
                                  var windowTxt='';
                                  if(serviceParam=='MONTHLY'){windowTxt='Month';}
                                  else if(serviceParam=='QUARTERLY'){windowTxt='Quarter';}
                                  var textShown=$('#contactAdminDiv').text();
                                  textShown=textShown.ReplaceAll('Year',windowTxt);
                                  $('#contactAdminDiv').text(textShown);


              }else{
              $
                           .ajax({
                                  type : "POST",
                                  url : getSummaryBarGraphDataURL,
                                  dataType : 'json',
                                  data : summaryBarData,
                                  success : function(data) {

                                  if(data.Service=='Cloud NS'){
                                                

                                         
                                         $("#unsubscribedServiceCloud").show();
                                         $("#unsubscribedServiceMns").hide();
                                         $("#unsubscribedServiceSaas").hide();

                                               $('#summarySection').show();
                                                $('#showGraphDiv').hide();
                                                $('#contactAdminDiv').hide();
                                                /*var tempHTML=$('#cloudmsgcontent').html();
                                                $('#unsubscribedService').html(tempHTML);*/
                                                                                                       
                                                $('#unsubscribedService').show();
                                         
                                                
                                                
                                         }else if(data.Service=='MNS NS'){

                                                $("#unsubscribedServiceMns").show();
                                                $("#unsubscribedServiceCloud").hide();
                                                $("#unsubscribedServiceSaas").hide();


                                                $('#summarySection').show();
                                                $('#showGraphDiv').hide();
                                                $('#contactAdminDiv').hide();
                                                /*var tempHTML=$('#mnsmsgcontent').html();
                                                $('#unsubscribedService').html(tempHTML);*/
                                                
                                                $('#unsubscribedService').show();
                                                
                                                
                                                
                                         }else if(data.Service=='SaaS NS'){

                                             $("#unsubscribedServiceSaas").show();
                                                 $("#unsubscribedServiceCloud").hide();
                                                 $("#unsubscribedServiceMns").hide();



                                             $('#summarySection').show();
                                             $('#showGraphDiv').hide();
                                             $('#contactAdminDiv').hide();
                                             /*var tempHTML=$('#mnsmsgcontent').html();
                                             $('#unsubscribedService').html(tempHTML);*/
                                             
                                             $('#unsubscribedService').show();
                                             
                                             
                                             
                                      }
                                         else if(data.Service=='Yearly'){

                                                $('#summarySection').show();
                                                $("#unsubscribedServiceCloud").hide();
                                                $("#unsubscribedServiceMns").hide();
                                                $("#unsubscribedServiceSaas").hide();



                                                $('#showGraphDiv').hide();
                                                $('#contactAdminDiv')
                                                              .show();
                                         }
                                         else{
                                         var no = 0;   
                                         var graphIndicatorText = "";
                                         str = "<div class='row-fluid cls'>";
                                         if (selectedEntities == 'Incident'){
                                                if(summaryBarData.windowIdentifier=='MNS'){
                                                colors=["#9C4BE7","#CD5C5C","#FF8333","#FFFF33"," #D8BFD8"];
                                                }
                                                if(summaryBarData.windowIdentifier=='CLOUD'){
                                                colors=["#CD5C5C","#33FFEC","#FF338D","#E9967A"];
                                                }
                                                
                                         }
                                         if (selectedEntities == 'Delivery'){
                                                
                                               colors=["#89C35C","#FA8072","#00B0F0","#7030A0","#A40800","#000000"];
                                               colors.reverse();
                                                
                                                
                                         }
                                         if (selectedEntities == 'Performance') {
                                         if(windowParam=='MNS'){
                                                colors=["#0000FF","#E9967A"];
                                                }
                                                if(windowParam=='CLOUD'){
                                                colors=["#CD5C5C"];
                                                }
                                         }
                                         if (selectedEntities == 'Utilization') {
                                                measuredQty='%';
                                                if(windowParam=='CLOUD'){
                                                
                                                colors=['#E87D7D','#F5B83D','#52B1E0','#F09E75','#CCCCCC'];
                                               }
                                         }
                                         

                                         $(".childDiv").html("");
                                         var graphIndicatorText = '';var nmbrsplit=0;
                                         
                                         if (isBahasa && !(selectedEntities=='Utilization' || (selectedEntities=='Performance' && windowParam=='CLOUD'))) {
                                                for (var i = 0; i < data.barGraph3.length; i++) {
                                                       graphIndicatorText += '<div class="tt-barlegends"><div class="colorIndicator" style="background-color:'+colors[i]+'"></div><div class="serviceIndicator">';
                                                                     if(no==4)
                                                                     {
                                                                     no=0;
                                                                     graphIndicatorText += "</div><div class='row-fluid cls'>";                   
                                                                     }
                                                       
                                                       
                                                       if(selectedEntities=='Delivery'){
                                                              graphIndicatorText +=  engBhsTranslation[data.barGraph3[i].key];

                                                       }else{
                                                              graphIndicatorText += data.barGraph3[i].key;
                                                       }
                                                       graphIndicatorText += '</div></div>';
                                                }
                                         } else {
                                                var ctr=0;graphIndicatorText +='<div class="row-fluid cls">';
                                                for (var i = 0; i < data.barGraph3.length; i++) {
                                                       
                                                       graphIndicatorText += '<div class="tt-barlegends"><div class="colorIndicator" style="background-color:'+colors[ctr]+'"></div><div class="serviceIndicator">';
                                                       if(selectedEntities=='Utilization' && windowParam=='CLOUD'){
                                                              graphIndicatorText += data.barGraph3[i].key.split("VCPU")[0]
                                                                     + '</div></div>';ctr++;i+=2;

                                                       }else{
                                                                    graphIndicatorText += data.barGraph3[i].key
                                                                     + '</div></div>';
                                                                     ctr++;}
nmbrsplit++;
if(nmbrsplit==4){
  graphIndicatorText +='</div><div class="row-fluid cls">'; nmbrsplit=0;   }                                          }
                                         }
                                         $("#graphIncdicatorDiv").html(graphIndicatorText);
                                         if (isBahasa) {
                                                $("#barGraphDiv1Text")
                                                              .html(
                                                                            engBhsTranslation[data.graphInterval1
                                                                                         .split(" ")[0]]
                                                                                         + " "
                                                                                         + data.graphInterval1
                                                                                                       .split(" ")[1]);
                                                $("#barGraphDiv2Text")
                                                              .html(
                                                                            engBhsTranslation[data.graphInterval2
                                                                                         .split(" ")[0]]
                                                                                         + " "
                                                                                         + data.graphInterval2
                                                                                                       .split(" ")[1]);
                                                $("#barGraphDiv3Text")
                                                              .html(
                                                                            engBhsTranslation[data.graphInterval3
                                                                                         .split(" ")[0]]
                                                                                         + " "
                                                                                         + data.graphInterval3
                                                                                                       .split(" ")[1]);
                                         } else {
                                                $("#barGraphDiv1Text").html(data.graphInterval1);
                                                $("#barGraphDiv2Text").html(data.graphInterval2);
                                                $("#barGraphDiv3Text").html(data.graphInterval3);
                                         }
                                         if (selectedEntities == 'Performance') {
                                                renderThresholdBarGraph(data.barGraph1,
                                                              "barGraphDiv1");
                                                renderThresholdBarGraph(data.barGraph2,
                                                              "barGraphDiv2");
                                                renderThresholdBarGraph(data.barGraph3,
                                                              "barGraphDiv3");
                                         }
                                         else if(selectedEntities =='Utilization'){
if(windowParam=='CLOUD'){
                                                renderDualThresholdBarGraphForCloud(data.barGraph1, "barGraphDiv1");
                                                renderDualThresholdBarGraphForCloud(data.barGraph2, "barGraphDiv2");
                                                renderDualThresholdBarGraphForCloud(data.barGraph3, "barGraphDiv3");
}else{
                                                renderDualThresholdBarGraph(data.barGraph1, "barGraphDiv1");
                                                renderDualThresholdBarGraph(data.barGraph2, "barGraphDiv2");
                                                renderDualThresholdBarGraph(data.barGraph3, "barGraphDiv3");
}
                                                
                                         }
                                         else {
                                                renderSingleBarGraph(data.barGraph1, "barGraphDiv1");
                                                renderSingleBarGraph(data.barGraph2, "barGraphDiv2");
                                                renderSingleBarGraph(data.barGraph3, "barGraphDiv3");
                                         }
                                                $("#unsubscribedServiceCloud").hide();
                                         $("#unsubscribedServiceMns").hide();
                                         $("#unsubscribedServiceSaas").hide();
                                         $('#summarySection').show();
                                         $('#showGraphDiv').show();
                                         $('#contactAdminDiv')
                                                                     .hide();
                                      }

                                  },
                                  error : function(data) {
                                         $('#summarySection').hide();
                                  }

                           });
              }

       }

       $(document)
                     .ready(
                                  function() {


                           $("#unsubscribedServiceCloud").hide();
                           $("#unsubscribedServiceMns").hide();
                           $("#unsubscribedServiceSaas").hide();
                                         
                                         
                                         $(".CursorCustom")
                                                       .click(
                                                                     function() {

                                                                           if (!(selectedEntities == 'All')) {

                                                                                  var window = (divDescriptors[this.id])
                                                                                                .split(" ")[0];
                                                                                  var service = (divDescriptors[this.id])
                                                                                                .split(" ")[1];
                                                                                  var serviceDisplay=service;
                                                                                  if(service=='CLOUD'){
                                                                                         serviceDisplay='PRIVATE CLOUD';
                                                                                  }

                                                                                  if (isBahasa) {
                                                                                         $("#summaryHeader")
                                                                                                       .html(
                                                                                                                     engBhsTranslation[window]
                                                                                                                                  + ' '
                                                                                                                                  + engBhsTranslation[selectedEntities
                                                                                                                                                .toUpperCase()]
                                                                                                                                  + '&nbsp;<liferay-ui:message key="REPORT FOR"/>&nbsp;'
                                                                                                                                  + serviceDisplay);
                                                                                  } else {
                                                                                         $("#summaryHeader")
                                                                                                       .html(
                                                                                                                     window
                                                                                                                                  + '&nbsp;'
                                                                                                                                  + selectedEntities
                                                                                                                                                .toUpperCase()
                                                                                                                                  + '&nbsp;<liferay-ui:message key="REPORT FOR"/>&nbsp;'
                                                                                                                                  + serviceDisplay);
                                                                                  }


                                                                                         renderSelectiveGraph(window, service);
                                                                                         

                                                                           } else {

                                                                                  $('#summarySection').hide();
                                                                           }

                                                                    }).children().click(function(e) {
                                                              return false;
                                                       });

                                         getSelectedEntities(document.getElementById("performance"));

                                  });
                                  
       function renderDualBarGraph(dataset,section){
       var w = 150;
       var h = 150;
       
       

       var xScale = d3.scale.ordinal()
                                  .domain(d3.range(dataset.length))
                                  .rangeRoundBands([0, w], 0); 

       var yScale = d3.scale.linear()
                                  .domain([0, d3.max(dataset, function(d) {return +d.value;})])
                                  .range([0, h]);

       var key = function(d) {
              return d.key;
       };

       //Create SVG element
       var svg = d3.select("#"+section)
                           .append("svg")
                           .attr("width", w)
                           .attr("height", h);

                           
       svg.selectAll("text")
          .data(dataset, key)
          .enter()
          .append("text")
          .text(function(d) {
                     if(dataset.indexOf(d)%2!=0){
                           if(d.value=="0"){
                                  return "";
                           }
                           else{

                                  return d.value;
                           }
                     }
                     else if(dataset.indexOf(d)%2==0){
                           if(dataset[dataset.indexOf(d)+1].value=="0"){
                                  return "";
                           }
                           else{
                                  return d.value;
                            }
                     }
                     else{
                           return d.value;
                     }
                     
                     
          })
          .attr("text-anchor", "middle")
          .attr("x", function(d, i) {
                     return (xScale(i)/1.2)+5;
          })
          .attr("y", function(d) {
                     return h - yScale(d.value) - 5;
                    
          })
          .attr("font-family", "sans-serif") 
          .attr("font-size", "11px")
          .attr("fill", "black");
       
       //Create bars
       svg.selectAll("rect")
          .data(dataset, key)
          .enter()
          .append("rect")
          .attr("x", function(d, i) {
                     return xScale(i)/1.2;
          })
          .attr("y", function(d) {
                     return h - yScale(d.value);
          })
          .attr("width", xScale.rangeBand()/1.2)
          .attr("height", function(d) {
                     return yScale(d.value);
          })
          
          .attr("style", function(d,i) {
                     if(dataset.indexOf(d)%2==0){
                            return "fill:"+colors[(dataset.indexOf(d))/2]+";";
                     }
                     else {
                           return "fill:"+ColorLuminance(colors[(dataset.indexOf(d)-1)/2],-0.2)+";";
                     }
          })
          

              //Tooltip
              .on("mouseover", function(d) {
                     //Get this bar's x/y values, then augment for the tooltip
                     var xPosition = parseFloat(d3.select(this).attr("x")) + xScale.rangeBand() / 2;
                     var yPosition = parseFloat(d3.select(this).attr("y")) + 14;
                     
                     //Update Tooltip Position & value
                     d3.select("#tooltip")
                           .style("left", xPosition + "px")
                           .style("top", yPosition + "px")
                           .select("#value")
                           .text(d.value);
                     d3.select("#tooltip").classed("hidden", false)
              })
              .on("mouseout", function() {
                     //Remove the tooltip
                     d3.select("#tooltip").classed("hidden", true);
              })
              .append("title")
          .append("text")
          .text(function(d) {
                     if(dataset.indexOf(d)%2==0){
                           if(d.key=='Private Cloud' && selectedEntities=='Utilization'){measuredQty='vBlocks';}
                           else if(d.key=='MNS' && selectedEntities=='Utilization'){measuredQty='Sites';}
                           if(isBahasa){
                                  return d.key+' - '+engBhsTranslation[qtyTypeFiltered]+' : '+d.value+' '+engBhsTranslation[measuredQty];                     }
                           else{
                                  return d.key+' - '+qtyTypeFiltered+' : '+d.value+' '+measuredQty; 
                           }
                     }
                     else{
                           if(d.key.split("Total")[0]=='Private Cloud' && selectedEntities=='Utilization'){measuredQty='vBlocks';}
                           if(isBahasa){
                                  return d.key.split("Total")[0]+' - '+engBhsTranslation[qtyTypeTotal]+' : '+d.value+' '+engBhsTranslation[measuredQty];                    }
                           else{
                                  return d.key.split("Total")[0]+' - '+qtyTypeTotal+' : '+d.value+' '+measuredQty;
                           }

                     }
          })
}

       function goToSummary(){
              var selElementId=selectedEntities.toLowerCase();
              getSelectedEntities(document.getElementById(selElementId));
       }

       function getSelectedEntities(checkEntity) {
       $('#graphIncdicatorDiv').addClass('tt-legenddisplay');
       $('#unsubscribedServiceMns').hide();
       $('#unsubscribedServiceSaas').hide();
       $('#summaryButtonDiv').css("display","none");
              $('#unsubscribedServiceCloud').hide();
              isLevel2=false;
              selectedEntities = '';
              var getSpeedoMeterGraphDataURL = "${getSpeedoMeterGraphData}";

              $('input[name="entityCheck"]:checked').each(function() {
                     selectedEntities = this.value;
              });

              if (selectedEntities == 'All' || selectedEntities == '') {
                    $('.AllGrey').addClass('Grey');
                     selAll = true;
                     $('#summarySection').hide();
              } else {
                     $('.AllGrey').removeClass('Grey');

                     selAll = false;
                    $('#summarySection').show();
                     $('#showGraphDiv').show();
                     $('#contactAdminDiv').hide();
              }
              if (selectedEntities == 'Incident' || selectedEntities == 'Delivery'
                           || selectedEntities == 'Performance' || selectedEntities == 'Utilization') {
                     if (selectedEntities == 'Incident') {
                           $("#y-axis-qty").html(
                                         '<liferay-ui:message key="Incident count"/>');measuredQty='Completed Incidents';
                     } else if (selectedEntities == 'Delivery') {
                           $("#y-axis-qty").html(' ');measuredQty='Sites';qtyTypeFiltered='Operational';qtyTypeTotal='Total';
                     } else if (selectedEntities == 'Performance') {
                           $("#y-axis-qty").html(
                                         '<liferay-ui:message key="Availability"/>');measuredQty='%';
                     } else if (selectedEntities == 'Utilization') {
                           $("#y-axis-qty").html(
                                         '<liferay-ui:message key="Utilization"/>');measuredQty='Sites';qtyTypeFiltered='Hot';qtyTypeTotal='Total';
                     }
                    selInc = true;
                     $('#summarySection').show();
                     $('#showGraphDiv').show();
                     $('#contactAdminDiv').hide();
              } else {
                     $('#summarySection').hide();
                     selInc = false;
              }

              var smGraphData = {
                     smGraphDataIdentifier : selectedEntities
              };

              $
                           .ajax({
                                  type : "POST",
                                  url : getSpeedoMeterGraphDataURL,
                                  dataType : 'json',
                                  data : smGraphData,
                                  success : function(data) {
                                         if (selectedEntities == 'Incident'
                                                       || selectedEntities == 'Delivery'
                                                       || selectedEntities == 'Performance'
                                                       || selectedEntities == 'Utilization') {
                                                drawSpeedometer(data.speedometerGraph);
                                                colors=["#9C4BE7","#CD5C5C","#8B008B","#D8BFD8","#FFFF33"];
                                         } else {
                                                drawSpeedometer(nullAllStatus);
                                         }

                                         if (isBahasa) {
                                                $("#summaryHeader")
                                                              .html(
                                                                            engBhsTranslation[selectedEntities
                                                                                         .toUpperCase()]
                                                                                         + '&nbsp;<liferay-ui:message key="SUMMARY"/>');
                                         } else {
                                                $("#summaryHeader")
                                                              .html(
                                                                            selectedEntities.toUpperCase()
                                                                                         + " SUMMARY");
                                         }
                                         $(".childDiv").html("");
                                         var graphIndicatorText = '';

                                         if (isBahasa) {
                                                $("#barGraphDiv1Text")
                                                              .html(
                                                                            engBhsTranslation[data.graphInterval1
                                                                                         .split(" ")[0]]
                                                                                         + " "
                                                                                         + data.graphInterval1
                                                                                                       .split(" ")[1]);
                                                $("#barGraphDiv2Text")
                                                              .html(
                                                                            engBhsTranslation[data.graphInterval2
                                                                                         .split(" ")[0]]
                                                                                         + " "
                                                                                         + data.graphInterval2
                                                                                                       .split(" ")[1]);
                                                $("#barGraphDiv3Text")
                                                              .html(
                                                                            engBhsTranslation[data.graphInterval3
                                                                                         .split(" ")[0]]
                                                                                         + " "
                                                                                         + data.graphInterval3
                                                                                                       .split(" ")[1]);
                                         } else {
                                                $("#barGraphDiv1Text").html(data.graphInterval1);
                                                $("#barGraphDiv2Text").html(data.graphInterval2);
                                                $("#barGraphDiv3Text").html(data.graphInterval3);
                                         }

                                         if (selectedEntities == 'Incident') {
                                                for (var i = 0; i < data.barGraph1.length; i++) {
                                                       graphIndicatorText += '<div class="tt-barlegends"><div class="colorIndicator" style="background-color:'+colors[i]+'"></div><div class="serviceIndicator">'
                                                                     + data.barGraph1[i].key
                                                                     + '</div></div>';
                                                }
                                                renderSingleBarGraph(data.barGraph1, "barGraphDiv1");
                                                renderSingleBarGraph(data.barGraph2, "barGraphDiv2");
                                                renderSingleBarGraph(data.barGraph3, "barGraphDiv3");
                                         } else if (selectedEntities == 'Delivery') {

                                                var ctr = 0;
                                                for (var i = 0; i < data.barGraph1.length; i++) {

                                                       if (!((data.barGraph1[i].key).indexOf('Total') > -1)) {
                                                              graphIndicatorText += '<div class="tt-barlegends"><div class="colorIndicator" style="background-color:'+colors[ctr]+'"></div><div class="colorIndicatorDual" style="background-color:'+ColorLuminance(colors[ctr],-0.2)+'"></div><div class="serviceIndicator">'
                                                                           + data.barGraph1[i].key;
                                                                     if(isBahasa){
                                                                           graphIndicatorText += '- '+engBhsTranslation['New']+'/'+engBhsTranslation['Total']+' '+engBhsTranslation['Sites'];
                                                                     }
                                                                     else{
                                                                           graphIndicatorText += '- New/Total Sites';

                                                                     }
                                                                           graphIndicatorText += '</div></div>';
                                                              ctr++;

                                                       }
                                                }
                                                renderDualBarGraph(data.barGraph1, "barGraphDiv1");
                                                renderDualBarGraph(data.barGraph2, "barGraphDiv2");
                                                renderDualBarGraph(data.barGraph3, "barGraphDiv3");

                                         }
                                         if (selectedEntities == 'Performance') {
                                                var ctr = 0;
                                                for (var i = 0; i < data.barGraph1.length; i++) {
                                                       graphIndicatorText += '<div class="tt-barlegends"><div class="colorIndicator" style="background-color:'+colors[ctr]+'"></div><div class="serviceIndicator">'
                                                                     + data.barGraph1[i].key
                                                                     + '</div></div>';
                                                       ctr++;
                                                }
                                                renderThresholdBarGraph(data.barGraph1,
                                                              "barGraphDiv1");
                                                renderThresholdBarGraph(data.barGraph2,
                                                             "barGraphDiv2");
                                                renderThresholdBarGraph(data.barGraph3,
                                                              "barGraphDiv3");
                                         }
                                         if (selectedEntities == 'Utilization') {
                                                var ctr = 0;console.log(data.barGraph1[i]);
                                                for (var i = 0; i < data.barGraph1.length; i++) {

                                                       if (!((data.barGraph1[i].key).indexOf('Total') > -1)) {
                                                              graphIndicatorText += '<div class="tt-barlegends"><div class="colorIndicator" style="background-color:'+colors[ctr]+'"></div><div class="serviceIndicator">'
                                                                           + data.barGraph1[i].key
                                                                           + '</div></div>';
                                                              ctr++;

                                                       }
                                                }
                                                renderDualBarGraph(data.barGraph1,
                                                              "barGraphDiv1");
                                                renderDualBarGraph(data.barGraph2,
                                                              "barGraphDiv2");
                                                renderDualBarGraph(data.barGraph3,
                                                              "barGraphDiv3");
                                         }
                                         $("#graphIncdicatorDiv").html(graphIndicatorText);

                                  },
                                  error : function(data) {
                                  }

                           });

       }
</script>
</head>

<body>
       <!-- START OF READERSECTION-->
       <div id="readerSection">
              <!-- START OF RADIO -->
              <div class="span12 tt-displaysite-header">
                     <div class="row-fluid tt-displaySite-check">
                           <div class="span3" style="display: inline-flex;">
                                   <input type="radio" name="entityCheck" id="delivery"
                                         value="Delivery" class="radio tt-displaysite-position"
                                         onchange="getSelectedEntities(this)"> <label
                                         for="delivery" class="entityLabels"><liferay-ui:message
                                                key="Delivery" /></label><br />
                           </div>
                           <div class="span3"style="display: inline-flex;">
                                  <input type="radio" name="entityCheck" id="incident"
                                         value="Incident" class="radio tt-displaysite-position"
                                         onchange="getSelectedEntities(this)"> <label
                                         for="incident" class="entityLabels"><liferay-ui:message
                                                key="Incident" /></label><br />
                           </div>


                           <div class="span3"style="display: inline-flex;">
                                  <input type="radio" name="entityCheck" id="performance"
                                         value="Performance" class="radio tt-displaysite-position"
                                         onchange="getSelectedEntities(this)" checked="checked"> <label
                                         for="performance" class="entityLabels"><liferay-ui:message
                                                key="Performance" /></label><br />
                           </div>

                          <div class="span3"style="display: inline-flex;">
                                  <input type="radio" name="entityCheck" id="utilization"
                                         value="Utilization" class="radio"
                                         onchange="getSelectedEntities(this)"> <label
                                         for="utilization" class="entityLabels"><liferay-ui:message
                                                key="Utilization" /></label><br />
                           </div>


                     </div>
              </div>

              <div id="filterResultDiv">
                     <!-- START OF SPEEDOMETER HEADER -->
                     <div class="span12 tt-sitetoggleheader">
                           <div id="toggleheaderlabel" class="tt-spdheaders span2"></div>
                           <div id="toggleheaderlabel" class="tt-monthlyhdr span3">
                                  <liferay-ui:message key="Monthly" />
                           </div>
                           <div id="toggleheaderlabel" class="tt-quarterlyhdr span3">
                                  <liferay-ui:message key="Quarterly" />
                           </div>
                           <div id="toggleheaderlabel" class="tt-yearlyhdr span3">
                                  <liferay-ui:message key="Yearly" />
                           </div>
                     </div>
                     <!-- END OF SPEEDOMETER HEADER -->

                     <!-- START OF SPEEDOMETER -->
                     <div id="tt-speedometer">
                           <div id="MNS" class="row-fluid tt-servicetype span12">
                                   <span id="tt-MNSlabel" class="tt-servicetypelabel span2"><liferay-ui:message
                                                key="MNS Service" /></span> <span id="togglelabel"><liferay-ui:message
                                                key="Monthly" /></span> <span id="tt-mnsmonthly"
                                         class="tt-service-all CursorCustom AllGrey span2"></span> <span
                                         id="togglelabel"><liferay-ui:message key="Quarterly" /></span> <span
                                         id="tt-mnsquarterly"
                                         class="tt-service-all CursorCustom AllGrey span2"></span> <span
                                         id="togglelabel"><liferay-ui:message key="Yearly" /></span> <span
                                         id="tt-mnsyearly"
                                         class="tt-service-all CursorCustom AllGrey span2"></span>
                           </div>
                           <div id="Cloud" class="row-fluid tt-servicetype span12">
                                  <span id="tt-Cloudlabel" class="tt-servicetypelabel span2"><liferay-ui:message
                                                key="Private Cloud" /></span> <span id="togglelabel"><liferay-ui:message
                                                key="Monthly" /></span> <span id="tt-cloudmonthly"
                                         class="tt-service-all CursorCustom AllGrey span2"></span> <span
                                         id="togglelabel"><liferay-ui:message key="Quarterly" /></span> <span
                                         id="tt-cloudquarterly"
                                         class="tt-service-all CursorCustom AllGrey span2"></span> <span
                                         id="togglelabel"><liferay-ui:message key="Yearly" /></span> <span
                                         id="tt-cloudyearly"
                                         class="tt-service-all CursorCustom AllGrey span2"></span>
                           </div>
                           <div id="SaaS" class="row-fluid tt-servicetype span12">
                                  <span id="tt-SaaSlabel" class="tt-servicetypelabel span2">SaaS</span>
                                 <span id="togglelabel"><liferay-ui:message key="Monthly" /></span>
                                  <span id="tt-saasmonthly" class="tt-service-all CursorCustom AllGrey span2"></span>
                                  <span id="togglelabel"><liferay-ui:message key="Quarterly" /></span>
                                  <span id="tt-saasquarterly" class="tt-service-all CursorCustom AllGrey span2"></span>
                                  <span id="togglelabel"><liferay-ui:message key="Yearly" /></span>
                                  <span id="tt-saasyearly" class="tt-service-all CursorCustom AllGrey span2"></span>
                           </div>
                           <div id="UC" class="row-fluid tt-servicetype span12">
                                  <span id="tt-UCLabel" class="tt-servicetypelabel span2">UC</span> <span
                                         id="togglelabel"><liferay-ui:message key="Monthly" /></span> <span
                                         id="tt-ucmonthly" class="tt-service-all Grey span2"></span> <span
                                         id="togglelabel"><liferay-ui:message key="Quarterly" /></span> <span
                                         id="tt-ucquarterly" class="tt-service-all Grey span2"></span> <span
                                         id="togglelabel"><liferay-ui:message key="Yearly" /></span> <span
                                         id="tt-ucyearly" class="tt-service-all Grey span2"></span>
                           </div>
                           <div id="ManageSecurity" class="row-fluid tt-servicetype span12">
                                  <span id="tt-ManageSecurityLabel" class="tt-servicetypelabel span2">Manage
                                         Security</span> <span id="togglelabel"><liferay-ui:message
                                                key="Monthly" /></span> <span id="tt-managesecuritymonthly"
                                         class="tt-service-all Grey span2"></span> <span id="togglelabel"><liferay-ui:message
                                                key="Quarterly" /></span> <span id="tt-managesecurityquarterly"
                                         class="tt-service-all Grey span2"></span> <span id="togglelabel"><liferay-ui:message
                                                key="Yearly" /></span> <span id="tt-managesecurityyearly"
                                         class="tt-service-all Grey span2"></span>
                           </div>

                     </div>
                     <!-- END OF SPEEDOMETER -->
             </div>
              <!-- END OF FILTERRESULTDIV-->
       </div>
       <!-- END OF READERSECTION-->

       <!-- START OF BARGRAPH-->
       <div id="summarySection">
              <div id="summaryHeader"></div>
                     <div id="unsubscribedServiceMns" style="display:none" class="container-fluid tt-container">
                     <%@ include file="unsubscribed_mns_service.jsp" %>
                     </div>
                     <div id="unsubscribedServiceCloud" style="display:none" class="container-fluid tt-container">
                     <%@ include file="unsubscribed_cloud_service.jsp" %>
                     </div>
                      <div id="unsubscribedServiceSaas" style="display:none" class="container-fluid tt-container">
                     <%@ include file="unsubscribed_saas_service.jsp" %>

                     </div>

              <div id="showGraphDiv"><div id="summaryButtonDiv"><button id="summaryButton" onclick="goToSummary()">&lt;<liferay-ui:message key="Back To Summary" /></button></div>
                     <div class="container-fluid" id="graphIncdicatorDiv"></div>
                     <div id="y-axis-qty"></div>
                     <div id="barGraphDisplay" class="row-fluid parentDiv">
                           <div class="span4 tt-bargraph-mobile tt-bargraph-boxleft">
                                  <span id="barGraphDiv1" class="span12 childDiv"></span><span
                                         id="barGraphDiv1Text" class="barGraphText"></span>
                           </div>
                            <div class="span4 tt-bargraph-mobile">
                                  <span id="barGraphDiv2" class="span12 childDiv"></span><span
                                         id="barGraphDiv2Text" class="barGraphText"></span>
                           </div>
                           <div class="span4 tt-bargraph-mobile tt-bargraph-boxright">
                                  <span id="barGraphDiv3" class="span12 childDiv"></span><span
                                         id="barGraphDiv3Text" class="barGraphText"></span>
                           </div>
                     </div>
              </div>

              <div id="contactAdminDiv">
                     <liferay-ui:message
                           key="If you require Year on Year comparison data, please contact our Service Manager contact available in Support menu." />
             </div>
       </div>
       <!-- END OF BARGRAPH-->

       <!-- START OF DASHBOARD MATRIX -->
       <div class="tt-evdashboard-header">
              <liferay-ui:message key="DASHBOARD MATRIX" />
       </div>
       <div class="table-responsive">
              <table class="table table-bordered">
                     <thead>
                           <tr>
                                  <th class="tt-thspeedometer"></th>
                                  <th class="tt-thspeedometer"><liferay-ui:message
                                                key="Delivery on time" /></th>
                                  <th class="tt-thspeedometer"><liferay-ui:message
                                                key="Incident Resolved" /></th>
                                  <th class="tt-thspeedometer"><liferay-ui:message
                                                key="Performance" /></th>
                                  <th class="tt-thspeedometer"><liferay-ui:message
                                                key="Utilization" /></th>
                           </tr>
                     </thead>
                     <tbody>
                           <tr>
                                   <td class="tt-tdspeedometer">
                                         <div class="tt-green-legend"></div>
                                  </td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Above " />80%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Above " />90%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Above " />98.9%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Below " />65%</td>
                           </tr>
                           <tr>
                                  <td class="tt-tdspeedometer">
                                         <div class="tt-orange-legend"></div>
                                  </td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message
                                                key="Between " />70% - 80%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message
                                                key="Between " />80% - 90%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message
                                                key="Between " />98.5% - 98.9%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message
                                                key="Between " />65% - 75%</td>




                           </tr>
                           <tr>
                                  <td class="tt-tdspeedometer">
                                         <div class="tt-red-legend"></div>
                                  </td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Below " />70%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Below " />80%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Below " />98.5%</td>
                                  <td class="tt-tdspeedometer"><liferay-ui:message key="Above " />75%</td>
                           </tr>
                     </tbody>
              </table>
       </div>
       <!-- END OF DASHBOARD MATRIX -->
</body>

</html>



