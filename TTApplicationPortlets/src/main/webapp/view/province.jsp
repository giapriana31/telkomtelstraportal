<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<portlet:defineObjects/>
<portlet:actionURL var="addProvinceURL">
<portlet:param name="action" value="add"/>
</portlet:actionURL>


<div class="box box-success" >
	<div class="box-header">
		<h2>Province</h2>
	</div>
	<form class="form-horizontal" method="post" action="${addProvinceURL}">
	<div class="box-body"></div>
		<div class="form-body">
			<div class="form-group">
				<input type="text" id="provincename" name="provincename" class="form-control" placeholder="Province Name"   >
			</div>
			<div class="form-group">
				<input type="text" id="lotitude" name="lotitude" class="form-control" placeholder="Lotitude" >
			</div>
			<div class="form-group">
				<input type="text" id="longitude" name="longitude" class="form-control" placeholder="Email" >
			</div>
		</div>
		<div class="box-footer">
			<div class= "col-md-11 col-md-12">
			<button type="reset" class= "btn btn-default pull-right">Cancel</button>
			</div>
			<button type="submit" value="Submit" class= "btn btn-primary ">Save</button>
	</div>
</form>
	
</div>

<!-- <h2>Data Province</h2>
<c:if test="${!empty listProvince }">
<table class="data">
	<tr>
		<td>Province Name</td>
		<td>Latitude</td>
		<td>Longitude</td>
		<td>#</td>
	</tr>
<c:forEach items="${listProvince} " var="listProvince">
	<tr>
		<td>${listProvince.provincename }</td>
		<td>${listProvince.latitude} </td>
		<td>${listProvince.longitude} </td>
		<td></td>
	</tr>
</c:forEach>	
</table>  
</c:if>-->

