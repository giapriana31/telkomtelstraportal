<%@ page import="com.tt.utils.TTGenericUtils"%>
<%@ page import="com.tt.utils.PropertyReader"%>
<%@ page import="java.util.Properties"%>
<%@ page import="com.tt.roles.CustomerRole"%>

<% 
	Properties prop = PropertyReader.getProperties(); 
	CustomerRole role = new CustomerRole();
	String customerID = com.tt.utils.TTGenericUtils.getCustomerIDFromSession(request,response);
	boolean allowAccess = com.tt.utils.TTGenericUtils.allowUserToPerformAction(request,response);
	boolean isCustomerPortalAdmin = role.isCustomerPortalAdmin(role.getLoggedInUser(request,response));
	boolean isbackdoor =  prop.getProperty("backdoor.entry.isenabled").toString().equalsIgnoreCase("true");
	boolean issladashboard=prop.getProperty("sladashboard.isenabled").toString().equalsIgnoreCase("true");
%>