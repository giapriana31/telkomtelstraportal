<%@ page import="javax.portlet.PortletSession"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ page import="javax.portlet.ActionRequest"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ include file="pvLogServiceRequestModal.jsp"%>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"
	type="text/javascript"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">	


<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="getSRListURI" var="getSRListURI"></portlet:resourceURL>

<portlet:resourceURL var="getStatusURL" id="getStatusURL" />
<portlet:resourceURL var="getServiceURL" id="getServiceURL" />

<portlet:resourceURL id="getServiceRequestItemDetails" var="getServiceRequestItemDetails"></portlet:resourceURL>
<portlet:actionURL name="refreshSR" var="refreshSR"></portlet:actionURL>


<script src="<%=request.getContextPath()%>/js/pvLogServiceRequestModal.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/pvLogServiceRequest.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"
	type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/fnReloadAjax.js" 
    type="text/javascript"></script>

<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js"
	type="text/javascript"></script>
	
	
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/jquery.dataTables.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/pvLogServiceRequestModal.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/pvLogServiceRequest.css" />	

<style>
div#p_p_id_1_WAR_kaleodesignerportlet_ {
    min-height: 148px;
}
 @media (max-width: 738px) and (min-width: 320px){
.tt-service-breadcrumb{
height: 550px !important;
}
}

@media (max-width: 500px){
button#srListSearchBtn {
    margin-bottom: 0px;
}
.tt-service-br-header{
	margin-top:0px !important;
	padding-left: 0px !important;
}
.tt-create-service-ctr{
padding-left: 0px !important;
padding-right: 0px !important;
}
td:nth-child(3){
display:none;
}
th:nth-child(3){
display:none;
}
td:nth-child(4){
display:none;
}
th:nth-child(4){
display:none;
}
.updateInfoHolder {
    margin-left: 9% !important;
}
.aui form {
    margin: 0 0 0px !important;
}
tt-service-br-header{
	margin-top: 0px !important;
}

.rwd{
		display:none !important;
	}
	.updatedTimeHolder{
		position:relative;
		right:7% !important; 
	}
	
	.wrapper {
		cursor: help;
		position: relative;
	  }
	
  .wrapper .tooltip {
	    background: #D4D4D4 ;
		bottom: 100%;
	  color: #fff;
	  display: block;
	  left: -25px;
	  margin-bottom: 15px;
	  opacity: 0;
	  padding: 20px;
	  pointer-events: none;
	  position: absolute;
	  width: 100%;
	  -webkit-transform: translateY(10px);
	     -moz-transform: translateY(10px);
	      -ms-transform: translateY(10px);
	       -o-transform: translateY(10px);
	          transform: translateY(10px);
	  -webkit-transition: all .25s ease-out;
	     -moz-transition: all .25s ease-out;
	      -ms-transition: all .25s ease-out;
	       -o-transition: all .25s ease-out;
	          transition: all .25s ease-out;
	  -webkit-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	     -moz-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	      -ms-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	       -o-box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	          box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.28);
	}
	
	
	/* This bridges the gap so you can mouse into the tooltip without it disappearing */
	.wrapper .tooltip:before {
	  bottom: -20px;
	  content: " ";
	  display: block;
	  height: 20px;
	  left: 0;
	  position: absolute;
	  width: 100%;
	}  
	
	/* CSS Triangles - see Trevor's post */
	.wrapper .tooltip:after {
	  border-left: solid transparent 10px;
	  border-right: solid transparent 10px;
	  border-top: solid #D4D4D4 10px;
	  bottom: -10px;
	  content: " ";
	  height: 0;
	  left: 50%;
	  margin-left: -13px;
	  position: absolute;
	  width: 0;
	}
	  
	.wrapper:hover .tooltip {
	  font-size: 8px;
	    color: black;
	  opacity: 1;
	  pointer-events: auto;
	  -webkit-transform: translateY(0px);
	     -moz-transform: translateY(0px);
	      -ms-transform: translateY(0px);
	       -o-transform: translateY(0px);
	          transform: translateY(0px);
	}

	/* IE can just show/hide with no transition */
	.lte8 .wrapper .tooltip {
	  display: none;
	}
	
	.lte8 .wrapper:hover .tooltip {
	  display: block;
	}
}

 @media(min-width: 768px){
		.aui .row-fluid .span6 {
		  width: auto !important;
		}
	.wrapper{
			display:none !important;
			}
	.tt-service-br-header{
	margin-top: 0px !important;
	}
	
	#srListSearchBtn{
		margin-top: 3%;
	}
}
	#tt-sr-modal {
		width: 55% !important;
		min-width: 365px;
	}


	.displayNoneImportant{
		display:none !important;
	}

	button#logServReqButton {
	  position: relative;
	  bottom: 12px;
	  z-index: 1;
	}

	button#refreshButton {
	  position: absolute;
	  bottom: 30px;
	  left: 240px;
	}

	.updateButtonDisable{
		background: #CCC;
		border-top-style: none;
		border-bottom-style: none;
		border-right-style: none;
		border-left-style: none;
		height:36px;
		color: #ffffff !important;
		width: auto;
		min-width:100px;
		border-radius:3px;
		padding-left:15px;
		padding-right:15px;
		font-family:'Gotham-Rounded' !important;
		cursor:default !important;
	}

	#updateDataHolderDiv {
		width: 1000px;
		position: relative;
		right: 10%;
		padding-bottom: 18px;
	}
	
	.updateInfoHolder {
		display: inline;
	}
	
	.updatedTimeHolder {
		display: inline;
		
	}
	
	.updateButtonHolder {
		display: inline;
	}
	
	.tt-create-service-breadcrumb{
		width: 340px !important;
		margin-top: 0px !important;
	}
	
	.tt-page-icon{
		float: left;
	}
	
	.tt-service-br-header{
		float: left;
		padding-left: 20px;
		font-size: 20px;
		color: #333333;
		text-shadow: none;
		padding-top: 5px;
		font-weight: 600;
	}
	
.current:hover {
	background: #e32212 !important;
}


/* This bridges the gap so you can mouse into the tooltip without it disappearing */
.wrapper .tooltip:before {
  bottom: -20px;
  content: " ";
  display: block;
  height: 20px;
  left: 0;
  position: absolute;
  width: 100%;
}  

/* CSS Triangles - see Trevor's post */
.wrapper .tooltip:after {
  border-left: solid transparent 10px;
  border-right: solid transparent 10px;
  border-top: solid #D4D4D4 10px;
  bottom: -10px;
  content: " ";
  height: 0;
  left: 50%;
  margin-left: -13px;
  position: absolute;
  width: 0;
}
  
.wrapper:hover .tooltip {
  font-size: 8px;
    color: black;
  opacity: 1;
  pointer-events: auto;
  -webkit-transform: translateY(0px);
     -moz-transform: translateY(0px);
      -ms-transform: translateY(0px);
       -o-transform: translateY(0px);
          transform: translateY(0px);
}

/* IE can just show/hide with no transition */
.lte8 .wrapper .tooltip {
  display: none;
}

.lte8 .wrapper:hover .tooltip {
  display: block;
}
div#p_p_id_1_WAR_kaleodesignerportlet_ {
    min-height: 148px;
}
@media (min-width: 501px) and (max-width: 767px){
	.tt-create-service-ctr {
    width: 0% !important;
    float: right !important;
    padding-left: 0px !important;
	}
	.wrapper{
			display:none !important;
	}
	.tt-service-br-header{
	margin-top:0px !important;
	}
	
	#srListSearchBtn{
	 margin-top: 7px !important;
	}
}

</style>
<script type="text/javascript">

function submitFm() {

	document.getElementById("refreshButton").disabled = 'disabled';
	document.getElementById("refreshButton").setAttribute("class", "updateButtonDisable");
	
	/* var refreshSRURL = "${refreshSR}";
	$.ajax({
		type : "POST",
		url : refreshSRURL,
		success : function(data) 
		{
			alert("success");
		},
		error : function(){
			alert("error");
		}
	}); */
	var url = '<%=refreshSR.toString()%>';
		document.forms["fm"].action = url;
		document.forms["fm"].submit();
}
</script>


<html>
<head>
</head>
<body>
<div class="container-fluid tt-service-breadcrumb">
	<div class="row-fluid">
		<div class="span4 tt-create-service-breadcrumb" style="display: inline-flex;">
			<div class="tt-page-icon">
				<img src="<%=request.getContextPath()%>/images/ServiceRequest_G_32.png" ></img>
			</div>
			<div class="tt-service-br-header" style="margin-top: 50px;"><liferay-ui:message key="Service Request"/></div>
		</div>
		<div class="span4 tt-create-service-breadcrumb" style="margin-bottom: 1px; float: right !important">
		<div id="updateDataHolderDiv">
			<div class="rwd updateInfoHolder">
				<liferay-ui:message key="Last updated on" />
			</div>
			<div class="wrapper updateInfoHolder"><liferay-ui:message key="Last upd.." /><div class="tooltip" style="margin-left:5px;text-align:center;">Last updated on</div></div>
			<div class="updatedTimeHolder">${lastRefreshDate}</div>
			<div class="updateButtonHolder">
			<form name="fm" method="Post">
				<%if(allowAccess)
				{
				%>
					<button type="button" id="refreshButton" class="btn-primary-tt refresh" onclick="submitFm()">
						<liferay-ui:message key="Update Data"/>
					</button>
				<%
				}
				else
				{
				%>
				<div class="updateButtonHolder">
					<button type="button" id="refreshButton" style="background-color: #CCC !important; color: white !important;" class="btn-primary-tt refresh" title="Contact Administrator">
						<liferay-ui:message key="Update Data"/>
					</button>
				</div>
				<%
				}
				%>	
			</form>
			</div>
		</div>
		</div>
		<div class="span8 tt-create-service-ctr" style=" float: right;">
			<button type="button" id="logServReqButton" class="img-create-servicerequest" data-toggle="modal"><liferay-ui:message key="Log a New Request" /></button>
		</div>
	</div>
</div>
</body>
</html>