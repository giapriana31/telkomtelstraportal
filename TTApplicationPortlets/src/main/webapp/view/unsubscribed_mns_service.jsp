<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.tt.utils.PropertyReader"%>
<%@ page import="java.util.Properties"%>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet" />
<link href="${pageContext.request.contextPath}/css/sites.css"
	rel="stylesheet" />


<%
	Properties prop = PropertyReader.getProperties();
%>

<div class="container-fluid tt-container ${additionalClass}" id="mnsMsg" style="height: 310px; margin-left: 10px; margin-right: 10px;">
	<!-- START UNSUBSCRIBED_MNS PORTLET HEADER-->
	<div class="row-fluid tt-header" id="unsubscribedHeader">
		<div class="span6 tt-page-title">
			<liferay-ui:message key="${title}" />
		</div>
		<div class="span6 tt-detail">
			<img class="imgalign"
				src="<%=request.getContextPath()%>/images/Expand_G_24.png" />
		</div>
	</div>
	<div class="row-fluid tt-content"
		style="text-align: center; margin-top: 10%; font-size: 16px;line-height: 40px;" id="msgContent">
		<img class="imgalign"src="<%=request.getContextPath()%>/images/no mns.png"style="width: 40px; margin-right: 17px;" />
		<liferay-ui:message key="You currently have not purchased any Managed Network services. To enquire about MNS Services, please"/> <a
			href="<%= prop.getProperty("unsubscribedservicelink")%>" target="_blank">&nbsp;<liferay-ui:message key="click here"/></a>.
	</div>
</div>