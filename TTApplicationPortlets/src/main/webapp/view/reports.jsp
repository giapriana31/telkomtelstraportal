<%@page import="com.tt.constants.GenericConstants"%>
<%@ page import="com.liferay.portal.model.User" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="common.jsp" %>
<style>

@media (max-width: 767px){
	#mobile_msg{
	/* font-size:11px;
	float:right; */
	display:none;
	}
	#mobile_msg1{
	display:none;
	}
	.aui a {
	text-decoration:none ;
	}
	.aui a:hover{
	text-decoration:none ;
	}
	
  }
  
	@media (min-width: 768px){
	#mobile_msg{
	display:none;
	}
	
}

	@media (min-width: 1200px) {
	#mobile_msg1{
	float:right;
	font-size:11px;
	}
	.aui a:hover{
	text-decoration:none ;
	}
	.aui a {
	text-decoration:none ;
	}
	
  }
@media (min-width: 768px) and (max-width: 1199px){
	#mobile_msg1{
	float:right;
	font-size:11px;
	}
	.aui a:hover{
	text-decoration:none ;
	}
	.aui a {
	text-decoration:none ;
	}
	
  }
}
@media (max-width: 700px){
.reportsmsg{
min-height:835px !important;
}

}




</style>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/reports.css" />



<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<portlet:defineObjects />
<liferay-theme:defineObjects />
<%-- <portlet:resourceURL var="openReportURL"  >
 <portlet:param name="action" value="openSWReports"/> 
</portlet:resourceURL>
<portlet:resourceURL var="getImageUrl"  >
 <portlet:param name="action" value="getImage"/> 
</portlet:resourceURL> --%>

<portlet:resourceURL id="openSWReports" var="openSWReports">
 <portlet:param name="action" value="openSWReports"/> 

</portlet:resourceURL>
<portlet:resourceURL id="getImage" var="getImage">
 <portlet:param name="action" value="getImage"/> 

</portlet:resourceURL>
<html> 
<body>
<c:set var="backDoorEntry" value='<%=isbackdoor%>'></c:set>
<c:if test="${backDoorEntry==true}">

<div>
<img src="<%=request.getContextPath()%>/images/alert1.png" style="position: relative;left: 43%;margin-top: 13%;">
<div style="font-size: 18px;color:#FF9C00;margin-left: 40px;margin-top: 42px;text-align:center;"><liferay-ui:message key="Page is temporarily unavailable, Please contact Administrator!!!"/></div>
 </div>
</c:if>
<c:if test="${backDoorEntry==false}">
<div style="width:100%;height:75px;display:table;background:white;" class="reportsheader">		
	<div style="display:table-cell;vertical-align:middle;padding-left:20px; border-bottom-width:1px;border-bottom-color:#CCC;border-bottom-style: solid;">
		<img src="<%=request.getContextPath()%>/images/Reports_G_32.png" />
		<font style="font-size:15px;vertical-align:middle;margin-left:1%;font-weight:200"><liferay-ui:message key="Reports"/></font>
 		<img  id="reportNameImage" src="<%=request.getContextPath()%>/images/ReadFull_G_16_right.png" />
 		<font id="reportName" style="font-size:15px;vertical-align:middle;margin-left:1%;font-weight:200;position: relative;top: 2px;"><liferay-ui:message key="Report Name"/></font>
 		
 	
 	<div id="backToReportsDiv"><button id="backToReports"><img id="backImage" src="<%=request.getContextPath()%>/images/Back_W_16.png" /><div id="backToReportsContent"><liferay-ui:message key="Back"/></div></button></div>
	</div>
	<hr style="border-bottom: 1px solid #CCC !important;
  border-top: none !important;">
</div>



<div id="mydiv" style="margin:25px;">
<div style="background:white;">    
	<div id="set_height" class="reportsmsg"style="position: relative; min-height:500px;border: solid 1px #CCCCCC;padding:3%;">
	
	<div>
	<div style="font-size:127%;
  background-color: #ccc;
  height: 35px;">
	<div style="  padding-top: 0.5%;
  padding-left: 0.5%;
 font-weight: bold">
	<liferay-ui:message key="Report Name"/>
	</div>
	
	</div>


						
 						<c:forEach items="${categoryList}" var="categoryList">
 						<div class="reportCategoryName" id="${categoryList}"><div id="reportCategoryNameInnerImage"></div><div id="reportCategoryNameInner">${categoryList}</div></div>
 						
 						<c:set var="oldCategory" value="${fn:trim(categoryList)}"></c:set>
 						
						<c:forEach items="${reportDetails}" var="details">
						<c:set var="currentCategory" value="${fn:trim(details.reportCategory)}"></c:set> 
						<c:if test="${oldCategory == currentCategory}">
					
						
 						
 						<a href="#" id="${details.reportName}" class="reportContent ${currentCategory}" style="display:none;" name="${details.reportId}" onclick="embedPDF('${openReportURL}&docid=${details.reportId}&reportName=${details.reportName}',this.id,this.name)" >
						<div class="reportNameOuterContainer" id="${details.reportName}" >


							<div class="reportName" >${details.reportName}</div><div class="reportCreateTime"><fmt:formatDate pattern="dd/MM/yyyy HH:mm:ss" value="${details.updateDate}" /></div>
						
						
						<img class="reportImage"style="" src="<%=request.getContextPath()%>/images/ReadFull_G_16_right.png" />
						
 						</div>
 						</a>
 						</c:if>
 						</c:forEach>
 						</c:forEach>
<div id="mobile_msg">
<liferay-ui:message key="ACCESS PORTAL IN DESKTOP FOR MORE REPORT"/>
</div>
	
<%
String currentView = "customerview";
String currentPage = "reports";
String currentFeature = GenericConstants.REPORTSLINK_FEATURE;
String feature = currentView + "." + currentPage + "." + currentFeature;
user = TTGenericUtils.getLoggedInUser(request);

if(TTGenericUtils.doesNotHaveAccessToUser(user, feature))
{
%>						
	<div id="mobile_msg1">
	<a href="#" class="reportContent" onclick="window.open('<%= prop.getProperty("Reports.URL")%>','','height:400;width:400;')" style="float:right;color:#555 !important;font-size:11px;"><liferay-ui:message key="CLICK HERE FOR MORE REPORT"/></a>
	</div>
<%
}
%>

<div id="reportsMessage" style="float:right;color:#555 !important;font-size:13px; clear:right;">
	<liferay-ui:message key="Please contact Service Desk to add more Reports."/>
</div>

</div>	
</div>
</div>
 </div>
 <div id="pdfOuterContainer">
 <div id="pdfContainer">
 
<%-- <object id="pdfEmb" height='500px' width='130%' style="height: 500px !important;">
		<p>It appears you don't have Adobe Reader or PDF support in this web browser. <a href="http://get.adobe.com/reader/" target="_blank">click here to install Adobe Reader</a>.</p>
	</object> --%>
	
	
	
	
 </div>
</div>


										
				   					
							
	</c:if>	
	
	
	
</body>
</html>
<script>

$(".reportCategoryName").click(function(){
	
	 var id = $(this).attr('id');
	 $("."+id).toggle();
	
	
});
function embedPDF(pdfSrc,id,name){
		
	
	
	$("#reportName").show();
	$("#reportNameImage").show();
	$("#backToReportsDiv").show();

	$("#reportName").html(id);
	
	$("#pdfContainer").show();		
	$("#mydiv").hide();	
	

	var parent = $('#pdfEmb').parent();
	var getReportData = "${openSWReports}&reportId="+name+"";

	$.ajax({
			type : "POST",
			url : getReportData,
			dataType : 'json',

			success : function(data) {
			
				
			for(var i = 0; i < data.length; i++) {
			var obj = data[i];
			$("#pdfContainer").append('<div class="pdfImage"><img src="${getImage}&imageId='+obj+'"></div>');
}
			
				
			},
			error : function(data) {
			}

		});
//	var newElement = "<object data='" + pdfSrc + "' id='pdfEmb' type='image/png' height='500px' width='100%'>";
	
	$('#pdfEmb').remove();
	parent.append(newElement);
	
	return false;
}


$("#backToReports").click(function(){
	$("#mydiv").show();
	$("#reportName").hide();
	$("#reportNameImage").hide();
	$("#backToReportsDiv").hide();
	$("#pdfContainer").empty();		
	$("#pdfContainer").hide();		
		
	$("#pdfContainer").css("display","none");
	
});

$(document).ready(function() {
	$("#reportName").hide();
	$("#reportNameImage").hide();
	$("#backToReportsDiv").hide();
	
	$("#pdfContainer").hide();
		
			
		});
</script>

