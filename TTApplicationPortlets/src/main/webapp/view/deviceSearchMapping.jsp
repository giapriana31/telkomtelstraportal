<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="datatableURI" var="datatableURI"></portlet:resourceURL>

<html>
<head>

<style>

.oddSNC{
       background:#9E9E9E !important;
}

.evenSNC{
       background:#E0DFDF !important;
}

#tooltipId{
       text-decoration:none !important;
       color: #555 !important;
}

#filterLabel{
       display:inline-block;
       padding-left: 20px;
}

#filterDiv{
       display:inline-block;
}

.filterbox{
       display:inline-block;
       padding:5px;
       margin: 0 5px 0 5px;
       border: solid 1px #ccc;
}

#exportDeviceListingButton {
    background-image: url("../images/srchad.png");
    height: 35px;
    width: 70px;
    border-style: none;
}

@media (min-width: 320px) and (max-width: 600px){
       th#tt-dev-adj-link {
    display: none;
       }
       th#tt-dev-adj-status {
           display: none;
       }
       th#tt-dev-adj-activatedon{
         display: none;
       }
       th#tt-dev-adj-ccd {
    display: none;
       }
       div#listingDatatableDiv {
       text-align: center;
    padding-left: 5px;
       }
       div#devicegriddiv {
           display: none;
       }
       td+td+td+td+td{
       display:none;
       }
       table#listingDatatable{
       border-collapse: collapse !important;
       }
}
       
@media (min-width: 601px) and (max-width: 885px){
       
       th#tt-dev-adj-activatedon{
         display: none;
       }
       th#tt-dev-adj-ccd {
    display: none;
       }
       div#listingDatatableDiv {
       text-align: center;
       }
       div#devicegriddiv {
           display: none;
       }
       }
@media (min-width: 886px){
       div#listingDatatableDiv {
       text-align: center;
       }
       }

</style>

<script src="<%=request.getContextPath()%>/js/jquery.dataTables.min.js"
       type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/fnReloadAjax.js"
       type="text/javascript"></script>
<link rel="stylesheet"
       href="<%=request.getContextPath()%>/css/jquery.dataTables.css" />
<script>

var isSiteNameSelected=true;

var delStatusList={"OrderReceived":"Order Received","DetailedDesign":"Detailed Design","Procurement":"Procurement","DeliveryandActivation":"Delivery and Activation","Monitoring":"Monitoring","Operational":"Operational","Total":null};

var delStatusInvList={"Order Received":"OrderReceived","Detailed Design":"DetailedDesign","Procurement":"Procurement","Delivery and Activation":"DeliveryandActivation","Monitoring":"Monitoring","Operational":"Operational","Total":null};

var receivedCiid='All';
var siteName="${siteNameReceived}";
var service=["All"];
var deliveryStatus=["Total"];
var filtersHtml="<liferay-ui:message key="No Filters Applied"/>";

String.prototype.ReplaceAll =function(stringToFind,stringToReplace){
              
       var temp = this;

       var index = temp.indexOf(stringToFind);
       
               while(index != -1){
       
                   temp = temp.replace(stringToFind,stringToReplace);
       
                   index = temp.indexOf(stringToFind);
       
           }
       
       return temp;
       
}

function renderFilterBox(keyString,valueString){
var filterBoxHtml='';
if(!(valueString=='All' || valueString=='Total')){
       filterBoxHtml='<div class="filterbox"  id="';
       if(keyString=='deliveryStatus'){
              filterBoxHtml+=""+delStatusInvList[""+valueString];
       }
       else if(keyString=='service'){
              filterBoxHtml+=""+keyString;
       }
       else if(keyString=='sitename'){
              filterBoxHtml+=""+keyString;
       }
       else if(keyString=='ciFilter'){
              filterBoxHtml+=""+keyString;
       }
       else if(keyString=='searchTerm'){
           filterBoxHtml+=""+keyString;
       }
       filterBoxHtml+='" aria-label="'+keyString+'">'+valueString+'<a href="javascript:void(0)" onclick="removeFilter(this)" id="';
       if(keyString=='deliveryStatus'){
              filterBoxHtml+=""+delStatusInvList[""+valueString];
       }
       else if(keyString=='service'){
              filterBoxHtml+=""+keyString;
       }
       else if(keyString=='sitename'){
              filterBoxHtml+=""+keyString;
       }
       else if(keyString=='ciFilter'){
              filterBoxHtml+=""+keyString;
       }
       else if(keyString=='searchTerm'){
           filterBoxHtml+=""+keyString;
       }
       filterBoxHtml+='" aria-label="'+keyString+'"><img src="${pageContext.request.contextPath}/images/u8.png"></a></div>';
}
       return filterBoxHtml;
}

function removeFilter(str){
       var datatableURL="${datatableURI}";
       $("#filterDiv #"+str.id).remove();
       
       $("#gridTableData td").css('border','solid 2px black');
       if($("#filterDiv").text()=="" || $("#filterDiv").text()==null){
              $("#filterDiv").html('<liferay-ui:message key="No Filters Applied"/>');
       }
       
       if(str.getAttribute("aria-label")=="service"){
              service=["All"];
              populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus+"&siteName="+siteName);
       }
       if(str.getAttribute("aria-label")=="deliveryStatus"){
              deliveryStatus=["Total"];
              if(!(service.indexOf("All")>-1)){
                     populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus+"&service="+service+"&siteName="+siteName);
              }
              else{
                     populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus+"&siteName="+siteName);
              }
       }
       if(str.getAttribute("aria-label")=="sitename"){
              siteName='All';
              if(!(service.indexOf("All")>-1)){
                     populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus+"&service="+service);
              }
              else{
                     populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus);
              }
       }
       if(str.getAttribute("aria-label")=="ciFilter"){
              ciFilter='All';
              if(!(service.indexOf("All")>-1)){
                     populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus+"&service="+service);
              }
              else{
                     populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus);
              }
       }
       if(str.getAttribute("aria-label")=="searchTerm"){
              service=["All"];
              populateTable(datatableURL+"&deliveryStatus=" +deliveryStatus+"&siteName="+siteName);
       }
       
       
}

function sendParameters(obj){
     var datatableURL="${datatableURI}";
       
       deliveryStatus=[];
       service=[];
       deliveryStatus.push(delStatusList[obj.getAttribute("id")]);
       service.push(obj.getAttribute("aria-label"));

       
       var serviceBox="";
       var delStatusBox="";
       var siteNameBox="";
       
       for(var i=0;i<deliveryStatus.length;i++){
              serviceBox=renderFilterBox("service",service[i]);
              if(deliveryStatus[i]!=null){
                     delStatusBox=renderFilterBox("deliveryStatus",deliveryStatus[i]);
              }else{
                     deliveryStatus=["Total"];
              }
       }
       if(!(siteName=='All')){
              siteNameBox=renderFilterBox("sitename",siteName);
       }
       filtersHtml=serviceBox+""+delStatusBox+""+siteNameBox;
       $("#filterDiv").html(filtersHtml);
       
       for(var i=0;i<deliveryStatus.length;i++){
              deliveryStatus[i]=deliveryStatus[i].ReplaceAll("&","%26");
       }
       
       populateTable(datatableURL+"&deliveryStatus=" + deliveryStatus + "&service=" + service + "&siteName=" + siteName);
}

$('table').on('click', 'td', function() {
       $("#gridTableData td").css('border','solid 2px black');
   $(this).css('border', 'solid 3px #f00');
});
$.fn.dataTable.pipeline = function(opts) {
              // Configuration options
              var conf = $.extend({
                     pages : 3, // number of pages to cache
                     url : '', // script url
                     data : null, // function or object with parameters to send to the server
                     // matching how `ajax.data` works in DataTables
                     method : 'GET' // Ajax HTTP method
              }, opts);

              // Private variables for storing the cache
              var cacheLower = -1;
              var cacheUpper = null;
              var cacheLastRequest = null;
              var cacheLastJson = null;

              return function(request, drawCallback, settings) {
                     var ajax = false;
                     var requestStart = request.start;
                     var drawStart = request.start;
                     var requestLength = request.length;
                     var requestEnd = requestStart + requestLength;

                     if (settings.clearCache) {
                           // API requested that the cache be cleared
                           ajax = true;
                           settings.clearCache = false;
                     } else if ((cacheLower < 0) || (requestStart < cacheLower)
                                  || (requestEnd > cacheUpper)) {
                           // outside cached data - need to make a request
                           ajax = true;
                     } else if (JSON.stringify(request.order) !== JSON
                                  .stringify(cacheLastRequest.order)
                                  || JSON.stringify(request.columns) !== JSON
                                                .stringify(cacheLastRequest.columns)
                                  || JSON.stringify(request.search) !== JSON
                                                .stringify(cacheLastRequest.search)) {
                           // properties changed (ordering, columns, searching)
                           ajax = true;
                     }

                     // Store the request for checking next time around
                     cacheLastRequest = $.extend(true, {}, request);


                     if (ajax) {
                           // Need data from the server
                           if (requestStart < cacheLower) {
                                  requestStart = requestStart
                                                - (requestLength * (conf.pages - 1));

                                  if (requestStart < 0) {
                                         requestStart = 0;
                                  }
                           }

                           cacheLower = requestStart;
                           cacheUpper = requestStart + (requestLength * conf.pages);

                           request.start = requestStart;
                           request.length = requestLength * conf.pages;

                           // Provide the same `data` options as DataTables.
                           if ($.isFunction(conf.data)) {
                                  // As a function it is executed with the data object as an arg
                                  // for manipulation. If an object is returned, it is used as the
                                  // data object to submit
                                  var d = conf.data(request);
                                  if (d) {
                                         $.extend(request, d);
                                  }
                           } else if ($.isPlainObject(conf.data)) {
                                  // As an object, the data given extends the default
                                  $.extend(request, conf.data);
                           }

                           settings.jqXHR = $.ajax({
                                  "type" : conf.method,
                                  "url" : conf.url,
                                  "data" : request,
                                  "dataType" : "json",
                                  "cache" : false,
                                  "success" : function(json) {
                                         cacheLastJson = $.extend(true, {}, json);

                                         if (cacheLower != drawStart) {
                                                json.data.splice(0, drawStart - cacheLower);
                                         }

                                         json.data.splice(requestLength, json.data.length);

                                         drawCallback(json);
var classList = document.getElementById('tt-dev-adj-sitename').className.split(/\s+/);
for (var i = 0; i < classList.length; i++) {
    if (classList[i] === 'sorting_asc' || classList[i] === 'sorting_desc') {
        isSiteNameSelected=true;
  break;
    }else{isSiteNameSelected=false;}
}
if(isSiteNameSelected){
for(var i=0;i<json.data.length;i++){
  if(json.data[i][0].split('&&&')[1]=='1')
    {var ctr=i*9;
    $("#listingDatatable td:eq("+ctr+")").closest('tr').find("td").addClass("evenSNC");}
  else if(json.data[i][0].split('&&&')[1]=='2')
    {var ctr=i*9;
    $("#listingDatatable td:eq("+ctr+")").closest('tr').find("td").addClass("oddSNC");}
}
 

}
                                  }
                           });
                     } else {
                           json = $.extend(true, {}, cacheLastJson);
                           json.draw = request.draw; // Update the echo for each response
                           json.data.splice(0, requestStart - cacheLower);
                           json.data.splice(requestLength, json.data.length);

                           drawCallback(json);
var classList = document.getElementById('tt-dev-adj-sitename').className.split(/\s+/);
for (var i = 0; i < classList.length; i++) {
    if (classList[i] === 'sorting_asc' || classList[i] === 'sorting_desc') {
        isSiteNameSelected=true;
  break;
    }else{isSiteNameSelected=false;}
}

if(isSiteNameSelected){
for(var i=0;i<json.data.length;i++){
  if(json.data[i][0].split('&&&')[1]=='1')
    {var ctr=i*9;
    $("#listingDatatable td:eq("+ctr+")").closest('tr').find("td").addClass("evenSNC");}
  else if(json.data[i][0].split('&&&')[1]=='2')
    {var ctr=i*9;
    $("#listingDatatable td:eq("+ctr+")").closest('tr').find("td").addClass("oddSNC");}
}


}
                     }
              }

       };

       //Register an API method that will empty the pipelined data, forcing an Ajax
       //fetch on the next draw (i.e. `table.clearPipeline().draw()`)
       $.fn.dataTable.Api.register('clearPipeline()', function() {
              return this.iterator('table', function(settings) {
                     settings.clearCache = true;
              });
       });

function toggleSNC(classRec){
if(classRec=='oddSNC'){return 'evenSNC';}
else{return 'oddSNC';}
}      

function populateTable(datatableURL) {   

       console.log(datatableURL);

       //DataTables initialisation
       var oTable=$('#listingDatatable').dataTable({
              "processing" : true,
              "serverSide" : true,
              "bDestroy": true,                 
              'bInfo' : true,
        "columnDefs": [
                {
                  // The `data` parameter refers to the data for the cell (defined by the
                  // `data` option, which defaults to the column being worked with, in
                  // this case `data: 0`.
                  "render": function ( data, type, row ) {
                        return data.split('&&&')[0];
                  },
                  "targets": 0
                }
         ],
              "oLanguage": {
                    "sInfo": '<liferay-ui:message key="Showing"/> _START_-_END_ <liferay-ui:message key="of"/> _TOTAL_',
                    "sInfoEmpty": '<liferay-ui:message key="Showing"/> 0-0 <liferay-ui:message key="of"/> 0',
                    "sEmptyTable": '<liferay-ui:message key="No data available in table"/>',
                       "oPaginate": {
                      "sPrevious": '&lt;&nbsp;<liferay-ui:message key="Previous"/>',
                           "sNext": '<liferay-ui:message key="Next"/>&nbsp;&gt;'
                    }
                       
                  },
              'bFilter' : false,
              'bLengthChange' : false,

              "ajax" : $.fn.dataTable.pipeline({
                     url : datatableURL,
                     pages : 3
              // number of pages to cache
              })
       });    
       oTable.fnSort( [ [0,'asc'] ] );
}

$(document).ready(function() {

$("#tt-dev-adj-sitename").click(function(){
       isSiteNameSelected=true;
});
       var datatableURL="${datatableURI}";
       var parameterReceived = getQueryString(window.location.href);
                     if(parameterReceived!=-1){
                           receivedCiid = Base64.decode(parameterReceived);
                     }
       
       if(!(siteName=='')){
              filtersHtml=renderFilterBox("sitename",siteName);
       }
       if(!(receivedCiid=='All')){
              filtersHtml=renderFilterBox("ciFilter",receivedCiid.split("&")[1]);
       }
       
       if(filtersHtml==""){
              filtersHtml="<liferay-ui:message key="No Filters Applied"/>"
       }
       $("#filterDiv").html(filtersHtml);
       populateTable(datatableURL+"&siteName="+siteName+"&ciFilter="+receivedCiid.split("&")[0]);

});

function deviceSearchFunction(){ 
    var datatableURL="${datatableURI}";
    filtersHtml=renderFilterBox("searchTerm",$('#deviceSearchTxt').val());
    $("#filterDiv").html(filtersHtml);
    populateTable(datatableURL+"&searchTerm=" + $('#deviceSearchTxt').val());
};

function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;

    // CSV file
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = "none";

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
}

function exportTableToCSV(filename) {
    var csv = [];
    var rows = document.querySelectorAll("#listingDatatable tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText);
        
        csv.push(row.join(","));        
    }

    // Download CSV file
    downloadCSV(csv.join("\n"), filename);
}

</script>
</head>
<body>
<div id="dev-grid-title"><liferay-ui:message key="Device Listing" /> 
  <a href="#" data-toggle="tooltip" data-placement="top" title="<liferay-ui:message key="Sites having multiple devices are highlighted."/>"> 
    <img class="helpIconImg" src="<%=request.getContextPath()%>/images/Discription_G_16.png">
  </a>
                    </div>
<div id="filterLabel"><liferay-ui:message key="Filters:" /></div>
<div id="filterDiv"></div>
<div class="srlSearchBox"><!--form id="tt-sr-normalSearchForm"-->
      <!--input id="deviceSearchTxt" type="search" class="" placeholder="" aria-controls="srListingTable"-->
      <!--button type="button" id="deviceSearchButton" class="normalSearch" onclick="deviceSearchFunction()"></button-->
      <!--button type="button" id="advSearchButton" data-toggle="modal"></button-->
      <button type="button" id="exportDeviceListingButton" data-toggle="modal" onclick="exportTableToCSV('DeviceListing.csv')">Export</button>
    <!--/form-->
</div>
<div id="listingDatatableDiv">
       <table id="listingDatatable" class="display" cellspacing="0" width="100%">
                           <thead>
                                  <tr>
                                         <th id="tt-dev-adj-sitename" class="tt-dev-adjCol-sitename"><liferay-ui:message key="Site Name" /></th>                                    
                                         <th id="tt-dev-adj-location" class="tt-dev-adjCol-location"><liferay-ui:message key="Location" /></th>
                                         <th id="tt-dev-adj-service" class="tt-dev-adjCol-service"><liferay-ui:message key="Service" /></th>
                                         <th id="tt-dev-adj-pc" class="tt-dev-adjCol-pc"><liferay-ui:message key="Type" /></th>
                                         <th id="tt-dev-adj-tiers" class="tt-dev-adjCol-tiers"><liferay-ui:message key="Tiers" /></th>
                                         <th id="tt-dev-adj-link" class="tt-dev-adjCol-link"><liferay-ui:message key="Link"/>&nbsp;(Kbps)</th>
                                         <th id="tt-dev-adj-status" class="tt-dev-adjCol-status"><liferay-ui:message key="Status" /></th>
                                         <th id="tt-dev-adj-activatedon" class="tt-dev-adjCol-status"><liferay-ui:message key="Activated On" /></th>
                                         <th id="tt-dev-adj-ccd" class="tt-dev-adjCol-ccd"><liferay-ui:message key="Customer Committed Date" /></th>
                                         
                                  </tr>
                           </thead>
       </table>
</div>
</body>
</html>