 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getUserData">
</portlet:resourceURL>

<%@ include file="common.jsp" %>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>
	<link href="${pageContext.request.contextPath}/css/sites.css"
	rel="stylesheet"></link>
<!-- <style>
a{
color:#fff !important;
text-decoration:none !important;
}
</style> -->	
	
<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/sites.js"></script>
<script>
	function basedOnTicket() {
		$('#basedOnSLATab-li').removeClass('selected');
		$('#basedOnTicket-li').addClass('selected');
		$('#site-sla').addClass('hide');
		$('#site-incident').removeClass('hide');
	}

	function basedOnSLA() {	
		$('#basedOnTicket-li').removeClass('selected');
		$('#basedOnSLATab-li').addClass('selected');
		$('#site-sla').removeClass('hide');
		$('#site-incident').addClass('hide');
		
	}
	
	
	var basedonslajson2 = ${basedonslajson};
	var basedonincidentsjson = ${basedonincidentsjson};
	
	$(document).ready(function(){
		var hash = document.location.hash;
		var maxsz='55px';
		$(".a_ttofycgreen").css('height', maxsz);
		 $(".a_ttofycamber").css('height', maxsz);
		 $(".a_ttofycred").css('height', maxsz);
		if(hash){
			if(hash === '#basedOnTicketTab'){
				basedOnTicket();
			}
			else if(hash === '#basedOnSLATab'){
				basedOnSLA();
			}
		}
		else{
			basedOnTicket();
		}
	});
	
	//var basedonincidentsjson = ${basedonincidentsjson}+"";
	//var basedonincidentsjson = [{"siteTier":"Gold","siteCount":"03","id":3,"officeBuildingSiteColorList":[{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"}]},{"siteTier":"Silver","siteCount":"02","id":2,"officeBuildingSiteColorList":[{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"},{"id":1,"siteName":"INFINKARHYD","colorCode":"green"},{"id":1,"siteName":"INFINKARMLR","colorCode":"green"},{"id":1,"siteName":"INFINKARBLR","colorCode":"green"}]},{"siteTier":"Bronze","siteCount":"01","id":1,"officeBuildingSiteColorList":[{"id":3,"siteName":"INFINKARHYD","colorCode":"red"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":2,"siteName":"INFINKARPUN","colorCode":"amber"},{"id":1,"siteName":"INFINKARPUN","colorCode":"green"}]}];
	//console.log(JSON.stringify(basedonincidentsjson));
	//var basedonslajson = ${basedonslajson};	
	//var basedonslajson2 = [{"id":"x","siteTier":"Gold Sites","siteCount":"02","slaColorList":[{"id":"01","siteName":"testsite","colorCode":"green","sitePercentileAverage":"98"},{"id":"01","siteName":"testsite","colorCode":"green","sitePercentileAverage":"98"}]},{"id":"y","siteTier":"Silver Sites","siteCount":"06","slaColorList":[{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02","siteName":"testsite2","colorCode":"amber","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"},{"id":"02a","siteName":"testsite2a","colorCode":"green","sitePercentileAverage":"30"}]},{"id":"z","siteTier":"Bronze Sites","siteCount":"04","slaColorList":[{"id":"03","siteName":"testsite3","colorCode":"red","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"amber","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"amber","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"green","sitePercentileAverage":"40"},{"id":"03","siteName":"testsite3","colorCode":"green","sitePercentileAverage":"40"}]}];
	//console.log(JSON.stringify(basedonslajson2));
	
	var siteFilters = {
		   'a_ttofycred' : true,
		   'a_ttofycamber': true,
		   'a_ttofycgreen' : true,
		   'a_ttofycredsla' : true,
		   'a_ttofycambersla': true,
		   'a_ttofycgreensla' : true
	};
	
	String.prototype.startsWith = function (str)
	{
	 return (this.indexOf(str) == 0) ? true : false;
	}
	
	$(function() {
		$("input[type='checkbox']").change(function() {
		
		if($('#checkbox_all').is(':checked')) 
		{		
			siteFilters["a_ttofycred"] = true;
			siteFilters["a_ttofycamber"] = true;
			siteFilters["a_ttofycgreen"] = true;
			siteFilters["a_ttofycredsla"] = true;
			siteFilters["a_ttofycambersla"] = true;
			siteFilters["a_ttofycgreensla"] = true;
		}
		else
		{
			siteFilters["a_ttofycred"] = false;
			siteFilters["a_ttofycamber"] = false;
			siteFilters["a_ttofycgreen"] = false;
			siteFilters["a_ttofycredsla"] = false;
			siteFilters["a_ttofycambersla"] = false;
			siteFilters["a_ttofycgreensla"] = false;
			
				if($('#checkbox_high').is(':checked')) 
				{
					siteFilters["a_ttofycred"] = true;			
					siteFilters["a_ttofycredsla"] = true;			
				}
				else
				{
					siteFilters["a_ttofycred"] = false;			
					siteFilters["a_ttofycredsla"] = false;			
				}
				
				if($('#checkbox_moderate').is(':checked')) 
				{
					siteFilters["a_ttofycamber"] = true;		
					siteFilters["a_ttofycambersla"] = true;		
				}
				else
				{		
					siteFilters["a_ttofycamber"] = false;		
					siteFilters["a_ttofycambersla"] = false;		
				}
				
				if($('#checkbox_normal').is(':checked')) 
				{
					siteFilters["a_ttofycgreen"] = true;
					siteFilters["a_ttofycgreensla"] = true;
				}
				else
				{		
					siteFilters["a_ttofycgreen"] = false;
					siteFilters["a_ttofycgreensla"] = false;
				}
		}
			
		
		
											$.each(basedonincidentsjson, function( index, siteType ) { 
													
											 var site = siteType['siteTier'].toLowerCase();
											 var actualCount = siteType['siteCount']; 
											 //alert(site);
											 //alert(actualCount);
											 var sitetttype = 'box' + site;
											  //alert(sitetttype);
											  
												
												
											 $("#"+sitetttype+" div div").each( function(){     
											 //$("#"+sitetttype+" div").each( function(){     
												
												var currentsitecolour = $(this).attr('class').split(" ");	
												//alert(currentsitecolour);
												$.each( currentsitecolour, function( key, value ) {
												  if(value.startsWith('a_'))
												  {  
														var filterResult = displayFilter(value, actualCount);         
												//alert(filterResult);
												
												$('.'+value).css( "display", filterResult.display); 
												//alert(filterResult.display);
												actualCount = filterResult.actualCount;
												//alert(actualCount);
											  }
											});		
											});
											
											 var countCont = $("#hd"+site).children();
											 $.each( countCont, function() {												
											   if($(this).attr('class')=='hd1'){											   
												  $(this).html(actualCount  > 9 ? "" + actualCount: "" + actualCount);  
											   }    
											 });											 
											});
											
											
											$.each(basedonslajson2, function( index, siteType ) { 
											var site = siteType['siteTier'].toLowerCase();
											 var actualCount = siteType['siteCount']; 
											 //alert(siteType['siteCount']);
											 //alert(actualCount);											 
											 var sitetttype1 = 'box' + site;
											var sitetttype=sitetttype1.replace(/\s/g, '');
											  //alert(sitetttype);
											 $("#"+sitetttype+" div div").each( function(){     
											  
												var currentsitecolour = $(this).attr('class').split(" ");	
												//alert(currentsitecolour);
												$.each( currentsitecolour, function( key, value ) {
												  if(value.startsWith('a_'))
												  {  
												  //alert('ok');
													var filterResult = displayFilter(value, actualCount);         
												//alert(filterResult);
												
												$('.'+value).css( "display", filterResult.display); 
												//alert(filterResult.display);
												actualCount = filterResult.actualCount;
												//alert(actualCount);
											  }
											});		
											});
											//alert("#"+site);
											 var countCont = $("#"+site.replace(/\s/g, '1')).children();											 
											 //console.log(JSON.stringify(countCont));
											 $.each( countCont, function() {																							 
											   if($(this).attr('class')=='hd1'){			
												  $(this).html(actualCount  > 9 ? "" + actualCount:"" + actualCount);  
											   } 
											 });											 
											});
		
		})
	})
	function displayFilter(currentsitecolour, actualCount) 
	{
		 var filterResult = {
			'display':'none',
			'actualCount':actualCount,
		 };
		 
		 if(siteFilters[currentsitecolour]) 
		 {    
			filterResult.display = 'block';			
			filterResult.actualCount = parseInt(filterResult.actualCount);;
		 }
		 else 
		 {
		
			filterResult.actualCount = --actualCount;			
		 }
		 
		 return filterResult;
		 
	}

	//alert(JSON.stringify(basedonincidentsjson));
	//alert(JSON.stringify(basedonslajson2));
	//console.log(JSON.stringify(basedonslajson2));

</script>

<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
				<span class="tt-current-page"><liferay-ui:message key="Sites"/></span>
			</div>
		</div>
		<div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" class="btn-primary-tt backButton">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
	</div>
</div>

<!--START DISPLAY SITES CONTAINER-->
<div class="container-fluid tt-site-display">
	<div class="row-fluid">
		<div class="span12">
			<div class="span2 tt-displaySite-title"><liferay-ui:message key="Display Sites:"/></div>
			<div class="span6">
				<div class="row-fluid tt-displaySite-check">
					<div class="span2"><input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/><liferay-ui:message key=" All"/></div>
					<div class="span3"><input type="checkbox" id="checkbox_high" class="tt-displaysite-position" /><liferay-ui:message key=" High Impact"/></div>
					<div class="span4"><input type="checkbox" id="checkbox_moderate" class="tt-displaysite-position" /><liferay-ui:message key=" Moderate Impact"/></div>
					<div class="span3"><input type="checkbox" id="checkbox_normal" class="tt-displaysite-position" /><liferay-ui:message key=" Normal"/></div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container-fluid tt-site-tab-container">
	<div class="row-fluid tt-site-tab-incident-content">
		<c:forEach items="${basedOnSLAListCpe}" var="basedOnSLAListCpe">
			<c:if test="${basedOnSLAListCpe.siteTier eq 'Gold Device'}">		
			<div class="span4 tt-sites-gold-tab pt-20">
			</c:if>
			<c:if test="${basedOnSLAListCpe.siteTier eq 'Silver Device'}">				
			<div class="span4 tt-sites-silver-tab pt-20">
			</c:if>
			<c:if test="${basedOnSLAListCpe.siteTier eq 'Bronze Device'}">
			<div class="span4 tt-sites-bronze-tab pt-20">
			</c:if>
			
				<div class="row-fluid tt-site-tab-header">
					<div class="span12" id="${basedOnSLAListCpe.siteTier.toLowerCase().replaceAll(' ','1')}">
						<div class="hd"><liferay-ui:message key="${basedOnSLAListCpe.siteTier}"/></div>
						<div class="hd1"><liferay-ui:message key="${basedOnSLAListCpe.siteCount}"/></div>
					</div>
				</div>
				<c:if test="${basedOnSLAListCpe.siteTier eq 'Gold Device'}">
					<div class="row-fluid tt-site-gold-content-row">
					<div id="boxgoldsites" class="span12 tt-sites-gold-container">
				</c:if>
				<c:if test="${basedOnSLAListCpe.siteTier eq 'Silver Device'}">
					<div class="row-fluid tt-site-silver-content-row">
					<div id="boxsilversites" class="span12 tt-sites-silver-container">
				</c:if>
				<c:if test="${basedOnSLAListCpe.siteTier eq 'Bronze Device'}">
					<div class="row-fluid tt-site-bronze-content-row">
					<div id="boxbronzesites" class="span12 tt-sites-bronze-container">
				</c:if>
				
							<c:set var="rowcount" value="${1}" />
							<c:set var="lengthofloop" value="${fn:length(basedOnSLAListCpe.slaColorList)}" />
							<c:forEach items="${basedOnSLAListCpe.slaColorList}" var="sitelist" varStatus="loopcount">
								<c:if test="${rowcount == 1}">
									<div class="row-fluid"> 
								</c:if>
								<c:if test="${sitelist.colorCode eq 'red'}">
								<c:choose>
									<c:when test="${rowcount == 1}"><div class="span6 tt-site-column tt-site-column-first a_ttofycredsla wrapword"></c:when>
									<c:otherwise><div class="span6 tt-site-column tt-site-column-second a_ttofycredsla wrapword"></c:otherwise>
								</c:choose>
								
								<div class="namealign">${sitelist.siteName}</div> <div class="peralign">${sitelist.sitePercentileAverage}%</div></div>
									</c:if>

									<c:if test="${sitelist.colorCode eq 'amber'}">
										<c:choose>
											<c:when test="${rowcount == 1}"><div class="span6 tt-site-column tt-site-column-first a_ttofycambersla wrapword"></c:when>
											<c:otherwise><div class="span6 tt-site-column tt-site-column-second a_ttofycambersla wrapword"></c:otherwise>
										</c:choose>
										<div class="namealign">${sitelist.siteName}</div> <div class="peralign">${sitelist.sitePercentileAverage}%</div>
											</div>
									</c:if>

									<c:if test="${sitelist.colorCode eq 'green'}">
										<c:choose>
											<c:when test="${rowcount == 1}"><div class="span6 tt-site-column tt-site-column-first a_ttofycgreensla wrapword"></c:when>
											<c:otherwise><div class="span6 tt-site-column tt-site-column-second a_ttofycgreensla wrapword"></c:otherwise>
										</c:choose>
										<div class="namealign">${sitelist.siteName}</div> <div class="peralign">${sitelist.sitePercentileAverage}%</div>
										</div>
									</c:if>
							 <c:choose>
								<c:when test="${lengthofloop == 1}"></div></c:when>
								<c:otherwise>
									<c:choose>
										<c:when test="${lengthofloop -1 == loopcount.index}"></div></c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${rowcount == 2}"></div>
													<c:set var="rowcount" value="${1}" />
												</c:when>
												<c:otherwise><c:set var="rowcount" value="${rowcount+rowcount}" />
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							 </c:choose>
							</c:forEach>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>


<!--END SITE PAGE CONTAINER-->