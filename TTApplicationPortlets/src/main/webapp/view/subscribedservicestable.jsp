<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="subscribedservicesTableURL" var="subscribedservicesTableURL"/>
<portlet:resourceURL id="assignUserToServiceURL" var="assignUserToServiceURL"/>

<link href="${pageContext.request.contextPath}/css/ttLogServiceRequest.css" rel="stylesheet"/>	
<style>
@media ( max-width : 500px) {
	td+td+td+td+td {
		display: none;
	}
	th+th+th+th+th {
		display: none;
	}
	.tt-subtitle{
	display:none !important;
	}
	
	#displayServicesFilter{
	margin-left:10px !important;
	}
}
</style>
<script>
var selected = ['all'];
$( document ).ready(function() {
	
	$('#myModal').hide();
	
	$("input[type='checkbox']").change(function() {
		selected = [];
		$('div#displayServicesFilter input[type=checkbox]').each(function() {
		   if ($(this).is(":checked")) {
			   selected.push($(this).attr('id'));
		   }
		});
		subscribedServicesTable();
	});
});

function getServiceAutoPopulatedDetails(link){
	var service= link.getAttribute("name");
	$("#serviceName").val(service);

}

/* Define two custom functions (asc and desc) for string sorting */
jQuery.fn.dataTableExt.oSort['string-case-asc']  = function(x,y) {

	var a= parseFloat(x.split('%')[0]);
	var b= parseFloat(y.split('%')[0]);
	return ((a < b) ? -1 : ((a > b) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['string-case-desc'] = function(x,y) {
	var a= parseFloat(x.split('%')[0]);
	var b= parseFloat(y.split('%')[0]);
	return ((a < b) ?  1 : ((a > b) ? -1 : 0));
};

function subscribedServicesTable()
{
	var subscribedservicesTableURL = "${subscribedservicesTableURL}"+"&serviceFilters="+selected;
	$.ajax({
		url: subscribedservicesTableURL,
		type: "POST",
		dataType: 'json',
		success : function(data)
		{
			$('#subscribedServicesTable').DataTable({
				"aaData" : data,
				"destroy": true,
				"bFilter" : false,
				"processing" : false,
				"bPaginate" : true,
				"pageLength": 7,
				"serverSide" : false,	
				"bLengthChange" : false,
				"bSortable" : false,
				"bInfo": false,
				"aoColumns": [
									null,
									null,
									null,
									null,
									{ "sType": 'string-case' },
									null
								],
				'columnDefs': [{
					 'targets' : 5,
					 'className': 'dt-body-center',
					 'render': function (data, type, full, meta){
							 var isPortalTTAdmin = full[5].split("-");
							
								
						 		if(isPortalTTAdmin[2] == "true")
						 		{
						 			if(isPortalTTAdmin[0] == "None")
								 	{
								 		return '<a href="#myModal" data-toggle="modal" data-target="#myModal" id="assignContact'+full[0]+'" name="'+full[0]+'" onclick="getServiceAutoPopulatedDetails(this);"><liferay-ui:message key="Assign a Name"/></a>';
								 	}
								 	else 
								 	{
								 		if(isPortalTTAdmin[0]=='NA')
								 		{
								 			return isPortalTTAdmin[0];
								 		}
								 		else
										{
											return '<a href="#myModal" data-toggle="modal" data-target="#myModal" id="assignContact'+full[0]+'" name="'+full[0]+'" title="'+isPortalTTAdmin[1]+'" onclick="getServiceAutoPopulatedDetails(this);">'+isPortalTTAdmin[0]+'</a>';
								 		}
								 	}
						 		}
						 		else
						 		{
						 			return '<label title="'+isPortalTTAdmin[1]+'">' + isPortalTTAdmin[0] + '</label>';
								}
							}
						}],
					'createdRow': function(row, data, dataIndex) {
						// alert("row :: " + row);
						// alert("data[4] :: " + data[4]);
						// alert("dataIndex :: " + dataIndex);
					
						var performanceData=data[4].split("%");
						
						 if(performanceData[0] >= 98.9)
						 {
							$(row).css('color','#a3cf62').css('font-style','bold');
						 }
						 else if(performanceData[0] > 98.5 && performanceData[0] < 98.9)
						 {
							 $(row).css('color','#ff9c00').css('font-style','bold');
						 }
						 else if(performanceData[0] >= 0 && performanceData[0] <= 98.5)
						 {
							 $(row).css('color','#e32212').css('font-style','bold');
						 }
						 else
						 {
							 $(row).css('color','#555').css('font-style','bold');
						 }
					 },
					'order': [[4, "asc"]]
				});
				
			},
		error: function()
		{
			//alert("error");
		}
	});
}

function assignUserToServiceAjax()
{
	var userNameValue = $('#userSelect').val();
	var serviceNameValue = $('#serviceName').val();
	var assignUserNameToService = "${assignUserToServiceURL}";
	var finalURL = assignUserNameToService + "&username=" + userNameValue + "&servicename=" + serviceNameValue;
	//alert("final :: " + finalURL);
	$.ajax({
	    url : finalURL,
	    type : "POST",
	    dataType : 'json',
	    success : function(response) 
	    {
	    	//alert("success");      
	    },
	    error : function(xhr) 
	    {
	          //alert("error");
	    }
	});
}

</script>
<!--  Start of Subscribed Services Data Table -->
<div class="container-fluid maincontainer subscribedservicestable-container">

	<% 
		if(currentURL.contains(bvDetailsURL))
		{
	%>
			<div id="displayServicesFilter">
				<div class="span2">
					<input type="checkbox" id="all" name="serviceId" class="serviceClass" checked>
					<liferay-ui:message key="All"/>
				</div>
				<c:forEach var="services" items="${servicesFilter}">
					<div class="span2" style="margin-left:0px !important;">
						<input type="checkbox" id="${services}" name="serviceId" class="serviceClass">
						<liferay-ui:message key="${services}"/>
					</div>
				</c:forEach>
			</div>
	<%
		}
	%>
	
	<table id="subscribedServicesTable" class="display tt-subscribedServicesTable-main" cellspacing="0" width="100%">
		<thead class="subscribedServicesTable-head">
		    <tr>	
				<th class="tableSelect"><liferay-ui:message key="Services"/></th>
		        <th class="tablefirstname"><liferay-ui:message key="Remaining (Contract Term)"/></th>
		        <th class="tableworkcontact"><liferay-ui:message key="Status"/></th>
		        <th class="tableworkcontact"><liferay-ui:message key="Capacity"/> / <liferay-ui:message key="Volume"/></th>
		        <th class="tablelastname"><liferay-ui:message key="Performance (Commitment)"/></th>
		        <th class="tablehomecontact"><liferay-ui:message key="Contact"/></th>
		    </tr>
		</thead>
	</table>
</div>
<!--  End of Subscribed Services Data Table -->


<!--Start of Assign Name Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
	 
      <div class="modal-header">
         <button type="button" class="close" id="closeMarkID" data-dismiss="modal">
		     <img class="closeMark" src="<%=request.getContextPath()%>/images/Close_G_24.png" />
	     </button>
        <div class="modal-title modalTitle">
             <liferay-ui:message key="Add a Contact" />
        </div>
      </div>
      
      <div class="modal-body">
	  
	  <form id="assignUserForm" name="assignUserForm" class="form-horizontal tt-modal-form" method="post">
	  
        <div class="container-fluid tt-form-container">

	   		<div class="row-fluid tt-form-row">
	    		<div class="span3 tt-form-left-col">
	                 <liferay-ui:message key="Service Name:*" />
	             </div>
                 <div class="span9 tt-form-right-col">
                        <input id="serviceName" name="serviceName" type="text" class="serviceName" tabindex="1" readonly="readonly"/>
                 </div>
            </div>
			<div class="row-fluid tt-form-row">
	        	<div class="span3 tt-form-left-col">
	            	<liferay-ui:message key="User:*" />
                </div>
                <div class="span9 tt-form-right-col">
                       <select name="user" id="userSelect" class="selectBoxLogInc" onchange="getUser(this)" tabindex="2">
							<c:forEach var="item" items="${usersList}">
								<c:set var="userAndEmail" value="${fn:split(item, '-')}"/>
								<option value="${item}">${userAndEmail[0]}</option>
							</c:forEach> 
	                    </select>
                 </div>
	         </div>
			 <div class="row-fluid tt-form-last-row pr-15 pt-30">
	         	<div class="span12 tt-submit-row">
	            	<span class="tt-submit">
	                
	                    <button type="submit" class="btn-primary-tt img-submit-servicerequestenabled"
	                           id="assignButtonId" tabindex="12" onclick="assignUserToServiceAjax()">
	                           <liferay-ui:message key="Assign" />
	                    </button> 
                     </span>
					 <span class="tt-cancel pr-10">
	                     <button type="button" class="btn-secondary-tt img-cancel"
	                            id="cancelButtonID" data-dismiss="modal" tabindex="13">
	                            <liferay-ui:message key="Cancel"/>
	                     </button>
	                 </span> 
	          	</div>
	   		</div>
	      </div>
		</form>
      </div>
 </div>
</div>
</div>
<!--End of Assign Name Modal -->