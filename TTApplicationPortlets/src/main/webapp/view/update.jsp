<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<!-- 
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-modal.js"></script>
<script
	src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script> -->

<script src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js"
	type="text/javascript"></script>
<script
	src="<%=request.getContextPath()%>/js/jquery.validate-1.9.min.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/ttrefresh.css">

<%@ include file="common.jsp"%>

<portlet:actionURL name="refresh" var="processURL" />
<script type="text/javascript">
function openSnow(){

	var win = window.open('https://telkomtelstratest.service-now.com/', '_blank');
if(win){
    //Browser has allowed it to be opened
    win.focus();
}else{
    //Broswer has blocked it
    alert('Please allow popups for this site');
}
}
function openSnowESS(){

	var win = window.open('https://telkomtelstratest.service-now.com/ess/', '_blank');
if(win){
    //Browser has allowed it to be opened
    win.focus();
}else{
    //Broswer has blocked it
    alert('Please allow popups for this site');
}
}

function submitFm() {

	document.getElementById("refreshButton").disabled = 'disabled';
	document.getElementById("refreshButton").setAttribute("class", "updateButtonDisable");
	
	var url = '<%=processURL.toString()%>';
		document.forms["fm"].action = url;
		document.forms["fm"].submit();
	}
</script>
<style>
.span6.tt-breadcrumb.dashboardTitle {
	position: relative;
	top: 12px;
}

.tt-lastupdate-timestamp-device {
	display: none;
}

.tt-dashboard-header {
	background-color: #ffffff;
	border-bottom: 2px solid #cccccc;
	padding: 20px;
}

.tt-update {
	text-align: right;
}

.refresh {
	background: url("/TTPortalTheme/images/telkom/refresh.png") no-repeat
		10px 10px #555555;
	background-color: #555555;
	padding: 10px 10px 10px 35px;
	border: 0;
}

.tt-lastupdate-timestamp, .tt-lastupdate-btn {
	display: inline-block;
}

@media ( min-width : 768px) and (max-width: 820px) {
	.tt-dashboard-header {
		padding: 20px !important;
	}
	.tt-breadcrumb {
		width: 40% !important;
		float: left !important;
	}
	.tt-update {
		width: 57% !important;
		float: left !important;
	}
}

@media ( min-width : 566px) and (max-width: 767px) {
	.tt-dashboard-header {
		padding: 20px !important;
	}
	.tt-breadcrumb {
		width: 40% !important;
		float: left !important;
	}
	.tt-update {
		width: 60% !important;
		float: left !important;
	}
}

@media ( min-width : 320px) and (max-width: 565px) {
	.tt-dashboard-header {
		padding: 20px !important;
	}
	.tt-breadcrumb {
		width: 100% !important;
		float: left !important;
	}
	.tt-update {
		padding-top: 10px;
		width: 100% !important;
		float: left !important;
		text-align: left;
	}
	.dashImageHolder {
		margin-left: 0px !important;
	}
}

@media ( min-width : 320px) and (max-width: 565px) {
	.tt-lastupdate-timestamp {
		display: none;
	}
	.tt-lastupdate-timestamp-device {
		display: inline-block;
	}
	.tt-lastupdate-btn {
		float: right;
	}
}
</style>
<form name="fm" method="Post">
	<div class="container-fluid tt-dashboard-header">
		<div class="row-fluid">
			<div class="span6 tt-breadcrumb dashboardTitle">
				<div class="dashImageHolder">
					<img class=""
						src="<%=request.getContextPath()%>/images/DashBoard_G_32.png" />
				</div>
				<div class="dashboardTitleHolder">
					<div class="dashboardText">
						<liferay-ui:message key="Dashboard" />

					</div>


				</div>

			</div>

			<div class="span6 tt-update">
				<div class="tt-lastupdate-timestamp">
					<liferay-ui:message key="Last updated on" />
					&nbsp;${lastRefreshDate}
				</div>
				<div class="tt-lastupdate-timestamp-device">
					<liferay-ui:message key="Last updated on" />
					<br>${lastRefreshDate}</div>
				<div class="tt-lastupdate-btn">
					<%
						if (allowAccess) {
					%>
					<button type="button" id="refreshButton"
						class="btn-primary-tt refresh" onclick="submitFm()">
						<liferay-ui:message key="Update Data" />
					</button>
					<%
						} else {
					%>
					<div class="updateButtonHolder">
						<button type="button" id="refreshButton"
							style="background-color: #CCC !important; color: white !important;"
							class="btn-primary-tt refresh" title="Contact Administrator">
							<liferay-ui:message key="Update Data" />
						</button>
					</div>
					<%
						}
					%>
				</div>
			</div>
		</div>
	</div>
</form>