<%--
/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>


<%@page import="javax.portlet.PortletSession"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<!-- <script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-modal.js"></script> -->
<portlet:defineObjects />

<portlet:resourceURL id="getGraphData" var="getGraphData"></portlet:resourceURL>


<html>
<head>

<!-- <script src="https://code.jquery.com/jquery-1.10.2.js"></script> -->
</head>
<body>


<div id="ticketgridbody">

<c:if test = "${statusParameter == 'completed'}">
<div id="incTrendsHeading"><liferay-ui:message key="Incident Trend"/></div>
<div id="incTrendsShortDescription"><liferay-ui:message key="Incidents against time, showing number of Incidents raised each week in a duration of last three months."/></div>

<div id="varianttrendbody">
<div class="graphBlocks">
	<div id="incidentcategorysites" class="varianttrends">
		<div class="varianttrendhead"><div class="variantcategory"></div><liferay-ui:message key="Sites"/><div class="variantcategorycount"></div></div>
		<div class="graphOverlay">

		</div>

		<div class="graphStatistics" id="graphStatisticsSites">
		</div>
		<div class="xMarkersGraph"></div>
	</div>

	<div id="incidentcategorynetwork" class="varianttrends">
		<div class="varianttrendhead"><div class="variantcategory"></div><liferay-ui:message key="Network"/><div class="variantcategorycount"></div></div>
		<div class="graphOverlay">

		</div>

		<div class="graphStatistics" id="graphStatisticsNetwork">
		</div>
		<div class="xMarkersGraph"></div>
	</div>
</div>
<div class="graphBlocks">
	<div id="incidentcategorycloudinfrastructure" class="varianttrends">
		<div class="varianttrendhead"><div class="variantcategory"></div><liferay-ui:message key="Cloud Infrastructure"/><div class="variantcategorycount"></div></div>
		<div class="graphOverlay">

		</div>

		<div class="graphStatistics" id="graphStatisticsCloudInfrastructure">
		</div>
		<div class="xMarkersGraph"></div>
		
	</div>

	<div id="incidentcategorysaas" class="varianttrends">

		<div class="varianttrendhead"><div class="variantcategory"></div><liferay-ui:message key="SaaS"/><div class="variantcategorycount"></div></div>
		<div class="graphOverlay">

		</div>

		<div class="graphStatistics"  id="graphStatisticsSaaS">
		</div>
		<div class="xMarkersGraph"></div>
	</div>
</div>
</div>

</c:if>



<br>
<table style="width:100%">
<tr>
<td align="left" style="text-align:left;width:95%;" class="ttheadingclassshown">
<div class="titleTicketGrid">
		<liferay-ui:message key="Incident & Service Matrix"/>
</div>
</td>
<td align="right" class="ttheadingclassshown">
<a href="#" class="shown" style="text-decoration:none;"></a>
</td>
</tr>
</table>

<div class="collapse1" id="collapsei1">
<div class="ticketServiceSubHeading">
	<liferay-ui:message key="Click on a cell to filter results."/>
</div>

<div id="tablematrixdiv">
<table border="1" id="myTableData" width="100%">
	<tr>
		<td rowspan="2" id="serviceTD"><liferay-ui:message key="Service Type"/></td>

		<td colspan="4" style="width: 40%;border-right:#ccc;"><a href="javascript:void(0);" id="allServices" aria-label="P1&P2&P3&P4&" class="With Impacts&" style="display:block;height:50px;width:99%;text-decoration:none;text-align:center;line-height:50px;color:#333;" onclick="generateTicketList(this)"><liferay-ui:message key="Incident Priority"/></a>
		</td>

		<%-- <td colspan="5" style="width: 40%;"><a href="javascript:void(0);" id="allServices" aria-label="P1&P2&P3&P4&P5&" class="At Risk&" style="display:block;height:50px;width:99%;text-decoration:none;text-align:center;line-height:50px;color:#333;" onclick="generateTicketList(this)"><liferay-ui:message key="At Risk(No Impacts)"/></a></td>
 --%>
		<td rowspan="2" id="totalTD"  style="border-left:#ccc;"><liferay-ui:message key="Total"/></td>

	</tr>

	<tr>

		<td id="P1i">Priority 1</td>
		<td id="P2i">Priority 2</td>
		<td id="P3i">Priority 3</td>
		<td id="P4i">Priority 4</td>
		<!-- <td id="P5i">P5</td> -->

		<!-- <td id="P1r">P1</td>
		<td id="P2r">P2</td>
		<td id="P3r">P3</td>
		<td id="P4r">P4</td>
		<td id="P5r">P5</td> -->

	</tr>
	
<c:forEach items="${serviceGridList}" var="serviceGridList">
	<tr>
		<td class="serviceTDclass" style="border-right:#ccc;">${serviceGridList.serviceName}</td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P1&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p1WithImpacts}</a></td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P2&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p2WithImpacts}</a></td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P3&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p3WithImpacts}</a></td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P4&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p4WithImpacts}</a></td>
		<%-- <td style="border-right:#ccc;"><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P5&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p5WithImpacts}</a></td> --%>
		<%-- <td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P1&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p1AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P2&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p2AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P3&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p3AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P4&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p4AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P5&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.p5AtRisk}</a></td> --%>
		<td><a href="javascript:void(0);" id="${serviceGridList.serviceName}" aria-label="P1&P2&P3&P4&" class="With Impacts&At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${serviceGridList.totalIncidents}</a></td>
	</tr>

	
	</c:forEach>
	
	<tr>
		<td class="totalTDclass">${totalIncidentRow.serviceName}</td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P1&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p1WithImpacts}</a></td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P2&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p2WithImpacts}</a></td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P3&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p3WithImpacts}</a></td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P4&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p4WithImpacts}</a></td>
		<%-- <td style="border-right:#000;"><a href="javascript:void(0);" id="allServices" aria-label="P5&" class="With Impacts&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p5WithImpacts}</a></td> --%>
		<%-- <td><a href="javascript:void(0);" id="allServices" aria-label="P1&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p1AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P2&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p2AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P3&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p3AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P4&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p4AtRisk}</a></td>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P5&" class="At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.p5AtRisk}</a></td> --%>
		<td><a href="javascript:void(0);" id="allServices" aria-label="P1&P2&P3&P4&" class="With Impacts&At Risk&" style="display:block;height:50px;width:98%;text-decoration:none;color:#333;text-align:center;line-height:50px;" onclick="generateTicketList(this)">${totalIncidentRow.totalIncidents}</a></td>
	</tr>
	
</table>
</div>
</div>
</div>
</body>
</html>


<script>
var getGraphDataURL = "${getGraphData}";
$( document ).ready(function() {

	graphMaker("sites");
	graphMaker("network");
	graphMaker("cloudinfrastructure");
	graphMaker("saas");

	$(".shown").click(function(){
		 			
		$(".collapse1").slideToggle("slow");
		$(".shown").toggleClass("tthidden");
		$(".ttheadingclassshown").toggleClass("ttheadingclasshidden");
			     	     
	});
	 
});
  
  
  	function graphMaker(obj){
  		
  		var jsonObj = {
  				"incidentCategory" : obj
  			};

  			var graphData = {
  				graphDataIdentifier : JSON.stringify(jsonObj)
  			};

  			$.ajax({
  				type : "POST",
  				url : getGraphDataURL,
  				dataType : 'json',
  				data : graphData,
  				success : function(data) {
					
  					drawBarFillGraph(data,obj,90,"incidentcategory");
					
  				},
  				error : function(data) {
  					alert("Error in fetching data");
  				}

  			});
  			return false;
  	}
	

</script>

<style>

.startXMarker {
  display: inline;
  position: relative;
  left: -18px;
}

.midXMarker {
  display: inline;
  position: relative;
  left: 48px;
}

.endXMarker {
  display: inline;
  position: relative;
  left: 191px;
}

.basbarlayout{
	position:absolute;
}

.basebar{
	display:inline-block;
	width:10px;
	height:90px;
	background-color:#ccc;
	margin-right:8px;
}

.markbarlayout{
	position:relative;
}

.markbar{
	display:inline-block;
	width:10px;
	background-color:#000;
	margin-right:8px;
}

.uppermarker {
    position: relative;
    bottom: 60px;
}

.midmarker {
    position: relative;
}

.graphStatistics
{
    display: inline-block;
    height: 60px;
    position: relative;
    top: 4px;
    right: 12px;
}



@media screen and (max-width:1152px){
	.graphBlocks{
		display:flex;
		justify-content:center;
	}
}

@media screen and (min-width:1153px){
	.graphBlocks{
		display:inline-flex;
	}
	#varianttrendbody{
		display:flex;
		justify-content:center;
	}
}

@-moz-document url-prefix() {
    #graphStatisticsSites
	{
		top: 0px;
	}
	#graphStatisticsNetwork{
		top:0px;
	}
	#graphStatisticsCloudInfrastructure{
		top:0x;
	}
	#graphStatisticsSaaS{
		top:0px;
	}
	.graphStatistics
	{
		display: inline-table;
		
	}
}

@media screen and (-webkit-min-device-pixel-ratio:0)
{ 
    #graphStatisticsSites
	{
		top: 0px;
	} 
	#graphStatisticsNetwork{
		top:0px;
	}
	#graphStatisticsCloudInfrastructure{
		top:0px;
	}
	#graphStatisticsSaaS{
		top:0px;
	}
	.graphStatistics
	{
		display: inline-table;
	}
}

.bottommarker {
    position: relative;
}

.variantcategory{
	display:inline;
}

.variantcategorycount{
	display:inline;
	float: right;
}

.varianttrends
{
	display: inline-block;
}

.varianttrendhead{
	padding:10px 10px 10px 0;
	font-weight:bold;
}

.graphOverlay{
	display:inline-block;
}



#incTrendsHeading{
    padding: 10px 0 5px 0;
    font-weight: 600;
    color: #333;
}

#incTrendsShortDescription{
	color: #555;
}

.nospacediv{
	height:0px !important;
}

#P1i {
	background: #e32212;
	color: #000000;
	height: 50px;
	text-align: center;
	border-right:solid 1px #ffffff;
}

#P2i {
	background: #ff9c00;
	color: #000000;
	height: 50px;
	text-align: center;
	border-right:solid 1px #ffffff;
}

#P3i {
	background: #ffff00;
	height: 50px;
	text-align: center;
	color: #000000;
	border-right:solid 1px #ffffff;
}

#P4i {
	background: #a3cf62;
	height: 50px;
	text-align: center;
	color: #000000;
	border-right:solid 1px #cccccc;
}

</style>