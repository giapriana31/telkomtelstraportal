<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>




<script src="<%= request.getContextPath() %>/js/jquery-1.11.3.min.js"></script>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="common.jsp" %>

<portlet:actionURL name="actionMethod1" var="sampleActionMethodURL">
</portlet:actionURL>
<!-- <script type="text/javascript">
       alert('Notes - '+ "${incident.notes}");
</script> -->
<script type="text/javascript">
	
</script>
<c:set var="ServiceNameListRAW"
	value="${incident.affectedServiceString}" />
<c:set var="comma" value="," />
<c:set var="newLine" value="\n" />
<c:set var="ServiceNameList"
	value="${fn:replace(ServiceNameListRAW,comma,'<br>')}" />
<style>

a#backToIncidentList {
  display: block;
  color: #fff;
  text-decoration: none;
  background: #555;
  width: 160px;
  padding: 5px;
  border-radius: 2px;
}

#back_to_tickets_button {
	background: url(<%=request.getContextPath ()%>/images/tickets/u5.png)
		no-repeat;
	height: 38px;
	width: 193px;
	color: white;
	border: none;
	position: relative;
}

#back_to_tickets_button_disabled {
	background-color: #CCC;
	height: 38px;
	width: 193px;
	color: white;
	border: none;
	position: relative;
}
</style>

<form action="${sampleActionMethodURL}" method="post">
	<div class="container-fluid tt-updateticket-header">
		<div class="row-fluid pt-20 tt-header-row">
			<div class="span12">
				<div>
					<div id="ticketIcon"></div>
					<div id="ticketTitle">
						<liferay-ui:message key="Incidents" />
					</div>
				</div>
				<div id="activeTickets" class="pt-15">
						<div class="activeTicketsText">
						<c:choose>
						<c:when test = "${checkStatusOfIncident == 'active'}">
							<liferay-ui:message key="Active Incidents" />
						</c:when>
						<c:otherwise>
							<liferay-ui:message key="Completed Incidents" />
						</c:otherwise>
						</c:choose>
						</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid tt-updateticket-ctnr-header pt-20">
		<div class="row-fluid">
			<div class="span12">
				<div class="pb-15"><liferay-ui:message key="Incident Details" /></div>
				<div>
				<c:choose>
						<c:when test = "${checkStatusOfIncident == 'active'}">
							<a id="backToIncidentList" href='<portlet:actionURL name="incidentStatusSender"><portlet:param name="status" value="active"/></portlet:actionURL>'><&nbsp;<liferay-ui:message key="Back to Incident List"/></a>
						</c:when>
						<c:otherwise>
							<a id="backToIncidentList" href='<portlet:actionURL name="incidentStatusSender"><portlet:param name="status" value="completed"/></portlet:actionURL>'><&nbsp;<liferay-ui:message key="Back to Incident List"/></a>
						</c:otherwise>
				</c:choose>
				</div>
			</div>
		</div>	
	</div>
	<div class="container-fluid tt-updateticket-container">
		<div class="row-fluid">
			<div class="span12 tt-main-container">
				<div class="row-fluid tt-IncidentSummary-header">
					<div class="span12 pt-15 summaryStyle" style="height: auto !important;word-break: break-all;">${incident.summary}</div>
				</div>
				<div class="row-fluid">
					<div class="span4 pt-15 tt-data-col">
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"> <liferay-ui:message key="Incident Id:"/></div>
								<div class="tt-ans">${incident.incidentid}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Priority:"/></div>
								<div class="tt-ans">P${incident.priority}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Impact:"/></div>
								<div class="tt-ans">${incident.impact}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Incident created on:"/></div>
								<div class="tt-ans" ><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" 
            value="${incident.reporteddatetime}" /></div>
							</div>
						</div>
						
					</div>
					<div class="span4 pt-15 tt-data-col">
						<div class="row-fluid tt-data-row">
							<div class="span12 ">
								<div class="tt-label" ><liferay-ui:message key="Status:"/></div>
								<div class="tt-ans">${incident.incidentstatus}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Service level target:"/></div>
								<div class="tt-ans"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" 
            value="${incident.serviceleveltarget}" /></div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label" ><liferay-ui:message key="Urgency:"/></div>
								<div class="tt-ans">${incident.urgency}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Last updated on" />:</div>
								<div class="tt-ans"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" 
            value="${incident.lastupdated}" /></div>
							</div>
						</div>
					</div>
					<div class="span4 pt-15 tt-data-col">
						<div class="row-fluid tt-data-row">
							<div class="span12">
								
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="State:"/></div>
								<div class="tt-ans">
								<c:choose>
									<c:when test="${incident.customerimpacted eq true}">
										<liferay-ui:message key="With Impact"/>
									</c:when>    
									<c:otherwise>
										<liferay-ui:message key="At Risk"/>
									</c:otherwise>
								</c:choose></div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Major Incident:"/></div>
								<div class="tt-ans">${incident.MIM}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<button type="button" class="labelMore" id="moreLabel"
									onclick="hiddenToggle()">
									<div id="more-button-content"></div>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid" id="customerDetails">
					<div class="span4 tt-data-col">
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Logged By:"/></div>
								<div class="tt-ans">${incident.caller}</div>
							</div>
						</div>
						<c:if test="${isSaasProduct=='false'}">
							<div class="row-fluid tt-data-row">
								<div class="span12">
									<div class="tt-label"><liferay-ui:message key="Site Name:"/></div>
									<div class="tt-ans">${incident.sitename}</div>
								</div>
							</div>
						</c:if>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Service Name:"/></div>
								<div class="tt-ans">${ServiceNameList}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Category:"/></div>
								<div class="tt-ans">${incident.category}</div>
							</div>
						</div>
					</div>
					<div class="span4 tt-data-col">
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Case Manager:"/></div>
								<div class="tt-ans">${incident.casemanager}</div>
							</div>
						</div>
						<c:if test="${isSaasProduct=='false'}">
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Site Zone:"/></div>
								<div class="tt-ans">${incident.sitezone}</div>
							</div>
						</div>
						</c:if>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Device Name:"/></div>
								<div class="tt-ans">${incident.affectedResourceString}</div>
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Sub Category:"/></div>
								<div class="tt-ans">${incident.subcategory}</div>
							</div>
						</div>

					</div>
					<div class="span4 tt-data-col">
						<div class="row-fluid tt-data-row">
							<div class="span12">
								
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								<div class="tt-label"><liferay-ui:message key="Service Tier:"/></div>
								<div class="tt-ans">${incident.siteservicetier}</div>
							</div>
						</div>
						
						<div class="row-fluid tt-data-row">
							<div class="span12">
								
							</div>
						</div>
						<div class="row-fluid tt-data-row">
							<div class="span12">
								
							</div>
						</div>

					</div>
				</div>
				<div class="row-fluid"> 
					<div class="span12 tt-data-col-desc tt-incident-description">
						<div class="row-fluid">
							<div class="span6">
								<div class="tt-label"><liferay-ui:message key="Incident description:"/></div>
								<div class="tt-ans">${incident.notes}</div>
							</div>
							<div class="span6">
								<div class="row-fluid">
									<div class="span12">
										<div class="tt-label"><liferay-ui:message key="Comments:"/></div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span12 tt-comment-list">
										<c:forEach items="${incident.comments}" var="note">
										<div class="row-fluid tt-comment-list-row">
											<div class="span1">
												<img src="<%=request.getContextPath()%>/images/tickets/u33.png" height="40" width="40"></img>
											</div>
											<div class="span11 pl-10">
												<div class="row-fluid">
													<div class="span4 tt-posted-by">${note.name}</div>
													<c:set var="commentTime" value="${note.time}"/>
													<div class="span8 tt-posted-timestamp">${fn:replace(commentTime,'GMT', 'JKT')}</div>
												</div>
												<div class="row-fluid">
													<div class="span12 tt-comment-text">
														<c:set var="noteRecieved" value="${note.note}"/>
															<c:set var="finalNote" value="${fn:replace(noteRecieved, '(Additional comments)', '')}" />
															${finalNote}
													</div>
												</div>
											</div>
										</div>
										</c:forEach>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span12 tt-comment-input">

										<c:choose>
											<c:when
												test="${incident.category eq 'Network' || incident.category eq 'network' || checkStatusOfIncident != 'active'}">
												<textarea id="textarea" class="tt-textarea"  name="<portlet:namespace />note_ticket"
												maxlength="1000" disabled></textarea>
											</c:when>
											<c:otherwise>
												<textarea id="textarea" class="tt-textarea"  name="<portlet:namespace />note_ticket"
													maxlength="1000" placeholder="<liferay-ui:message key="Click to add comment"/>"></textarea>
											</c:otherwise>
										</c:choose> 
									</div>
								</div>
								<div class="row-fluid">
									<div class="span12">
										<c:choose>
											<c:when test="${incident.category eq 'Network' || incident.category eq 'network' || checkStatusOfIncident != 'active'}">


												<input type="submit" name="button" value="+ &nbsp;<liferay-ui:message key="Add Comments"/>"
													class="btn-primary-tt btn-add-Comments" id="back_to_tickets_button_disabled"
													disabled="disabled" />

				
											</c:when>
											
											<c:otherwise>
									
												<%if(allowAccess)
												{
												%>
													<input type="submit" name="button" value="+ &nbsp;<liferay-ui:message key="Add Comments"/>"
														class="btn-primary-tt btn-add-Comments" id="back_to_tickets_button" />
												<%
												}
												else
												{
												%>
													<input type="button" name="button" value="+ &nbsp;<liferay-ui:message key="Add Comments"/>"
														class="btn-primary-tt btn-add-Comments" id="back_to_tickets_button"
														style="background-color: #CCC !important; color: white !important;"
														title="Contact Administrator" />
												<%
												}
												%>
											</c:otherwise>
										</c:choose>
									</div>
									
									
									<div>
										<INPUT TYPE="hidden"
											NAME="<portlet:namespace />userid_ticket" readonly="readonly"
											VALUE="<%=themeDisplay.getUser().getExpandoBridge()
                                  			.getAttribute("CustomerUniqueID")%>">
									</div>
									<div><INPUT TYPE="hidden"
											NAME="<portlet:namespace />name_ticket" readonly="readonly"
											VALUE="<%=user.getFirstName() + " " + user.getLastName()%>"></div>
									<div><INPUT TYPE="hidden"
											NAME="<portlet:namespace />incidentID_ticket" readonly="readonly"
											VALUE="${incident.incidentid}"></div>
									
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
</form>

<script>
var isClicked=0;
	$(document).ready(function() {

		$("#more-button-content").html('<span style="padding:5px;"><liferay-ui:message key="More Details"/></span><img height="30px" src="<%=request.getContextPath()%>/images/more.png"/>');

		$("#customerDetails").hide();
	});

	/* $("#moreLabel").click(function() {
	       $("#hiddenDetails").toggle();

	}); */

	function hiddenToggle() {

		$("#customerDetails").slideToggle();
		isClicked=isClicked+1;
		if((isClicked%2)!=0){
		$("#more-button-content").html('<span style="padding:5px;"><liferay-ui:message key="Less Details"/></span><img height="30px" src="<%=request.getContextPath()%>/images/less.png"/>');
		}
		else{
		$("#more-button-content").html('<span style="padding:5px;"><liferay-ui:message key="More Details"/></span><img height="30px" src="<%=request.getContextPath()%>/images/more.png"/>');
		}


		
	}
</script>