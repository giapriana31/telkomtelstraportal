<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />
<script src="<%=request.getContextPath()%>/js/bootstrap-3.3.4.min.js"></script>
<%@ include file="common.jsp"%>

<portlet:resourceURL id="getPercentage" var="getPercentage">
</portlet:resourceURL>

<style>

#availabilityPortletTitleBar{
       height:37px;
}

#availabilityTitleText{
       line-height:34px;
}

.chromeBoxSLA{
	display:flex;
	justify-content:center;
}

.safariBoxSLA{
	display:-webkit-flex;
}



.sla-service-names{
	align-self:center;
}

.sla-boxes-service {
    display: -webkit-flex;
    -webkit-align-items:center;
    
    width: 21%;
    text-align: center;
    margin: 1% 1% 2% 1%;
    color: white;
    word-wrap: break-word;
    vertical-align: middle;
    justify-content: center;
    min-height: 45px;
    padding: 0 1%;
}
.progress {
	margin: 0px 5px;
	position: relative;
	top: 5px;
}
</style>

<body>
	<div
		style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
		<div id="availabilityPortletTitleBar" class="portletTitleBar">
			<div id="availabilityTitleText" class="titleText">
				<liferay-ui:message key="AVAILABILITY" />
			</div>
			<div class="navigationArrow">
				<a class="navArrowRight"
					href='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttsladetailsPageName")%>'>
					<img class="navArrowRightImg"
					src="<%=request.getContextPath()%>/images/Expand_G_24.png" />
				</a>
			</div>
		</div>
	</div>
	<c:if test="${checkData}">


		<div id="SLABoxesSection">
			<div id="SLA_graph">
				<div class="graphContainer">
					<div class="progress">
						<div class="progress-bar" role="progressbar"
							style="width:${percentage}%; background-color:${graphColor}"
							id="dashboardSLAGraph"></div>
						<div class="progress-bar progress-bar-grey" role="progressbar"
							style="width:${remPercentage}%"></div>
					</div>
				</div>
				<%--<div id="dashboardData"><span id="status">Status</span><span id="dashboardPercentage">${percentage}%</span></div> --%>
			</div>

			<div id="SLABoxDiv">

				
					<c:if test="${checkDataSites}">
						<div class="sla-boxes-service" style="background-color:${SitesColor}">
							<div class="sla-service-names">
								<liferay-ui:message key="Sites" />
							</div>

						</div>
					</c:if>
					<c:if test="${checkDataSites==false}">
						<div class="sla-boxes-service" style="background-color: gray;">
							<div class="sla-service-names">
								<liferay-ui:message key="No Sites Data" />
							</div>

						</div>
					</c:if>
				
				
					<c:if test="${(networkAvailability.color)=='Green'}">
						<div class="sla-boxes-service" style="background-color: #A3CF62;">
							<div class="sla-service-names">
								<liferay-ui:message key="Networks" />
							</div>
						</div>
					</c:if>
					<c:if test="${(networkAvailability.color)=='Red'}">
						<div class="sla-boxes-service" style="background-color: #E32212;">
							<div class="sla-service-names">
								<liferay-ui:message key="Networks" />
							</div>
						</div>
					</c:if>
					<c:if test="${(networkAvailability.color)=='Amber'}">
						<div class="sla-boxes-service" style="background-color: #FF9C00;">
							<div class="sla-service-names">
								<liferay-ui:message key="Networks" />
							</div>
						</div>
					</c:if>
					<c:if test="${(networkAvailability.color)== 'no data'}">
						<div class="sla-boxes-service" id=""
							style="background-color: gray;">
							<div class="sla-service-names">
								<liferay-ui:message key="No Network Data" />
							</div>
						</div>
					</c:if>
				

				<!-- 	box of IaaS -->
				
					<c:choose>
						<c:when test="${IaaSColor == 'green'}">
							<div class="sla-boxes-service" id=""
								style="background-color: #A3CF62;">
								<div class="sla-service-names">
									<liferay-ui:message key="Private Cloud" />
								</div>
							</div>
						</c:when>

						<c:when test="${IaaSColor == 'red'}">
							<div class="sla-boxes-service" id=""
								style="background-color: #E32212;">
								<div class="sla-service-names">
									<liferay-ui:message key="Private Cloud" />
								</div>
							</div>
						</c:when>

						<c:when test="${IaaSColor == 'amber'}">
							<div class="sla-boxes-service" id=""
								style="background-color: #FF9C00;">
								<div class="sla-service-names">
									<liferay-ui:message key="Private Cloud" />
								</div>
							</div>
						</c:when>

						<c:otherwise>
							<div class="sla-boxes-service" id=""
								style="background-color: gray;">
								<div class="sla-service-names">
									<liferay-ui:message key="No Cloud Data" />
								</div>
							</div>
						</c:otherwise>
					</c:choose>
				
				
					<c:choose>
						<c:when test="${SaaSColor == 'Green'}">
							<div class="sla-boxes-service" id=""
								style="background-color: #A3CF62;">
								<div class="sla-service-names">
									<liferay-ui:message key="SaaS" />
								</div>
							</div>
						</c:when>

						<c:when test="${SaaSColor == 'Red'}">
							<div class="sla-boxes-service" id=""
								style="background-color: #E32212;">
								<div class="sla-service-names">
									<liferay-ui:message key="SaaS" />
								</div>
							</div>
						</c:when>

						<c:when test="${SaaSColor == 'Amber'}">
							<div class="sla-boxes-service" id=""
								style="background-color: #FF9C00;">
								<div class="sla-service-names">
									<liferay-ui:message key="SaaS" />
								</div>
							</div>
						</c:when>

						<c:otherwise>
							<div class="sla-boxes-service" id=""
								style="background-color: gray;">
								<div class="sla-service-names">
									<liferay-ui:message key="No SaaS Data" />
								</div>
							</div>
						</c:otherwise>
					</c:choose>
		
			</div>
		</div>
		</div>
		</div>

	</c:if>
	<c:if test="${checkData==false}">
		<div id="noDataDiv">
			<liferay-ui:message key="Data will be displayed from next month." />
		</div>
	</c:if>
</body>

<script>
	$(document).ready(function() {

var ua = navigator.userAgent.toLowerCase(); 
if (ua.indexOf('safari') != -1) { 
  if (ua.indexOf('chrome') > -1) {
    $("#SLABoxDiv").addClass("chromeBoxSLA"); // Chrome
  } else {
    $("#SLABoxDiv").addClass("safariBoxSLA"); // Safari
  }
}
		var color = '${networkAvailability.color}';
		/*if(color.toLowerCase()=='green'){		

			//$("#first-column-dashboard").css("background-color","${SitesColor}");
			$("#second-column-dashboard").css("background-color","#A3CF62");
			
			}

			if(color.toLowerCase()=='red'){		

			$("#second-column-dashboard").css("background-color","#E32212");
			//$("#first-column-dashboard").css("background-color","${SitesColor}");
			}


			if(color.toLowerCase()=='amber'){		

			$("#second-column-dashboard").css("background-color","#FF9C00");
			//$("#first-column-dashboard").css("background-color","${SitesColor}");
			}*/

		$("#dashboardPercentage").css("color", "${graphColor}");
		$("#dashboardSLAGraph").css("background-color", "${graphColor}");

	});
	//document.getElementById("SLA_portlet_container").classList.add("loading-animation");

	var resizeTimeout;
	var isIE = (false || !!document.documentMode);

	function toggleTable() {
		var status = document.getElementById("SLA_table").style.display;
		if (status == 'block') {
			document.getElementById("SLA_table").style.display = "none";
		} else {
			document.getElementById("SLA_table").style.display = "block";
		}
	}
</script>