<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.liferay.taglib.portlet.DefineObjectsTag" %>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.ActionRequest"%>
<%@page import="java.util.ArrayList" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
@media (min-width: 320px) and (max-width: 768px){
	#faqAndTutorialContainer {
    display: block; 
	}
	#FAQHeader{
	margin-left:0px !important;
	width: initial;
	}
	#TMHeader{
	margin-left:0px !important;
	width: initial;
	}
	#tutorialsAndManual{
	width: initial;
	}
	#FAQ{
	width: initial;
	}
	#tmEditButtons {
    float: right;
    position: relative;
    bottom: 70px;
    left:0% !important;
	}
	.faqcntnt{
	margin-left: 12px;
	}
	#FAQContent{
	 padding-bottom: 60px !important;
	}
	
	.tmcntnt{
	 margin-left: 15px;
	display: table-caption;
    position: relative;
    bottom: 21px;
	}
	
}

button {
	border-radius: 5px;
}


</style>
<script src="<%= request.getContextPath() %>/js/jquery-1.11.1.min.js"></script>
<script src="<%= request.getContextPath() %>/js/jquery.confirm.min.js"></script>
<script src="<%= request.getContextPath() %>/js/bootstrap-3.3.4.min.js"></script>

<script src="<%=request.getContextPath()%>/js/jquery.validate-1.11.0.min.js" type="text/javascript"></script>
<script src="<%= request.getContextPath() %>/js/jquery.form-validator.min.js" type="text/javascript"></script>

<%@ include file="common.jsp" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<liferay-ui:success key="tmAddedSuccess"
	message="Tutorial or Manual added successfully" />
	<liferay-ui:success key="tmDeletedSuccess"
	message="Tutorial or Manual deleted successfully" />
	<liferay-ui:error key="tmAddedFailed"
	message="Failed to add Tutorial or Manual"></liferay-ui:error>
	<liferay-ui:error key="tmDeletedFailed"
	message="Failed to delete Tutorial or Manual"></liferay-ui:error>
	<liferay-ui:error key="documentAlreadyExist"
	message="Document with same name already exists."></liferay-ui:error>
<portlet:actionURL name="handleActionRequest" var="searchContentUrl"></portlet:actionURL>
<%-- <portlet:actionURL var="searchContentUrl" windowState="<%=LiferayWindowState.NORMAL.toString()%>">
<portlet:param name="<%=ActionRequest.ACTION_NAME%>" value="handleActionRequest"/>
</portlet:actionURL>
 --%>
 <portlet:actionURL name="uploadDocument" var="uploadArtifact"></portlet:actionURL>
<%-- <portlet:actionURL var="uploadArtifact">
        <portlet:param name="action" value="uploadArtifact"/>
</portlet:actionURL> --%>
<portlet:actionURL name="deleteDocument" var="deleteArtifact"></portlet:actionURL>
<portlet:resourceURL var="kmContentUrl"  >
 <portlet:param name="action" value="openKMContents"/> 
</portlet:resourceURL>
<html>


<body>
<div id="searchPart">
	<form action="${searchContentUrl}" method="post" id="searchForm">
	 <div class="row">
      <div class="span4" id="searchKnowledge"><INPUT TYPE="TEXT" NAME="searchTerm" id="searchBox" value="${searchTerm}" /><button type="submit" id="searchButton"><div id="searchIcon"></div></button></div>
      
   </div>
   <div class="row" id="searchErrorMsg" style="
    font-family: "Gotham Rounded Book"!important;
    color: red;
    padding-left: 3.5%;
    padding-top: 2%;
">
   <liferay-ui:message key="You have not answered all required fields"/>
   </div>
	</form>
	<c:if test="${searchTerm !=null}">
	<c:if test="${noResultsFAQ && noResultsTM}">
	<div id="searchErrorBox"><liferay-ui:message key="Your search "/> <b>${searchTerm}</b> <liferay-ui:message key="did not match any FAQs and documents."/> </div>
	</c:if>
	</c:if>
	</div>
	
<div id="faqAndTutorialContainer">
<div id="FAQ">
	<div id="FAQHeader">
							
							<div class="faqcntnt"><liferay-ui:message key="Frequently Asked Questions"/></div>
						</div> 
						<div id="FAQContent">
						<c:if test="${contentListSearch !=null }">
						<c:forEach items="${contentListSearch}" var="categoryList">
						<c:set var="categoryWithSpace" value="${fn:toLowerCase(categoryList.categoryName)} "></c:set>
						
						<c:if test="${searchResults !=null && searchResults.contains(fn:replace(categoryWithSpace,' ', ''))}">
								<c:set var="isFound" value="found"></c:set>
						
						
							</c:if>
							
							<div class="categoryHeader" style="white-space: pre;"> <div id="openIcon"></div>${categoryList.categoryName}</div>
							
							<div class="categoryContent ${isFound}">
							<c:set var="isFound" value=""></c:set>
								<c:forEach items="${categoryList.knowledgeContent}"
									var="categoryContent">

									<c:forEach items="${categoryContent}" var="mapContent">
	

									<c:if test="${mapContent.key  eq 'Question'}"  >
									<div id="${mapContent.key}">
									${mapContent.value}
										</div>

										</c:if>   


						<c:if test="${mapContent.key  eq 'Answer'}"  >
						<div id="${mapContent.key}">
						${mapContent.value}
						</div>
   						</c:if>
							</c:forEach>

								</c:forEach>

							</div>
						</c:forEach>
						</c:if>
						<c:if test="${contentListSearch ==null}">
						<c:forEach items="${contentList}" var="categoryList">
						<c:set var="categoryWithSpace" value="${fn:toLowerCase(categoryList.categoryName)} "></c:set>
						<c:if test="${searchResults !=null &&searchResults.contains(fn:replace(categoryWithSpace,' ', ''))}">
								<c:set var="isFound" value="found"></c:set>
							</c:if>
							
							<div class="categoryHeader"> <div id="openIcon"></div>${categoryList.categoryName}</div>
							
							<div class="categoryContent ${isFound}">
							<c:set var="isFound" value=""></c:set>
								<c:forEach items="${categoryList.knowledgeContent}"
									var="categoryContent">

									<c:forEach items="${categoryContent}" var="mapContent">
	

									<c:if test="${mapContent.key  eq 'Question'}"  >
									<div id="${mapContent.key}">
									${mapContent.value}
										</div>

										</c:if>   


						<c:if test="${mapContent.key  eq 'Answer'}"  >
						<div id="${mapContent.key}">
						${mapContent.value}
						</div>
   						</c:if>
							</c:forEach>

								</c:forEach>

							</div>
						</c:forEach>
						</c:if>
							
						</div>

</div>
<div id="tutorialsAndManual">
<div id="TMHeader">
			<form method="post" action="${deleteArtifact}" id="deleteDocumentForm">					
							<div class="tmcntnt"><liferay-ui:message key="Tutorials and Manuals"/>  
</div>   
	<%if(isCustomerPortalAdmin)
						{
						%>
							<div id="tmEditButtons">  
<button type="button" class="" data-toggle="modal" data-target="#addDocumentModal" id="addDocumentButton"><div id="addDocumentIcon"></div><div id="addDocumentContent"><liferay-ui:message key="Add"/></div></button>
<button type="button" class="deleteDocumentButton" id="deleteDocumentButton"><div id="deleteDocumentIcon"></div><div id="deleteDocumentContent"><liferay-ui:message key="Delete"/></div></button>

</div>
<%
						}
%>
						</div> 
						<div id="TMContent">
						
						<div id="deleteDocumentErrorMsg" style="display:none;"><liferay-ui:message key="Please select a document to delete"/></div>
						<c:if test="${contentListSearchTM !=null }">
					
						<c:forEach items="${contentListSearchTM}" var="categoryListTM">
						<c:set var="categoryWithSpaceTM" value="${fn:toLowerCase(categoryListTM.categoryNameTM)} "></c:set>
						
						
							 <c:if test="${searchResultsTM !=null && searchResultsTM.contains(fn:replace(categoryWithSpaceTM,' ', ''))}"> 
								<c:set var="isFound" value="found"></c:set>
						
						
							</c:if>
							
							<div class="categoryHeaderTM" style="white-space: pre;"> <div id="openIconTM"></div>${categoryListTM.categoryNameTM}</div>
							
							<div class="categoryContentTM ${isFound}">
							<c:set var="isFound" value=""></c:set>
								<c:forEach items="${categoryListTM.content}"
									var="categoryContentTM">
									
									<c:forEach items="${categoryContentTM}" var="mapContentTM">	
									
										<c:if test="${mapContentTM.key  eq 'DocumentTitle'}"  >
											<c:set var="docTitle" value="${mapContentTM.value}"></c:set>
										</c:if>
									</c:forEach> 

									<c:forEach items="${categoryContentTM}" var="mapContentTM">	

										  <c:if test="${mapContentTM.key  eq 'DocumentTitle'}"  >
												<c:set var="docTitle" value="${mapContentTM.value}"></c:set>												
									</c:if>  
									<c:if test="${mapContentTM.key  eq 'DocumentId'}"  >
									<div id="documentTitleContainer">
							<%if(isCustomerPortalAdmin)
						{
						%>
									<div id="deleteDocumentCheckbox">
											<input type="checkbox" value="${mapContentTM.value}" id="${mapContentTM.value}" name="documentTODelete" class="documentTODelete">
											</div>
											<%
						}
											%>
											<div id="${mapContentTM.key}" class="documentLinkContainer">
											<a href="${kmContentUrl}&docid=${mapContentTM.value}" id="documentLink" style="color: red;" target="_blank">
											<div id="outerDocumentTitle">
											<div id="innerDocumentPre">
											<img id="documentTitlePre" src="/TTApplicationPortlets/images/documentPre.png"/>
											</div>
											<div id="innerDocumentTitle">
											${docTitle}
											</div>
											<div id="innerDocumentPost">
											<img id="documentTitlePost" src="/TTApplicationPortlets/images/documentPost.png"/>
											</div>
											</div>
											</a>
											
											</div>
											</div>
										
				   					</c:if>
									</c:forEach>

								</c:forEach>

							</div>
							
						</c:forEach>
												
						
						
						</c:if> 
						<c:if test="${contentListSearchTM ==null}">
						<%-- <form method="POST" action="${deleteArtifact}" id="deleteDocumentForm"> --%>
						<c:forEach items="${contentListTM}" var="categoryListTM">
						<c:set var="categoryWithSpaceTM" value="${fn:toLowerCase(categoryListTM.categoryNameTM)} "></c:set>
						<c:if test="${searchResults !=null &&searchResults.contains(fn:replace(categoryWithSpaceTM,' ', ''))}">
								<c:set var="isFound" value="found"></c:set>
							</c:if>
							
							<div class="categoryHeaderTM"> <div id="openIconTM"></div>${categoryListTM.categoryNameTM}</div>
							
							<div class="categoryContentTM ${isFound}">
							<c:set var="isFound" value=""></c:set>
							<%-- <c:if test="${categoryListTM != null}">
								${categoryListTM.content}
							</c:if> --%>
							
							<c:if test="${not empty categoryListTM.content}">
								<c:forEach items="${categoryListTM.content}"
									var="categoryContentTM">
								
								<c:forEach items="${categoryContentTM}" var="mapContentTM">	
									
										<c:if test="${mapContentTM.key  eq 'DocumentTitle'}"  >
											<c:set var="docTitle" value="${mapContentTM.value}"></c:set>
										</c:if>
									</c:forEach> 
								
									<c:forEach items="${categoryContentTM}" var="mapContentTM">	
											
								    <c:if test="${mapContentTM.key  eq 'DocumentTitle'}"  >										
											<c:set var="docTitle" value="${mapContentTM.value}"></c:set>																				
									</c:if>  
									<c:if test="${mapContentTM.key  eq 'DocumentId'}"  >
											
									<div id="documentTitleContainer">
										<%if(isCustomerPortalAdmin)
						{
						%>
									<div id="deleteDocumentCheckbox">
											<input type="checkbox" value="${mapContentTM.value}" id="${mapContentTM.value}" name="documentTODelete" class="documentTODelete">
											</div>
											<%
						}
											%>
											<div id="${mapContentTM.key}" class="documentLinkContainer">
											<a href="${kmContentUrl}&docid=${mapContentTM.value}" id="documentLink" style="color: red;" target="_blank">
											<div id="outerDocumentTitle">
											<div id="innerDocumentPre"><img id="documentTitlePre" src="/TTApplicationPortlets/images/documentPre.png"/>
											</div><div id="innerDocumentTitle">${docTitle}</div>
											<div id="innerDocumentPost">
											<img id="documentTitlePost" src="/TTApplicationPortlets/images/documentPost.png"/>
											</div>
											</div>
											</a>	
											
											</div>
										</div>
										
				   					</c:if>
								</c:forEach>

								</c:forEach> 
						</c:if> 
							</div>
						</c:forEach>
					
						</c:if> 
							
						</div>
					</form>

</div>


</div>
	
	
	

        
        
        



<!-- Modal -->
<div id="addDocumentModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="addDocumentCloseModalTop"><div id="closeModalIcon"></button>
        <span id="addDocumentHeader"><liferay-ui:message key="Add Tutorials and Manuals"/></span>
      </div>
      <div class="modal-body">
          <form method="post" action="${uploadArtifact}" id="addDocumentForm" enctype="multipart/form-data">
        
        <div class="row">
        <div class="span4">
        <span id="categoryNameData">
      <liferay-ui:message key="Category Name:*"/>  
      </span>
        </div>
         <div class="span4">
        <select id="categoryName"
						name="categoryName">
						<option value=""><liferay-ui:message key="--Select--"/></option>
						<c:forEach var="category" items="${categoryList}">
							<option value="${category}">${category}</option>

						</c:forEach>
						<option value="Add Category"><liferay-ui:message key="Add Category"/></option>
					</select>
        </div>
        
        </div>
         <div id="addCategoryDiv">
                <div class="row">
                         <div class="span4">
                         <span id="addCategoryData">
                <liferay-ui:message key="Add Category:*"/>
                </span>  
                </div>
                <div class="span4">
                <input type="text" id="addCategory" name="addCategory" >
                </div>
                
                </div>
                </div>
                
                 <div class="row">
                         <div class="span4">
                          <span id="documentNameData">
             <liferay-ui:message key="Document Name:*"/>  
             </span>
                </div>
                  <div class="span4">
                 <INPUT TYPE="TEXT" NAME="documentName" id="documentName">
                </div>
             
               
                </div>
                <div class="row">
                 <div class="span4">
                 <span id="selectFileData">
               <liferay-ui:message key="Select File:*"/> 
               </span> 
                </div>
                   <div class="span4">
                   
                   <input type="text" id="fileName" readonly="readonly" name="fileName"/> 
		
					<button type="button" id="browseButton"><div id="browseButtonContent"><liferay-ui:message key="Browse"/></div></button>
					<div id="selectFileErrorBox" style="display: none;"> <liferay-ui:message key="Please select a file to upload"/> </div>
					
					
              		  
              		  <input type="file" name="document" onchange="copyFilePath()" id="addDocumentSelectFile" style="display: none;"/>
                </div>
                </div>
            
                
                </div>
                
                 
            
                
                </div>
                
        
       
      </div>
      <div class="modal-footer" id="addDocumentModalfooter" >
       <div class="row">
                         <div class="span12">
                         <button type="submit" class="saveDocumentButton" id="saveDocumentButton"><div id="saveDocumentIcon"></div><div id="saveDocumentContent"><liferay-ui:message key="Save"/></div></button>
           <!--  <input type="submit" value="Save" id="saveDocumentButton"> -->
                </div>
      </div>
       </form>
    </div>

  </div>
  
  
  
  
  
  
 <div class="modal fade" id="deleteDocumentConfirmationModal" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="border: none;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
        
         <div id="deleteDocumenModalBody"> <liferay-ui:message key="Are you sure you want to delete?"/></div>
        </div>
        <div class="modal-footer" style="background-color: #fff; border-top:none;">
        <div id="confirmDeleteButtons">
          <button type="button" class="" id="deleteDocumentConfirm"><liferay-ui:message key="OK"/></button>
           <button type="button" class="" data-dismiss="modal" id="deleteCloseButton"><liferay-ui:message key="Close"/></button></div>
        </div>
      </div>
      
    </div>
  </div>
  
  
</div>
         <button type="button" class="" data-toggle="modal" data-target="deleteDocumentConfirmationModal" id="deleteModalOpen" hidden="true"></button></div>
        
        
        
        
        
        
        
        
	
</body>
</html>

<script>

$("#deleteDocumentConfirm").click(function(){
	$("#deleteDocumentConfirmationModal").modal("hide");
	$("#deleteDocumentForm").submit();
	
	
});
$("#saveDocumentButton").click(function(e){
	$("#addDocumentForm").valid();
	
		var fileValue=document.getElementById('fileName').value;
		
		if(fileValue=="" || fileValue==null){
			
			 e.preventDefault(e);
			$("#selectFileErrorBox").show();
		}
		else{
			$("#selectFileErrorBox").hide();
			$("#addDocumentForm").submit();
		}
	
	
});



 $("#addDocumentSelectFile").change(function(){
	
	var fileValue=document.getElementById('fileName').value;
	if(fileValue=="" || fileValue==null){
		   // checked
		$("#selectFileErrorBox").show();
		}
	else{
		
		$("#selectFileErrorBox").hide();
	}
	
});
 

$('#deleteDocumentButton').click(function(){
	var isChecked=false;

	if($(".documentTODelete").is(':checked')){
	   // checked
	    isChecked= true;
	}
	else{
		
		$("#deleteDocumentErrorMsg").show();
	}
	
	if(isChecked){
	
	// var confirmIt=confirm("Are you sure you want to delete?");
	//$("#deleteModalOpen").trigger("click");
				$("#deleteDocumentConfirmationModal").modal("show");
				
		/* if(confirmIt){
			$("#deleteDocumentForm").submit();
			
		}*/
		
	} 
	
   });

$(".documentTODelete").change(function(){
	
	
	if($(".documentTODelete").is(':checked')){
		   // checked
		$("#deleteDocumentErrorMsg").hide();
		}
	
});



function copyFilePath()
{
	/*The pop() method removes the last element of an array, and returns that element.*/
	
	var filename = $('input[type=file]').val().split('\\').pop();
	document.getElementById('fileName').value = filename;
}


$("#browseButton").click(function(){
	
	
	$("#addDocumentSelectFile").trigger("click");
	
});





















	$(document).ready(function() {
		
	/* 	$("#searchErrorBox").hide()
		if(${contentListSearch}==null && ${contentListSearchTM}==null && ${searchTerm}!=null){
			
			$("#searchErrorBox").show()
			
		} */
	    $("#searchErrorMsg").hide();
		$("#deleteDocumentErrorMsg").hide();
		$("#addDocumentModal").hide();
		
		$("#deleteDocumentConfirmationModal").hide();
		$("#addDocumentCloseModalTop").click(function(){
			  $("#addCategoryDiv").hide();

			$("#selectFileErrorBox").hide();
			validator.resetForm();
			 $('#addDocumentForm').trigger("reset"); 
	
			
		});
		

		$("#addDocumentCloseModalBtm").click(function(){
			$("#addDocumentForm").reset();
			$("#selectFileErrorBox").hide();
		$('#addDocumentForm').trigger("reset");
		
			
		});
		
	
		
		
		jQuery.validator.addMethod('addCategoryName', function(value, element, param) {
			
			var selectedCategory=$("#categoryName :selected").text();

			if(selectedCategory=="Add Category"){
				
				
				if(value=="" || value ==null){
					
					return false;
				}
				
			}
			 return true; 
			
		}, '<liferay-ui:message key="Please enter category name"/>');
		
		






	var validator=	  $("#addDocumentForm").validate({
			    
		       // Specify the validation rules
		       rules: {
		       	categoryName: "required",
		       	documentName: "required",
		       	document: "required",
		       	addCategory:"addCategoryName"
		       
		       	
		       },
		       
		       // Specify the validation error messages
		       messages: {
		       	categoryName: '<liferay-ui:message key="Please select a valid category"/>',
		       	documentName: '<liferay-ui:message key="Please enter document name"/>',
		       	document: '<liferay-ui:message key="Please select a file to upload"/>'
		       		
		          
		       },
		       
		       submitHandler: function(form) {
		           form.submit();
		       }
		   });
		  
		  
			
			jQuery.validator.addMethod('isCheckboxChecked', function(value, element, param) {
				
				

				if($(".documentTODelete").is(':checked')){
				   // checked
				    return true;
				}
				else{
				  
				return false;
				}
				
			}, '<liferay-ui:message key="Please select a document to delete"/>');
			
		  
		  
			



			  $("#deleteDocumentForm").validate({
				    
			       // Specify the validation rules
			       rules: {
			    	   documentTODelete: "isCheckboxChecked"
			       
			      
			       	
			       },
			       
			       // Specify the validation error messages
			       messages: {
			      
			       },
			       
			       submitHandler: function(form) {
			           form.submit();
			       }
			   });
			  

		
		  $("#addCategoryDiv").hide();
		  $("#categoryName").change(function() {
		  	var selectedCategory=$("#categoryName :selected").text();
		  	if(selectedCategory=="Add Category" || selectedCategory=="Tambahkan Kategori" ){
		  		
		  		$("#addCategoryDiv").show();
		  		
		  	}
		  	
		  	else{
		  		
		  		$("#addCategoryDiv").hide();
		  		
		  	}
		  });


		
		
		$(".categoryContent").hide();
		$(".categoryContentTM").hide();
		
		
		$(".found").show();
		
		$(".categoryHeader").click(function() {
			$(this).next(".categoryContent").slideToggle();
		});
		$(".categoryHeaderTM").click(function() {
			$(this).next(".categoryContentTM").slideToggle();
		});
		
	});

	
	
	
	$.validate({
		  validateOnBlur : false,
		  
		});
	
	
	/* $("#searchForm").validate({
	    
	       // Specify the validation rules
	       rules: {
	    	   searchTerm: "required",  	
	       },
	       
	       // Specify the validation error messages
	       messages: {
	    	   searchTerm: '<liferay-ui:message key="You have not answered all required fields"/>',
	      },
	      
	      submitHandler: function(form) {
	           form.submit();
	       }
	       
	   });
	 */

	 
	$("#searchButton").click(function(e){
			
				var fileValue=document.getElementById('searchBox').value;
				
				if(fileValue=="" || fileValue==null){
					
					 e.preventDefault(e);
					$("#searchErrorMsg").show();
					$("#searchErrorBox").hide();
				}
				else{
					$("#searchErrorMsg").hide();
					$("#searchErrorBox").show();
					$("#addDocumentForm").submit();
				}
			
			
		});

	
		$("#searchBox").change(function(){
			
		 var fileValue=document.getElementById('searchBox').value;
			
			if(fileValue=="" || fileValue==null){
				
				$("#searchErrorMsg").show();
				$("#searchErrorBox").hide();
			}
			else{
				$("#searchErrorMsg").hide();

			}
			
		}); 
	
</script>

<%! 
 boolean containsCategory(String categoryName, ArrayList<String> searchList){
    for (String string : searchList){
       if (string.equalsIgnoreCase(categoryName)){
           return true;
        }
    }
   return false;
 }

%>