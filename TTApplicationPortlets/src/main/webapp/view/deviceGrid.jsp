<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<html>
<head>
<style>

div#dev-grid-title {
  font-size: 16px;
  color: #333;
  font-weight: bold;
  padding: 20px 0 10px 20px;
}

#deviceHeaderDiv {
  padding: 20px 0 20px 20px;
  background-color: #fff;
  border-bottom: solid 1px #ccc;
}
#pv-tt-icon{
	display:inline;
}
#pv-tt-header {
  display: inline;
  font-size: 20px;
  color: #333;
  padding-left: 10px;
  font-weight: bold;
}


</style>

<script>

</script>
</head>
<body>
<div id="deviceHeaderDiv">
	<div id="pv-tt-icon"><img src="/TTApplicationPortlets/images/u841.png"/></div>
	<div id="pv-tt-header"><liferay-ui:message key="Project View"/></div>
</div>
<div id="devicegriddiv">
<div id="dev-grid-title"><liferay-ui:message key="Device & Service Grid"/></div>
<table id="gridTableData">
       <tr class="gridHeadings">
              
              <c:forEach items="${fieldRow}" var="fieldRow">
              <c:set var="fieldRaw" value="${fieldRow}" />
              <c:set var="fieldInset" value="${fn:replace(fieldRaw,' ','')}" />
              <c:set var="fieldInset" value="${fn:replace(fieldInset,'&','')}" />
              <td class="${fieldInset}"><liferay-ui:message key="${fieldRow}"/></td>
              </c:forEach>
              <td class="total"><liferay-ui:message key="Total"/></td>

       </tr>
       <c:forEach items="${gridRows}" var="gridRows">
       <tr>
       <td id="${gridRows.key}">
        <c:choose>
          <c:when test="${gridRows.key == 'M-LAN'}">
            <c:set var = "devicegridRowName" scope = "session" value="MNS LAN"/>
            MNS LAN
          </c:when>
          <c:when test="${gridRows.key == 'M-WAN'}">
          	<c:set var = "devicegridRowName" scope = "session" value="MNS WAN"/>
            MNS WAN
          </c:when>
          <c:when test="${gridRows.key == 'M-WANOPT'}">
          	<c:set var = "devicegridRowName" scope = "session" value="MNS WAN OPTI"/>
            MNS WAN OPTI
          </c:when>
          <c:when test="${gridRows.key == 'IPScape'}">
          	<c:set var = "devicegridRowName" scope = "session" value="Cloud Contact Center"/>
            Cloud Contact Center
          </c:when>
          <c:when test="${gridRows.key == 'Whispir'}">
			<c:set var = "devicegridRowName" scope = "session" value="Multi Channel Communication"/>
            Multi Channel Communication
          </c:when>
          <c:when test="${gridRows.key == 'Mandoe Digital Signage'}">
			<c:set var = "devicegridRowName" scope = "session" value="Digital Proximitry"/>
            Digital Proximitry
          </c:when>
          <c:otherwise>
          	<c:set var = "devicegridRowName" scope = "session" value="${gridRows.key}"/>
            <liferay-ui:message key="${gridRows.key}"/>
          </c:otherwise>
        </c:choose>
       </td>
       <c:forEach items="${gridRows.value}" var="individualrow">
       <c:set var="individualRaw" value="${fn:replace(individualrow,' ','')}" />
       <c:set var="individualRowInset" value="${fn:replace(individualRaw,'&','')}" />
       <c:set var="individualRowInsetVar" value="${fn:split(individualRowInset,'=')}"/>
       <td onClick="sendParameters(this)" class="data-rows-grid" id="${individualRowInsetVar[0]}" aria-label="${devicegridRowName}"><liferay-ui:message key="${individualRowInsetVar[1]}"/></td>
       </c:forEach>
       </c:forEach>
       
</table>
</div>
</body>
</html>