 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib prefix="cfn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="com.tt.utils.PropertyReader"%>
<%@ page import="java.util.Properties"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getUserData">
</portlet:resourceURL>

<%
	Properties properties = PropertyReader.getProperties();
%>


<%@ include file="common.jsp" %>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<link href="${pageContext.request.contextPath}/css/main.css"
	rel="stylesheet"></link>
	<link href="${pageContext.request.contextPath}/css/sites.css"
	rel="stylesheet"></link>

<script>
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

function RBHealth() {
	$('#RBHealth-li').addClass('selected');
	$('#data-gold').removeClass('hide');
	$('#data-silver').removeClass('hide');
	$('#data-bronze').removeClass('hide');
	$('#RBCheckbox').removeClass('hide');
	$('#riverbedCountId').removeClass('hide');
	$('#riverbedTitleText').removeClass('hide');
	$('#title-gold').removeClass('hide');
	$('#title-silver').removeClass('hide');
	$('#title-bronze').removeClass('hide');
}

var VMs = [{priority:'2',productclassification:'MNS',servicetier:'Gold',sitename:'EC5GK000F3BAA',ciname:'EC5GK000F3BAA',colour:'background-color:#ff9c00',citype:'Resource'}];
			

VMs = JSON.stringify('${SiteClassificationProduct}');
VMs = eval('(' + '${SiteClassificationProduct}' + ')');


var temp = VMs.slice();

$(document).ready(function(){
	
	var hash = document.location.hash;
	if(hash){
		if(hash === '#RBHealth'){
			RBHealth();
		}
	}
	else{
		RBHealth();
	}		
	
	//VMs = VMs.sort(function IHaveAName(a, b) { return b.priority> a.priority?  1 : b.priority< a.priority? -1 : 0; });

	populateData(VMs);	
	
	$("#linkTable").addClass('hide');
				
})

	Array.prototype.removeValue = function(name, value){
	   var array = $.map(this, function(v,i){
		  return v[name] === value ? null : v;
	   });
	   this.length = 0;
	   this.push.apply(this, array);
	}
	
$(function() {
	
	$("input[type='checkbox']").change(
	function()	{
		
			temp = VMs.slice();	
			var all = false;
			var priority1 = false;
			var priority2 = false;
			var priority3 = false;
			var normal = false;
			var isAll = false;
			var isGold = false;
			var isSilver = false;
			var isBronze = false;
			
			if($('#checkbox_all').is(':checked')) 
			{		
				all = true;
			}
			if($('#checkbox_priority1').is(':checked')) 
			{		
				priority1 = true;
			}
			if($('#checkbox_priority2').is(':checked')) 
			{		
				priority2 = true;
			}
			if($('#checkbox_priority3').is(':checked')) 
			{		
				priority3 = true;
			}
			if($('#checkbox_normal').is(':checked')) 
			{		
				normal = true;
			}
			if($('#checkbox_isall').is(':checked')) 
			{		
				isAll = true;
				 $(".tt-displaysite-position2").prop('checked',false);
				 $(".tt-displaysite-position3").prop('checked',false);
				 $(".tt-displaysite-position4").prop('checked',false);
                 $(".tt-displaysite-position1").prop('checked',true);
			}
			if($('#checkbox_gold').is(':checked')) 
			{		
				isGold = true;
				$(".tt-displaysite-position1").prop('checked',false);
				 $(".tt-displaysite-position3").prop('checked',false);
				 $(".tt-displaysite-position4").prop('checked',false);
                $(".tt-displaysite-position2").prop('checked',true);
			}
			if($('#checkbox_silver').is(':checked')) 
			{		
				isSilver = true;
				$(".tt-displaysite-position1").prop('checked',false);
				 $(".tt-displaysite-position2").prop('checked',false);
				 $(".tt-displaysite-position4").prop('checked',false);
                $(".tt-displaysite-position3").prop('checked',true);
			}
			if($('#checkbox_bronze').is(':checked')) 
			{		
				isBronze = true;
				$(".tt-displaysite-position1").prop('checked',false);
				 $(".tt-displaysite-position2").prop('checked',false);
				 $(".tt-displaysite-position3").prop('checked',false);
                $(".tt-displaysite-position4").prop('checked',true);
			}
			

			if(all == false)
			{
				if(priority1==false)
					temp.removeValue('priority', '1');
				if(priority2==false)
					temp.removeValue('priority', '2');
				if(priority3==false)
					temp.removeValue('priority', '3');
				if(normal==false)
					temp.removeValue('priority', '4');
			}else if(isAll == false){
				if(isGold==false)
					temp.removeValue('servicetier', 'Gold');
				if(isSilver==false)
					temp.removeValue('servicetier', 'Silver');
				if(isBronze==false)
					temp.removeValue('servicetier', 'Bronze');
			}
			
			$('#searchText').val('');
			
			populateData(temp);			
	});	
	});
	
var sortByName = function sortByName(a, b) { 
	return a.priority - b.priority;
}

var sortByTier = function sortByTier(a, b) { 
	return a.servicetier - b.servicetier;
}

function populateData(temp)
{	
	temp = temp.sort(sortByName);
	
	var parameterReceived = getQueryString(window.location.href);
	var appliances = '';
    if(parameterReceived!=-1){
    	var applianceSpecifics = parameterReceived;
        appliances=applianceSpecifics.split("&")[0];
        parameter2=applianceSpecifics.split("&")[1];
    }
    
    
	
	if(temp.length==0)
	{
		document.getElementById("data-Radio").innerHTML = "<div class='row-fluid'></div>";
		/* document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		document.getElementById("data-RC").innerHTML = "<div class='row-fluid'></div>";
		document.getElementById("data-RD").innerHTML = "<div class='row-fluid'></div>"; */
		return;
	}
	else
	{
		var no = 0;
		var cek = 0;
		var str = "";
		
		var no2 = 0;
		var cek2 = 0;
		var str2 = "";
		
		var no3 = 0;
		var cek3 = 0;
		var str3 = "";
		
		str = "<div class='span4'><div class='row-fluid'>";
		str2 = "<div class='span4'><div class='row-fluid'>";
		str3 = "<div class='span4'><div class='row-fluid'>";
		for (i in temp)
		{
			if(parameterReceived!=-1){
			if(appliances=='DEVICE'){
				if(temp[i].sitename==parameter2 && temp[i].servicetier=='Gold' && temp[i].citype=='Resource'){
					str += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 12px;'>"+temp[i].ciname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
					no++;
					cek++;
					if(no==2)
					{
						no=0;
						str += "</div><div class='row-fluid'>";			 
					}	
				}else if(temp[i].sitename==parameter2 && temp[i].servicetier=='Silver' && temp[i].citype=='Resource'){
					str2 += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 12px;'>"+temp[i].ciname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
					no2++;
					cek2++;
					if(no2==2)
					{
						no2=0;
						str2 += "</div><div class='row-fluid'>";			 
					}
				}else if(temp[i].sitename==parameter2 && temp[i].servicetier=='Bronze' && temp[i].citype=='Resource'){
					str3 += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 12px;'>"+temp[i].ciname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
					no3++;
					cek3++;
					if(no3==2)
					{
						no3=0;
						str3 += "</div><div class='row-fluid'>";			 
					}
				}
			}else{
			if(temp[i].productclassification==appliances && temp[i].servicetier=='Gold'){
				str += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 12px;'>"+temp[i].ciname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
				no++;
				cek++;
				if(no==2)
				{
					no=0;
					str += "</div><div class='row-fluid'>";			 
				}	
			}else if(temp[i].productclassification==appliances && temp[i].servicetier=='Silver'){
				str2 += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 12px;'>"+temp[i].ciname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
				no2++;
				cek2++;
				if(no2==2)
				{
					no2=0;
					str2 += "</div><div class='row-fluid'>";			 
				}
			}else if(temp[i].productclassification==appliances && temp[i].servicetier=='Bronze'){
				str3 += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 12px;'>"+temp[i].ciname+"</a></div></td></tr><tr><td height='10' align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
				no3++;
				cek3++;
				if(no3==2)
				{
					no3=0;
					str3 += "</div><div class='row-fluid'>";			 
				}
			}
			}
		}else
		{
				str += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].ciname+"</a></div></td></tr><tr><td align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
				no++;
				cek++;
				if(no==2)
				{
					no=0;
					str += "</div><div class='row-fluid'>";			 
				}
				
				str2 += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].ciname+"</a></div></td></tr><tr><td align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
				no2++;
				cek2++;
				if(no2==2)
				{
					no2=0;
					str2 += "</div><div class='row-fluid'>";			 
				}	
				
				str3 += "<div class='span6'><div class='extra_style' style='"+temp[i].colour+"'><table width='100%'><tr><td align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important;'>"+temp[i].ciname+"</a></div></td></tr><tr><td align='center'><div class='span12'><a style='color:#fff!important;text-decoration:initial!important; font-size : 10px;'>"+temp[i].sitename+"</a></div></td></tr></table></div></div>";
				no3++;
				cek3++;
				if(no3==2)
				{
					no3=0;
					str3 += "</div><div class='row-fluid'>";			 
				}
		}
			
		}
		str += "</div> </div>";
		str2 += "</div> </div>";
		str3 += "</div> </div>";
		document.getElementById("title-SITE").innerHTML = appliances;
		var URLSITE = '/group/telkomtelstra/sites';
		var URLMWANOPT = '/group/telkomtelstra/sites';
		
		if(appliances=='M-WANOPT'){
			document.getElementById("data-Button").innerHTML = "<button type='button' class='btn-primary-tt backButton'><liferay-ui:message key='Optimization Dashboard >>'/></button> <button type='button' class='btn-primary-tt backButton'><a href='"+URLSITE+"' style='color:#fff!important;text-decoration:initial!important;'>< Back</a></button>";	
		}else if(appliances=='M-WLAN'){
			document.getElementById("data-Button").innerHTML = "<button type='button' class='btn-primary-tt backButton'><liferay-ui:message key='Go to Meraki Dashboard >>'/></button> <button type='button' class='btn-primary-tt backButton'><a href='"+URLSITE+"' style='color:#fff!important;text-decoration:initial!important;'>< Back</a></button>";		
		}else{
			document.getElementById("data-Button").innerHTML = "<button type='button' class='btn-primary-tt backButton'><a href='"+URLSITE+"' style='color:#fff!important;text-decoration:initial!important;'>< Back</a></button>";		
		}
		
		if(appliances=='M-FIREWALL'){
			document.getElementById("title-gold").innerHTML = "Enhanced Plus";
			document.getElementById("title-silver").innerHTML = "Enhanced";
			document.getElementById("title-bronze").innerHTML = "Standard";
			document.getElementById("cbtitle-gold").innerHTML = "Enhanced Plus";
			document.getElementById("cbtitle-silver").innerHTML = "Enhanced";
			document.getElementById("cbtitle-bronze").innerHTML = "Standard";
		}else{
			document.getElementById("title-gold").innerHTML = "Gold";
			document.getElementById("title-silver").innerHTML = "Silver";
			document.getElementById("title-bronze").innerHTML = "Bronze";
			document.getElementById("cbtitle-gold").innerHTML = "Gold";
			document.getElementById("cbtitle-silver").innerHTML = "Silver";
			document.getElementById("cbtitle-bronze").innerHTML = "Bronze";
		}
		
		
		if(cek==0){
			document.getElementById("data-Radio").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-Radio").innerHTML = str + str2 + str3;
		}
		
		/* if(cek==0){
			document.getElementById("data-RB").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-RB").innerHTML = str;
		}
		
		if(cek2==0){
			document.getElementById("data-RC").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-RC").innerHTML = str2;
		}
		
		if(cek3==0){
			document.getElementById("data-RD").innerHTML = "<div class='row-fluid'></div>";
		}else{
			document.getElementById("data-RD").innerHTML = str3;
		} */
	}
}

function getQueryString(url){
	if (url.indexOf('?') > -1) {
		var pos = url.indexOf('?');
		var extract = url.substring(pos + 1,url.length);
		return extract;
	}
	else{
		return -1;
	}
}

</script>

<style>

#sitePortletTitleBar{
       height:37px;
}

#siteTitleText{
       line-height:34px;
}
#siteNavigationArrow{
       line-height:34px;
}
.site-title-dashboard {
    font-size: 15px;
    display: flex;
    justify-content: center;
}

#siteCountId{
       fill:#888 !important;
       font-size:20px !important;
}

#siteLabelId{
       fill:#888 !important;
       font-size:18px !important;
       font-weight: 100;
}

div#site {
    display: flex;
   justify-content: center;
}

#markers-tt {
       display: flex;
       justify-content: center;
}

#tt-red {
       background-color: #e32212;
}

#tt-amber {
       background-color: #ff9c00;
}

#tt-green {
       background-color: #a3cf62;
}

#tt-yellow {
       background-color: #ffff00;
}

#tt-black-marker {
       color: #000000;
}

#tt-red-marker {
       color: #e32212;
}

#tt-amber-marker {
       color: #ff9c00;
}

#tt-green-marker {
       color: #a3cf62;
}

#tt-yellow-marker {
       color: #ffff00;
}

.tt-color-legends {
       display: inline-block;
       width: 10px;
       height: 10px;
       position: relative;
       top: 5px;
       margin: 0 5px;
}

.tt-markers {
       display: inline-block;
       font-size: 12px;
}

.legend-site {
       font-size: 15px;
       line-height: 120%;
       padding-top: 5px;
       padding-bottom: 30px;
       height: 30px;
       text-align: center;
}

#siteblock {
       justify-content: center;
       background-color: #fff;
       height: 262px;
       /* border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; 
       margin-left: 50px;*/
}

#mwan {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mwanopti {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mwlan {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mlan {
	   display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

#mfirewall {
       display: flex;
       justify-content: center;
       background-color: #fff;
       /*height: 262px;
        border-left: solid 1px #ccc;
       border-right: solid 1px #ccc;
       border-bottom: solid 1px #ccc; */
}

.siteServices {
       width: 150px;
       margin: 0 40px 0 40px;
}

.value-site {
       text-align: center;
       height: 50px;
       line-height: 50px;
       border: solid 1px black;
       border-radius: 8px;
}

#status-val-service {
       border-color: transparent;
}

.row-fluid {
    width: 100%;
    margin-bottom: 2%;
}
</style>

<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;<liferay-ui:message key="Site Information"/>&nbsp;&gt;&nbsp;</span>
				<%-- <span class="tt-current-page"><liferay-ui:message key="Site Information"/>&nbsp;&gt;&nbsp;</span> --%>
				<span id="title-SITE" class="tt-current-page">
				</span>
			</div>
		</div>
		<div class="span6 tt-back" id="data-Button">	
				</div>
		
	</div>
</div>

<div class="span6" id="markers-tt">
			  <div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="Display Sites : " /></div>
				<input type="checkbox" id="checkbox_isall" class="tt-displaysite-position1" checked/>
              <div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="All" /></div>
             <input type="checkbox" id="checkbox_gold" class="tt-displaysite-position2"/>
              <div class="tt-markers" id="cbtitle-gold"><%-- <liferay-ui:message key="Gold" /> --%></div>
              <input type="checkbox" id="checkbox_silver" class="tt-displaysite-position3"/>
              <div class="tt-markers" id="cbtitle-silver"><%-- <liferay-ui:message key="Silver" /> --%></div>
               <input type="checkbox" id="checkbox_bronze" class="tt-displaysite-position4"/>
              <div class="tt-markers" id="cbtitle-bronze"><%-- <liferay-ui:message key="Bronze" /> --%></div>
               
       	</div>
     <div id="markers-tt">
		     <div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="Display Incident : " /></div>
				<input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/>
              <div class="tt-markers" id="tt-black-marker"><liferay-ui:message key="All" /></div>
             <input type="checkbox" id="checkbox_priority1" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-red-marker"><liferay-ui:message key="Priority 1" /></div>
               <input type="checkbox" id="checkbox_priority2" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-amber-marker"><liferay-ui:message key="Priority 2" /></div>
               <input type="checkbox" id="checkbox_priority3" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-yellow-marker"><liferay-ui:message key="Priority 3" /></div>
               <input type="checkbox" id="checkbox_normal" class="tt-displaysite-position"/>
              <div class="tt-markers" id="tt-green-marker"><liferay-ui:message key="Normal" /></div>
       	</div>  	

<div class="container-fluid tt-site-tab-container">

<div class="span12 tt-graph tt-graph-first tt-gold">
	<div style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
	<div class="row-fluid">
	<div class="span4">
						<div id="title-gold" class="span12 hd" align="center">
                     <%-- <liferay-ui:message key="Gold" /> --%>
              </div>
		</div>
		<div class="span4">
			<div id="title-silver" class="span12 hd" align="center">
                     <%-- <liferay-ui:message key="Silver" /> --%> 
              </div>
		</div>
		<div class="span4">
			<div id="title-bronze" class="span12 hd" align="center">
                     <%-- <liferay-ui:message key="Bronze" /> --%> 
              </div>
		</div>
	</div>
	</div>
	<div style="padding: 1%; background-color: #fff; border: solid 1px #ccc;">
	<div class="row-fluid">
		<div id="data-Radio">
		</div>
		<!-- <div class="span4">
			<div id="data-RB" class="span12" style="padding:10px;background-color: white;">	
				</div>
		</div>
		<div class="span4">
			<div id="data-RC" class="span12" style="padding:10px;background-color: white;">
				</div>
		</div>
		<div class="span4">
			<div id="data-RD" class="span12" style="padding:10px;background-color: white;">
				</div>
		</div> -->
		</div>
	
	</div>
</div>

</div>