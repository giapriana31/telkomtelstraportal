<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="${pageContext.request.contextPath}/css/main.css" rel="stylesheet"></link>

<!--  
<link href="${pageContext.request.contextPath}/css/pvSiteWanLv0.css" rel="stylesheet"></link>
-->

<script src="${pageContext.request.contextPath}/js/d3pie/d3.v4.min.js"></script>
<script src="${pageContext.request.contextPath}/js/d3pie/jquery-1.9.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>

<!-- 
<script src="${pageContext.request.contextPath}/js/pvSiteWanLv0.js"></script>
-->

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%@ include file="common.jsp" %>

<style>
@media (min-width: 769px) and (max-width: 929px) {


	#p_p_id_TTPVSiteLv0_WAR_TTApplicationPortlets_ {
	    margin-left: 24% !important;
	    margin-top: -3% !important;
	    margin-right: 3%;
	}
	#cloud {
	    margin-left: 0% !important;
	}
	#graphTitle {
	    
	    margin-left: 40% !important;
	   
	}
}
@media (max-width: 500 px){
	
	
	#p_p_id_TTPVSiteLv0_WAR_TTApplicationPortlets_ {
		margin-left: 4% !important;
		margin-right: 3% !important;
		margin-top: -11% !important;
	}
}
</style>

<div class="container-fluid tt-container" id="officePortel">
	<!-- START OFFICE PORTLET HEADER-->
	<div class="row-fluid tt-header">
		<div class="span6 tt-page-title"><liferay-ui:message key="M-WAN"/></div>
		<div class="span6 tt-detail">
			<%-- <a href="<portlet:actionURL name="refresh"></portlet:actionURL>"> --%>
			<a href="<%=prop.getProperty("tt.pv.homeURL")%>/pvdashboardlv2?M-WAN">
			<img class="imgalign" src="<%=request.getContextPath()%>/images/Expand_G_24.png" /></a>
		</div>
	</div>
	<!-- END OFFICE PORTLET HEADER-->
	<!-- START OFFICE PORTLET GRAPH CONTENT-->
	<div class="row-fluid tt-content">
		<div class="span4 tt-graph tt-graph-first">
			<div id="PVSiteWan"></div>
<!-- Start d3 Script -->
<script type="text/javascript">

var graphColorDataSiteWan = {"#A40800":1,"#7030A0":3,"#89C35C":0,"#FA8072":2,"#000":0,"#00B0F0":8};
var colorsSiteWan = ["#89C35C","#FA8072","#00B0F0","#7030A0","#A40800","#000"];
//new code//


var w = 280;
var h = 200;
var r = 90;
var ir = 80;
var textOffset = 14;
var tweenDuration = 500;

//OBJECTS TO BE POPULATED WITH DATA LATER
var lines = Array();
var valueLabels = Array();
var nameLabels = Array();
var pieData = [];
var oldPieData = [];
var filteredPieData = [];

//D3 helper function to populate pie slice parameters from array data
var donut = d3.pie().value(function(d) {
    return d.octetTotalCount;
});   
    
var siteColors = colorsSiteWan;

var color = function(index) {
    if (index < siteColors.length)
        return siteColors[index];
    else
        return '#888888';
}

//D3 helper function to draw arcs, populates parameter "d" in path object
var arc = d3.arc()
    .startAngle(function(d) {
        return d.startAngle;
    })
    .endAngle(function(d) {
        return d.endAngle;
    })
    .innerRadius(ir)
    .outerRadius(r);

///////////////////////////////////////////////////////////
//GENERATE FAKE DATA /////////////////////////////////////
///////////////////////////////////////////////////////////

var streakerDataAdded;

function findCoordinates(r, angle) {
    //alert(angle);
    angle = angle - Math.PI / 2;
    return {
        x: Math.cos(angle) * r,
        y: Math.sin(angle) * r
    };
}

function fillArray() {
    return {
        port: "",
        octetTotalCount: 10
    };
}


///////////////////////////////////////////////////////////
//CREATE VIS & GROUPS ////////////////////////////////////
///////////////////////////////////////////////////////////
//var sites = ['gold', 'silver', 'bronze'];

var pvsites = "PVSiteWan";
var vis;
var arc_group;
var center_group;
var paths;
var label_group;
var grayCircle;
var whiteCircle;
var totalLabel;
var totalValue;

var graphColorCode = [];

var count = 0;

for (var graphColor in colorsSiteWan) {
    var clr = colorsSiteWan[graphColor];
    graphColorCode.push(graphColorDataSiteWan[clr]);
}

var graphJsonObject = [];

for (i = 0; i < graphColorCode.length; i++) { 
	graphJsonObject.push({octetTotalCount: graphColorCode[i]});
}

var totalData = [];
    
totalData.push(graphJsonObject);   

for (var p = 0; p < totalData.length; p++) {
    var tempArray = totalData[p];
    for (var q = tempArray.length - 1; q >= 0; q--) {
        if (tempArray[q].octetTotalCount < 1) {
            tempArray.splice(q, 1);
            siteColors.splice(q, 1);
        }
    }
}
    
vis = d3.select("#" + pvsites).append("svg:svg")
    .attr("width", w)
    .attr("height", h);

//		GROUP FOR ARCS/PATHS
arc_group = vis.append("svg:g")
    .attr("class", "arc")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

//		GROUP FOR CENTER TEXT  
center_group = vis.append("svg:g")
    .attr("class", "center_group")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

//		PLACEHOLDER GRAY CIRCLE
paths = arc_group.append("svg:circle")
    .attr("fill", "#EFEFEF")
    .attr("r", r);

//		GROUP FOR LABELS
label_group = vis.append("svg:g")
    .attr("class", "label_group")
    .attr("transform", "translate(" + (w / 2) + "," + (h / 2) + ")");

///////////////////////////////////////////////////////////
//		CENTER TEXT ////////////////////////////////////////////
///////////////////////////////////////////////////////////

//		WHITE CIRCLE BEHIND LABELS
grayCircle = center_group.append("svg:circle")
    .attr("fill", "#eeeeee")
    .attr("r", ir);

whiteCircle = center_group.append("svg:circle")
    .attr("fill", "white")
    .attr("r", ir - 7);

//		"TOTAL" LABEL
totalLabel = center_group.append("svg:text")
    .attr("class", "label")
    .attr("dy", 15)
    .attr("text-anchor", "middle") // text-align: right
    .text("Total");

//		TOTAL TRAFFIC VALUE
totalValue = center_group.append("svg:text")
    .attr("class", "total")
    .attr("dy", -5)
    .attr("text-anchor", "middle") // text-align: right
    .text("0");
    
///////////////////////////////////////////////////////////
//STREAKER CONNECTION ////////////////////////////////////
///////////////////////////////////////////////////////////

draw();
draw();    

//to run each time data is generated
function draw() {
    
    var siteIndex = 0;
    
    streakerDataAdded = totalData[siteIndex];
    oldPieData = filteredPieData;
    pieData = donut(streakerDataAdded);

    var totalOctets = 0;

    function filterData(element, index, array) {
        element.name = streakerDataAdded[index].port;
        element.value = streakerDataAdded[index].octetTotalCount;
        totalOctets += element.value;
        return (element.value >= 0);
    }
    
    filteredPieData = pieData.filter(filterData);
    
    if (filteredPieData.length > 0 && oldPieData.length > 0) {

        //REMOVE PLACEHOLDER CIRCLE
        arc_group.selectAll("circle").remove();

        totalValue.text(function() {

            return totalOctets;
            //return bchart.label.abbreviated(totalOctets*8);
        });

    //DRAW TICK MARK LINES FOR LABELS

        lines[siteIndex] = label_group.selectAll("line").data(filteredPieData);
        var genCoordsX=[];
        var genCoordsY=[];
        var firstLineX=0;
        var latestLineX=0;
        var firstLineY=0;
        var latestLineY=0;
  
        lines[siteIndex].enter().append("svg:line")
            .attr("x1", function(d) {
      
                return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).x;
            })
            .attr("x2", function(d) {
                if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                    return "-" + (r + 10);
                else
                    return "" + (r + 10);
                })
            .attr("y1", function(d) {
                return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
                })
            .attr("y2", function(d) {
                    if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {                                                                                           
                        var flagDiff = 0;
                        
                        for (var i = 0; i < genCoordsX.length; i++) {
                            var diffcoords = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y- genCoordsX[i]);

                            if (diffcoords < 10 && diffcoords > 0) {
                                flagDiff = 1;
                                break;
                            } else {
                                continue;
                            }
                        }
                        
                        if (flagDiff == 0) {
				            genCoordsX.push(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y);
                            return ""+ findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                        } else {
                            if (firstLineX == 0) {
                                var incres = findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y;
                                incres += 20;
                                incres=findIsolation(incres,genCoordsX);
                                firstLineX = 1;
                                latestLineX = incres;
								genCoordsX.push(latestLineX);
                                return ""+ incres;
                            } else {
                                latestLineX += 20;
                                genCoordsX.push(latestLineX);
                                return ""+ latestLineX;
                            }
                        }
                    } else {
                        var flagDiff = 0;
                        
                        for (var i = 0; i < genCoordsY.length; i++) {
                            var diffcoords = Math.abs(findCoordinates(r - 5,(d.startAngle + d.endAngle) / 2).y - genCoordsY[i]);

                            if (diffcoords < 10 && diffcoords > 0) {
                                flagDiff = 1;
                                break;
                            } else {
                                continue;
                            }
                        }
                        
                        if (flagDiff == 0) {
                            genCoordsY.push(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y);
                            return "" + findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
                        } else {
                            if (firstLineY == 0) {
                                var incres = findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
                                incres += 20;
                                incres = findIsolation(incres, genCoordsY);
                                firstLineY = 1;
                                latestLineY = incres;
                                genCoordsY.push(latestLineY);
                                return "" + incres;
                            } else {
                                latestLineY += 20;
                                genCoordsY.push(latestLineY);
                                return "" + latestLineY;
                            }
                        }
                    }
                })
            .attr("stroke", function(d, i) {
                                return color(i);
                            });
            
        lines[siteIndex].transition().duration(tweenDuration);
        lines[siteIndex].exit().remove();
        
        function findIsolation(coord,coordArray){
            var coordFirst;
            for(var i=0;i<coordArray.length;i++){
                var diff=Math.abs(coord-coordArray[i]);
                if(diff<15 && diff>0){
                    coord+=20;
                }
            }
            coordFirst=coord;
            for(var i=0;i<coordArray.length;i++){
                var diff=Math.abs(coord-coordArray[i]);
                if(diff<15 && diff>0){
                    coord+=20;
                }
            }
            if(coord==coordFirst){
            return coord;
            }
            else{findIsolation(coord,coordArray);}
        }

        //DRAW ARC PATHS

        paths[siteIndex] = arc_group.selectAll("path").data(filteredPieData);
        paths[siteIndex].enter().append("svg:path")
            .attr("stroke", "white")
            .attr("stroke-width", 0.5)
            .attr("fill", function(d, i) {
                    return color(i);
                })
            .transition()
            .ease(d3.easeLinear)
            .duration(tweenDuration)
            .attrTween("d", pieTween);
        
        paths[siteIndex].transition()
            .ease(d3.easeLinear)
            .duration(tweenDuration)
            .attrTween("d", pieTween);
        
        paths[siteIndex].exit()
            .transition()
            .duration(tweenDuration)
            .attrTween("d", removePieTween)
            .remove();

        //DRAW LABELS WITH PERCENTAGE VALUES

        var genCoordsTextX = [];
        var genCoordsTextY = [];
        var latestTextX = 0;
        var firstTextX = 0;
        var latestTextY = 0;
        var firstTextY = 0;

        valueLabels[siteIndex] = label_group.selectAll("text.value")
            .data(filteredPieData)
            .attr("y",function(d) {
                if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {
                    var flagDiffText = 0;
                    
                    for (var i = 0; i < genCoordsTextX.length; i++) {
                        var diffcoordsText = Math.abs(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5 - genCoordsTextX[i]);
                        if (diffcoordsText < 10 && diffcoordsText > 0) {
                            flagDiffText = 1;
                            break;
                        } else {
                            continue;
                        }
                    }
                        
                    if (flagDiffText == 0) {
                        genCoordsTextX.push(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                        return "" + (findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                    } else {
                        if (firstTextX == 0) {
                            var incresText = findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
                            incresText += 25;
                            incresText = findIsolaton(incresText, genCoordsTextX);
                            firstTextX = 1;
                            latestTextX = incresText;
                            genCoordsTextX.push(incresText);
                            return "" + incresText;
                        } else {
                            latestTextX += 20;
                            genCoordsTextX.push(latestTextX);
                            return "" + latestTextX;
                        }
                    }
                } else {
                    var flagDiffText = 0;
                    
                    for (var i = 0; i < genCoordsTextY.length; i++) {
                        var diffcoordsText = Math.abs(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5 - genCoordsTextY[i]);
                        
                        if (diffcoordsText < 10 && diffcoordsText > 0) {
                            flagDiffText = 1;
                            break;
                        } else {
                            continue;
                        }
                    }
                    
                    if (flagDiffText == 0) {
                        genCoordsTextY.push(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                        return "" + (findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                    } else {
                        if (firstText == 0) {
                            var incresText = findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
                            incresText += 25;
                            incresText = findIsolation(incresText, genCoordsTextY);
                            firstTextY = 1;
                            latestTextY = incresText;
                            genCoordsTextY.push(incresText);
                            return "" + incresText;
                        } else {
                            latestTextY += 20;
                            genCoordsTextY.push(latestTextY);
                            return "" + latestTextY;
                        }
                    }

                    }

                })
            .attr("x", function(d) { 
                    if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                        return "-" + (r + 10);
                    else
                        return "" + (r + 10);
                })
            .attr("fill", function(d, i) { return color(i); })
            .attr("text-anchor", function(d) {
                    if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                        return "end";
                    else
                        return "begning";
                })
            .text(function(d) { return d.value; });
        
        genCoordsTextX = [];
        genCoordsTextY = [];
        latestTextX = 0;
        firstTextX = 0;
        latestTextY = 0;
        firstTextY = 0;

        valueLabels[siteIndex]
            .enter()
            .append("svg:text")
            .attr("class", "value")
            .attr("y", function(d) { 
                    if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI) {
                        var flagDiffText = 0;
                            
                        for (var i = 0; i < genCoordsTextX.length; i++) {
                            var diffcoordsText = Math.abs(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5 - genCoordsTextX[i]);
                            
                            if (diffcoordsText < 10 && diffcoordsText > 0) {
                                flagDiffText = 1;
                                break;
                            } else {
                                continue;
                            }
                        }
                            
                        if (flagDiffText == 0) {
                            genCoordsTextX.push(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                            return "" + (findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                        } else {
                            if (firstTextX == 0) {
                                var incresText = findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
                                incresText += 25;
                                firstTextX = 1;
                                incresText = findIsolation(incresText, genCoordsTextX);
                                latestTextX = incresText;
                                genCoordsTextX.push(incresText);
                                return "" + incresText;
                            } else {
                                latestTextX += 20;
                                genCoordsTextX.push(latestTextX);
                                return "" + latestTextX;
                            }

                        }
                    } else {
                        var flagDiffText = 0;
                        
                        for (var i = 0; i < genCoordsTextY.length; i++) {
                            var diffcoordsText = Math.abs(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5 - genCoordsTextY[i]);
                            
                            if (diffcoordsText < 10 && diffcoordsText > 0) {
                                flagDiffText = 1;
                                break;
                            } else {
                                continue;
                            }
                        }
                            
                        if (flagDiffText == 0) {
                            genCoordsTextY.push(findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                            return "" + (findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y + 5);
                        } else {
                            if (firstText == 0) {
                                var incresText = findCoordinates(r - 5, (d.startAngle + d.endAngle) / 2).y;
                                incresText += 25;
                                firstTextY = 1;
                                incresText = findIsolation(incresText, genCoordsTextY);
                                latestTextY = incresText;
                                genCoordsTextY.push(incresText);
                                return "" + incresText;
                            } else {
                                latestTextY += 20;
                                genCoordsTextY.push(latestTextY);
                                return "" + latestTextY;
                            }
                        }
                    } })
            .attr("x", function(d) {
                        if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                            return "-" + (r + 10);
                        else
                            return "" + (r + 10); })
            .attr("fill", function(d, i) { return color(i); })
            .attr("text-anchor", function(d) {
                            if ((d.startAngle + d.endAngle) / 2 > Math.PI && (d.startAngle + d.endAngle) / 2 < 2 * Math.PI)
                                return "end";
                            else
                                return "begning"; })
            .text(function(d) { return d.value; });
        
        valueLabels[siteIndex].exit()
            .remove();

        //DRAW LABELS WITH ENTITY NAMES
        nameLabels[siteIndex] = label_group.selectAll("text.units")
            .data(filteredPieData)
            .attr("dy", function(d) {
                    if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2 && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
                        return 17;
                    } else {
                        return 5;
                    } })
            .attr("text-anchor", function(d) {
                    if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
                        return "beginning";
                    } else {
                        return "end";
                    } })
            .text(function(d) { return d.name; });
        
        nameLabels[siteIndex].enter()
            .append("svg:text")
            .attr("class", "units")
            .attr("transform", function(d) {
                    return "translate(" + Math.cos(((d.startAngle + d.endAngle - Math.PI) / 2)) * (r + textOffset) + "," + Math.sin((d.startAngle + d.endAngle - Math.PI) / 2) * (r + textOffset) + ")"; })
                .attr("dy", function(d) {
                    if ((d.startAngle + d.endAngle) / 2 > Math.PI / 2 && (d.startAngle + d.endAngle) / 2 < Math.PI * 1.5) {
                        return 17;
                    } else {
                        return 5;
                    } })
                .attr("text-anchor", function(d) {
                    if ((d.startAngle + d.endAngle) / 2 < Math.PI) {
                        return "beginning";
                    } else {
                        return "end";
                    }
                })
            .text(function(d) { return d.name; });
        
        nameLabels[siteIndex].transition().duration(tweenDuration).attrTween("transform", textTween);
        nameLabels[siteIndex].exit().remove();
    }
}
    
///////////////////////////////////////////////////////////
//FUNCTIONS //////////////////////////////////////////////
///////////////////////////////////////////////////////////

//Interpolate the arcs in data space.
function pieTween_old(d, i) {
    var s0;
    var e0;
    
    if (oldPieData[i]) {
        s0 = oldPieData[i].startAngle;
        e0 = oldPieData[i].endAngle;
    } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
        s0 = oldPieData[i - 1].endAngle;
        e0 = oldPieData[i - 1].endAngle;
    } else if (!(oldPieData[i - 1]) && oldPieData.length > 0) {
        s0 = oldPieData[oldPieData.length - 1].endAngle;
        e0 = oldPieData[oldPieData.length - 1].endAngle;
    } else {
        s0 = 0;
        e0 = 0;
    }
    
    var i = d3.interpolate({
        startAngle: s0,
        endAngle: e0
    }, {
        startAngle: d.startAngle,
        endAngle: d.endAngle
    });
    
    return function(t) {
        var b = i(t);
        return arc(b);
    };
}

function removePieTween(d, i) {
    s0 = 2 * Math.PI;
    e0 = 2 * Math.PI;
    
    var i = d3.interpolate({
        startAngle: d.startAngle,
        endAngle: d.endAngle
    }, {
        startAngle: s0,
        endAngle: e0
    });
    
    return function(t) {
        var b = i(t);
        return arc(b);
    };
}

function pieTween (b){
    b.innerRadius = 0;
    var i = d3.interpolate({startAngle: 0, endAngle: 0}, b);
            
    return function (t) { return arc(i(t)); };
}   

function textTween(d, i) {
    var a;
    
    if (oldPieData[i]) {
        a = (oldPieData[i].startAngle + oldPieData[i].endAngle - Math.PI) / 2;
    } else if (!(oldPieData[i]) && oldPieData[i - 1]) {
        a = (oldPieData[i - 1].startAngle + oldPieData[i - 1].endAngle - Math.PI) / 2;
    } else if (!(oldPieData[i - 1]) && oldPieData.length > 0) {
        a = (oldPieData[oldPieData.length - 1].startAngle + oldPieData[oldPieData.length - 1].endAngle - Math.PI) / 2;
    } else {
        a = 0;
    }
    
    var b = (d.startAngle + d.endAngle - Math.PI) / 2;

    var fn = d3.interpolateNumber(a, b);
    
    return function(t) {
        var val = fn(t);
        return "translate(" + Math.cos(val) * (r + textOffset) + "," + Math.sin(val) * (r + textOffset) + ")";
    };
}
</script>
<!-- end d3 Script-->			
		</div>
	</div>
	<!-- END OFFICE PORTLET GRAPH CONTENT-->
</div>