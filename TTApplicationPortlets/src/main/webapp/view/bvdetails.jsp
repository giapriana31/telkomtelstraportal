<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<script src="<%=request.getContextPath() %>/js/d3.js"></script>
<!-- <script src="<%=request.getContextPath() %>/js/jquery.min.js"></script>  -->
<script src="<%=request.getContextPath() %>/js/main.js"></script>
<script src="<%=request.getContextPath() %>/js/bvdashboard.js"></script>
<script src="<%= request.getContextPath()%>/js/jquery.dataTables.min.js"></script>  
<link href="${pageContext.request.contextPath}/css/sites.css" rel="stylesheet" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.dataTables.css"/>
	
<%@ include file="common.jsp" %>

<script>

$(document).ready(function()
{
	smtableTabClick(); 
});

function sstableTabClick() 
{
	$('#smtable-li').removeClass('selected');
	$('#sstable-li').addClass('selected');
	$('#smtablediv').addClass('hide');
	$('#sstablediv').removeClass('hide');
	$('#titleOfTabSS').html('<liferay-ui:message key="Subscribed Services"/>');
	subscribedServicesTable();
}

function smtableTabClick() 
{	
	$('#sstable-li').removeClass('selected');
	$('#smtable-li').addClass('selected');
	$('#smtablediv').removeClass('hide');
	$('#sstablediv').addClass('hide');
	$('#titleOfTabSS').html('<liferay-ui:message key="Site Management"/>');
	var URLbv2 = window.location.href;

if(URLbv2.includes("bvdetails"))
{
	siteManagementTableAjax2();
}
else
{
	siteManagementTableAjax();
}
}
</script>

<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
				<span class="tt-current-page" id="titleOfTabSS"></span>
			</div>
		</div>
		<div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.bv.homeURL")%>/<%=prop.getProperty("ttBVDashboard")%>'" class="btn-primary-tt backButton">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
	</div>
	<div class="row-fluid tt-page-content">
		<ul id="tabc" class="nav nav-pills tt-site-ul">
			<li class="tt-site-nav-pills selected" id="smtable-li">
				<a data-toggle="tab" href="#siteManagement" class="tt-site-nav-pills" id="smtable" onclick="smtableTabClick()"><liferay-ui:message key="Site Management"/></a>
			</li>
			<li class="active tt-site-nav-pills " id="sstable-li">
				<a data-toggle="tab" href="#subscribedServices" class="tt-site-nav-pills" id="sstable" onclick="sstableTabClick()"><liferay-ui:message key="Subscribed Services"/></a>
			</li>			
		</ul>
	</div>
	
	<%
		String currentURL = themeDisplay.getURLCurrent();
		String bvDetailsURL = TTGenericUtils.prop.getProperty("ttBVDetails");
	%>
	
	<div id="sstablediv" class="row-fluid tt-site-tab-incident-content">
		<%@ include file="subscribedservicestable.jsp" %>
	</div>

	<div id="smtablediv" class="row-fluid tt-site-tab-incident-content">
		<%@ include file="sitemanagementtable.jsp" %>
	</div>
</div>