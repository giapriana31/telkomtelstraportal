package com.common.refactor;

public class ProductClassificationRefactor {
	
	public static final String MNS_LAN = "MNS LAN";
	public static final String MNS_WAN = "MNS WAN";
	public static final String MNS_WAN_OPTI = "MNS WAN OPTI";
	public static final String CLOUD_CONTACT_CENTER = "Cloud Contact Center";
	public static final String DIGITAL_PROXIMITRY = "Digital Proximitry";
	public static final String MULTI_CHANNEL_COMMUNICATION = "Multi Channel Communication";
	
	public static final String M_LAN = "M-LAN";
	public static final String M_WAN = "M-WAN";
	public static final String M_WANOPT = "M-WANOPT";
	public static final String IPSCAPE = "IPScape";
	public static final String WHISPIR = "Whispir";
	public static final String MANDOE_DIGITAL_SIGNAGE = "Mandoe Digital Signage";
	

	
	
	public static String refactorNewProductClassificationToPlain(String serviceNameChanged) {
		String plainName;
		switch(serviceNameChanged) {
			case MNS_LAN :
				plainName =M_LAN;
		    break;
			case MNS_WAN :
				plainName =M_WAN;
		    break;
			case MNS_WAN_OPTI :
				plainName =M_WANOPT;
		    break;
			case CLOUD_CONTACT_CENTER :
				plainName =IPSCAPE;
		    break;
			case DIGITAL_PROXIMITRY :
				plainName =WHISPIR;
		    break;
			case MULTI_CHANNEL_COMMUNICATION :
				plainName =MANDOE_DIGITAL_SIGNAGE;
		    break;
			default : 
				plainName =serviceNameChanged;
			break;
		}
		return plainName;
	}
	
	public static String refactorPlainToNewProductClassification(String serviceNameChanged) {
		String newProductClassificationName;
		switch(serviceNameChanged) {
			case M_LAN:
				newProductClassificationName =MNS_LAN;
		    break;
			case M_WAN :
				newProductClassificationName =MNS_WAN;
		    break;
			case M_WANOPT:
				newProductClassificationName =MNS_WAN_OPTI;
		    break;
			case IPSCAPE:
				newProductClassificationName =CLOUD_CONTACT_CENTER;
		    break;
			case  WHISPIR:
				newProductClassificationName =DIGITAL_PROXIMITRY;
		    break;
			case  MANDOE_DIGITAL_SIGNAGE:
				newProductClassificationName =MULTI_CHANNEL_COMMUNICATION;
		    break;
			default : 
				newProductClassificationName =serviceNameChanged;
			break;
		}
		return newProductClassificationName;
	}



}
