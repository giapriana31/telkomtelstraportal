package com.common.exceptionhandling;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.tt.exception.GenericException;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(SQLException.class)
    public String handleSQLException(HttpServletRequest request, Exception ex) {
	logger.info("SQLException Occured:: URL=" + request.getRequestURL());
	return "database_error";
    }


    @ExceptionHandler(GenericException.class)
    public String handleCustomException(HttpServletRequest request, Exception ex, Model model) {
	logger.info("SQLException Occured:: URL=" + request.getRequestURL());
	logger.error("Requested URL=" + request.getRequestURL());
	logger.error("Exception Raised=" + ex);

	model.addAttribute("exception", ex);
	model.addAttribute("url", request.getRequestURL());
	return "error";

    }
}
