package com.common.exceptionhandling.resolver;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

public class MySimpleMappingExceptionResolver extends SimpleMappingExceptionResolver {

    Logger log = Logger.getLogger(MySimpleMappingExceptionResolver.class);
    
    @Override
    public String buildLogMessage(Exception ex, HttpServletRequest request) {
	log.debug("Test the flow -------->>>"+ex.getMessage());
	
	return "Spring MVC exception: " + ex.getLocalizedMessage();
    }
}
