package com.common.subscribedservices;

import java.math.BigInteger;

import org.hibernate.Query;
import org.hibernate.Session;

import com.tt.dao.HibernateUtil;

public class SubscribedService {
	
	
	
	 public static int isSubscribedServices(String serviceName, String customerId)
	    {
	    	Session session = HibernateUtil.getSessionFactory().openSession();
	    	
	    	StringBuilder queryString = new StringBuilder();
			queryString.append("select count(1) from configurationitem where rootProductId=:serviceName and customeruniqueid=:customerId");
			
			Query query = session.createSQLQuery(queryString.toString());
			query.setString("serviceName", serviceName);
			query.setString("customerId", customerId);
			
			int count = ((BigInteger) query.uniqueResult()).intValue();
			
			session.close();
			return count;
	    }
	 
	 
	 public static int isSubscribedServicesOpt(String productclassification, String customerId){
		 Session session = HibernateUtil.getSessionFactory().openSession();
	    	
	    	StringBuilder queryString = new StringBuilder();
			queryString.append("select count(1) from configurationitem where productclassification=:productclassification and customeruniqueid=:customerId");
			
			Query query = session.createSQLQuery(queryString.toString());
			query.setString("productclassification", productclassification);
			query.setString("customerId", customerId);
			
			int count = ((BigInteger) query.uniqueResult()).intValue();
			
			session.close();
			return count;
	 }
	 
	 public static int isSubscribedServicesSite(String customerId){
		 Session session = HibernateUtil.getSessionFactory().openSession();
	    	
	    	StringBuilder queryString = new StringBuilder();
			queryString.append(" select count(1) from Site sa where sa.isManaged <>'No' and sa.isDataCenter <> 'Yes' and sa.rootProductId in ('MNS','MSS') and sa.customeruniqueid=:customerId and status in ('Operational') ");
			
			Query query = session.createSQLQuery(queryString.toString());
			query.setString("customerId", customerId);
			
			int count = ((BigInteger) query.uniqueResult()).intValue();
			
			session.close();
			return count;
	 
	 }

}
