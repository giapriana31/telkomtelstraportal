package com.tt.devicemapping.mvc.service.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.devicemapping.mvc.dao.DeviceSearchDaoImpl;
import com.tt.devicemapping.mvc.model.DeviceResults;
import com.tt.devicemapping.mvc.model.DeviceSearchParameters;

@Service(value="deviceServiceManager")
@Transactional
public class DeviceServiceManagerImpl {

	private DeviceSearchDaoImpl deviceDaoImpl;

	@Autowired
	@Qualifier("deviceServiceDaoImpl")
	public void setDeviceDaoImpl(DeviceSearchDaoImpl deviceDaoImpl) {
		this.deviceDaoImpl = deviceDaoImpl;
	}
	

	public DeviceResults getDevices(DeviceSearchParameters deviceSearchParameters){
		return deviceDaoImpl.getDevices(deviceSearchParameters);
	}
	
}
