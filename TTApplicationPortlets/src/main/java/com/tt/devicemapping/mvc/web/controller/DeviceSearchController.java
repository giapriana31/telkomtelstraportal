
package com.tt.devicemapping.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.aop.aspectj.annotation.ReflectiveAspectJAdvisorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.portlet.util.PortletUtils;

import com.common.refactor.ProductClassificationRefactor;
import com.google.gson.Gson;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.devicemapping.mvc.model.DeviceResults;
import com.tt.devicemapping.mvc.model.DeviceSearchParameters;
import com.tt.devicemapping.mvc.service.common.DeviceServiceManagerImpl;
import com.tt.logging.Logger;
import com.tt.model.Configuration;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("deviceSearchController")
@RequestMapping("VIEW")
public class DeviceSearchController {

    private DeviceServiceManagerImpl deviceSearchManager;
    
    public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(DeviceSearchController.class);
    
    public static final String SITENAME = "s.customersite";
	public static final String LOCATION = "s.stateprovince";
	public static final String SERVICE = "c.cmdbclass";
	public static final String PRODUCTCLASSIFICATION = "c.productclassification";
	public static final String TIERS = "s.servicetier";
	public static final String LINK = "c.link";
	public static final String STATUS = "c.deliverystatus";
	public static final String ACTIVATEDON = "c.activatedon";
	public static final String CCD = "c.customercommiteddate";
	
	
 	private static Map<String, String> mapDTSortCriterion() {
 		Map<String, String> sortCriterion = new HashMap<String, String>();
 		sortCriterion.put("0", SITENAME);
 		sortCriterion.put("1", LOCATION);
 		sortCriterion.put("2", SERVICE);
 		sortCriterion.put("8", PRODUCTCLASSIFICATION); 
 		sortCriterion.put("3", TIERS);
 		sortCriterion.put("4", LINK);
 		sortCriterion.put("5", STATUS); 
 		sortCriterion.put("6", ACTIVATEDON); 
 		sortCriterion.put("7", CCD); 
 		
 		
 		return Collections.unmodifiableMap(sortCriterion);
 	}
    
 	private static final Map<String, String> DTSORTCRITERION = mapDTSortCriterion();
 	
	@Autowired
    @Qualifier("deviceServiceManager")
    public void setDeviceSearchManager(DeviceServiceManagerImpl deviceSearchManager) {
		this.deviceSearchManager = deviceSearchManager;
    }

    @RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws Exception {
		try {
			HttpServletRequest httprequest = PortalUtil
					.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil
					.getHttpServletResponse(renderResponse);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(
					httprequest, httpresponse);
			String siteName="All";
			if(null!=PortletUtils.getSessionAttribute(request, "siteNameSession")){
				siteName=(String)PortletUtils.getSessionAttribute(request, "siteNameSession");
			}
			model.addAttribute("siteNameReceived",siteName);
			System.out.println("site name received"+siteName);
			PortletUtils.setSessionAttribute(request, "siteNameSession", null);
			
			return "deviceSearchMapping";
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

  
	@ResourceMapping(value = "datatableURI")
    public void getDeviceData(ResourceRequest resourceRequest,ResourceResponse resourceResponse)
	{
		log.info("Class: DeviceSearchController, Method: getDeviceData Start");
    	PrintWriter out = null;
    	
    	try{  
	    	
	    	DeviceSearchParameters searchParameters = getUserDeviceSearchParameters(resourceRequest,resourceResponse);
		    
		    DeviceResults deviceResults= deviceSearchManager.getDevices(searchParameters);
		    
			com.liferay.portal.kernel.json.JSONObject dtJsonValue = getResultJson(deviceResults);
			
			log.info("dtJsonValue in getServiceRequestData : " + dtJsonValue );
			
			out = resourceResponse.getWriter();			
			out.print(dtJsonValue.toString());
			log.info("Class: ServiceRequestController, Method: getServiceRequestData End");
    	}
    	catch(Exception e){
    		log.error("Error in getServiceRequestData " + e.getMessage());
    		e.printStackTrace();
    	}
    	finally{
    		if(null != out) {
    			out.flush();
    			out.close();
    		}
    	}		
    }
	
	
	public DeviceSearchParameters getUserDeviceSearchParameters(ResourceRequest resourceRequest,ResourceResponse resourceResponse) {
		
		log.info("Class: DeviceSearchController, Method: getUserDeviceSearchParameters");
		DeviceSearchParameters searchParameters = new DeviceSearchParameters();		
		
		try {
		
			HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(resourceRequest);
			HttpServletResponse httpresponse = PortalUtil.getHttpServletResponse(resourceResponse);			
			Enumeration paramNames = httprequest.getParameterNames();
			
			String cusomerUniqueId = TTGenericUtils.getCustomerIDFromSession(httprequest, httpresponse);
			searchParameters.setCustomeruniqueid(cusomerUniqueId);
			
			//todo:delet after test
			while (paramNames.hasMoreElements()) {
				Map <String, String>requestParamMap = new HashMap<String, String>();

				String paramName = (String) paramNames.nextElement();
				
				//todo:still needs to handle multi-selects
				//request.getParameterValues(paramName);
				
				String paramValue = httprequest.getParameter(paramName);
				requestParamMap.put(paramName, paramValue);
				log.info("paramName : " + paramName + " paramValues: "+ paramValue);
			}

			searchParameters.setSortCriterion(
					httprequest.getParameter("order[0][column]") != null 
					? DTSORTCRITERION.get(httprequest.getParameter("order[0][column]")) 
					: DTSORTCRITERION.get(SITENAME));
			
			searchParameters.setSortDirection(
					httprequest.getParameter("order[0][dir]") != null 
					? httprequest.getParameter("order[0][dir]") 
					: "asc");		
			
			searchParameters.setPageNumber(
					httprequest.getParameter("start") != null 
					? Integer.parseInt(httprequest.getParameter("start").toString())
					: 0);	
			log.info("search parameters page number---"+searchParameters.getPageNumber());
			
			searchParameters.setSearchqueryList(
					httprequest.getParameter("searchTerm") != null 
					? httprequest.getParameter("searchTerm").toString()
					: null);
			
			searchParameters.setMaxResults(
					httprequest.getParameter("maxResults") != null 
					? Integer.parseInt(httprequest.getParameter("maxResults").toString())
					: 30);
			
			searchParameters.setDeliveryStatus(
					httprequest.getParameter("deliveryStatus") != null 
					? httprequest.getParameter("deliveryStatus").toString()
					: null);
			searchParameters.setCmdbclass(
					httprequest.getParameter("service") != null
					? ProductClassificationRefactor.refactorNewProductClassificationToPlain(httprequest.getParameter("service").toString())
					: null);
			searchParameters.setSiteName(
					httprequest.getParameter("siteName") != null
					? httprequest.getParameter("siteName").toString()
					: null);
			searchParameters.setCiFilter(
					httprequest.getParameter("ciFilter") != null
					? httprequest.getParameter("ciFilter").toString()
					: null);
			if(null!=searchParameters.getDeliveryStatus()){
				if(searchParameters.getDeliveryStatus().equalsIgnoreCase("Total")){
					searchParameters.setDeliveryStatus(null);
				}
			}	
			if(null!=searchParameters.getCmdbclass()){
				if(searchParameters.getCmdbclass().equalsIgnoreCase("All")){
					searchParameters.setCmdbclass(null);
				}
			}
			if(null!=searchParameters.getSiteName()){
				if(searchParameters.getSiteName().equalsIgnoreCase("All")){
					searchParameters.setSiteName(null);
				}
			}
			if(null!=searchParameters.getCiFilter()){
				if(searchParameters.getCiFilter().equalsIgnoreCase("All")){
					searchParameters.setCiFilter(null);
				}
			}
			if(null!=searchParameters.getSearchqueryList() && 
					!searchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")){
				searchParameters.setSiteName(searchParameters.getSearchqueryList());
				searchParameters.setLocation(searchParameters.getSearchqueryList());
				searchParameters.setDeliveryStatus(searchParameters.getSearchqueryList());
				searchParameters.setTiers(searchParameters.getSearchqueryList());
				searchParameters.setCmdbclass(searchParameters.getSearchqueryList());
				searchParameters.setLink(searchParameters.getSearchqueryList());
				searchParameters.setActivatedOn(searchParameters.getSearchqueryList());
				searchParameters.setCommittedDate(searchParameters.getSearchqueryList());
				log.info(searchParameters.toString());
			}

			log.info("getAllQueryParameters request param = " + searchParameters.toString());	
			
		} catch (Exception e) {		
			e.printStackTrace();
		}
		
		return searchParameters;
	}
	
	public String getAllQueryParamAsJson(HttpServletRequest request) {
		
		//collect all the request parameters in a hash map
		HashMap grequestparamMap = getAllQueryParameters(request);
		
		//Convert the hashmap to json
		Gson gson = new Gson();		
		String queryParamJson = gson.toJson(grequestparamMap);
		
		return queryParamJson;
	}
	
	public HashMap getAllQueryParameters(HttpServletRequest request) {

		HashMap <String, String>requestParamMap = new HashMap<String, String>();

		Enumeration paramNames = request.getParameterNames();

		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			
			//todo:still needs to handle multi-selects
			//request.getParameterValues(paramName);
			
			String paramValue = request.getParameter(paramName);
			requestParamMap.put(paramName, paramValue);
			log.info("paramName : " + paramName + " paramValues: "+ paramValue);
		}		
		
		requestParamMap.put("sortCriterion",
				request.getParameter("order[0][column]") != null 
				? DTSORTCRITERION.get(request.getParameter("order[0][column]")) 
				: DTSORTCRITERION.get(SITENAME));
		
		requestParamMap.put("sortDirection",
				request.getParameter("order[0][dir]") != null 
				? request.getParameter("order[0][dir]") 
				: "asc");		
		
		Integer pageNo=(Integer.parseInt(request.getParameter("start"))/10)+1;
		requestParamMap.put("pageNumber",
				request.getParameter("start") != null 
				? pageNo.toString() 
				: "0");				

		log.info("getAllQueryParameters request param = " + requestParamMap);
		
		return requestParamMap;
	}
	
	public com.liferay.portal.kernel.json.JSONObject getResultJson(DeviceResults deviceResults){
		
		JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
		com.liferay.portal.kernel.json.JSONObject dtJsonValue = JSONFactoryUtil
				.createJSONObject();

		if (deviceResults != null) {
			dtJsonValue.put("draw", deviceResults.getDraw());
			dtJsonValue.put("recordsFiltered", deviceResults.getRecordCount());

			resultJsonArray = populateDeviceArrayData(deviceResults
					.getConfigurationItems());

			dtJsonValue.put("recordsTotal", deviceResults.getRecordCount());
			dtJsonValue.put("data", resultJsonArray);
		} else {
			dtJsonValue.put("draw", 0);
			dtJsonValue.put("recordsFiltered", 0);
			dtJsonValue.put("recordsTotal", 0);
			dtJsonValue.put("data", resultJsonArray);
		}

		return dtJsonValue;
	}
	
	public JSONArray populateDeviceArrayData(List<Configuration> deviceList){
		JSONArray rowJsonValue;
    	
    	JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
    	
    	for (Configuration configuration : deviceList) 
    	{
    		rowJsonValue = JSONFactoryUtil.createJSONArray();
    		rowJsonValue.put(configuration.getSitename());
    		rowJsonValue.put(configuration.getCarriageserviced());
    		rowJsonValue.put(configuration.getCmdblass());
    		rowJsonValue.put(configuration.getProductclassification());
    		rowJsonValue.put(configuration.getColor());
    		rowJsonValue.put(configuration.getLink());
    		rowJsonValue.put(configuration.getDeliverystatus());
    		//rowJsonValue.put(configuration.getActivatedon());
    		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    		Date dateccd,dateActdOn;
    		String dateStringCcd = null;
    		String dateStringActdOn = null;
			try {
				dateccd = simpleDateFormat.parse(configuration.getCustomercommiteddate());
				dateActdOn = simpleDateFormat.parse(configuration.getActivatedon());
				dateStringCcd = simpleDateFormat.format(dateccd);
				dateStringActdOn = simpleDateFormat.format(dateActdOn);
			} catch (ParseException e) {
				log.error(e.getMessage());
			}
			rowJsonValue.put(dateStringActdOn);
     		rowJsonValue.put(dateStringCcd);
     	
			
			resultJsonArray.put(rowJsonValue);				
		}
    	return resultJsonArray;
	}
	
	@EventMapping(value = "{http://localhost:8080/pvSiteTicket}siteName")
	public void processEventSiteName(EventRequest request, EventResponse response)
			throws PortletException, IOException {

		log.info("SiteName recieved and processed through IPC for sitename in PV SR controller");

		javax.portlet.Event event = request.getEvent();
		String siteName = (String) event.getValue();
		log.info("sitename--" + siteName);
		PortletUtils.setSessionAttribute(request, "siteNameSession", siteName);

	}
}

