package com.tt.devicemapping.mvc.model;

public class DeviceSearchParameters {

	private String sortCriterion;
	private String sortDirection;
	private int pageNumber = 1; // default value
	private String searchqueryList;
	private int maxResults = 0;

	// Set it from session
	private String customeruniqueid;
	private String deliveryStatus;
	private String cmdbclass;
	private String ciFilter;
	private String siteName;
	private String location;
	private String tiers;
	private String link;
	private String activatedOn;
	private String committedDate;
	
	public String getCiFilter() {
		return ciFilter;
	}
	public void setCiFilter(String ciFilter) {
		this.ciFilter = ciFilter;
	}
	
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getSortCriterion() {
		return sortCriterion;
	}
	public void setSortCriterion(String sortCriterion) {
		this.sortCriterion = sortCriterion;
	}
	public String getSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSearchqueryList() {
		return searchqueryList;
	}
	public void setSearchqueryList(String searchqueryList) {
		this.searchqueryList = searchqueryList;
	}
	public int getMaxResults() {
		return maxResults;
	}
	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}
	public String getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
	public String getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getCmdbclass() {
		return cmdbclass;
	}
	public void setCmdbclass(String cmdbclass) {
		this.cmdbclass = cmdbclass;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTiers() {
		return tiers;
	}
	public void setTiers(String tiers) {
		this.tiers = tiers;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getActivatedOn() {
		return activatedOn;
	}
	public void setActivatedOn(String activatedOn) {
		this.activatedOn = activatedOn;
	}
	public String getCommittedDate() {
		return committedDate;
	}
	public void setCommittedDate(String committedDate) {
		this.committedDate = committedDate;
	}
	@Override
	public String toString() {
		return "DeviceSearchParameters [sortCriterion=" + sortCriterion + ", sortDirection=" + sortDirection
				+ ", pageNumber=" + pageNumber + ", searchqueryList=" + searchqueryList + ", maxResults=" + maxResults
				+ ", customeruniqueid=" + customeruniqueid + ", deliveryStatus=" + deliveryStatus + ", cmdbclass="
				+ cmdbclass + ", ciFilter=" + ciFilter + ", siteName=" + siteName + ", location=" + location
				+ ", tiers=" + tiers + ", link=" + link + ", activatedOn=" + activatedOn + ", committedDate="
				+ committedDate + "]";
	}
}
