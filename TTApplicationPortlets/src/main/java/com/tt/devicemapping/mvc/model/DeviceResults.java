package com.tt.devicemapping.mvc.model;

import java.util.List;

import com.tt.model.Configuration;

public class DeviceResults {

	private int recordCount = 0 ; // default value
	private List<Configuration> configurationItems;
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	public List<Configuration> getConfigurationItems() {
		return configurationItems;
	}
	public void setConfigurationItems(List<Configuration> configurationItems) {
		this.configurationItems = configurationItems;
	}
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	private int draw = 0 ;
	private int recordsFiltered = 0;
}
