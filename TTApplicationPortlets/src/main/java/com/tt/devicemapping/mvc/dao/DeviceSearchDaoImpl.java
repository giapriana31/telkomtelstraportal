package com.tt.devicemapping.mvc.dao; 

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.common.refactor.ProductClassificationRefactor;
import com.tt.devicemapping.mvc.model.DeviceResults;
import com.tt.devicemapping.mvc.model.DeviceSearchParameters;
import com.tt.logging.Logger;
import com.tt.model.Configuration;
import com.tt.utils.PropertyReader;

@Repository(value = "deviceServiceDaoImpl")
@Transactional
public class DeviceSearchDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;
    public static Properties prop = PropertyReader.getProperties();
    private Session session = null;
    private Transaction tx = null;
    private final static Logger log = Logger.getLogger(DeviceSearchDaoImpl.class);

    
    public DeviceResults getDevices(DeviceSearchParameters deviceSearchParameters){
                
                System.out.println("Dao implementation for device Search");
                DeviceResults deviceResults= null;
                
                                StringBuilder searhQuery = new StringBuilder();
                                StringBuilder searhQueryInner= new StringBuilder();
                                
                                String selectObj="select  s.customersite,s.stateprovince, case when c.rootProductId = 'SaaS' then c.productid else c.rootProductId end,s.servicetier,c.link,c.deliverystatus,c.activatedon,c.customercommiteddate,c.productclassification,(select count(distinct ci.ciid) from configurationitem ci join site si where ci.siteid=si.siteid and ci.customeruniqueid =:customerId and ((ci.rootproductid = 'Private Cloud' and ci.cmdbclass = 'vBLOCK' and ci.citype='Resource') or (ci.rootproductid = 'MNS' and ci.cmdbclass in ('CPE') and ci.citype='Resource') or (ci.rootproductid = 'SaaS' and ci.cmdbclass in ('SaaS Business Service') and ci.citype='Service'))";
								
								String selectInnerQueryObj=" and si.customersite=s.customersite) as countDev";
                                
                                String sourceQueryString=" from configurationitem c join site s where c.siteid=s.siteid and c.customeruniqueid =:customerId and ((c.rootproductid = 'Private Cloud' and c.cmdbclass = 'vBLOCK' and c.citype='Resource') or (c.rootproductid = 'MNS' and c.cmdbclass in ('CPE') and c.citype='Resource') or (c.rootproductid = 'SaaS' and c.cmdbclass in ('SaaS Business Service') and c.citype='Service'))";
                                
                                //construct the searhquery and searchqueryinner
                                if(deviceSearchParameters.getSearchqueryList() == null || 
                                		deviceSearchParameters.getSearchqueryList().isEmpty()) {
                                	constructQueryWithoutSearchListQuery(deviceSearchParameters,searhQuery,searhQueryInner);
                                }else {
                                	constructQueryWithSearchQueryList(deviceSearchParameters,searhQuery,searhQueryInner);                                                                      
                                }
                                	                                
                                log.info("Printing searhQuery for getting devices " + searhQuery);

                                
                                
                                Query queryCount = sessionFactory.getCurrentSession().createSQLQuery("select count(*) "+sourceQueryString+searhQuery.toString());
                                if (deviceSearchParameters.getCustomeruniqueid() != null)
                                {
                                                queryCount.setString("customerId", deviceSearchParameters.getCustomeruniqueid());
                                }
                                
                                /* getting count from the query - used for pagination */
                                Number recordCounts = (Number) queryCount.uniqueResult();
                                
                                if (recordCounts != null && recordCounts.intValue() > 0) {
                                                
                                                deviceResults= new DeviceResults();
                                                deviceResults.setRecordCount(recordCounts.intValue());
                                                log.info("recordCounts.intValue() \n "
                                                                                + recordCounts.intValue());
																				
												String formerQueryString=selectObj+searhQueryInner.toString()+selectInnerQueryObj;
                                                // sortCriterion
                                                if (deviceSearchParameters.getSortCriterion() != null
                                                                                && !deviceSearchParameters.getSortCriterion().isEmpty())
                                                                searhQuery.append(" ORDER BY ").append(
                                                                                                deviceSearchParameters.getSortCriterion());

                                                // asc or desc
                                                if (deviceSearchParameters.getSortDirection() != null
                                                                                && !deviceSearchParameters.getSortDirection().isEmpty())
                                                                searhQuery.append(" ").append(
                                                                                                deviceSearchParameters.getSortDirection());
                                                // page no pageNumber
                                                Integer offsetInt = deviceSearchParameters.getPageNumber();
                                                Integer maxResults = deviceSearchParameters.getMaxResults();
                                                
                                                searhQuery.append(" limit ").append(maxResults);
                                                searhQuery.append(" offset ").append(offsetInt);
                                                
                                                Query query = sessionFactory.getCurrentSession().createSQLQuery(formerQueryString+sourceQueryString+searhQuery.toString());
                                                                
                                                
                                                if (deviceSearchParameters.getCustomeruniqueid() != null)
                                                {
                                                                query.setString("customerId", deviceSearchParameters.getCustomeruniqueid());
                                                }
                                                
                                                log.info("Printing final query with sorting \n "
                                                                                                + formerQueryString+sourceQueryString+searhQuery.toString());
                                                                
                                                List<Configuration> deviceList = new ArrayList<Configuration>();
                                                
                                                List configList = query.list();
                                                log.info("list obtained from device query :: " + configList);
                                                
                                                //construct the device list
                                                constructDeviceList(deviceList,configList,null,null,0,1,0);
                                                                                                
                                                deviceResults.setConfigurationItems(deviceList);
                                }
                                                

                                log.info("DeviceSearchDaoImpl, getDevices with parameters, End");
                                return deviceResults;

    }
    

	private void constructQueryWithSearchQueryList(DeviceSearchParameters deviceSearchParameters,
			StringBuilder searhQuery, StringBuilder searhQueryInner) {
		
			log.info("constructQueryWithSearchQueryList with parameter :"+ deviceSearchParameters.toString());
			
			searhQuery.append(" and (");
			searhQueryInner.append(" and (");
			if(deviceSearchParameters.getCmdbclass()!=null){
				
			     searhQuery.append(" (c.rootProductId in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQuery.append(" or c.productclassification in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQuery.append(" or c.productId in ('").append(deviceSearchParameters.getCmdbclass()).append("'))");
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("Private Cloud")){
			                     searhQuery.append(" and c.cmdbclass = 'vBLOCK'");
			     }
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-LAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WANOPTI")){
			         searhQuery.append(" and c.productclassification is not null and c.productclassification <> ''");
			     }   
			     searhQueryInner.append(" (ci.rootProductId in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQueryInner.append(" or c.productclassification in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQueryInner.append(" or c.productId in ('").append(deviceSearchParameters.getCmdbclass()).append("'))");
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("Private Cloud")){
			     	searhQueryInner.append(" and ci.cmdbclass = 'vBLOCK'");
			     }
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-LAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WANOPTI")){
			         searhQuery.append(" and c.productclassification is not null and c.productclassification <> ''");
			     }                                       
			}
			
			if(deviceSearchParameters.getDeliveryStatus()!=null){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" c.deliverystatus in ('").append(deviceSearchParameters.getDeliveryStatus()).append("')");
	            searhQueryInner.append(" ci.deliverystatus in ('").append(deviceSearchParameters.getDeliveryStatus()).append("')");
			}
			
			if(deviceSearchParameters.getSiteName()!=null){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" s.customersite in ('").append(deviceSearchParameters.getSiteName()).append("')");
	            searhQueryInner.append(" si.customersite in ('").append(deviceSearchParameters.getSiteName()).append("')");
			}
			
			if(deviceSearchParameters.getLocation()!=null &&
					!deviceSearchParameters.getLocation().isEmpty()){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" s.stateprovince in ('").append(deviceSearchParameters.getLocation()).append("')");
	            searhQueryInner.append(" si.stateprovince in ('").append(deviceSearchParameters.getLocation()).append("')");
			}
			
			if(deviceSearchParameters.getTiers()!=null &&
					!deviceSearchParameters.getTiers().isEmpty()){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" s.servicetier in ('").append(deviceSearchParameters.getTiers()).append("')");
	            searhQueryInner.append(" si.servicetier in ('").append(deviceSearchParameters.getTiers()).append("')");
			}
			
			if(deviceSearchParameters.getLink()!=null &&
					!deviceSearchParameters.getLink().isEmpty()){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" c.link in ('").append(deviceSearchParameters.getLink()).append("')");
	            searhQueryInner.append(" ci.link in ('").append(deviceSearchParameters.getLink()).append("')");
			}
			if(deviceSearchParameters.getActivatedOn()!=null &&
					!deviceSearchParameters.getActivatedOn().isEmpty()){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" c.activatedon in ('").append(deviceSearchParameters.getActivatedOn()).append("')");
	            searhQueryInner.append(" ci.activatedon in ('").append(deviceSearchParameters.getActivatedOn()).append("')");
			}
			if(deviceSearchParameters.getCommittedDate()!=null &&
					!deviceSearchParameters.getCommittedDate().isEmpty()){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" c.customercommiteddate like '%").append(deviceSearchParameters.getCommittedDate()).append("%'");
	            searhQueryInner.append(" c.customercommiteddate like '%").append(deviceSearchParameters.getCommittedDate()).append("%'");
			}
			
			if(deviceSearchParameters.getCiFilter()!=null){
				//advance search or not
				if(deviceSearchParameters.getSearchqueryList().equalsIgnoreCase("advancesearch")) {
					searhQuery.append(" and ");
					searhQueryInner.append(" and ");
				}
				else {
					searhQuery.append(" or ");
					searhQueryInner.append(" or ");
				}
				searhQuery.append(" ((c.cmdbclass='vBLOCK' and c.ciid in ('"+deviceSearchParameters.getCiFilter()+"')) or (c.cmdbclass='SaaS Business Service' and c.ciid in ('"+deviceSearchParameters.getCiFilter()+"')))");
	            searhQueryInner.append(" ((ci.cmdbclass='vBLOCK' and ci.ciid in ('"+deviceSearchParameters.getCiFilter()+"')) or (c.cmdbclass='SaaS Business Service' and c.ciid in ('"+deviceSearchParameters.getCiFilter()+"')))");
			}
			searhQuery.append(" ) ");
			searhQueryInner.append(" ) ");
			
			log.info("searhQuery :"+ searhQuery.toString());
			log.info("searhQueryInner :"+ searhQueryInner.toString());

	}


	private void constructQueryWithoutSearchListQuery(DeviceSearchParameters deviceSearchParameters,
			StringBuilder searhQuery, StringBuilder searhQueryInner) {
			if(deviceSearchParameters.getCmdbclass()!=null){
			     searhQuery.append(" and (c.rootProductId in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQuery.append(" or c.productclassification in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQuery.append(" or c.productId in ('").append(deviceSearchParameters.getCmdbclass()).append("'))");
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("Private Cloud")){
			                     searhQuery.append(" and c.cmdbclass = 'vBLOCK'");
			     }
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-LAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WANOPTI")){
			         searhQuery.append(" and c.productclassification is not null and c.productclassification <> ''");
			     }   
			     searhQueryInner.append(" and (ci.rootProductId in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQueryInner.append(" or c.productclassification in ('").append(deviceSearchParameters.getCmdbclass()).append("')");
			     searhQueryInner.append(" or c.productId in ('").append(deviceSearchParameters.getCmdbclass()).append("'))");
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("Private Cloud")){
			     	searhQueryInner.append(" and ci.cmdbclass = 'vBLOCK'");
			     }
			     if(deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-LAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WAN")||
			     		deviceSearchParameters.getCmdbclass().equalsIgnoreCase("M-WANOPTI")){
			         searhQuery.append(" and c.productclassification is not null and c.productclassification <> ''");
			     }                                       
			}
			
			if(deviceSearchParameters.getDeliveryStatus()!=null){
			             searhQuery.append(" and c.deliverystatus in ('").append(deviceSearchParameters.getDeliveryStatus()).append("')");
			             searhQueryInner.append(" and ci.deliverystatus in ('").append(deviceSearchParameters.getDeliveryStatus()).append("')");
			}
			
			if(deviceSearchParameters.getSiteName()!=null){
			             searhQuery.append(" and s.customersite in ('").append(deviceSearchParameters.getSiteName()).append("')");
			             searhQueryInner.append(" and si.customersite in ('").append(deviceSearchParameters.getSiteName()).append("')");
			}
			
			if(deviceSearchParameters.getCiFilter()!=null){
			             searhQuery.append(" and ((c.cmdbclass='vBLOCK' and c.ciid in ('"+deviceSearchParameters.getCiFilter()+"')) or (c.cmdbclass='SaaS Business Service' and c.ciid in ('"+deviceSearchParameters.getCiFilter()+"')))");
			             searhQueryInner.append(" and ((ci.cmdbclass='vBLOCK' and ci.ciid in ('"+deviceSearchParameters.getCiFilter()+"')) or (c.cmdbclass='SaaS Business Service' and c.ciid in ('"+deviceSearchParameters.getCiFilter()+"')))");
			}
	}


	private void constructDeviceList(List<Configuration> deviceList, List configList,Object[] object, String siteNameCheck,Integer countCheck, int flagValue, int heldVal) {
		for(int i=0;i<configList.size(); i++)
        {
                        Configuration configuration= new Configuration();
                        log.info("setting all the parameters in the object : " + configList.get(i));
                        
                        object = (Object[]) configList.get(i);
                        
                        countCheck=Integer.parseInt(object[9].toString());
                        if(i==0){
                        	if(countCheck.intValue()==1){
                        		heldVal=0;
                        	}
                        	else{
                        		heldVal=flagValue;
                        	}
                        }else{
                        	if(countCheck.intValue()==1){
                        		heldVal=0;
                        	}
                        	else if(object[0].toString().equalsIgnoreCase(siteNameCheck)){
                        		heldVal=flagValue;
                        	}
                        	else{
                        		flagValue=toggleFlag(flagValue);
                        		heldVal=flagValue;
                        	}
                        }
                        
                        configuration.setSitename(null!=object[0] ? object[0].toString()+"&&&"+heldVal: "");
                        configuration.setCarriageserviced(null!=object[1] ? object[1].toString(): "");
                        configuration.setCmdblass(null!=object[2] ? ProductClassificationRefactor.refactorPlainToNewProductClassification(object[2].toString()): "");
                        configuration.setColor(null!=object[3] ? object[3].toString(): "");
                        configuration.setLink(null!=object[4] ? object[4].toString(): "");
                        configuration.setDeliverystatus(null!=object[5] ? object[5].toString(): "");
                        configuration.setActivatedon(null!=object[6] ? object[6].toString(): "");
                        configuration.setCustomercommiteddate(null!=object[7] ? object[7].toString(): "");
                        configuration.setProductclassification(null!=object[8] ? ProductClassificationRefactor.refactorPlainToNewProductClassification(object[8].toString()): "");
                        
                        deviceList.add(configuration);
                        
                        siteNameCheck=object[0].toString();
        }

		
	}


	public int toggleFlag(int flag){
    	if(flag==1){
    		return 2;
    	}
    	else{
    		return 1;
    	}
    }
    
}

