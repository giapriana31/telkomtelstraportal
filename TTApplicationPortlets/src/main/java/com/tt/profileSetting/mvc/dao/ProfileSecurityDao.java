package com.tt.profileSetting.mvc.dao;

import java.util.HashMap;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;

@Service(value = "profileSecurityDao")
@Transactional
public class ProfileSecurityDao
{

	private final static Logger log = Logger.getLogger(ProfileSecurityDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public String getCustomerName(String customerID)
	{

		String customerName = null;
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(
				"Select accountName from customers where customerUniqueId='" + customerID + "'");

		if (null != query.list() && !query.list().isEmpty())
		{

			customerName = (String) query.list().get(0);
		}

		return customerName;
	}

	public int getNumOfOktaUsers(String customerId)
	{
		System.out.println("inside Dao getNumOfOktaUsers");
		int numOfOktaUsers = 0;
		try
		{
			SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(
					"select numberofoktausers from customers where customerUniqueId='" + customerId + "'");
			@SuppressWarnings("unchecked")
			List<Integer> count = query.list();

			if (null != count && !count.isEmpty())
			{

				for (int i = 0; i < count.size(); i++)
				{
					numOfOktaUsers = count.get(i);
				}

			}
		}
		catch (Exception e)
		{
			System.out.println("Exception" + e.getMessage());
		}

		return numOfOktaUsers;
	}

	public String getSubCategory()
	{
		String subCategory = "";
		try{
			SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("Select subcategory FROM servicerequestsubcategory where subcategory = 'Acquiring additional user Licenses' and category = 'Generic'");
			@SuppressWarnings("unchecked")
			List<String> subCategoryTemp = query.list();
			if(null != subCategoryTemp && !subCategoryTemp.isEmpty()){
				for(int i = 0; i < subCategoryTemp.size(); i++){
					subCategory = subCategoryTemp.get(i);
				}
			}
		
		}
		catch (Exception e)
		{
			System.out.println("Exception" + e.getMessage());
		}

		return subCategory;
	}
}
