package com.tt.profileSetting.mvc.model;
public class UserProfile {

	
	private String firstName;
	private String lastName;
	private String customerID;
	private String customerName;
	private String location;
	private String workContact;
	private String homeContact;
	private String email;
	
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCustomerID() {
		return customerID;
	}
	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getWorkContact() {
		return workContact;
	}
	public void setWorkContact(String workContact) {
		this.workContact = workContact;
	}
	public String getHomeContact() {
		return homeContact;
	}
	public void setHomeContact(String homeContact) {
		this.homeContact = homeContact;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
	
}
