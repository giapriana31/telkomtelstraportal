package com.tt.profileSetting.mvc.web.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.SessionFactory;
import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.portlet.util.PortletUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.tt.client.model.createServiceRequestapi.InsertResponse;
import com.tt.client.service.incident.createServiceRequestWSCallService;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;
import com.tt.model.Incident;
import com.tt.profileSetting.mvc.dao.ProfileSecurityDao;
import com.tt.profileSetting.mvc.model.SecurityQuestion;
import com.tt.profileSetting.mvc.model.UserProfile;
import com.tt.profileSetting.mvc.service.ProfileSecurityManager;
import com.tt.servicerequestlist.mvc.model.CreateSRForm;
import com.tt.servicerequestlist.mvc.service.common.CreateSRManager;
import com.tt.servicerequestlist.mvc.service.common.ServiceRequestService;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.utils.TTGenericUtils;

@Controller("profileSecuritySetting")
@RequestMapping("VIEW")
public class ProfileSecurityController
{

	private ProfileSecurityManager profileSecurityManager;
	private ProfileSecurityDao profileSecurityDao;
	private SessionFactory sessionFactory;

	@Autowired
	@Qualifier("profileSecurityManager")
	public void setProfileSecurityManager(ProfileSecurityManager profileSecurityManager)
	{
		this.profileSecurityManager = profileSecurityManager;
	}

	@Autowired
	@Qualifier("profileSecurityDao")
	public void setProfileSecurityDao(ProfileSecurityDao profileSecurityDao)
	{
		this.profileSecurityDao = profileSecurityDao;
	}

	Properties prop = PropertyReader.getProperties();
	String statusNew = prop.getProperty("statusNew");
	String statusAssigned = prop.getProperty("statusAssigned");
	String statusPending = prop.getProperty("statusPending");
	String statusInProgress = prop.getProperty("statusInProgress");
	String statusClosed = prop.getProperty("statusClosed");
	String statusCancelled = prop.getProperty("statusCancelled");
	String statusComplete = prop.getProperty("statusComplete");
	String statusCompleted = prop.getProperty("statusCompleted");
	String statusClosedCancelled = prop.getProperty("statusClosedCancelled");
	String changeTypeRoutine = prop.getProperty("changeTypeRoutine");
	String changeTypeComprehensive = prop.getProperty("changeTypeComprehensive");
	String changeTypeEmergency = prop.getProperty("changeTypeEmergency");
	String changeSourceInternal = prop.getProperty("changeSourceInternal");
	String changeSourceExternal = prop.getProperty("changeSourceExternal");
	String changeSourceCustomer = prop.getProperty("changeSourceCustomer");

	private ServiceRequestService serviceRequestService;

	private CreateSRManager createSRManager;

	@Autowired
	@Qualifier("createSRManager")
	public void setCreateSRManager(CreateSRManager createSRManager)
	{
		this.createSRManager = createSRManager;
	}

	@Autowired
	@Qualifier("serviceRequestService")
	public void setServiceRequestService(ServiceRequestService serviceRequestService)
	{
		this.serviceRequestService = serviceRequestService;
	}

	public static final int LOCALE_PREPEND_FRIENDLY_URL_STYLE = GetterUtil.getInteger(PropsUtil
			.get("locale.prepend.friendly.url.style"));

	private final static Logger log = Logger.getLogger(ProfileSecurityController.class);

	@RenderMapping
	public String render(RenderRequest request, RenderResponse response, Model model) throws TTPortalAppException,
			PortletException, IOException
	{
		BufferedReader in = null;
		HttpsURLConnection con = null;
		HttpServletRequest request1 = PortalUtil.getHttpServletRequest(request);
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		// String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),
		// PortalUtil.getHttpServletResponse(response));
		User user =  TTGenericUtils.getLoggedInUser(httpServletRequest);;
		String id = null;
		boolean NOCManager = false;
		boolean customerPortalAdmin = false;
		boolean endCustomerAdmin = false;
		
		try
		{
			String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),
					PortalUtil.getHttpServletResponse(response));
			
			System.out.println("customerId" + customerId);
			List<Role> roles = user.getRoles();
			for(int i = 0;i<roles.size();i++){
				String role = roles.get(i).getName().toString();
				System.out.println("Roles::::" +role);
				//String loggedInuserCustId = user.getExpandoBridge().getAttribute("CustomerUniqueID").toString();
				System.out.println("customerId" +customerId);
				System.out.println("customer id in property file::" +prop.getProperty("ttLabCustId"));
				/*model.addAttribute("customerId", customerId);*/
				
				if(role.equalsIgnoreCase("NOCManager") && customerId.equalsIgnoreCase(prop.getProperty("ttLabCustId"))){
					NOCManager = true;
					model.addAttribute("NOCManager", NOCManager);
					
				}
				else{
					model.addAttribute("NOCManager", NOCManager);
								
				}
				
				if(role.equalsIgnoreCase("EndCustomerAdmin")){
					endCustomerAdmin = true;
					model.addAttribute("endCustomerAdmin", endCustomerAdmin);
					
				}
				else{
					
					model.addAttribute("endCustomerAdmin", endCustomerAdmin);
					
				}
				
				if(role.equalsIgnoreCase("CustomerPortalAdmin")){
					customerPortalAdmin = true;
					model.addAttribute("customerPortalAdmin", customerPortalAdmin);
					
				}
				
				else{
					
					model.addAttribute("customerPortalAdmin", customerPortalAdmin);
		
					
				}
			}
			int numberOfOktaUsers = profileSecurityDao.getNumOfOktaUsers(customerId);
			System.out.println("numberOfOktaUsers" + numberOfOktaUsers);
			model.addAttribute("numberOfOktaUsers", numberOfOktaUsers);
			if (customerId != null)
			{
				System.out.println("Rendering Service Request Modal for user inside Profile Security Controller: "
						+ customerId);
				/*
				 * LinkedHashMap<String, String> regionList = createSRManager.getRegionNameMap(customerId);
				 * LinkedHashMap<String, String> regionListActual = new LinkedHashMap<String, String>();
				 * regionListActual.put("0", "--Select--"); for( Map.Entry<String,String> entry :
				 * regionList.entrySet()){ regionListActual.put(entry.getKey(), entry.getValue()); }
				 * model.addAttribute("regionList", regionListActual);
				 */

				String subCategory = profileSecurityManager.getSubCategory();
				model.addAttribute("subCategory", subCategory);

				model.addAttribute("logSRFormModel", new CreateSRForm());
			}
			else
			{
				System.out
						.println("Rendering without any user details  inside Profile Security Controller:- CustomerUniqueID not found.");
				model.addAttribute("logSRFormModel", new CreateSRForm());
			}

			//user = TTGenericUtils.getLoggedInUser(httpServletRequest);

			log.info("ProfileSecurityController.java-render()-Inside Render-Start");
			RefreshUtil.setProxyObject();

			JSONParser parser = new JSONParser();

			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

			String email = user.getDisplayEmailAddress();

			id = user.getExpandoBridge().getAttribute("OKTAUserId").toString();

			URL userProfileurl = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + email);
			con = RefreshUtil.getHttpURLConnection(userProfileurl);
			log.info("ProfileSecurityController.java-render()-Inside Render is connection null: " + con);

			con.setRequestProperty("Authorization", prop.getProperty("Token"));
			con.connect();
			con.getResponseCode();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String userDetailList;
			String userDetailList2 = "";
			while ((userDetailList = in.readLine()) != null)
			{

				userDetailList2 = userDetailList;
			}
			JSONObject userObj = new JSONObject(userDetailList2);
			UserProfile userProfile = new UserProfile();
			if (null == id || id.equals(""))
			{
				log.info("ProfileSecurityController.java-render() inside id null if okta id= " + id);
				id = userObj.getString("id");
				log.info("ProfileSecurityController.java-render() inside ");
				user.getExpandoBridge().setAttribute("OKTAUserId", id);
				UserLocalServiceUtil.updateUser(user);
			}

			JSONObject profileJsonObj = (JSONObject) userObj.getJSONObject("profile");

			String profileJsonString = profileJsonObj.toString();

			if (profileJsonString.contains("firstName"))
			{

				userProfile.setFirstName(profileJsonObj.getString("firstName"));
			}
			else
			{

				userProfile.setFirstName("");

			}
			if (profileJsonString.contains("lastName"))
			{

				userProfile.setLastName(profileJsonObj.getString("lastName"));
			}
			else
			{

				userProfile.setLastName("");

			}
			if (profileJsonString.contains("customerid"))
			{

				userProfile.setCustomerID(profileJsonObj.getString("customerid"));
			}
			else
			{

				userProfile.setCustomerID("");

			}
			if (profileJsonString.contains("city"))
			{

				userProfile.setLocation(profileJsonObj.getString("city"));
			}
			else
			{

				userProfile.setLocation("");

			}
			if (profileJsonString.contains("secondEmail"))
			{

				userProfile.setEmail(profileJsonObj.getString("secondEmail"));
			}
			else
			{

				userProfile.setEmail("");

			}
			if (profileJsonString.contains("userMobilePhone"))
			{

				userProfile.setHomeContact(profileJsonObj.getString("userMobilePhone"));
			}
			else
			{

				userProfile.setHomeContact("");

			}
			if (profileJsonString.contains("primaryPhone"))
			{

				userProfile.setWorkContact(profileJsonObj.getString("primaryPhone"));
			}
			else
			{

				userProfile.setWorkContact("");

			}

			String customerNameFromDb = profileSecurityManager.getCustomerName(userProfile.getCustomerID());
			request.setAttribute("customerNameFromDb", customerNameFromDb);
			userProfile.setCustomerName(customerNameFromDb);
			request.setAttribute("userDetails", userProfile);

			log.info("ProfileSecurityController.java-render()-Profile Json  fname: " + userProfile.getFirstName());
			log.info("ProfileSecurityController.java-render()-Profile Json customerid: " + userProfile.getCustomerID());
			log.info("ProfileSecurityController.java-render()-Profile Json second email: " + userProfile.getEmail());
			log.info("ProfileSecurityController.java-render()-Profile Json location:  " + userProfile.getLocation());
			log.info("ProfileSecurityController.java-render()-Profile Json lastname:  " + userProfile.getLastName());

			URL url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + id + "/factors/questions");

			con = RefreshUtil.getHttpURLConnection(url);

			con.setRequestProperty("Authorization", prop.getProperty("Token"));

			con.connect();
			con.getResponseCode();
			log.info("ProfileSecurityController.java-render()-Response Code Okta For fetching Security Controller "
					+ con.getResponseCode());

			in = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String jsonList;
			String jsonList2 = "";

			while ((jsonList = in.readLine()) != null)
			{

				jsonList2 = jsonList;
			}

			log.debug("ProfileSecurityController.java-render()-SecurityQuestion List Okta" + jsonList2 == null ? "NULL"
					: jsonList2.toString());

			Gson gson = new Gson();

			List<SecurityQuestion> classOfT = new ArrayList<SecurityQuestion>();
			Type listType = new TypeToken<ArrayList<SecurityQuestion>>()
			{
			}.getType();

			classOfT = gson.fromJson(jsonList2, listType);

			request1.setAttribute("securityQuestion", classOfT);

			if (request1.getAttribute("renderQuestionDiv") == null)
			{
				request1.setAttribute("renderQuestionDiv", "false");
			}
			if (request1.getAttribute("renderPasswordDiv") == null)
			{
				request1.setAttribute("renderPasswordDiv", "false");
			}

			// Getting site name list
			Map<String, String> siteNameMap = createSRManager.getSiteNameMap(customerId);
			log.info("SiteNameList " + siteNameMap);
			if (siteNameMap != null && !(siteNameMap.isEmpty()))
			{
				log.info("Null Handled");
				Map<String, String> siteNameMapActual = new LinkedHashMap<String, String>();
				siteNameMapActual.put("allSites", "All Sites");
				for (Map.Entry<String, String> entry : siteNameMap.entrySet())
				{
					siteNameMapActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("sitenamelist", siteNameMapActual);
			}
			else
			{
				Map<String, String> siteNameListActual = new LinkedHashMap<String, String>();
				siteNameListActual.put("allSites", "All Sites");
				model.addAttribute("sitenamelist", siteNameListActual);
			}

			// Populating changeType
			LinkedHashMap<String, String> changeTypeMap = new LinkedHashMap<String, String>();
			changeTypeMap.put("allTypes", "All Types");
			changeTypeMap.put(changeTypeRoutine, changeTypeRoutine);
			changeTypeMap.put(changeTypeComprehensive, changeTypeComprehensive);
			changeTypeMap.put(changeTypeEmergency, changeTypeEmergency);
			model.addAttribute("changetypelist", changeTypeMap);

			// Populating change source
			LinkedHashMap<String, String> changeSourceMap = new LinkedHashMap<String, String>();
			changeSourceMap.put("allTypes", "All Types");
			changeSourceMap.put(changeSourceInternal, changeSourceInternal);
			changeSourceMap.put(changeSourceExternal, changeSourceExternal);
			changeSourceMap.put(changeSourceCustomer, changeSourceCustomer);
			model.addAttribute("changesourcelist", changeSourceMap);
			String tabStatusCheck = "activeTab";
			if (PortletUtils.getSessionAttribute(request, "tabStatusSelected") != null)
			{
				tabStatusCheck = (String) PortletUtils.getSessionAttribute(request, "tabStatusSelected");
			}

			model.addAttribute("tabStatusCheck", tabStatusCheck);
			
			

		}
		catch (Exception e)
		{
			log.error("ProfileSecurityController.java-render()-Catch " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			// in.close();
			con.disconnect();

		}
		log.info("ProfileSecurityController.java-render()-Inside Render-End");

		return "profileSecuritySetting";
	}

	// Change Password Starts Here

	@ActionMapping(value = "actionMethod1")
	public void actionMethod1(ActionRequest request, ActionResponse response) throws IOException, PortletException
	{
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		User user = null;
		HttpsURLConnection con = null;
		BufferedReader in = null;

		try
		{
			// user =
			// UserLocalServiceUtil.getUser((Long)httpServletRequest.getSession().getAttribute(WebKeys.USER_ID));
			user = TTGenericUtils.getLoggedInUser(httpServletRequest);

			log.info("ProfileSecurityController.java-actionMethod1()-Inside action1-Change Password-Start");
			RefreshUtil.setProxyObject();
			HttpServletRequest request1 = PortalUtil.getHttpServletRequest(request);
			HttpSession session = request1.getSession();
			ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			String securityQuestion;
			String responseMessage = "";

			String email = user.getDisplayEmailAddress();
			// String email = "TestUser1@TestUser.com";
			log.info("ProfileSecurityController.java-actionMethod1()-Inside action1-Email: " + email);

			String customQuestion = ParamUtil.get(request, "customSecurityQuestion", "");
			String dropSecurityQuestion = ParamUtil.get(request, "securityQuestion", "");
			if (customQuestion != null)
			{
				securityQuestion = customQuestion;
			}
			else
			{
				securityQuestion = dropSecurityQuestion;

			}
			String answer = ParamUtil.get(request, "answer", "");

			String password = ParamUtil.get(request, "password", "");
			String newPassword = ParamUtil.get(request, "newPassword", "");
			String confirmPassword = ParamUtil.get(request, "confirmPassword", "");

			PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
			LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
			SessionMessages.add(request, liferayPortletConfig.getPortletId()
					+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

			// change Password

			String id = user.getExpandoBridge().getAttribute("OKTAUserId").toString();
			if (null == id || id.equals(""))
			{
				URL userProfileurl = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + email);
				con = RefreshUtil.getHttpURLConnection(userProfileurl);
				con.setRequestProperty("Authorization", prop.getProperty("Token"));
				con.connect();
				con.getResponseCode();
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String userDetailList;
				String userDetailList2 = "";
				while ((userDetailList = in.readLine()) != null)
				{

					userDetailList2 = userDetailList;
				}
				JSONObject userObj = new JSONObject(userDetailList2);
				UserProfile userProfile = new UserProfile();
				id = userObj.getString("id");
				user.getExpandoBridge().setAttribute("OKTAUserId", id);
				UserLocalServiceUtil.updateUser(user);
			}

			if (password != "" && password != null && newPassword != "" && newPassword != null && confirmPassword != ""
					&& confirmPassword != null && newPassword.equals(confirmPassword))
			{

				JSONObject obj = new JSONObject();
				JSONObject oldPassword = new JSONObject();
				JSONObject newPasswordd = new JSONObject();
				oldPassword.put("value", password);
				newPasswordd.put("value", newPassword);
				obj.put("oldPassword", oldPassword);
				obj.put("newPassword", newPasswordd);

				URL url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + id + "/credentials/change_password/");

				con = RefreshUtil.getHttpURLConnection(url);
				con.setRequestProperty("Authorization", prop.getProperty("Token"));
				con.setDoOutput(true);
				con.setDoInput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", "application/json");
				con.setRequestProperty("Accept", "application/json");
				con.setRequestMethod("POST");
				con.connect();
				OutputStream os = con.getOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
				obj.write(osw);
				osw.flush();
				osw.close();
				os.close();
				con.getResponseCode();
				InputStream is;
				if (con.getResponseCode() >= 400)
				{
					is = con.getErrorStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
					StringBuilder results = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null)
					{
						results.append(line);
					}
					con.disconnect();
					log.error("ProfileSecurityController.java-actionMethod1()-Change Password- Error-Response from Okta "
							+ results.toString());
					//
				}
				else
				{
					is = con.getInputStream();
					con.disconnect();
				}
				log.info("ProfileSecurityController.java-actionMethod1()-Change Password-Response Code from Okta "
						+ con.getResponseCode() + " Message " + con.getResponseMessage());
				//
				if (con.getResponseCode() == 403)
				{
					SessionErrors.add(request, "passwordError");
				}
				else if (con.getResponseCode() == 200)
				{
					SessionMessages.add(request, "passwordChangeSuccess");
				}

				else
				{
					SessionErrors.add(request, "contactAdmin");
				}

			}
			request1.setAttribute("renderQuestionDiv", "false");
			request1.setAttribute("renderPasswordDiv", "true");
			response.setRenderParameter("tab", "Security Settings");
		}
		catch (Exception e)
		{
			log.error("ProfileSecurityController.java-action1()-Catch " + e.getMessage());
			e.printStackTrace();
		}
		log.info("ProfileSecurityController.java-actionMethod1()-Inside action1-Change Password-End");
	}

	// Change Security Question and Answer Starts here

	@ActionMapping(value = "actionMethod3")
	public void actionMethod3(ActionRequest request, ActionResponse response) throws IOException, PortletException
	{
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		User user = null;
		HttpsURLConnection con = null;
		BufferedReader in = null;
		try
		{
			log.info("ProfileSecurityController.java-actionMethod3()-Inside action3-Change Security Question");
			// user =
			// UserLocalServiceUtil.getUser((Long)httpServletRequest.getSession().getAttribute(WebKeys.USER_ID));

			user = TTGenericUtils.getLoggedInUser(httpServletRequest);

			RefreshUtil.setProxyObject();

			HttpServletRequest request1 = PortalUtil.getHttpServletRequest(request);
			HttpSession session = request1.getSession();
			ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

			String responseMessage = "";

			String email = user.getDisplayEmailAddress();
			String securityQuestion;
			String customQuestion = ParamUtil.get(request, "customSecurityQuestion", "");
			String dropSecurityQuestion = ParamUtil.get(request, "securityQuestion", "");
			log.info("ProfileSecurityController.java-actionMethod3()-Inside action3-Change Security Question-Selected Question "
					+ dropSecurityQuestion);
			if (dropSecurityQuestion.equals("Add your own custom question"))
			{

				securityQuestion = customQuestion;
				log.info("ProfileSecurityController.java-actionMethod3()-Inside action3-Change Security Question-Custom Question "
						+ customQuestion);
			}
			else
			{
				securityQuestion = dropSecurityQuestion;

			}

			String answer = ParamUtil.get(request, "answer", "");

			String password = ParamUtil.get(request, "password", "");
			String newPassword = ParamUtil.get(request, "newPassword", "");
			String confirmPassword = ParamUtil.get(request, "confirmPassword", "");

			PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
			LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
			SessionMessages.add(request, liferayPortletConfig.getPortletId()
					+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

			String id = user.getExpandoBridge().getAttribute("OKTAUserId").toString();
			if (null == id || id.equals(""))
			{
				URL userProfileurl = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + email);
				con = RefreshUtil.getHttpURLConnection(userProfileurl);
				con.setRequestProperty("Authorization", prop.getProperty("Token"));
				con.connect();
				con.getResponseCode();
				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String userDetailList;
				String userDetailList2 = "";
				while ((userDetailList = in.readLine()) != null)
				{

					userDetailList2 = userDetailList;
				}
				JSONObject userObj = new JSONObject(userDetailList2);
				UserProfile userProfile = new UserProfile();
				id = userObj.getString("id");
				user.getExpandoBridge().setAttribute("OKTAUserId", id);
				UserLocalServiceUtil.updateUser(user);
			}

			// Change Security Question and Answer
			if (answer != null && answer != "")
			{
				JSONObject obj = new JSONObject();
				JSONObject passwordObj = new JSONObject();
				JSONObject recovery_question = new JSONObject();
				passwordObj.put("value", password);
				recovery_question.put("question", securityQuestion);
				recovery_question.put("answer", answer);
				obj.put("password", passwordObj);
				obj.put("recovery_question", recovery_question);

				URL url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + email
						+ "/credentials/change_recovery_question/");

				con = RefreshUtil.getHttpURLConnection(url);

				con.setRequestProperty("Authorization", prop.getProperty("Token"));

				con.setDoOutput(true);
				con.setDoInput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", "application/json");
				con.setRequestProperty("Accept", "application/json");
				con.setRequestMethod("POST");
				con.connect();
				OutputStream os = con.getOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
				obj.write(osw);
				osw.flush();
				osw.close();
				os.close();
				con.getResponseCode();

				InputStream is;
				if (con.getResponseCode() >= 400)
				{
					is = con.getErrorStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
					StringBuilder results = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null)
					{
						results.append(line);
					}
					con.disconnect();
					log.error("ProfileSecurityController.java-actionMethod3()-Change Security Question- Error-Response from Okta "
							+ results.toString());

				}
				else
				{
					is = con.getInputStream();
					con.disconnect();
				}
				log.info("ProfileSecurityController.java-actionMethod1()-Change Security Question-Response Code from Okta "
						+ con.getResponseCode() + " Message " + con.getResponseMessage());
				if (con.getResponseCode() == 403)
				{
					SessionErrors.add(request, "recoveryQuestionError");
				}
				else if (con.getResponseCode() == 200)
				{
					SessionMessages.add(request, "recoveryQuestionChangeSuccess");
				}
				else
				{
					SessionErrors.add(request, "contactAdmin");
				}

			}
			request1.setAttribute("renderQuestionDiv", "true");
			request1.setAttribute("renderPasswordDiv", "false");
			response.setRenderParameter("tab", "Security Settings");
		}
		catch (Exception e)
		{
			log.error("ProfileSecurityController.java-action3()-Change Security Question-Catch " + e.getMessage());
			e.printStackTrace();
		}
	}

	// Change profile Starts here

	@ActionMapping(value = "actionMethod2")
	public void actionMethod2(ActionRequest request, ActionResponse response) throws IOException, PortletException
	{
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		User user = null;
		try
		{

			log.info("ProfileSecurityController.java-actionMethod2()-Inside action2-Change Profile-start ");
			// user =
			// UserLocalServiceUtil.getUser((Long)httpServletRequest.getSession().getAttribute(WebKeys.USER_ID));

			user = TTGenericUtils.getLoggedInUser(httpServletRequest);
			RefreshUtil.setProxyObject();

			PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
			LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
			SessionMessages.add(request, liferayPortletConfig.getPortletId()
					+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

			// Profie
			HttpServletRequest request1 = PortalUtil.getHttpServletRequest(request);
			HttpSession session = request1.getSession();
			ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

			String location = ParamUtil.get(request, "Location", "");
			String workContact = ParamUtil.get(request, "WorkContact", "");
			String homeContact = ParamUtil.get(request, "HomeContact", "");
			String preferredLanguage = ParamUtil.get(request, "lang", "");

			String email = user.getDisplayEmailAddress();

			String secondMail = ParamUtil.get(request, "secondMail", "");
			String companyID = ParamUtil.get(request, "CompanyID", "");
			String customerName = ParamUtil.get(request, "customerName", "");
			String companyIDCustom = ParamUtil.get(request, "companyIDCustom", "");

			Locale locale = LocaleUtil.fromLanguageId(preferredLanguage);
			session.setAttribute("org.apache.struts.action.LOCALE", locale);
			if (workContact != "" && workContact != null && homeContact != "" && homeContact != null && email != ""
					&& email != null)
			{
				JSONObject profile1 = new JSONObject();
				JSONObject obj = new JSONObject();
				obj.put("city", location);
				obj.put("primaryPhone", workContact);
				obj.put("mobilePhone", homeContact);
				obj.put("secondEmail", secondMail);
				obj.put("preferredLanguage", preferredLanguage);
				obj.put("userPreferredLanguage", preferredLanguage);
				obj.put("customerid", companyIDCustom);
				obj.put("userTimezone", (user.getTimeZone().toString().split("\\[id=\"")[1]).split("\",offset")[0]);
				obj.put("userMobilePhone", homeContact);
				profile1.put("profile", obj);

				URL url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + email);

				HttpsURLConnection con = RefreshUtil.getHttpURLConnection(url);

				con.setRequestProperty("Authorization", prop.getProperty("Token"));

				con.setDoOutput(true);
				con.setDoInput(true);
				con.setUseCaches(false);
				con.setRequestProperty("Content-Type", "application/json");
				con.setRequestProperty("Accept", "application/json");
				con.setRequestMethod("POST");
				con.connect();
				OutputStream os = con.getOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
				profile1.write(osw);
				osw.flush();
				osw.close();
				os.close();
				con.getResponseCode();

				InputStream is;
				StringBuilder results = new StringBuilder();
				if (con.getResponseCode() >= 400)
				{
					is = con.getErrorStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));

					String line;
					while ((line = reader.readLine()) != null)
					{
						results.append(line);
					}
					con.disconnect();
					log.error("ProfileSecurityController.java-actionMethod2()-Change Profile- Error-Response from Okta "
							+ results.toString());

				}
				else
				{
					is = con.getInputStream();
					con.disconnect();
				}
				log.info("ProfileSecurityController.java-actionMethod1()-Change Profile-Response Code from Okta "
						+ con.getResponseCode() + " Message " + con.getResponseMessage());
				if (results.toString().contains("secondEmail"))
				{
					SessionErrors.add(request, "secondEmailSameError");
				}
				else if (con.getResponseMessage().equals("OK"))
				{
					SessionMessages.add(request, "profileUpdateSuccess");
					user.getExpandoBridge().setAttribute("WorkContact", workContact, false);
					user.getExpandoBridge().setAttribute("HomeContact", homeContact, false);

					user.getExpandoBridge().setAttribute("SecondaryEmailTemp", secondMail, false);
					user.getExpandoBridge().setAttribute("SecondaryEmail", secondMail, false);

					String id = user.getExpandoBridge().getAttribute("OKTAUserId").toString();
					if (null == id || id.equals(""))
					{

						URL userProfileurl = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + email);
						con = RefreshUtil.getHttpURLConnection(userProfileurl);
						con.setRequestProperty("Authorization", prop.getProperty("Token"));
						con.connect();
						con.getResponseCode();
						BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String userDetailList;
						String userDetailList2 = "";
						while ((userDetailList = in.readLine()) != null)
						{

							userDetailList2 = userDetailList;
						}
						JSONObject userObj = new JSONObject(userDetailList2);
						UserProfile userProfile = new UserProfile();
						id = userObj.getString("id");
						user.getExpandoBridge().setAttribute("OKTAUserId", id);
					}

					// user.getExpandoBridge().setAttribute("CustomerUniqueID",
					// companyIDCustom, false);
					// user.getExpandoBridge().setAttribute("customerName",
					// customerName, false);
					user.setLanguageId(preferredLanguage);
					UserLocalServiceUtil.updateUser(user);

					// language change redirect code
					ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
					String redirect1 = PortalUtil.getLayoutFriendlyURL(themeDisplay.getLayout(), themeDisplay, locale);

					String redirect = ParamUtil.getString(request, "redirect");

					String layoutURL = "";
					String queryString = "";

					int pos = redirect.indexOf("/-/");
					if (pos == -1)
					{

						pos = redirect.indexOf("?");
					}
					if (pos != -1)
					{

						layoutURL = redirect.substring(0, pos);
						queryString = redirect.substring(pos);
					}
					else
					{

						layoutURL = redirect;
					}
					Layout layout = td.getLayout();

					Group group = layout.getGroup();
					if (isGroupFriendlyURL(group, layout, layoutURL, locale))
					{
						if (LOCALE_PREPEND_FRIENDLY_URL_STYLE == 0)
						{

							redirect = layoutURL;
						}
						else
						{

							redirect = PortalUtil.getGroupFriendlyURL(group, layout.isPrivateLayout(), td, locale);

						}
					}
					else if (LOCALE_PREPEND_FRIENDLY_URL_STYLE == 0)
					{

						if (td.isI18n())
						{

							redirect = layout.getFriendlyURL(locale);

						}
						else
						{

							redirect = PortalUtil.getLayoutFriendlyURL(layout, td, locale);

						}
					}
					else
					{
						redirect = PortalUtil.getLayoutFriendlyURL(layout, td, locale);

					}
					int lifecycle = GetterUtil.getInteger(HttpUtil.getParameter(queryString, "p_p_lifecycle", false));
					if (lifecycle == 0)
					{
						redirect = redirect + queryString;

					}

					response.sendRedirect(redirect + prop.getProperty("profilePage"));

				}

				else
				{
					SessionErrors.add(request, "profileUpdateError");
				}
			}

		}
		catch (Exception e)
		{
			log.error("ProfileSecurityController.java-action2()-Change Profile-Catch " + e.getMessage());
			e.printStackTrace();

		}
		log.info("ProfileSecurityController.java-actionMethod2()-Inside action2-Change Profile-End ");
	}

	protected boolean isGroupFriendlyURL(Group group, Layout layout, String layoutURL, Locale locale)
	{
		if (Validator.isNull(layoutURL))
		{
			return true;
		}
		int pos = layoutURL.lastIndexOf('/');
		String layoutURLLanguageId = layoutURL.substring(pos + 1);
		Locale layoutURLLocale = LocaleUtil.fromLanguageId(layoutURLLanguageId, true, false);
		if (layoutURLLocale != null)
		{
			return true;
		}
		if (PortalUtil.isGroupFriendlyURL(layoutURL, group.getFriendlyURL(), layout.getFriendlyURL(locale)))
		{
			return true;
		}
		return false;
	}

	@ResourceMapping(value = "userListPopulation")
	public void renderUsersData(ResourceRequest request, ResourceResponse response) throws PortletException,
			IOException, SystemException, JSONException
	{
		log.info("Class: ProfileSecurityController, Method: renderUserData , Start");

		String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),
				PortalUtil.getHttpServletResponse(response));

		PrintWriter out = response.getWriter();
		/*
		 * int numberOfOktaUsers = profileSecurityDao.getNumOfOktaUsers(customerId);
		 * System.out.println("numberOfOktaUsers" +numberOfOktaUsers);
		 */

		List<User> userList = getUserListFromLiferayDatabase(customerId);

		com.liferay.portal.kernel.json.JSONArray customerSpecificUserListJson = JSONFactoryUtil.createJSONArray();
		com.liferay.portal.kernel.json.JSONArray userListJson;

		for (User customerUser : userList)
		{
			userListJson = JSONFactoryUtil.createJSONArray();
			/* Status = 0 - Active, Status = 5 - Inactive */
			userListJson.put(customerUser.getStatus() == 0 ? "true" : "");

			userListJson.put(customerUser.getStatus() == 0 ? "Active" : "Inactive");

			String firstName = customerUser.getFirstName();
			userListJson
					.put((firstName == null || "".equalsIgnoreCase(firstName) || "null".equalsIgnoreCase(firstName)) ? ""
							: firstName);

			String lastName = customerUser.getLastName();
			userListJson
					.put((lastName == null || "".equalsIgnoreCase(lastName) || "null".equalsIgnoreCase(lastName)) ? ""
							: lastName);

			String workContact = (String) customerUser.getExpandoBridge().getAttribute("WorkContact");
			userListJson.put((workContact == null || "".equalsIgnoreCase(workContact) || "null"
					.equalsIgnoreCase(workContact)) ? "" : workContact);

			log.debug("HOME Contact :: " + customerUser.getExpandoBridge().getAttribute("HomeContact")
					+ " home contact is null :: "
					+ String.valueOf((customerUser.getExpandoBridge().getAttribute("HomeContact") == null)));

			String homeContact = (String) customerUser.getExpandoBridge().getAttribute("HomeContact");
			userListJson.put((homeContact == null || "".equalsIgnoreCase(homeContact) || "null"
					.equalsIgnoreCase(homeContact)) ? "" : homeContact);

			String emailAddress = customerUser.getDisplayEmailAddress();
			userListJson.put((emailAddress == null || "".equalsIgnoreCase(emailAddress) || "null"
					.equalsIgnoreCase(emailAddress)) ? "" : emailAddress);

			String oktaUserId = (String) customerUser.getExpandoBridge().getAttribute("OKTAUserId");
			userListJson.put((oktaUserId == null || "".equalsIgnoreCase(oktaUserId) || "null"
					.equalsIgnoreCase(oktaUserId)) ? "" : oktaUserId);

			customerSpecificUserListJson.put(userListJson);

		}

		log.debug("Final JSON :: " + customerSpecificUserListJson);

		out.print(customerSpecificUserListJson.toString());
		out.close();
		log.info("Class: ProfileSecurityController, Method: renderUserData , End");
	}

	@ResourceMapping(value = "disableUser")
	public void disableUsers(ResourceRequest request, ResourceResponse response) throws IOException, JSONException,
			SystemException
	{
		log.info("Class: ProfileSecurityController, Method: disableUsers , Start");
		URL url = null;
		HttpsURLConnection con = null;
		PrintWriter out = response.getWriter();
		String userList = request.getParameter("userList");
		String userOktaList = request.getParameter("userOktaIdArray");
		com.liferay.portal.kernel.json.JSONObject responseJson = JSONFactoryUtil.createJSONObject();

		log.debug("List of emails of selected users :: " + userList);

		String[] userArrayList = userList.split(",");
		String[] userOktaArrayList = null;

		if (userOktaList != null)
		{
			userOktaArrayList = userOktaList.split(",");
		}

		int i = -1;
		if (userArrayList != null)
		{
			for (String userEmail : userArrayList)
			{
				i++;

				if ("".equalsIgnoreCase(userEmail))
					continue;
				else
				{
					log.debug("email :: " + userEmail);

					if (userOktaArrayList == null || userOktaArrayList[i].equalsIgnoreCase(""))
					{
						URL userProfileurl = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + userEmail);

						con = RefreshUtil.getHttpURLConnection(userProfileurl);

						con.setRequestProperty("Authorization", prop.getProperty("Token"));
						con.setRequestProperty("Content-Type", "application/json");
						con.setRequestProperty("Accept", "application/json");
						con.setRequestMethod("GET");
						con.connect();
						con.getResponseCode();
						log.debug("response code on sending email address :: " + con.getResponseCode());

						BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

						StringBuilder userDetailsBuilder = new StringBuilder();
						String userDetails = "";

						while ((userDetails = in.readLine()) != null)
						{
							userDetailsBuilder.append(userDetails);
						}

						JSONObject userObj = new JSONObject(userDetailsBuilder.toString());

						String id = userObj.getString("id");
						log.debug("OKTA internal Id of user : " + id);

						url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + id + "/lifecycle/deactivate");
					}
					else
					{
						url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + userOktaArrayList[i]
								+ "/lifecycle/deactivate");
					}

					log.debug("URL is :: " + url.toString());

					con = RefreshUtil.getHttpURLConnection(url);
					con.setRequestProperty("Authorization", prop.getProperty("Token"));
					con.setRequestProperty("Content-Type", "application/json");
					con.setRequestProperty("Accept", "application/json");
					con.setRequestMethod("POST");
					con.connect();

					log.debug("response code from OKTA :: " + con.getResponseCode());

					if (con.getResponseCode() == 200)
					{
						responseJson.put("name", userEmail);
						responseJson.put("status", "success");
					}
					else
					{
						responseJson.put("name", userEmail);
						responseJson.put("status", "failure");
					}
					con.disconnect();
				}
			}
		}

		log.debug("Response JSON :: " + responseJson);
		out.print(responseJson);
		out.close();
		log.info("Class: ProfileSecurityController, Method: disableUsers , End");
	}

	// Created by Niranjan
	@ResourceMapping(value = "enableUser")
	public void enableUsers(ResourceRequest request, ResourceResponse response) throws IOException, JSONException,
			SystemException
	{

		log.info("Class: ProfileSecurityController, Method: enableUsers");
		URL url = null;
		HttpsURLConnection con = null;
		PrintWriter out = response.getWriter();
		String userOktaList = request.getParameter("userOktaIdArray");
		System.out.println("userOktaList" + userOktaList);
		com.liferay.portal.kernel.json.JSONObject responseJson = JSONFactoryUtil.createJSONObject();

		if (userOktaList != null)
		{
			String[] userOktaArrayList = userOktaList.split(",");
			for (int i = 0; i < userOktaArrayList.length; i++)
			{
				System.out.println("Okta Id" + userOktaArrayList[i]);
				if (userOktaArrayList[i] != "")
				{
					url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + userOktaArrayList[i]
							+ "/lifecycle/activate?sendEmail=true");

					log.debug("URL created" + url.toString());
					con = RefreshUtil.getHttpURLConnection(url);
					con.setRequestProperty("Authorization", prop.getProperty("Token"));
					con.setRequestProperty("Content-Type", "application/json");
					con.setRequestProperty("Accept", "application/json");
					con.setRequestMethod("POST");
					con.connect();
					con.getResponseCode();
					log.debug("Response code" + con.getResponseCode() + userOktaArrayList[i]);
					System.out.println("Response code" + con.getResponseCode());
					// JsonArray list = new JsonArray();
					if (con.getResponseCode() == 200)
					{
						responseJson.put("status", "Success");

					}
					else
					{

						responseJson.put("status", "Failure");
					}
					con.disconnect();
				}

			}
		}

		log.debug("Response JSON :: " + responseJson);
		out.print(responseJson);
		out.close();
	}

	@ResourceMapping(value = "addUser")
	public void addUser(ResourceRequest request, ResourceResponse response) throws IOException, JSONException,
			SystemException
	{
		String oktaId = "";
		String userId = "";
		String roleOktaID = "";
		List<String> roleOktaIdArr = new ArrayList<String>();

		BufferedReader reader = null;
		HttpServletRequest uploadRequest = PortalUtil.getHttpServletRequest(request);

		log.info("Class: ProfileSecurityController, Method: addUsers");
		URL url = null;
		HttpsURLConnection con = null;
		PrintWriter out = response.getWriter();
		com.liferay.portal.kernel.json.JSONObject responseJson = JSONFactoryUtil.createJSONObject();

		System.out.println("First name" + uploadRequest.getParameter("firstname"));
		String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),
				PortalUtil.getHttpServletResponse(response));
		JSONObject addUserTemp = new JSONObject();
		addUserTemp.put("firstName", uploadRequest.getParameter("firstname"));
		addUserTemp.put("lastName", uploadRequest.getParameter("lastname"));
		addUserTemp.put("email", uploadRequest.getParameter("username"));
		addUserTemp.put("login", uploadRequest.getParameter("username"));
		addUserTemp.put("customerid", customerId);// "TT001 ");
		addUserTemp.put("userPreferredLanguage", uploadRequest.getParameter("language"));
		addUserTemp.put("userTimezone", "Asia/Jakarta");
		addUserTemp.put("userMobilePhone", uploadRequest.getParameter("mobilenumber"));

		JSONObject addUser = new JSONObject();
		addUser.put("profile", addUserTemp);

		System.out.println("Json1" + addUserTemp.toString());
		System.out.println("Json2" + addUser.toString());
		if (addUser != null)
		{

			url = new URL(prop.getProperty("oktaURL") + "/api/v1/users?activate=true");
			con = RefreshUtil.getHttpURLConnection(url);

			con.setRequestProperty("Authorization", prop.getProperty("Token"));
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("POST");
			con.setDoOutput(true);

			con.connect();

			OutputStream os = con.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			addUser.write(osw);
			osw.flush();
			osw.close();
			os.close();

			// read the JSON and retrieve the okta id of the created user
			try
			{
				reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}
			catch (Exception e)
			{
				String str = "";
				String temp = "";
				reader = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((temp = reader.readLine()) != null)
				{
					str += temp;
				}
				System.out.println(str + "\n------------------------------\n" + e);
			}
			String addUserDataTemp;
			String addUserData = "";
			while ((addUserDataTemp = reader.readLine()) != null)
			{
				addUserData = addUserDataTemp;
			}
			JSONObject userObj = new JSONObject(addUserData);

			// put okta id in a variable for later use
			userId = userObj.getString("id");

			System.out.println("Response code:" + con.getResponseCode());

			System.out.println("userId::" + userId);

			if (con.getResponseCode() == 200)
			{
				responseJson.put("status", uploadRequest.getParameter("username"));

			}
			else
			{

				responseJson.put("status", "User creation Failed");
			}

		}
		con.disconnect();

		// Get the OKTA id of the logged in user
		User user = TTGenericUtils.getLoggedInUser(uploadRequest);

		oktaId = (String) user.getExpandoBridge().getAttribute("OKTAUserId"); // "00u5iipackqfA6xZB0h7" ;
		System.out.println("Login id::" + oktaId);

		// Make a call to OKTA API and get the cgrp group id
		url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/" + oktaId + "/groups");
		con = RefreshUtil.getHttpURLConnection(url);

		con.setRequestProperty("Authorization", prop.getProperty("Token"));
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestMethod("GET");
		con.connect();

		System.out.println("Response code 1:" + con.getResponseCode());
		// Read the JSON and get id of role whose name contains cgrp
		BufferedReader readRoles = new BufferedReader((new InputStreamReader(con.getInputStream())));
		String dataTemp;
		String data = "";
		while ((dataTemp = readRoles.readLine()) != null)
		{
			data = dataTemp;
		}

		JSONArray roles = new JSONArray(data);
		System.out.println("JSONArray" + roles.toString());
		for (int i = 0; i < roles.length(); i++)
		{
			JSONObject obj = roles.getJSONObject(i);
			JSONObject profile = obj.getJSONObject("profile");
			System.out.println("profile::" + profile.toString());
			if (profile.getString("name").contains("cgrp"))
			{
				roleOktaID = obj.getString("id");
				System.out.println("roleOktaID::" + roleOktaID);
				roleOktaIdArr.add(roleOktaID);
			}
		}
		con.disconnect();

		// Code to assign user groups

		String portalRoles[] = uploadRequest.getParameterValues("customer");
		for (int i = 0; i < portalRoles.length; i++)
		{
			System.out.println("Portal Roles::" + portalRoles[i]);
			String portalRolesOktaId = prop.getProperty(portalRoles[i]);
			roleOktaIdArr.add(portalRolesOktaId);

		}

		for (int i = 0; i < roleOktaIdArr.size(); i++)
		{
			System.out.println("roleOktaIdArr::" + roleOktaIdArr.get(i));
			String oktaIdTemp = roleOktaIdArr.get(i);
			url = new URL(prop.getProperty("oktaURL") + "/api/v1/groups/" + oktaIdTemp + "/users/" + userId);
			con = RefreshUtil.getHttpURLConnection(url);

			con.setRequestProperty("Authorization", prop.getProperty("Token"));
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
			con.connect();

			System.out.println("Response code 2:" + con.getResponseCode());

			con.disconnect();
		}

		out.print(responseJson);
		out.close();
	}

	@ResourceMapping(value = "addLicense")
	public void acquireLicense(ResourceRequest request, ResourceResponse response) throws IOException, JSONException,
			SystemException
	{

		HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(request);
		System.out.println("inside Acquire license");
		UploadPortletRequest uRequest = PortalUtil.getUploadPortletRequest(request);
		PrintWriter writer = response.getWriter();
		Incident incident = new Incident();
		com.liferay.portal.kernel.json.JSONObject json = JSONFactoryUtil.createJSONObject();

		String regionListActual = "";
		String site = "";

		try
		{
			User user = UserLocalServiceUtil.getUser((Long) httprequest.getSession().getAttribute(WebKeys.USER_ID));
			String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),
					PortalUtil.getHttpServletResponse(response));
			System.out.println("customerId" + customerId);
			RefreshUtil.setProxyObject();
			createServiceRequestWSCallService callService = new createServiceRequestWSCallService();
			System.out.println("uRequest summary" + uRequest.getParameter("summary"));
			incident.setSummary(uRequest.getParameter("summary"));

			LinkedHashMap<String, String> regionList = createSRManager.getRegionNameMap(customerId);

			Map.Entry<String, String> entry = regionList.entrySet().iterator().next();
			// String key= entry.getKey();
			regionListActual = entry.getValue().toString();
			System.out.println("regionListActual" + regionListActual);
			incident.setRegion(regionListActual);

			LinkedHashMap<String, String> siteIDListtemp = createSRManager.getSiteFromRegion(regionListActual,
					customerId);
			Map.Entry<String, String> siteEntry = siteIDListtemp.entrySet().iterator().next();
			// String key= entry.getKey();
			site = siteEntry.getValue().toString();
			System.out.println("site" + site);
			incident.setSiteid(site);

			String serviceName = createSRManager.getServiceNameforLicenses(customerId);
			System.out.println("serviceName" + serviceName);
			incident.setServiceid(serviceName);

			incident.setCategory("Other");
			incident.setUserid(user.getEmailAddress() + "");
			System.out.println("user is ::" + user.getEmailAddress() + "");
			incident.setImpact(prop.getProperty("defaultSRImpact"));
			incident.setCustomerpreferedlanguage((String) user.getExpandoBridge().getAttribute("preferredLanguage"));
			incident.setSubcategory("Acquiring additional user Licenses");
			incident.setTicketsource(prop.getProperty("RequestSource"));
			incident.setTickettype(prop.getProperty("ServiceRequestTicketType"));
			incident.setUrgency(prop.getProperty("defaultSRUrgency"));
			incident.setWorklognotes(uRequest.getParameter("description"));

			System.out.println("Data sent to SNOW - " + incident.getWorklognotes());
			InsertResponse insert = new InsertResponse();
			boolean isException = false;
			try
			{
				// System.out.println("Inside try");
				insert = callService.insertIncidentWebService(incident);
			}
			catch (Exception ce)
			{
				// System.out.println("Catch mai aya hai"+ce.getMessage());
				isException = true;
			}
			if (!isException)
			{
				/*
				 * System.out.println("End of call"+insert.getDisplayName()+"::"+insert.getDisplayValue()+""+insert.
				 * getErrorMessage());
				 * 
				 * System.out.println("Success Response: " + insert.getDisplayValue());
				 * System.out.println("Error Response: " + insert.getErrorMessage());
				 */
				if (insert.getDisplayValue() != null && insert.getDisplayValue().trim().length() != 0)
				{
					json.put("requestID", "Success");
				}
				else
				{
					json.put("requestID", insert.getErrorMessage());
				}
				json.put("incidentID", insert.getDisplayValue());

				writer.print(json.toString());
				// System.out.println("JSON:::" +json.toString());

			}
			else
			{
				log.error("Connection timed out");
				// System.out.println("Connection timed out::::::::");
				json.put("requestID", "Error@" + "Connection Timed Out. Please try again later.");
				writer.print(json.toString());

			}

		}
		catch (Exception e)
		{
			log.error("Error while submitting Service Request" + e.getMessage());
		}

	}

	@ResourceMapping(value = "editUser")
	public void editUser(ResourceRequest request, ResourceResponse response) throws IOException, JSONException,
			SystemException
	{

		HttpServletRequest editRequest = PortalUtil.getHttpServletRequest(request);
		PrintWriter writer = response.getWriter();
		JSONObject json = new JSONObject();
		String email = editRequest.getParameter("usrname");
		Long companyId = CompanyThreadLocal.getCompanyId();

		try
		{
			User user = UserLocalServiceUtil.getUserByEmailAddress(companyId, email);
			System.out.println("user details:::::::::" + user.getEmailAddress());
			List<Role> userRoles = user.getRoles();
			List<String> roles = new ArrayList<String>();
			for (int i = 0; i < userRoles.size(); i++)
			{
				roles.add(userRoles.get(i).getName());
			}
			json.put("roles", roles);
			writer.print(json.toString());
		}
		catch (PortalException e)
		{
			// TODO Auto-generated catch block
			log.error("Error while submitting Service Request for getting user roles" + e.getMessage());
		}
	}

	@ResourceMapping(value = "updateUser")
	public void updateUser(ResourceRequest request, ResourceResponse response) throws IOException, JSONException,
			SystemException
	{
		HttpServletRequest updateReq = PortalUtil.getHttpServletRequest(request);
		PrintWriter writer = response.getWriter();
		com.liferay.portal.kernel.json.JSONObject responseJson = JSONFactoryUtil.createJSONObject();
		String email = updateReq.getParameter("usrname");
		String updateRolesTemp[] = updateReq.getParameterValues("customer");
		List<String> updateRoles = new ArrayList<String>();
		if(updateRolesTemp != null){
		updateRoles.addAll(Arrays.asList(updateRolesTemp));
		}

		Long companyId = CompanyThreadLocal.getCompanyId();
		try
		{
			User user = UserLocalServiceUtil.getUserByEmailAddress(companyId, email);
			String oktaId = (String) user.getExpandoBridge().getAttribute("OKTAUserId");
			
			
			
			int responseRemoveRoles = 0;
			List<String> roleOktaIds = new ArrayList<String>();
			
			
			
			
			roleOktaIds.add(prop.getProperty("customerbusinessview"));
			roleOktaIds.add(prop.getProperty("customerexecutiveview"));
			roleOktaIds.add(prop.getProperty("customerprojectview"));
			roleOktaIds.add(prop.getProperty("customerview"));
			
			

			List<String> updatedOktaIds = new ArrayList<String>();
			if(updateRoles!=null){
				for (int i = 0; i < updateRoles.size(); i++)
				{
					updatedOktaIds.add(prop.getProperty(updateRoles.get(i).toString()));
					
					roleOktaIds.remove(prop.getProperty(updateRoles.get(i).toString()));
				}
				
				// Code to delete a role:START
				
				if (roleOktaIds != null || !roleOktaIds.isEmpty())
				{
					for (int i = 0; i < roleOktaIds.size(); i++)
					{
						System.out.println("Remove role:::" + roleOktaIds.get(i));
						String remove = roleOktaIds.get(i);
						responseRemoveRoles = removeRolesAPI(remove, oktaId);

					}
					if (responseRemoveRoles == 204)
					{
						responseJson.put("status_deleteroles", "Success");

					}
					else
					{
						responseJson.put("status_deleteroles", "Failure");
					}
				}	
				
				if(roleOktaIds == null || roleOktaIds.isEmpty()){
					responseJson.put("status_deleteroles", "Success");
				}
			}
			
				if(updateRolesTemp == null){
					if (roleOktaIds != null || !roleOktaIds.isEmpty())
					{
						
						System.out.println("check updated okta ids size::::" +updatedOktaIds.size());
						for (int i = 0; i < roleOktaIds.size(); i++)
						{
							
							System.out.println("Remove all UI role:::" + roleOktaIds.get(i));
							String remove = roleOktaIds.get(i);
							responseRemoveRoles = removeRolesAPI(remove, oktaId);

						}
						if (responseRemoveRoles == 204)
						{
							responseJson.put("status_deleteroles", "Success");
							responseJson.put("status_roles", "Success");
							

						}
						else
						{
							responseJson.put("status_deleteroles", "Failure");
						}
					}	
					
				
				}
			
				
			// Code to delete a role:END

			// COde to assign new roles :START
				List<Role> existingRoles =  user.getRoles();
				List <String> temp = new ArrayList<String>();
				temp.addAll(updateRoles);
				List<String> addRolesOktaIds = new ArrayList<String>();
				List<String> rolesAfterFiltering = new ArrayList<String>();
				if(existingRoles!= null){
					for(int i = 0; i<existingRoles.size();i++){
						//System.out.println("Existing Roles::"+existingRoles.get(i).getName().toString());
						for(int j = 0; j<updateRoles.size();j++){
							
							//System.out.println("Updated roles::" +updateRoles.get(j).toString());
							if(existingRoles.get(i).getName().equalsIgnoreCase(updateRoles.get(j).toString())){
								temp.remove(updateRoles.get(j).toString());
								
							}
							
							
						}
						
					}
					for(int i = 0; i<temp.size();i++){
						System.out.println("Temp :::::::::" +temp.get(i).toString());
						rolesAfterFiltering.add(temp.get(i).toString());
					}
					System.out.println("rolesAfterFiltering::::" +rolesAfterFiltering.size());
					
					for(int i =0; i< rolesAfterFiltering.size();i++){
		     		addRolesOktaIds.add(prop.getProperty(rolesAfterFiltering.get(i)).toString());
					}
					
				}

			
		if(addRolesOktaIds.size()!= 0){
			int responseRoles = 0;
			System.out.println("updatedOktaIds size::::::"+addRolesOktaIds.size());
			for (int i = 0; i < addRolesOktaIds.size(); i++)
			{
				
				System.out.println("roleOktaIdArr::" +addRolesOktaIds.get(i));
				String roleOktaId = addRolesOktaIds.get(i);
				responseRoles = addRolesAPI(roleOktaId, oktaId);

			}

			if (responseRoles == 204)
			{
				responseJson.put("status_roles", "Success");

			}
			else
			{
				responseJson.put("status_roles", "Failure");
			}
		}
			// Assigning roles: END

			writer.print(responseJson);
			writer.close();
		}

		catch (PortalException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

@ResourceMapping(value = "getLanguage")
public void getLanguage(ResourceRequest request, ResourceResponse response)throws IOException,
JSONException, SystemException{
	System.out.println("Inside getLanguage");
		HttpServletRequest getLang = PortalUtil.getHttpServletRequest(request);
		PrintWriter writer = response.getWriter();
		JSONObject json = new JSONObject();
		String email = getLang.getParameter("uname");
		Long companyId = CompanyThreadLocal.getCompanyId();
		
		try {
			User user = UserLocalServiceUtil.getUserByEmailAddress(companyId, email);
			String lang = (String) user.getExpandoBridge().getAttribute("Language");
			System.out.println("Language::" +lang);
			json.put("language", lang);
			writer.print(json.toString());
			
		} catch (PortalException e) {
			e.printStackTrace();
		}
		
		
}
	
	
	@ResourceMapping(value = "editMobile")
	public void updateMobileNumber(ResourceRequest request, ResourceResponse response) throws IOException,
			JSONException, SystemException
	{
		HttpServletRequest updateMobile = PortalUtil.getHttpServletRequest(request);
		HttpSession session = updateMobile.getSession();
		PrintWriter writer = response.getWriter();
		com.liferay.portal.kernel.json.JSONObject responseJson = JSONFactoryUtil.createJSONObject();
		String mobileNumber = updateMobile.getParameter("mobilenum");
		String email = updateMobile.getParameter("uname");
		String language = updateMobile.getParameter("editlanguage");
		Long companyId = CompanyThreadLocal.getCompanyId();
		
		
		JSONObject addUserInfoTemp = new JSONObject();
		JSONObject addUserInfo = new JSONObject();

		addUserInfoTemp.put("userPreferredLanguage", language);
		addUserInfoTemp.put("userMobilePhone", mobileNumber);
		addUserInfo.put("profile", addUserInfoTemp);

		System.out.println("Json1" + addUserInfoTemp.toString());
		System.out.println("Json2" + addUserInfo.toString());

		try
		{
			User user = UserLocalServiceUtil.getUserByEmailAddress(companyId, email);
			String oktaId = (String) user.getExpandoBridge().getAttribute("OKTAUserId");
			// Code to update user info :START
			URL url = null;
			HttpsURLConnection con = null;
			//String userId = "00u5iipackqfA6xZB0h7";
			url = new URL(prop.getProperty("oktaURL") + "/api/v1/users/"+oktaId); //+ userId); 
			con = RefreshUtil.getHttpURLConnection(url);

			con.setRequestProperty("Authorization", prop.getProperty("Token"));
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("POST");
			con.setDoOutput(true);
			con.connect();

			OutputStream os = con.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
			addUserInfo.write(osw);
			osw.flush();
			osw.close();
			os.close();
			System.out.println("Response code update:" + con.getResponseCode());

			/*
			 * try { reader = new BufferedReader(new InputStreamReader(con.getInputStream())); } catch(Exception e) {
			 * String str = ""; String temp = ""; reader = new BufferedReader(new
			 * InputStreamReader(con.getErrorStream())); while ((temp = reader.readLine()) != null) { str += temp; }
			 * System.out.println(str+"\n------------------------------\n"+e); }
			 */

			con.disconnect();
			if (con.getResponseCode() == 200)
			{
				responseJson.put("status_mobile", "Success");
			               
				Locale locale = LocaleUtil.fromLanguageId(language);
				
                user.setLanguageId(locale.getLanguage());
                UserLocalServiceUtil.updateUser(user);
                
                user.getExpandoBridge().setAttribute("preferredLanguage", language);
                user.getExpandoBridge().setAttribute("WorkContact", mobileNumber);
               
				

			}
			else
			{
				responseJson.put("status_mobile", "Failure");
			}

			writer.print(responseJson);
			writer.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		// Code to update user info: END
	}

	private List<User> getUserListFromLiferayDatabase(String customerId) throws SystemException
	{
		log.info("Class : ProfileSecurityController, Method: getUserListFromLiferayDatabase, Start");

		List<User> userList = new ArrayList<User>();
		User user;

		String customAttributeName = "CustomerUniqueID";

		long classNameId = ClassNameLocalServiceUtil.getClassNameId(User.class);
		long companyId = PortalUtil.getDefaultCompanyId();

		List<ExpandoValue> requiredExpandoValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId, classNameId,
				ExpandoTableConstants.DEFAULT_TABLE_NAME, customAttributeName, customerId, -1, -1);

		log.debug("expando values :: " + requiredExpandoValues.size());

		for (int i = 0; i < requiredExpandoValues.size(); i++)
		{
			long userId = requiredExpandoValues.get(i).getClassPK();
			try
			{
				user = UserLocalServiceUtil.getUser(userId);
				userList.add(user);
			}
			catch (PortalException e)
			{
				log.error("Error in Class: ProfileSecurityController, Method: getUserListFromLiferayDatabase "
						+ e.getMessage());
			}
		}
		log.info("Class : ProfileSecurityController, Method: getUserListFromLiferayDatabase, End");
		return userList;
	}

	public void RenderLogServiceRequest(@Valid RenderRequest request, RenderResponse renderResponse, Model model)
	{
		try
		{
			log.info("Rendering Service Request Page - Profile Security Controller");

		}
		catch (Exception e)
		{
			log.error("Error while rendering for Log A New Request  inside Profile Security Controller: - "
					+ e.getMessage());
		}
	}

	@ModelAttribute("logSRFormModel")
	@ResourceMapping(value = "renderAcquireLicense")
	public void handleLogServiceRequest(CreateSRForm srForm, Model model, ResourceRequest request,
			ResourceResponse response) throws Exception
	{
		HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpresponse = PortalUtil.getHttpServletResponse(response);
		JSONObject json = new JSONObject();
		try
		{

			User user = UserLocalServiceUtil.getUser((Long) httprequest.getSession().getAttribute(WebKeys.USER_ID));
			RefreshUtil.setProxyObject();
			createServiceRequestWSCallService callService = new createServiceRequestWSCallService();
			Incident incident = new Incident();

			incident.setUserid(user.getEmailAddress() + "");
			incident.setServiceid(srForm.getServiceName());
			incident.setResolvergroupname("");
			incident.setImpact(prop.getProperty("defaultSRImpact"));
			incident.setCustomerpreferedlanguage((String) user.getExpandoBridge().getAttribute("preferredLanguage"));
			incident.setSummary(srForm.getSummary());
			String category;
			try
			{
				category = getCategoryForSubmission(srForm.getCategory(), srForm.getServiceName());
			}
			catch (Exception e)
			{
				category = "";
			}
			incident.setCategory(category);
			incident.setSubcategory(srForm.getSubCategory());
			incident.setTicketsource(prop.getProperty("RequestSource"));
			incident.setTickettype(prop.getProperty("ServiceRequestTicketType"));
			incident.setUrgency(prop.getProperty("defaultSRUrgency"));

			String selectedAction = srForm.getAction();
			if (selectedAction != null && selectedAction.trim().length() != 0 && selectedAction != "0")
			{
				String actualNotes = srForm.getDescription();
				String appendActionText = prop.getProperty("ActionText") + " " + selectedAction + ".";
				log.info("Action Message to be appended - " + appendActionText);
				incident.setWorklognotes(appendActionText + " " + actualNotes);
			}
			else
			{
				log.info("Selected Category -" + srForm.getCategory());
				log.debug("If Category is MACD then the validation has failed as No action has been selected.");
				incident.setWorklognotes(srForm.getDescription());
			}

			log.info("Multiple Service Impacted - " + srForm.isMSI());
			if (srForm.isMSI())
			{
				String actualNotes = incident.getWorklognotes();
				String appendMIM = prop.getProperty("MIMText");
				log.info("MIM Message to be appended - " + appendMIM);
				incident.setWorklognotes(appendMIM + " " + actualNotes);
			}

			log.info("Description being sent to SNOW - " + incident.getWorklognotes());
			InsertResponse insert = new InsertResponse();
			boolean isException = false;
			try
			{
				insert = callService.insertIncidentWebService(incident);
			}
			catch (Exception ce)
			{
				isException = true;
			}
			if (!isException)
			{
				log.info("this is the end of call" + insert.getDisplayName() + "::" + insert.getDisplayValue() + ""
						+ insert.getErrorMessage());

				log.info("Display Response: " + insert.getDisplayValue());
				log.info("Error Response: " + insert.getErrorMessage());
				if (insert.getDisplayValue() != null && insert.getDisplayValue().trim().length() != 0)
				{
					json.put("requestID", "Success@" + insert.getDisplayValue());
				}
				else
				{
					json.put("requestID", "Error@" + insert.getErrorMessage());
				}
				json.put("incidentID", insert.getDisplayValue());
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}
			else
			{
				log.error("Connection timed out");
				json.put("requestID", "Error@" + "Connection Timed Out. Please try again later.");
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}

		}
		catch (Exception e)
		{
			log.error("Error while submitting Service Request" + e.getMessage());
		}
	}

	public String getCategoryForSubmission(String selectedCateogry, String selectedService) throws Exception
	{
		try
		{
			if (selectedCateogry.equalsIgnoreCase("MACD"))
			{
				return createSRManager.fetchCategoryForMACD(selectedService);
			}
			else
			{
				return prop.getProperty("GenericCategory");
			}
		}
		catch (Exception e)
		{
			log.error("Error while fetching Category for MACD to be submitted to SNOW in controller - "
					+ e.getMessage());
			throw e;
		}
		finally
		{

		}
	}

	public int removeRolesAPI(String roleOKTAId, String userOktaId){
		System.out.println("Role okta id remove::" + roleOKTAId);
		System.out.println("User okta id remove::" + userOktaId);
		//String userId = "00u5iipackqfA6xZB0h7";
		int code = 0;
		URL url = null;
		HttpsURLConnection con = null;
		BufferedReader reader = null;
		try
		{
			url = new URL(prop.getProperty("oktaURL") + "/api/v1/groups/" + roleOKTAId + "/users/" + userOktaId); // +userOktaId
			con = RefreshUtil.getHttpURLConnection(url);

			con.setRequestProperty("Authorization", prop.getProperty("Token"));
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("DELETE");
			con.setDoOutput(true);
			con.connect();

			System.out.println("Response code delete roles:" + con.getResponseCode());

			try
			{

				reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}
			catch (Exception e)
			{
				String strD = "";
				String tempD = "";
				reader = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((tempD = reader.readLine()) != null)
				{
					strD += tempD;
				}
				System.out.println(strD + "\n------------------------------\n" + e);
			}
			con.disconnect();
			code = con.getResponseCode();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return code;
	}
	
	public int addRolesAPI(String roleOKTAId, String userOktaId)
	{
		System.out.println("Role okta id::" + roleOKTAId);
		System.out.println("User okta id::" + userOktaId);
		//String userId = "00u5iipackqfA6xZB0h7";
		int code = 0;
		URL url = null;
		HttpsURLConnection con = null;
		BufferedReader reader = null;
		try
		{
			url = new URL(prop.getProperty("oktaURL") + "/api/v1/groups/" + roleOKTAId + "/users/" + userOktaId); // +userOktaId
			con = RefreshUtil.getHttpURLConnection(url);

			con.setRequestProperty("Authorization", prop.getProperty("Token"));
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
			con.connect();

			System.out.println("Response code add roles:" + con.getResponseCode());

			try
			{

				reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			}
			catch (Exception e)
			{
				String str = "";
				String temp = "";
				reader = new BufferedReader(new InputStreamReader(con.getErrorStream()));
				while ((temp = reader.readLine()) != null)
				{
					str += temp;
				}
				System.out.println(str + "\n------------------------------\n" + e);
			}
			con.disconnect();
			code = con.getResponseCode();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return code;
	}

}
