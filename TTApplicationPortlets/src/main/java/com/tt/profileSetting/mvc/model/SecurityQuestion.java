package com.tt.profileSetting.mvc.model;

public class SecurityQuestion {

    private String question;
    private String questionText;

    public String getQuestion() {
	return question;
    }

    public void setQuestion(String question) {
	this.question = question;
    }

    public String getQuestionText() {
	return questionText;
    }

    public void setQuestionText(String questionText) {
	this.questionText = questionText;
    }

    @Override
    public String toString() {
	return "SecurityQuestion [question=" + question + ", questionText="
		+ questionText + "]";
    }

}
