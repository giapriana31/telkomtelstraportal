package com.tt.profileSetting.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.profileSetting.mvc.dao.ProfileSecurityDao;

@Service(value = "profileSecurityManager")
@Transactional
public class ProfileSecurityManager {
	
	private ProfileSecurityDao profileSecurityDao;



	@Autowired
	@Qualifier("profileSecurityDao")
	public void setProfileSecurityDao(ProfileSecurityDao profileSecurityDao) {
		this.profileSecurityDao = profileSecurityDao;
	}
	
	
	
	public String getCustomerName(String customerID){
		
		
		String customerName=profileSecurityDao.getCustomerName(customerID);
		
		return customerName;
	}



	public String getSubCategory()
	{
		String subCategory = profileSecurityDao.getSubCategory();
		return subCategory;
	}
	

}
