package com.tt.profileSetting.mvc.model;

import java.util.ArrayList;
import java.util.List;

public class SecurityQuestionList {

    private List<SecurityQuestion> questions = new ArrayList<SecurityQuestion>();

    public List<SecurityQuestion> getQuestions() {
	return questions;
    }

    public void setQuestions(List<SecurityQuestion> questions) {
	this.questions = questions;
    }

}
