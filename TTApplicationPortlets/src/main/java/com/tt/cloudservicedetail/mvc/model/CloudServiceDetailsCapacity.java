package com.tt.cloudservicedetail.mvc.model;

import java.util.ArrayList;

import org.json.simple.JSONObject;

public class CloudServiceDetailsCapacity 
{
	private Double RemainCPU;
	private Double UsedCPU;
	private Double RemainCPUPer;
	private Double UsedCPUPer;
	
	private Double RemainMemory;
	private Double UsedMemory;
	private Double RemainMemoryPer;
	private Double UsedMemoryPer;
	
	private Double RemainStorage;
	private Double UsedStorage;
	private Double RemainStoragePer;
	private Double UsedStoragePer;
	
	private Double RemainVM;
	private Double UsedVM;
	private Double RemainVMPer;
	private Double UsedVMPer;
	
	ArrayList<JSONObject> LinksArray;
	
	public Double getRemainCPU() {
		return RemainCPU;
	}
	public void setRemainCPU(Double remainCPU) {
		RemainCPU = remainCPU;
	}
	public Double getUsedCPU() {
		return UsedCPU;
	}
	public void setUsedCPU(Double usedCPU) {
		UsedCPU = usedCPU;
	}
	public Double getRemainCPUPer() {
		return RemainCPUPer;
	}
	public void setRemainCPUPer(Double remainCPUPer) {
		RemainCPUPer = remainCPUPer;
	}
	public Double getUsedCPUPer() {
		return UsedCPUPer;
	}
	public void setUsedCPUPer(Double usedCPUPer) {
		UsedCPUPer = usedCPUPer;
	}
	public Double getRemainMemory() {
		return RemainMemory;
	}
	public void setRemainMemory(Double remainMemory) {
		RemainMemory = remainMemory;
	}
	public Double getUsedMemory() {
		return UsedMemory;
	}
	public void setUsedMemory(Double usedMemory) {
		UsedMemory = usedMemory;
	}
	public Double getRemainMemoryPer() {
		return RemainMemoryPer;
	}
	public void setRemainMemoryPer(Double remainMemoryPer) {
		RemainMemoryPer = remainMemoryPer;
	}
	public Double getUsedMemoryPer() {
		return UsedMemoryPer;
	}
	public void setUsedMemoryPer(Double usedMemoryPer) {
		UsedMemoryPer = usedMemoryPer;
	}
	public Double getRemainStorage() {
		return RemainStorage;
	}
	public void setRemainStorage(Double remainStorage) {
		RemainStorage = remainStorage;
	}
	public Double getUsedStorage() {
		return UsedStorage;
	}
	public void setUsedStorage(Double usedStorage) {
		UsedStorage = usedStorage;
	}
	public Double getRemainStoragePer() {
		return RemainStoragePer;
	}
	public void setRemainStoragePer(Double remainStoragePer) {
		RemainStoragePer = remainStoragePer;
	}
	public Double getUsedStoragePer() {
		return UsedStoragePer;
	}
	public void setUsedStoragePer(Double usedStoragePer) {
		UsedStoragePer = usedStoragePer;
	}
	public Double getRemainVM() {
		return RemainVM;
	}
	public void setRemainVM(Double remainVM) {
		RemainVM = remainVM;
	}
	public Double getUsedVM() {
		return UsedVM;
	}
	public void setUsedVM(Double usedVM) {
		UsedVM = usedVM;
	}
	public Double getRemainVMPer() {
		return RemainVMPer;
	}
	public void setRemainVMPer(Double remainVMPer) {
		RemainVMPer = remainVMPer;
	}
	public Double getUsedVMPer() {
		return UsedVMPer;
	}
	public void setUsedVMPer(Double usedVMPer) {
		UsedVMPer = usedVMPer;
	}
	public ArrayList<JSONObject> getLinksArray() {
		return LinksArray;
	}
	public void setLinksArray(ArrayList<JSONObject> linksArray) {
		LinksArray = linksArray;
	}
	
	public void removeNaN()
	{
		if(Double.isNaN(RemainCPU)){RemainCPU=0d;}
		if(Double.isNaN(UsedCPU)){UsedCPU=0d;}
		if(Double.isNaN(RemainCPUPer)){RemainCPUPer=0d;}
		if(Double.isNaN(UsedCPUPer)){UsedCPUPer=0d;}
		
		if(Double.isNaN(RemainMemory)){RemainMemory=0d;}
		if(Double.isNaN(UsedMemory)){UsedMemory=0d;}
		if(Double.isNaN(RemainMemoryPer)){RemainMemoryPer=0d;}
		if(Double.isNaN(UsedMemoryPer)){UsedMemoryPer=0d;}
		
		if(Double.isNaN(RemainStorage)){RemainStorage=0d;}
		if(Double.isNaN(UsedStorage)){UsedStorage=0d;}
		if(Double.isNaN(RemainStoragePer)){RemainStoragePer=0d;}
		if(Double.isNaN(UsedStoragePer)){UsedStoragePer=0d;}
		
		if(Double.isNaN(RemainVM)){RemainVM=0d;}
		if(Double.isNaN(UsedVM)){UsedVM=0d;}
		if(Double.isNaN(RemainVMPer)){RemainVMPer=0d;}
		if(Double.isNaN(UsedVMPer)){UsedVMPer=0d;}
	}
	
	@Override
	public String toString() {
		return "CloudServiceDetailsCapacity [RemainCPU=" + RemainCPU
				+ ", UsedCPU=" + UsedCPU + ", RemainCPUPer=" + RemainCPUPer
				+ ", UsedCPUPer=" + UsedCPUPer + ", RemainMemory="
				+ RemainMemory + ", UsedMemory=" + UsedMemory
				+ ", RemainMemoryPer=" + RemainMemoryPer + ", UsedMemoryPer="
				+ UsedMemoryPer + ", RemainStorage=" + RemainStorage
				+ ", UsedStorage=" + UsedStorage + ", RemainStoragePer="
				+ RemainStoragePer + ", UsedStoragePer=" + UsedStoragePer
				+ ", RemainVM=" + RemainVM + ", UsedVM=" + UsedVM
				+ ", RemainVMPer=" + RemainVMPer + ", UsedVMPer=" + UsedVMPer
				+ ", LinksArray=" + LinksArray + "]";
	}
		
}
