package com.tt.cloudservicedetail.mvc.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.common.RefreshUtil;
import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value = "cloudServiceDetailsDaoImpl")
@Transactional
public class CloudServiceDetailsDaoImpl {

	private final static Logger log = Logger.getLogger(CloudServiceDetailsDaoImpl.class);
	Properties prop = PropertyReader.getProperties();

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public String getVMJSONArray(String CustomerID) 
	{
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();

		log.debug("----- CloudServiceDetailsDaoImpl ----- getVMJSONArray() ----- Method Called");
		
		StringBuilder sql = new StringBuilder();
		//CustomerID = "TT001";
		sql.append("Select device.ciid, device.ciname, Min(device.color) as priority, case when Min(device.color) = 1 then 'Red' when Min(device.color) = 2 then 'Amber' else 'Green' end as vmcolor, device.vblock, device.vmalias");
		sql.append(" from ( Select c.ciid,c.ciname, i.priority,");
		sql.append(" CASE WHEN i.priority IN (1 , 2) AND i.customerimpacted = 1 THEN 1");
		sql.append(" WHEN i.priority IN (1 , 2) AND  i.customerimpacted = 0 OR i.priority IN (3 , 4, 5) AND i.customerimpacted = 1 THEN 2 ELSE 3 END as color,");
		sql.append(" (select ci.baseciname from cirelationship ci where ci.dependentciid = c.ciid) as vblock, vm.vmalias as vmalias");
		sql.append(" FROM  configurationitem c, incdtodevicemapping id, incident i, virtualmachine vm");
		sql.append(" WHERE id.deviceciid = c.ciid AND id.incidentid = i.incidentid AND c.ciid=vm.ciid and c.cmdbclass='VM Instance'  AND c.customeruniqueid = '"+CustomerID+"'");
		sql.append(" and c.status = 'Operational' and lower(i.incidentstatus) not in ( 'Closed','Cancelled','Completed')) ");
		sql.append(" device GROUP BY device.ciid");
		
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());

		List<Object[]> per_color = (List<Object[]>) query.list();
		log.debug("----- CloudServiceDetailsDaoImpl ----- getVMJSONArray() ----- Query is "+per_color==null?"NULL":""+per_color.size());
		List<String> vblockList = new ArrayList<String>();
		List<String> vmList = new ArrayList<String>();
		if (per_color != null) 
		{
			for (Object[] row : per_color) 
			{
				if (row[0] != null && row[0].toString().trim() != "" && row[1] != null && row[1].toString().trim() != "" && row[3] != null && row[3].toString().trim() != "" ) 
				{					
					obj = new JSONObject();
					vmList.add(row[0].toString());
					obj.put("name", row[1].toString());
					obj.put("color", colorNameToCode(row[3].toString()));
					obj.put("ciid", row[0].toString());
					if(row[4]!=null){
						vblockList.add(row[4].toString());
						obj.put("catagory", row[4].toString() );
					}else{
						obj.put("catagory", null);
					}
					obj.put("priority", getPriority(row[3].toString()));
					obj.put("vmalias", row[5].toString());
					JSONObjArray.add(obj);
					
					log.debug("----- CloudServiceDetailsDaoImpl ----- getVMJSONArray() ----- Obj "+obj.toString());
				}
			}
		}
		
		String withNoDependenceSql = "select c.ciid, c.ciname, c.cmdbclass, vm.vmalias, (select ci.baseciname from cirelationship ci where ci.dependentciid = c.ciid) as depend "
				+ "from configurationitem c, virtualmachine vm where c.status = 'Operational' and c.customeruniqueid = '"+CustomerID+"' "
						+ "and ((c.cmdbclass = 'vBLOCK' and c.ciname not in (:vblockList)) or (c.cmdbclass = 'VM Instance' and c.ciid not in (:vmList))) and c.ciid=vm.ciid";
		Query vblockBaseQuery = sessionFactory.getCurrentSession().createSQLQuery(withNoDependenceSql);
		if(vblockList!=null && vblockList.size()>0)
			vblockBaseQuery.setParameterList("vblockList", vblockList);
		else
			vblockBaseQuery.setParameter("vblockList", "''");
		if(vmList!=null && vmList.size()>0)
			vblockBaseQuery.setParameterList("vmList", vmList);
		else
			vblockBaseQuery.setParameter("vmList", "''");
		if(vblockBaseQuery.list()!=null && vblockBaseQuery.list().size()>0){
			System.out.println("Testing the flow");
			for (Object[] vBlockCiName : (List<Object[]>)vblockBaseQuery.list()) {
				log.info(" vBlockCiName[2]-----------"+  vBlockCiName[2].toString()+ "vBlockCiName[1]-------"+vBlockCiName[1].toString());
				if(vBlockCiName[0]!=null && vBlockCiName[1]!=null){
					if(vBlockCiName[2]!=null && "vBLOCK".equalsIgnoreCase(vBlockCiName[2].toString().trim())){
						
						log.info("VBLOCK ----------------"+vBlockCiName[1].toString());
						obj = new JSONObject();
						obj.put("name", null);
						obj.put("color", null);
						obj.put("ciid", null);
						obj.put("catagory", vBlockCiName[1].toString() );
						obj.put("priority", null);
						JSONObjArray.add(obj);
						
					}
					if(vBlockCiName[2]!=null && "VM Instance".equalsIgnoreCase(vBlockCiName[2].toString().trim())){
					
						obj = new JSONObject();
						obj.put("name", vBlockCiName[1].toString());
						obj.put("color", colorNameToCode("Green"));
						obj.put("ciid", vBlockCiName[0].toString());
						if(vBlockCiName[4]!=null)
							obj.put("catagory", vBlockCiName[4].toString());
						else
							obj.put("catagory", null);
						obj.put("priority", "3");
						
						obj.put("vmalias",vBlockCiName[3].toString());
						JSONObjArray.add(obj);
					}
				}
			}
		}
		
		System.out.println("JSONObjArray.tostring----------------------"+JSONObjArray.toString());
		return JSONObjArray.toString();
	}

	

	@SuppressWarnings({ "unchecked" })
	public String getvBlockArrayJSON(String CustomerID)
	{
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		System.out.println("----- CloudServiceDetailsDaoImpl ----- getvBlockArrayJSON() ----- Method Called");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select ci.ciid, ci.ciname, TRUNCATE(vb.maxsizeofstorage,2), TRUNCATE(vb.numberofvcpu,2), TRUNCATE(vb.maxmemory,2), vb.vcevisionurl from configurationitem ci, vblock vb where ci.ciid = vb.ciid and ci.cmdbclass='vBLOCK' and ci.customeruniqueid = '"+CustomerID+"' and (lower(ci.deliverystatus) = 'operational' or lower(ci.cistatus) = 'mac')");
				
		List<Object[]> result = (List<Object[]>) query.list();
		System.out.println("----- CloudServiceDetailsDaoImpl ----- getvBlockArrayJSON() ----- Query is "+result==null?"NULL":""+result.size());
		
		if (result != null) 
		{
			for (Object[] vblock : result) 
			{
				System.out.println("vblock ---------------->>>>>>>>"+vblock[0].toString());
				obj = new JSONObject();
				
				if (null != vblock[1] && null != vblock[2] && null != vblock[3] && null != vblock[4] && null != vblock[5] )
				{
					obj.put("vBlock", vblock[1].toString());	
					
					obj.put("storage_total",vblock[2].toString());
					obj.put("cpu_total", vblock[3].toString());
					obj.put("memory_total", vblock[4].toString());							
					obj.put("visionURLArray", vblock[5].toString());
					
					//Query query1 = sessionFactory.getCurrentSession().createSQLQuery("select ci.ciid, ci.cmdbclass, TRUNCATE(vm.disks*vm.disksize,2), vm.cpus,TRUNCATE(vm.memory/1024,2), vc.vcenterurl from configurationitem ci, virtualmachine vm, vcenter vc where"
					//		+ " ci.ciid IN (SELECT distinct(cr.dependentCIId) FROM CIRelationship cr WHERE cr.baseCIId IN ('"+vblock[0].toString()+"')) and (ci.ciid =  vm.ciid OR ci.ciid =  vc.ciid) GROUP BY ciid;");
					
					Query query1 = sessionFactory.getCurrentSession().createSQLQuery("select ci.ciid, ci.cmdbclass, TRUNCATE(vm.disks*vm.disksize,2), vm.cpus,TRUNCATE(vm.memory/1024,2), vc.vcenterurl from   configurationitem ci, virtualmachine vm, vcenter vc"
							   + " where (ci.ciid =  vm.ciid OR ci.ciid =  vc.ciid) and ci.customeruniqueid = '"+CustomerID+"' GROUP BY ciid");
					
					double storage_used = 0;
					double cpu_used = 0;
					double memory_used = 0;
					String vCenterURLArray = "";				
					
					List<Object[]> dependentCI = (List<Object[]>) query1.list();
					
					if(dependentCI != null)
					{
						for (Object[] deConfiguration : dependentCI) 
						{	
							if(null != deConfiguration[1] && "VM Instance".equalsIgnoreCase((String)deConfiguration[1]))
							{
								try
								{
									storage_used += Double.parseDouble(deConfiguration[2]+"");
								}
								catch(Exception e){}									
								
								try
								{
									cpu_used += Double.parseDouble(deConfiguration[3].toString());			
								}
								catch(Exception e){}
								
								try
								{
									memory_used+= Double.parseDouble(deConfiguration[4].toString()); 	
								}
								catch(Exception e){}
									
							}
							else if(deConfiguration[1]!=null && deConfiguration[5]!=null && "vCENTER".equalsIgnoreCase((String)deConfiguration[1]))
							{
								try
								{
									vCenterURLArray = deConfiguration[5].toString();
								}
								catch(Exception e){}
							}
						}
					}				
				
					obj.put("storage_used", storage_used);
					obj.put("cpu_used",cpu_used);							
					obj.put("memory_used",memory_used);					
					obj.put("vCenterURLArray",vCenterURLArray);
					JSONObjArray.add(obj);
				}
			}
		}

		log.debug("----- CloudServiceDetailsDaoImpl ----- getvBlockArrayJSON() ----- "+JSONObjArray.toString());
		return JSONObjArray.toString();
	}
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("green"))
			return "background-color:rgb(163, 207, 98)";
		else if (color.equalsIgnoreCase("amber"))
			return "background-color:rgb(255, 156, 0)";
		else if (color.equalsIgnoreCase("red"))
			return "background-color:rgb(227, 34, 18)";
		else
			return "";
	}

	private String getPriority(String object) 
	{
		if (object.equalsIgnoreCase("red"))
			return "1";
		else if (object.equalsIgnoreCase("amber"))
			return "2";
		else
			return "3";
	}
	
	public String getLastRefreshDate(String customerId, String entityType) {

		log.debug("cloudServiceDetailsDaoImpl * getLastRefreshDate * Inside the method");
		StringBuilder sql = new StringBuilder();
		sql.append("select max(lastrefreshtime) from lastrefreshtime sa");
		sql.append(" where sa.customeruniqueid=:customerId and entityname=:entityType");
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
		query.setString("customerId", customerId);
		query.setString("entityType", entityType);
		log.debug("cloudServiceDetailsDaoImpl * getLastRefreshDate * Customer id and entity type----->"+customerId+" and "+entityType);
		Date refreshDate = (Date) query.uniqueResult();
		String refreshDateStr = com.tt.util.RefreshUtil.convertDateToString(refreshDate);
		
		log.debug("Last Refresh Date is :: " + refreshDate);
		log.debug("cloudServiceDetailsDaoImpl * getLastRefreshDate * Exit the method");
		return refreshDateStr;
	}
	
}
