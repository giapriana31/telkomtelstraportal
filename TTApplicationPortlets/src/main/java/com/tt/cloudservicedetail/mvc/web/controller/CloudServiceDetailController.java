package com.tt.cloudservicedetail.mvc.web.controller;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.cloudservicedetail.mvc.service.CloudServiceDetailsManagerImpl;
import com.tt.logging.Logger;
import com.tt.utils.TTGenericUtils;

@Controller("CloudServiceDetailController")
@RequestMapping("VIEW")
public class CloudServiceDetailController {

	CloudServiceDetailsManagerImpl cloudServiceDetailsManagerImpl;
	private final static Logger log = Logger.getLogger(CloudServiceDetailController.class);

	@Autowired
	@Qualifier("cloudServiceDetailsManagerImpl")
	public void setCloudServiceDetailsManagerImpl(
			CloudServiceDetailsManagerImpl cloudServiceDetailsManagerImpl) {
		this.cloudServiceDetailsManagerImpl = cloudServiceDetailsManagerImpl;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse Response, Model model) throws PortalException,SystemException 
	{
		log.debug("CloudServiceDetailController ----- handleRenderRequest Called");
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(Response));
		log.debug("CloudServiceDetailController ----- handleRenderRequest CustomerID "+customerUniqueID);
		
		model.addAttribute("VMArrayJSON",cloudServiceDetailsManagerImpl.getVMJSONArray(customerUniqueID));
		model.addAttribute("vBlockArrayJSON",cloudServiceDetailsManagerImpl.getvBlockArrayJSON(customerUniqueID));

		String lastRefreshDate = cloudServiceDetailsManagerImpl.getLastRefreshDate(customerUniqueID);
		model.addAttribute("lastRefreshDate", lastRefreshDate);
		
		log.debug("CloudServiceDetailController ----- return from handleRenderRequest");
		return "cloudServicedetail";
	}
	
	@ActionMapping(value = "refreshRealTimeVMs")
	public void dataRefreshRealTimeVMs(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException, PortalException, SystemException{
		
		log.debug("CloudServiceDetailController * dataRefresh * Inside the method");
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(actionRequest),PortalUtil.getHttpServletResponse(actionResponse));
		//String customerId = (String)user.getExpandoBridge().getAttribute("CustomerUniqueID");
		log.debug("CloudServiceDetailController * dataRefresh * customerId--------------->>"+customerUniqueID);
		cloudServiceDetailsManagerImpl.dataRefreshRealTimeVMs(customerUniqueID);
		//String url = prop.getProperty("tt.homeURL") + "/" + prop.getProperty("ttIncidentPageName");
		log.debug("TicketHeaderController * dataRefresh * Exit the method");
		//actionResponse.sendRedirect(url);
	}
	
}