package com.tt.cloudservicedetail.mvc.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.batchrefresh.CIPrivateCloudBatchRefresh;
import com.tt.batchrefresh.PrivateCloudCIRelationBatchRefresh;
import com.tt.cloudservicedetail.mvc.dao.CloudServiceDetailsDaoImpl;
import com.tt.cloudservicedetail.mvc.web.controller.CloudServiceDetailController;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;

@Service(value = "cloudServiceDetailsManagerImpl")
@Transactional
public class CloudServiceDetailsManagerImpl 
{	
	private CloudServiceDetailsDaoImpl cloudServiceDetailsDaoImpl;
	
	private final static Logger log = Logger.getLogger(CloudServiceDetailController.class);

	@Autowired
	@Qualifier("cloudServiceDetailsDaoImpl")
	public void setCloudServiceDetailsDaoImpl(CloudServiceDetailsDaoImpl cloudServiceDetailsDaoImpl) 
	{
		this.cloudServiceDetailsDaoImpl = cloudServiceDetailsDaoImpl;
	}

	public String getVMJSONArray(String CustomerID) 
	{		
		return cloudServiceDetailsDaoImpl.getVMJSONArray(CustomerID);
	}
	
	public String getvBlockArrayJSON(String CustomerID)
	{
		return cloudServiceDetailsDaoImpl.getvBlockArrayJSON(CustomerID);
	}
	public void dataRefreshRealTimeVMs(String customerId) {

		log.debug("cloudServiceDetailsManagerImpl * dataRefresh * Inside the method");
		String lastRefreshDateCI = cloudServiceDetailsDaoImpl.getLastRefreshDate(customerId,"PCConfiguration");
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		try {
			CIPrivateCloudBatchRefresh refreshObjPCCI = new CIPrivateCloudBatchRefresh();
			
			String toDate = new String();
			String fromDateCI = new String();
			Date todate = new Date();
			toDate = df.format(todate);
			log.debug("cloudServiceDetailsManagerImpl * dataRefresh * toDate------>"+toDate);
			if (lastRefreshDateCI != null)
				fromDateCI = lastRefreshDateCI;
			else{
				String batchLastRefreshDateCI = cloudServiceDetailsDaoImpl.getLastRefreshDate("", "PCConfiguration");
				if(batchLastRefreshDateCI != null)
					fromDateCI = batchLastRefreshDateCI;
				else
					fromDateCI = "2015-01-01 00:00:00";
			}
			log.debug("cloudServiceDetailsManagerImpl * dataRefresh * fromDate------>"+fromDateCI);

			refreshObjPCCI.refreshDataCustom(fromDateCI,toDate,"CUST",customerId);
			log.debug("cloudServiceDetailsManagerImpl * dataRefresh * Exit the method");
		} catch (Exception ex) {
			log.error(" Exception while calling data refresh ");
			ex.printStackTrace();
		}
	}
	
	public String getLastRefreshDate(String customerId)
	{
		String lastRefreshDate = cloudServiceDetailsDaoImpl.getLastRefreshDate(customerId,"PCConfiguration");
		return lastRefreshDate;
	}
}
