package com.tt.riverbedlevel0.mvc.dao;

import java.util.ArrayList;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value = "riverbedLevel0DaoImpl")
@Transactional
public class RiverbedLevel0DaoImpl {
	
	@Autowired
	private SessionFactory sessionFactory;
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(RiverbedLevel0DaoImpl.class);
	private static final String greenColor=prop.getProperty("greenColor");
	private static final String redColor=prop.getProperty("redColor");
	private static final String amberColor=prop.getProperty("amberColor");
	private static final String greyColor=prop.getProperty("greyColor");
	
	public ArrayList<String> getRiverbedOptimizationStatus(String customerId){
		
		ArrayList<String> riverbedOptimizationStatus = new ArrayList<String>();
		
		StringBuffer sb = new StringBuffer();
		/*sb.append(" select *, (select case when optimization<0 then '"+greyColor+"' ");
		sb.append(" when optimization between 0 and 39 then '"+redColor+"' ");
		sb.append(" when optimization between 40 and 69 then '"+amberColor+"' ");
		sb.append(" when optimization >=70 then '"+greenColor+"' end) colour, (SELECT CASE WHEN 540-(floor(540/(100/optimization))) IS NULL THEN '540' ELSE 540-(floor(540/(100/optimization))) END ) degree from (");
		sb.append(" select count(1) appliance, floor(sum(avg_reduction)/count(1)) optimization from (");
		sb.append(" select appliance,count(1) jml_application,floor(sum(reduction_zero)/count(1)) avg_reduction from (");
		sb.append(" select *, (select case when reduction <= 0 then 0 else reduction end) reduction_zero from (");
		sb.append(" select a.*,floor((((lan_in + lan_out) - (wan_in + wan_out)) / (lan_in + lan_out))*(100)) reduction from riverbeddashboard a, configurationitem b");
		sb.append(" where a.ciname = b.ciname and b.productclassification = 'M-WANOPT' and b.customeruniqueid = '"+customerId+"' ");
		sb.append(" and a.start_time BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00') AND DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59')");
		sb.append(" )c");
		sb.append(" )d group by appliance");
		sb.append(" )e");
		sb.append(" )f");*/
		sb.append(" select *, (select case when optimization<0 then '"+greyColor+"'");
		sb.append(" when optimization between 0 and 39 then '"+redColor+"' ");
		sb.append(" when optimization between 40 and 69 then '"+amberColor+"' ");
		sb.append(" when optimization >=70 then '"+greenColor+"' end) colour, (SELECT CASE WHEN 540-(floor(540/(100/optimization))) IS NULL THEN '540' ELSE 540-(floor(540/(100/optimization))) END ) degree from (");
		sb.append(" select count(1) appliance, floor(sum(avg_reduction)/count(1)) optimization from (");
		sb.append(" select appliance,count(1) jml_application,floor(sum(reduction_zero)/count(1)) avg_reduction from (");
		sb.append(" select *, (select case when reduction <= 0 or reduction is null then 0 else reduction end) reduction_zero from (");
		sb.append(" select a.*,floor((((lan_in + lan_out) - (wan_in + wan_out)) / (lan_in + lan_out))*(100)) reduction from riverbeddashboard a, (select a.*,SUBSTRING_INDEX(SUBSTRING_INDEX(b.customersite,'-',-2),'-',1 ) city from configurationitem a, site b where a.siteid = b.siteid and a.productclassification = 'M-WANOPT' and a.customeruniqueid = '"+customerId+"') b");
		sb.append(" where a.ciname = b.ciname");
		sb.append(" and a.start_time BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00') AND DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59')");
		sb.append(" )c");
		sb.append(" )d group by appliance");
		sb.append(" )e");
		sb.append(" )f");
		log.info("query for riverbedlevel 0-" +sb);
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		Object[] result=(Object[])query.uniqueResult();
		
		if(null!=result){
			riverbedOptimizationStatus.add(null!=result[0]?result[0].toString():"0");
			riverbedOptimizationStatus.add(null!=result[1]?result[1].toString():"0");
			
			if(null!=result[2]) {
				riverbedOptimizationStatus.add(result[2].toString());
			}else {
				riverbedOptimizationStatus.add(greyColor);
			}
			
			riverbedOptimizationStatus.add(null!=result[3]?result[3].toString():"0");
			
		}
		
		
		return riverbedOptimizationStatus;
		
	}

}
