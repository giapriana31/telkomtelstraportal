package com.tt.riverbedlevel0.mvc.web.controller;

import java.util.ArrayList;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.riverbedlevel0.mvc.service.RiverbedLevel0ManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("riverbedLevel0Controller")
@RequestMapping("VIEW")
public class RiverbedLevel0Controller {
	
	private RiverbedLevel0ManagerImpl riverbedLevel0ManagerImpl;

	@Autowired
	@Qualifier("riverbedLevel0Manager")
	public void setRiverbedLevel0ManagerImpl(RiverbedLevel0ManagerImpl riverbedLevel0ManagerImpl) {
		this.riverbedLevel0ManagerImpl = riverbedLevel0ManagerImpl;
	}
	
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(renderResponse);
	
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		
		SubscribedService ss = new SubscribedService();
		int count = ss.isSubscribedServicesOpt(GenericConstants.PRODUCTCLASSIFICATION_RIVERBED, customerId);

		ArrayList<String> riverbedOptimizationStatus= riverbedLevel0ManagerImpl.getRiverbedOptimizationStatus(customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_RIVERBED);
			return "unsubscribed_riverbed_service";
		}
		else
		{
			model.addAttribute("riverbedOptimizationStatus", riverbedOptimizationStatus);
				
			return "riverbedLevel0";
		}
	}
	

}
