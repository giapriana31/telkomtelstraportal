package com.tt.riverbedlevel0.mvc.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.riverbedlevel0.mvc.dao.RiverbedLevel0DaoImpl;

@Service(value = "riverbedLevel0Manager")
@Transactional
public class RiverbedLevel0ManagerImpl {
	
	private RiverbedLevel0DaoImpl riverbedLevel0DaoImpl;

	@Autowired
	@Qualifier("riverbedLevel0DaoImpl")
	public void setRiverbedLevel0DaoImpl(RiverbedLevel0DaoImpl riverbedLevel0DaoImpl) {
		this.riverbedLevel0DaoImpl = riverbedLevel0DaoImpl;
	}
	
	public ArrayList<String> getRiverbedOptimizationStatus(String customerId){
		return riverbedLevel0DaoImpl.getRiverbedOptimizationStatus(customerId);
	}
	
	

}
