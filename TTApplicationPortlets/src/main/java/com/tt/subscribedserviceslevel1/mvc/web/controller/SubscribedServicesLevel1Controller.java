package com.tt.subscribedserviceslevel1.mvc.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.tt.bvdashboard.mvc.service.BVDashboardService;
import com.tt.bvdashboard.mvc.web.controller.BVDashBoardController;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.utils.TTGenericUtils;
@Controller("subscribedServicesController")
@RequestMapping("VIEW")
public class SubscribedServicesLevel1Controller {
	
	
	private final static Logger log = Logger.getLogger(SubscribedServicesLevel1Controller.class);
	private BVDashboardService bvDashboardService;

	
	@Autowired
    @Qualifier("bvDashboardService")
	public void setBvDashboardService(BVDashboardService bvDashboardService) {
		this.bvDashboardService = bvDashboardService;
	}
	@RenderMapping
	public String handleRenderRequest(RenderRequest renderRequest, RenderResponse renderResponse, Model model) throws SystemException
	{
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		HttpServletResponse response = PortalUtil.getHttpServletResponse(renderResponse);
		String customerId = TTGenericUtils.getCustomerIDFromSession(request, response);
		
		List<String> userList = getUserListFromLiferayDatabase(customerId);
		
		if (userList != null) 
		{
			model.addAttribute("usersList", userList);
		}
		else
		{
			model.addAttribute(GenericConstants.NA, userList);
		}
		
		
		return "subscribedserviceslevel1";
	
		}

	
	@ResourceMapping(value = "subscribedservicesTableURL")
	public void getSubscribedServicesTableData(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortalException, SystemException
	{
		log.info("Class:BVDashBoardController Method:getSubscribedServicesTableData Start");
		
		HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletResponse response = PortalUtil.getHttpServletResponse(resourceResponse);
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
		String serviceFilters = uploadRequest.getParameter("serviceFilters");
		//String serviceFiltersRequest = request.getParameter("serviceFilters");
		log.info("The service filters came::"+serviceFilters);
		String[] filtersArray = serviceFilters.split(",");
		List<String> servicesFiltersList = new ArrayList<String>();
		for(int i=0; i<filtersArray.length ; i++){
			servicesFiltersList.add(filtersArray[i]);
		}
		log.info("List"+servicesFiltersList);
		log.info("List Size"+servicesFiltersList.size());
		
		
		String customerId = TTGenericUtils.getCustomerIDFromSession(request, response);
		JSONArray jsonData = bvDashboardService.getSubscribedServicesTableData(customerId, servicesFiltersList, request);
		log.debug("JSON Array for SS :: " + jsonData);
		
		resourceResponse.getWriter().print(jsonData.toString());
		
		log.info("Class:BVDashBoardController Method:getSubscribedServicesTableData End");
	}
	
	
	
	@ResourceMapping(value = "assignUserToServiceURL")
    public void assignUserNameToService(ResourceRequest request, ResourceResponse response) throws Exception
	{
		
		log.info("Class : BVDashBoardController, Method: assignUserNameToService, Start");
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse resp = PortalUtil.getHttpServletResponse(response);
		
		String customerId = TTGenericUtils.getCustomerIDFromSession(req, resp);
		
		UploadPortletRequest uRequest = PortalUtil.getUploadPortletRequest(request);
		
		String uName = uRequest.getParameter("username");
		String sName = uRequest.getParameter("servicename");
		
		log.debug("from upload portlet request User name is :: " + uName);
		log.debug("from upload portlet request  Service Name is :: " + sName);
		
		com.liferay.portal.kernel.json.JSONObject addContactJSONObject=bvDashboardService.getAddContact(uName , sName, customerId);
		
	     response.getWriter().print(addContactJSONObject.toString());
		
		log.info("Class : BVDashBoardController, Method: assignUserNameToService, End");
	
		
		
	}
	
	private List<String> getUserListFromLiferayDatabase(String customerId) throws SystemException 
	{
		log.info("Class : BVDashBoardController, Method: getUserListFromLiferayDatabase, Start");
		
    	List<String> userList = new ArrayList<String>();
    	User user;
    	
    	String customAttributeName = "CustomerUniqueID";
    	
    	long classNameId = ClassNameLocalServiceUtil.getClassNameId(User.class);
    	long companyId = PortalUtil.getDefaultCompanyId();
    	
		List<ExpandoValue> requiredExpandoValues = ExpandoValueLocalServiceUtil.getColumnValues(companyId, classNameId,
						ExpandoTableConstants.DEFAULT_TABLE_NAME,
						customAttributeName, customerId, -1, -1);
		
		log.debug("expando values :: " + requiredExpandoValues.size());
		
		for(int i=0; i<requiredExpandoValues.size(); i++)
		{
			long userId = requiredExpandoValues.get(i).getClassPK();
			try 
			{
				user = UserLocalServiceUtil.getUser(userId);
				userList.add(user.getFullName() + "-" + user.getDisplayEmailAddress());
			} 
			catch (PortalException e) 
			{
				log.error("Error in Class: BVDashBoardController, Method: getUserListFromLiferayDatabase " + e.getMessage());
			}
		}
		log.info("Class : BVDashBoardController, Method: getUserListFromLiferayDatabase, End");
		return userList;
	}

	
}

