package com.tt.pvservicerequest.mvc.model;

public class PVSRSearchParameters {

	private String sortCriterion;
	private String sortDirection;
	private int pageNumber = 1; // default value
	private String searchqueryList;
	private int maxResults = 0;

	// Set it from session
	private String customeruniqueid;
	private String citype = "Service";
	private String tabSelected;
	public String getSortCriterion() {
		return sortCriterion;
	}
	public void setSortCriterion(String sortCriterion) {
		this.sortCriterion = sortCriterion;
	}
	public String getSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSearchqueryList() {
		return searchqueryList;
	}
	public void setSearchqueryList(String searchqueryList) {
		this.searchqueryList = searchqueryList;
	}
	public int getMaxResults() {
		return maxResults;
	}
	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}
	public String getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
	public String getCitype() {
		return citype;
	}
	public void setCitype(String citype) {
		this.citype = citype;
	}
	public String getTabSelected() {
		return tabSelected;
	}
	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}
	
}
