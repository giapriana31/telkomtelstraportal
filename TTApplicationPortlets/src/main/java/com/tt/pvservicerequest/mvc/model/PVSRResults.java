package com.tt.pvservicerequest.mvc.model;

import java.util.List;

import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestView;

public class PVSRResults {
	private int recordCount = 0 ; // default value
	/*private List<ServiceRequest> serviceRequests;*/
	private List<ServiceRequestView> serviceRequests;
	private int draw = 0 ;
	private int recordsFiltered = 0;
	
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	public List<ServiceRequestView> getServiceRequests() {
		return serviceRequests;
	}
	public void setServiceRequests(List<ServiceRequestView> serviceRequests) {
		this.serviceRequests = serviceRequests;
	}
	
	
}
