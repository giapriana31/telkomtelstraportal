package com.tt.pvservicerequest.mvc.service.common;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.pvservicerequest.mvc.dao.PVCreateSRDao;
import com.tt.pvservicerequest.mvc.model.PVSRResults;
import com.tt.pvservicerequest.mvc.model.PVSRSearchParameters;
import com.tt.servicerequestlist.mvc.model.DetailsSearchParameters;

@Service(value="pvSRManager")
@Transactional
public class PVSRManager {
	
	private PVCreateSRDao pvCreateSRDao;
	
	@Autowired
	@Qualifier("pvCreateSrDao")
	public void setPvCreateSRDao(PVCreateSRDao pvCreateSRDao) {
		this.pvCreateSRDao = pvCreateSRDao;
	}
	
	public PVSRResults getSRData(PVSRSearchParameters searchParameters){
		return pvCreateSRDao.getSRData(searchParameters);
	}
	
	public ServiceRequest getServiceRequestDetailsForRequestId(
			String requestId, DetailsSearchParameters detailsSearchParameters) {
		return pvCreateSRDao.getServiceRequestDetailsForRequestId(
				requestId, detailsSearchParameters);
	}
	
	public ServiceRequestItem getServiceRequestItemDetailsForItemId(
			String itemId) {
		return pvCreateSRDao.getServiceRequestItemDetailsForItemId(itemId);
	}

}
