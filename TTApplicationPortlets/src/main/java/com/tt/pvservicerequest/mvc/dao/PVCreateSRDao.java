package com.tt.pvservicerequest.mvc.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.model.servicerequest.ServiceRequestView;
import com.tt.pvservicerequest.mvc.model.PVSRResults;
import com.tt.pvservicerequest.mvc.model.PVSRSearchParameters;
import com.tt.servicerequestlist.mvc.model.DetailsSearchParameters;
import com.tt.utils.PropertyReader;

@Repository(value = "pvCreateSrDao")
@Transactional
public class PVCreateSRDao {
	
	public static Properties prop = PropertyReader.getProperties();
	String statusClosed=prop.getProperty("statusClosed");
	String statusCancelled=prop.getProperty("statusCancelled");
	String statusComplete=prop.getProperty("statusComplete");
	String statusCompleted=prop.getProperty("statusCompleted");
	String statusClosedCancelled=prop.getProperty("statusClosedCancelled");
	private final static Logger log = Logger.getLogger(PVCreateSRDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public static final String AND = "' AND ";
	public static final String EQUAL_SINGLE = " = '";
	public static final String INOPERATOR = " IN ";
	public static final String OPENBRACE = " ( ";
	public static final String CLOSEBRACE = " ) ";
	public static final String SINGLEQUOTE = "'";
	public static final String ANDOPERATOR = " AND ";
	public static final String BETWEEN = " BETWEEN ";
	public static final String TO_DATE = " TO_DATE ";
	public static final String DATEFORMAT = " 'yyyy-MM-dd HH:mm:ss' ";
	public static final String COMMA = " , ";
	public static final String ORDERBY = " ORDER BY ";
	public static final String OFFSET = " OFFSET ";
	public static final String FETCHNEXT = " FETCH NEXT ";
	public static final String ROWSONLY = " ROWS ONLY ";
	public static final String LIKE = " LIKE ";
	public static final String OROPERATOR = " OR ";
	public static final String PERCENTAGE = "%";
	public static final String LIMIT = " LIMIT ";
	public static final String GREATERTHANEQUALTO = " >= '";
	public static final String LESSTHANEQUALTO = " <= '";
	// Field level constants
	public static final String CMDBCLASS = " cmdbclass ";
	public static final String CUSTOMERIMPACTED = " customerimpacted ";
	public static final String OPENDATETIME = " opendatetime ";
	public static final String RESOLUTIONDATETIME = " resolutiondatetime ";
	public static final String CASEMANAGER = " casemanager ";
	public static final String INCIDENTID = " incidentid ";
	public static final String SHORTDESCRIPTION = " summary ";
	public static final String CUSTOMERUNIQUEID = " customeruniqueid  ";
	public static final String CITYPE = " citype  ";
	public static final String USERID = " userid  ";
	public static final String CINAME = " ciname  ";
	public static final String SITEID = " siteid  ";
	private static final String COMPLETED_TAB = "completedTab";
	private static final String ACTIVE_TAB = "activeTab";
	private static final String CUSTOMER_ID = "customerId";
	
	private static final String CHANGE_SOURCE_CUSTOMER = "Customer";
	private static final String CHANGE_SOURCE_TELKOMTELSTRA = "telkomtelstra";
	private static final String BLANK = "";
	private static final String CUSTOMER_APPROVAL_STATUS = "customerapprovalstatus";
	
	public PVSRResults getSRData(PVSRSearchParameters searchParameters) {
		
			log.info("PVCreateSRDao, getSRData with parameters, Start");
			
	    	PVSRResults pvsrResults= null;
	    	
			StringBuilder searhQuery = new StringBuilder();
			
			/*searhQuery.append(" from ServiceRequest sr join sr.serviceRequestItem sri ")
 			          .append("where sr.requestid = sri.servicerequestid ")
			          .append("and sr.customeruniqueid =:customerId and ")
					  .append("((sri.itemtype = '"+GenericConstants.RITM_MNS_NEW+"' OR sri.itemtype LIKE '%"+GenericConstants.RITM_LIKE_NEW+"%')")
			          .append(" or (sri.itemtype Like '%"+GenericConstants.RITM_LIKE_MAC+"%' and sri.projecttype LIKE '%"+GenericConstants.RITM_MAC_TYPE_COMPLEX+"%')")
			          .append(")");*/
			searhQuery.append(" from ServiceRequestView sr ")
	          .append("where sr.customeruniqueid =:customerId ");
			
						
			log.info("Printing searhQuery for getting SRData for PV " + searhQuery);

			if (searchParameters.getSearchqueryList() != null
					&& !searchParameters.getSearchqueryList().isEmpty()) {

				searhQuery.append(ANDOPERATOR).append(OPENBRACE)
						.append(" requestid ").append(LIKE).append(SINGLEQUOTE)
						.append(PERCENTAGE)
						.append(searchParameters.getSearchqueryList())
						.append(PERCENTAGE).append(SINGLEQUOTE).append(OROPERATOR)
						.append(" subject ").append(LIKE).append(SINGLEQUOTE)
						.append(PERCENTAGE)
						.append(searchParameters.getSearchqueryList())
						.append(PERCENTAGE).append(SINGLEQUOTE).append(OROPERATOR)
						.append(" requestor ").append(LIKE).append(SINGLEQUOTE)
						.append(PERCENTAGE)
						.append(searchParameters.getSearchqueryList())
						.append(PERCENTAGE).append(SINGLEQUOTE).append(CLOSEBRACE);
			}
			
			String srListingQuery = "Select distinct sr"  + searhQuery.toString();
			
	     	String selectCountQueryInitial = "Select count(distinct sr.requestid) "  + searhQuery.toString();
			String selectCountQuery=selectCountQueryInitial.replaceAll("Configuration", "configurationitem");
			selectCountQuery=selectCountQuery.replaceAll("cmdblass", "cmdbclass");
			log.info("selectCountQuery :: " + selectCountQuery);
			
			Query queryCount = sessionFactory.getCurrentSession().createQuery(selectCountQuery);
			
			if (searchParameters.getCustomeruniqueid() != null)
			{
				queryCount.setString("customerId", searchParameters.getCustomeruniqueid());
			}
			
			/* getting count from the query - used for pagination */
			Number recordCounts = (Number) queryCount.uniqueResult();
			
			if (recordCounts != null && recordCounts.intValue() > 0) {
				
				pvsrResults= new PVSRResults();
				pvsrResults.setRecordCount(recordCounts.intValue());
				log.info("recordCounts.intValue() \n "
						+ recordCounts.intValue());
				// sortCriterion
				if (searchParameters.getSortCriterion() != null
						&& !searchParameters.getSortCriterion().isEmpty())
					searhQuery.append(" ORDER BY ").append(
							searchParameters.getSortCriterion());

				// asc or desc
				if (searchParameters.getSortDirection() != null
						&& !searchParameters.getSortDirection().isEmpty())
					searhQuery.append(" ").append(
							searchParameters.getSortDirection());
				// page no pageNumber
				Integer offsetInt = searchParameters.getPageNumber();
				Integer maxResults = searchParameters.getMaxResults();
				
				log.debug("Listing query :: " + srListingQuery.toString());
				Query query = sessionFactory.getCurrentSession().createQuery(srListingQuery.toString());
					
				query.setFirstResult(offsetInt);
				query.setMaxResults(maxResults);
				
				if (searchParameters.getCustomeruniqueid() != null)
				{
					query.setString("customerId", searchParameters.getCustomeruniqueid());
				}
		
				log.info("Printing searhQuery with sorting \n "
							+ searhQuery);
					
				/*List<ServiceRequest> serviceRequests = query.list();*/
				List<ServiceRequestView> serviceRequests = query.list();
				
				log.info("serviceRequests count by list" + serviceRequests.size());
				pvsrResults.setServiceRequests(serviceRequests);
			}
				

			log.info("PVSRDAOimpl, getDevices with parameters, End");
			return pvsrResults;
				
	}
	
	public ServiceRequest getServiceRequestDetailsForRequestId(String requestId, DetailsSearchParameters detailsSearchParameters) {

		StringBuilder hql = new StringBuilder();
		hql.append("from ServiceRequest sr");
		hql.append(" where sr.requestid =:requestId");
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("requestId", requestId);

		log.info(query.toString());
		
		ServiceRequest serviceRequest = null;
		if (query.list() != null && !query.list().isEmpty()) 
		{
			serviceRequest = (ServiceRequest) query.uniqueResult();
			List<ServiceRequestItem> serviceRequestItems = filterServiceRequestItemsByIPType(serviceRequest.getServiceRequestItem());
			serviceRequest.setServiceRequestItem(serviceRequestItems);
		}
		return serviceRequest;
	}
	
	private List<ServiceRequestItem> filterServiceRequestItemsByIPType(List<ServiceRequestItem> serviceRequestItemsRecieved){

		List<ServiceRequestItem> newSRItemList = new ArrayList<ServiceRequestItem>();
		
		if (serviceRequestItemsRecieved != null && !serviceRequestItemsRecieved.isEmpty()) 
		{
	
			//iterate over serviceRequestItems
			for (ServiceRequestItem element : serviceRequestItemsRecieved)
			{
				String recieveditemtypes = element.getItemtype();
				String recievedprojecttypes = element.getProjecttype();
			
				if( ((recieveditemtypes.equalsIgnoreCase(GenericConstants.RITM_MNS_NEW))||(recieveditemtypes.toLowerCase().contains(GenericConstants.RITM_LIKE_NEW)))
						|| (recieveditemtypes.toLowerCase().contains(GenericConstants.RITM_LIKE_MAC) && recievedprojecttypes.toLowerCase().contains(GenericConstants.RITM_MAC_TYPE_COMPLEX)) )
				{
					newSRItemList.add(element);
				}
			}
		}
		
		log.info("New Item List---------" +newSRItemList);
		return newSRItemList;
	}

	public ServiceRequestItem getServiceRequestItemDetailsForItemId(
			String itemId) {

		StringBuilder hql = new StringBuilder();
		hql.append("from ServiceRequestItem sr");
      	hql.append(" where sr.itemid =:itemId");

		Query query = sessionFactory.getCurrentSession().createQuery(
				hql.toString());
		query.setString("itemId", itemId);
		log.info(query.toString());
		
		ServiceRequestItem serviceRequestItem = null;
		if (query.list() != null && !query.list().isEmpty()) {
			serviceRequestItem = (ServiceRequestItem) query
					.uniqueResult();
		}
		return serviceRequestItem;
	}
	
}

