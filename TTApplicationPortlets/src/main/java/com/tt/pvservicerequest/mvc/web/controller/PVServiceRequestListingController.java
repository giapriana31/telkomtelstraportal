package com.tt.pvservicerequest.mvc.web.controller;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.devicemapping.mvc.web.controller.DeviceSearchController;
import com.tt.logging.Logger;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.model.servicerequest.ServiceRequestItemDevices;
import com.tt.model.servicerequest.ServiceRequestItemNotes;
import com.tt.model.servicerequest.ServiceRequestView;
import com.tt.pvservicerequest.mvc.model.PVSRResults;
import com.tt.pvservicerequest.mvc.model.PVSRSearchParameters;
import com.tt.pvservicerequest.mvc.service.common.PVSRManager;
import com.tt.servicerequestlist.mvc.model.CreateSRForm;
import com.tt.servicerequestlist.mvc.model.DetailsSearchParameters;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("pvServiceRequestListingController")
@RequestMapping("VIEW")
public class PVServiceRequestListingController {

	private PVSRManager pvSRManager;
	
	public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(PVServiceRequestListingController.class);
    
    public static final String REQUESTID = "requestid";
	public static final String SUBJECT = "subject";
	public static final String LASTUPDATEDATE = "lastupdatedate";
	public static final String REQUESTOR = "requestor";
	public static final String STATUS = "status";
	
  	//Will go into constants
 	private static Map<String, String> mapDTSortCriterion() {
 		Map<String, String> sortCriterion = new HashMap<String, String>();
 		sortCriterion.put("0", REQUESTID);
 		sortCriterion.put("1", SUBJECT);
 		sortCriterion.put("2", LASTUPDATEDATE);
 		sortCriterion.put("3", REQUESTOR);
 		sortCriterion.put("4", STATUS); 		

 		return Collections.unmodifiableMap(sortCriterion);
 	}

    // todo:needs to move it to constants
 	// html table column order is important
 	private static final Map<String, String> DTSORTCRITERION = mapDTSortCriterion();
 	

	@Autowired
    @Qualifier("pvSRManager")
	public void setPvSRManager(PVSRManager pvSRManager) {
		this.pvSRManager = pvSRManager;
	}


	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {

		model.addAttribute("logSRFormModel", new CreateSRForm());
		return "pvServiceRequestListing";
	}
	
	@ResourceMapping(value = "datatableURI")
    public void getSRData(ResourceRequest resourceRequest,ResourceResponse resourceResponse)
	{
		log.info("Class: PVServiceRequestListingController, Method: getDeviceData Start");
    	PrintWriter out = null;
    	
    	try{  
	    	
	    	PVSRSearchParameters searchParameters = getUserDeviceSearchParameters(resourceRequest,resourceResponse);
		    
		    PVSRResults pvsrResults= pvSRManager.getSRData(searchParameters);
		    
			com.liferay.portal.kernel.json.JSONObject dtJsonValue = getResultJson(pvsrResults);
			
			log.info("dtJsonValue in getServiceRequestData : " + dtJsonValue );
			
			out = resourceResponse.getWriter();			
			out.print(dtJsonValue.toString());
			log.info("Class: ServiceRequestController, Method: getServiceRequestData End");
    	}
    	catch(Exception e){
    		log.error("Error in getServiceRequestData " + e.getMessage());
    		e.printStackTrace();
    	}
    	finally{
    		if(null != out) {
    			out.flush();
    			out.close();
    		}
    	}		
    }
	
	public PVSRSearchParameters getUserDeviceSearchParameters(ResourceRequest resourceRequest,ResourceResponse resourceResponse) {
		
		log.info("Class: PVServiceRequestListingController, Method: getUserPVSRSearchParameters");
		PVSRSearchParameters searchParameters = new PVSRSearchParameters();		
		
		try {
		
			HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(resourceRequest);
			HttpServletResponse httpresponse = PortalUtil.getHttpServletResponse(resourceResponse);			
			Enumeration paramNames = httprequest.getParameterNames();
			
			String cusomerUniqueId = TTGenericUtils.getCustomerIDFromSession(httprequest, httpresponse);
			searchParameters.setCustomeruniqueid(cusomerUniqueId);
			
			//todo:delet after test
			while (paramNames.hasMoreElements()) {
				Map <String, String>requestParamMap = new HashMap<String, String>();

				String paramName = (String) paramNames.nextElement();
				
				//todo:still needs to handle multi-selects
				//request.getParameterValues(paramName);
				
				String paramValue = httprequest.getParameter(paramName);
				requestParamMap.put(paramName, paramValue);
				log.info("paramName : " + paramName + " paramValues: "+ paramValue);
			}

			searchParameters.setSortCriterion(
					httprequest.getParameter("order[0][column]") != null 
					? DTSORTCRITERION.get(httprequest.getParameter("order[0][column]")) 
					: DTSORTCRITERION.get(REQUESTID));
			
			searchParameters.setSortDirection(
					httprequest.getParameter("order[0][dir]") != null 
					? httprequest.getParameter("order[0][dir]") 
					: "asc");	
			
			searchParameters.setSearchqueryList(
					httprequest.getParameter("searchTerm") != null 
					? httprequest.getParameter("searchTerm").toString()
					: null);
			
			searchParameters.setPageNumber(
					httprequest.getParameter("start") != null 
					? Integer.parseInt(httprequest.getParameter("start").toString())
					: 0);	
			log.info("search parameters page number---"+searchParameters.getPageNumber());
			
			searchParameters.setSearchqueryList(
					httprequest.getParameter("searchTerm") != null 
					? httprequest.getParameter("searchTerm").toString()
					: null);
			
			searchParameters.setMaxResults(
					httprequest.getParameter("maxResults") != null 
					? Integer.parseInt(httprequest.getParameter("maxResults").toString())
					: 30);
			

			log.info("getAllQueryParameters request param = " + searchParameters.getSearchqueryList());	
			
		} catch (Exception e) {		
			e.printStackTrace();
		}
		
		return searchParameters;
	}
	
public String getAllQueryParamAsJson(HttpServletRequest request) {
		
		//collect all the request parameters in a hash map
		HashMap grequestparamMap = getAllQueryParameters(request);
		
		//Convert the hashmap to json
		Gson gson = new Gson();		
		String queryParamJson = gson.toJson(grequestparamMap);
		
		return queryParamJson;
	}
	
	public HashMap getAllQueryParameters(HttpServletRequest request) {

		HashMap <String, String>requestParamMap = new HashMap<String, String>();

		Enumeration paramNames = request.getParameterNames();

		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			
			//todo:still needs to handle multi-selects
			//request.getParameterValues(paramName);
			
			String paramValue = request.getParameter(paramName);
			requestParamMap.put(paramName, paramValue);
			log.info("paramName : " + paramName + " paramValues: "+ paramValue);
		}		
		
		requestParamMap.put("sortCriterion",
				request.getParameter("order[0][column]") != null 
				? DTSORTCRITERION.get(request.getParameter("order[0][column]")) 
				: DTSORTCRITERION.get(REQUESTID));
		
		requestParamMap.put("sortDirection",
				request.getParameter("order[0][dir]") != null 
				? request.getParameter("order[0][dir]") 
				: "asc");		
		
		Integer pageNo=(Integer.parseInt(request.getParameter("start"))/10)+1;
		requestParamMap.put("pageNumber",
				request.getParameter("start") != null 
				? pageNo.toString() 
				: "0");				

		log.info("getAllQueryParameters request param = " + requestParamMap);
		
		return requestParamMap;
	}
	
	public com.liferay.portal.kernel.json.JSONObject getResultJson(PVSRResults pvsrResults){
		
		JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
		com.liferay.portal.kernel.json.JSONObject dtJsonValue = JSONFactoryUtil
				.createJSONObject();

		if (pvsrResults != null) {
			dtJsonValue.put("draw", pvsrResults.getDraw());
			dtJsonValue.put("recordsFiltered", pvsrResults.getRecordCount());

			resultJsonArray = populateDeviceArrayData(pvsrResults
					.getServiceRequests());

			dtJsonValue.put("recordsTotal", pvsrResults.getRecordCount());
			dtJsonValue.put("data", resultJsonArray);
		} else {
			dtJsonValue.put("draw", 0);
			dtJsonValue.put("recordsFiltered", 0);
			dtJsonValue.put("recordsTotal", 0);
			dtJsonValue.put("data", resultJsonArray);
		}

		return dtJsonValue;
	}
	
	public JSONArray populateDeviceArrayData(List<ServiceRequestView> servRequests){
		JSONArray rowJsonValue;
    	
    	JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
    	
    	for (ServiceRequestView serviceRequest : servRequests) 
    	{
    		rowJsonValue = JSONFactoryUtil.createJSONArray();
    		rowJsonValue.put("<a href=\"javascript:void(0)\" class=\"service-request-id-links\" onclick=\"getSingleServiceRequestDetails(this)\" id=\""+serviceRequest.getRequestid()+"\">"+serviceRequest.getRequestid()+"</a>");
			rowJsonValue.put("<span class=\"tt-sr-subject\">"+serviceRequest.getSubject()+"</span><br><span class=\"tt-sr-description\">"+serviceRequest.getDescription()+"</span>");
			
			if(serviceRequest.getLastupdatedate()!=null){
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String lastUpdatedString = dateFormat.format(serviceRequest.getLastupdatedate());
				rowJsonValue.put(lastUpdatedString);
			}
			else{
				rowJsonValue.put("");
			}
			
			rowJsonValue.put(serviceRequest.getRequestor());
			rowJsonValue.put(serviceRequest.getStatus());
    						
			
			resultJsonArray.put(rowJsonValue);				
		}
    	return resultJsonArray;
	}
	
	 @ResourceMapping(value = "getServiceRequestDetails")
		public void getRequestDetails(ResourceRequest req,ResourceResponse res) {
			log.info("PV Controller for getting SR Request details");

			try {

				HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
				HttpServletResponse response = PortalUtil.getHttpServletResponse(res);

				/*ThemeDisplay td = (ThemeDisplay) request
						.getAttribute(WebKeys.THEME_DISPLAY);*/
				String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(request, response);
				log.info("setting Cust ID");

				org.json.JSONObject obj = new JSONObject(request.getParameter("serviceRequestFilterIdentifier"));

				String requestId = obj.getString("requestid");
				log.info("request id from jsp :: " + requestId);
				

				getServiceRequestDetails(response,requestId);
				
				log.info("Population completed for Request details");

			} 
			catch (Exception e) {
				log.error("Exception while getting request details "+e.getMessage());
			}
		}
	 
	 public void getServiceRequestDetails(HttpServletResponse response,String requestId){
	    	
	    	try {
				/*com.liferay.portal.kernel.json.JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();*/
	    		PrintWriter out;
				out = response.getWriter();
				
				DetailsSearchParameters detailsSearchParameters=new DetailsSearchParameters();
				detailsSearchParameters.setRequestType("Service");
				
				ServiceRequest serviceRequest=pvSRManager.getServiceRequestDetailsForRequestId(requestId, detailsSearchParameters);
				if(serviceRequest!=null){
					
					List<ServiceRequestItem> listOfServiceRequestItems=serviceRequest.getServiceRequestItem();
					
					com.liferay.portal.kernel.json.JSONObject parentJsonValue = JSONFactoryUtil.createJSONObject();		
					parentJsonValue.put("summary", serviceRequest.getSubject());
					parentJsonValue.put("serviceRequestId", serviceRequest.getRequestid());
					parentJsonValue.put("status", serviceRequest.getStatus());
					parentJsonValue.put("requestor", serviceRequest.getRequestor());
					
					if(serviceRequest.getLastupdatedate()!=null){
						SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String lastUpdatedDateString= simpleDateFormat.format(serviceRequest.getLastupdatedate());
						parentJsonValue.put("lastUpdated", lastUpdatedDateString);
					}
					else{
						parentJsonValue.put("lastUpdated", "");
					}
					
					if(serviceRequest.getCreateddate()!=null){
						SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String createdOnString= simpleDateFormat.format(serviceRequest.getCreateddate());
						parentJsonValue.put("createdOn", createdOnString);
					}
					else{
						parentJsonValue.put("createdOn", "");
					}
					
					if(serviceRequest.getStartdate()!=null){
						SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String startDateString= simpleDateFormat.format(serviceRequest.getStartdate());
						parentJsonValue.put("startDate", startDateString);
					}
					else{
						parentJsonValue.put("startDate", "");
					}
					
					if(serviceRequest.getEnddate()!=null){
						SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String endDateString= simpleDateFormat.format(serviceRequest.getEnddate());
						parentJsonValue.put("endDate", endDateString);
					}
					else{
						parentJsonValue.put("endDate", "");
					}
					
					parentJsonValue.put("requestType", serviceRequest.getRequesttype());
					
					parentJsonValue.put("description", serviceRequest.getDescription());
					
					/*parentJsonValue.put("outagePresent", serviceRequest.getOutageendtime());
					parentJsonValue.put("outageStartTime", serviceRequest.getOutagestarttime());
					parentJsonValue.put("outageEndTime", serviceRequest.getOutageendtime());*/
					
					SimpleDateFormat crDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					/*parentJsonValue.put("plannedStartDate", serviceRequest.getPlannedstartdate() != null ?  crDateFormat1.format(serviceRequest.getPlannedstartdate()) : "");
					parentJsonValue.put("plannedEndDate", serviceRequest.getPlannedenddate() != null ?  crDateFormat1.format(serviceRequest.getPlannedenddate()) : "");
					parentJsonValue.put("outageStartTime", serviceRequest.getOutagestarttime() != null ? crDateFormat1.format(serviceRequest.getOutagestarttime()) : "");
					parentJsonValue.put("outageEndTime", serviceRequest.getOutageendtime() != null ? crDateFormat1.format(serviceRequest.getOutageendtime()) : "");

					parentJsonValue.put("category", serviceRequest.getCategory() != null ? serviceRequest.getCategory() : "");
					parentJsonValue.put("risk", serviceRequest.getRisk() != null ? serviceRequest.getRisk() : "");
					parentJsonValue.put("deviceName", serviceRequest.getDevicename() != null ? serviceRequest.getDevicename() : "");
					parentJsonValue.put("priority", serviceRequest.getPriority() != null ? serviceRequest.getPriority() : "");
					parentJsonValue.put("impact", serviceRequest.getImpact() != null ? serviceRequest.getImpact() : "");
					parentJsonValue.put("resolverGroup", serviceRequest.getResolvergroup() != null ? serviceRequest.getResolvergroup() : "");
					parentJsonValue.put("caseManager", serviceRequest.getCasemanager() != null ? serviceRequest.getCasemanager() : "");
					parentJsonValue.put("comments", serviceRequest.getComments() != null ? serviceRequest.getComments() : "");				
					parentJsonValue.put("outagePresent", serviceRequest.getOutagepresent() != null ? serviceRequest.getOutagepresent() : "");				
					parentJsonValue.put("serviceName", serviceRequest.getServicename() != null ? serviceRequest.getServicename() : "");
					parentJsonValue.put("siteName", serviceRequest.getSitename() != null ? serviceRequest.getSitename() : "");*/
					
					com.liferay.portal.kernel.json.JSONArray sriJsonArray = JSONFactoryUtil.createJSONArray();
					
					for (ServiceRequestItem serviceRequestItem : listOfServiceRequestItems) {
						com.liferay.portal.kernel.json.JSONArray sriJsonValue = JSONFactoryUtil.createJSONArray();
						
						sriJsonValue.put("<span class=\"tt-sri-itemid\">"+serviceRequestItem.getItemid()+"</span>");
						String itemtype=serviceRequestItem.getItemtype();
						if(itemtype.equalsIgnoreCase(GenericConstants.RITM_MNS_NEW)){
						sriJsonValue.put(GenericConstants.RITM_MNS_NEW_DISPALY);
						}
						else{
							sriJsonValue.put(itemtype);
						}
							
						sriJsonValue.put(serviceRequestItem.getSubcategory());
						sriJsonValue.put(serviceRequestItem.getProjecttype());
						sriJsonValue.put(serviceRequestItem.getStatus());
						if(serviceRequestItem.getLastupdate()!=null){
							SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							String lastUpdateString= simpleDateFormat.format(serviceRequestItem.getLastupdate());
							sriJsonValue.put(lastUpdateString);
						}
						else{
							sriJsonValue.put("");
						}
						sriJsonValue.put("<a href=\"javascript:void(0)\" class=\"drillDownArrow\" onclick=\"getSingleServiceRequestItemDetails(this)\" id=\""+serviceRequestItem.getItemid()+"\"></a>");
						
						sriJsonArray.put(sriJsonValue);
					}
					
					com.liferay.portal.kernel.json.JSONObject childJson = JSONFactoryUtil.createJSONObject();
					
					childJson.put("draw", 0);
					childJson.put("recordsFiltered", sriJsonArray.length());
					childJson.put("recordsTotal", sriJsonArray.length());
					childJson.put("data", sriJsonArray);
					
					parentJsonValue.put("datatableServiceRequestItems", childJson);
					
					
					log.info("JSON for SR details--"+parentJsonValue.toString());
					out.print(parentJsonValue.toString());
				}
				
				else{
					out.print("No data in SR");
				}
				log.info("Population completed for SR details");
			}
	    	catch (Exception e) {
				e.printStackTrace();
			}

	 }
	 
	 @ResourceMapping(value = "getServiceRequestItemDetails")
		public void getServiceRequestItemDetails(ResourceRequest req,
				ResourceResponse res, Model model) {
			log.info("Controller for SRI details");

			try {

				HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
				HttpServletResponse response = PortalUtil
						.getHttpServletResponse(res);

				/*ThemeDisplay td = (ThemeDisplay) request
						.getAttribute(WebKeys.THEME_DISPLAY);*/
				String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(request, response);
				log.info("setting Cust ID");

				org.json.JSONObject obj = new JSONObject(
						request.getParameter("serviceRequestItemFilterIdentifier"));

				String itemId = obj.getString("itemid");

				com.liferay.portal.kernel.json.JSONArray resultJsonArray = JSONFactoryUtil
						.createJSONArray();

				PrintWriter out;
				out = response.getWriter();

				ServiceRequestItem serviceRequestItem= pvSRManager.getServiceRequestItemDetailsForItemId(itemId);
				
				ServiceRequest serviceRequest=null;
				if(serviceRequestItem!=null){
					
					List<ServiceRequestItemNotes> comments=serviceRequestItem.getServiceRequestItemNotesList();
					
					List<ServiceRequestItemDevices> devices= serviceRequestItem.getServiceRequestItemDevicesList();
					
					com.liferay.portal.kernel.json.JSONObject parentJsonValue = JSONFactoryUtil.createJSONObject();		
					parentJsonValue.put("ritmId", serviceRequestItem.getItemid());
					parentJsonValue.put("serviceName", serviceRequestItem.getServicename());
					parentJsonValue.put("siteName", serviceRequestItem.getSitename());
					
					com.liferay.portal.kernel.json.JSONArray deviceNameArray = JSONFactoryUtil
							.createJSONArray();
					
					for (ServiceRequestItemDevices serviceRequestItemDevices : devices) {
						deviceNameArray.put(serviceRequestItemDevices.getCiname());
					}
					
					parentJsonValue.put("deviceNameList", deviceNameArray);
					
					com.liferay.portal.kernel.json.JSONArray commentsArray = JSONFactoryUtil
							.createJSONArray();
					
					for (ServiceRequestItemNotes serviceRequestItemsNotes : comments) {
						com.liferay.portal.kernel.json.JSONObject commentBodyJson = JSONFactoryUtil.createJSONObject();
						if(serviceRequestItemsNotes.getUsername()!=null){
							commentBodyJson.put("userId", serviceRequestItemsNotes.getUsername());
						}
						else{
							commentBodyJson.put("userId", " ");
						}
						
						if(serviceRequestItemsNotes.getModifiedtime()!=null){
							SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							String modifiedDateString= simpleDateFormat.format(serviceRequestItemsNotes.getModifiedtime());
							commentBodyJson.put("modifiedDate", modifiedDateString);
						}
						else{
							commentBodyJson.put("modifiedDate", "");
						}
						
						commentBodyJson.put("notes", serviceRequestItemsNotes.getCustinfonotes());
						
						commentsArray.put(commentBodyJson);
						
					}
					
					parentJsonValue.put("commentsList", commentsArray);

					
					log.info("SRI details JSON--"+parentJsonValue.toString());
					out.print(parentJsonValue.toString());
				    
					
				}
				
				else{
					out.print("No data");
				}
				
				log.info("Population completed for SRI details");


			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
}
