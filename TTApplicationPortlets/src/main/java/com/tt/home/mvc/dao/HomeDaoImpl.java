package com.tt.home.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;


@Repository(value = "homeDaoImpl")
@Transactional
public class HomeDaoImpl {

	
	
	@Autowired
	private SessionFactory sessionFactory;
	private final static Logger log = Logger.getLogger(HomeDaoImpl.class);
	
	public List<String> getSubscribedServices(String customerId) {
		
		Query query= sessionFactory.getCurrentSession().createSQLQuery("select distinct(rootProductId) from configurationitem where rootProductId is not null and rootProductId <> '' and customeruniqueid='"+customerId+"' group by rootProductId having count(*) > 0;");
		
		List<String> subscribedList=query.list();
		return subscribedList;
	}
	
	
	public HashMap<String, String> getStatusBarDetails(String customerId,List<String> subscribedList){
		
	HashMap<String, String> colorNameToCodeMap= new HashMap<String, String>();
	colorNameToCodeMap.put("green", "#a3cf62");
	colorNameToCodeMap.put("amber", "#ff9c00");
	colorNameToCodeMap.put("red", "#e32212");
	
	HashMap<String, String> statusBarMap=new HashMap<String, String>();
	
	if(null!=subscribedList && subscribedList.size()>0){
		
		for (String service : subscribedList) {
			
			if(service.equalsIgnoreCase("MNS")){
				
				StringBuilder queryMNS= new StringBuilder();
				queryMNS.append("select inc.sitecolor from incddashboardmetrics inc");
				queryMNS.append(" where rootProductId='MNS' and incdcategory='MNS'");
				queryMNS.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled') and inc.priority in (select min(priority) from incddashboardmetrics");
				queryMNS.append(" where rootProductId='MNS' and incdcategory='MNS'");
				queryMNS.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled'))  and inc.customerimpacted in (select max(customerimpacted) from incddashboardmetrics");
				queryMNS.append(" where rootProductId='MNS' and incdcategory='MNS'");
				queryMNS.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled'))");
				
				
				StringBuilder queryNetwork= new StringBuilder();
				queryNetwork.append("select inc.sitecolor from incddashboardmetrics inc");
				queryNetwork.append(" where rootProductId='MNS' and incdcategory='Network'");
				queryNetwork.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled') and inc.priority in (select min(priority) from incddashboardmetrics");
				queryNetwork.append(" where rootProductId='MNS' and incdcategory='Network'");
				queryNetwork.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled')) and inc.customerimpacted in (select max(customerimpacted) from incddashboardmetrics");
				queryNetwork.append(" where rootProductId='MNS' and incdcategory='Network'");
				queryNetwork.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled'))");
				
				Query queryM=sessionFactory.getCurrentSession().createSQLQuery(queryMNS.toString());
				Query queryN=sessionFactory.getCurrentSession().createSQLQuery(queryNetwork.toString());
				
				List<String> mnsList=queryM.list();
				List<String> networkList=queryN.list();
				
				if(null!=mnsList && mnsList.size()>0){
					
					String color=mnsList.get(0);
					
					if (null!=color && color.length()>0) {
						System.out.println("Mns color: " + color);
						statusBarMap.put("mns",
								colorNameToCodeMap.get(color.toLowerCase()));
					}else{
						statusBarMap.put("mns",colorNameToCodeMap.get("green"));
					}
				}
				else{
					statusBarMap.put("mns",colorNameToCodeMap.get("green"));
				}
				
				
				if(null!=networkList && networkList.size()>0){
					String color=networkList.get(0);
					
					if (null!=color && color.length()>0) {
					
					System.out.println("Network color: "+color);

					statusBarMap.put("network", colorNameToCodeMap.get(color.toLowerCase()));
					}else{
						statusBarMap.put("network",colorNameToCodeMap.get("green"));
					}
				}
				else{
					
					statusBarMap.put("network",colorNameToCodeMap.get("green"));
				}
			} else if(service.equalsIgnoreCase("SaaS")){
				
				String productIdQuery = "select distinct(productname) from producttype where rootProductname = 'SaaS'";
				Query productIdQueryList = sessionFactory.getCurrentSession().createSQLQuery(productIdQuery.toString());
				
				List<String> saasService = productIdQueryList.list();
				

				StringBuilder queryService= new StringBuilder();
				queryService.append("select distinct(inc.sitecolor) from incddashboardmetrics inc");
				queryService.append(" where rootProductId in :productid");
				queryService.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled') and inc.priority in (select min(priority) from incddashboardmetrics");
				queryService.append(" where rootProductId in :productid");
				queryService.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled')) and inc.customerimpacted in (select max(customerimpacted) from incddashboardmetrics");
				queryService.append(" where rootProductId in :productid");
				queryService.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled'))");
				
				
				
				Query query=sessionFactory.getCurrentSession().createSQLQuery(queryService.toString());
				query.setParameterList("productid", saasService);			
				
				List<String> serviceList=query.list();
				
				if(null!=serviceList && serviceList.size()>0){
					String color=serviceList.get(0);
					if (null!=color && color.length()>0){
						System.out.println(service+" color: "+color);
						statusBarMap.put("saas", colorNameToCodeMap.get(color.toLowerCase()));
					} else {
						statusBarMap.put("saas", colorNameToCodeMap.get("green"));
					}
				} else {
					statusBarMap.put("saas", colorNameToCodeMap.get("green"));
				}
			} else{
				StringBuilder queryService= new StringBuilder();
				queryService.append("select distinct(inc.sitecolor) from incddashboardmetrics inc");
				queryService.append(" where rootProductId='"+service+"'");
				queryService.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled') and inc.priority in (select min(priority) from incddashboardmetrics");
				queryService.append(" where rootProductId='"+service+"'");
				queryService.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled')) and inc.customerimpacted in (select max(customerimpacted) from incddashboardmetrics");
				queryService.append(" where rootProductId='"+service+"'");
				queryService.append(" and customeruniqueid='"+customerId+"' and incidentstatus not in('Closed','Cancelled','Completed','Closed Cancelled'))");
				
				Query query=sessionFactory.getCurrentSession().createSQLQuery(queryService.toString());

				List<String> serviceList=query.list();
				
				if(null!=serviceList && serviceList.size()>0){
					
					String color=serviceList.get(0);
					if (null!=color && color.length()>0) 
					{
						System.out.println(service+" color: "+color);
						
						if(service.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_PRIVATECLOUD))
						{
							service = "cloud";
						}
						statusBarMap.put(service.toLowerCase(), colorNameToCodeMap.get(color.toLowerCase()));
					}
					else
					{
						if(service.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_PRIVATECLOUD))
						{
							service = "cloud";
						}
						statusBarMap.put(service.toLowerCase(),colorNameToCodeMap.get("green"));
					}
				}
				else
				{
					if(service.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_PRIVATECLOUD))
					{
						service = "cloud";
					}
					statusBarMap.put(service.toLowerCase(),colorNameToCodeMap.get("green"));
				}
			}
		}
	}
		
		return statusBarMap;
	}
	
}
