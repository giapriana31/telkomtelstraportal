package com.tt.home.mvc.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.home.mvc.dao.HomeDaoImpl;
import com.tt.reports.mvc.dao.ReportsDaoImpl;


@Service(value = "homeManager")
@Transactional
public class HomeManagerImpl {
	
	private HomeDaoImpl homeDaoImpl;

	@Autowired
	@Qualifier("homeDaoImpl")
	public void setHomeDaoImpl(HomeDaoImpl homeDaoImpl) {
		this.homeDaoImpl = homeDaoImpl;
	}
	
	
	public HashMap<String, String> getStatusBarDetails(String customerId){
		
		
		List<String> serviceList= homeDaoImpl.getSubscribedServices(customerId);
		return homeDaoImpl.getStatusBarDetails(customerId, serviceList);
	}

}
