package com.tt.home.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.home.mvc.dao.HomeDaoImpl;
import com.tt.home.mvc.service.HomeManagerImpl;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("homeController")
@RequestMapping("VIEW")
public class HomeController {
	
	
	private HomeManagerImpl homeManagerImpl;

	@Autowired
	@Qualifier("homeManager")
	public void setHomeManagerImpl(HomeManagerImpl homeManagerImpl) {
		this.homeManagerImpl = homeManagerImpl;
	}
	
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(renderResponse);
	
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		System.out.println("cust: "+customerId);
		HashMap<String, String> statusBarDetails=homeManagerImpl.getStatusBarDetails(customerId);
		
		JSONObject json = new JSONObject();
		json.putAll(statusBarDetails);
		
		
		model.addAttribute("statusBarMap", json.toString());

		Set<String> keyset=statusBarDetails.keySet();
		
		for (String string : keyset) {
			
			System.out.println("Key: "+string+" value: "+statusBarDetails.get(string));
		}
		
		return "home";
	}

}
