package com.tt.networkdetail.mvc.service.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.model.Site;

import com.tt.networkdetail.mvc.dao.NetworkDetailDaoImpl;

@Service(value="networkDetailManagerImpl")
@Transactional
public class NetworkDetailManagerImpl {

	private final static Logger log = Logger.getLogger(NetworkDetailManagerImpl.class);
	private NetworkDetailDaoImpl networkDetailDaoImpl;

	@Autowired
	@Qualifier("networkDetailDaoImpl")
	public void setNetworkDetailDaoImpl(
			NetworkDetailDaoImpl networkDetailDaoImpl) {
	    this.networkDetailDaoImpl = networkDetailDaoImpl;
	}
	

	public Map<String,Long > getNetworkDetails(String customerId) {
		log.debug("NetworkDetailManagerImpl * getNetworkDetails * Inside the method");
		try{
		Map<String,Long> networkDetailsMap = new HashMap<String,Long>();
		Long customerImpactedIncidentsCount = networkDetailDaoImpl.getCustomerImpactedIncidentsCount(customerId);
		Long atRiskIncidentsCount = networkDetailDaoImpl.getAtRiskIncidentsCount(customerId);
		Map<String,Integer> colorMap = getNetworkColorGraph(customerId);
		
		int redSitesCount = colorMap.get("Red") != null ? colorMap.get("Red") : 0;
		int amberSitesCount = colorMap.get("Amber") !=null ? colorMap.get("Amber") : 0;
		int greenSitesCount = colorMap.get("Green") !=null ? colorMap.get("Green") : 0;
		
		
		log.debug("NetworkDetailManagerImpl * getNetworkDetails * red"+ redSitesCount+ "ambar"+ amberSitesCount+ "green" +greenSitesCount);
		
		networkDetailsMap.put("customerImpactedIncidentsCount",customerImpactedIncidentsCount);
		networkDetailsMap.put("atRiskIncidentsCount",atRiskIncidentsCount);
		networkDetailsMap.put("redSitesCount",(long)redSitesCount);
		networkDetailsMap.put("amberSitesCount",(long)amberSitesCount);
		networkDetailsMap.put("greenSitesCount",(long)greenSitesCount);
		
		return networkDetailsMap;
	}catch (Exception e) {
	    log.error("Exception in getNetworkDetails method "+e.getMessage());
	}	
	log.debug("NetworkDetailManagerImpl * getNetworkDetails * Exit the method");
	return null;
	}
	
	public  Map<String,Integer >  getNetworkColorGraph(String customerId) {
		log.debug("NetworkDetailManagerImpl * getNetworkColorGraph * Inside the method");
		try{
		List<Site> sites = new ArrayList<Site>();
		sites = networkDetailDaoImpl.getSites(customerId);
		List<String> siteMapped = new ArrayList<String>();
		List<IncidentDashboardMapping> incidentList = networkDetailDaoImpl.getNetworkColorGraph(customerId);
//		System.out.println("******************** Incident count Manager Impl "+ incidentList.size());
//		System.out.println("*******site count Manger Impl "+ sites.size());
		Map<String, String> siteColorMap = new HashMap<String, String>();
		Map<String, Integer> colorCountMap = new HashMap<String, Integer>();
	
		if(incidentList!=null && incidentList.size()>0){

			for (IncidentDashboardMapping incidentDashboardMapping : incidentList) {
				if (siteColorMap.containsKey(incidentDashboardMapping.getSitename())) {

					if (isCurrentSiteGreater(incidentDashboardMapping.getSitecolor(), siteColorMap.get(incidentDashboardMapping.getSitename()))) {
						
						String oldColorCode = siteColorMap.get(incidentDashboardMapping.getSitename());
						if(colorCountMap.containsKey(oldColorCode)){
							colorCountMap.put(oldColorCode, colorCountMap.get(oldColorCode)-1);
						}
						if(colorCountMap.containsKey(incidentDashboardMapping.getSitecolor())){
							colorCountMap.put(incidentDashboardMapping.getSitecolor(), colorCountMap.get(incidentDashboardMapping.getSitecolor())+1);	
						}else{
							colorCountMap.put(incidentDashboardMapping.getSitecolor(), 1);
						}
						siteColorMap.put(incidentDashboardMapping.getSitename(), incidentDashboardMapping.getSitecolor());
					}
				} else {
					
					siteMapped.add(incidentDashboardMapping.getSitename());
					siteColorMap.put(incidentDashboardMapping.getSitename(), incidentDashboardMapping.getSitecolor());

					if(colorCountMap.containsKey(incidentDashboardMapping.getSitecolor())){
						
						colorCountMap.put(incidentDashboardMapping.getSitecolor(), colorCountMap.get(incidentDashboardMapping.getSitecolor())+1);	
					}else{
						colorCountMap.put(incidentDashboardMapping.getSitecolor(), 1);
					}
				}
			}
		}
		
		if(sites!=null && sites.size()>0){
			for (Site site : sites) {
			
				if (siteMapped != null	&& !siteMapped.contains(site.getCustomersite())) {
						if (colorCountMap.containsKey("Green")) {
							colorCountMap.put("Green", colorCountMap.get("Green") + 1);
						} else {
							colorCountMap.put("Green", 1);
						}
				}
			}
		}
	 return colorCountMap;	
	}catch (Exception e) {
	    log.error("Exception in getNetworkColorGraph method "+e.getMessage());
	}	
	log.debug("NetworkDetailManagerImpl * getNetworkColorGraph * Exit the method");
	return null;
	}

	private boolean isCurrentSiteGreater(String currentSitecolor, String oldSitecolor) {
		log.debug("NetworkDetailManagerImpl * isCurrentSiteGreater * Inside the method");
		try{
		if(getPriority(currentSitecolor)>getPriority(oldSitecolor)){
			return true;
		}		
		log.debug("NetworkDetailManagerImpl * isCurrentSiteGreater * Exit the method");
		return false;
	}catch (Exception e) {
	    log.error("Exception in isCurrentSiteGreater method "+e.getMessage());
	    throw e;
	}	
}
	
	
	public int getPriority(String siteColor) {
		log.debug("NetworkDetailManagerImpl * getPriority * Inside the method");
		try{
		if("Red".equalsIgnoreCase(siteColor)){
			return 3;
		} else if("Green".equalsIgnoreCase(siteColor)){
			return 1;
		} else if("Amber".equalsIgnoreCase(siteColor)){
			return 2;
		}
		log.debug("NetworkDetailManagerImpl * getPriority * Exit the method");
		return 1;
	}catch (Exception e) {
	    log.error("Exception in getPriority method "+e.getMessage());
	    throw e;
	}	
	}
}