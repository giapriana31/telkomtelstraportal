package com.tt.networkdetail.mvc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.model.Site;



@Repository(value="networkDetailDaoImpl")
@Transactional
public class NetworkDetailDaoImpl {
	private final static Logger log = Logger.getLogger(NetworkDetailDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	public String getcommonDBCall(){
		log.debug("NetworkDetailDaoImpl * getcommonDBCall * Inside the method");
		try{
		Query query = sessionFactory.getCurrentSession().createQuery("select i.category from Incident i where i.assigneeName = 'Vidya'");
		return (String) query.list().get(0);
	}catch (Exception e) {
	    log.error("Exception in getcommonDBCall method "+e.getMessage());
	}	
	log.debug("NetworkDetailDaoImpl * getcommonDBCall * Exit the method");
	return null;
	}
	
	@SuppressWarnings("unchecked")
	public Long getCustomerImpactedIncidentsCount(String customerId) {
		log.debug("NetworkDetailDaoImpl * getCustomerImpactedIncidentsCount * Inside the method");
		try{
		StringBuilder hql = new StringBuilder();
		
		hql.append("select count(incidentid) from IncidentDashboardMapping idm");
		hql.append(" where lower(idm.incdcategory) = :incdcategory and idm.customeruniqueid =:customerId and ");
		hql.append(" customerimpacted = '1' and lower(idm.incidentstatus) not in ('cancelled','closed')");
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		query.setString("incdcategory", "network");
		
		if( query.list()!=null &&  query.list().size()>0){
			Long customerimpactedcount =  (Long) query.list().get(0);
			return customerimpactedcount;
			
		}else{
			Long customerimpactedcount = (long) 0;
			return customerimpactedcount ;
		}
		}catch (Exception e) {
		    log.error("Exception in getCustomerImpactedIncidentsCount method "+e.getMessage());
		}
		log.debug("NetworkDetailDaoImpl * getCustomerImpactedIncidentsCount * Exit the method");
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Long getAtRiskIncidentsCount(String customerId) {
		log.debug("NetworkDetailDaoImpl * getAtRiskIncidentsCount * Inside the method");
		try{
		StringBuilder hql = new StringBuilder();
		
		hql.append("select count(incidentid) from IncidentDashboardMapping idm");
		hql.append(" where lower(idm.incdcategory) = :incdcategory and idm.customeruniqueid =:customerId and ");
		hql.append(" customerimpacted = '0' and lower(idm.incidentstatus) not in ('cancelled','closed')");
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		query.setString("incdcategory", "network");
		if( query.list()!=null &&  query.list().size()>0){
			Long customerimpactedcount =  (Long) query.list().get(0);
			return customerimpactedcount;
			
		}else{
			 Long customerimpactedcount = (long) 0;
			return customerimpactedcount;
		}
		}catch (Exception e) {
		    log.error("Exception in getAtRiskIncidentsCount method "+e.getMessage());
		}	
		log.debug("NetworkDetailDaoImpl * getAtRiskIncidentsCount * Exit the method");
		return null;
	}
	
	


	@SuppressWarnings("unchecked")
	public List<IncidentDashboardMapping> getNetworkColorGraph(
			String customerId) {
		log.debug("NetworkDetailDaoImpl * getNetworkColorGraph * Inside the method");
		try{
		StringBuilder hql = new StringBuilder();
		hql.append("from IncidentDashboardMapping idm");
		hql.append(" where lower(idm.incdcategory) <> :incdcategory and idm.customeruniqueid =:customerId and ");
		hql.append(" lower(idm.incidentstatus) not in ('cancelled','closed') and rootproductid = 'MNS'");
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		query.setString("incdcategory", "network");
		
		log.debug("NetworkDetailDaoImpl * getNetworkColorGraph * ResultSet size"+ query.list().size());
		log.debug("NetworkDetailDaoImpl * getNetworkColorGraph * Exit the method");
		if( query.list()!=null &&  query.list().size()>0){
			return query.list();	
		}else{
			return null;
		}
		}catch (Exception e) {
		    log.error("Exception in getNetworkColorGraph method "+e.getMessage());
		    throw e;
		}	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Site> getSites(String customerId) {
		try{
		log.debug("NetworkDetailDaoImpl * getSites * Inside the method");
		StringBuilder hql = new StringBuilder();
		
		hql.append("from Site sa ");
        hql.append("where sa.customeruniqueid=:customerId and sa.isManaged <>'No' and sa.isDataCenter <> 'Yes' and sa.servicetier <> 'Fully Managed' and sa.status in ('");

		
		//hql.append("from Site sa ");
		//hql.append("where sa.customeruniqueid=:customerId and sa.status in ('");
	    hql.append(GenericConstants.STATUSCOMMISSIONING);
	    hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
	    hql.append(GenericConstants.STATUSOPERATIONAL);
	    hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		
		log.debug("NetworkDetailDaoImpl * getSites * Result"+ query.list().size());
		if( query.list()!=null &&  query.list().size()>0){
			return query.list();	
		}else{
			return null;
		}
	
	}catch (Exception e) {
	    log.error("Exception in getSites method "+e.getMessage());
	}	
	log.debug("NetworkDetailDaoImpl * getSites * Exit the method");
	return null;
}
}
	
