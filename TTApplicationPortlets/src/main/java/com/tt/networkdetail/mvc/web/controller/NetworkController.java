package com.tt.networkdetail.mvc.web.controller;


import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.networkdetail.mvc.service.common.NetworkDetailManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("networkMapping")
@RequestMapping("VIEW")
public class NetworkController {
	private final static Logger log = Logger.getLogger(NetworkController.class);
	private NetworkDetailManagerImpl  networkDetailManagerImpl;
	
	
	@Autowired
	@Qualifier("networkDetailManagerImpl")
	public void setNetworkDetailManagerImpl(
			NetworkDetailManagerImpl networkDetailManagerImpl) {
		this.networkDetailManagerImpl = networkDetailManagerImpl;
	}
	
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		log.debug("NetworkController * handleRenderRequest * Inside the method");
		try{
			
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		//User user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), themeDisplay.getUser().getDisplayEmailAddress());
		String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(renderResponse));
		log.debug("NetworkController * handleRenderRequest * customerId--------------->>"+customerId);	
		
		int count = SubscribedService.isSubscribedServices(GenericConstants.PRODUCTTYPE_MNS, customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, "NETWORK");
			return "unsubscribed_mns_service";
		}
		else
		{
			Map<String, Long> networkDetailsMap = networkDetailManagerImpl.getNetworkDetails(customerId);
			
			for (String key : networkDetailsMap.keySet()) {
				
				model.addAttribute(key, networkDetailsMap.get(key));
			}
			Gson gson = new Gson();
			String ttNetworkDetailMap = gson.toJson(networkDetailsMap);
				
			model.addAttribute("ttNetworkDetailMap", ttNetworkDetailMap);
			
			log.debug("NetworkController * handleRenderRequest * ttNetworkDetailMap ******* : " + ttNetworkDetailMap);
			log.debug("NetworkController * handleRenderRequest * Exit the method");
			return "networkDetalList";
		}
	}catch (Exception e) {
	    log.error("Exception in handleRenderRequest method "+e.getMessage());
	    throw e;
	}	
	}
}
