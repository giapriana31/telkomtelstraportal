package com.tt.pvsiteslevel0.mvc.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.pvsiteslevel0.mvc.model.GraphDetails;
import com.tt.pvsiteslevel0.mvc.service.common.PVSitesLevel0Manager;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("pvSitesLevel0Controller")
@RequestMapping("VIEW")
public class PVSitesLevel0Controller {
	
	private final static Logger log = Logger.getLogger(PVSitesLevel0Controller.class);
	public static Properties prop = PropertyReader.getProperties();
	private PVSitesLevel0Manager pvSitesLevel0Manager;
	
	
	@Autowired
	@Qualifier(value="pvSitesLevel0Manager")
	public void setPvSitesLevel0Manager(PVSitesLevel0Manager pvSitesLevel0Manager) {
		this.pvSitesLevel0Manager = pvSitesLevel0Manager;
	}



	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {

    	log.debug("PVSitesLevel0Controller * handleRenderRequest * Inside the method");

		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		//String customerId="TT001";
		SubscribedService ss = new SubscribedService();

		int serviceCount = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_MNS, customerId);

		if(serviceCount == 0)
		{
			model.addAttribute(GenericConstants.TITLE, "SITES");
			return "unsubscribed_mns_service";
		}
		else
		{
			ArrayList<GraphDetails> details=pvSitesLevel0Manager.getGraphDetails(customerId);
			Map<String,Integer> colorToDeliveryStageMap= new HashMap<String, Integer>();
			colorToDeliveryStageMap.put("#89C35C", 6);
			colorToDeliveryStageMap.put("#FA8072", 5);
			colorToDeliveryStageMap.put("#00B0F0", 4);
			colorToDeliveryStageMap.put("#7030A0", 3);
			colorToDeliveryStageMap.put("#A40800", 2);
			colorToDeliveryStageMap.put("#000", 1);
			
			Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
			deliveryStageToColor.put(6, "#89C35C");
			deliveryStageToColor.put(5, "#FA8072");
			deliveryStageToColor.put(4, "#00B0F0");
			deliveryStageToColor.put(3, "#7030A0");
			deliveryStageToColor.put(2, "#A40800");
			deliveryStageToColor.put(1, "#000");
			

			Map<String, Integer> silverColor = new HashMap<String, Integer>();
			Map<String, Integer> goldColor = new HashMap<String, Integer>();
			Map<String, Integer> bronzeColor = new HashMap<String, Integer>();
			Map<String, Integer> silverColorTempMap = new HashMap<String, Integer>();
			Map<String, Integer> goldColorTempMap = new HashMap<String, Integer>();
			Map<String, Integer> bronzeColorTempMap = new HashMap<String, Integer>();
			
			if (null != details && details.size() > 0) {
				for (GraphDetails graphDetails : details) {
					if (null != graphDetails.getDelivery_Stage()
							&& graphDetails.getServiceTier().equalsIgnoreCase(
									"gold")) {
						if (null != graphDetails.getSiteId()
								&& goldColorTempMap.keySet().contains(
										graphDetails.getSiteId())) {
							Integer deliveryStage = goldColorTempMap
									.get(graphDetails.getSiteId());
							if (null != deliveryStage
									&& null != graphDetails.getDelivery_Stage()
									&& deliveryStage > Integer
											.parseInt(graphDetails
													.getDelivery_Stage())) {
								goldColorTempMap.put(graphDetails.getSiteId(),
										Integer.parseInt(graphDetails
												.getDelivery_Stage()));
							}
						} else {
							goldColorTempMap.put(graphDetails.getSiteId(), Integer
									.parseInt(graphDetails.getDelivery_Stage()));
						}
					} else if (null != graphDetails.getDelivery_Stage()
							&& graphDetails.getServiceTier().equalsIgnoreCase(
									"silver")) {
						if (null != graphDetails.getSiteId()
								&& silverColorTempMap.keySet().contains(
										graphDetails.getSiteId())) {
							Integer deliveryStage = silverColorTempMap
									.get(graphDetails.getSiteId());
							if (null != deliveryStage
									&& null != graphDetails.getDelivery_Stage()
									&& deliveryStage > Integer
											.parseInt(graphDetails
													.getDelivery_Stage())) {
								silverColorTempMap.put(graphDetails.getSiteId(),
										Integer.parseInt(graphDetails
												.getDelivery_Stage()));
							}
						} else {
							silverColorTempMap.put(graphDetails.getSiteId(),
									Integer.parseInt(graphDetails
											.getDelivery_Stage()));
						}
					} else if (null != graphDetails.getDelivery_Stage()
							&& graphDetails.getServiceTier().equalsIgnoreCase(
									"bronze")) {
						if (null != graphDetails.getSiteId()
								&& bronzeColorTempMap.keySet().contains(
										graphDetails.getSiteId())) {
							Integer deliveryStage = bronzeColorTempMap
									.get(graphDetails.getSiteId());
							if (null != deliveryStage
									&& null != graphDetails.getDelivery_Stage()
									&& deliveryStage > Integer
											.parseInt(graphDetails
													.getDelivery_Stage())) {
								bronzeColorTempMap.put(graphDetails.getSiteId(),
										Integer.parseInt(graphDetails
												.getDelivery_Stage()));
							}
						} else {
							bronzeColorTempMap.put(graphDetails.getSiteId(),
									Integer.parseInt(graphDetails
											.getDelivery_Stage()));
							
						}
					}
				}

				Set<String> goldKeySet = goldColorTempMap.keySet();
				Set<String> silverKeySet = silverColorTempMap.keySet();
				Set<String> bronzeKeySet = bronzeColorTempMap.keySet();

				for (String string : goldKeySet) {
					if (goldColor.keySet().contains(
							deliveryStageToColor.get(goldColorTempMap.get(string)))) {
						Integer count = goldColor.get(deliveryStageToColor
								.get(goldColorTempMap.get(string)));
						count++;
						goldColor.put(deliveryStageToColor.get(goldColorTempMap
								.get(string)), count);
					} else {
						goldColor.put(deliveryStageToColor.get(goldColorTempMap
								.get(string)), 1);
					}
				}
				for (String string : silverKeySet) {
					if (silverColor.keySet()
							.contains(
									deliveryStageToColor.get(silverColorTempMap
											.get(string)))) {
						Integer count = silverColor.get(deliveryStageToColor
								.get(silverColorTempMap.get(string)));
						count++;
						silverColor.put(deliveryStageToColor.get(silverColorTempMap
								.get(string)), count);
					} else {
						silverColor.put(deliveryStageToColor.get(silverColorTempMap
								.get(string)), 1);
					}
				}
				for (String string : bronzeKeySet) {
				
					if (bronzeColor.keySet()
							.contains(
									deliveryStageToColor.get(bronzeColorTempMap
											.get(string)))) {
						Integer count = bronzeColor.get(deliveryStageToColor
								.get(bronzeColorTempMap.get(string)));
						count++;
						bronzeColor.put(deliveryStageToColor.get(bronzeColorTempMap
								.get(string)), count);
					} else {
						bronzeColor.put(deliveryStageToColor.get(bronzeColorTempMap
								.get(string)), 1);
				
					}

				}

			}
			
			

			
			Gson gson = new Gson();
			List<String> colorList=new ArrayList<String>();
		
			
			
		colorList.add("#89C35C");
		colorList.add("#FA8072");
		colorList.add("#00B0F0");
		colorList.add("#7030A0");
		colorList.add("#A40800");
		colorList.add("#000");

			
		
		for (String string : colorList) {
			if(null != goldColor && !goldColor.keySet().contains(string)){
				goldColor.put(string, 0);
			}
			
			if(null != silverColor && !silverColor.keySet().contains(string)){
				
				silverColor.put(string, 0);
			}
			
			if(null != bronzeColor && !bronzeColor.keySet().contains(string)){
				
				bronzeColor.put(string, 0);
			}
			
		}
		
		
		
		
		
			Map<String, Integer> linkedGoldHasMap= new LinkedHashMap<String, Integer>();
			Map<String, Integer> linkedSilverHasMap= new LinkedHashMap<String, Integer>();
			Map<String, Integer> linkedBronzeHasMap= new LinkedHashMap<String, Integer>();
		
		
		
		
		
		
		
			String ttOfficeBuildingSilverColorMap = gson.toJson(silverColor);
			String ttOfficeBuildingGoldColorMap = gson.toJson(goldColor);
			String ttOfficeBuildingBronzeColorMap = gson.toJson(bronzeColor);
			String colorListJson=gson.toJson(colorList);
			ArrayList<String> graphBottom= new ArrayList<String>();
		//	HashMap<String, String> graphBottom=new HashMap<String, String>();
			
			
			graphBottom.add("Order Received-#000");
			graphBottom.add("Detailed Design-#A40800");
			graphBottom.add("Procurement-#7030A0");
			graphBottom.add("Delivery & Activation-#00B0F0");
			graphBottom.add("Monitoring-#FA8072");
			graphBottom.add("Operational-#89C35C");
			
			model.addAttribute("ttOfficeBuildingSilverColorMap", ttOfficeBuildingSilverColorMap);
			model.addAttribute("ttOfficeBuildingGoldColorMap", ttOfficeBuildingGoldColorMap);
			model.addAttribute("ttOfficeBuildingBronzeColorMap", ttOfficeBuildingBronzeColorMap);
			model.addAttribute("graphColorList", colorListJson);
			model.addAttribute("graphBottomList", graphBottom);
				
	    	log.debug("PVSitesLevel0Controller * handleRenderRequest * color list: "+colorListJson);
	    	log.debug("PVSitesLevel0Controller * handleRenderRequest * gold map: "+ttOfficeBuildingGoldColorMap);
	    	log.debug("PVSitesLevel0Controller * handleRenderRequest * silver map: "+ttOfficeBuildingSilverColorMap);
	    	log.debug("PVSitesLevel0Controller * handleRenderRequest * bronze map: "+ttOfficeBuildingBronzeColorMap);

	    	log.debug("PVSitesLevel0Controller * handleRenderRequest * exit method");

		
			return "pvSitesLevel0";
		}
	}
}


/*class OfficeBuildingSiteColorComparator implements Comparator<OfficeBuildingSiteColor> {
    @Override
    public int compare(OfficeBuildingSiteColor o1, OfficeBuildingSiteColor o2) {
    	Integer id1 = ((OfficeBuildingSiteColor) o1).get();
    	Integer id2 = ((OfficeBuildingSiteColor) o2).getId();

		return id2.compareTo(id1);
	}
}*/

