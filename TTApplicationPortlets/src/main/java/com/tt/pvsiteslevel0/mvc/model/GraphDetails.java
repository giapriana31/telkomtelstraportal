package com.tt.pvsiteslevel0.mvc.model;

public class GraphDetails {

	private String siteId;
	private String ciId;
	private String deliveryStatus;
	private String delivery_Stage;
	private String color;
	private String serviceTier;
	
	

	public String getServiceTier() {
		return serviceTier;
	}

	public void setServiceTier(String serviceTier) {
		this.serviceTier = serviceTier;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getCiId() {
		return ciId;
	}

	public void setCiId(String ciId) {
		this.ciId = ciId;
	}

	public String getDeliveryStatus() {
		return deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public String getDelivery_Stage() {
		return delivery_Stage;
	}

	public void setDelivery_Stage(String delivery_Stage) {
		this.delivery_Stage = delivery_Stage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
