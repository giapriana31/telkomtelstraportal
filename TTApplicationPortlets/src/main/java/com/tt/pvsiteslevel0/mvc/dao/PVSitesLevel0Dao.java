package com.tt.pvsiteslevel0.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvsiteslevel0.mvc.model.GraphDetails;
import com.tt.pvsiteslevel0.mvc.service.common.PVSitesLevel0Manager;

@Repository(value = "pvSitesLevel0Dao")
@Transactional
public class PVSitesLevel0Dao {

	
	private final static Logger log = Logger.getLogger(PVSitesLevel0Dao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public ArrayList<GraphDetails> getGraphDetails(String customerId) {
		
		Map<String,String> nameToDeliveryStageMap= new HashMap<String, String>();
		nameToDeliveryStageMap.put("Order Received", "1");
		nameToDeliveryStageMap.put("Detailed Design", "2");
		nameToDeliveryStageMap.put("Procurement", "3");
		nameToDeliveryStageMap.put("Delivery & Activation", "4");
		nameToDeliveryStageMap.put("Monitoring", "5");
		nameToDeliveryStageMap.put("Operational", "6");
		
		
		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(6, "#89C35C");
		deliveryStageToColor.put(5, "#FA8072");
		deliveryStageToColor.put(4, "#00B0F0");
		deliveryStageToColor.put(3, "#7030A0");
		deliveryStageToColor.put(2, "#A40800");
		deliveryStageToColor.put(1, "#000");
		
    	log.debug("PVSitesLevel0Dao * getGraphDetails * Inside the method");


		ArrayList<GraphDetails> graphDetails = null;

		SQLQuery query = sessionFactory
				.getCurrentSession()
				.createSQLQuery(
						"Select  s.siteid,s.servicetier,s.pvsitedeliverystatus from site s where isManaged <>'No' and isDataCenter <> 'Yes' and  s.customeruniqueid='"+ customerId + "' and s.rootProductId='MNS' ");

		// query.setString("customerId", customerId);
		
    	log.debug("PVSitesLevel0Dao * getGraphDetails * Query String: "+query.getQueryString());

		List<Object[]> detailList = query.list();

		if (detailList != null && !detailList.isEmpty()) {
	    	log.debug("PVSitesLevel0Dao * getGraphDetails * Is query List not null size: "+detailList.size());

			graphDetails = new ArrayList<GraphDetails>();

			for (Object[] details : detailList) {

				GraphDetails gDetails = new GraphDetails();

				gDetails.setSiteId((String) details[0]);
				gDetails.setServiceTier((String) details[1]);
				gDetails.setDeliveryStatus((String) details[2]);
				gDetails.setDelivery_Stage(nameToDeliveryStageMap.get((String) details[2]));
				
				gDetails.setColor(deliveryStageToColor.get(Integer.parseInt(nameToDeliveryStageMap.get((String) details[2]))));
				System.out.println("Color: "+deliveryStageToColor.get(Integer.parseInt(nameToDeliveryStageMap.get((String) details[2]))));
				graphDetails.add(gDetails);
				
			}

		}
		
    	log.debug("PVSitesLevel0Dao * getGraphDetails * exit");

		return graphDetails;
	}


}
