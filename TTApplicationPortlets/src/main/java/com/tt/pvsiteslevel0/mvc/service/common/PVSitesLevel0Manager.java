package com.tt.pvsiteslevel0.mvc.service.common;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvsiteslevel0.mvc.dao.PVSitesLevel0Dao;
import com.tt.pvsiteslevel0.mvc.model.GraphDetails;
import com.tt.pvsiteslevel0.mvc.web.controller.PVSitesLevel0Controller;

@Service(value = "pvSitesLevel0Manager")
@Transactional
public class PVSitesLevel0Manager {

	private PVSitesLevel0Dao pvSitesLevel0Dao;
	private final static Logger log = Logger.getLogger(PVSitesLevel0Manager.class);

	@Autowired
	@Qualifier(value = "pvSitesLevel0Dao")
	public void setPvSitesLevel0Dao(PVSitesLevel0Dao pvSitesLevel0Dao) {
		this.pvSitesLevel0Dao = pvSitesLevel0Dao;
	}

	public ArrayList<GraphDetails> getGraphDetails(String customerId) {
    	log.debug("PVSitesLevel0Manager * getGraphDetails * Inside the method");

		return pvSitesLevel0Dao.getGraphDetails(customerId);
	
	
	}
	

}
