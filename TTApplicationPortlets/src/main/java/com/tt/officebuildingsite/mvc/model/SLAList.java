package com.tt.officebuildingsite.mvc.model;

import java.util.List;

public class SLAList implements Comparable<SLAList>{

	private int id;
	private String siteTier;
	private String siteCount;
	private List<SLAColor> slaColorList;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiteTier() {
		return siteTier;
	}
	public void setSiteTier(String siteTier) {
		this.siteTier = siteTier;
	}
	public String getSiteCount() {
		return siteCount;
	}
	public void setSiteCount(String siteCount) {
		this.siteCount = siteCount;
	}
	public List<SLAColor> getSlaColorList() {
		return slaColorList;
	}
	public void setSlaColorList(List<SLAColor> slaColorList) {
		this.slaColorList = slaColorList;
	}
	
	@Override
	public  int compareTo(SLAList slaList) {
		int siteTierId = ((SLAList) slaList).getId();
		return (int) (siteTierId - this.id);
	}
	
	
}
