package com.tt.officebuildingsite.mvc.model;

import java.util.List;

public class SLAListCpe implements Comparable<SLAListCpe> {

	private int id;
	private String siteTier;
	private String siteCount;
	private List<SLAColorCpe> slaColorList;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiteTier() {
		return siteTier;
	}
	public void setSiteTier(String siteTier) {
		this.siteTier = siteTier;
	}
	public String getSiteCount() {
		return siteCount;
	}
	public void setSiteCount(String siteCount) {
		this.siteCount = siteCount;
	}
	public List<SLAColorCpe> getSlaColorList() {
		return slaColorList;
	}
	public void setSlaColorList(List<SLAColorCpe> slaColorList) {
		this.slaColorList = slaColorList;
	}
	@Override
	public int compareTo(SLAListCpe slaListCpe) {
		int siteTierId = ((SLAListCpe) slaListCpe).getId();
		return (int) (siteTierId - this.id);
	}
	

}
