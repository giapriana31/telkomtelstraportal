package com.tt.officebuildingsite.mvc.model;

public class SLAColorCpe implements Comparable<SLAColorCpe>{

	private int id;
	private String siteName;
	private String colorCode;
	private String sitePercentileAverage;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	public String getSitePercentileAverage() {
		return sitePercentileAverage;
	}
	public void setSitePercentileAverage(String sitePercentileAverage) {
		this.sitePercentileAverage = sitePercentileAverage;
	}
	@Override
	public int compareTo(SLAColorCpe slaColorCpe) {
		int priorityId = ((SLAColorCpe) slaColorCpe).getId();
		return (int) (priorityId - this.id);
	}
	
	
}
