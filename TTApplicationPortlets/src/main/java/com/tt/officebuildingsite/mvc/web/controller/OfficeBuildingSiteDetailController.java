package com.tt.officebuildingsite.mvc.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.officebuildingsite.mvc.model.OfficeBuildingSiteList;
import com.tt.officebuildingsite.mvc.model.SLAList;
import com.tt.officebuildingsite.mvc.model.SLAListCpe;
import com.tt.officebuildingsite.mvc.service.OfficeBuildingSiteManagerImpl;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("officeBuildingSiteDetail")
@RequestMapping("VIEW")
public class OfficeBuildingSiteDetailController {

    private final static Logger log = Logger.getLogger(OfficeBuildingSiteDetailController.class);
    private OfficeBuildingSiteManagerImpl officeBuildingSiteManager;
    public static Properties prop = PropertyReader.getProperties();
    
    @Autowired
    @Qualifier("officeBuildingSiteManager")
    public void setOfficeBuildingSiteManager(
	    OfficeBuildingSiteManagerImpl officeBuildingSiteManager) {
	this.officeBuildingSiteManager = officeBuildingSiteManager;
    }

    @RenderMapping
    public String handleRenderRequest(RenderRequest request,
	    RenderResponse renderResponse, Model model) throws PortalException,
	    SystemException {
    	log.debug("OfficeBuildingSiteDetailController * handleRenderRequest * Inside the method");
    try{
	log.debug("OfficeBuildingSiteDetailController * handleRenderRequest * Test Debug Code-------------");
	// Long customerId = 91919L;
	ThemeDisplay themeDisplay = (ThemeDisplay) request .getAttribute(WebKeys.THEME_DISPLAY);
//	User user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay .getCompanyId(), themeDisplay.getUser() .getDisplayEmailAddress());
	String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(renderResponse));
	
	log.debug("OfficeBuildingSiteDetailController * handleRenderRequest * customerId......." + customerId);

	List<OfficeBuildingSiteList> officeBuildingSiteLists = new ArrayList<OfficeBuildingSiteList>();
	List<SLAList> basedOnSLAList = new ArrayList<SLAList>();
	List<SLAListCpe> basedOnSLAListCpe = new ArrayList<SLAListCpe>();
	officeBuildingSiteLists = officeBuildingSiteManager.getOfficeBuildingSecondPage(customerId);
	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest *  Before setting List");

	OfficeBuildingSiteList goldSiteTier = new OfficeBuildingSiteList();
	goldSiteTier.setSiteCount("0");
	goldSiteTier.setSiteTier("Gold");
	OfficeBuildingSiteList silverSiteTier = new OfficeBuildingSiteList();
	silverSiteTier.setSiteCount("0");
	silverSiteTier.setSiteTier("Silver");
	OfficeBuildingSiteList bronzeSiteTier = new OfficeBuildingSiteList();
	bronzeSiteTier.setSiteCount("0");
	bronzeSiteTier.setSiteTier("Bronze");

	boolean isGoldAdded = false;
	boolean isSilverAdded = false;
	boolean isBronzeAdded = false;
	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest *  Is office Building list: "+officeBuildingSiteLists.isEmpty());

	if (officeBuildingSiteLists != null && officeBuildingSiteLists.size() > 0)
	    for (OfficeBuildingSiteList buildingSiteList : officeBuildingSiteLists) {
	    	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest * Inside sites Loop");

		if (buildingSiteList.getSiteCount() != null && buildingSiteList.getSiteCount().length() <= 1)
		    buildingSiteList.setSiteCount("0" + buildingSiteList.getSiteCount());
    	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest * Inside IF");

		if ("gold".equalsIgnoreCase(buildingSiteList.getSiteTier())) {
		    isGoldAdded = true;
		    goldSiteTier = buildingSiteList;
		} else if ("silver".equalsIgnoreCase(buildingSiteList.getSiteTier())) {
		    isSilverAdded = true;
		    silverSiteTier = buildingSiteList;
		} else if ("bronze".equalsIgnoreCase(buildingSiteList.getSiteTier())) {
		    isBronzeAdded = true;
		    bronzeSiteTier = buildingSiteList;
		}

	    }
	basedOnSLAList = officeBuildingSiteManager.getSitesList(customerId);
	basedOnSLAListCpe = officeBuildingSiteManager.getSitesListCpe(customerId);
	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest *  Is office sla list: "+basedOnSLAList.isEmpty());

	log.debug("OfficeBuildingSiteDetailController * handleRenderRequest * "+officeBuildingSiteLists + "=====officeBuildingSiteLists===");
	for (SLAList slaList : basedOnSLAList) {
    	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest * Inside sla Loop");

		log.debug("OfficeBuildingSiteDetailController * handleRenderRequest * slaList.getSiteCount()------------" + slaList.getSiteCount());
	    if (slaList.getSiteCount() == null || (slaList.getSiteCount() != null && slaList.getSiteCount().length() < 1)) {
	    	slaList.setSiteCount("0");
		} else if (slaList.getSiteCount() != null && slaList.getSiteCount().length() <= 1) {
			slaList.setSiteCount(slaList.getSiteCount());
		}
	}
	
	Gson gson = new Gson();
	
	String basedonincidentsjson = gson.toJson(officeBuildingSiteLists);
	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest *  incident json: "+basedonincidentsjson);

	String basedonslajson = gson.toJson(basedOnSLAList);
	log.debug("XYZDEBUG OfficeBuildingSiteDetailController * handleRenderRequest *  sla json: "+basedOnSLAList);

	//String basedonslajson = "[{\"id\":'x',\"siteTier\":\"Gold Sites\",\"siteCount\":\"01\",\"slaColorList\":[{\"id\":\"01\", \"siteName\":\"testsite\", \"colorCode\":\"red\", \"sitePercentileAverage\":\"30\"}]},{\"id\":'y',\"siteTier\":\"Silver Sites\",\"siteCount\":\"02\",\"slaColorList\":[{\"id\":\"02\", \"siteName\":\"testsite2\", \"colorCode\":\"amber\", \"sitePercentileAverage\":\"30\"}, {\"id\":\"02a\", \"siteName\":\"testsite2a\", \"colorCode\":\"green\", \"sitePercentileAverage\":\"30\"}]},{\"id\":'z',\"siteTier\":\"Bronze Sites\",\"siteCount\":\"01\",\"slaColorList\":[{\"id\":\"03\", \"siteName\":\"testsite3\", \"colorCode\":\"green\", \"sitePercentileAverage\":\"40\"}]}];"; 
	model.addAttribute("showTabBasedOn", "basedOnTicket");
	model.addAttribute("goldSiteTier", goldSiteTier);
	model.addAttribute("silverSiteTier", silverSiteTier);
	model.addAttribute("bronzeSiteTier", bronzeSiteTier);
	model.addAttribute("isGoldAdded", isGoldAdded);
	model.addAttribute("isSilverAdded", isSilverAdded);
	model.addAttribute("isBronzeAdded", isBronzeAdded);
	model.addAttribute("basedOnSLAList", basedOnSLAList);
	model.addAttribute("basedonincidentsjson", basedonincidentsjson);
	model.addAttribute("basedonslajson", basedonslajson);
	model.addAttribute("basedOnSLAListCpe",basedOnSLAListCpe);
	
	log.debug("OfficeBuildingSiteDetailController * handleRenderRequest * Exit the method");
	return "officeBuildingSiteDetail";
    }catch (Exception e) {
	    log.error("Exception in handleRenderRequest method "+e.getMessage());
	}	
    return null;
    }

    @ActionMapping(value = "viewSite")
    public void viewSite(ActionRequest actionRequest,
		    ActionResponse actionResponse) throws IOException, PortletException {
	    	log.debug("OfficeBuildingSiteDetailController * viewSite * Inside the method");
	    	try{
	    	String siteName = actionRequest.getParameter("siteName");
	    	String siteId = officeBuildingSiteManager.getSiteId(siteName);
	    	log.debug("OfficeBuildingSiteDetailController * viewSite * Site Id -----" + siteId);
	    	javax.xml.namespace.QName siteNameBean = new QName("http://localhost:8080/siteTicket", "siteName", "event");
	    	actionResponse.setEvent(siteNameBean, siteId);
	    	
			String url = prop.getProperty("tt.homeURL") + "/" + prop.getProperty("ttIncidentPageName");
			log.debug("OfficeBuildingSiteDetailController * viewSite * Exit the method");
			actionResponse.sendRedirect(url);
	    }catch (Exception e) {
		    log.error("Exception in viewSite method "+e.getMessage());	    
		}
	}
}
