package com.tt.officebuildingsite.mvc.model;



public class OfficeBuildingSiteColor  implements Comparable<OfficeBuildingSiteColor>{
	
	private int id;
	private String siteName;
	private String colorCode;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	@Override
	public  int compareTo(OfficeBuildingSiteColor buildingSiteColor) {
		int priorityId = ((OfficeBuildingSiteColor) buildingSiteColor).getId();
		return (int) (priorityId - this.id);
	}
	
}
