package com.tt.officebuildingsite.mvc.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;

/*import com.tt.model.Incident;*/
import com.tt.model.IncidentDashboardMapping;
import com.tt.model.SLAAvailability;
import com.tt.model.Site;
import com.tt.officebuildingsite.mvc.model.SLAColorCpe;
import com.tt.officebuildingsite.mvc.model.SLAListCpe;

@Repository(value="officeBuildingSiteDaoImpl")
@Transactional
public class OfficeBuildingSiteDaoImpl {
	private final static Logger log = Logger.getLogger(OfficeBuildingSiteDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public List<IncidentDashboardMapping> getOfficeBuildingSecondGraph(String customerId) {
		log.debug("OfficeBuildingSiteDaoImpl * getOfficeBuildingSecondGraph * Inside the method");
		try{
		StringBuilder hql = new StringBuilder();
		hql.append("from IncidentDashboardMapping idm");
		hql.append(" where idm.sitestatus='Operational' and lower(idm.incdcategory) <> :incdcategory and idm.customeruniqueid =:customerId and  ");
		hql.append(" lower(idm.incidentstatus) not in ('cancelled','closed') and idm.rootproductid='MNS' ");
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		query.setString("incdcategory", "network");
		if( query.list()!=null &&  query.list().size()>0){
			return query.list();	
		}else{
			return null;
		}
		}catch (Exception e) {
		    log.error("Exception in getOfficeBuildingSecondGraph method "+e.getMessage());
		}
		log.debug("OfficeBuildingSiteDaoImpl * getOfficeBuildingSecondGraph * Exit the method");
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<SLAAvailability> getBasedOnSLASitesList(String customerId) {
		log.debug("OfficeBuildingSiteDaoImpl * getBasedOnSLASitesList * Inside the method");
		try{
			
		Calendar calendar = Calendar.getInstance();
   	 	calendar.add(Calendar.MONTH, -1);
   	 	String monthString = new SimpleDateFormat("yyyy-MM").format(calendar.getTime());
   	 	log.debug("monthString :: " + monthString);	
			
		StringBuilder hql = new StringBuilder();
		hql.append("from SLAAvailability sa ");
		hql.append("where sa.customer_ID=:customerId and monthyear=:monthyear");
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		query.setParameter("monthyear", monthString);
		if( query.list()!=null &&  query.list().size()>0){
			return query.list();	
		}else{
			return null;
		}
		}catch (Exception e) {
		    log.error("Exception in getBasedOnSLASitesList method "+e.getMessage());
		}
		log.debug("OfficeBuildingSiteDaoImpl * getBasedOnSLASitesList * Exit the method");
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<SLAListCpe> getBasedOnSLASitesListCpe(String customerId){
		log.debug("OfficeBuildingSiteDaoImpl * getBasedOnSLASitesListCpe * Inside the method");

		List<SLAListCpe> basedOnSLASiteList= new ArrayList<SLAListCpe>();
		List<SLAColorCpe> goldSLAColor= new ArrayList<SLAColorCpe>();
		List<SLAColorCpe> silverSLAColor= new ArrayList<SLAColorCpe>();
		List<SLAColorCpe> bronzeSLAColor= new ArrayList<SLAColorCpe>();
		SLAListCpe goldSLA = new SLAListCpe();
		goldSLA.setSiteTier("Gold Device");		
		SLAListCpe silverSLA = new SLAListCpe();
		silverSLA.setSiteTier("Silver Device");
		SLAListCpe bronzeSLA = new SLAListCpe();
		bronzeSLA.setSiteTier("Bronze Device");
		int goldCount=0;
		int silverCount=0;
		int bronzeCount=0;
		
		StringBuffer sb = new StringBuffer();
		sb.append(" select *,(select case when total > 0.50 then 'red'");
		sb.append(" when total between 0.11 and 0.50 then 'amber' ");
		sb.append(" when total <= 0.10 then 'green' end) colour,");
		sb.append(" (select case when total > 0.50 then '1' ");
		sb.append(" when total between 0.11 and 0.50 then '2'");
		sb.append(" when total <= 0.10 then '3' end) priority from (");
		sb.append(" select *, (contractavailability - availability) total from (");
		sb.append(" select *, truncate( ( (((days * 24) - duration) / (days * 24)) * 100  ),2) availability from (");
		sb.append(" select ciname,servicetier,contractavailability,days,sum(duration) duration from (");
		sb.append(" select ciid,ciname,customeruniqueid,customersite,servicetier,contractavailability,incidentid,incidentstatus,taskbusinessduration,day(last_day(incidentcreated))days,duration");
		sb.append(" from sitesavailabilitycpe");
		sb.append(" where incidentcreated BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00') AND DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59')");
		sb.append(" and tasksla like '%Resolution%' and customeruniqueid = '"+customerId+"' ");
		sb.append(" ) a group by ciname,servicetier,contractavailability,days");
		sb.append(" )b");
		sb.append(" )c");
		sb.append(" )d");
		
		log.info("query for getBasedOnSLASitesListCpe" +sb.toString());
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] incidentcpe : (List<Object[]>)query.list()) {
				if (incidentcpe[0]!=null) {
					log.info("customersite ----------------"+incidentcpe[0].toString());
					SLAColorCpe sa = new SLAColorCpe();
					sa.setColorCode(incidentcpe[7].toString());
					sa.setId(getPriority(incidentcpe[7].toString()));
					sa.setSiteName(incidentcpe[0].toString());
					sa.setSitePercentileAverage(incidentcpe[5].toString());
					log.info("incidentcpe[1] ---------------"+incidentcpe[1].toString());
					if("Gold".equalsIgnoreCase(incidentcpe[1].toString())){
						goldSLAColor.add(sa);
						goldCount++;
						log.info("incidentcpe[1] goldSLAColor ---------------"+sa.getColorCode());
					}else if("Silver".equalsIgnoreCase(incidentcpe[1].toString())){
						silverSLAColor.add(sa);
						silverCount++;
						log.info("incidentcpe[1] silverSLAColor ---------------"+sa.getColorCode());
					}else if("Bronze".equalsIgnoreCase(incidentcpe[1].toString())){
						bronzeSLAColor.add(sa);
						bronzeCount++;
						log.info("incidentcpe[1] bronzeSLAColor ---------------"+sa.getColorCode());
					}
				}		
			}
			
			log.debug("OfficeBuildingSiteManagerImpl * getSitesList * goldSLAColor.size()------"+goldSLAColor.size());
			log.debug("OfficeBuildingSiteManagerImpl * getSitesList * silverSLAColor.size()------"+silverSLAColor.size());
			log.debug("OfficeBuildingSiteManagerImpl * getSitesList * bronzeSLAColor.size()------"+bronzeSLAColor.size());
			
			Collections.sort(goldSLAColor, new SLAColorComparator());
			Collections.sort(silverSLAColor, new SLAColorComparator());
			Collections.sort(bronzeSLAColor, new SLAColorComparator());
		}
		
		goldSLA.setSlaColorList(goldSLAColor);
		goldSLA.setSiteCount(""+goldCount);
		silverSLA.setSlaColorList(silverSLAColor);
		silverSLA.setSiteCount(""+silverCount);
		bronzeSLA.setSlaColorList(bronzeSLAColor);
		bronzeSLA.setSiteCount(""+bronzeCount);
		basedOnSLASiteList.add(goldSLA);
		basedOnSLASiteList.add(silverSLA);
		basedOnSLASiteList.add(bronzeSLA);
		log.debug("OfficeBuildingSiteManagerImpl * getSitesList * Exit the method");
		
		return basedOnSLASiteList;
	}

	@SuppressWarnings("unchecked")
	public List<Site> getSites(String customerId) {
		log.debug("OfficeBuildingSiteDaoImpl * getSites * Inside the method");
		try{
			StringBuilder hql = new StringBuilder();
		
		hql.append("from Site sa ");
		hql.append("where sa.isManaged <>'No' and sa.isDataCenter <> 'Yes' and sa.rootProductId = 'MNS' and sa.customeruniqueid=:customerId and status in ('");
	    hql.append(GenericConstants.STATUSOPERATIONAL);
	    hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		if( query.list()!=null &&  query.list().size()>0){
			return query.list();	
		}else{
			return null;
		}
		}catch (Exception e) {
		    log.error("Exception in getSites method "+e.getMessage());
		}
		log.debug("OfficeBuildingSiteDaoImpl * getSites * Exit the method");
		return null;
	}


	public String getSiteId(String siteName){
		log.debug("OfficeBuildingSiteDaoImpl * getSiteId * Inside the method");
		try{
		StringBuilder hql = new StringBuilder();
		hql.append("select siteid from Site sa ");
		hql.append("where sa.customersite=:siteName and status in ('");
	 
	    hql.append(GenericConstants.STATUSOPERATIONAL);
	    hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("siteName", siteName);
		if( query.list()!=null &&  query.list().size()>0){
			return (String) query.list().get(0);	
		}else{
			return null;
		}
		}catch (Exception e) {
		    log.error("Exception in getSiteId method "+e.getMessage());
		}
		log.debug("OfficeBuildingSiteDaoImpl * getSiteId * Exit the method");
		return null;
	}
	
	public int getPriority(String siteColor) {
		log.debug("OfficeBuildingSiteManagerImpl * getPriority * Inside the method");
		try{
		if("Red".equalsIgnoreCase(siteColor)){
			return 3;
		} else if("Green".equalsIgnoreCase(siteColor)){
			return 1;
		} else if("Amber".equalsIgnoreCase(siteColor)){
			return 2;
		}
		
		return 1;
		}catch (Exception e) {
		    log.error("Exception in getPriority method "+e.getMessage());
		}
		log.debug("OfficeBuildingSiteManagerImpl * getPriority * Exit the method");
		return 0;
	}
	
	class SLAColorComparator implements Comparator<SLAColorCpe> {
	    @Override
	    public int compare(SLAColorCpe o1, SLAColorCpe o2) {
	    	Integer id1 = ((SLAColorCpe) o1).getId();
	    	Integer id2 = ((SLAColorCpe) o2).getId();
			return id2.compareTo(id1);
		}
	}
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("4"))
			return "background-color:rgb(163, 207, 98)";
		else if (color.equalsIgnoreCase("3"))
			return "background-color:rgb(255,255,0)";
		else if (color.equalsIgnoreCase("2"))
			return "background-color:rgb(255, 156, 0)";
		else if (color.equalsIgnoreCase("1"))
			return "background-color:rgb(227, 34, 18)";
		else
			return "background-color:rgb(204,204,204)";
	}

}
