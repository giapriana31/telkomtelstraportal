package com.tt.officebuildingsite.mvc.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.IncidentDashboardMapping;
import com.tt.model.SLAAvailability;
import com.tt.model.Site;

import com.tt.officebuildingsite.mvc.dao.OfficeBuildingSiteDaoImpl;
import com.tt.officebuildingsite.mvc.model.OfficeBuildingSiteColor;
import com.tt.officebuildingsite.mvc.model.OfficeBuildingSiteList;
import com.tt.officebuildingsite.mvc.model.SLAColor;
import com.tt.officebuildingsite.mvc.model.SLAList;
import com.tt.officebuildingsite.mvc.model.SLAListCpe;

@Service(value = "officeBuildingSiteManager")
@Transactional
public class OfficeBuildingSiteManagerImpl {

	
	private final static Logger log = Logger.getLogger(OfficeBuildingSiteManagerImpl.class);
	private OfficeBuildingSiteDaoImpl officeBuildingSiteDaoImpl;

	@Autowired
	@Qualifier("officeBuildingSiteDaoImpl")
	public void setOfficeBuildingSiteDaoImpl(
			OfficeBuildingSiteDaoImpl officeBuildingSiteDaoImpl) {
		this.officeBuildingSiteDaoImpl = officeBuildingSiteDaoImpl;
	}

	public OfficeBuildingSiteDaoImpl getOfficeBuildingSiteDaoImpl() {
		return officeBuildingSiteDaoImpl;
	}
	
	public List<OfficeBuildingSiteList> getOfficeBuildingSecondPage(String customerId) {
		log.debug("OfficeBuildingSiteManagerImpl * getOfficeBuildingSecondPage * Inside the method");
		try{
		List<OfficeBuildingSiteList> officeBuildingSiteList = new ArrayList<OfficeBuildingSiteList>();
		List<Site> sites = new ArrayList<Site>();
		List<String> siteCode = new ArrayList<String>();
		sites = officeBuildingSiteDaoImpl.getSites(customerId);
		List<IncidentDashboardMapping> incidentList = officeBuildingSiteDaoImpl.getOfficeBuildingSecondGraph(customerId);
		Map<String, List<IncidentDashboardMapping>> siteTierIncidentMap = new HashMap<String, List<IncidentDashboardMapping>>();
		Map<String, List<Site>> mappedSite = new HashMap<String, List<Site>>();
		if (incidentList != null && incidentList.size() > 0) {
			if(incidentList!=null && incidentList.size()>0){
				for (IncidentDashboardMapping incident : incidentList) {
					
					if (siteTierIncidentMap.containsKey(incident.getSitetier())) {
						siteTierIncidentMap.get(incident.getSitetier()).add(
								incident);
					} else {
						List<IncidentDashboardMapping> incidentSiteTierList = new ArrayList<IncidentDashboardMapping>();
						incidentSiteTierList.add(incident);
						siteTierIncidentMap.put(incident.getSitetier(),incidentSiteTierList);
					}
				}
			}
		}
		
		if(sites!=null && sites.size()>0){
			for (Site site : sites) {
				if (mappedSite.containsKey(site.getServicetier())) {
					List<Site> siteMap = mappedSite.get(site.getServicetier());
					siteMap.add(site);
					mappedSite.put(site.getServicetier(), siteMap);
				} else {
					List<Site> siteMap = new ArrayList<Site>();
					siteMap.add(site);
					mappedSite.put(site.getServicetier(),siteMap);
				}
			}
		}
		
		for (String siteTier : siteTierIncidentMap.keySet()) {
			
			log.debug("OfficeBuildingSiteManagerImpl * getOfficeBuildingSecondPage * siteTier_------------->>>"+siteTier);
			OfficeBuildingSiteList officeBuildingSite = new OfficeBuildingSiteList();
			officeBuildingSite.setSiteTier(siteTier);
			officeBuildingSite.setId(getSiteTierId(siteTier));
			List<OfficeBuildingSiteColor> buildingSiteColors = new ArrayList<OfficeBuildingSiteColor>();
			
			Map<String, Integer> sitePriorityMap = new HashMap<String, Integer>();
			Integer siteTierCount = 1;

			for (IncidentDashboardMapping incident : siteTierIncidentMap.get(siteTier)) {
				OfficeBuildingSiteColor buildingSiteColor = new OfficeBuildingSiteColor();
				if (!sitePriorityMap.containsKey(incident.getSitename())) {
					buildingSiteColor.setId(getPriority(incident.getSitecolor()));
					buildingSiteColor.setColorCode(incident.getSitecolor().toLowerCase());
					siteCode.add(incident.getSitename());
					buildingSiteColor.setSiteName(incident.getSitename());
					sitePriorityMap.put(incident.getSitename(), getPriority(incident.getSitecolor()));
					officeBuildingSite.setSiteCount(siteTierCount.toString());
					siteTierCount = siteTierCount + 1;
				} else {
					if (sitePriorityMap.get(incident.getSitename()) < getPriority(incident.getSitecolor())) {
						
						if (buildingSiteColors != null && buildingSiteColors.size() > 0) {

							Iterator<OfficeBuildingSiteColor> officeBuildingSiteColorIterator = buildingSiteColors.iterator();
							while (officeBuildingSiteColorIterator.hasNext()) {
								OfficeBuildingSiteColor siteColorIterator = officeBuildingSiteColorIterator.next();
								if (siteColorIterator.getSiteName()!=null && incident.getSitename().equalsIgnoreCase(siteColorIterator.getSiteName())) {
									siteColorIterator.setColorCode(incident.getSitecolor().toLowerCase());
									siteColorIterator.setId(getPriority(incident.getSitecolor()));
								}
							}
							sitePriorityMap.put(incident.getSitename(), getPriority(incident.getSitecolor()));
						}
					}
				}

				if(buildingSiteColor!=null && buildingSiteColor.getColorCode()!=null)
					buildingSiteColors.add(buildingSiteColor);
			}
			
			if(sites!=null && sites.size()>0)
			for (Site site : sites) {
				if(site!=null && site.getServicetier().equalsIgnoreCase(siteTier))
					if(siteCode!=null && !siteCode.contains(site.getCustomersite())){
						OfficeBuildingSiteColor buildingSiteColorNoInc = new OfficeBuildingSiteColor();
						buildingSiteColorNoInc.setSiteName(site.getCustomersite());
						buildingSiteColorNoInc.setId(1);
						buildingSiteColorNoInc.setColorCode("green");
						buildingSiteColors.add(buildingSiteColorNoInc);
						Integer count = Integer.parseInt(officeBuildingSite.getSiteCount());
						count = count+1;
						officeBuildingSite.setSiteCount(count.toString());
					}
			}
			
			Collections.sort(buildingSiteColors, new OfficeBuildingSiteColorComparator());
			officeBuildingSite.setOfficeBuildingSiteColorList(buildingSiteColors);
			String lowerCaseSiteTier=siteTier.charAt(0)+siteTier.substring(1).toLowerCase();
			officeBuildingSite.setSiteTier(lowerCaseSiteTier);
			officeBuildingSiteList.add(officeBuildingSite);
		}
		
		if(mappedSite !=null && !mappedSite.isEmpty())
		for (String siteMap : mappedSite.keySet()) {
			if(!siteTierIncidentMap.containsKey(siteMap)){
				log.debug("OfficeBuildingSiteManagerImpl * getOfficeBuildingSecondPage * siteMap----"+siteMap);
				OfficeBuildingSiteList officeBuildingSite = new OfficeBuildingSiteList();
				officeBuildingSite.setSiteTier(siteMap);
				officeBuildingSite.setId(getSiteTierId(siteMap));
				List<OfficeBuildingSiteColor> buildingSiteColors = new ArrayList<OfficeBuildingSiteColor>();
				Integer siteTierCount = 1;
				for (Site site : mappedSite.get(siteMap)) {
					OfficeBuildingSiteColor buildingSiteColor = new OfficeBuildingSiteColor();
					buildingSiteColor.setId(1);
					buildingSiteColor.setColorCode("green");
					buildingSiteColor.setSiteName(site.getCustomersite());
					officeBuildingSite.setSiteCount(siteTierCount.toString());
					buildingSiteColors.add(buildingSiteColor);
					siteTierCount = siteTierCount + 1;
				}
				officeBuildingSite.setOfficeBuildingSiteColorList(buildingSiteColors);
				String lowerCaseSiteTier=siteMap.charAt(0)+siteMap.substring(1).toLowerCase();
				officeBuildingSite.setSiteTier(lowerCaseSiteTier);
				officeBuildingSiteList.add(officeBuildingSite);
				
			}
		}
		log.debug("OfficeBuildingSiteManagerImpl * getOfficeBuildingSecondPage * officeBuildingSiteList----------------"+officeBuildingSiteList.toString());

		Collections.sort(officeBuildingSiteList, new OfficeBuildingSiteListComparator());
		log.debug("OfficeBuildingSiteManagerImpl * getOfficeBuildingSecondPage * Exit the method");
		return officeBuildingSiteList;
	}catch (Exception e) {
	    log.error("Exception in getOfficeBuildingSecondPage method "+e.getMessage());
	}
	return null;
	}
	private int getSiteTierId(String siteTier) {
		log.debug("OfficeBuildingSiteManagerImpl * getSiteTierId * Inside the method");
		try{
		if("Gold".equalsIgnoreCase(siteTier)){
			return 3;
		} else if("Silver".equalsIgnoreCase(siteTier)){
			return 2;
		} else if("Bronze".equalsIgnoreCase(siteTier)){
			return 1;
		}
		return 0;
	}catch (Exception e) {
	    log.error("Exception in getSiteTierId method "+e.getMessage());
	}
		log.debug("OfficeBuildingSiteManagerImpl * getSiteTierId * Exit the method");
	return 0;
	}
	

	public int getPriority(String siteColor) {
		log.debug("OfficeBuildingSiteManagerImpl * getPriority * Inside the method");
		try{
		if("Red".equalsIgnoreCase(siteColor)){
			return 3;
		} else if("Green".equalsIgnoreCase(siteColor)){
			return 1;
		} else if("Amber".equalsIgnoreCase(siteColor)){
			return 2;
		}
		
		return 1;
		}catch (Exception e) {
		    log.error("Exception in getPriority method "+e.getMessage());
		}
		log.debug("OfficeBuildingSiteManagerImpl * getPriority * Exit the method");
		return 0;
	}

	public List<SLAList> getSitesList(String customerId){
		log.debug("OfficeBuildingSiteManagerImpl * getSitesList * Inside the method");
		try{
		List<SLAAvailability> slaSiteList =  officeBuildingSiteDaoImpl.getBasedOnSLASitesList(customerId);
		List<SLAList> basedOnSLASiteList= new ArrayList<SLAList>();
		List<SLAColor> goldSLAColor= new ArrayList<SLAColor>();
		List<SLAColor> silverSLAColor= new ArrayList<SLAColor>();
		List<SLAColor> bronzeSLAColor= new ArrayList<SLAColor>();
		SLAList goldSLA = new SLAList();
		goldSLA.setSiteTier("Gold Sites");		
		SLAList silverSLA = new SLAList();
		silverSLA.setSiteTier("Silver Sites");
		SLAList bronzeSLA = new SLAList();
		bronzeSLA.setSiteTier("Bronze Sites");
		int goldCount=0;
		int silverCount=0;
		int bronzeCount=0;
		
		if(slaSiteList != null && slaSiteList.size() > 0){			
			for (SLAAvailability slaAvailability : slaSiteList) {
				SLAColor sa = new SLAColor();
				sa.setColorCode(slaAvailability.getSite_color().toLowerCase());
				sa.setId(getPriority(slaAvailability.getSite_color()));
				sa.setSiteName(slaAvailability.getSite_name());
				sa.setSitePercentileAverage(slaAvailability.getSite_percentile_average());;
				if("gold".equalsIgnoreCase(slaAvailability.getSite_tier())){
					goldSLAColor.add(sa);
					goldCount++;
				}else if("silver".equalsIgnoreCase(slaAvailability.getSite_tier())){
					silverSLAColor.add(sa);
					silverCount++;
				}else if("bronze".equalsIgnoreCase(slaAvailability.getSite_tier())){
					bronzeSLAColor.add(sa);
					bronzeCount++;
				}			
			}			
			log.debug("OfficeBuildingSiteManagerImpl * getSitesList * goldSLAColor.size()------"+goldSLAColor.size());
			
			Collections.sort(goldSLAColor, new SLAColorComparator());
			Collections.sort(silverSLAColor, new SLAColorComparator());
			Collections.sort(bronzeSLAColor, new SLAColorComparator());
			
		}
		goldSLA.setSlaColorList(goldSLAColor);
		goldSLA.setSiteCount(""+goldCount);
		silverSLA.setSlaColorList(silverSLAColor);
		silverSLA.setSiteCount(""+silverCount);
		bronzeSLA.setSlaColorList(bronzeSLAColor);
		bronzeSLA.setSiteCount(""+bronzeCount);
		basedOnSLASiteList.add(goldSLA);
		basedOnSLASiteList.add(silverSLA);
		basedOnSLASiteList.add(bronzeSLA);
		log.debug("OfficeBuildingSiteManagerImpl * getSitesList * Exit the method");
		return basedOnSLASiteList;
	}catch (Exception e) {
	    log.error("Exception in getSitesList method "+e.getMessage());
	}
	return null;
	}
	
	public List<SLAListCpe> getSitesListCpe (String customerId){
		
		return officeBuildingSiteDaoImpl.getBasedOnSLASitesListCpe(customerId);	
	}

	public String getSiteId(String siteName){
		
		return officeBuildingSiteDaoImpl.getSiteId(siteName);
	}
	
}
class OfficeBuildingSiteColorComparator implements Comparator<OfficeBuildingSiteColor> {
    @Override
    public int compare(OfficeBuildingSiteColor o1, OfficeBuildingSiteColor o2) {
    	Integer id1 = ((OfficeBuildingSiteColor) o1).getId();
    	Integer id2 = ((OfficeBuildingSiteColor) o2).getId();

		return id2.compareTo(id1);
	}
}

class SLAColorComparator implements Comparator<SLAColor> {
    @Override
    public int compare(SLAColor o1, SLAColor o2) {
    	Integer id1 = ((SLAColor) o1).getId();
    	Integer id2 = ((SLAColor) o2).getId();
		return id2.compareTo(id1);
	}
}

class OfficeBuildingSiteListComparator implements Comparator<OfficeBuildingSiteList> {
    @Override
    public int compare(OfficeBuildingSiteList o1, OfficeBuildingSiteList o2) {
    	Integer id1 = ((OfficeBuildingSiteList) o1).getId();
    	Integer id2 = ((OfficeBuildingSiteList) o2).getId();
		return id2.compareTo(id1);
	}
}