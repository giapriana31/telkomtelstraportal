package com.tt.pvsiteslevel1.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.pvsiteslevel1.mvc.model.OfficeBuildingSiteColor;



@Repository(value="pvSitesLevel1Dao")
@Transactional
public class PVSitesLevel1Dao {

	private final static Logger log = Logger.getLogger(PVSitesLevel1Dao.class);
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public ArrayList<OfficeBuildingSiteColor> getSitesLevel1Details(String customerId){
		log.debug("PVSitesLevel1Dao * getSitesLevel1Details * Inside the method");
		
		
		Map<String,String> nameToDeliveryStageMap= new HashMap<String, String>();
		nameToDeliveryStageMap.put("Order Received", "1");
		nameToDeliveryStageMap.put("Detailed Design", "2");
		nameToDeliveryStageMap.put("Procurement", "3");
		nameToDeliveryStageMap.put("Delivery & Activation", "4");
		nameToDeliveryStageMap.put("Monitoring", "5");
		nameToDeliveryStageMap.put("Operational", "6");
		

		
		ArrayList<OfficeBuildingSiteColor> sitesLevel1Details=null;
		
		SQLQuery query= sessionFactory.getCurrentSession().createSQLQuery("select s.customersite, s.pvsitedeliverystatus, s.servicetier from  site s where s.isManaged <>'No' and s.isDataCenter <> 'Yes' and s.customeruniqueid = '"+customerId+"' and s.rootProductId='MNS' ");
		log.debug("PVSitesLevel1Dao * getSitesLevel1Details * Query String: "+query.getQueryString());

		List<Object[]> detailList=query.list();
		if(detailList!=null && !detailList.isEmpty()){
			log.debug("PVSitesLevel1Dao * getSitesLevel1Details * Query not null and size: "+detailList.size());

			 sitesLevel1Details= new ArrayList<OfficeBuildingSiteColor>();
			 for (Object[] details : detailList) {
				 
				 OfficeBuildingSiteColor buildingSiteColor= new OfficeBuildingSiteColor();
				 buildingSiteColor.setSiteName((String) details[0]);
				 buildingSiteColor.setDeliveryStage(nameToDeliveryStageMap.get((String) details[1]));
				 buildingSiteColor.setColorCode(getColor(nameToDeliveryStageMap.get((String) details[1])));
				 buildingSiteColor.setSiteTier((String) details[2]);
					log.debug("PVSitesLevel1Dao * getSitesLevel1Details * Site Id: "+buildingSiteColor.getDeliveryStage());
					log.debug("PVSitesLevel1Dao * getSitesLevel1Details * Site Tier: "+buildingSiteColor.getSiteTier());
					log.debug("PVSitesLevel1Dao * getSitesLevel1Details * Site Color: "+buildingSiteColor.getColorCode());
					log.debug("PVSitesLevel1Dao * getSitesLevel1Details * Site Name: "+buildingSiteColor.getSiteName());

				
				 sitesLevel1Details.add(buildingSiteColor);
				 
			}
			
			
		}
		log.debug("PVSitesLevel1Dao * getSitesLevel1Details * exit");

		return sitesLevel1Details;
	}
	
	private String getColor(String deliveryStage){
		
		if(null != deliveryStage){
			switch(deliveryStage){
				case "1":
					return "#000";
				case "2":
					return "#A40800";
				case "3":
					return "#7030A0";
				case "4":
					return "#00B0F0";
				case "5":
					return "#FA8072";
				case "6":
					return "#89C35C";
			}
		}
		
		return "black";
	}
	
	public String getSiteId(String siteName) {
		
		try {
			StringBuilder hql = new StringBuilder();
			hql.append("select siteid from Site sa ");
			hql.append("where sa.customersite=:siteName and status in ('");
			hql.append(GenericConstants.STATUSCOMMISSIONING);
			hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
			hql.append(GenericConstants.STATUSOPERATIONAL);
			hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);

			Query query = sessionFactory.getCurrentSession().createQuery(
					hql.toString());
			query.setString("siteName", siteName);
			if (query.list() != null && query.list().size() > 0) {
				return (String) query.list().get(0);
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in getSiteId method " + e.getMessage());
		}
		log.debug("OfficeBuildingSiteDaoImpl * getSiteId * Exit the method");
		return null;
	}


}
