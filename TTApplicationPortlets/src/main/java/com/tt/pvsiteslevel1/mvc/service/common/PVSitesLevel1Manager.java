package com.tt.pvsiteslevel1.mvc.service.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;







import com.tt.pvsiteslevel1.mvc.dao.PVSitesLevel1Dao;
import com.tt.pvsiteslevel1.mvc.model.OfficeBuildingSiteColor;



@Service(value="pvSitesLevel1Manager")
@Transactional
public class PVSitesLevel1Manager {

	
	private PVSitesLevel1Dao pvSitesLevel1Dao;
	private final static Logger log = Logger.getLogger(PVSitesLevel1Manager.class);

	
	@Autowired
	@Qualifier(value="pvSitesLevel1Dao")
	public void setPvSitesLevel1Dao(PVSitesLevel1Dao pvSitesLevel1Dao) {
		this.pvSitesLevel1Dao = pvSitesLevel1Dao;
	}
	
	
	public ArrayList<OfficeBuildingSiteColor> getSitesLevel1Details(String customerId){
		log.debug("PVSitesLevel1Manager * getSitesLevel1Details * Inside the method");

		return pvSitesLevel1Dao.getSitesLevel1Details(customerId);
	}
	
	public String getSiteId(String siteName) {
		log.debug("PVSitesLevel1Manager * getSiteId * Inside the method");

		return pvSitesLevel1Dao.getSiteId(siteName);
	}	
}
