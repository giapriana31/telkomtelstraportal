package com.tt.pvsiteslevel1.mvc.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.pvsiteslevel1.mvc.model.OfficeBuildingSiteColor;
import com.tt.pvsiteslevel1.mvc.model.OfficeBuildingSiteList;
import com.tt.pvsiteslevel1.mvc.service.common.PVSitesLevel1Manager;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("pvSitesLevel1Controller")
@RequestMapping("VIEW")
public class PVSitesLevel1Controller {

	private PVSitesLevel1Manager pvSitesLevel1Manager;
	private final static Logger log = Logger
			.getLogger(PVSitesLevel1Controller.class);
	public static Properties prop = PropertyReader.getProperties();

	@Autowired
	@Qualifier(value = "pvSitesLevel1Manager")
	public void setPvSitesLevel1Manager(
			PVSitesLevel1Manager pvSitesLevel1Manager) {
		this.pvSitesLevel1Manager = pvSitesLevel1Manager;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		log.debug("PVSitesLevel1Controller * handleRenderRequest * Inside the method");
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) request
					.getAttribute(WebKeys.THEME_DISPLAY);
			
			  String customerId = TTGenericUtils.getCustomerIDFromSession(
			  PortalUtil.getHttpServletRequest(request),
			  PortalUtil.getHttpServletResponse(renderResponse));
			 

			log.debug("PVSitesLevel1Controller * handleRenderRequest * customerId......."
					+ customerId);

			ArrayList<OfficeBuildingSiteColor> sitesLevel1Details = pvSitesLevel1Manager
					.getSitesLevel1Details(customerId);
			OfficeBuildingSiteList goldSiteTier = new OfficeBuildingSiteList();
			goldSiteTier.setSiteCount("0");
			goldSiteTier.setSiteTier("Gold");
			OfficeBuildingSiteList silverSiteTier = new OfficeBuildingSiteList();
			silverSiteTier.setSiteCount("0");
			silverSiteTier.setSiteTier("Silver");
			OfficeBuildingSiteList bronzeSiteTier = new OfficeBuildingSiteList();
			bronzeSiteTier.setSiteCount("0");
			bronzeSiteTier.setSiteTier("Bronze");
			boolean isGoldAdded = false;
			boolean isSilverAdded = false;
			boolean isBronzeAdded = false;
			int goldCount = 0;
			int silverCount = 0;
			int bronzeCount = 0;
			ArrayList<OfficeBuildingSiteList> sitesLevel1MasterList = new ArrayList<OfficeBuildingSiteList>();
			ArrayList<OfficeBuildingSiteColor> goldTierList = new ArrayList<OfficeBuildingSiteColor>();
			ArrayList<OfficeBuildingSiteColor> silverTierList = new ArrayList<OfficeBuildingSiteColor>();
			ArrayList<OfficeBuildingSiteColor> bronzeTierList = new ArrayList<OfficeBuildingSiteColor>();
			
			if (null!=sitesLevel1Details && sitesLevel1Details.size()>0) {
				for (OfficeBuildingSiteColor sitesLevel1List : sitesLevel1Details) {
					if (sitesLevel1List.getSiteTier().equalsIgnoreCase("gold")) {
						goldTierList.add(sitesLevel1List);
						isGoldAdded=true;
						goldCount++;
					} else if (sitesLevel1List.getSiteTier().equalsIgnoreCase("silver")) {
						silverTierList.add(sitesLevel1List);
						silverCount++;
						isSilverAdded=true;
					} else if (sitesLevel1List.getSiteTier().equalsIgnoreCase("bronze")) {
						bronzeTierList.add(sitesLevel1List);
						bronzeCount++;
						isBronzeAdded=true;
					}
				}
			}
			goldSiteTier.setSiteCount("" + goldCount);
			silverSiteTier.setSiteCount("" + silverCount);
			bronzeSiteTier.setSiteCount("" + bronzeCount);
			if(goldTierList!=null){
				Collections.sort(goldTierList, new OfficeBuildingSiteColorComparator());

				
			}
			if(silverTierList!=null){
				Collections.sort(silverTierList, new OfficeBuildingSiteColorComparator());

				
			}
			if(bronzeTierList!=null){
				Collections.sort(bronzeTierList, new OfficeBuildingSiteColorComparator());

				
			}
			//Collections.sort(silverTierList, new OfficeBuildingSiteColorComparator());
			//Collections.sort(bronzeTierList, new OfficeBuildingSiteColorComparator());
			goldSiteTier.setOfficeBuildingSiteColorList(goldTierList);
			silverSiteTier.setOfficeBuildingSiteColorList(silverTierList);
			bronzeSiteTier.setOfficeBuildingSiteColorList(bronzeTierList);
			sitesLevel1MasterList.add(goldSiteTier);
			sitesLevel1MasterList.add(silverSiteTier);
			sitesLevel1MasterList.add(bronzeSiteTier);

			Gson gson = new Gson();
			String basedonincidentsjson = gson.toJson(sitesLevel1MasterList);
			log.debug("PVSitesLevel1Controller * handleRenderRequest * incidentListJson: "+basedonincidentsjson);
			// String basedonslajson =
			// "[{\"id\":'x',\"siteTier\":\"Gold Sites\",\"siteCount\":\"01\",\"slaColorList\":[{\"id\":\"01\", \"siteName\":\"testsite\", \"colorCode\":\"red\", \"sitePercentileAverage\":\"30\"}]},{\"id\":'y',\"siteTier\":\"Silver Sites\",\"siteCount\":\"02\",\"slaColorList\":[{\"id\":\"02\", \"siteName\":\"testsite2\", \"colorCode\":\"amber\", \"sitePercentileAverage\":\"30\"}, {\"id\":\"02a\", \"siteName\":\"testsite2a\", \"colorCode\":\"green\", \"sitePercentileAverage\":\"30\"}]},{\"id\":'z',\"siteTier\":\"Bronze Sites\",\"siteCount\":\"01\",\"slaColorList\":[{\"id\":\"03\", \"siteName\":\"testsite3\", \"colorCode\":\"green\", \"sitePercentileAverage\":\"40\"}]}];";
			model.addAttribute("showTabBasedOn", "basedOnTicket");
			model.addAttribute("goldSiteTier", goldSiteTier);
			model.addAttribute("silverSiteTier", silverSiteTier);
			model.addAttribute("bronzeSiteTier", bronzeSiteTier);
			model.addAttribute("isGoldAdded", isGoldAdded);
			model.addAttribute("isSilverAdded", isSilverAdded);
			model.addAttribute("isBronzeAdded", isBronzeAdded);
			// model.addAttribute("basedOnSLAList", basedOnSLAList);
			model.addAttribute("basedonincidentsjson", basedonincidentsjson);
			// model.addAttribute("basedonslajson", basedonslajson);
			log.debug("PVSitesLevel1Controller * handleRenderRequest * Exit the method");
			return "pvSitesLevel1";
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" PVSitesLevel1Controller*Exception in handleRenderRequest method "
					+ e.getMessage());
		}
		return null;
	}

	
    @ActionMapping(value = "viewSite")
    public void viewSite(ActionRequest actionRequest,
		    ActionResponse actionResponse) throws IOException, PortletException {
	    	log.debug("PVSitesLevel0Controller * viewSite * Inside the method");
	    	try{
	    	String siteName = actionRequest.getParameter("siteName");
	    	log.debug("PVSitesLevel0Controller * viewSite * Site Id -----" + siteName);
	    	javax.xml.namespace.QName siteNameBean = new QName("http://localhost:8080/pvSiteTicket", "siteName", "event");
	    	actionResponse.setEvent(siteNameBean, siteName);
	    	
			String url = prop.getProperty("tt.pv.homeURL") + "/" + prop.getProperty("ttPVProjectView");
			log.debug("PVSitesLevel0Controller * viewSite * Exit the method");
			actionResponse.sendRedirect(url);
	    }catch (Exception e) {
		    log.error("Exception in viewSite method "+e.getMessage());	    
		}
	}
	
	

}
class OfficeBuildingSiteColorComparator implements Comparator<OfficeBuildingSiteColor> {
    @Override
    public int compare(OfficeBuildingSiteColor o1, OfficeBuildingSiteColor o2) {
    	Integer id1 = Integer.parseInt(((OfficeBuildingSiteColor) o1).getDeliveryStage());
    	Integer id2 = Integer.parseInt(((OfficeBuildingSiteColor) o2).getDeliveryStage());

		return id2.compareTo(id1);
	}
}