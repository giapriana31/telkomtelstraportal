package com.tt.pvsiteslevel1.mvc.model;

public class SLAColor implements Comparable<SLAColor>{
	
	private int id;
	private String siteName;
	private String colorCode;
	private String sitePercentileAverage;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSitePercentileAverage() {
		return sitePercentileAverage;
	}
	public void setSitePercentileAverage(String sitePercentileAverage) {
		this.sitePercentileAverage = sitePercentileAverage;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	
	@Override
	public  int compareTo(SLAColor slaColor) {
		int priorityId = ((SLAColor) slaColor).getId();
		return (int) (priorityId - this.id);
	}

}
