package com.tt.pvsiteslevel1.mvc.model;

import java.util.ArrayList;

public class OfficeBuildingSiteList  implements Comparable<OfficeBuildingSiteList>{

	private String siteTier;
	private String siteCount;
	private int id;
	
	private ArrayList<OfficeBuildingSiteColor> officeBuildingSiteColorList;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiteTier() {
		return siteTier;
	}
	public void setSiteTier(String siteTier) {
		this.siteTier = siteTier;
	}
	public String getSiteCount() {
		return siteCount;
	}
	public void setSiteCount(String siteCount) {
		this.siteCount = siteCount;
	}


	public ArrayList<OfficeBuildingSiteColor> getOfficeBuildingSiteColorList() {
		return officeBuildingSiteColorList;
	}
	public void setOfficeBuildingSiteColorList(
			ArrayList<OfficeBuildingSiteColor> officeBuildingSiteColorList) {
		this.officeBuildingSiteColorList = officeBuildingSiteColorList;
	}
	@Override
	public String toString() {
		return "OfficeBuildingSiteList [siteTier=" + siteTier + ", siteCount="
				+ siteCount + ", id=" + id + ", officeBuildingSiteColorList="
				+ officeBuildingSiteColorList + "]";
	}
	@Override
	public  int compareTo(OfficeBuildingSiteList slaList) {
		int siteTierId = ((OfficeBuildingSiteList) slaList).getId();
		return (int) (siteTierId - this.id);
	}
}
