package com.tt.pvsiteslevel1.mvc.model;

public class OfficeBuildingSiteColor implements
		Comparable<OfficeBuildingSiteColor> {

	private String deliveryStage;
	private String siteName;
	private String colorCode;
	private String siteTier;

	public String getSiteTier() {
		return siteTier;
	}

	public void setSiteTier(String siteTier) {
		this.siteTier = siteTier;
	}

	public String getDeliveryStage() {
		return deliveryStage;
	}

	public void setDeliveryStage(String deliveryStage) {
		this.deliveryStage = deliveryStage;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}

	@Override
	public int compareTo(OfficeBuildingSiteColor buildingSiteColor) {
		int priorityId = Integer.parseInt(((OfficeBuildingSiteColor) buildingSiteColor).getDeliveryStage());
		return (int) (priorityId - Integer.parseInt(((OfficeBuildingSiteColor) buildingSiteColor).getDeliveryStage()));
	}
}
