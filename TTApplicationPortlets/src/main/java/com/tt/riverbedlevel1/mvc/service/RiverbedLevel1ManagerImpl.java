package com.tt.riverbedlevel1.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tt.riverbedlevel1.mvc.dao.RiverbedLevel1DaoImpl;

@Service(value = "riverbedLevel1ManagerImpl")
@Transactional
public class RiverbedLevel1ManagerImpl {
	
	private RiverbedLevel1DaoImpl riverbedLevel1DaoImpl;


	@Autowired
	@Qualifier("riverbedLevel1DaoImpl")
	public void setRiverbedLevel1DaoImpl(RiverbedLevel1DaoImpl riverbedLevel1DaoImpl) {
		this.riverbedLevel1DaoImpl = riverbedLevel1DaoImpl;
	}
	
	public String getApplianceJSONArray(String customerId){
		return riverbedLevel1DaoImpl.getApplianceJSONArray(customerId);
		
	}
	
	public String getApplication(String customerId){
		return riverbedLevel1DaoImpl.getApplication(customerId);
	}
	

}
