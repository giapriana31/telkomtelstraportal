package com.tt.riverbedlevel1.mvc.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value ="riverbedLevel1DaoImpl")
@Transactional
public class RiverbedLevel1DaoImpl {
	
	private final static Logger log = Logger.getLogger(RiverbedLevel1DaoImpl.class);
	public static Properties prop = PropertyReader.getProperties();
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public String getApplianceJSONArray(String customerId){
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		
		log.debug("----- riverbedLevel1DaoImpl ----- getApplianceGoldJSONArray() ----- Method Called");
		
		StringBuffer sb = new StringBuffer();
		sb.append(" select *, (select case when avg_reduction between 0 and 39.99 then 'red'");
		sb.append(" when avg_reduction between 40 and 69.99 then 'amber' ");
		sb.append(" when avg_reduction >=70 then 'green' end) colour,");
		sb.append(" (select case when avg_reduction<0 then '0' ");
		sb.append(" when avg_reduction between 0 and 39 then '1' ");
		sb.append(" when avg_reduction between 40 and 69 then '2'");
		sb.append(" when avg_reduction >=70 then '3' end) priority from (");
		sb.append(" select ciname,count(1) jml_application,truncate(sum(reduction_zero)/count(1),2) avg_reduction,city from (");
		sb.append(" select *, (select case when reduction <= 0.00 or reduction is null then 0.00 else reduction end) reduction_zero from (");
		sb.append(" select a.*,(((lan_in + lan_out) - (wan_in + wan_out)) / (lan_in + lan_out))*(100) reduction, b.city from riverbeddashboard a, (select a.*,SUBSTRING_INDEX(SUBSTRING_INDEX(b.customersite,'-',-2),'-',1 ) city from configurationitem a, site b where a.siteid = b.siteid and a.productclassification = 'M-WANOPT' and a.customeruniqueid = '"+customerId+"') b");
		sb.append(" where a.ciname = b.ciname");
		sb.append(" and a.start_time BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00') AND DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59')");
		sb.append(" )c");
		sb.append(" )d group by appliance");
		sb.append(" )e");
		
		log.info("query for riverbedlevel 1-" +sb.toString());
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] appliance : (List<Object[]>)query.list()) {
				if (appliance[0]!=null) {
					log.info("applianceName ----------------"+appliance[0].toString());
					obj = new JSONObject();
					obj.put("appliance", appliance[0].toString());
					obj.put("jml_application", appliance[1].toString());
					obj.put("avg_reduction", appliance[2].toString());
					obj.put("sitename", appliance[3].toString());
					obj.put("colour", colorNameToCode(appliance[4].toString()));
					obj.put("priority", appliance[5].toString());
					JSONObjArray.add(obj);
				}
				
			}
		}
		
		return JSONObjArray.toString();
	}
	
@SuppressWarnings("unchecked")
public String getApplication(String customerId){
		
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		
		log.debug("----- riverbedLevel1DaoImpl ----- getApplianceGoldJSONArray() ----- Method Called");
		
		StringBuffer sb = new StringBuffer();
		sb.append(" select ports,ciname,truncate(reduction_zero,2),(select case when reduction_zero between 0 and 39.99 then 'red'");
		sb.append(" when reduction_zero between 40 and 69.99 then 'amber'");
		sb.append(" when reduction_zero >=70 then 'green' end) colour,");
		sb.append(" (select case when reduction_zero between 0 and 39.99 then '1' ");
		sb.append(" when reduction_zero between 40 and 69.99 then '2'");
		sb.append(" when reduction_zero >=70 then '3' end) priority from (");
		sb.append(" select *, (select case when reduction <= 0.00 or reduction is null then 0.00 else reduction end) reduction_zero from (");
		sb.append(" select a.*,(((lan_in + lan_out) - (wan_in + wan_out)) / (lan_in + lan_out))*(100) reduction from riverbeddashboard a, configurationitem b");
		sb.append(" where a.ciname = b.ciname and b.productclassification = 'M-WANOPT' and b.customeruniqueid = '"+customerId+"' ");
		sb.append(" and a.start_time BETWEEN DATE_FORMAT(NOW() - INTERVAL 1 MONTH, '%Y-%m-01 00:00:00') AND DATE_FORMAT(LAST_DAY(NOW() - INTERVAL 1 MONTH), '%Y-%m-%d 23:59:59')");
		sb.append(" )c");
		sb.append(" )d");
		
		log.info("query for riverbedlevel 1-" +sb.toString());
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] applianceGold : (List<Object[]>)query.list()) {
				if (applianceGold[3]!=null) {
					obj = new JSONObject();
					obj.put("application", applianceGold[0].toString());
					obj.put("appliance", applianceGold[1].toString());
					obj.put("optimization", applianceGold[2].toString());
					obj.put("colour", colorNameToCode(applianceGold[3].toString()));
					obj.put("priority", applianceGold[4].toString());
					JSONObjArray.add(obj);
				}
				
			}
		}
		
		return JSONObjArray.toString();
	}
	
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("green"))
			return "background-color:rgb(163, 207, 98)";
		else if (color.equalsIgnoreCase("amber"))
			return "background-color:rgb(255, 156, 0)";
		else if (color.equalsIgnoreCase("red"))
			return "background-color:rgb(227, 34, 18)";
		else
			return "";
	}

}
