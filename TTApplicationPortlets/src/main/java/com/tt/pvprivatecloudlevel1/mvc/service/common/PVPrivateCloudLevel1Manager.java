package com.tt.pvprivatecloudlevel1.mvc.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvprivatecloudlevel1.mvc.dao.PVPrivateCloudLevel1Dao;
import com.tt.pvprivatecloudlevel1.mvc.model.drillDownDetails;




@Service(value = "pvPrivateCloudLevel1Manager")
@Transactional
public class PVPrivateCloudLevel1Manager {

	@Autowired
	@Qualifier(value="pvPrivateCloudLevel1Dao")
	private PVPrivateCloudLevel1Dao privateCloudLevel1Dao;
	private final static Logger log = Logger.getLogger(PVPrivateCloudLevel1Manager.class);
	
	
	public List<drillDownDetails> getDrillDownDetails(String customerId,String filters,String searchKeyword){
		
		log.info("PVPrivateCloudLevel1Manager-getDrillDownData()-start");

		
		
		return privateCloudLevel1Dao.getDrillDownData(customerId,filters,searchKeyword);
		

		
		
	}
}
