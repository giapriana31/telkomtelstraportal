package com.tt.pvprivatecloudlevel1.mvc.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.officebuildingsite.mvc.model.OfficeBuildingSiteList;
import com.tt.pvprivatecloudlevel1.mvc.model.drillDownDetails;



@Repository(value = "pvPrivateCloudLevel1Dao")
@Transactional
public class PVPrivateCloudLevel1Dao {
	
	
	private final static Logger log = Logger.getLogger(PVPrivateCloudLevel1Dao.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public List<drillDownDetails> getDrillDownData(String customerId,String filters,String searchKeyword){
		
	
		
		log.info("PVPrivateCloudLevel1Dao-getDrillDownData()-start");

		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(6, "#89C35C");
		deliveryStageToColor.put(5, "#FA8072");
		deliveryStageToColor.put(4, "#00B0F0");
		deliveryStageToColor.put(3, "#7030A0");
		deliveryStageToColor.put(2, "#A40800");
		deliveryStageToColor.put(1, "#000");
		Query query=null;
		
		if(!filters.equalsIgnoreCase("null")){

			log.info("PVPrivateCloudLevel1Dao-getDrillDownData()-filter not null "+filters+"search: "+searchKeyword);

			if(!searchKeyword.equalsIgnoreCase("null")){
				

				if(filters.contains("All")){
					 query= sessionFactory.getCurrentSession().createSQLQuery("Select delivery_stage,ciname,ciid from configurationitem where customeruniqueid='"+customerId+"' and lower(rootProductId)=lower('Private Cloud') and lower(cmdbclass)=lower('vBLOCK') and ciname LIKE '%"+searchKeyword+"%' order by delivery_stage desc");

					
				}else{
					
					 query= sessionFactory.getCurrentSession().createSQLQuery("Select delivery_stage,ciname,ciid from configurationitem where customeruniqueid='"+customerId+"' and lower(rootProductId)=lower('Private Cloud') and  lower(cmdbclass)=lower('vBLOCK') and delivery_stage in ("+filters+") and ciname LIKE '%"+searchKeyword+"%' order by delivery_stage desc");

					
				}
				
			

				
			}else{
				
				
				if(filters.contains("All")){
					 query= sessionFactory.getCurrentSession().createSQLQuery("Select delivery_stage,ciname,ciid from configurationitem where customeruniqueid='"+customerId+"' and lower(rootProductId)=lower('Private Cloud') and  lower(cmdbclass)=lower('vBLOCK') order by delivery_stage desc ");

					
				}else{
					
					 query= sessionFactory.getCurrentSession().createSQLQuery("Select delivery_stage,ciname,ciid from configurationitem where customeruniqueid='"+customerId+"' and  lower(rootProductId)=lower('Private Cloud') and  lower(cmdbclass)=lower('vBLOCK') and delivery_stage in ("+filters+") order by delivery_stage desc");

					
				}
			}
			
		}
		
		
	
		
		
		List<drillDownDetails> details = null;
		
		if (null!=query) {
			log.info("PVPrivateCloudLevel1Dao-getDrillDownData()-query "
					+ query.getQueryString());
			details = new ArrayList<drillDownDetails>();
			if (null != query.list() && query.list().size() > 0) {

				List<Object[]> drillDownData = query.list();

				for (Object[] object : drillDownData) {

					if (null!=object[0]) {
						
						drillDownDetails downDetails = new drillDownDetails();
						downDetails.setColor(deliveryStageToColor.get(Integer
								.parseInt((String) object[0])));
						downDetails.setvBlockName((String) object[1]+" stage:"+(String) object[0]);
						downDetails.setCiid((String) object[2]);
						details.add(downDetails);
					}
					

				}

			}
		}
		log.info("PVPrivateCloudLevel1Dao-getDrillDownData()-end");

		
		 if (details.size() > 0) {
			    Collections.sort(details, new privateCloudComparator());
			   }
		
		
		return details;
		
	}
	

}


class privateCloudComparator implements Comparator<drillDownDetails> {
	

	
	
	@Override
	public int compare(drillDownDetails o1, drillDownDetails o2) {
		final Map<String,Integer> colorToDeliveryStageMap= new HashMap<String, Integer>();
		colorToDeliveryStageMap.put("#89C35C", 6);
		colorToDeliveryStageMap.put("#FA8072", 5);
		colorToDeliveryStageMap.put("#00B0F0", 4);
		colorToDeliveryStageMap.put("#7030A0", 3);
		colorToDeliveryStageMap.put("#A40800", 2);
		colorToDeliveryStageMap.put("#000", 1);
		// TODO Auto-generated method stub
		
		System.out.println("1: "+ colorToDeliveryStageMap.get(o1.getColor()));
		System.out.println("2: "+ colorToDeliveryStageMap.get(o2.getColor()));
		
        return colorToDeliveryStageMap.get(o2.getColor()).compareTo(colorToDeliveryStageMap.get(o1.getColor()));

	}
}
