package com.tt.pvprivatecloudlevel1.mvc.model;

public class drillDownDetails {

	
	
	private String color;
	private String vBlockName;
	private String ciid;
	
	
	public String getCiid() {
		return ciid;
	}
	public void setCiid(String ciid) {
		this.ciid = ciid;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getvBlockName() {
		return vBlockName;
	}
	public void setvBlockName(String vBlockName) {
		this.vBlockName = vBlockName;
	}
	
	
	
}
