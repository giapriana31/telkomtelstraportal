package com.tt.pvprivatecloudlevel1.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.pvprivatecloudlevel1.mvc.model.drillDownDetails;
import com.tt.pvprivatecloudlevel1.mvc.service.common.PVPrivateCloudLevel1Manager;
import com.tt.utils.TTGenericUtils;


@Controller("pvPrivateCloudLevel1Controller")
@RequestMapping("VIEW")
public class PVPrivateCloudLevel1Controller {
	
	@Autowired
	@Qualifier(value="pvPrivateCloudLevel1Manager")
	private PVPrivateCloudLevel1Manager pvPrivateCloudLevel1Manager;
	private final static Logger log = Logger.getLogger(PVPrivateCloudLevel1Controller.class);

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {

		
		log.info("pvPrivateCloudLevel1Controller-render()");
		
		
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);

		
		//String customerId="TT001";
		
		
		
		
		
			return "pvPrivateCloudLevel1";
		
		
		
		
		
		
	}
	
	@ResourceMapping(value="getDrillDownDetail")
	public void getDrillDownData(ResourceRequest request,ResourceResponse response){
	
		
		Map<String,String> nameToDeliveryStageMap= new HashMap<String, String>();
		nameToDeliveryStageMap.put("Order Received", "1");
		nameToDeliveryStageMap.put("Detailed Design", "2");
		nameToDeliveryStageMap.put("Procurement", "3");
		nameToDeliveryStageMap.put("DeliveryActivation", "4");
		nameToDeliveryStageMap.put("Monitoring", "5");
		nameToDeliveryStageMap.put("Operational", "6");
		
		log.info("pvPrivateCloudLevel1Controller-getDrillDownData()-start");

		

		
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(response);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);

		//String customerId="TT001";
		
		String filters=request.getParameter("filters");
		String searchTerm=request.getParameter("searchTerm");
		System.out.println("searchTerm: "+searchTerm);
		System.out.println("filters: "+filters);

		log.info("pvPrivateCloudLevel1Controller-getDrillDownData()-FIlters: "+filters);
		log.info("pvPrivateCloudLevel1Controller-getDrillDownData()-Searchterm: "+searchTerm);


		String filterStages=null;
		String filterStagesString=null;
		if (null!=filters) {
			if (filters.contains("All")) {

				filterStagesString = "All";
			} else {
				filterStages="";
				String filterArray[] = filters.split("-");

				for (int i = 0; i < filterArray.length; i++) {
					String tempString=nameToDeliveryStageMap.get(filterArray[i]);
					
					filterStages =   filterStages + ",'"+tempString+"'";
				}

				filterStagesString = filterStages.substring(1);

			}
		}
		log.info("pvPrivateCloudLevel1Controller-getDrillDownData()-FIlters: "+filterStagesString);

		
		List<drillDownDetails> drillDetailList=pvPrivateCloudLevel1Manager.getDrillDownDetails(customerId,filterStagesString,searchTerm);
		
		JSONObject objectList;
		JSONArray drillDownArray= new JSONArray();

		if (null!=drillDetailList) {
			for (drillDownDetails drillDownDetails : drillDetailList) {

				log.info("pvPrivateCloudLevel1Controller-getDrillDownData() "
						+ "Color: " + drillDownDetails.getColor() + " Name: "
						+ drillDownDetails.getvBlockName());

			}
			LinkedHashMap<String, String> drillDownMap = new LinkedHashMap<String, String>();
		
			for (drillDownDetails drillDownDetails : drillDetailList) {
				JSONObject tempJsonObj = new JSONObject();
				tempJsonObj.put(drillDownDetails.getvBlockName()+"***"+drillDownDetails.getCiid(), drillDownDetails.getColor());
				drillDownArray.add(tempJsonObj);
				drillDownMap.put(drillDownDetails.getvBlockName()+"***"+drillDownDetails.getCiid(),
						drillDownDetails.getColor());
			}
			ArrayList<String> keys= new  ArrayList<String>();
			keys.addAll(drillDownMap.keySet());
			
			objectList= new JSONObject();
			
			for (int i = 0; i < keys.size(); i++) {
				objectList.put(keys.get(i), drillDownMap.get(keys.get(i)));
			}
			
			log.info("pvPrivateCloudLevel1Controller-getDrillDownData() Drill Down Json: "
					+ objectList);
		}
		
		else{
			
			objectList = new JSONObject();
			
		}
		PrintWriter out = null;
		try {
			out=response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			log.error("pvPrivateCloudLevel1Controller-getDrillDownData()-catch: "+e.getMessage());

		}
		
		//out.print(objectList.toString());
		out.print(drillDownArray.toString());
		
		
		log.info("pvPrivateCloudLevel1Controller-getDrillDownData()-end");

		
	}

}
