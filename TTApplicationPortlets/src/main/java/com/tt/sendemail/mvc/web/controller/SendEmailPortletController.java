package com.tt.sendemail.mvc.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Controller("sendEmailPortlet")
@RequestMapping("VIEW")
public class SendEmailPortletController {
	   private final static Logger log = Logger.getLogger(SendEmailPortletController.class);

	private JavaMailSender mailSender;
	
	Properties properties = PropertyReader.getProperties();
	
	private final String EMAIL_SUBJECT = "mailSubject";
	private final String EMAIL_BODY = "mailBody";
	private final String SENDER_EMAIL_ADDRESS = "senderEmailAddess";
	private final String RECEIVER_EMAIL_ADDRESS = "receiverEmailAddess";
	private final String ATTACHMENT = "attachment"; 
	private final String EMAIL_SEND_FAILURE = "mail-send-failure";
	private final String EMAIL_SEND_SUCCESS = "mail-send-success";
	private final String EMAIL_VALIDATE_SUCCESS = "mail-validate-success";
	private final String UPLOADED_FILE_NAME = "uploadedFileName";
	private final String FILE_SIZE_TOO_LONG = "file_size_too_long";
	private final String FILE_TYPE_INVALID = "file_type_invalid";
	private final long ALLOWED_FILE_SIZE = Long.parseLong(properties.getProperty("allowedFileSize")); 
	private final String BLANK = "";
	
	@Autowired
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		model.addAttribute("AttachmentDescription", properties.getProperty("AttachmentDescription"));
		return "sendEmail";
	}

	@ActionMapping(value = "sendMailMessage")
	public void sendMailMessage(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException,
			PortletException, AddressException {

		log.info("SendEmailPortletController.java-sendMailMessage()--Start");
		
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		
		String mailSubject = uploadRequest.getParameter(EMAIL_SUBJECT);
		String mailBody = uploadRequest.getParameter(EMAIL_BODY);
		String senderMailAddress = uploadRequest.getParameter(SENDER_EMAIL_ADDRESS);
		String receiverMailAddress = uploadRequest.getParameter(RECEIVER_EMAIL_ADDRESS);
		File uploadfile = uploadRequest.getFile(ATTACHMENT);
		String uploadedFileName = uploadRequest.getFileName(ATTACHMENT);
		
		//setting all parameters in a hashmap
		Map<String, Object> mailDetails = new HashMap<String, Object>();
		mailDetails.put(EMAIL_SUBJECT, mailSubject);
		mailDetails.put(EMAIL_BODY, mailBody);
		mailDetails.put(SENDER_EMAIL_ADDRESS, senderMailAddress);
		mailDetails.put(RECEIVER_EMAIL_ADDRESS, receiverMailAddress);
		mailDetails.put(ATTACHMENT, uploadfile);
		mailDetails.put(UPLOADED_FILE_NAME, uploadedFileName);
		
		try
		{
			
			String validationMessage = validateMessage(mailDetails);
			log.debug("SendEmailPortletController.java-sendMailMessage()-validationMessage" + validationMessage);
			
			if(validationMessage.equals(EMAIL_SEND_FAILURE))
			{
				SessionMessages.add(actionRequest.getPortletSession(), EMAIL_SEND_FAILURE);
			}
			else if(validationMessage.equals(FILE_SIZE_TOO_LONG))
			{
				SessionMessages.add(actionRequest.getPortletSession(), FILE_SIZE_TOO_LONG);
			}
			else if(validationMessage.equalsIgnoreCase(FILE_TYPE_INVALID))
			{	log.info("File type invalid is being sent" );
				SessionMessages.add(actionRequest.getPortletSession(), FILE_TYPE_INVALID);
			}
			else
			{
				//more than one receivers are stored in the list 
				String[] emailArray = receiverMailAddress.split(";");
				List<String> emailList = new ArrayList<String>();
				for (String email : emailArray) 
				{
					if (email != null && email.trim().length() > 0) 
					{
						emailList.add(email.trim());
					}
				}
				
				//for each receiver, email is sent
				for (String email : emailList) 
				{
					if (!email.trim().contains("@")) 
					{
						throw new Exception();
					} 
					else if (email.contains("@"))
					{
						//email address is split by @ so that no email starts from @
						String[] emailSub = email.trim().split("@");
						
						if (emailSub.length > 1) 
						{
							// code to send images in the email
							ThemeDisplay td = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
							
/*							String src = td.getURLPortal() + "/TTLoginLogoutHook/images/Brand1.png";
							String src2 = td.getURLPortal() + "/TTLoginLogoutHook/images/tt1.png";*/
							
							String src = properties.getProperty("SendEmailURL") + "/TTLoginLogoutHook/images/Brand1.png";
							String src2 = properties.getProperty("SendEmailURL") + "/TTLoginLogoutHook/images/tt1.png";
							log.info("Brand URL "+src);
							log.info("TT1 URL "+src2);
							String mailBodyFormat = "<!DOCTYPE html> <html lang='en'> <table width='100%'> <tbody sizset='0' sizcache='15' width='100%'>  "
									+ "<tr height='10%'>          <td rowspan='2' style='width:5%'> <img src='"
									+ src
									+ "'> </td>    <td style='width:95%;text-align:center;height:30px' valign='top' text-align='center'><img  src='"
									+ src2
									+ "' ></td>"
									+ "</tr> <tr height='90%'> <td valign='top' style='padding-left:60px'> <pre><br><br>"
									+ mailBody
									+ "</pre></td> </tr> </tbody> </table> </body> </html>";
							
							log.debug("SendEmailPortletController.java-sendMailMessage()-Mailbody " + mailBody == null ? "NULL" : mailBody.toString());
							
							String emailStatusMessage = sendMessage(mailDetails, email, mailBodyFormat);
							
							if(emailStatusMessage.equals(EMAIL_SEND_SUCCESS))
							{
								SessionMessages.add(actionRequest.getPortletSession(),	EMAIL_SEND_SUCCESS);
								log.info("SendEmailPortletController.java-sendMailMessage()--Email Sent successfully");
							}
							else
							{
								log.info("SendEmailPortletController.java-sendMailMessage()--Email not Sent successfully");
							}
						}
						else 
						{
							log.error("SendEmailPortletController.java-sendMailMessage()-Error Problem with email address");
							throw new AddressException();							
						}
					}
				}
			}
		} 
		catch (Exception e) 
		{
			log.error("SendEmailPortletController.java-sendMailMessage()-Catch Exception Message "+e.getMessage() );
		}
		log.info("SendEmailPortletController.java-sendMailMessage()--End");
	}
	
	
	public String validateMessage(Map<String, Object> mailDetails) 
	{
		log.info("SendEmailPortletController.java-validateMessage()--Start ");
		String mailSubject = (String) mailDetails.get(EMAIL_SUBJECT);
		String mailBody = (String) mailDetails.get(EMAIL_BODY);
		String senderMailAddress = (String) mailDetails.get(SENDER_EMAIL_ADDRESS);
		String receiverMailAddress = (String) mailDetails.get(RECEIVER_EMAIL_ADDRESS);
		File uploadedFile = (File) mailDetails.get(ATTACHMENT);
		
		String ext=FilenameUtils.getExtension(uploadedFile.getName());
		
		long fileSize = 0;
		if(uploadedFile != null)
		{
			fileSize = uploadedFile.length();

			if(!properties.getProperty("fileExtensions").matches("(.*)"+ext+"(.*)")){
				log.info("SendEmailPortletController.java-validateMessage()--End ");
				return FILE_TYPE_INVALID;
			}						
		}
		
		if ((mailBody == null || mailSubject == null || senderMailAddress == null || receiverMailAddress == null)
				|| (mailBody.trim().length() == 0 || mailSubject.trim().length() == 0 || senderMailAddress.trim().length() == 0 
				|| receiverMailAddress.trim().length() == 0)) 
		{
			log.info("SendEmailPortletController.java-validateMessage()--End ");
			return EMAIL_SEND_FAILURE;
		} 
		else if(fileSize > ALLOWED_FILE_SIZE)
		{
			log.info("SendEmailPortletController.java-validateMessage()--End ");
			return FILE_SIZE_TOO_LONG;
		}
		else 
		{
			log.info("SendEmailPortletController.java-validateMessage()--End ");
			return EMAIL_VALIDATE_SUCCESS;
		}
	}

	public String sendMessage(Map<String, Object> mailDetails, String email, String mailBodyFormat) throws SendFailedException, Exception
	{
		log.info("SendEmailPortletController.java-sendMessage()--Start");
		String senderMailAddress = (String) mailDetails.get(SENDER_EMAIL_ADDRESS);
		String mailSubject = (String) mailDetails.get(EMAIL_SUBJECT);
		File uploadedFile = (File) mailDetails.get(ATTACHMENT);
		String uploadedFileName = (String) mailDetails.get(UPLOADED_FILE_NAME);
		
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		
		try
		{		
			
			mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
			mimeMessage.setFrom(new InternetAddress(senderMailAddress));
			mimeMessage.setSubject(mailSubject);
			
			Multipart multipart = new MimeMultipart();
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(mailBodyFormat, "text/html");
			multipart.addBodyPart(messageBodyPart);
			
			if(uploadedFileName != null && !BLANK.equals(uploadedFileName))
			{
				MimeBodyPart attachmentPart = new MimeBodyPart();
		        attachmentPart = new MimeBodyPart();
		        DataSource source = new FileDataSource(uploadedFile);
		        attachmentPart.setDataHandler(new DataHandler(source));
		        attachmentPart.setFileName(uploadedFileName);
		        multipart.addBodyPart(attachmentPart);
		        mimeMessage.setContent(multipart);
			}
			else
			{
				mimeMessage.setContent(mailBodyFormat,"text/html");
			}
			
	        mailSender.send(mimeMessage);
	        
	        log.info("SendEmailPortletController.java-sendMessage()--End");
	        return EMAIL_SEND_SUCCESS;
		}
		catch(SendFailedException sendFailedException)
		{
			log.error("SendEmailPortletController.java-sendMailMessage()-Catch Exception Message "+ sendFailedException.getMessage());
			throw new SendFailedException();
		}
		catch(Exception e)
		{
			log.error("SendEmailPortletController.java-sendMailMessage()-Catch Exception Message "+e.getMessage());
			throw new Exception();
		}
	}
}
