package com.tt.subscribedservices.mvc.service.common;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

//import com.sun.org.apache.xml.internal.utils.IntVector;
import com.tt.logging.Logger;
import com.tt.model.BVSubscribedServices;
import com.tt.pvprivatecloudlevel0.mvc.service.common.PVPrivateCloudLevel0Manager;
import com.tt.subscribedservices.mvc.dao.SubscribedServicesDaoImpl;


@Service(value="subscribedServicesManagerImpl")
@Transactional
public class SubscribedServicesManagerImpl {
	
	@Autowired
	@Qualifier(value="subscribedServicesDaoImpl")
	private SubscribedServicesDaoImpl subscribedServiceDaoImpl;
	private final static Logger log = Logger.getLogger(SubscribedServicesManagerImpl.class);
	private static final String MNS = "MNS"; 
	private static final String privateCloud = "Private Cloud"; 
	private static final String unifiedComm = "Unified Comm"; 
	private static final String manageSecurity = "Manage Security"; 
	private static final String IPScape = "IPScape"; 
	private static final String whispir = "Whispir"; 
	private static final String Mandoe = "Mandoe"; 
	private static final String NA = "NA"; 


	
	public List<BVSubscribedServices> getSubscribedServicesData(String customerId){
		
		
		List<BVSubscribedServices> subscribedServicesData=subscribedServiceDaoImpl.getSubscribedServicesData(customerId);
		List<BVSubscribedServices> tempServices= new ArrayList<BVSubscribedServices>();
		if(null!=subscribedServicesData && subscribedServicesData.size()>0){
			
			for (BVSubscribedServices bvSubscribedServices : subscribedServicesData) {
				if (!bvSubscribedServices.getContractduration().equalsIgnoreCase(NA)) {
					BVSubscribedServices tempSubscribedServices=new BVSubscribedServices();
					tempSubscribedServices.setCustomeruniqueid(bvSubscribedServices.getCustomeruniqueid());
					tempSubscribedServices.setProductname(bvSubscribedServices.getProductname());
					tempSubscribedServices.setContractduration(bvSubscribedServices.getContractduration());
					tempSubscribedServices.setStatus(bvSubscribedServices.getStatus());
					
					
					String noOfDays = tempSubscribedServices
							.getContractduration();
					String months = convertDaystoMonthsInDecimal(noOfDays);

					
					
					if(Float.parseFloat(months) == Math.floor(Float.parseFloat(months))){
						
						 months=""+Math.round(Double.parseDouble(months));
					
					}
					tempSubscribedServices.setContractduration(months+" months");
					System.out.println("No of Months: "+tempSubscribedServices.getContractduration());
					tempServices.add(tempSubscribedServices);
				}else{
					
					tempServices.add(bvSubscribedServices);
				}
				
				
				
			}
			
			return tempServices;
		}
		
		else{
			
			System.out.println("Manger Else");
			
			List<String> productList= new ArrayList<String>();
			productList.add(MNS);
			productList.add(privateCloud);
			productList.add(unifiedComm);
			productList.add(manageSecurity);
			productList.add(IPScape);
			productList.add(whispir);
			productList.add(Mandoe);
			
			subscribedServicesData= new ArrayList<BVSubscribedServices>();
			for (String productName : productList) {
				
				BVSubscribedServices subscribedServices= new BVSubscribedServices();
				subscribedServices.setProductname(productName);
				subscribedServices.setContractduration(NA);
				subscribedServices.setContractremainingtime(NA);
				subscribedServices.setCustomeruniqueid(customerId);
				subscribedServices.setPerformance(NA);
				subscribedServices.setStatus(NA);
				
				subscribedServicesData.add(subscribedServices);
				
			}
			
			return subscribedServicesData;
			
			
		}
	
	}
	
	
	
	private String convertDaystoMonthsInDecimal(String numberOfDays)
	{
		float durationInDays = Float.parseFloat(numberOfDays);
		float contractDurationInMonths = durationInDays/(float)30;
		
		String months = String.format("%.1f",contractDurationInMonths);
		return months;
	}
}
