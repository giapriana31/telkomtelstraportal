package com.tt.subscribedservices.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.model.BVSubscribedServices;
import com.tt.subscribedservices.mvc.service.common.SubscribedServicesManagerImpl;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;


@Controller("subscribedservicesController")
@RequestMapping("VIEW")
public class SubscribedServicesController {


	@Autowired
	@Qualifier(value="subscribedServicesManagerImpl")
	private SubscribedServicesManagerImpl subscribedServicesManagerImpl ;
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);
		
		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		List<BVSubscribedServices> subscribedServicesData=subscribedServicesManagerImpl.getSubscribedServicesData(customerId);
	
		for (BVSubscribedServices bvSubscribedServices : subscribedServicesData) {
			
			System.out.println("Prod Name: "+ bvSubscribedServices.getProductname()+" Contract Duration: "+bvSubscribedServices.getContractduration());
		}
		model.addAttribute("subscribedServicesData", subscribedServicesData);
		return "subscribedservices";
	}
	
	
	
}