package com.tt.subscribedservices.mvc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.BVSubscribedServices;
import com.tt.pvprivatecloudlevel1.mvc.dao.PVPrivateCloudLevel1Dao;

@Repository(value="subscribedServicesDaoImpl")
@Transactional
public class SubscribedServicesDaoImpl {
	
	
	private final static Logger log = Logger.getLogger(SubscribedServicesDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	public List<BVSubscribedServices> getSubscribedServicesData(String customerId){
	
		System.out.println("Dao");
		Query query=sessionFactory.getCurrentSession().createQuery("from BVSubscribedServices where customeruniqueid='"+customerId+"' ORDER BY performance, capacity");
		
		System.out.println("Query: "+query.getQueryString());
		System.out.println("CustomerId: "+customerId);
		List<BVSubscribedServices> subscribedServicesList=query.list();
		//sessionFactory.getCurrentSession().close();
		//sessionFactory.close();
		
		
		return subscribedServicesList; 
	}
}
