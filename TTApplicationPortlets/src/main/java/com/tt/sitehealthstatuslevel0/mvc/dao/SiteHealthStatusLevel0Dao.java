package com.tt.sitehealthstatuslevel0.mvc.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value = "SiteHealthStatusLevel0Dao")
@Transactional
public class SiteHealthStatusLevel0Dao {

	@Autowired
	private SessionFactory sessionFactory;
	
		public Map<String,Integer> getGraphData(String customerId){
			
			Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
			deliveryStageToColor.put(4, "#a3cf62");
			deliveryStageToColor.put(3, "#ffff00");
			deliveryStageToColor.put(2, "#ff9c00");
			deliveryStageToColor.put(1, "#e32212");
			Map<String,Integer> graphData=new HashMap<String, Integer>();

			StringBuffer sb = new StringBuffer();
			sb.append(" SELECT b.priority,a.customersite  ");
			sb.append(" FROM site a LEFT JOIN ");
			sb.append(" (SELECT * FROM( ");
			sb.append(" SELECT siteid,priority FROM incident WHERE customeruniqueid = '" + customerId + "' AND incidentstatus NOT IN ('Cancelled','Completed', 'Closed') ");
			sb.append(" order by priority ASC) a ");
			sb.append(" GROUP BY siteid ORDER BY priority ASC) b  ");
			sb.append(" ON a.siteid = b.siteid ");
			sb.append(" WHERE a.customeruniqueid = '" + customerId + "' AND b.priority IS NOT NULL ");
			
            Query innerQuery= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());

					List<Object[]> productStatusList=innerQuery.list();
					
					if(null!=productStatusList && productStatusList.size()>0){
						for (Object[] productData : productStatusList) {
							String productDeliveryStage=String.valueOf(productData[0]);
							if(null!=productDeliveryStage && productDeliveryStage.length()>0){
								
								String deliveryColor=deliveryStageToColor.get(Integer.parseInt(productDeliveryStage));
								if(graphData.size()>0){
									
									Set<String> mapKeySet=graphData.keySet();
									
									if(mapKeySet.contains(deliveryColor)){
										
										int currentCount=graphData.get(deliveryColor);
										graphData.put(deliveryColor, currentCount+1);
									}else{
										
										graphData.put(deliveryColor, 1);
									}
									
								}else{
									
									graphData.put(deliveryColor, 1);	
								}
								
							}
						}
						
					}
					
		return graphData;
		}	
	
	
}
