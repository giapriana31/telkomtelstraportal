package com.tt.sitehealthstatuslevel0.mvc.service;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.sitehealthstatuslevel0.mvc.dao.SiteHealthStatusLevel0Dao;;


@Service(value = "SiteHealthLevel0Manager")
@Transactional
public class SiteHealthLevel0Manager {

	private SiteHealthStatusLevel0Dao sitehealthstatuslevel0Dao1;

	@Autowired
	@Qualifier("SiteHealthStatusLevel0Dao")
	public void setSiteHealthStatusLevel0Dao(SiteHealthStatusLevel0Dao sitehealthStatusLevel0Dao2) {
		this.sitehealthstatuslevel0Dao1  = sitehealthStatusLevel0Dao2;
	}
	
	public Map<String,Integer> getGraphData(String customerId){
		return sitehealthstatuslevel0Dao1.getGraphData(customerId);
	}
	
}
