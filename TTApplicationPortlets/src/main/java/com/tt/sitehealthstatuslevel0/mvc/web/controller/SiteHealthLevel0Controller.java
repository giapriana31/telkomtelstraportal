package com.tt.sitehealthstatuslevel0.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.sitehealthstatuslevel0.mvc.service.SiteHealthLevel0Manager;
import com.tt.utils.TTGenericUtils;

@Controller("SiteHealthLevel0Controller")
@RequestMapping("VIEW")
public class SiteHealthLevel0Controller {

	private SiteHealthLevel0Manager sitehealthLevel0Manager2;
	private final static Logger log = Logger.getLogger(SiteHealthLevel0Controller.class);

	@Autowired
	@Qualifier("SiteHealthLevel0Manager")
	public void setSiteHealthLevel0Manager(SiteHealthLevel0Manager sitehealthLevel0Manager1) {
		this.sitehealthLevel0Manager2 = sitehealthLevel0Manager1;
	}	
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
	
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		SubscribedService ss = new SubscribedService();

		//int count = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_SAAS, customerId);
		int count = ss.isSubscribedServicesSite(customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_SAAS);
			return "unsubscribed_sitehealth_service";
		}
		else
		{
			//ArrayList<String> serviceStatusHealth= sitehealthLevel0Manager2.getServiceStatusHealth(customerId);
			//model.addAttribute("serviceStatusHealth", serviceStatusHealth);


			
			Map<String,Integer> graphDataMap=sitehealthLevel0Manager2.getGraphData(customerId);
			
			Set<String> graphDataKeys=graphDataMap.keySet();
			
			//for (String string : graphDataKeys) 
			//{
			//	log.info("PVSaaSLevel0Controller-graph data-"+"Graphkey: "+string+"  Data:"+graphDataMap.get(string));
			//}
			
		
			Map<String, Integer> graphColorMap = new HashMap<String, Integer>();

			Gson gson = new Gson();
			List<String> colorList=new ArrayList<String>();

			colorList.add("#a3cf62");
			colorList.add("#e32212");
			colorList.add("#ff9c00");
			colorList.add("#ffff00");

		
			for (String string : colorList) 
			{
				if(null != graphDataMap && !graphDataMap.keySet().contains(string))
				{
					graphDataMap.put(string, 0);
				}	
			}
		
			String saasGraphDataJson = gson.toJson(graphDataMap);
			log.info("siteGraphDataJson -----"+saasGraphDataJson);
			
			String colorListJson=gson.toJson(colorList);
			ArrayList<String> graphBottom= new ArrayList<String>();
		//	HashMap<String, String> graphBottom=new HashMap<String, String>();
			
			graphBottom.add("Normal-#a3cf62");
			graphBottom.add("Priority 1-#e32212");
			graphBottom.add("Priority 2-#ff9c00");
			graphBottom.add("Priority 3-#ffff00");
			
			model.addAttribute("siteStatusGraphData", saasGraphDataJson);
			
			model.addAttribute("graphColorListSiteStatus", colorListJson);
			model.addAttribute("graphBottomListSiteStatus", graphBottom);
				
			
			return "sitehealthLevel0";
		}
	}
	
	
	
}
