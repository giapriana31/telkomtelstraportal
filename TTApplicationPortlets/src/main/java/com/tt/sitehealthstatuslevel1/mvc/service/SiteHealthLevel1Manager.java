package com.tt.sitehealthstatuslevel1.mvc.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.sitehealthstatuslevel1.mvc.dao.SiteHealthStatusLevel1Dao;

@Service(value = "siteHealthLevel1Manager")
@Transactional
public class SiteHealthLevel1Manager {

	private final static Logger log = Logger.getLogger(SiteHealthLevel1Manager.class);
	private SiteHealthStatusLevel1Dao siteHealthStatusLevel1Dao;
	
	@Autowired
	@Qualifier("siteHealthStatusLevel1Dao")
	public void setSiteHealthStatusLevel1Dao(SiteHealthStatusLevel1Dao siteHealthStatusLevel1Dao) {
		this.siteHealthStatusLevel1Dao = siteHealthStatusLevel1Dao;
	}

	public SiteHealthStatusLevel1Dao getSiteHealthStatusLevel1Dao() {
		return siteHealthStatusLevel1Dao;
	}
	
	public String getBySite(String customerId){
		return siteHealthStatusLevel1Dao.getBySite(customerId);
	}
	
	public Map<String,Integer> getGraphDetailsMWAN(String customerId) {
		return siteHealthStatusLevel1Dao.getGraphDataProductClassification(customerId,"M-WAN");
	}

	public Map<String,Integer> getGraphDetailsMWANOPTI(String customerId) {
		return siteHealthStatusLevel1Dao.getGraphDataProductClassification(customerId,"M-WANOPT");
	}

	public Map<String,Integer> getGraphDetailsMWLAN(String customerId) {
		return siteHealthStatusLevel1Dao.getGraphDataProductClassification(customerId,"M-WLAN");
	}	

	public Map<String,Integer> getGraphDetailsMLAN(String customerId) {
		return siteHealthStatusLevel1Dao.getGraphDataProductClassification(customerId,"M-LAN");
	}

	public Map<String,Integer> getGraphDetailsMFIREWALL(String customerId) {
		return siteHealthStatusLevel1Dao.getGraphDataProductClassification(customerId,"M-FIREWALL");
	}
	
}
