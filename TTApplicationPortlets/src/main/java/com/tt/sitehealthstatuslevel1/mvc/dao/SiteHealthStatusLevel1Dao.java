package com.tt.sitehealthstatuslevel1.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;

@Repository(value="siteHealthStatusLevel1Dao")
@Transactional
public class SiteHealthStatusLevel1Dao {
	private final static Logger log = Logger.getLogger(SiteHealthStatusLevel1Dao.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public String getBySite(String customerId){
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		
		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT b.priority,a.customersite  ");
		sb.append(" FROM site a LEFT JOIN ");
		sb.append(" (SELECT * FROM( ");
		sb.append(" SELECT siteid,priority FROM incident WHERE customeruniqueid = '" + customerId + "' AND incidentstatus NOT IN ('Cancelled','Completed', 'Closed') ");
		sb.append(" order by priority ASC) a ");
		sb.append(" GROUP BY siteid ORDER BY priority ASC) b  ");
		sb.append(" ON a.siteid = b.siteid ");
		sb.append(" WHERE a.customeruniqueid = '" + customerId + "' AND b.priority IS NOT NULL ");
		
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] appliance : (List<Object[]>)query.list()) {
				if (appliance[0]!=null) {
					obj = new JSONObject();
					obj.put("sitename", appliance[1].toString());
					obj.put("colour", colorNameToCode(appliance[0].toString()));
					obj.put("priority", appliance[0].toString());
					JSONObjArray.add(obj);
				}
				
			}
		}
		
		return JSONObjArray.toString();
		
	}
	
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("4"))
			return "background-color:rgb(163, 207, 98)";
		else if (color.equalsIgnoreCase("3"))
			return "background-color:rgb(255,255,0)";
		else if (color.equalsIgnoreCase("2"))
			return "background-color:rgb(255, 156, 0)";
		else if (color.equalsIgnoreCase("1"))
			return "background-color:rgb(227, 34, 18)";
		else
			return "background-color:rgb(204,204,204)";
	}
	
@SuppressWarnings("unchecked")
public Map<String,Integer> getGraphDataProductClassification(String customerId, String productClassification) {
		
		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(4, "#a3cf62");
		deliveryStageToColor.put(3, "#ffff00");
		deliveryStageToColor.put(2, "#ff9c00");
		deliveryStageToColor.put(1, "#e32212");

		Map<String,Integer> graphDataProductClassification=new HashMap<String, Integer>();

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT c.priority,a.productclassification FROM configurationitem a LEFT JOIN ");
		sb.append(" (SELECT b.priority,a.siteid ");
		sb.append(" FROM site a LEFT JOIN ");
		sb.append(" (SELECT * FROM( ");
		sb.append(" SELECT siteid,priority FROM incident WHERE customeruniqueid = '" + customerId + "' AND incidentstatus NOT IN ('Cancelled','Completed', 'Closed') ");
		sb.append(" order by priority ASC) a ");
		sb.append(" GROUP BY siteid ORDER BY priority ASC) b ");
		sb.append(" ON a.siteid = b.siteid  ");
		sb.append(" WHERE a.customeruniqueid = '" + customerId + "' AND b.priority IS NOT NULL ");
		sb.append(" ) c ");
		sb.append(" ON a.siteid = c.siteid ");
		sb.append(" WHERE a.customeruniqueid = '" + customerId + "' ");
		sb.append(" AND a.deliverystatus IN ('Operational') ");
		sb.append(" AND a.rootProductId IN ('MNS','MSS') ");
		sb.append(" AND a.productclassification = '" + productClassification + "'  ");
        sb.append(" AND c.priority IS NOT NULL ");
		
		Query innerQuery= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());

		List<Object[]> productStatusListProductClassification=innerQuery.list();
		
		if(null!=productStatusListProductClassification && productStatusListProductClassification.size()>0){
			for (Object[] productDataProductClassification : productStatusListProductClassification) {
				String productDeliveryStageProductClassification=String.valueOf(productDataProductClassification[0]);
				if(null!=productDeliveryStageProductClassification && productDeliveryStageProductClassification.length()>0){
					
					String deliveryColor=deliveryStageToColor.get(Integer.parseInt(productDeliveryStageProductClassification));
					if(graphDataProductClassification.size()>0){
						
						Set<String> mapKeySet=graphDataProductClassification.keySet();
						
						if(mapKeySet.contains(deliveryColor)){
							
							int currentCount=graphDataProductClassification.get(deliveryColor);
							graphDataProductClassification.put(deliveryColor, currentCount+1);
						}else{
							
							graphDataProductClassification.put(deliveryColor, 1);
						}
						
					}else{
						
						graphDataProductClassification.put(deliveryColor, 1);	
					}
					
				}
			}
			
		}

		return graphDataProductClassification;
	}

}
