package com.tt.sitehealthstatuslevel1.mvc.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.sitehealthstatuslevel1.mvc.service.SiteHealthLevel1Manager;
import com.tt.utils.TTGenericUtils;

@Controller("siteHealthLevel1Controller")
@RequestMapping("VIEW")
public class SiteHealthLevel1Controller {

	private final static Logger log = Logger.getLogger(SiteHealthLevel1Controller.class);
	private SiteHealthLevel1Manager siteHealthLevel1Manager;
	
	@Autowired
	@Qualifier("siteHealthLevel1Manager")
	public void setSiteHealthLevel1Manager(SiteHealthLevel1Manager siteHealthLevel1Manager) {
		this.siteHealthLevel1Manager = siteHealthLevel1Manager;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
		    RenderResponse renderResponse, Model model) throws PortalException,
		    SystemException {
		
		try {
			
			String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(renderResponse));
			
			//MWAN START
			Map<String,Integer> graphDataMapMWAN=siteHealthLevel1Manager.getGraphDetailsMWAN(customerId);

			Set<String> graphDataKeysMWAN=graphDataMapMWAN.keySet();
			int subscribedMWAN = 0;
			Gson gsonmwan = new Gson();
			List<String> colorListMWAN=new ArrayList<String>();
			colorListMWAN.add("#a3cf62");
			colorListMWAN.add("#e32212");
			colorListMWAN.add("#ff9c00");
			colorListMWAN.add("#ffff00");

			for (String string : colorListMWAN) 
			{
				if(null != graphDataMapMWAN && graphDataMapMWAN.keySet().contains(string))
				{
					subscribedMWAN = subscribedMWAN + graphDataMapMWAN.get(string).intValue();
				}
			}

			for (String string : colorListMWAN) 
			{
				if(null != graphDataMapMWAN && !graphDataMapMWAN.keySet().contains(string))
				{
					graphDataMapMWAN.put(string, 0);
				}
			}

			String siteGraphDataJsonMWAN = gsonmwan.toJson(graphDataMapMWAN);
			log.info("siteGraphDataJsonMWAN -----"+siteGraphDataJsonMWAN);
			String colorListJsonMWAN=gsonmwan.toJson(colorListMWAN);
			//MWAN END
			
			//MWANOPTI START
			Map<String,Integer> graphDataMapMWANOPTI=siteHealthLevel1Manager.getGraphDetailsMWANOPTI(customerId);

			Set<String> graphDataKeysMWANOPTI=graphDataMapMWANOPTI.keySet();
			int subscribedMWANOPTI = 0;
			Gson gsonmwanopti = new Gson();
			List<String> colorListMWANOPTI=new ArrayList<String>();
			colorListMWANOPTI.add("#a3cf62");
			colorListMWANOPTI.add("#e32212");
			colorListMWANOPTI.add("#ff9c00");
			colorListMWANOPTI.add("#ffff00");


			for (String string : colorListMWANOPTI) 
			{
				if(null != graphDataMapMWANOPTI && graphDataMapMWANOPTI.keySet().contains(string))
				{
					subscribedMWANOPTI = subscribedMWANOPTI + graphDataMapMWANOPTI.get(string).intValue();
				}	
			}
			
			for (String string : colorListMWANOPTI) 
			{
				if(null != graphDataMapMWANOPTI && !graphDataMapMWANOPTI.keySet().contains(string))
				{
					graphDataMapMWANOPTI.put(string, 0);
				}	
			}

			String siteGraphDataJsonMWANOPTI = gsonmwanopti.toJson(graphDataMapMWANOPTI);
			String colorListJsonMWANOPTI=gsonmwanopti.toJson(colorListMWANOPTI);
			//MWANOPTI END
			
			//MWLAN START
			Map<String,Integer> graphDataMapMWLAN=siteHealthLevel1Manager.getGraphDetailsMWLAN(customerId);

			Set<String> graphDataKeysMWLAN=graphDataMapMWLAN.keySet();
			int subscribedMWLAN = 0;
			Gson gsonmwlan = new Gson();
			List<String> colorListMWLAN=new ArrayList<String>();
			colorListMWLAN.add("#a3cf62");
			colorListMWLAN.add("#e32212");
			colorListMWLAN.add("#ff9c00");
			colorListMWLAN.add("#ffff00");

			for (String string : colorListMWLAN) 
			{
				if(null != graphDataMapMWLAN && graphDataMapMWLAN.keySet().contains(string))
				{
					subscribedMWLAN = subscribedMWLAN + graphDataMapMWLAN.get(string).intValue();
				}	
			}

			for (String string : colorListMWLAN) 
			{
				if(null != graphDataMapMWLAN && !graphDataMapMWLAN.keySet().contains(string))
				{
					graphDataMapMWLAN.put(string, 0);
				}	
			}

			String siteGraphDataJsonMWLAN = gsonmwlan.toJson(graphDataMapMWLAN);
			String colorListJsonMWLAN=gsonmwlan.toJson(colorListMWLAN);
			//MWLAN END	
			
			//MLAN START
			Map<String,Integer> graphDataMapMLAN=siteHealthLevel1Manager.getGraphDetailsMLAN(customerId);

			Set<String> graphDataKeysMLAN=graphDataMapMLAN.keySet();
			int subcsribedMLAN = 0;
			Gson gsonmlan = new Gson();
			List<String> colorListMLAN=new ArrayList<String>();
			colorListMLAN.add("#a3cf62");
			colorListMLAN.add("#e32212");
			colorListMLAN.add("#ff9c00");
			colorListMLAN.add("#ffff00");

			for (String string : colorListMLAN) 
			{
				if(null != graphDataMapMLAN && graphDataMapMLAN.keySet().contains(string))
				{
					subcsribedMLAN = subcsribedMLAN + graphDataMapMLAN.get(string).intValue();
				}	
			}

			for (String string : colorListMLAN) 
			{
				if(null != graphDataMapMLAN && !graphDataMapMLAN.keySet().contains(string))
				{
					graphDataMapMLAN.put(string, 0);
				}	
			}

			String siteGraphDataJsonMLAN = gsonmlan.toJson(graphDataMapMLAN);
			String colorListJsonMLAN=gsonmlan.toJson(colorListMLAN);
			//MLAN END
			
			
			//MFIREWALL START
			Map<String,Integer> graphDataMapMFIREWALL=siteHealthLevel1Manager.getGraphDetailsMFIREWALL(customerId);

			Set<String> graphDataKeysMFIREWALL=graphDataMapMFIREWALL.keySet();
			int subscribedMFIREWALL = 0;
			Gson gsonmfirewall = new Gson();
			List<String> colorListMFIREWALL=new ArrayList<String>();
			colorListMFIREWALL.add("#a3cf62");
			colorListMFIREWALL.add("#e32212");
			colorListMFIREWALL.add("#ff9c00");
			colorListMFIREWALL.add("#ffff00");

			for (String string : colorListMFIREWALL) 
			{
				if(null != graphDataMapMFIREWALL && graphDataMapMFIREWALL.keySet().contains(string))
				{
					subscribedMFIREWALL = subscribedMFIREWALL + graphDataMapMFIREWALL.get(string).intValue();
				}	
			}

			for (String string : colorListMFIREWALL) 
			{
				if(null != graphDataMapMFIREWALL && !graphDataMapMFIREWALL.keySet().contains(string))
				{
					graphDataMapMFIREWALL.put(string, 0);
				}	
			}

			String siteGraphDataJsonMFIREWALL = gsonmfirewall.toJson(graphDataMapMFIREWALL);
			String colorListJsonMFIREWALL=gsonmfirewall.toJson(colorListMFIREWALL);
			//MFIREWALL END
			
			ArrayList<String> graphBottom= new ArrayList<String>();
			//	HashMap<String, String> graphBottom=new HashMap<String, String>();
				
				graphBottom.add("Normal-#a3cf62");
				graphBottom.add("Priority 1-#e32212");
				graphBottom.add("Priority 2-#ff9c00");
				graphBottom.add("Priority 3-#ffff00");
				
				model.addAttribute("bysitejson", siteHealthLevel1Manager.getBySite(customerId));
				
				//MWAN
				model.addAttribute("siteStatusGraphDataMWAN", siteGraphDataJsonMWAN);
				model.addAttribute("graphColorListSiteStatusMWAN", colorListJsonMWAN);
				model.addAttribute("subscribedMWAN", subscribedMWAN);
				log.info("siteStatusGraphDataMWAN -----" + siteGraphDataJsonMWAN.toString());
				log.info("graphColorListSiteStatusMWAN -----" + colorListJsonMWAN.toString());
				log.info("subscribedMWAN -----" + subscribedMWAN);
				//MWAN
				
				//MWANOPTI
				model.addAttribute("siteStatusGraphDataMWANOPTI", siteGraphDataJsonMWANOPTI);
				model.addAttribute("graphColorListSiteStatusMWANOPTI", colorListJsonMWANOPTI);
				model.addAttribute("subscribedMWANOPTI", subscribedMWANOPTI);
				//MWANOPTI

				//MWLAN
				model.addAttribute("siteStatusGraphDataMWLAN", siteGraphDataJsonMWLAN);
				model.addAttribute("graphColorListSiteStatusMWLAN", colorListJsonMWLAN);
				model.addAttribute("subscribedMWLAN", subscribedMWLAN);
				//MWLAN			

				//MLAN
				model.addAttribute("siteStatusGraphDataMLAN", siteGraphDataJsonMLAN);
				model.addAttribute("graphColorListSiteStatusMLAN", colorListJsonMLAN);
				model.addAttribute("subcsribedMLAN", subcsribedMLAN);
				log.info("siteStatusGraphDataMLAN -----" + siteGraphDataJsonMLAN.toString());
				log.info("graphColorListSiteStatusMLAN -----" + colorListJsonMLAN.toString());
				log.info("subcribedMLAN -----" + subcsribedMLAN);
				
				//MLAN				
				
				//MFIREWALL
				model.addAttribute("siteStatusGraphDataMFIREWALL", siteGraphDataJsonMFIREWALL);
				model.addAttribute("graphColorListSiteStatusMFIREWALL", colorListJsonMFIREWALL);
				model.addAttribute("subscribedMFIREWALL", subscribedMFIREWALL);
				//MFIREWALL
				
				
				model.addAttribute("graphBottomListSiteStatus", graphBottom);
				
				return "sitehealthLevel1";
			
		} catch (Exception e) {
			log.error("Exception in handleRenderRequest method "+e.getMessage());
		}
				return null;
		
	}
	
	
	
	
}
