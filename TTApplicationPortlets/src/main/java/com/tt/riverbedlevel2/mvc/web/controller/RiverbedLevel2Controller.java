package com.tt.riverbedlevel2.mvc.web.controller;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.riverbedlevel1.mvc.service.RiverbedLevel1ManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("riverbedLevel2Controller")
@RequestMapping("VIEW")
public class RiverbedLevel2Controller {
	
	private RiverbedLevel1ManagerImpl riverbedLevel2ManagerImpl;
	private final static Logger log = Logger.getLogger(RiverbedLevel2Controller.class);

	@Autowired
	@Qualifier("riverbedLevel1ManagerImpl")
	public void setRiverbedLevel1ManagerImpl(RiverbedLevel1ManagerImpl riverbedLevel2ManagerImpl) {
		this.riverbedLevel2ManagerImpl = riverbedLevel2ManagerImpl;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse Response, Model model) throws PortalException,SystemException 
	{
		log.debug("riverbedLevel1Controller ----- handleRenderRequest Called");
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(Response));
		log.debug("riverbedLevel1Controller ----- handleRenderRequest CustomerID "+customerUniqueID);
		log.debug("test--------------->"+ riverbedLevel2ManagerImpl.getApplication(customerUniqueID));
		model.addAttribute("ApplicationArrayJSON",riverbedLevel2ManagerImpl.getApplication(customerUniqueID));

		log.debug("riverbedLevel1Controller ----- return from handleRenderRequest");
		return "riverbedLevel2";
		
	}
	
	
    
    

}
