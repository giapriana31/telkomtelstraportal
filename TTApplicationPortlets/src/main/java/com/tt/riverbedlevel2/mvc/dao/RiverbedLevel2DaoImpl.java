package com.tt.riverbedlevel2.mvc.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.riverbedlevel1.mvc.dao.RiverbedLevel1DaoImpl;
import com.tt.utils.PropertyReader;

@Repository(value ="riverbedLevel2DaoImpl")
@Transactional
public class RiverbedLevel2DaoImpl {

	private final static Logger log = Logger.getLogger(RiverbedLevel1DaoImpl.class);
	Properties prop = PropertyReader.getProperties();
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public String getApplication(String customerId){
		
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		
		log.debug("----- riverbedLevel1DaoImpl ----- getApplianceGoldJSONArray() ----- Method Called");
		
		String queryString = "select '001' Record_ID,'telkomtelstra LAB' Customer_Name,'Gold' Service_Tiers,'TTIJKJK01OP01RC7' Appliance_Name,'80' AvgPort_Optimization_per_application_month,'80' AvgAppliance_Optimization_Percentage,'201703' Periode,'green' Mapping_Color from dual"; 
		log.info("query for riverbedlevel 1-" +queryString);
		Query query= sessionFactory.getCurrentSession().createSQLQuery(queryString);
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] applianceGold : (List<Object[]>)query.list()) {
				if (applianceGold[3]!=null) {
					log.info("applianceNameGold ----------------"+applianceGold[3].toString());
					obj = new JSONObject();
					obj.put("id", applianceGold[0].toString());
					obj.put("name", applianceGold[1].toString());
					obj.put("tier", applianceGold[2].toString());
					obj.put("port", applianceGold[3].toString());
					obj.put("appliance", applianceGold[4].toString());
					obj.put("period", applianceGold[5].toString());
					obj.put("color", colorNameToCode(applianceGold[6].toString()));
					JSONObjArray.add(obj);
				}
				
			}
		}
		
		return JSONObjArray.toString();
	}
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("green"))
			return "background-color:rgb(163, 207, 98)";
		else if (color.equalsIgnoreCase("amber"))
			return "background-color:rgb(255, 156, 0)";
		else if (color.equalsIgnoreCase("red"))
			return "background-color:rgb(227, 34, 18)";
		else
			return "";
	}
	
	
}
