package com.tt.riverbedlevel2.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.riverbedlevel2.mvc.dao.RiverbedLevel2DaoImpl;

@Service(value = "riverbedLevel2ManagerImpl")
@Transactional
public class RiverbedLevel2ManagerImpl {
	
	private RiverbedLevel2DaoImpl riverbedLevel2DaoImpl;
	private final static Logger log = Logger.getLogger(RiverbedLevel2ManagerImpl.class);
	
	@Autowired
	@Qualifier("riverbedLevel2DaoImpl")
	public void setRiverbedLevel2DaoImpl(RiverbedLevel2DaoImpl riverbedLevel2DaoImpl) {
		this.riverbedLevel2DaoImpl = riverbedLevel2DaoImpl;
	}
	
	public String getApplication(String customerId){
		return riverbedLevel2DaoImpl.getApplication(customerId);
	}

	

}
