package com.tt.pvsitelv2.mvc.service.common;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvsitelv2.mvc.dao.PVSiteLv2Dao;
import com.tt.pvsitelv1.mvc.dao.PVSiteLv1Dao;

@Service(value = "pvSiteLv2ManagerImpl")
@Transactional
public class PVSiteLv2ManagerImpl {
	
	private final static Logger log = Logger.getLogger(PVSiteLv2ManagerImpl.class);
	private PVSiteLv2Dao pvSiteLv2Dao;
	
	@Autowired
	@Qualifier("pvSiteLv2Dao")
	public void setPVSiteLv2Dao(PVSiteLv2Dao pvSiteLv2Dao){
		this.pvSiteLv2Dao = pvSiteLv2Dao;
		
	}

	public String getSiteClassificationProduct(String customerUniqueID) {
		return pvSiteLv2Dao.getSiteClassificationProduct(customerUniqueID);
	}

}
