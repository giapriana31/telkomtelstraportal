package com.tt.pvsitelv2.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.pvsitelv2.mvc.service.common.PVSiteLv2ManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("pvSiteLv2Controller")
@RequestMapping("VIEW")
public class PVSiteLv2Controller {
	
	private final static Logger log = Logger.getLogger(PVSiteLv2Controller.class);
	
	private PVSiteLv2ManagerImpl pvSiteLv2ManagerImpl;
	
	@Autowired
	@Qualifier("pvSiteLv2ManagerImpl")
	public void setPVSiteLv2ManagerImpl(PVSiteLv2ManagerImpl pvSiteLv2ManagerImpl){
		this.pvSiteLv2ManagerImpl = pvSiteLv2ManagerImpl;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		
		log.info("PVSiteLv2Controller-render-start");
	
		log.debug("PVSiteLv2Controller ----- handleRenderRequest Called");
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(renderResponse));
		log.debug("PVSiteLv2Controller ----- handleRenderRequest CustomerID "+customerUniqueID);
		log.debug("PVSiteLv2Controller--------------->"+ pvSiteLv2ManagerImpl.getSiteClassificationProduct(customerUniqueID));
		model.addAttribute("PVSiteLv2Controller",pvSiteLv2ManagerImpl.getSiteClassificationProduct(customerUniqueID));
		
		log.debug("SiteClassificationProduct ----- return from handleRenderRequest");
	
		return "pvSiteLv2";
	}
}
