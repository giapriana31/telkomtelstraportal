package com.tt.pvsitelv2.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvsiteslevel0.mvc.dao.PVSitesLevel0Dao;
import com.tt.pvsiteslevel0.mvc.model.GraphDetails;

@Repository(value = "pvSiteLv2Dao")
@Transactional
public class PVSiteLv2Dao {

	private final static Logger log = Logger.getLogger(PVSiteLv2Dao.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public String getSiteClassificationProduct(String customerId){
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		StringBuffer sb = new StringBuffer();
		sb.append("select ci.productclassification, ci.ciname, si.customersite, si.servicetier, ci.deliverystatus, ci.delivery_stage, ci.citype from configurationitem ci");
		sb.append(" LEFT JOIN site si");
		sb.append(" on si.siteid = ci.siteid ");
		sb.append(" where ci.productclassification is not null and ci.productclassification != ''");
		sb.append(" and ci.delivery_stage is not null and ci.delivery_stage != ''");
		sb.append(" and si.servicetier != '' and si.servicetier is not null");
		sb.append(" and si.customeruniqueid = '"+customerId+"' ");
		//sb.append(" and si.rootProductId in ('MNS','MSS') ");
		//sb.append(" group by ci.productclassification, ci.ciname, si.customersite, si.servicetier, ci.deliverystatus, ci.delivery_stage");
		sb.append(" group by si.siteid, ci.delivery_stage");
		sb.append(" order by delivery_stage asc;");
		
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] productlist : (List<Object[]>)query.list()) {
				if (productlist[0]!=null) {
					obj = new JSONObject();
					obj.put("productclassification", productlist[0].toString());
					obj.put("ciname", productlist[1].toString());
					obj.put("sitename", productlist[2].toString());
					obj.put("servicetier", productlist[3].toString());
					obj.put("deliverystatus", productlist[4].toString());
					obj.put("colour", colorNameToCode(productlist[5].toString()));
					obj.put("deliverystage", productlist[5].toString());
					obj.put("citype", productlist[6].toString());
					JSONObjArray.add(obj);
				}
				
			}
		}
		return JSONObjArray.toString();
	}
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("1") )
			return "background-color:rgb(0, 0, 15)";
		else if (color.equalsIgnoreCase("2"))
			return "background-color:rgb(164, 8, 0)";
		else if (color.equalsIgnoreCase("3") )
			return "background-color:rgb(112, 48, 160)";
		else if (color.equalsIgnoreCase("4") )
			return "background-color:rgb(0, 176, 240)";
		else if (color.equalsIgnoreCase("5"))
			return "background-color:rgb(250, 128, 114)";
		else
			return "background-color:rgb(137, 195, 92)";
	}
}
