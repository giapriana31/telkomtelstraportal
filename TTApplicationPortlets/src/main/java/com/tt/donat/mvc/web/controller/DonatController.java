package com.tt.donat.mvc.web.controller;

import java.util.ArrayList;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.donat.mvc.service.DonatServiceImpl;
import com.tt.utils.TTGenericUtils;

@Controller("donatController")
@RequestMapping("VIEW")
public class DonatController {

	private DonatServiceImpl donatServiceImpl;
	
	@Autowired
	@Qualifier("donatServiceImpl")
	public void setDonatServiceImpl(DonatServiceImpl donatServiceImpl){
		this.donatServiceImpl = donatServiceImpl;
	}
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse renderResponse, Model model ) throws PortalException,
		SystemException{
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
		
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
		System.out.println("cust: "+customerId);
		SubscribedService ss = new SubscribedService();
		
		int count = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_SAAS, customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_SAAS);
			return "unsubscribed_donat_service";
			
		}
		else{
			ArrayList<String> serviceStatusSpecs = donatServiceImpl.getServiceStatusSpecs(customerId);
			model.addAttribute("serviceStatusSpecs", serviceStatusSpecs);
			return "donat";
			
		}
		
		
	}
	
	
}
