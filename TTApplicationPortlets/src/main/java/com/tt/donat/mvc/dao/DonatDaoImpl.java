package com.tt.donat.mvc.dao;

import java.util.ArrayList;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value="donatDaoImpl")
@Transactional
public class DonatDaoImpl {

	@Autowired
	private SessionFactory sessionFactory;
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(DonatDaoImpl.class);
	private static final String saasServiceCmdb=prop.getProperty("saasServiceCmdb");
	private static final String saasRootProduct=prop.getProperty("saasRootProduct");
	private static final String blueColor=prop.getProperty("blueColor");
	private static final String brownColor=prop.getProperty("brownColor");
	private static final String amberColor=prop.getProperty("amberColor");
	private static final String greyColor=prop.getProperty("greyColor");
	
	public ArrayList<String> getServiceStatusSpecs(String customerId){
		ArrayList<String> serviceStatusSpecs = new ArrayList<String>();
		
		String queryString="select (select count(distinct(c.ciid)) from configurationitem c where c.rootproductid in('"+saasRootProduct+"') and c.customeruniqueid='"+customerId+"' and c.status in ('Commissioning','Operational') and c.cmdbclass='"+saasServiceCmdb+"'),(select case when (select count(distinct(c.ciid)) from configurationitem c where c.customeruniqueid='"+customerId+"' and c.status in ('Commissioning','Operational') and c.cmdbclass='"+saasServiceCmdb+"')=0 then '"+greyColor+"' when(select count(distinct(i1.incidentid)) from incident i1 where i1.incidentstatus not in ('Cancelled','Completed', 'Closed') and i1.priority in ('1','2') and i1.customerimpacted='1' and i1.customeruniqueid='"+customerId+"' and i1.rootProductId in ('IPScpae','Mandoe','Whispir'))>=1 then '"+brownColor+"' when ((select count(distinct(i2.incidentid)) from incident i2 where i2.incidentstatus not in ('Cancelled','Completed', 'Closed') and i2.priority in ('3','4') and i2.customerimpacted='1' and i2.customeruniqueid='"+customerId+"' and i2.rootProductId in ('IPScpae','Mandoe','Whispir'))>=1 or (select count(distinct(i3.incidentid)) from incident i3 where i3.incidentstatus not in ('Cancelled','Completed', 'Closed') and i3.priority in ('1','2') and i3.customerimpacted='0' and i3.customeruniqueid='"+customerId+"' and i3.rootProductId in ('IPScpae','Mandoe','Whispir'))>=1) then '"+amberColor+"' when ((select count(distinct(i4.incidentid)) from incident i4 where i4.incidentstatus not in ('Cancelled','Completed', 'Closed') and i4.priority in ('1','2') and i4.customerimpacted='0' and i4.customeruniqueid='"+customerId+"' and i4.rootProductId in ('IPScpae','Mandoe','Whispir'))>=1 or (select count(distinct(i5.incidentid)) from incident i5 where i5.incidentstatus not in ('Cancelled','Completed', 'Closed') and i5.customeruniqueid='"+customerId+"' and i5.rootProductId in ('IPScpae','Mandoe','Whispir'))=0) then '"+blueColor+"' end) from dual";
		log.info("query for donat-"+queryString);
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryString);
		
		Object[] result=(Object[])query.uniqueResult();
		
		if(null!=result){
			serviceStatusSpecs.add(null!=result[0]?result[0].toString():"0");
			
			if(null!=result[1]) {
				serviceStatusSpecs.add(result[1].toString());
			}else {
				serviceStatusSpecs.add(greyColor);
			}
			
		}
		
		return serviceStatusSpecs;
	}
}
