package com.tt.donat.mvc.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.donat.mvc.dao.DonatDaoImpl;





@Service(value="donatServiceImpl")
@Transactional
public class DonatServiceImpl {
	private DonatDaoImpl donatDaoImpl;

	@Autowired
	@Qualifier("donatDaoImpl")
	public void setDonatDaoImpl(DonatDaoImpl donatDaoImpl){
		this.donatDaoImpl = donatDaoImpl;
	}
	public ArrayList<String> getServiceStatusSpecs(String customerId){
		return donatDaoImpl.getServiceStatusSpecs(customerId);
	}
}
