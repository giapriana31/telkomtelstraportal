package com.tt.sitehealthstatuslevel2.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.sitehealthstatuslevel2.mvc.dao.SiteHealthStatusLevel2Dao;

@Service(value = "siteHealthLevel2Manager")
@Transactional
public class SiteHealthLevel2Manager {

	private SiteHealthStatusLevel2Dao siteHealthStatusLevel2Dao;

	@Autowired
	@Qualifier("siteHealthStatusLevel2Dao")
	public void setSiteHealthStatusLevel2Dao(SiteHealthStatusLevel2Dao siteHealthStatusLevel2Dao) {
		this.siteHealthStatusLevel2Dao = siteHealthStatusLevel2Dao;
	}
	
	public String getSiteClassificationProduct(String customerId){
		return siteHealthStatusLevel2Dao.getSiteClassificationProduct(customerId);	
	}
	
}
