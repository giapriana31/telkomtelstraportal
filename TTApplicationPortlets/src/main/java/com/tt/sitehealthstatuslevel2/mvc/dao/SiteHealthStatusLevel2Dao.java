package com.tt.sitehealthstatuslevel2.mvc.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.tt.utils.PropertyReader;

@Repository(value ="siteHealthStatusLevel2Dao")
@Transactional
public class SiteHealthStatusLevel2Dao {

	public static Properties prop = PropertyReader.getProperties();
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public String getSiteClassificationProduct(String customerId){
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT c.priority,a.productclassification,c.servicetier,c.customersite,a.ciname,a.citype, a.deliverystatus FROM configurationitem a LEFT JOIN ");
		sb.append(" (SELECT b.priority,a.siteid,a.servicetier,a.customersite ");
		sb.append(" FROM site a LEFT JOIN ");
		sb.append(" (SELECT * FROM( ");
		sb.append(" SELECT siteid,priority FROM incident WHERE customeruniqueid = '" + customerId + "' AND incidentstatus NOT IN ('Cancelled','Completed', 'Closed') ");
		sb.append(" order by priority ASC) a ");
		sb.append(" GROUP BY siteid ORDER BY priority ASC) b ");
		sb.append(" ON a.siteid = b.siteid  ");
		sb.append(" WHERE a.customeruniqueid = '" + customerId + "' AND b.priority IS NOT NULL ");
		sb.append(" ) c ");
		sb.append(" ON a.siteid = c.siteid ");
		sb.append(" WHERE a.customeruniqueid = '" + customerId + "' ");
		sb.append(" AND a.deliverystatus IN ('Operational') ");
		sb.append(" AND a.rootProductId IN ('MNS','MSS') ");
        sb.append(" AND c.priority IS NOT NULL ");
		
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] productlist : (List<Object[]>)query.list()) {
				if (productlist[0]!=null) {
					obj = new JSONObject();
					obj.put("priority", productlist[0].toString());
					obj.put("productclassification", productlist[1].toString());
					obj.put("servicetier", productlist[2].toString());
					obj.put("sitename", productlist[3].toString());
					obj.put("ciname", productlist[4].toString());
					obj.put("colour", colorNameToCode(productlist[0].toString()));
					obj.put("citype", productlist[5].toString());
					JSONObjArray.add(obj);
				}
				
			}
		}
		return JSONObjArray.toString();
	}
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("4"))
			return "background-color:rgb(163, 207, 98)";
		else if (color.equalsIgnoreCase("3"))
			return "background-color:rgb(255,255,0)";
		else if (color.equalsIgnoreCase("2"))
			return "background-color:rgb(255, 156, 0)";
		else if (color.equalsIgnoreCase("1"))
			return "background-color:rgb(227, 34, 18)";
		else
			return "background-color:rgb(204,204,204)";
	}
}
