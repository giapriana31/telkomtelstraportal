package com.tt.sitehealthstatuslevel2.mvc.web.controller;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.sitehealthstatuslevel2.mvc.service.SiteHealthLevel2Manager;
import com.tt.utils.TTGenericUtils;

@Controller("SiteHealthLevel2Controller")
@RequestMapping("VIEW")
public class SiteHealthLevel2Controller {

	private SiteHealthLevel2Manager siteHealthLevel2Manager;
	private final static Logger log = Logger.getLogger(SiteHealthLevel2Controller.class);
	
	@Autowired
	@Qualifier("siteHealthLevel2Manager")
	public void setSiteHealthLevel2Manager(SiteHealthLevel2Manager siteHealthLevel2Manager) {
		this.siteHealthLevel2Manager = siteHealthLevel2Manager;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse Response, Model model) throws PortalException,SystemException 
	{
		log.debug("SiteClassificationProduct ----- handleRenderRequest Called");
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(Response));
		log.debug("SiteClassificationProduct ----- handleRenderRequest CustomerID "+customerUniqueID);
		log.debug("SiteClassificationProduct--------------->"+ siteHealthLevel2Manager.getSiteClassificationProduct(customerUniqueID));
		model.addAttribute("SiteClassificationProduct",siteHealthLevel2Manager.getSiteClassificationProduct(customerUniqueID));
		
		log.debug("SiteClassificationProduct ----- return from handleRenderRequest");
		return "sitehealthlevel2";
		
	}
}
