package com.tt.ipscape.mvc.model;


import java.net.URL;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;

import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.logging.Logger;
import com.tt.logticket.mvc.web.controller.TTLogIncidentController;

public class CategoryAPI {
                
                private static Properties properties = PropertyReader.getProperties();
                private final static Logger log = Logger.getLogger(CategoryAPI.class);
                private final String IPSCAPE_ORGANISATION = properties.getProperty("ipscapeorganisation").toString();
                private final String AUTHORIZATION = properties.getProperty("authorization").toString();
                
                public JSONObject getResponseFromAPI(String URL) throws ClientProtocolException,
                                                IOException, org.json.JSONException {
                                URL campaignListURL = new URL(URL);  
                                
                                HttpsURLConnection con =  RefreshUtil.getHttpURLConnection(campaignListURL);
                                BufferedReader br;
                                JSONObject jsonObject = null;
                                log.info("Control in getResponseFromAPI");
                                                
                                con.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
                                con.setRequestProperty("Authorization", AUTHORIZATION);
                                con.connect();
                                int responseCode = con.getResponseCode();
                                log.info("ReadCampaignList RESPONSE CODE ::"+responseCode);
                                if(responseCode == 200){
                                                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                                                String campaignListTraverse;
                                                String campaignList = null;
                                                
                                                while((campaignListTraverse = br.readLine()) != null){
                                                                campaignList = campaignListTraverse;
                                                }
                                                
                                                jsonObject = new JSONObject(campaignList);
                                
                                }else{
                                                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
                                                StringBuffer errorData = null;
                                                while(br.readLine() != null){
                                                                errorData.append(br.readLine());
                                                }
                                                log.info("ReadCampaignList API FAILED :: RESPONSE ::"+errorData);
                                }
                                return jsonObject;
                }

}


