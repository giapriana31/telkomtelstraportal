package com.tt.ipscape.mvc.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.icu.util.Calendar;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.tt.bvdashboard.mvc.dao.BVDashboardDao;
import com.tt.bvdashboard.mvc.web.controller.BVDashBoardController;
import com.tt.ipscape.mvc.dao.IpscapeDaoImpl;
import com.tt.ipscape.mvc.web.controller.IPScapeController;
import com.tt.logging.Logger;
import com.tt.profileSetting.mvc.dao.ProfileSecurityDao;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;

@Service(value = "ipscapeServiceImpl")
@Transactional
public class IpscapeServiceImpl {
                
                private final static Logger log = Logger.getLogger(IpscapeServiceImpl.class);
                private IpscapeDaoImpl ipscapeDaoImpl = new IpscapeDaoImpl();
                
                @Autowired
                @Qualifier("ipscapeDaoImpl")
                public void setProfileSecurityDao(IpscapeDaoImpl ipscapeDaoImpl) {
                                this.ipscapeDaoImpl = ipscapeDaoImpl;
                }
                
                Properties properties = PropertyReader.getProperties();
                private static final String LEAD_TYPE = "Call Back - Queue";
                private final long LIST_ID = Long.parseLong(properties.getProperty("IPScapeListID")); 
                private final String CREATE_LEAD_API =  properties.getProperty("createLeadAPI").toString();
                private final String READ_CAMPAIGN_LIST_API =  properties.getProperty("readCampaignListAPI").toString();
                private final String API_KEY = properties.getProperty("apikey").toString();
                private final String IPSCAPE_ORGANISATION = properties.getProperty("ipscapeorganisation").toString();
                private final String AUTHORIZATION = properties.getProperty("authorization").toString();
                private final String IPSCAPE_LOGIN_API = properties.getProperty("ipscapeloginAPI").toString();
                private final String IPSCAPE_LIST_IDS = properties.getProperty("ipscapelistids").toString();
                private final String TTLabs_Id = properties.getProperty("telkomtelstraLabs").toString();
                private final Map<String, String> campaignAndListIds = new HashMap<String, String>();                
                private final String IPSCAPE_READ_LEAD_LIST_API = properties.getProperty("readLeadListAPI").toString();
                private final String IPSCAPE_UPDATE_LEAD_API = properties.getProperty("updateLeadDetailsAPI").toString();
                
                public String getCustomerName(String customerID){
                                String customerName = null;
                                customerName = ipscapeDaoImpl.getCustomerName(customerID);
                                return customerName;
                }
                
                public com.liferay.portal.kernel.json.JSONArray getCallbackIncompleteDetailsTable(String customerId) throws IOException, JSONException, ParseException{
                    log.info("Control in getCallbackIncompleteDetailsTable SERVICE function");
                    
                    com.liferay.portal.kernel.json.JSONArray jsonData = JSONFactoryUtil.createJSONArray(); //Final JSONArray
                    
                    String customerUniqueId = customerId;  //passed as parameter while calling API method
                    
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Calendar todayDate = Calendar.getInstance();
                    todayDate.add(Calendar.MONTH, -3);
                    String formattedDateLast3Months = dateFormat1.format(todayDate.getTime());   //passed as parameter while calling API method
                    
                    int page=1;//passed as parameter while calling API method
                    
                    JSONObject responseJsonObjectFromAPI = getCallbackIncompleteDetailsFromAPI(customerUniqueId,formattedDateLast3Months,page);
                    JSONArray data = responseJsonObjectFromAPI.getJSONArray("data");
                    
                    int leadsInPage = responseJsonObjectFromAPI.getInt("inPage");
                    int totalLeads = responseJsonObjectFromAPI.getInt("total");
                    
	                  if(totalLeads > leadsInPage){
	                	  
	                	  log.info("Leads are more than 100:::: CallbackIncompleteDetails");
	                	  page++;
	                	  
	                	  responseJsonObjectFromAPI = getCallbackIncompleteDetailsFromAPI(customerUniqueId,formattedDateLast3Months,page);
	                      data.put(responseJsonObjectFromAPI.getJSONArray("data"));
	                      
	                      leadsInPage =  leadsInPage + responseJsonObjectFromAPI.getInt("inPage");
	                	}
	                  
	                  	log.info("data.length() ::CallbackIncompleteDetails/////////////////////"+data.length());
	                  	log.info("data::::CallbackIncompleteDetails///////////////////////"+data);
	                 
	                  		for(int i = 0; i < data.length(); i++){
                            com.liferay.portal.kernel.json.JSONArray jsonRow = JSONFactoryUtil.createJSONArray();
                            
                            JSONObject leadJsonObj = data.getJSONObject(i);
                            String leadId = leadJsonObj.getString("leadId");
                            
                            String callbackTime = leadJsonObj.getString("scheduledCallTimestamp");
                            String phoneNumber = leadJsonObj.getString("phone1");
                            String status = "Active";
                            //String categoryName = "Test"; //leadJsonObj.getString("campaignTitle");
                            
                            String leadData = leadJsonObj.getString("leadData");
                            int tempStringA = leadData.lastIndexOf("CB_Creator_email");
                            int tempStringB = leadData.lastIndexOf("CB_Cancellation");
                            String userEmail = leadData.substring(tempStringA+19, tempStringB-3);
                            
                            int tempStringC = leadData.lastIndexOf("CB_notes");
                            int tempStringD = leadData.lastIndexOf("Company_Name");
                            String notes = leadData.substring(tempStringC+11, tempStringD-3);
                            
                            int tempStringE = leadData.lastIndexOf("CB_creator");
                            int tempStringF = leadData.lastIndexOf("CB_notes");
                            String name = leadData.substring(tempStringE+13, tempStringF-3);
                            
                            int tempStringG = leadData.lastIndexOf("Campaign_Title");
                            int tempStringH = leadData.lastIndexOf("Company_ID");
                            String categoryName = leadData.substring(tempStringG+17, tempStringH-3);
                                                                                            
                            log.info("The userEmail::"+userEmail+" &&&& the Notes::"+notes);
                                                            
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date timeStamp = dateFormat.parse(callbackTime);
                            
                            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                            String formattedDateTime = format.format(timeStamp);
                            
                            //jsonRow.put(leadId);
                            jsonRow.put(name);
                            jsonRow.put(callbackTime);
                            jsonRow.put(phoneNumber);
                            jsonRow.put(status);
                            jsonRow.put(notes);
                            jsonRow.put(categoryName);
                            jsonRow.put(leadId+"@@@"+userEmail);
                            jsonRow.put(leadId+"@@@"+userEmail);
                            jsonData.put(jsonRow);
	                  	}
                  	//}
                  	return jsonData;
                }
                
                public org.json.JSONObject getCallbackIncompleteDetailsFromAPI(String customerId, String formattedDateLast3Months, int page) throws IOException, JSONException, ParseException{
                    log.info("Control in getCallbackIncompleteDetailsFromAPI SERVICE function");
                   
                    String URL= IPSCAPE_READ_LEAD_LIST_API+customerId+"&leadStatus=INCOMPLETE&pageNo="+page+"&perPage=100&loadedDateFrom="+URLEncoder.encode(formattedDateLast3Months, "UTF-8");
                    URL readLeadListURL = new URL(URL);
                    HttpsURLConnection connection3 =  RefreshUtil.getHttpURLConnection(readLeadListURL);
                    connection3.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
                    connection3.setRequestProperty("Authorization", AUTHORIZATION);
                    connection3.connect(); 
                    
                    int responseCode = connection3.getResponseCode();
                    log.info("Active Read Lead List API FORMED::"+ URL);
                    log.info("Response code for Read Lead List::"+ responseCode);
                    
                    JSONObject resultJson = null;
                    
                    if(responseCode == HttpsURLConnection.HTTP_OK){
                                    BufferedReader in = new BufferedReader(new InputStreamReader(connection3.getInputStream()));
                                    String inputLine;
                                    
                                    StringBuffer readLeadListResponse = new StringBuffer();
                                    
                                    while ((inputLine = in.readLine()) != null) 
                                    {
                                                    readLeadListResponse.append(inputLine);
                                    }
                                    in.close();
                                    log.info("INCOMPLETE ReadLeadList response::"+readLeadListResponse.toString());
                                    JSONObject responseJson = new JSONObject(readLeadListResponse.toString());
                                    resultJson = responseJson.getJSONObject("result"); 
                                  }else
                                  {              
                                    BufferedReader in = new BufferedReader(new InputStreamReader(connection3.getErrorStream()));
                                    String inputLine;
                                    
                                    StringBuffer callBackResponse = new StringBuffer();
    
                                    while ((inputLine = in.readLine()) != null) 
                                    {
                                                    callBackResponse.append(inputLine);
                                    }
                                    in.close();
                                    log.info("Callback response::"+callBackResponse.toString());
                                    
                                    //request.getPortletSession().setAttribute("errorMessage",errorDescription);
                                    }
                    return resultJson;
                }
                
                public com.liferay.portal.kernel.json.JSONArray getCallbackCompleteDetailsTable(String customerId) throws IOException, JSONException{
            		log.info("Control in getCallbackCompleteDetailsTable SERVICE function");
                    
            		com.liferay.portal.kernel.json.JSONArray jsonData = JSONFactoryUtil.createJSONArray(); //Final JSONArray
                    
                    String customerUniqueId = customerId;  //passed as parameter while calling API method
                    
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Calendar todayDate = Calendar.getInstance();
                    todayDate.add(Calendar.MONTH, -3);
                    String formattedDateLast3Months = dateFormat.format(todayDate.getTime());   //passed as parameter while calling API method
                    
                    int page=1;//passed as parameter while calling API method
                    
                    JSONObject responseJsonObjectFromAPI = getCallbackCompleteDetailsTableFromAPI(customerUniqueId,formattedDateLast3Months,page);
                    JSONArray data = responseJsonObjectFromAPI.getJSONArray("data");
                    
                    int leadsInPage = responseJsonObjectFromAPI.getInt("inPage");
                    int totalLeads = responseJsonObjectFromAPI.getInt("total");
                    
	                  if(totalLeads > leadsInPage){
	                	  
	                	  log.info("Leads are more than:: CallbackCompleteDetails::::: 100");
	                	  page++;
	                	  
	                	  responseJsonObjectFromAPI = getCallbackCompleteDetailsTableFromAPI(customerUniqueId,formattedDateLast3Months,page);
	                      data.put(responseJsonObjectFromAPI.getJSONArray("data"));
	                      
	                      leadsInPage =  leadsInPage + responseJsonObjectFromAPI.getInt("inPage");
	                	}
	                  
	                  	log.info("data.length():: CallbackCompleteDetails/////////////////////"+data.length());
	                  	log.info("data:: CallbackCompleteDetails///////////////////////"+data);
	                  	
	                  	for(int i = 0; i < data.length(); i++){
                            com.liferay.portal.kernel.json.JSONArray jsonRow = JSONFactoryUtil.createJSONArray();
                            JSONObject leadJsonObj = data.getJSONObject(i);
                            String leadId = leadJsonObj.getString("leadId");
                            
                            String callbackTime = leadJsonObj.getString("scheduledCallTimestamp");
                            String phoneNumber = leadJsonObj.getString("phone1");
                            String status = "Complete";
                            
                            //String categoryName ="Test";
                            //leadJsonObj.getString("campaignTitle");
                            
                            String leadData = leadJsonObj.getString("leadData");
                            int tempStringA = leadData.lastIndexOf("CB_Creator_email");
                            int tempStringB = leadData.lastIndexOf("CB_Cancellation");
                            String userEmail = leadData.substring(tempStringA+19, tempStringB-3);
                            
                            int tempStringC = leadData.lastIndexOf("CB_notes");
                            int tempStringD = leadData.lastIndexOf("Company_Name");
                            String notes = leadData.substring(tempStringC+11, tempStringD-3);
                            
                            int tempStringE = leadData.lastIndexOf("CB_creator");
                            int tempStringF = leadData.lastIndexOf("CB_notes");
                            String name = leadData.substring(tempStringE+13, tempStringF-3);
                            
                            int tempStringG = leadData.lastIndexOf("Campaign_Title");
                            int tempStringH = leadData.lastIndexOf("Company_ID");
                            String categoryName = leadData.substring(tempStringG+17, tempStringH-3);
                            
                            
                            //jsonRow.put(leadId);
                            jsonRow.put(name);
                            jsonRow.put(callbackTime);
                            jsonRow.put(phoneNumber);
                            jsonRow.put(status);
                            jsonRow.put(notes);
                            jsonRow.put(categoryName);
                            jsonData.put(jsonRow);
	                  	}
                	return jsonData;
                }
                
                public org.json.JSONObject getCallbackCompleteDetailsTableFromAPI(String customerId, String formattedDateLast3Months, int page) throws IOException, JSONException{
                    log.info("Control in getCallbackCompleteDetailsTableFromAPI SERVICE function");
                    
                    JSONObject resultJson = null;
                    
                    String URL= IPSCAPE_READ_LEAD_LIST_API+customerId+"&leadStatus=COMPLETE&pageNo=1&perPage=100&loadedDateFrom="+URLEncoder.encode(formattedDateLast3Months, "UTF-8");
                    URL readLeadListURL = new URL(URL);                                  
                    HttpsURLConnection connection4 =  RefreshUtil.getHttpURLConnection(readLeadListURL);
                    connection4.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
                    connection4.setRequestProperty("Authorization", AUTHORIZATION);
                    connection4.connect(); 
                    
                    int responseCode = connection4.getResponseCode();
                    log.info("Response code for Read Lead List::"+ responseCode);
                    log.info("COMPLETE Read Lead List API FORMED::"+ URL);
                    
                    if(responseCode == HttpsURLConnection.HTTP_OK){
                                    BufferedReader in = new BufferedReader(new InputStreamReader(connection4.getInputStream()));
                                    String inputLine;
                                    
                                    StringBuffer readLeadListResponse = new StringBuffer();
    
                                    while ((inputLine = in.readLine()) != null) 
                                    {
                                                    readLeadListResponse.append(inputLine);
                                    }
                                    in.close();
                                    log.info("COMPLETE ReadLeadList response::"+readLeadListResponse.toString());
                                    JSONObject responseJson = new JSONObject(readLeadListResponse.toString());
                                    resultJson = responseJson.getJSONObject("result"); 
                                    }else
                                    {              
	                                    BufferedReader in = new BufferedReader(new InputStreamReader(connection4.getErrorStream()));
	                                    String inputLine;
	                                    
	                                    StringBuffer callBackResponse = new StringBuffer();
	    
	                                    while ((inputLine = in.readLine()) != null) 
	                                    {
	                                                    callBackResponse.append(inputLine);
	                                    }
	                                    in.close();
	                                    log.info("Callback response::"+callBackResponse.toString());
	                                    
	                                    //request.getPortletSession().setAttribute("errorMessage",errorDescription);
                                    }
                    return resultJson;
                }
                
                public String deleteLeadAPI(String leadId) throws IOException{ // /api/v1.0.0/lead/deletelead?leadId=
                                
	                String deleteStatus=null;
	                
	                String[] extractedLeadId = leadId.split("@@@");
	                String URL= IPSCAPE_UPDATE_LEAD_API+"?isComplete=1&leadId="+extractedLeadId[0];
	                URL readLeadListURL = new URL(URL);                                  
	                HttpsURLConnection connection4 =  RefreshUtil.getHttpURLConnection(readLeadListURL);
	                connection4.setRequestMethod("PUT");
	                connection4.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
	                connection4.setRequestProperty("Authorization", AUTHORIZATION);
	                connection4.connect(); 
	                
	                int responseCode = connection4.getResponseCode();
	                log.info("Response code for DELETE LEAD::"+ responseCode);
	                
	                if(responseCode == HttpsURLConnection.HTTP_OK){
	                                deleteStatus = "success";
	                }else{
	                                deleteStatus = "fail";
	                                BufferedReader in = new BufferedReader(new InputStreamReader(connection4.getErrorStream()));
	                                String inputLine;
	                                
	                                StringBuffer deleteLeadResponse = new StringBuffer();
	
	                                while ((inputLine = in.readLine()) != null) 
	                                {
	                                                deleteLeadResponse.append(inputLine);
	                                }
	                                in.close();
	                                log.info("DELETE LEAD FAILURE response::"+deleteLeadResponse.toString());
	                }
	                
	                return deleteStatus;
                }
                
                public void loginIpscapeFunction() throws IOException{
	                String URL= IPSCAPE_LOGIN_API;
	                URL loginURL = new URL(URL);                                  
	                HttpsURLConnection con =  RefreshUtil.getHttpURLConnection(loginURL);
	                BufferedReader br;
	                con.setRequestMethod("POST");
	                con.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
	                con.setRequestProperty("Authorization", AUTHORIZATION);
	                con.setRequestProperty("Content-Length", "19");
	                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	                String urlParametersString="apiKey="+API_KEY;
	                con.setDoOutput(true); 
	                DataOutputStream outWriter = new DataOutputStream(con.getOutputStream());
	                outWriter.writeBytes(urlParametersString);
	                outWriter.flush();
	                outWriter.close();
	                                                
	                con.getResponseCode();
	                log.info("Response code for ipscape Login API::"+con.getResponseCode());
                }
                                
}
