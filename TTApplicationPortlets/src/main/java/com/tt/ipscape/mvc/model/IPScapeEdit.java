package com.tt.ipscape.mvc.model;

public class IPScapeEdit {

	private String notesEdit;
	private String callbackDateEdit;
	private String leadIdEdit;
	private String categoryEdit;
	private String nameEdit;
	private String phoneEdit;
	private String ampmradioEdit;
	
	public String getNotesEdit() {
		return notesEdit;
	}
	public void setNotesEdit(String notesEdit) {
		this.notesEdit = notesEdit;
	}
	public String getCallbackDateEdit() {
		return callbackDateEdit;
	}
	public void setCallbackDateEdit(String callbackDateEdit) {
		this.callbackDateEdit = callbackDateEdit;
	}
	public String getLeadIdEdit() {
		return leadIdEdit;
	}
	public void setLeadIdEdit(String leadIdEdit) {
		this.leadIdEdit = leadIdEdit;
	}
	public String getCategoryEdit() {
		return categoryEdit;
	}
	public void setCategoryEdit(String categoryEdit) {
		this.categoryEdit = categoryEdit;
	}
	public String getNameEdit() {
		return nameEdit;
	}
	public void setNameEdit(String nameEdit) {
		this.nameEdit = nameEdit;
	}
	public String getPhoneEdit() {
		return phoneEdit;
	}
	public void setPhoneEdit(String phoneEdit) {
		this.phoneEdit = phoneEdit;
	}
	public String getAmpmradioEdit() {
		return ampmradioEdit;
	}
	public void setAmpmradioEdit(String ampmradioEdit) {
		this.ampmradioEdit = ampmradioEdit;
	}
	
}
