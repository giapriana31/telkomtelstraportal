package com.tt.ipscape.mvc.web.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.hibernate.validator.util.privilegedactions.GetConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.ipscape.mvc.model.CategoryAPI;
import com.tt.ipscape.mvc.model.IPScape;
import com.tt.ipscape.mvc.model.IPScapeEdit;
import com.tt.ipscape.mvc.service.IpscapeServiceImpl;
import com.tt.logging.Logger;
import com.tt.logticket.mvc.web.controller.TTLogIncidentController;
import com.tt.profileSetting.mvc.dao.ProfileSecurityDao;
import com.tt.profileSetting.mvc.service.ProfileSecurityManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletResponse;

import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.utils.TTGenericUtils;


@Controller("ipScapeController")
@RequestMapping("VIEW")
public class IPScapeController {

                Properties properties = PropertyReader.getProperties();
                private final static Logger log = Logger.getLogger(IPScapeController.class);
                private static final String LEAD_TYPE = "Call Back - Queue";
                private final long LIST_ID = Long.parseLong(properties.getProperty("IPScapeListID")); 
                private final String CREATE_LEAD_API =  properties.getProperty("createLeadAPI").toString();
                private final String READ_CAMPAIGN_LIST_API =  properties.getProperty("readCampaignListAPI").toString();
                private final String API_KEY = properties.getProperty("apikey").toString();
                private final String IPSCAPE_ORGANISATION = properties.getProperty("ipscapeorganisation").toString();
                private final String AUTHORIZATION = properties.getProperty("authorization").toString();
                private final String IPSCAPE_LOGIN_API = properties.getProperty("ipscapeloginAPI").toString();
                private final String IPSCAPE_LIST_IDS = properties.getProperty("ipscapelistids").toString();
                private final String TTLabs_Id = properties.getProperty("telkomtelstraLabs").toString();
                private final Map<String, String> campaignAndListIds = new HashMap<String, String>();
                private final String IPSCAPE_READ_LEAD_LIST_API = properties.getProperty("readLeadListAPI").toString();
                private final String IPSCAPE_UPDATE_LEAD_API = properties.getProperty("updateLeadDetailsAPI").toString();
                
                private IpscapeServiceImpl ipscapeServiceImpl = new IpscapeServiceImpl();
                @Autowired
                @Qualifier("ipscapeServiceImpl")
                public void setProfileSecurityManager(IpscapeServiceImpl ipscapeServiceImpl)
                {
                                this.ipscapeServiceImpl = ipscapeServiceImpl;
                }

                @RenderMapping
                public String handleRenderRequest2(RenderRequest request,
                                                RenderResponse renderResponse, Model model) throws PortalException,
                                                SystemException, IOException, JSONException {
                                //-----------------------CALLING IPSCAPE LOGIN API
                                String URL= IPSCAPE_LOGIN_API;
                                URL loginURL = new URL(URL);                                  
                                HttpsURLConnection con =  RefreshUtil.getHttpURLConnection(loginURL);
                                BufferedReader br;
                                con.setRequestMethod("POST");
                                con.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
                                con.setRequestProperty("Authorization", AUTHORIZATION);
                                con.setRequestProperty("Content-Length", "19");
                                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                                String urlParametersString="apiKey="+API_KEY;
                                con.setDoOutput(true); 
                                DataOutputStream outWriter = new DataOutputStream(con.getOutputStream());
                                outWriter.writeBytes(urlParametersString);
                                outWriter.flush();
                                outWriter.close();
                                                                
                                con.getResponseCode();
                                log.info("Response code for ipscape Login API::"+con.getResponseCode());
                                //-----------------------CALLING IPSCAPE LOGIN API ENDS
                                
                                
                                log.info("RenderMapping function called");
                                TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Jakarta");
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTimeZone(timeZone1);
                                log.info("Hours::"+calendar.get(Calendar.HOUR_OF_DAY));
                                log.info("Minutes::"+calendar.get(Calendar.MINUTE));
                                log.info("Date::"+calendar.get(Calendar.DATE));
                                log.info("Month::"+calendar.get(Calendar.MONTH));
                                log.info("Year::"+calendar.get(Calendar.YEAR));
                                
                                
                                
                                String userName=null;
                                String workContact=null;
                                HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
                                HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
                                User user = UserLocalServiceUtil.getUser((Long)httpServletRequest.getSession().getAttribute(WebKeys.USER_ID));
                                userName=user.getContact().getFullName();
                                String firstName = user.getFirstName();
                                String lastName = user.getLastName(); 
                                String emailId = user.getEmailAddress();
                                String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
                                String customerName = ipscapeServiceImpl.getCustomerName(customerId);
                                String isAdmin = "false";
                                String isNOCManager = "false";
                                
                                List<Role> userCurrentRoles = TTGenericUtils.getUserRoleList(user);
                                //String userRolesString = userCurrentRoles.toString();
                                IPScape ipscapeObj=new IPScape();
                                for(Role role: userCurrentRoles){
                                                log.info("Roles assigned are::::"+ role.getName());
                                                if(role.getName().equalsIgnoreCase("endCustomerAdmin") || role.getName().equalsIgnoreCase("CustomerPortalAdmin")){
                                                                isAdmin = "true";                                                             
                                                }else{
                                                                isAdmin = "false";
                                                }
                                                
                                                if(role.getName().equalsIgnoreCase("NOCManager")){
                                                	isNOCManager = "true";
                                                }
                                                else{
                                                	isNOCManager = "false";
                                                }
                                }
                                                
                                
                                log.info("The current logged in user ROLES ::"+userCurrentRoles);
                                String isExternalUser = TTGenericUtils.isExternal(user)+"";
                                log.info("The current logged in user is External::"+isExternalUser);
                                
                                
                                                                
                                // TO GET THE COMPANY NAME
                                //String companyName=(String)httpServletRequest.getSession().getAttribute("LIFERAY_SHARED_customerName_KEY");
                                workContact=(String) user.getExpandoBridge().getAttribute("WorkContact");
                                
                                ipscapeObj.setCustomerName(userName);
                                ipscapeObj.setTelephone(workContact);
                                ipscapeObj.setFirstName(firstName);
                                ipscapeObj.setLastName(lastName);
                                ipscapeObj.setHoursAsiaJakarta(calendar.get(Calendar.HOUR_OF_DAY)+"");
                                ipscapeObj.setMinutesAsiaJakarta(calendar.get(Calendar.MINUTE)+"");
                                ipscapeObj.setDateAsiaJakarta(calendar.get(Calendar.DATE)+"");
                                ipscapeObj.setMonthsAsiaJakarta(calendar.get(Calendar.MONTH)+"");
                                ipscapeObj.setYearAsiaJakarta(calendar.get(Calendar.YEAR)+"");
                                ipscapeObj.setCompanyName(customerName);
                                ipscapeObj.setEmailId(emailId);
                                ipscapeObj.setIsExternalUser(isExternalUser);
                                ipscapeObj.setUserAdminRole(isAdmin);
                                ipscapeObj.setIsNOCManager(isNOCManager);
                                if(TTLabs_Id.equals(customerId)){
                                                log.info("Properties customer::"+ TTLabs_Id +"-------Logged in Customer::"+customerId);
                                                ipscapeObj.setIsTTlabs("true");
                                }else{
                                                ipscapeObj.setIsTTlabs("false");
                                }
                                
                                                                
                                model.addAttribute("ipscapeObj", ipscapeObj);
                                
                                return "ipScape";
                }
                
                //-------------------------------------------- SubmitResource URL CREATE LEAD API---------------------
                @ResourceMapping(value = "submitResourceURLIPScape")
                public void submitResourceURL(@ModelAttribute("ipscapeObj") IPScape ipscapeObj, Model model,ResourceRequest request, ResourceResponse response) throws IOException, PortletException, ParseException, JSONException 
                {              
                                log.info("Controll on IPScape Submit resource URL");
                                JSONObject json = new JSONObject();
                                UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
                                
                                
                                PrintWriter out = response.getWriter();
                                String firstName = uploadRequest.getParameter("firstName");
                                String lastName = uploadRequest.getParameter("lastName");
                                String phoneNumber = uploadRequest.getParameter("telephone");
                                String campaignId = uploadRequest.getParameter("category");
                                String specificTime = uploadRequest.getParameter("specificTime");
                                String rangeTime = uploadRequest.getParameter("rangeTime");
                                String callDate = uploadRequest.getParameter("callbackDate");
                                String comments = uploadRequest.getParameter("comments");
                                String ampmradio = uploadRequest.getParameter("ampmradio");
                        
                                String campaignTitle = "";
                                
                                
                                
                                CategoryAPI obj2 = new CategoryAPI();
                                String URL = READ_CAMPAIGN_LIST_API + "1"+"&perPage=100";
                                System.out.println("ReadCmpaign Again At Submit ::::: "+URL);
                                JSONObject campaignIdTitleMapJson2 = obj2.getResponseFromAPI(URL);
                                log.info("Response for getCampaignList API::" + campaignIdTitleMapJson2.toString());
                               
                                
                                JSONObject resultJsonCampaignMap = campaignIdTitleMapJson2.getJSONObject("result"); 
                                org.json.JSONArray dataCampaignMap = resultJsonCampaignMap.getJSONArray("data");
                                
                                log.info("dataCampaignMap::::::::"+dataCampaignMap);
                                log.info("dataCampaignMap length::::::::::::"+dataCampaignMap.length());
                                
                                for(int i = 0; i < dataCampaignMap.length(); i++){
                                	JSONObject campaignJsonObj = dataCampaignMap.getJSONObject(i);
                                	
                                	log.debug("campaignJsonObj"+campaignJsonObj);
                                	
                                    String campaignIdFromAPI = campaignJsonObj.getString("campaignId");
                                    
                                    log.info("campaignIdFromAPI:::::"+campaignIdFromAPI);
                                	if(campaignId.equalsIgnoreCase(campaignIdFromAPI)){
                                		campaignTitle = campaignJsonObj.getString("campaignTitle");
                                		break;
                                	}
                                }
                           
                                log.info("first name::"+firstName);
                                log.info("last name::"+lastName);
                                log.info("Telephone::"+phoneNumber);
                                log.info("Category::"+campaignId);
                                log.info("Specific Time::"+specificTime);
                                log.info("Range Time::"+rangeTime);
                                log.info("Call back Date::"+callDate);
                                log.info("Comments::"+comments);
                                log.info("AM radio::"+ampmradio);
                                //log.info("PM radio::"+pmradio);
                                
                                String[] listIdsArray = IPSCAPE_LIST_IDS.split(",");
                                for(int i=0;i<listIdsArray.length;i++){
                                                String[] listId = listIdsArray[i].split(":");
                                                campaignAndListIds.put(listId[0], listId[1]);
                                }
                                
                                log.info("The campaign and List id MAP::"+campaignAndListIds);
                                
                                //---------------------------- get the parameters - campaignid , telephone number, customer key 
                                                                HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
                                                                HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
                                                                                
                                                                
                                                                
                                                                String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
                                                                
                                                                String customerName = ipscapeServiceImpl.getCustomerName(customerId);
                                                                
                                                                User user = TTGenericUtils.getLoggedInUser(httpServletRequest);
                                                                String userId = String.valueOf(user.getUserId());
                                                                
                                                                String customerKey = customerId + ":" + userId; //Not yet used. We are setting doUpdate to 0.
                                                                String email = user.getEmailAddress();
                                                                
                                                                TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Jakarta");
                                                                Calendar calendar = Calendar.getInstance();
                                                                SimpleDateFormat formatCallbackCreatedDate = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
                                                                formatCallbackCreatedDate.setTimeZone(timeZone1);
                                                                String callBackCreatedDate = formatCallbackCreatedDate.format(calendar.getTime()) +"";
                                                                log.info("Created DATE"+callBackCreatedDate);
                                                                
                                                                String leadData = "{\"Company_ID\":"+ "\""+customerId+"\""
                                                                                                        +",\"Company_Name\":"+ "\""+customerName+"\""
                                                                                                        +",\"CB_Created_Date\":"+ "\""+callBackCreatedDate+"\""
                                                                                                        +",\"CB_notes\":"+ "\""+comments+"\""
                                                                                                        +",\"CB_Creator_email\":"+ "\""+email+"\""
                                                                                                        +",\"Campaign_Title\":"+ "\""+campaignTitle+"\""
                                                                                                        +",\"CB_creator\":"+ "\""+firstName+" "+lastName+"\""+"}";
                                                                log.info("Request.get paramenter::::"+leadData);
                                                                
                                                                
                                                                String encodedPhoneNumber = URLEncoder.encode(phoneNumber, "UTF-8");
                                                                long listId = Long.parseLong(campaignAndListIds.get(campaignId));
                                                                long doUpdate = 0;
                                                                
                                                                                                
                                                                String callTime = "";
                                                                
                                                                if(rangeTime != null && rangeTime != "")
                                                                {
                                                                                if(ampmradio != null && ampmradio.equalsIgnoreCase("pm") && !rangeTime.contains("12:")){
                                                                                                String[] pmTime = rangeTime.split(":");
                                                                                                Integer time = Integer.parseInt(pmTime[0]);
                                                                                                time = time + 12;
                                                                                                pmTime[0] = time.toString();
                                                                                                rangeTime = pmTime[0]+":"+pmTime[1];             
                                                                                                
                                                                                }else if(ampmradio != null && ampmradio.equalsIgnoreCase("am") && rangeTime.contains("12:")){
                                                                                                String[] amTime = rangeTime.split(":");
                                                                                                amTime[0] = "00";
                                                                                                rangeTime = amTime[0]+":"+amTime[1];              
                                                                                }
                                                                                log.info("Range Time in 24 hours::"+rangeTime);
                                                                                callTime = rangeTime + ":00";
                                                                                
                                                                }
                                                                else
                                                                {
                                                                                Calendar calender = Calendar.getInstance();
                                                                                String hours = String.valueOf(calender.get(Calendar.HOUR_OF_DAY));
                                                                                String minutes = String.valueOf(calender.get(Calendar.MINUTE));
                                                                                callTime = hours + ":" + minutes + ":00";
                                                                }
                                                                
                                                                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                                                Date timeStamp = format.parse(callDate + " " + callTime);
                                                                //Calendar cal = Calendar.getInstance();
                                                                //cal.set(year, month, date, hourOfDay, minute, second);
                                                                
                                                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                                String formattedDate = dateFormat.format(timeStamp);
                                                                log.info("final formatted date :: " + formattedDate);
                                                                
                                                                
                                                                //----------------- calling the ipscape apis 
                                                                //String urlString = "https://apisandbox.ipscape.com.au/api/v1.0.0/lead/createlead";
                                                                String urlString = CREATE_LEAD_API;
                                                                String urlParametersString = "campaignId=" + campaignId +  "&earliestCallTimestamp=" + formattedDate 
                                                                                                + "&listId=" + listId + "&phone1=" + encodedPhoneNumber
                                                                                                + "&leadData=" + leadData + "&leadType=" + LEAD_TYPE + "&customText1="+ customerName + "&customDate1="+ formattedDate + "&customText2="+ customerId;
                                                                String encodedURI = URLEncoder.encode(urlParametersString, "UTF-8");
                                                                log.info("Encoded URI::"+encodedURI);
                                                                
                                                                
                                                                URL createLeadURL = new URL(urlString);
                                                                HttpsURLConnection connection = RefreshUtil.getHttpURLConnection(createLeadURL);
                                                                connection.setRequestMethod("POST");
                                                                connection.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
                                                                connection.setRequestProperty("Authorization", AUTHORIZATION);
                                                                connection.setDoOutput(true); 
                                                                
                                                                DataOutputStream outWriter = new DataOutputStream(connection.getOutputStream());
                                                                outWriter.writeBytes(urlParametersString);
                                                                outWriter.flush();
                                                                outWriter.close();
                                                                
                                                                int responseCode = connection.getResponseCode();
                                                                String responseMessage = connection.getResponseMessage(); 
                                                                
                                                                                                
                                                                log.info("Post parameters : " + urlParametersString);
                                                                log.info("Response Code : " + responseCode);
                                                                log.info("Response Message : " + responseMessage);
                                                                
                                                                if(responseCode == HttpsURLConnection.HTTP_OK)
                                                                {
                                                                                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                                                                String inputLine;
                                                                                
                                                                                StringBuffer callBackResponse = new StringBuffer();
                                                
                                                                                while ((inputLine = in.readLine()) != null) 
                                                                                {
                                                                                                callBackResponse.append(inputLine);
                                                                                }
                                                                                in.close();
                                                                                log.info("Create Lead response::"+callBackResponse.toString());
                                                                                JSONObject responseJson = new JSONObject(callBackResponse.toString());
                                                                                String result = responseJson.getString("resultCode");
                                                                                
                                                                                if("success".equalsIgnoreCase(result))
                                                                                {
                                                                                                //SessionMessages.add(request.getPortletSession(), "callback request success");
                                                                                                json.put("ipscape", "success");
                                                                                }
                                                                                
                                                                }
                                                                else
                                                                {              
                                                                                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                                                                                String inputLine;
                                                                                
                                                                                StringBuffer callBackResponse = new StringBuffer();
                                                
                                                                                while ((inputLine = in.readLine()) != null) 
                                                                                {
                                                                                                callBackResponse.append(inputLine);
                                                                                }
                                                                                in.close();
                                                                                log.info("Create Lead response::"+callBackResponse.toString());
                                                                                json.put("ipscape", "fail");
                                                                                //request.getPortletSession().setAttribute("errorMessage",errorDescription);
                                                                }
                                                                
                                                                out.print(json.toString());
                }

                
                
                @ResourceMapping(value="getCampaignList")
                public void getCampaignList(ResourceRequest request, ResourceResponse response) throws ClientProtocolException, IOException, JSONException
                {              log.info("Control in getCampaignList");
                                PrintWriter out = response.getWriter();
                                //-----------------API Start
                                CategoryAPI obj = new CategoryAPI();

                                
                                //String URL = "https://apisandbox.ipscape.com.au/api/v1.0.0/campaign/readcampaignslist?pageNo="+ request.getParameter("pageNo")+"&perPage=100";
                                String URL = READ_CAMPAIGN_LIST_API + request.getParameter("pageNo")+"&perPage=100";
                                System.out.println("Phat ra hai API::"+URL);
                                JSONObject campaignIdTitleMapJson = obj.getResponseFromAPI(URL);
                                log.info("Response for getCampaignList API::" + campaignIdTitleMapJson.toString());
                                out.print(campaignIdTitleMapJson);
                } 
                
                //ACTIVE
                @ResourceMapping(value = "incompleteCallbackDetailsURL")
                public void getIncompleteCallbackDetailsTable(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, JSONException, ParseException
                {              
                                log.info("Control in getIncompleteCallbackDetailsTable()");
                                HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
                                HttpServletResponse response = PortalUtil.getHttpServletResponse(resourceResponse);
                                UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
                                
                                String customerId = TTGenericUtils.getCustomerIDFromSession(request, response);
                                
                                JSONArray jsonData = ipscapeServiceImpl.getCallbackIncompleteDetailsTable(customerId);
                                response.getWriter().print(jsonData.toString());
                }
                
                @ResourceMapping(value = "completeCallbackDetailsURL")
                public void getCompleteCallbackDetailsTable(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, JSONException
                {              
                                log.info("Control in getCompleteCallbackDetailsTable()");
                                HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
                                HttpServletResponse response = PortalUtil.getHttpServletResponse(resourceResponse);
                                UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
                                
                                String customerId = TTGenericUtils.getCustomerIDFromSession(request, response);
                                
                                JSONArray jsonData = ipscapeServiceImpl.getCallbackCompleteDetailsTable(customerId);
                                response.getWriter().print(jsonData.toString());
                }
                
                
                @ResourceMapping(value = "deleteCallbackURL")
                public void deleteCallback(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, JSONException
                {              
                                log.info("Control in deleteCallback");
                                
                                JSONObject json = new JSONObject();
                                HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
                                HttpServletResponse response = PortalUtil.getHttpServletResponse(resourceResponse);
                                UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
                                PrintWriter out = response.getWriter();
                                String leadId = uploadRequest.getParameter("leadId");
                                log.info("Delete callback List in controller::"+leadId);
                                String cancelStatus = ipscapeServiceImpl.deleteLeadAPI(leadId);
                                log.info("The deletion status::"+ cancelStatus);
                                
                                json.put("deleteCallback", "success");
                                //out.print(json.toString());
                                response.getWriter().print(json.toString());
                }
                
                @ResourceMapping(value = "submitEditLeadIPScapeURL")
                public void updapeLead(@ModelAttribute("ipscapeEditObj") IPScapeEdit ipscapeEditObj, Model model, ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, ParseException, JSONException{
                                log.info("Control EDIT LEAD function");
                                
                                HttpServletRequest request = PortalUtil.getHttpServletRequest(resourceRequest);
                                HttpServletResponse response = PortalUtil.getHttpServletResponse(resourceResponse);
                                UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(resourceRequest);
                                
                                String leadId = uploadRequest.getParameter("leadIdEdit");
                                String phoneNumberEdit = uploadRequest.getParameter("phoneEdit");
                                String callbackDateEdit = uploadRequest.getParameter("callbackDateEdit");
                                String ampmRadioEdit = uploadRequest.getParameter("ampmradioEdit");
                                String notesEdit = uploadRequest.getParameter("notesEdit");
                                String rangeTimeEdit = uploadRequest.getParameter("rangeTimeEdit");
                                

                                log.info("LEAD ID ---------------------------"+leadId);
                                log.info("phoneNumberEdit ---------------------------"+phoneNumberEdit);
                                log.info("callbackDateEdit ---------------------------"+callbackDateEdit);
                                log.info("ampmRadioEdit ---------------------------"+ampmRadioEdit);
                                log.info("notesEdit ---------------------------"+notesEdit);
                                
                                
                                String callTime = "";
                                if(rangeTimeEdit != null && rangeTimeEdit != "")
                                {
                                                if(ampmRadioEdit != null && ampmRadioEdit.equalsIgnoreCase("pm") && !rangeTimeEdit.contains("12:")){
                                                                String[] pmTime = rangeTimeEdit.split(":");
                                                                Integer time = Integer.parseInt(pmTime[0]);
                                                                time = time + 12;
                                                                pmTime[0] = time.toString();
                                                                rangeTimeEdit = pmTime[0]+":"+pmTime[1];     
                                                             log.info("ampmradioEdit::: Contains pm values ::::"+rangeTimeEdit+ampmRadioEdit);   
                                                }else if(ampmRadioEdit != null && ampmRadioEdit.equalsIgnoreCase("am") && rangeTimeEdit.contains("12:")){
                                                                String[] amTime = rangeTimeEdit.split(":");
                                                                amTime[0] = "00";
                                                                rangeTimeEdit = amTime[0]+":"+amTime[1]; 
                                                            log.info("ampmradioEdit::: Contains am values ::::"+rangeTimeEdit+ampmRadioEdit);  
                                                }
                                                log.info("Range Time in 24 hours::"+rangeTimeEdit);
                                                callTime = rangeTimeEdit + ":00";
                                                log.info("changing rangetime in 24 hours in formst HH:MM:SS"+callTime);
                                                
                                }
                                else
                                {
                                                Calendar calender = Calendar.getInstance();
                                                String hours = String.valueOf(calender.get(Calendar.HOUR_OF_DAY));
                                                String minutes = String.valueOf(calender.get(Calendar.MINUTE));
                                                callTime = hours + ":" + minutes + ":00";
                                                log.info("value of calltime when rangetimeEdit recieved is null"+callTime);
                                }
                                
                                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                Date timeStamp = format.parse(callbackDateEdit + " " + callTime);
                                
                                log.info("value of date an time stamp after date format parse to dd/MM/yyyy HH:mm:ss"+timeStamp);
                  
                                
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String formattedDateAPI = dateFormat.format(timeStamp);
                                log.info("final formatted EDIT date :: " + formattedDateAPI);
                                
                                
                                String notesLeadData = "{\"CB_notes\":\""+notesEdit+"\"}";
                                String encodedPhoneNumber = URLEncoder.encode(phoneNumberEdit, "UTF-8");
                                
                                String urlString = IPSCAPE_UPDATE_LEAD_API;
                                String urlParametersString = "leadId=" + leadId 
			                                                            +  "&scheduledCallTimestamp="+ formattedDateAPI 
			                                                            + "&phone1=" + encodedPhoneNumber
			                                                            + "&leadData=" + notesLeadData;
                                String encodedURI = URLEncoder.encode(urlParametersString, "UTF-8");
                                log.info("Encoded URI::"+encodedURI);
                                
                                
                                URL createLeadURL = new URL(urlString);
                                HttpsURLConnection connection = RefreshUtil.getHttpURLConnection(createLeadURL);
                                connection.setRequestMethod("POST");
                                connection.setRequestProperty("X-Ipscape-org", IPSCAPE_ORGANISATION);
                                connection.setRequestProperty("Authorization", AUTHORIZATION);
                                connection.setDoOutput(true); 
                                
                                DataOutputStream outWriter = new DataOutputStream(connection.getOutputStream());
                                outWriter.writeBytes(urlParametersString);
                                outWriter.flush();
                                outWriter.close();
                                
                                int responseCode = connection.getResponseCode();
                                String responseMessage = connection.getResponseMessage(); 
                                
                                                
                                log.info("Post parameters : " + urlParametersString);
                                log.info("Response Code : " + responseCode);
                                log.info("Response Message : " + responseMessage);
                                
                                JSONObject json = new JSONObject();
                                PrintWriter out = response.getWriter();
                                if(responseCode == HttpsURLConnection.HTTP_OK)
                                {
                                                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                                String inputLine;
                                                
                                                StringBuffer updateLeadResponse = new StringBuffer();
                
                                                while ((inputLine = in.readLine()) != null) 
                                                {
                                                                updateLeadResponse.append(inputLine);
                                                }
                                                in.close();
                                                log.info("Update Lead response::"+updateLeadResponse.toString());
                                                JSONObject responseJson = new JSONObject(updateLeadResponse.toString());
                                                String result = responseJson.getString("resultCode");
                                                
                                                if("success".equalsIgnoreCase(result))
                                                {
                                                                //SessionMessages.add(request.getPortletSession(), "callback request success");
                                                                json.put("update", "success");
                                                }
                                                
                                }
                                else
                                {              
                                                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                                                String inputLine;
                                                
                                                StringBuffer updateLeadResponse = new StringBuffer();
                
                                                while ((inputLine = in.readLine()) != null) 
                                                {
                                                                updateLeadResponse.append(inputLine);
                                                }
                                                in.close();
                                                log.info("Update lead response::"+updateLeadResponse.toString());
                                                json.put("update", "fail");
                                                //request.getPortletSession().setAttribute("errorMessage",errorDescription);
                                }
                                
                                out.print(json.toString());
                                
                                
                                
                }
                
}
