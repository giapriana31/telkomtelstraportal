package com.tt.ipscape.mvc.model;


public class IPScape {
	private String customerName;
	private String firstName;
	private String lastName;
	private String telephone;
	private String category;
	private String callbackDate;
	private String timeradio;
	private String specificTime;
	private String rangeTime;
	private String comments;
	private String amradio;
	private String pmradio;
	private String ampmradio;
	private String hoursAsiaJakarta;
	private String minutesAsiaJakarta;
	private String dateAsiaJakarta;
	private String monthsAsiaJakarta;
	private String yearAsiaJakarta;
	private String companyName;
	private String emailId;
	private String isExternalUser;
	private String userAdminRole;
	private String isTTlabs;
	private String isNOCManager;
	

	
	public String getIsNOCManager() {
		return isNOCManager;
	}
	public void setIsNOCManager(String isNOCManager) {
		this.isNOCManager = isNOCManager;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSpecificTime() {
		return specificTime;
	}
	public void setSpecificTime(String specificTime) {
		this.specificTime = specificTime;
	}
	public String getRangeTime() {
		return rangeTime;
	}
	public void setRangeTime(String rangeTime) {
		this.rangeTime = rangeTime;
	}
	public String getCallbackDate() {
		return callbackDate;
	}
	public void setCallbackDate(String callbackDate) {
		this.callbackDate = callbackDate;
	}
	public String getTimeradio() {
		return timeradio;
	}
	public void setTimeradio(String timeradio) {
		this.timeradio = timeradio;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getAmradio() {
		return amradio;
	}
	public void setAmradio(String amradio) {
		this.amradio = amradio;
	}
	public String getPmradio() {
		return pmradio;
	}
	public void setPmradio(String pmradio) {
		this.pmradio = pmradio;
	}
	public String getAmpmradio() {
		return ampmradio;
	}
	public void setAmpmradio(String ampmradio) {
		this.ampmradio = ampmradio;
	}
	public String getHoursAsiaJakarta() {
		return hoursAsiaJakarta;
	}
	public void setHoursAsiaJakarta(String hoursAsiaJakarta) {
		this.hoursAsiaJakarta = hoursAsiaJakarta;
	}
	public String getMinutesAsiaJakarta() {
		return minutesAsiaJakarta;
	}
	public void setMinutesAsiaJakarta(String minutesAsiaJakarta) {
		this.minutesAsiaJakarta = minutesAsiaJakarta;
	}
	public String getMonthsAsiaJakarta() {
		return monthsAsiaJakarta;
	}
	public void setMonthsAsiaJakarta(String monthsAsiaJakarta) {
		this.monthsAsiaJakarta = monthsAsiaJakarta;
	}
	public String getYearAsiaJakarta() {
		return yearAsiaJakarta;
	}
	public void setYearAsiaJakarta(String yearAsiaJakarta) {
		this.yearAsiaJakarta = yearAsiaJakarta;
	}
	public String getDateAsiaJakarta() {
		return dateAsiaJakarta;
	}
	public void setDateAsiaJakarta(String dateAsiaJakarta) {
		this.dateAsiaJakarta = dateAsiaJakarta;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getIsExternalUser() {
		return isExternalUser;
	}
	public void setIsExternalUser(String isExternalUser) {
		this.isExternalUser = isExternalUser;
	}
	public String getUserAdminRole() {
		return userAdminRole;
	}
	public void setUserAdminRole(String userAdminRole) {
		this.userAdminRole = userAdminRole;
	}
	public String getIsTTlabs() {
		return isTTlabs;
	}
	public void setIsTTlabs(String isTTlabs) {
		this.isTTlabs = isTTlabs;
	}
	
	
}
