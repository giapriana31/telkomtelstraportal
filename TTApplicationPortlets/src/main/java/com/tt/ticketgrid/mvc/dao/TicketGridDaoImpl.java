package com.tt.ticketgrid.mvc.dao;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.constants.TTPortalAppErrors;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;
import com.tt.ticketgrid.mvc.model.CompositeIncidentMetrics;
import com.tt.ticketgrid.mvc.model.DatePair;
import com.tt.utils.PropertyReader;

@Transactional
@Repository(value = "ticketGridDAO")
public class TicketGridDaoImpl {
	
	public static Properties prop=PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(TicketGridDaoImpl.class);
	private static final String saasRootProduct=prop.getProperty("saasRootProduct");

	@Autowired
	private SessionFactory sessionFactory;

	public List<Integer> getIncidentTrends(List<DatePair> cycleDateRange,
			String customerId, String incCategory, int numberOfWeeks)
			throws TTPortalAppException {

		log.info("getIncidentTrend DAOImpl start");
		List<Integer> incidentTrends = null;

		try {

			incidentTrends = new ArrayList<Integer>();
			StringBuilder hsql = new StringBuilder();
			hsql.append("select ");
			// -- Loop to get count of completed incidents for different
			// weeks---------

			for (int i = 0; i < numberOfWeeks; i++) {

				hsql.append("(select count(1) from incident i ");
				hsql.append(" where (i.createddate between '");
				hsql.append(cycleDateRange.get(i).getStartOfWeek())
						.append("' and '")
						.append(cycleDateRange.get(i).getEndOfWeek())
						.append("')");

				if (incCategory.equalsIgnoreCase("network")) {
					hsql.append(" and i.category in ('Network')");
				} else if (incCategory.equalsIgnoreCase("sites")) {
					hsql.append(" and i.category not in ('Network')");
				} else if (incCategory.equalsIgnoreCase("saas")) {
					hsql.append(" and i.category in (select ind.rootproductid from incident ind where ind.customeruniqueid='"+customerId+"') and i.incidentstatus in ('Closed','Completed','Cancelled') and i.rootproductid in (select p.productname from producttype p where p.rootproductname='"+saasRootProduct+"') and i.incidentid in (select im.incidentid from incddashboardmetrics im where im.incidentstatus in ('Closed','Cancelled','Completed') and  im.customeruniqueid ='"+customerId+"' AND  im.citype   = 'Service')");
				} else if (incCategory.equalsIgnoreCase("cloudinfrastructure")) {
					hsql.append(" and i.category in ('Private Cloud')");
				}

				hsql.append(
						" and i.customeruniqueid ='"+customerId+"' and i.incidentstatus in ('Closed','Completed','Cancelled') and i.incidentid in (select imo.incidentid from incddashboardmetrics imo where imo.incidentstatus in ('Closed','Cancelled','Completed') and  imo.customeruniqueid ='"+customerId+"' AND  imo.citype   = 'Service')")
						.append(")");

				if (i < (numberOfWeeks - 1)) {
					hsql.append(",");
				}

			}

			hsql.append(" from dual");

			log.info("Search query for getting Completed Incident Graph data----------"
					+ hsql);

			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					hsql.toString());
			Object[] result = (Object[]) query.uniqueResult();

			for (int i = 0; i < result.length; i++) {
				Integer count = ((BigInteger) result[i]).intValue();
				incidentTrends.add(count);
			}

			return incidentTrends;

		} catch (Exception e) {
			throw new TTPortalAppException(e, "SS:1111",
					TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}

	}

	public CompositeIncidentMetrics getDashboardMappings(String customerId,
			String status) throws TTPortalAppException {
		try {

			log.info("CompositeIncidentMetrics started");
			CompositeIncidentMetrics compositeIncidentMetrics = new CompositeIncidentMetrics();

			StringBuilder hsql = new StringBuilder();

			hsql.append("select distinct(ci.cmdblass) from Configuration ci");
			hsql.append(" where ci.citype = 'Service' and customeruniqueid =:customerId and ci.status in ('");
			hsql.append(GenericConstants.STATUSCOMMISSIONING);
			hsql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
			hsql.append(GenericConstants.STATUSOPERATIONAL);
			hsql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);

			Query queryInitial = sessionFactory.getCurrentSession()
					.createQuery(hsql.toString());

			queryInitial.setString("customerId", customerId);

			if (queryInitial.list() != null && queryInitial.list().size() > 0) {
				log.debug("list of cmdbclases-------" + queryInitial.list());
				compositeIncidentMetrics.setDistinctServiceClasses(queryInitial
						.list());

				StringBuilder hql = new StringBuilder();
				hql.append("from IncidentDashboardMapping idm");
				if (status.equalsIgnoreCase("active")) {
					hql.append(" where idm.citype = 'Service' and (idm.incidentstatus not in ('Closed','Cancelled','Completed')) and idm.customeruniqueid =:customerId ");
				} else {
					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.DAY_OF_WEEK,
							-(cal.get(Calendar.DAY_OF_WEEK) - 1));
					java.util.Date currentDate = cal.getTime();
					int period = (-1) * 84;
					cal.add(Calendar.DATE, period);
					java.util.Date startDate = cal.getTime();

					cal.setTime(currentDate);
					cal.add(Calendar.DATE, -1);
					currentDate = cal.getTime();

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
							"yyyy-MM-dd");
					String currentDateString = simpleDateFormat
							.format(currentDate);
					currentDateString = currentDateString.concat(" 23:59:59");
					String startDateString = simpleDateFormat.format(startDate);
					startDateString = startDateString.concat(" 00:00:00");
					hql.append(
							" where idm.citype = 'Service' and (idm.incidentstatus in ('Closed','Cancelled','Completed')) and idm.customeruniqueid =:customerId and createddate between '")
							.append(startDateString).append("' and '")
							.append(currentDateString).append("'");
				}
				Query query = sessionFactory.getCurrentSession().createQuery(
						hql.toString());
				query.setString("customerId", customerId);
				log.debug("cust passed as input:::" + customerId);
				if (query.list() != null && query.list().size() > 0) {
					log.debug("query.list()---->>>" + query.list().get(0));
					compositeIncidentMetrics.setIncidentDashboardMappings(query
							.list());
					log.info("CompositeIncidentMetrics end");
					return compositeIncidentMetrics;
				} else {
					log.info("CompositeIncidentMetrics end");
					return compositeIncidentMetrics;
				}
			}
			log.info("sessionFactory end");
			return null;

		} catch (Exception e) {
			throw new TTPortalAppException(e, "SS:1111",
					TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}
	}

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String, ArrayList<String>> getTicketGridData(
			String customerId, String status) {
		LinkedHashMap<String, ArrayList<String>> ticketGridDataResults = null;

		try {

			log.info("getTicketGridData DAO level started");

			StringBuilder serviceQuery = new StringBuilder();
			serviceQuery.append(" select product,productId,rootproductid from( ");
			serviceQuery.append(" select distinct(case when ci.rootproductid='SaaS' and ci.productId Like '%Whispir%' then 'Multi Channel Communication' ");
			serviceQuery.append(" when ci.rootproductid='SaaS' and ci.productId Like '%Mandoe%' then 'Digital Proximity' ");
			serviceQuery.append(" when ci.rootproductid='SaaS' and ci.productId Like '%IPScape%' then 'Cloud Contact Centre'  ");
			serviceQuery.append(" when ci.rootproductid='SaaS' then ci.productid  ");
			serviceQuery.append(" else ci.productclassification end) product, ci.productId, ci.rootproductid ");
			serviceQuery.append(" from configurationitem ci  ");
			serviceQuery.append(" where ci.citype = 'Service' and ci.customeruniqueid='"+customerId+"' and ci.deliverystatus in ('Operational') ");
			serviceQuery.append(" ) a where product <>'' and product <> 'NA' ");

			/*serviceQuery.append("select distinct(case when ci.rootproductid='SaaS' and ci.productId Like 'Mandoe%' then 'Mandoe' when  ci.rootproductid='SaaS' then ci.productid else ci.rootproductid end) from configurationitem ci where ci.citype = 'Service' and ci.customeruniqueid='").append(customerId).append("' and ci.status in ('");
			serviceQuery.append(GenericConstants.STATUSCOMMISSIONING);
			serviceQuery.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
			serviceQuery.append(GenericConstants.STATUSOPERATIONAL);
			serviceQuery.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);*/

			Query queryForService = sessionFactory.getCurrentSession()
					.createSQLQuery(serviceQuery.toString());

			if (queryForService.list() != null
					&& queryForService.list().size() > 0) {
				log.debug("list of services(cmdbclasses)-------"
						+ queryForService.list());

				ticketGridDataResults = new LinkedHashMap<String, ArrayList<String>>();

				//for (int i = 0; i < queryForService.list().size(); i++) {
				for (Object[] productlist : (List<Object[]>)queryForService.list()) {
					if (productlist[0]!=null) {

					String service = productlist[0].toString();
					String productId = productlist[1].toString();
					String rootproductid = productlist[2].toString();
					log.debug("TicketGridDaoImpl service : " + service + " productId : "+productId+ " rootproductid : "+ rootproductid);

					StringBuilder queryBuilder = new StringBuilder();
					queryBuilder.append("select ");

					if (status.equalsIgnoreCase("active")) {
						if(rootproductid.equalsIgnoreCase("SaaS")){
							queryBuilder
							.append("(select count(distinct(a.incidentid)) from incidentdashboard a where a.citype ='Service' and a.incidentstatus not in ('Closed','Cancelled','Completed') and a.customeruniqueid='"
									+ customerId
									+ "' and a.productId in ('"+ productId + "') and a.priority='1'),");
							queryBuilder
									.append("(select count(distinct(b.incidentid)) from incidentdashboard b where b.citype ='Service' and b.incidentstatus not in ('Closed','Cancelled','Completed') and b.customeruniqueid='"
											+ customerId
											+ "' and b.productId in ('"+ productId + "') and b.priority='2'),");
							queryBuilder
									.append("(select count(distinct(c.incidentid)) from incidentdashboard c where c.citype ='Service' and c.incidentstatus not in ('Closed','Cancelled','Completed') and c.customeruniqueid='"
											+ customerId
											+ "' and c.productId in ('"+ productId + "') and c.priority='3'),");
							queryBuilder
									.append("(select count(distinct(d.incidentid)) from incidentdashboard d where d.citype ='Service' and d.incidentstatus not in ('Closed','Cancelled','Completed') and d.customeruniqueid='"
											+ customerId
											+ "' and d.productId in ('"+ productId + "') and d.priority='4'),");
							queryBuilder
									.append("(select count(distinct(k.incidentid)) from incidentdashboard k where k.citype ='Service' and k.incidentstatus not in ('Closed','Cancelled','Completed') and k.customeruniqueid='"
											+ customerId
											+ "' and k.productId in ('"+ productId + "'))");
						} else {
							queryBuilder
							.append("(select count(distinct(a.incidentid)) from incidentdashboard a where a.citype ='Service' and a.incidentstatus not in ('Closed','Cancelled','Completed') and a.customeruniqueid='"
									+ customerId
									+ "' and a.productclassification in ('"+ service + "') and a.priority='1'),");
							queryBuilder
									.append("(select count(distinct(b.incidentid)) from incidentdashboard b where b.citype ='Service' and b.incidentstatus not in ('Closed','Cancelled','Completed') and b.customeruniqueid='"
											+ customerId
											+ "' and b.productclassification in ('"+ service + "') and b.priority='2'),");
							queryBuilder
									.append("(select count(distinct(c.incidentid)) from incidentdashboard c where c.citype ='Service' and c.incidentstatus not in ('Closed','Cancelled','Completed') and c.customeruniqueid='"
											+ customerId
											+ "' and c.productclassification in ('"+ service + "') and c.priority='3'),");
							queryBuilder
									.append("(select count(distinct(d.incidentid)) from incidentdashboard d where d.citype ='Service' and d.incidentstatus not in ('Closed','Cancelled','Completed') and d.customeruniqueid='"
											+ customerId
											+ "' and d.productclassification in ('"+ service + "') and d.priority='4'),");
							queryBuilder
									.append("(select count(distinct(k.incidentid)) from incidentdashboard k where k.citype ='Service' and k.incidentstatus not in ('Closed','Cancelled','Completed') and k.customeruniqueid='"
											+ customerId+ "' and k.productclassification in ('"+ service + "'))");
						}
					} else {
						ArrayList<String> completedIncidentsDuration = getCompletedIncidentsDuration();
						if(rootproductid.equalsIgnoreCase("SaaS")){
							queryBuilder
									.append("(select count(distinct(ca.incidentid)) from incidentdashboard ca where ca.citype ='Service' and ca.incidentstatus in ('Closed','Cancelled','Completed') and ca.customeruniqueid='"
											+ customerId
											+ "' and ca.productId in ('"+ productId + "') and ca.priority='1' and (ca.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(cb.incidentid)) from incidentdashboard cb where cb.citype ='Service' and cb.incidentstatus in ('Closed','Cancelled','Completed') and cb.customeruniqueid='"
											+ customerId
											+ "' and cb.productId in ('"+ productId + "') and cb.priority='2' and (cb.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(cc.incidentid)) from incidentdashboard cc where cc.citype ='Service' and cc.incidentstatus in ('Closed','Cancelled','Completed') and cc.customeruniqueid='"
											+ customerId
											+ "' and cc.productId in ('"+ productId + "') and cc.priority='3' and (cc.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(cd.incidentid)) from incidentdashboard cd where cd.citype ='Service' and cd.incidentstatus in ('Closed','Cancelled','Completed') and cd.customeruniqueid='"
											+ customerId
											+ "' and cd.productId in ('"+ productId + "') and cd.priority='4' and (cd.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(ck.incidentid)) from incidentdashboard ck where ck.citype ='Service' and ck.incidentstatus in ('Closed','Cancelled','Completed') and ck.customeruniqueid='"
											+ customerId
											+ "' and ck.productId in ('"+ productId + "') and (ck.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "'))");
						} else {
							queryBuilder
									.append("(select count(distinct(ca.incidentid)) from incidentdashboard ca where ca.citype ='Service' and ca.incidentstatus in ('Closed','Cancelled','Completed') and ca.customeruniqueid='"
											+ customerId
											+ "' and ca.productclassification in ('"+ service + "') and ca.priority='1' and (ca.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(cb.incidentid)) from incidentdashboard cb where cb.citype ='Service' and cb.incidentstatus in ('Closed','Cancelled','Completed') and cb.customeruniqueid='"
											+ customerId
											+ "' and cb.productclassification in ('"+ service + "') and cb.priority='2' and (cb.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(cc.incidentid)) from incidentdashboard cc where cc.citype ='Service' and cc.incidentstatus in ('Closed','Cancelled','Completed') and cc.customeruniqueid='"
											+ customerId
											+ "' and cc.productclassification in ('"+ service + "') and cc.priority='3' and (cc.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(cd.incidentid)) from incidentdashboard cd where cd.citype ='Service' and cd.incidentstatus in ('Closed','Cancelled','Completed') and cd.customeruniqueid='"
											+ customerId
											+ "' and cd.productclassification in ('"+ service + "') and cd.priority='4' and (cd.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "')),");
							queryBuilder
									.append("(select count(distinct(ck.incidentid)) from incidentdashboard ck where ck.citype ='Service' and ck.incidentstatus in ('Closed','Cancelled','Completed') and ck.customeruniqueid='"
											+ customerId
											+ "' and ck.productclassification in ('"+ service + "') and (ck.createddate between '"
											+ completedIncidentsDuration.get(0)
											+ "' and '"
											+ completedIncidentsDuration.get(1)
											+ "'))");
						}

					}

					queryBuilder.append(" from dual");

					log.info("query for ticketgrid-- "
							+ queryBuilder.toString());

					Query queryRow = sessionFactory.getCurrentSession()
							.createSQLQuery(queryBuilder.toString());
					Object[] result = (Object[]) queryRow.uniqueResult();

					ArrayList<String> countRow = new ArrayList<String>();

					for (int j = 0; j < result.length; j++) {
						countRow.add(result[j].toString());
					}

					ticketGridDataResults.put(service, countRow);

				}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return ticketGridDataResults;
	}

	public ArrayList<String> getCompletedIncidentsDuration() {

		ArrayList<String> durations = new ArrayList<String>();

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK) - 1));
		java.util.Date currentDate = cal.getTime();
		int period = (-1) * 84;
		cal.add(Calendar.DATE, period);
		java.util.Date startDate = cal.getTime();

		cal.setTime(currentDate);
		cal.add(Calendar.DATE, -1);
		currentDate = cal.getTime();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateString = simpleDateFormat.format(currentDate);
		currentDateString = currentDateString.concat(" 23:59:59");
		String startDateString = simpleDateFormat.format(startDate);
		startDateString = startDateString.concat(" 00:00:00");

		durations.add(startDateString);
		durations.add(currentDateString);

		return durations;

	}
}
