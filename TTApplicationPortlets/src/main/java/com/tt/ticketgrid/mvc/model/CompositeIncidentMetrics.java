package com.tt.ticketgrid.mvc.model;

import java.util.List;

import com.tt.model.IncidentDashboardMapping;

public class CompositeIncidentMetrics {
	
	private List<IncidentDashboardMapping> incidentDashboardMappings;
	private List<String> distinctServiceClasses;
	
	public List<IncidentDashboardMapping> getIncidentDashboardMappings() {
		return incidentDashboardMappings;
	}
	public void setIncidentDashboardMappings(
			List<IncidentDashboardMapping> incidentDashboardMappings) {
		this.incidentDashboardMappings = incidentDashboardMappings;
	}
	public List<String> getDistinctServiceClasses() {
		return distinctServiceClasses;
	}
	public void setDistinctServiceClasses(List<String> distinctServiceClasses) {
		this.distinctServiceClasses = distinctServiceClasses;
	}

}
