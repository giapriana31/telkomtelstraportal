package com.tt.ticketgrid.mvc.model;

public class ServiceClass {

	private String serviceName;
	private String p1WithImpacts;
	private String p2WithImpacts;
	private String p3WithImpacts;
	private String p4WithImpacts;
	private String p5WithImpacts;
	private String p1AtRisk;
	private String p2AtRisk;
	private String p3AtRisk;
	private String p4AtRisk;
	private String p5AtRisk;
	private String totalIncidents;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getP1WithImpacts() {
		return p1WithImpacts;
	}

	public void setP1WithImpacts(String p1WithImpacts) {
		this.p1WithImpacts = p1WithImpacts;
	}

	public String getP2WithImpacts() {
		return p2WithImpacts;
	}

	public void setP2WithImpacts(String p2WithImpacts) {
		this.p2WithImpacts = p2WithImpacts;
	}

	public String getP3WithImpacts() {
		return p3WithImpacts;
	}

	public void setP3WithImpacts(String p3WithImpacts) {
		this.p3WithImpacts = p3WithImpacts;
	}

	public String getP4WithImpacts() {
		return p4WithImpacts;
	}

	public void setP4WithImpacts(String p4WithImpacts) {
		this.p4WithImpacts = p4WithImpacts;
	}

	public String getP5WithImpacts() {
		return p5WithImpacts;
	}

	public void setP5WithImpacts(String p5WithImpacts) {
		this.p5WithImpacts = p5WithImpacts;
	}

	public String getP1AtRisk() {
		return p1AtRisk;
	}

	public void setP1AtRisk(String p1AtRisk) {
		this.p1AtRisk = p1AtRisk;
	}

	public String getP2AtRisk() {
		return p2AtRisk;
	}

	public void setP2AtRisk(String p2AtRisk) {
		this.p2AtRisk = p2AtRisk;
	}

	public String getP3AtRisk() {
		return p3AtRisk;
	}

	public void setP3AtRisk(String p3AtRisk) {
		this.p3AtRisk = p3AtRisk;
	}

	public String getP4AtRisk() {
		return p4AtRisk;
	}

	public void setP4AtRisk(String p4AtRisk) {
		this.p4AtRisk = p4AtRisk;
	}

	public String getP5AtRisk() {
		return p5AtRisk;
	}

	public void setP5AtRisk(String p5AtRisk) {
		this.p5AtRisk = p5AtRisk;
	}

	public String getTotalIncidents() {
		return totalIncidents;
	}

	public void setTotalIncidents(String totalIncidents) {
		this.totalIncidents = totalIncidents;
	}

}
