package com.tt.ticketgrid.mvc.service.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.TTPortalAppErrors;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.ticketgrid.mvc.dao.TicketGridDaoImpl;
import com.tt.ticketgrid.mvc.model.CompositeIncidentMetrics;
import com.tt.ticketgrid.mvc.model.DatePair;
import com.tt.ticketgrid.mvc.model.ServiceClass;

@Service(value = "ticketGridManager")
@Transactional
public class TicketGridManagerImpl {

	private TicketGridDaoImpl ticketGridDaoImpl;
	private final static Logger log = Logger
			.getLogger(TicketGridManagerImpl.class);

	@Autowired
	@Qualifier("ticketGridDAO")
	public void setTicketGridDaoImpl(TicketGridDaoImpl ticketGridDaoImpl) {
		this.ticketGridDaoImpl = ticketGridDaoImpl;
	}

	public List<Integer> getIncidentTrends(List<DatePair> cycleDateRange,
			String customerId, String incCategory, int numberOfWeeks)
			throws TTPortalAppException {
		try {
			return ticketGridDaoImpl.getIncidentTrends(cycleDateRange,
					customerId, incCategory, numberOfWeeks);
		} catch (Exception e) {
			throw new TTPortalAppException(e, "SS:1111",
					TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}
	}

	public List<ServiceClass> getTicketGridData(String customerId, String status) {
		List<ServiceClass> serviceClassList = null;
		try {
			LinkedHashMap<String, ArrayList<String>> ticketGridData = ticketGridDaoImpl
					.getTicketGridData(customerId, status);
			if (null != ticketGridData) {

				serviceClassList = new ArrayList<ServiceClass>();

				ArrayList<String> services = new ArrayList<String>();
				services.addAll(ticketGridData.keySet());

				for (int i = 0; i < services.size(); i++) {
					ServiceClass serviceClass = new ServiceClass();
					serviceClass.setServiceName(services.get(i));
					serviceClass.setP1WithImpacts(ticketGridData.get(
							services.get(i)).get(0));
					serviceClass.setP2WithImpacts(ticketGridData.get(
							services.get(i)).get(1));
					serviceClass.setP3WithImpacts(ticketGridData.get(
							services.get(i)).get(2));
					serviceClass.setP4WithImpacts(ticketGridData.get(
							services.get(i)).get(3));
					/*serviceClass.setP5WithImpacts(ticketGridData.get(
							services.get(i)).get(4));
					serviceClass.setP1AtRisk(ticketGridData
							.get(services.get(i)).get(5));
					serviceClass.setP2AtRisk(ticketGridData
							.get(services.get(i)).get(6));
					serviceClass.setP3AtRisk(ticketGridData
							.get(services.get(i)).get(7));
					serviceClass.setP4AtRisk(ticketGridData
							.get(services.get(i)).get(8));
					serviceClass.setP5AtRisk(ticketGridData
							.get(services.get(i)).get(9));*/
					serviceClass.setTotalIncidents(ticketGridData.get(
							services.get(i)).get(4));

					serviceClassList.add(serviceClass);
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return serviceClassList;

	}

	public List<ServiceClass> getDashboardMappings(String customerId,
			String status) {

		CompositeIncidentMetrics compositeIncidentMetrics = ticketGridDaoImpl
				.getDashboardMappings(customerId, status);
		if (compositeIncidentMetrics != null) {

			List<ServiceClass> listServiceClasses = new ArrayList<ServiceClass>();
			List<IncidentDashboardMapping> dashboardMappings = compositeIncidentMetrics
					.getIncidentDashboardMappings();
			Map<String, List<IncidentDashboardMapping>> keyValuePairs = new HashMap<String, List<IncidentDashboardMapping>>();

			if (dashboardMappings != null && dashboardMappings.size() > 0) {

				HashMap<String, String> temp = new HashMap<String, String>();

				log.debug("dashboardMappings...." + dashboardMappings.size());
				for (IncidentDashboardMapping incidentDashboardMapping : dashboardMappings) {

					for (String cmdbClassCheck : compositeIncidentMetrics
							.getDistinctServiceClasses()) {

						if (cmdbClassCheck
								.equalsIgnoreCase(incidentDashboardMapping
										.getCmdbclass())) {

							if (!(keyValuePairs
									.containsKey(incidentDashboardMapping
											.getCmdbclass()))) {
								List<IncidentDashboardMapping> setDashboardMappings = new ArrayList<IncidentDashboardMapping>();
								setDashboardMappings
										.add(incidentDashboardMapping);
								keyValuePairs
										.put(incidentDashboardMapping
												.getCmdbclass(),
												setDashboardMappings);
							} else {

								List<IncidentDashboardMapping> setDashboardMappings = keyValuePairs
										.get(incidentDashboardMapping
												.getCmdbclass());
								if (!temp.containsKey(incidentDashboardMapping
										.getIncidentid())) {
									setDashboardMappings
											.add(incidentDashboardMapping);
									keyValuePairs.put(incidentDashboardMapping
											.getCmdbclass(),
											setDashboardMappings);
								}
								temp.put(incidentDashboardMapping
										.getIncidentid(), "");
							}

						}

						else {
							continue;
						}
					}

				}

				for (String cmdbClass : keyValuePairs.keySet()) {
					ServiceClass serviceClass = new ServiceClass();
					serviceClass.setServiceName(cmdbClass);
					Integer p1withImpact = 0;
					Integer p2withImpact = 0;
					Integer p3withImpact = 0;
					Integer p4withImpact = 0;
					Integer p5withImpact = 0;
					Integer p1atRisk = 0;
					Integer p2atRisk = 0;
					Integer p3atRisk = 0;
					Integer p4atRisk = 0;
					Integer p5atRisk = 0;

					for (IncidentDashboardMapping dashboardMapping : keyValuePairs
							.get(cmdbClass)) {
						if (dashboardMapping.getPriority()
								.equalsIgnoreCase("1")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("1")) {
							p1withImpact++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("2")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("1")) {
							p2withImpact++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("3")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("1")) {
							p3withImpact++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("4")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("1")) {
							p4withImpact++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("5")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("1")) {
							p5withImpact++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("1")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("0")) {
							p1atRisk++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("2")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("0")) {
							p2atRisk++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("3")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("0")) {
							p3atRisk++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("4")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("0")) {
							p4atRisk++;
						} else if (dashboardMapping.getPriority()
								.equalsIgnoreCase("5")
								&& dashboardMapping.getCustomerimpacted()
										.equalsIgnoreCase("0")) {
							p5atRisk++;
						}
					}

					serviceClass.setP1WithImpacts(p1withImpact.toString());
					serviceClass.setP2WithImpacts(p2withImpact.toString());
					serviceClass.setP3WithImpacts(p3withImpact.toString());
					serviceClass.setP4WithImpacts(p4withImpact.toString());
					serviceClass.setP5WithImpacts(p5withImpact.toString());

					serviceClass.setP1AtRisk(p1atRisk.toString());
					serviceClass.setP2AtRisk(p2atRisk.toString());
					serviceClass.setP3AtRisk(p3atRisk.toString());
					serviceClass.setP4AtRisk(p4atRisk.toString());
					serviceClass.setP5AtRisk(p5atRisk.toString());
					Integer total = p1withImpact + p2withImpact + p3withImpact
							+ p4withImpact + p5withImpact + p1atRisk + p2atRisk
							+ p3atRisk + p4atRisk + p5atRisk;

					serviceClass.setTotalIncidents(total.toString());
					listServiceClasses.add(serviceClass);

				}

				for (String distinctServiceClass : compositeIncidentMetrics
						.getDistinctServiceClasses()) {
					if (!keyValuePairs.containsKey(distinctServiceClass)) {
						ServiceClass serviceClass = new ServiceClass();
						serviceClass.setServiceName(distinctServiceClass);
						serviceClass.setP1WithImpacts("0");
						serviceClass.setP2WithImpacts("0");
						serviceClass.setP3WithImpacts("0");
						serviceClass.setP4WithImpacts("0");
						serviceClass.setP5WithImpacts("0");

						serviceClass.setP1AtRisk("0");
						serviceClass.setP2AtRisk("0");
						serviceClass.setP3AtRisk("0");
						serviceClass.setP4AtRisk("0");
						serviceClass.setP5AtRisk("0");

						serviceClass.setTotalIncidents("0");
						listServiceClasses.add(serviceClass);
					}
				}
				return listServiceClasses;
			} else {

				for (String distinctServiceClass : compositeIncidentMetrics
						.getDistinctServiceClasses()) {

					ServiceClass serviceClass = new ServiceClass();
					serviceClass.setServiceName(distinctServiceClass);
					serviceClass.setP1WithImpacts("0");
					serviceClass.setP2WithImpacts("0");
					serviceClass.setP3WithImpacts("0");
					serviceClass.setP4WithImpacts("0");
					serviceClass.setP5WithImpacts("0");

					serviceClass.setP1AtRisk("0");
					serviceClass.setP2AtRisk("0");
					serviceClass.setP3AtRisk("0");
					serviceClass.setP4AtRisk("0");
					serviceClass.setP5AtRisk("0");

					serviceClass.setTotalIncidents("0");
					listServiceClasses.add(serviceClass);

				}

				return listServiceClasses;
			}
		} else
			return null;
	}
}
