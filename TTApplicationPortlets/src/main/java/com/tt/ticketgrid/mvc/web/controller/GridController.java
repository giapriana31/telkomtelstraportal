package com.tt.ticketgrid.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.EventPortlet;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.portlet.util.PortletUtils;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.TTPortalAppErrors;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;
import com.tt.ticketgrid.mvc.model.DatePair;
import com.tt.ticketgrid.mvc.model.ServiceClass;
import com.tt.ticketgrid.mvc.service.common.TicketGridManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("gridController")
@RequestMapping("VIEW")
public class GridController implements EventPortlet {

	private TicketGridManagerImpl ticketGridManager;
	private final static Logger log = Logger.getLogger(GridController.class);

	@Autowired
	@Qualifier("ticketGridManager")
	public void setTicketGridManager(TicketGridManagerImpl ticketGridManager) {
		this.ticketGridManager = ticketGridManager;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) {
		try {
			log.info("handleRenderRequest-----");

			HttpServletRequest httprequest = PortalUtil
					.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil
					.getHttpServletResponse(renderResponse);

			String customerId = TTGenericUtils.getCustomerIDFromSession(
					httprequest, httpresponse);
			log.info("setting Cust ID,customerUniqueID:::" + customerId);
			String statusParameter = "active";
			if (PortletUtils.getSessionAttribute(request, "statusGlobal") != null) {
				statusParameter = (String) PortletUtils.getSessionAttribute(
						request, "statusGlobal");
			}
			log.info("status in gridcontroller-- "+statusParameter);
			model.addAttribute("statusParameter", statusParameter);

			List<ServiceClass> serviceGridList = ticketGridManager
					.getTicketGridData(customerId, statusParameter);
			ServiceClass totalIncidentsRow = new ServiceClass();
			totalIncidentsRow.setServiceName("Total");
			if (serviceGridList != null) {
				Integer totalP1WithImpact = 0;
				Integer totalP2WithImpact = 0;
				Integer totalP3WithImpact = 0;
				Integer totalP4WithImpact = 0;
				/*Integer totalP5WithImpact = 0;
				Integer totalP1AtRisk = 0;
				Integer totalP2AtRisk = 0;
				Integer totalP3AtRisk = 0;
				Integer totalP4AtRisk = 0;
				Integer totalP5AtRisk = 0;*/
				Integer totalIncidents = 0;

				for (ServiceClass serviceClass : serviceGridList) {
					totalP1WithImpact += Integer.parseInt(serviceClass
							.getP1WithImpacts());
					log.info("getP1WithImpacts--- "+ serviceClass
							.getP1WithImpacts());
					totalP2WithImpact += Integer.parseInt(serviceClass
							.getP2WithImpacts());
					log.info("getP2WithImpacts--- "+ serviceClass
							.getP2WithImpacts());
					totalP3WithImpact += Integer.parseInt(serviceClass
							.getP3WithImpacts());
					log.info("getP3WithImpacts--- "+ serviceClass
							.getP3WithImpacts());
					totalP4WithImpact += Integer.parseInt(serviceClass
							.getP4WithImpacts());
					log.info("getP4WithImpacts--- "+ serviceClass
							.getP4WithImpacts());
					/*totalP5WithImpact += Integer.parseInt(serviceClass
							.getP5WithImpacts());
					totalP1AtRisk += Integer.parseInt(serviceClass
							.getP1AtRisk());
					totalP2AtRisk += Integer.parseInt(serviceClass
							.getP2AtRisk());
					totalP3AtRisk += Integer.parseInt(serviceClass
							.getP3AtRisk());
					totalP4AtRisk += Integer.parseInt(serviceClass
							.getP4AtRisk());
					totalP5AtRisk += Integer.parseInt(serviceClass
							.getP5AtRisk());*/

					totalIncidents += Integer.parseInt(serviceClass
							.getTotalIncidents());

				}

				totalIncidentsRow
						.setP1WithImpacts(totalP1WithImpact.toString());
				totalIncidentsRow
						.setP2WithImpacts(totalP2WithImpact.toString());
				totalIncidentsRow
						.setP3WithImpacts(totalP3WithImpact.toString());
				totalIncidentsRow
						.setP4WithImpacts(totalP4WithImpact.toString());
				/*totalIncidentsRow
						.setP5WithImpacts(totalP5WithImpact.toString());
				totalIncidentsRow.setP1AtRisk(totalP1AtRisk.toString());
				totalIncidentsRow.setP2AtRisk(totalP2AtRisk.toString());
				totalIncidentsRow.setP3AtRisk(totalP3AtRisk.toString());
				totalIncidentsRow.setP4AtRisk(totalP4AtRisk.toString());
				totalIncidentsRow.setP5AtRisk(totalP5AtRisk.toString());*/
				totalIncidentsRow.setTotalIncidents(totalIncidents.toString());
				model.addAttribute("serviceGridList", serviceGridList);
				model.addAttribute("totalIncidentRow", totalIncidentsRow);
			} else {
				model.addAttribute("serviceGridList", serviceGridList);

				totalIncidentsRow.setP1WithImpacts("0");
				totalIncidentsRow.setP2WithImpacts("0");
				totalIncidentsRow.setP3WithImpacts("0");
				totalIncidentsRow.setP4WithImpacts("0");
				/*totalIncidentsRow.setP5WithImpacts("0");
				totalIncidentsRow.setP1AtRisk("0");
				totalIncidentsRow.setP2AtRisk("0");
				totalIncidentsRow.setP3AtRisk("0");
				totalIncidentsRow.setP4AtRisk("0");
				totalIncidentsRow.setP5AtRisk("0");*/
				totalIncidentsRow.setTotalIncidents("0");
				model.addAttribute("totalIncidentRow", totalIncidentsRow);

				
			}
			PortletUtils.setSessionAttribute(request, "statusGlobal", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "ticketGridFramework";
	}

	// --- Code for getting the date ranges for the given number of weeks------
	// START--------------------------

	public List<DatePair> getDateRanges(int numberOfWeeks) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK) - 1));
		int period = (-1) * numberOfWeeks * 7;
		cal.add(Calendar.DATE, period);

		List<DatePair> cycleDateRangeList = new ArrayList<DatePair>();

		for (int i = 1; i <= numberOfWeeks; i++) {

			DatePair datePair = new DatePair();
			Date date = cal.getTime();
			datePair.setStartOfWeek(getStartDate(date));
			cal.add(Calendar.DATE, 6);
			date = cal.getTime();
			datePair.setEndOfWeek(getEndDate(date));
			cal.add(Calendar.DATE, 1);
			cycleDateRangeList.add(datePair);
		}

		return cycleDateRangeList;

	}

	public String getStartDate(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = simpleDateFormat.format(date);
		startDate = startDate.concat(" 00:00:00");
		return startDate;
	}

	public String getEndDate(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = simpleDateFormat.format(date);
		startDate = startDate.concat(" 23:59:59");
		return startDate;
	}

	// --- Code for getting the date ranges for the given number of weeks------
	// END--------------------------

	// Resource portlet function to get graph data for Completed Incidents and
	// send to VIEW----START-----------------

	@ResourceMapping(value = "getGraphData")
	public void getGraphDataForIncTrends(ResourceRequest req,
			ResourceResponse res, Model model) {

		log.info("GraphDataMethod for Completed Incidents in Controller");

		try {

			HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
			HttpServletResponse response = PortalUtil
					.getHttpServletResponse(res);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(
					request, response);

			JSONObject obj = new JSONObject(
					req.getParameter("graphDataIdentifier"));

			String incCategory = obj.getString("incidentCategory");

			log.info("incCategory received as parameter--" + incCategory);

			int numberOfWeeks = 12;

			List<DatePair> cycleDateRange = getDateRanges(numberOfWeeks);

			List<Integer> incTrends = ticketGridManager.getIncidentTrends(
					cycleDateRange, customerUniqueID, incCategory,
					numberOfWeeks);

			com.liferay.portal.kernel.json.JSONArray incTrendArray = JSONFactoryUtil
					.createJSONArray();

			for (int i = 0; i < incTrends.size(); i++) {
				int val = incTrends.get(i).intValue();
				incTrendArray.put(val);
				System.out.println(val);
			}

			PrintWriter out;
			out = res.getWriter();

			com.liferay.portal.kernel.json.JSONObject jsonValue = JSONFactoryUtil
					.createJSONObject();

			jsonValue
					.put("variantCategoryReceived", getTrendTitle(incCategory));
			jsonValue.put("variantStatistics", incTrendArray);

			out.print(jsonValue.toString());

		} catch (Exception e) {
			throw new TTPortalAppException(e, "SS:1111",
					TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}
	}

	// Resource portlet function to get graph data for Completed Incidents and
	// send to VIEW----END-----------------

	public String getTrendTitle(String title) {

		String trendTitle;

		switch (title) {
		case "sites":
			trendTitle = "Sites";
			break;
		case "network":
			trendTitle = "Network";
			break;
		case "cloudinfrastructure":
			trendTitle = "Cloud Infrastructure";
			break;
		case "saas":
			trendTitle = "SaaS";
			break;
		default:
			trendTitle = "";
			break;
		}
		return trendTitle;
	}

	@EventMapping(value = "{http://localhost:8080/siteTicket}siteName")
	public void processEvent(EventRequest request, EventResponse response)
			throws PortletException, IOException {

		log.info("Process event for siteName in GridController");

		javax.portlet.Event event = request.getEvent();
		String siteName = (String) event.getValue();
		log.debug("siteName received in GridController----" + siteName);
		response.setRenderParameter("action", "list");
	}

	@EventMapping(value = "{http://localhost:8080/ticketHeader}headerTab")
	public void processEventStatus(EventRequest request, EventResponse response)
			throws PortletException, IOException {

		log.info("Process event for status in GridController");

		javax.portlet.Event event = request.getEvent();
		String status = (String) event.getValue();

		log.debug("status received in GridController----" + status);

		PortletUtils.setSessionAttribute(request, "statusGlobal", status);

	}

	@EventMapping(value = "{http://localhost:8080/updateTicket}backTo")
	public void processEventStatusFromUpdateTicket(EventRequest request,
			EventResponse response) throws PortletException, IOException {

		log.info("Process event for status from updateTicket in GridController");

		javax.portlet.Event event = request.getEvent();
		String status = (String) event.getValue();

		log.debug("status from updateTicket received in GridController----"
				+ status);

		PortletUtils.setSessionAttribute(request, "statusGlobal", status);

	}

}