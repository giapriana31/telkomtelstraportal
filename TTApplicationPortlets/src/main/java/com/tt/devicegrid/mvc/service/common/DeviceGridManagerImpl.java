package com.tt.devicegrid.mvc.service.common;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.devicegrid.mvc.dao.DeviceGridDaoImpl;
import com.tt.devicemapping.mvc.dao.DeviceSearchDaoImpl;

@Service(value="deviceGridManagerImpl")
@Transactional
public class DeviceGridManagerImpl {

	private DeviceGridDaoImpl deviceGridDaoImpl;

	@Autowired
	@Qualifier("deviceGridDaoImpl")
	public void setDeviceGridDaoImpl(DeviceGridDaoImpl deviceGridDaoImpl) {
		this.deviceGridDaoImpl = deviceGridDaoImpl;
	}
	
	public LinkedHashMap<String,LinkedHashMap<String, Integer>> getRows(String customerId){
		return deviceGridDaoImpl.getRows(customerId);
	}

}
