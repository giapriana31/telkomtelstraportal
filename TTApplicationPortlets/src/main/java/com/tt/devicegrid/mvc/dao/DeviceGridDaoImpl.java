package com.tt.devicegrid.mvc.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value = "deviceGridDaoImpl")
@Transactional
public class DeviceGridDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;
    public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(DeviceGridDaoImpl.class);
    private Session session = null;
    private Transaction tx = null;
    
    private static final String[] statusList= {"Order Received","Detailed Design", "Procurement", "Delivery and Activation", "Monitoring", "Operational"};
    
    public LinkedHashMap<String,LinkedHashMap<String, Integer>> getRows(String customerId){
    	
    	LinkedHashMap<String,LinkedHashMap<String, Integer>> rows= new LinkedHashMap<String, LinkedHashMap<String,Integer>>();
    	
    	System.out.println("Dao implementation for device grid");
	    

		StringBuilder hqlProductClassificationLookUp = new StringBuilder();
		
		hqlProductClassificationLookUp.append("select distinct(ci.productclassification) from Configuration ci ");
		hqlProductClassificationLookUp.append("where ci.customeruniqueid =:customerId and ci.productclassification is not null ");
		hqlProductClassificationLookUp.append("and ci.productclassification <> '' and ci.productclassification <> 'NA' order by ci.productclassification asc");
		System.out.println("query additional for hqlProductClassificationLookUp - "+hqlProductClassificationLookUp.toString());
		Query queryProductClassificationLookUp = sessionFactory.getCurrentSession().createQuery(hqlProductClassificationLookUp.toString());
		queryProductClassificationLookUp.setString("customerId", customerId);
		List queryResultList = queryProductClassificationLookUp.list();
		
		StringBuilder hqlProductIdLookUp = new StringBuilder();
		hqlProductIdLookUp.append("select distinct(ci.productId) from Configuration ci ");
		hqlProductIdLookUp.append("where ci.customeruniqueid =:customerId and ci.productId is not null ");
		hqlProductIdLookUp.append("and ci.productId <> '' and ci.rootProductId = 'SaaS' order by ci.productId asc");
		System.out.println("query additional for hqlProductIdLookUp - "+hqlProductIdLookUp.toString());
		Query queryProductIdLookUp = sessionFactory.getCurrentSession().createQuery(hqlProductIdLookUp.toString());
		queryProductIdLookUp.setString("customerId", customerId);
		queryResultList.addAll(queryProductIdLookUp.list());
		
    	StringBuilder hql = new StringBuilder();
	    	
		hql.append("select distinct(ci.rootProductId) from Configuration ci ");
		hql.append("where ci.customeruniqueid =:customerId and ci.rootProductId is not null ");
		hql.append("and ci.rootProductId <> '' and ci.rootProductId <> 'MNS' and ci.rootProductId <> 'MSS' ");
		hql.append("and ci.rootProductId <> 'SaaS' order by ci.rootProductId asc");
		
		System.out.println("query initial for cmdb classes-"+hql.toString());
	
		Query queryInitial = sessionFactory.getCurrentSession().createQuery(hql.toString());
		
		queryInitial.setString("customerId", customerId);
		
		queryResultList.addAll(queryInitial.list());

		if (queryResultList != null && queryResultList.size() > 0) 
		{		
			ArrayList<String> cmdbClasses= (ArrayList<String>) queryResultList;
			log.info("list of cmdbclases-------"+ cmdbClasses);
			for (int i = 0; i < cmdbClasses.size(); i++) {
				StringBuilder hsql = new StringBuilder();
				hsql.append("select ");
				//-- Loop to get count of completed incidents for different weeks---------
				
				for(int j=0;j<statusList.length;j++){
					
					hsql.append("(select count(*) from configurationitem ci join site s ");
					hsql.append("where ci.siteid=s.siteid and ci.deliverystatus in ('");
					hsql.append(statusList[j]);				
					hsql.append("') and ci.customeruniqueid =:customerId and (ci.rootProductId in ('");
					hsql.append(String.valueOf(cmdbClasses.get(i))).append("') or ");
					hsql.append("ci.productId in ('");
					hsql.append(String.valueOf(cmdbClasses.get(i))).append("') or ");
					hsql.append("ci.productclassification in ('");
					hsql.append(String.valueOf(cmdbClasses.get(i))).append("') )");

					if(cmdbClasses.get(i).equalsIgnoreCase("M-LAN")||
							cmdbClasses.get(i).equalsIgnoreCase("M-WAN")||
							cmdbClasses.get(i).equalsIgnoreCase("M-WANOPT")){
						hsql.append(" and ci.cmdbclass = 'CPE'");
						hsql.append(" and ci.citype='Resource'),");
					}
					else if(cmdbClasses.get(i).equalsIgnoreCase("Private Cloud")){
						hsql.append(" and ci.cmdbclass = 'vBLOCK'");
						hsql.append(" and ci.citype='Resource'),");
					}
					else if(cmdbClasses.get(i).equalsIgnoreCase("IPScape")  ||
							cmdbClasses.get(i).equalsIgnoreCase("Mandoe Digital Signage") ||
							cmdbClasses.get(i).equalsIgnoreCase("Whispir")){
						hsql.append(" and ci.cmdbclass = 'SAAS Business Service'),");
					}else {
						hsql.append("),");
					}
					
				}
				
				hsql.append("(select count(*) from configurationitem ci join site s ");				
				hsql.append("where ci.siteid=s.siteid ");
				hsql.append("and ci.customeruniqueid =:customerId and (ci.rootProductId in ('");
				hsql.append(String.valueOf(cmdbClasses.get(i))).append("') or ");
				hsql.append("ci.productId in ('");
				hsql.append(String.valueOf(cmdbClasses.get(i))).append("') or ");
				hsql.append("ci.productclassification in ('");
				hsql.append(String.valueOf(cmdbClasses.get(i))).append("') )");
				if(cmdbClasses.get(i).equalsIgnoreCase("M-LAN")||
						cmdbClasses.get(i).equalsIgnoreCase("M-WAN")||
						cmdbClasses.get(i).equalsIgnoreCase("M-WANOPT")){
					hsql.append(" and ci.cmdbclass = 'CPE'");
					hsql.append(" and ci.citype='Resource')");
				}
				else if(cmdbClasses.get(i).equalsIgnoreCase("Private Cloud")){
					hsql.append(" and ci.cmdbclass = 'vBLOCK'");
					hsql.append(" and ci.citype='Resource')");
				}
				else if(cmdbClasses.get(i).equalsIgnoreCase("IPScape")  ||
						cmdbClasses.get(i).equalsIgnoreCase("Mandoe Digital Signage") ||
						cmdbClasses.get(i).equalsIgnoreCase("Whispir")){
					hsql.append(" and ci.cmdbclass = 'SAAS Business Service')");
				}else {
					hsql.append(")");
				}
				
				hsql.append(" from dual");
				
				System.out.println("Search query for getting Completed Incident Graph data----------"+hsql);
				
				Query query = sessionFactory.getCurrentSession().createSQLQuery(hsql.toString());
				query.setString("customerId", customerId);
				Object[] result=(Object[])query.uniqueResult();
				
				LinkedHashMap<String, Integer> intermediary= new LinkedHashMap<String, Integer>();

				for (int k = 0; k < result.length; k++) {
					Integer count=((BigInteger) result[k]).intValue();
					if (k + 1 == result.length) {
						intermediary.put("Total", count);
					} else {
						intermediary.put(statusList[k], count);
					}
				}
				rows.put(cmdbClasses.get(i).toString(), intermediary);
			}
		}
	    return rows;
    }
}
