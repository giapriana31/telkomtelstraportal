
package com.tt.devicegrid.mvc.web.controller;

import java.util.LinkedHashMap;
import java.util.Properties;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.util.PortalUtil;
import com.tt.devicegrid.mvc.service.common.DeviceGridManagerImpl;
import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("deviceGridController")
@RequestMapping("VIEW")
public class DeviceGridController {

    private DeviceGridManagerImpl deviceGridManager;
    
    public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(DeviceGridController.class);
    
    private static final String[] statusList= {"Service","Order Received","Detailed Design", "Procurement", "Delivery and Activation", "Monitoring", "Operational"};
    
	@Autowired
    @Qualifier("deviceGridManagerImpl")
    public void setDeviceGridManager(DeviceGridManagerImpl deviceGridManager) {
		this.deviceGridManager = deviceGridManager;
    }

    @RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws Exception {
		try {
			HttpServletRequest httprequest = PortalUtil
					.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil
					.getHttpServletResponse(renderResponse);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(
					httprequest, httpresponse);
			
			LinkedHashMap<String,LinkedHashMap<String, Integer>> rows= deviceGridManager.getRows(customerUniqueID);
			
			System.out.println("Data for rows-"+rows);
			
			model.addAttribute("fieldRow",statusList);
			model.addAttribute("gridRows", rows);

			return "deviceGrid";
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

  
}
