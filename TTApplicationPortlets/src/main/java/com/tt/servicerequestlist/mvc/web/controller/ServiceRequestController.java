
package com.tt.servicerequestlist.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.portlet.util.PortletUtils;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.client.model.createServiceRequestapi.InsertResponse;
import com.tt.client.service.incident.createServiceRequestWSCallService;
import com.tt.constants.GenericConstants;
import com.tt.constants.TTPortalAppErrors;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;
import com.tt.model.Incident;
import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ChangeRequestApprovals;
import com.tt.model.servicerequest.ChangeRequestDevices;
import com.tt.model.servicerequest.ChangeRequestNotes;
import com.tt.model.servicerequest.ChangeRequestServices;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.model.servicerequest.ServiceRequestItemDevices;
import com.tt.model.servicerequest.ServiceRequestItemNotes;
import com.tt.servicerequestlist.mvc.model.CreateSRForm;
import com.tt.servicerequestlist.mvc.model.DatePair;
import com.tt.servicerequestlist.mvc.model.DetailsSearchParameters;
import com.tt.servicerequestlist.mvc.model.SearchParameters;
import com.tt.servicerequestlist.mvc.model.ServiceRequestResults;
import com.tt.servicerequestlist.mvc.service.common.CreateSRManager;
import com.tt.servicerequestlist.mvc.service.common.ServiceRequestService;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.utils.TTGenericUtils;

@Controller("serviceRequestController")
@RequestMapping("VIEW")

public class ServiceRequestController {
	public static final Properties prop = PropertyReader.getProperties();
	String statusNew=prop.getProperty("statusNew");
	String statusAssigned=prop.getProperty("statusAssigned");
	String statusPending=prop.getProperty("statusPending");
	String statusInProgress=prop.getProperty("statusInProgress");
	String statusClosed=prop.getProperty("statusClosed");
	String statusCancelled=prop.getProperty("statusCancelled");
	String statusComplete=prop.getProperty("statusComplete");
	String statusCompleted=prop.getProperty("statusCompleted");
	String statusClosedCancelled=prop.getProperty("statusClosedCancelled");
	String changeTypeRoutine=prop.getProperty("changeTypeRoutine");
	String changeTypeComprehensive=prop.getProperty("changeTypeComprehensive");
	String changeTypeEmergency=prop.getProperty("changeTypeEmergency");
	String changeSourceInternal=prop.getProperty("changeSourceInternal");
	String changeSourceExternal=prop.getProperty("changeSourceExternal");
	String changeSourceCustomer=prop.getProperty("changeSourceCustomer");
	private final static Logger log = Logger.getLogger(ServiceRequestController.class);
	
    private ServiceRequestService serviceRequestService;

	private CreateSRManager createSRManager;
    
	@Autowired
	@Qualifier("createSRManager")
	public void setCreateSRManager(CreateSRManager createSRManager) {
		this.createSRManager = createSRManager;
	}


	@Autowired
    @Qualifier("serviceRequestService")
    public void setServiceRequestService(ServiceRequestService serviceRequestService) {
		this.serviceRequestService = serviceRequestService;
	}
   
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws TTPortalAppException {
		try {		
				
			String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request), PortalUtil.getHttpServletResponse(renderResponse));
            String lastRefreshDate = serviceRequestService.getLastRefreshDate(customerId);
            model.addAttribute("lastRefreshDate", lastRefreshDate);
			RenderLogServiceRequest(request, renderResponse, model);

            //Getting site name list
    	    Map<String, String> siteNameMap = createSRManager.getSiteNameMap(customerId);
			log.info("SiteNameList " + siteNameMap);
			if (siteNameMap != null && !(siteNameMap.isEmpty())) {
				log.info("Null Handled");
				Map<String, String> siteNameMapActual = new LinkedHashMap<String, String>();
				siteNameMapActual.put("allSites", "All Sites");
				for (Map.Entry<String, String> entry : siteNameMap.entrySet()) {
					siteNameMapActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("sitenamelist", siteNameMapActual);
			} else {
				Map<String, String> siteNameListActual = new LinkedHashMap<String, String>();
				siteNameListActual.put("allSites", "All Sites");
				model.addAttribute("sitenamelist", siteNameListActual);
			}
			
			//Populating changeType
			LinkedHashMap<String, String> changeTypeMap = new LinkedHashMap<String, String>();
			changeTypeMap.put("allTypes", "All Types");
			changeTypeMap.put(changeTypeRoutine, changeTypeRoutine);
			changeTypeMap.put(changeTypeComprehensive, changeTypeComprehensive);
			changeTypeMap.put(changeTypeEmergency, changeTypeEmergency);
			model.addAttribute("changetypelist", changeTypeMap);			
			
			//Populating change source
			LinkedHashMap<String, String> changeSourceMap = new LinkedHashMap<String, String>();
			changeSourceMap.put("allTypes", "All Types");
			changeSourceMap.put(changeSourceInternal, changeSourceInternal);
			changeSourceMap.put(changeSourceExternal, changeSourceExternal);
			changeSourceMap.put(changeSourceCustomer, changeSourceCustomer);
			model.addAttribute("changesourcelist", changeSourceMap);
			String tabStatusCheck="activeTab";
			if(PortletUtils.getSessionAttribute(request, "tabStatusSelected")!=null){
				tabStatusCheck=(String)PortletUtils.getSessionAttribute(request, "tabStatusSelected");
			}
			
			model.addAttribute("tabStatusCheck", tabStatusCheck);
			return "serviceRequestMapping";
		} catch (Exception e) {
			log.error("In render mapping:"+e.getMessage());
			throw new TTPortalAppException(e, "SS:1111", TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}		
	}
			
	@ResourceMapping(value = "getServiceForProductTypeURL")
    public void handleSericeIDFetch(Model model, ResourceRequest request, ResourceResponse response) throws Exception {
	HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
	HttpServletResponse res = PortalUtil.getHttpServletResponse(response);
	try {
		    String selectedProductType =  req.getParameter("selectedProductID");
		    String selectedSiteID =  req.getParameter("selectedSiteID");
		    
		    log.debug("getServiceForProductTypeURL in servreq controller :::: \n selectedSiteID recieved in controller to fetch service::>>"+selectedSiteID+""
		    		+ "\n selectedProductType recieved in controller to fetch service::>> "+selectedProductType);
		
		    String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req,res);
		    
   				Map<String, String> serviceIDListtemp = createSRManager.getServiceFromProductTypeAndSiteID(selectedProductType , selectedSiteID, customerUniqueID);
   				log.info("Service Recieved from dao in getServiceForProductTypeURL : " + serviceIDListtemp);
   				String responseStr = "";
   				if (serviceIDListtemp != null) {
   					for (Map.Entry<String, String> entry : serviceIDListtemp
   							.entrySet()) {
   						responseStr += (entry.getKey() + "@@@@@"
   								+ entry.getValue() + "#####");
   					}
   					PrintWriter out = response.getWriter();
   					out.print(responseStr);
   				}
		    	
    }
	catch (Exception e) {
				throw new TTPortalAppException(e, "SS:1111", TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
	}
}
	
	public static final String REQUESTID = "requestid";
	public static final String SUBJECT = "subject";
	public static final String REQUESTTYPE = "requesttype";
	public static final String LASTUPDATEDATE = "lastupdatedate";
	public static final String REQUESTOR = "requestor";
	public static final String STATUS = "status";
	
  	//Will go into constants
 	private static Map<String, String> mapDTSortCriterion() {
 		Map<String, String> sortCriterion = new HashMap<String, String>();
 		sortCriterion.put("0", REQUESTID);
 		sortCriterion.put("1", SUBJECT);
 		sortCriterion.put("2", REQUESTTYPE);
 		sortCriterion.put("3", LASTUPDATEDATE);
 		sortCriterion.put("4", REQUESTOR);
 		sortCriterion.put("5", STATUS); 		

 		return Collections.unmodifiableMap(sortCriterion);
 	}

    // todo:needs to move it to constants
 	// html table column order is important
 	private static final Map<String, String> DTSORTCRITERION = mapDTSortCriterion();

	//Will go into commmons
	//Method to convert all the request parametres into json. 
	//pass the httpservlet request and it will return queryParamJson of all available parametrs
	public String getAllQueryParamAsJson(HttpServletRequest request) {
		
		//collect all the request parameters in a hash map
		HashMap grequestparamMap = getAllQueryParameters(request);
		
		//Convert the hashmap to json
		Gson gson = new Gson();		
		String queryParamJson = gson.toJson(grequestparamMap);
		
		return queryParamJson;
	}
 		
 		
 	//Will go into common
 	//method to get the customer unique id in portal session
	public String getCusomerUniqueId(ResourceRequest request, ResourceResponse response) throws TTPortalAppException  {
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		/*HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);*/
		
 		/*ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);*/
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, null);
		//TODO:Needs to add a condition and correct value for internal user simulated customer id
		
		return customerUniqueID;
	}
	
	

	//Will go into commmons
	//Method to convert all the request parametres into hasmap. 
	//pass the httpservlet request and it will return hashmap of all available parametrs
	public HashMap getAllQueryParameters(HttpServletRequest request) {

		HashMap <String, String>requestParamMap = new HashMap<String, String>();

		Enumeration paramNames = request.getParameterNames();

		while (paramNames.hasMoreElements()) {
			String paramName = (String) paramNames.nextElement();
			
			//todo:still needs to handle multi-selects
			//request.getParameterValues(paramName);
			
			String paramValue = request.getParameter(paramName);
			requestParamMap.put(paramName, paramValue);
			log.info("paramName : " + paramName + " paramValues: "+ paramValue);
		}		
		
		requestParamMap.put("sortCriterion",
				request.getParameter("order[0][column]") != null 
				? DTSORTCRITERION.get(request.getParameter("order[0][column]")) 
				: DTSORTCRITERION.get(REQUESTID));
		
		requestParamMap.put("sortDirection",
				request.getParameter("order[0][dir]") != null 
				? request.getParameter("order[0][dir]") 
				: "asc");		
		
		Integer pageNo=(Integer.parseInt(request.getParameter("start"))/10)+1;
		requestParamMap.put("pageNumber",
				request.getParameter("start") != null 
				? pageNo.toString() 
				: "0");				

		log.info("getAllQueryParameters request param = " + requestParamMap);
		
		return requestParamMap;
	}
    
	
	//Will go into commmons
	//Method to convert all the request parametres into hasmap. 
	//pass the httpservlet request and it will return hashmap of all available parametrs
	public SearchParameters getUserSearchParameters(ResourceRequest resourceRequest,ResourceResponse resourceResponse) {
		
		log.info("Class: ServiceRequestController, Method: getUserSearchParameters");
		SearchParameters searchParameters = new SearchParameters();		
		
		try {
		
			HttpServletRequest httpServletReq = PortalUtil.getHttpServletRequest(resourceRequest);			
			Enumeration paramNames = httpServletReq.getParameterNames();
			
			String cusomerUniqueId = getCusomerUniqueId(resourceRequest,resourceResponse);
			searchParameters.setCustomeruniqueid(cusomerUniqueId);
			
			//todo:delet after test
			while (paramNames.hasMoreElements()) {
				Map <String, String>requestParamMap = new HashMap<String, String>();

				String paramName = (String) paramNames.nextElement();
				
				//todo:still needs to handle multi-selects
				//request.getParameterValues(paramName);
				
				String paramValue = httpServletReq.getParameter(paramName);
				requestParamMap.put(paramName, paramValue);
				log.info("paramName : " + paramName + " paramValues: "+ paramValue);
			}

			searchParameters.setSortCriterion(
					httpServletReq.getParameter("order[0][column]") != null 
					? DTSORTCRITERION.get(httpServletReq.getParameter("order[0][column]")) 
					: DTSORTCRITERION.get(REQUESTID));
			
			searchParameters.setSortDirection(
					httpServletReq.getParameter("order[0][dir]") != null 
					? httpServletReq.getParameter("order[0][dir]") 
					: "asc");		
			
			searchParameters.setPageNumber(
					httpServletReq.getParameter("start") != null 
					? Integer.parseInt(httpServletReq.getParameter("start").toString())
					: 0);	
			log.info("search parameters page number---"+searchParameters.getPageNumber());
			
			searchParameters.setSearchqueryList(
					httpServletReq.getParameter("searchTerm") != null 
					? httpServletReq.getParameter("searchTerm").toString()
					: null);
			
			searchParameters.setMaxResults(
					httpServletReq.getParameter("maxResults") != null 
					? Integer.parseInt(httpServletReq.getParameter("maxResults").toString())
					: 30);
			searchParameters.setTabSelected(
					httpServletReq.getParameter("tabSelected") != null 
					? httpServletReq.getParameter("tabSelected").toString()
					: null
					);
			
			PortletUtils.setSessionAttribute(resourceRequest,"tabStatusSelected",searchParameters.getTabSelected());
			
			searchParameters.setRequesttype(
					httpServletReq.getParameter("reqType") != null 
					? httpServletReq.getParameter("reqType").toString()
					: null
					);
			
			//Setting requestType into searchParameters
			searchParameters.setAdvReqType(
					httpServletReq.getParameter("requestType") != null 
					? httpServletReq.getParameter("requestType").toString()
					: null
					);
			//Setting priority into searchParameters			
			searchParameters.setPriority(
					(httpServletReq.getParameterValues("priority") != null)
					? httpServletReq.getParameterValues("priority")
					: null
					);
			String strPriority[] = searchParameters.getPriority();
			if(strPriority != null){
				if("All".equalsIgnoreCase(strPriority[0])){
					searchParameters.setPriority(null);
				}
				else{
					String[] pr = new String[strPriority.length];
					for (int i = 0; i < strPriority.length; i++) {
						switch(strPriority[i]){
							case "P1" : pr[i]="1 - Critical";
										break;
							case "P2" : pr[i]="2 - High";
										break;
							case "P3" : pr[i]="3 - Moderate";
										break;
							case "P4" : pr[i]="4 - Low";
										break;
							default   : pr[i]= null;
										break;
						}
					}
					searchParameters.setPriority(pr);
				}
			}
			
			//Setting siteName into searchParameters
			searchParameters.setSiteName(
					(httpServletReq.getParameter("siteName") != null && !("allSites".equalsIgnoreCase(httpServletReq.getParameter("siteName"))))
					? httpServletReq.getParameter("siteName").toString()
					: null
					);
			
			//Setting serViceName into searchParameters
			searchParameters.setServiceName(
					(httpServletReq.getParameter("serviceName") != null  && !("allServices".equalsIgnoreCase(httpServletReq.getParameter("serviceName")))) 
					? httpServletReq.getParameter("serviceName").toString()
					: null
					);
			
			//Setting dateFrom into searchParameters
			String dateFrom = httpServletReq.getParameter("dateFrom");
			String fromDate=null;
            if (!("".equalsIgnoreCase(dateFrom)) && dateFrom != null) {
                  
                  SimpleDateFormat simpleDateFormat=new SimpleDateFormat("MM/dd/yyyy");
                  Date fromDateParameter= simpleDateFormat.parse(dateFrom);
                  
                  simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
                  fromDate= simpleDateFormat.format(fromDateParameter); 
                  String timeFromFactor=" 00:00:00";
                  fromDate = fromDate.concat(timeFromFactor);
            } else {
                  fromDate = null;
            }          
            searchParameters.setFromDate(fromDate);
            
          //Setting dateTo into searchParameters
            Date dateToParameter;
            String toDate=null;
            String dateTo = httpServletReq.getParameter("dateTo"); 
            if (!("".equalsIgnoreCase(dateTo)) && dateTo != null) {
                  
                  SimpleDateFormat simpleDateFormat=new SimpleDateFormat("MM/dd/yyyy");
                  Date toDateParameter= simpleDateFormat.parse(dateTo);
                  
                  simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
                  toDate= simpleDateFormat.format(toDateParameter);
            } else {
                  dateToParameter=new Date();
                  SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
                  toDate= simpleDateFormat.format(dateToParameter);
            }
            
            String timeToFactor=" 23:59:59";
            toDate= toDate.concat(timeToFactor);
            searchParameters.setToDate(toDate);
            
          //Setting assignedTo into searchParameters
			searchParameters.setAsignedTo(
					(httpServletReq.getParameter("assignedTo") != null && !(httpServletReq.getParameter("assignedTo").isEmpty()))
					? httpServletReq.getParameter("assignedTo").toString()
					: null
					);
			
			//Setting changeType into searchParameters
			searchParameters.setChangeType(
					(httpServletReq.getParameter("changeType") != null && !("allTypes".equalsIgnoreCase(httpServletReq.getParameter("changeType"))))
					? httpServletReq.getParameter("changeType").toString()
					: null
					);
			
			//Setting changeSource into searchParameters
			searchParameters.setChangeSource(
					(httpServletReq.getParameter("changeSource") != null && !("allTypes".equalsIgnoreCase(httpServletReq.getParameter("changeSource"))))
					? httpServletReq.getParameter("changeSource").toString()
					: null
					);
			
			//Setting requestor into searchParameters			
			searchParameters.setRequestor(
					(httpServletReq.getParameter("requestor") != null && !(httpServletReq.getParameter("requestor").isEmpty()))
					? httpServletReq.getParameter("requestor").toString()
					: null
					);
			
			//Setting status into searchParameters			
			searchParameters.setStatus(
					(httpServletReq.getParameter("status") != null && !("All Status".equalsIgnoreCase(httpServletReq.getParameter("status"))))
					? httpServletReq.getParameter("status").toString()
					: null
					);
			
			searchParameters.setSearchBySRCustTT(
					(httpServletReq.getParameter("searchBySRCustTT") != null && "true"
					.equalsIgnoreCase(httpServletReq
							.getParameter("searchBySRCustTT"))) ? true : false);
			
			searchParameters.setClickedTdId(httpServletReq
					.getParameter("clickedTdId") != null ? httpServletReq
					.getParameter("clickedTdId") : null);			
			

			searchParameters.setNormalSearch(
					(httpServletReq.getParameter("normalSearch") != null && 
					"true".equalsIgnoreCase(httpServletReq.getParameter("normalSearch"))) ? 
							true : false);
			
			searchParameters.setFilterSearch(httpServletReq
					.getParameter("isFilterSearch") != null
					&& "true".equalsIgnoreCase(httpServletReq
							.getParameter("isFilterSearch")) ? true : false);
			
			searchParameters.setAppliedFilters(httpServletReq
					.getParameter("appliedFilters") != null
					&& !(httpServletReq.getParameter("appliedFilters")
							.isEmpty()) ? httpServletReq.getParameter(
					"appliedFilters").toString() : null);
			
			log.info("getAllQueryParameters request param = " + searchParameters.getSearchqueryList() + searchParameters.getTabSelected());			
			
		} catch (Exception e) {		
			throw new TTPortalAppException(e, "SY:1111", TTPortalAppErrors.getErrorMessageByCode("SY:1111"));
		}
		
		return searchParameters;
	}
	
	public List<DatePair> getDateRanges(){
		Calendar cal=Calendar.getInstance();
		cal.add( Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK)-1)); 
		cal.add(Calendar.DATE, -84);
		
		List<DatePair> cycleDateRangeList= new ArrayList<DatePair>();
		
		for(int i=1;i<=12;i++){
			
			DatePair datePair= new DatePair();
			Date date= cal.getTime();
			log.info("loopstartdate--"+getStartDate(date));
			datePair.setStartOfWeek(getStartDate(date));
			cal.add(Calendar.DATE, 6);
			date= cal.getTime();
			log.info("loopenddate--"+getEndDate(date));
			datePair.setEndOfWeek(getEndDate(date));
			cal.add(Calendar.DATE, 1);
			cycleDateRangeList.add(datePair);
		}
		
		return cycleDateRangeList;
		
	}

	public String getStartDate(Date date){
		SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
		String startDate= simpleDateFormat.format(date);
		startDate=startDate.concat(" 00:00:00");
		return startDate;
	}
	
	public String getEndDate(Date date){
		SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
		String startDate= simpleDateFormat.format(date);
		startDate=startDate.concat(" 23:59:59");
		return startDate;
	}
	

	
	@ResourceMapping(value="getSRGraph")
	public void getSRGraphData(ResourceRequest request,ResourceResponse resourceResponse, Model model){
		
		
    	PrintWriter out = null;
    	try{  
	    	
    		log.info("reached sr graph rsource controller-------------------");
	    	
	    	HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil.getHttpServletResponse(resourceResponse);
			
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httprequest, httpresponse);
			
			JSONObject obj = new JSONObject(request.getParameter("graphSRDataIdentifier"));
			
			String initiateSource = obj.getString("initiateSource");
			
			log.info("initiateSource************"+initiateSource);
			
			List<DatePair> cycleDateRange=getDateRanges();
			
			for (int i = 0; i < cycleDateRange.size(); i++) {
				log.info("Start of week--"+cycleDateRange.get(i).getStartOfWeek());
				log.info("End of week--"+cycleDateRange.get(i).getEndOfWeek());
			}
			
			List<Integer> srTrends=null;
			
			if(initiateSource.equalsIgnoreCase("srCustInitiated")){
				srTrends= serviceRequestService.getServiceRequestTrends(cycleDateRange, customerUniqueID);
			}
			else if(initiateSource.equalsIgnoreCase("crCustInitiated")){
				srTrends= serviceRequestService.getChangeRequestTrends(cycleDateRange, customerUniqueID, initiateSource);
			}
			else if(initiateSource.equalsIgnoreCase("crTtInitiated")){
				srTrends= serviceRequestService.getChangeRequestTrends(cycleDateRange, customerUniqueID, initiateSource);
			}
			
			for (int i = 0; i < srTrends.size(); i++) {
				log.info("intermediate level stats---->"+srTrends.get(i).intValue());
				
			}
			
			
			com.liferay.portal.kernel.json.JSONArray resultJsonArray = JSONFactoryUtil
				    .createJSONArray();


			out = resourceResponse.getWriter();
			
			if(!(srTrends.isEmpty()))
			{
				for (int i = 0; i < 12; i++) {
					resultJsonArray.put(srTrends.get(i).intValue());
				}
			}
			
			com.liferay.portal.kernel.json.JSONObject jsonValue = JSONFactoryUtil
					.createJSONObject();
			
			jsonValue.put("variantStatistics", resultJsonArray);
			jsonValue.put("variantCategoryReceived",getTrendTitle(initiateSource));
			
			out.print(jsonValue.toString());

//		    String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httprequest, httpresponse);
				
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	finally{
    		if(null != out) {
    			out.flush();
    			out.close();
    		}
    	}		
	}
	
	public String getTrendTitle(String title){
		if(title.equalsIgnoreCase("srCustInitiated")){
			return "Customer Initiated SR";
		}
		else if(title.equalsIgnoreCase("crCustInitiated")){
			return "Customer Initiated CR";
		}
		else if(title.equalsIgnoreCase("crTtInitiated")){
			return "TelkomTelstra Initiated CR";
		}
		else{
			return "";
		}
	}
		
	//Apply the search sort paging filters to get the result set and respond the json results
	@ResourceMapping(value = "getSRListURI")
    public void getServiceRequestData(ResourceRequest request,ResourceResponse resourceResponse)
	{
		log.info("Class: ServiceRequestController, Method: getServiceRequestData Start");
    	PrintWriter out = null;
    	
    	try{  
	    	
	    	SearchParameters searchParameters = getUserSearchParameters(request,resourceResponse);
	    	
	    	HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil.getHttpServletResponse(resourceResponse);

		    String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httprequest, httpresponse);
			
			ServiceRequestResults serviceRequestResults = serviceRequestService.getAllServiceRequests(searchParameters,customerUniqueID);
			
			com.liferay.portal.kernel.json.JSONObject dtJsonValue = getResultJson(serviceRequestResults,searchParameters.getAdvReqType());
			
			log.info("dtJsonValue in getServiceRequestData : " + dtJsonValue );
			
			out = resourceResponse.getWriter();			
			out.print(dtJsonValue.toString());
			log.info("Class: ServiceRequestController, Method: getServiceRequestData End");
    	}
    	catch(Exception e){
    		log.error("Error in getServiceRequestData " + e.getMessage());
    		throw new TTPortalAppException(e, "SY:2222", TTPortalAppErrors.getErrorMessageByCode("SY:2222"));
    	}
    	finally{
    		if(null != out) {
    			out.flush();
    			out.close();
    		}
    	}		
    }
	
	//convert the result set into a json required for datatable to display with required parameters.

    protected com.liferay.portal.kernel.json.JSONObject getResultJson(ServiceRequestResults serviceRequestResults,String reqType) {
    	
    	log.info("ServiceRequestController, getResultJson, Start");
    	com.liferay.portal.kernel.json.JSONObject dtJsonValue = JSONFactoryUtil.createJSONObject();		
		
		/*for normal search and metrics Grid */
		if((reqType == null && serviceRequestResults.getServiceResults() != null) || "serviceRequest".equalsIgnoreCase(reqType))
		{
			List<ServiceRequest> listServiceRequests=serviceRequestResults.getServiceResults();
			
	    	if (listServiceRequests != null && !(listServiceRequests.isEmpty())) 
	    	{
	    		createJsonForSR(dtJsonValue, serviceRequestResults, listServiceRequests);
	    	}
	    	else 
	    	{
	    		putEmptyJson(dtJsonValue);
			}
		}
		else if((reqType == null && serviceRequestResults.getChangeResults() != null) || "changeRequest".equalsIgnoreCase(reqType))
		{
			List<ChangeRequest> listChangeRequests=serviceRequestResults.getChangeResults();
			
	    	if (listChangeRequests != null && !(listChangeRequests.isEmpty())) 
	    	{
	    		createJsonForCR(dtJsonValue, serviceRequestResults, listChangeRequests);
	    	}
	    	else 
	    	{
	    		putEmptyJson(dtJsonValue);
			}
		}
		else 
    	{
    		putEmptyJson(dtJsonValue);
		}

	return dtJsonValue;
}

private void createJsonForSR(com.liferay.portal.kernel.json.JSONObject dtJsonValue, ServiceRequestResults serviceRequestResults, List<ServiceRequest> listServiceRequests) 
{
	JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
	dtJsonValue.put("draw", serviceRequestResults.getDraw());
	dtJsonValue.put("recordsFiltered",serviceRequestResults.getRecordCount());
	
	resultJsonArray = populateSRData(listServiceRequests);

	dtJsonValue.put("recordsTotal", serviceRequestResults.getRecordCount());
	dtJsonValue.put("data", resultJsonArray);
}

private void createJsonForCR(com.liferay.portal.kernel.json.JSONObject dtJsonValue,  ServiceRequestResults serviceRequestResults, List<ChangeRequest> listChangeRequests) 
{
	JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
	dtJsonValue.put("draw", serviceRequestResults.getDraw());
	dtJsonValue.put("recordsFiltered",serviceRequestResults.getRecordCount());// serviceRequestResults.getRecordsFiltered());
	
	resultJsonArray = populateCRData(listChangeRequests);
	
	dtJsonValue.put("recordsTotal", serviceRequestResults.getRecordCount());
	dtJsonValue.put("data", resultJsonArray);
}


private void putEmptyJson(com.liferay.portal.kernel.json.JSONObject dtJsonValue) 
{
	JSONArray emptyJsonArray = JSONFactoryUtil.createJSONArray();
	dtJsonValue.put("draw", 0);
	dtJsonValue.put("recordsFiltered", 0);
	dtJsonValue.put("recordsTotal", 0);
	dtJsonValue.put("data", emptyJsonArray);
}
    
    public JSONArray populateSRData(List<ServiceRequest> listServiceRequests)
    {
    	JSONArray rowJsonValue;
    	
    	JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
    	
    	for (ServiceRequest serviceRequest : listServiceRequests) 
    	{
    		rowJsonValue = JSONFactoryUtil.createJSONArray();
    		rowJsonValue.put("<a href=\"javascript:void(0)\" class=\"service-request-id-links\" onclick=\"getSingleServiceRequestDetails(this, '" + serviceRequest.getRequesttype() + "')\" id=\""+serviceRequest.getRequestid()+"\">"+serviceRequest.getRequestid()+"</a>");
			rowJsonValue.put("<span class=\"tt-sr-subject\">"+serviceRequest.getSubject()+"</span><br><span class=\"tt-sr-description\">"+serviceRequest.getDescription()+"</span>");
			rowJsonValue.put(serviceRequest.getRequesttype());
			
			if(serviceRequest.getLastupdatedate()!=null){
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String lastUpdatedString = dateFormat.format(serviceRequest.getLastupdatedate());
				rowJsonValue.put(lastUpdatedString);
			}
			else{
				rowJsonValue.put("");
			}
			
			rowJsonValue.put(serviceRequest.getRequestor());
			rowJsonValue.put(serviceRequest.getStatus());				
			
			resultJsonArray.put(rowJsonValue);				
		}
    	return resultJsonArray;
    }
    
    public JSONArray populateCRData(List<ChangeRequest> listChangeRequests){
    	JSONArray rowJsonValue;
    	JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();
    	
    	for (ChangeRequest changeRequest : listChangeRequests) {
    		rowJsonValue = JSONFactoryUtil.createJSONArray();
			rowJsonValue.put("<a href=\"javascript:void(0)\" class=\"service-request-id-links\" onclick=\"getSingleServiceRequestDetails(this, 'Change')\" id=\""+changeRequest.getChangerequestid()+"\">"+changeRequest.getChangerequestid()+"</a>");
			rowJsonValue.put("<span class=\"tt-sr-subject\">"+changeRequest.getSubject()+"</span><br><span class=\"tt-sr-description\">"+changeRequest.getDescription()+"</span>");
			rowJsonValue.put("Change");
			
			if(changeRequest.getLastupdatedate()!=null){
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String lastUpdatedString = dateFormat.format(changeRequest.getLastupdatedate());
				rowJsonValue.put(lastUpdatedString);
			}
			else{
				rowJsonValue.put("");
			}
			
			rowJsonValue.put(changeRequest.getRequestor());
			rowJsonValue.put(changeRequest.getStatus());				
			
			resultJsonArray.put(rowJsonValue);				
		}
    	return resultJsonArray;
    }
    
    @ResourceMapping(value = "getRequestDetails")
	public void getRequestDetails(ResourceRequest req,ResourceResponse res) {
		log.info("Controller for getting SR/CR Request details");

		try {

			HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
			HttpServletResponse response = PortalUtil.getHttpServletResponse(res);

			/*ThemeDisplay td = (ThemeDisplay) request
					.getAttribute(WebKeys.THEME_DISPLAY);*/
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(request, response);
			log.info("setting Cust ID");

			org.json.JSONObject obj = new JSONObject(request.getParameter("serviceRequestFilterIdentifier"));

			String requestId = obj.getString("requestid");
			String requestType = obj.getString("requestType");
			log.info("requestType in controller to call sr/cr details:: >>>"+requestType+"<<<<");
			log.info("request id from jsp :: " + requestId);
			
			if("Change".equalsIgnoreCase(requestType))
			{
				log.info("within if of change");
				getChangeRequestDetails(response,requestType,requestId,customerUniqueID);
				
				log.info("if change end");
			}
			else if("Service".equalsIgnoreCase(requestType)){
				log.info("within if of service");
				getServiceRequestDetails(response,requestType,requestId);
			}
				
			log.info("Population completed for Request details");

		} 
		catch (Exception e) {
			log.error("Exception while getting request details "+e.getMessage());
		}
	}
    
    //Method to fetch SR details
    public void getServiceRequestDetails(HttpServletResponse response,String requestType,String requestId){
    	
    	try {
			/*com.liferay.portal.kernel.json.JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();*/
    		PrintWriter out;
			out = response.getWriter();
			
			DetailsSearchParameters detailsSearchParameters=new DetailsSearchParameters();
			detailsSearchParameters.setRequestType(requestType);
			
			ServiceRequest serviceRequest=serviceRequestService.getServiceRequestDetailsForRequestId(requestId, detailsSearchParameters);
			if(serviceRequest!=null){
				
				List<ServiceRequestItem> listOfServiceRequestItems=serviceRequest.getServiceRequestItem();
				
				com.liferay.portal.kernel.json.JSONObject parentJsonValue = JSONFactoryUtil.createJSONObject();		
				parentJsonValue.put("summary", serviceRequest.getSubject());
				parentJsonValue.put("serviceRequestId", serviceRequest.getRequestid());
				parentJsonValue.put("status", serviceRequest.getStatus());
				parentJsonValue.put("requestor", serviceRequest.getRequestor());
				
				if(serviceRequest.getLastupdatedate()!=null){
					SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String lastUpdatedDateString= simpleDateFormat.format(serviceRequest.getLastupdatedate());
					parentJsonValue.put("lastUpdated", lastUpdatedDateString);
				}
				else{
					parentJsonValue.put("lastUpdated", "");
				}
				
				if(serviceRequest.getCreateddate()!=null){
					SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String createdOnString= simpleDateFormat.format(serviceRequest.getCreateddate());
					parentJsonValue.put("createdOn", createdOnString);
				}
				else{
					parentJsonValue.put("createdOn", "");
				}
				
				if(serviceRequest.getStartdate()!=null){
					SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String startDateString= simpleDateFormat.format(serviceRequest.getStartdate());
					parentJsonValue.put("startDate", startDateString);
				}
				else{
					parentJsonValue.put("startDate", "");
				}
				
				if(serviceRequest.getEnddate()!=null){
					SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String endDateString= simpleDateFormat.format(serviceRequest.getEnddate());
					parentJsonValue.put("endDate", endDateString);
				}
				else{
					parentJsonValue.put("endDate", "");
				}
				
				parentJsonValue.put("requestType", serviceRequest.getRequesttype());
				
				parentJsonValue.put("description", serviceRequest.getDescription());
				
				/*parentJsonValue.put("outagePresent", serviceRequest.getOutageendtime());
				parentJsonValue.put("outageStartTime", serviceRequest.getOutagestarttime());
				parentJsonValue.put("outageEndTime", serviceRequest.getOutageendtime());*/
				
				SimpleDateFormat crDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				/*parentJsonValue.put("plannedStartDate", serviceRequest.getPlannedstartdate() != null ?  crDateFormat1.format(serviceRequest.getPlannedstartdate()) : "");
				parentJsonValue.put("plannedEndDate", serviceRequest.getPlannedenddate() != null ?  crDateFormat1.format(serviceRequest.getPlannedenddate()) : "");
				parentJsonValue.put("outageStartTime", serviceRequest.getOutagestarttime() != null ? crDateFormat1.format(serviceRequest.getOutagestarttime()) : "");
				parentJsonValue.put("outageEndTime", serviceRequest.getOutageendtime() != null ? crDateFormat1.format(serviceRequest.getOutageendtime()) : "");

				parentJsonValue.put("category", serviceRequest.getCategory() != null ? serviceRequest.getCategory() : "");
				parentJsonValue.put("risk", serviceRequest.getRisk() != null ? serviceRequest.getRisk() : "");
				parentJsonValue.put("deviceName", serviceRequest.getDevicename() != null ? serviceRequest.getDevicename() : "");
				parentJsonValue.put("priority", serviceRequest.getPriority() != null ? serviceRequest.getPriority() : "");
				parentJsonValue.put("impact", serviceRequest.getImpact() != null ? serviceRequest.getImpact() : "");
				parentJsonValue.put("resolverGroup", serviceRequest.getResolvergroup() != null ? serviceRequest.getResolvergroup() : "");
				parentJsonValue.put("caseManager", serviceRequest.getCasemanager() != null ? serviceRequest.getCasemanager() : "");
				parentJsonValue.put("comments", serviceRequest.getComments() != null ? serviceRequest.getComments() : "");				
				parentJsonValue.put("outagePresent", serviceRequest.getOutagepresent() != null ? serviceRequest.getOutagepresent() : "");				
				parentJsonValue.put("serviceName", serviceRequest.getServicename() != null ? serviceRequest.getServicename() : "");
				parentJsonValue.put("siteName", serviceRequest.getSitename() != null ? serviceRequest.getSitename() : "");*/
				
				com.liferay.portal.kernel.json.JSONArray sriJsonArray = JSONFactoryUtil.createJSONArray();
				
				for (ServiceRequestItem serviceRequestItem : listOfServiceRequestItems) {
					com.liferay.portal.kernel.json.JSONArray sriJsonValue = JSONFactoryUtil.createJSONArray();
					
					sriJsonValue.put("<span class=\"tt-sri-itemid\">"+serviceRequestItem.getItemid()+"</span>");
					String itemtype=serviceRequestItem.getItemtype();
					if(itemtype.equalsIgnoreCase(GenericConstants.RITM_MNS_NEW)){
						sriJsonValue.put(GenericConstants.RITM_MNS_NEW_DISPALY);
					}
					else{
						sriJsonValue.put(itemtype);
					}
					
					sriJsonValue.put(serviceRequestItem.getSubcategory());
					sriJsonValue.put(serviceRequestItem.getProjecttype());
					sriJsonValue.put(serviceRequestItem.getStatus());
					if(serviceRequestItem.getLastupdate()!=null){
						SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String lastUpdateString= simpleDateFormat.format(serviceRequestItem.getLastupdate());
						sriJsonValue.put(lastUpdateString);
					}
					else{
						sriJsonValue.put("");
					}
					sriJsonValue.put("<a href=\"javascript:void(0)\" class=\"drillDownArrow\" onclick=\"getSingleServiceRequestItemDetails(this)\" id=\""+serviceRequestItem.getItemid()+"\"></a>");
					
					sriJsonArray.put(sriJsonValue);
				}
				
				com.liferay.portal.kernel.json.JSONObject childJson = JSONFactoryUtil.createJSONObject();
				
				childJson.put("draw", 0);
				childJson.put("recordsFiltered", sriJsonArray.length());
				childJson.put("recordsTotal", sriJsonArray.length());
				childJson.put("data", sriJsonArray);
				
				parentJsonValue.put("datatableServiceRequestItems", childJson);
				
				
				log.info("JSON for SR details--"+parentJsonValue.toString());
				out.print(parentJsonValue.toString());
			}
			
			else{
				out.print("No data in SR");
			}
			log.info("Population completed for SR details");
		}
    	catch (Exception e) {
			e.printStackTrace();
		}
    		
    			
    }
			
    //Method to fetch CR Details - Divya
    public void getChangeRequestDetails(HttpServletResponse response,String requestType,String requestId, String customerId) {
    	log.info("getChangeRequestDetails ");
		List<ChangeRequestNotes> commentsList = new ArrayList<ChangeRequestNotes>();
  
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
    	try {
		    	
				/*com.liferay.portal.kernel.json.JSONArray resultJsonArray = JSONFactoryUtil.createJSONArray();*/

				PrintWriter out;
				out = response.getWriter();
				
				DetailsSearchParameters detailsSearchParameters=new DetailsSearchParameters();
				detailsSearchParameters.setRequestType(requestType);

				log.info("request id :: " + requestId + ",  " + requestType);
				
				ChangeRequest changeRequest=serviceRequestService.getChangeRequestDetailsForRequestId(requestId, detailsSearchParameters, customerId);
				log.info("back to controller --- changerequest :: " + changeRequest);
				
				if(changeRequest!=null){
				
					com.liferay.portal.kernel.json.JSONObject parentJsonValue = JSONFactoryUtil.createJSONObject();		
					parentJsonValue.put("summary", changeRequest.getSubject());
					parentJsonValue.put("serviceRequestId", changeRequest.getChangerequestid());
					parentJsonValue.put("status", changeRequest.getStatus());
					parentJsonValue.put("requestor", changeRequest.getRequestor());
					
					if(changeRequest.getLastupdatedate()!=null){
						String lastUpdatedDateString= simpleDateFormat.format(changeRequest.getLastupdatedate());
						parentJsonValue.put("lastUpdated", lastUpdatedDateString);
					}
					else{
						parentJsonValue.put("lastUpdated", "");
					}
					
					if(changeRequest.getCreateddate()!=null){
						String createdOnString= simpleDateFormat.format(changeRequest.getCreateddate());
						parentJsonValue.put("createdOn", createdOnString);
					}
					else{
						parentJsonValue.put("createdOn", "");
					}
					
					if(changeRequest.getPlannedstartdate()!=null){
						String startDateString= simpleDateFormat.format(changeRequest.getPlannedstartdate());
						parentJsonValue.put("startDate", startDateString);
					}
					else{
						parentJsonValue.put("startDate", "");
					}
					
					if(changeRequest.getPlannedenddate()!=null){
						String endDateString= simpleDateFormat.format(changeRequest.getPlannedenddate());
						parentJsonValue.put("endDate", endDateString);
					}
					else{
						parentJsonValue.put("endDate", "");
					}
					
					if(changeRequest.getChangeRequestDevices()!=null && changeRequest.getChangeRequestDevices().size()>0){
						
						String deviceName = ""; 
						for (ChangeRequestDevices devices : changeRequest.getChangeRequestDevices()){
							deviceName = deviceName + devices.getCiname() +", "; 
						}
						deviceName = deviceName.substring(0, deviceName.length()-2); 
						parentJsonValue.put("deviceName", deviceName);	
					} 
					else {
						parentJsonValue.put("deviceName", "");	
					} 
					
					if(changeRequest!=null && changeRequest.getChangeRequestServices()!=null && changeRequest.getChangeRequestServices().size()>0){
						
						String serviceName = ""; 
						for (ChangeRequestServices services : changeRequest.getChangeRequestServices()){
							serviceName = serviceName + services.getServicename() +", "; 
						}
						serviceName= serviceName.substring(0, serviceName.length()-2); 
						parentJsonValue.put("serviceName", serviceName);	
					} 
					else {
						parentJsonValue.put("serviceName", "");	
					} 
					
					parentJsonValue.put("requestType", "Change");
					parentJsonValue.put("changeType", changeRequest.getChangetype());
					parentJsonValue.put("changeSource", changeRequest.getChangesource());
					
					parentJsonValue.put("description", changeRequest.getDescription());
					
					parentJsonValue.put("outagePresent", changeRequest.getOutagepresent());
					
					if(changeRequest.getOutagestarttime()!=null){
						
						String outageStartTime= simpleDateFormat.format(changeRequest.getOutagestarttime());
						parentJsonValue.put("outageStartTime", outageStartTime);
					}
					else{
						parentJsonValue.put("outageStartTime", "");
					}
					
					if(changeRequest.getOutageendtime()!=null){
						String outageEndTime= simpleDateFormat.format(changeRequest.getOutageendtime());
						parentJsonValue.put("outageEndTime", outageEndTime);
					}
					else{
						parentJsonValue.put("outageStartTime", "");
					}
					
					parentJsonValue.put("category", changeRequest.getCategory() != null ? changeRequest.getCategory() : "");
					parentJsonValue.put("risk", changeRequest.getRisk() != null ? changeRequest.getRisk() : "");
					parentJsonValue.put("priority", changeRequest.getPriority() != null ? changeRequest.getPriority() : "");
					parentJsonValue.put("impact", changeRequest.getImpact() != null ? changeRequest.getImpact() : "");
					parentJsonValue.put("resolverGroup", changeRequest.getResolvergroup() != null ? changeRequest.getResolvergroup() : "");
					parentJsonValue.put("caseManager", changeRequest.getCasemanager() != null ? changeRequest.getCasemanager() : "");
					parentJsonValue.put("siteName", changeRequest.getSitename() != null ? changeRequest.getSitename() : "");

				com.liferay.portal.kernel.json.JSONArray commentsArrayCR = JSONFactoryUtil
						.createJSONArray();
				commentsList.addAll(changeRequest.getChangeRequestNotes());
				
				for (ChangeRequestNotes changeRequestNotes : commentsList) {
					com.liferay.portal.kernel.json.JSONObject commentBodyJson = JSONFactoryUtil
							.createJSONObject();
					if (changeRequestNotes.getUserid() != null) {
						commentBodyJson.put("userId",
								changeRequestNotes.getUserid());
					} else {
						commentBodyJson.put("userId", " ");
					}

					if (changeRequestNotes.getCreateddatetime() != null) {
						String modifiedDateString = simpleDateFormat
								.format(changeRequestNotes.getCreateddatetime());
						commentBodyJson.put("modifiedDate", modifiedDateString);
					} 
					else {
						commentBodyJson.put("modifiedDate", "");
					}

					commentBodyJson.put("notes",
							changeRequestNotes.getNotes());

					commentsArrayCR.put(commentBodyJson);

				}


				parentJsonValue.put("comments", commentsArrayCR);				
				
				/*new attributes of change request start*/
				
				if(changeRequest.getRequestedbydate() != null)
				{
					String requestedByDate = simpleDateFormat.format(changeRequest.getRequestedbydate());
					parentJsonValue.put("requestedByDate", requestedByDate);
				}
				
				if(changeRequest != null && changeRequest.getChangeRequestApprovals() != null && !changeRequest.getChangeRequestApprovals().isEmpty())
				{
					String approver = "";
					String approvalDate = "";
					String approvalGroup = "";
					
					for(ChangeRequestApprovals changeRequestApprovals : changeRequest.getChangeRequestApprovals())
					{
						approver = approver + changeRequestApprovals.getApprover() + ", ";
						approvalDate = approvalDate + simpleDateFormat.format(changeRequestApprovals.getApprovaldate()) + ", ";
						approvalGroup = approvalGroup + changeRequestApprovals.getApprovalgroup() + ", ";
					}
					approver = approver.substring(0, approver.length() - 2);
					approvalDate = approvalDate.substring(0, approvalDate.length()-2);
					approvalGroup = approvalGroup.substring(0, approvalGroup.length() - 2);
					
					parentJsonValue.put("approver", approver);
					parentJsonValue.put("approvalDate", approvalDate);
					parentJsonValue.put("approvalGroup", approvalGroup);
				}
				else
				{
					parentJsonValue.put("approver", "");
					parentJsonValue.put("approvalDate", "");
					parentJsonValue.put("approvalGroup", "");
				}
				
				parentJsonValue.put("customerApprover", changeRequest.getCustomerapprover() != null ? changeRequest.getCustomerapprover() : "");
				
				if(changeRequest.getCustomerapprovaldate() != null)
				{
					String customerApproverDate = simpleDateFormat.format(changeRequest.getCustomerapprovaldate());
					parentJsonValue.put("customerApprovalDate", customerApproverDate);
				}
				
				parentJsonValue.put("outcomeStatus", changeRequest.getOutcomestatus() != null ? changeRequest.getOutcomestatus() : "");
				
				/*new parameters added end*/
				

				log.info("JSON for CR details--"+parentJsonValue.toString());
				out.print(parentJsonValue.toString());   
				
				}			
				else{
					log.info("No data in CR");
					out.print("No data in CR");
				}
				log.info("Population completed for CR details");	
			} catch (Exception e) {
				e.printStackTrace();
			}
			
    }
    
    
    @ActionMapping(value = "refreshSR")
    public void dataRefresh(ActionRequest actionRequest,
	    ActionResponse actionResponse) throws IOException,
	    PortletException, PortalException, SystemException {

	log.info("refreshSR.------------------");
	String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(actionRequest), PortalUtil.getHttpServletResponse(actionResponse));
	log.info("customerId--------------->>" + customerId);
	serviceRequestService.dataRefresh(customerId);
	String url = prop.getProperty("tt.homeURL") + "/" + prop.getProperty("ttServiceRequest");
	actionResponse.sendRedirect(url);
    }

    
    @ResourceMapping(value = "getServiceRequestItemDetails")
	public void getServiceRequestItemDetails(ResourceRequest req,
			ResourceResponse res, Model model) {
		log.info("Controller for SRI details");

		try {

			HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
			HttpServletResponse response = PortalUtil
					.getHttpServletResponse(res);

			/*ThemeDisplay td = (ThemeDisplay) request
					.getAttribute(WebKeys.THEME_DISPLAY);*/
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(request, response);
			log.info("setting Cust ID");

			org.json.JSONObject obj = new JSONObject(
					request.getParameter("serviceRequestItemFilterIdentifier"));

			String itemId = obj.getString("itemid");

			com.liferay.portal.kernel.json.JSONArray resultJsonArray = JSONFactoryUtil
					.createJSONArray();

			PrintWriter out;
			out = response.getWriter();

			ServiceRequestItem serviceRequestItem= serviceRequestService.getServiceRequestItemDetailsForItemId(itemId);
			
			ServiceRequest serviceRequest=null;
			if(serviceRequestItem!=null){
				
				List<ServiceRequestItemNotes> comments=serviceRequestItem.getServiceRequestItemNotesList();
				
				List<ServiceRequestItemDevices> devices= serviceRequestItem.getServiceRequestItemDevicesList();
				
				com.liferay.portal.kernel.json.JSONObject parentJsonValue = JSONFactoryUtil.createJSONObject();		
				parentJsonValue.put("ritmId", serviceRequestItem.getItemid());
				parentJsonValue.put("serviceName", serviceRequestItem.getServicename());
				parentJsonValue.put("siteName", serviceRequestItem.getSitename());
				
				com.liferay.portal.kernel.json.JSONArray deviceNameArray = JSONFactoryUtil
						.createJSONArray();
				
				for (ServiceRequestItemDevices serviceRequestItemDevices : devices) {
					deviceNameArray.put(serviceRequestItemDevices.getCiname());
				}
				
				parentJsonValue.put("deviceNameList", deviceNameArray);
				
				com.liferay.portal.kernel.json.JSONArray commentsArray = JSONFactoryUtil
						.createJSONArray();
				
				for (ServiceRequestItemNotes serviceRequestItemsNotes : comments) {
					com.liferay.portal.kernel.json.JSONObject commentBodyJson = JSONFactoryUtil.createJSONObject();
					if(serviceRequestItemsNotes.getUsername()!=null){
						commentBodyJson.put("userId", serviceRequestItemsNotes.getUsername());
					}
					else{
						commentBodyJson.put("userId", " ");
					}
					
					if(serviceRequestItemsNotes.getModifiedtime()!=null){
						SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						String modifiedDateString= simpleDateFormat.format(serviceRequestItemsNotes.getModifiedtime());
						commentBodyJson.put("modifiedDate", modifiedDateString);
					}
					else{
						commentBodyJson.put("modifiedDate", "");
					}
					
					commentBodyJson.put("notes", serviceRequestItemsNotes.getCustinfonotes());
					
					commentsArray.put(commentBodyJson);
					
				}
				
				parentJsonValue.put("commentsList", commentsArray);

				
				log.info("SRI details JSON--"+parentJsonValue.toString());
				out.print(parentJsonValue.toString());
			    
				
			}
			
			else{
				out.print("No data");
			}
			
			log.info("Population completed for SRI details");


		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

    public void RenderLogServiceRequest (RenderRequest request, RenderResponse renderResponse, Model model) {
    	try{
			log.info("Rendering Service Request Page - Log a Service Request");
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request), PortalUtil.getHttpServletResponse(renderResponse));
			if(customerUniqueID != null){
				log.info("Rendering Service Request Modal for user : " + customerUniqueID);
				
				
				List<String> productTypeList =createSRManager.getProductTypeList();
				
				productTypeList.add(0, "--Select--");
				model.addAttribute("productTypeList",productTypeList);
				
				LinkedHashMap<String, String> regionList = createSRManager.getRegionNameMap(customerUniqueID);
				LinkedHashMap<String, String> regionListActual = new LinkedHashMap<String, String>();
				regionListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : regionList.entrySet()){
					regionListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("regionList", regionListActual);
				
				LinkedHashMap<String, String> siteNameList = new LinkedHashMap<String, String>();
				siteNameList.put("0", "--Select--");
				model.addAttribute("siteNameList", siteNameList);
				
				LinkedHashMap<String, String> serviceIDList = new LinkedHashMap<String, String>();
				serviceIDList.put("0", "--Select--");
				model.addAttribute("serviceIDList", serviceIDList);
				
				log.info("Testing Property file : " + prop.getProperty("lovNameAction"));
				
				LinkedHashMap<String, String> actionListActual = new LinkedHashMap<String, String>();
				actionListActual.put("0", "--Select--");
				model.addAttribute("actionListMap", actionListActual);
				
				LinkedHashMap<String, String> subCategoryList = new LinkedHashMap<String, String>();
				subCategoryList.put("0", "--Select--");
				model.addAttribute("subCateogryListMap", subCategoryList);
				
				LinkedHashMap<String, String> deviceList = new LinkedHashMap<String, String>();
				deviceList.put("0", "--Select--");
				model.addAttribute("deviceNameListMap", deviceList);
				
				LinkedHashMap<String, String> prodClassList = new LinkedHashMap<String, String>();
				prodClassList.put("0", "--Select--");
				model.addAttribute("productClassificationListMap", prodClassList);
				
				model.addAttribute("logSRFormModel", new CreateSRForm());
			}
			else{
				log.debug("Rendering without any user details - CustomerUniqueID not found.");
				model.addAttribute("logSRFormModel", new CreateSRForm());
			}
			
		}
		catch (Exception e){
			log.error("Error while rendering for Log A New Request - " + e.getMessage());
		}
    }
	
   /* public void LogServiceRequest (RenderRequest request, RenderResponse renderResponse, Model model) {
    	try{
			log.info("Rendering Service Request Page - Log a Service Request");
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request), PortalUtil.getHttpServletResponse(renderResponse));
			if(customerUniqueID != null){
				log.info("Rendering Service Request Modal for user : " + customerUniqueID);
				
				List<String> productTypeList =createSRManager.getProductTypeList();
				
				productTypeList.add(0, "--Select--");
				model.addAttribute("productTypeList",productTypeList);
				
				LinkedHashMap<String, String> regionList = createSRManager.getRegionNameMap(customerUniqueID);
				LinkedHashMap<String, String> regionListActual = new LinkedHashMap<String, String>();
				regionListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : regionList.entrySet()){
					regionListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("regionList", regionListActual);
				
				LinkedHashMap<String, String> siteNameList = new LinkedHashMap<String, String>();
				siteNameList.put("0", "--Select--");
				model.addAttribute("siteNameList", siteNameList);
				
				LinkedHashMap<String, String> serviceIDList = new LinkedHashMap<String, String>();
				serviceIDList.put("0", "--Select--");
				model.addAttribute("serviceIDList", serviceIDList);
				
				log.info("Testing Property file : " + prop.getProperty("lovNameAction"));
				
				LinkedHashMap<String, String> actionListActual = new LinkedHashMap<String, String>();
				actionListActual.put("0", "--Select--");
				model.addAttribute("actionListMap", actionListActual);
				
				LinkedHashMap<String, String> subCategoryList = new LinkedHashMap<String, String>();
				subCategoryList.put("0", "--Select--");
				model.addAttribute("subCateogryListMap", subCategoryList);
				
				LinkedHashMap<String, String> deviceList = new LinkedHashMap<String, String>();
				deviceList.put("0", "--Select--");
				model.addAttribute("deviceNameListMap", deviceList);
				
				LinkedHashMap<String, String> prodClassList = new LinkedHashMap<String, String>();
				prodClassList.put("0", "--Select--");
				model.addAttribute("productClassificationListMap", prodClassList);
				
				model.addAttribute("logSRFormModel", new CreateSRForm());
			}
			else{
				log.debug("Rendering without any user details - CustomerUniqueID not found.");
				model.addAttribute("logSRFormModel", new CreateSRForm());
			}
			
		}
		catch (Exception e){
			log.error("Error while rendering for Log A New Request - " + e.getMessage());
		}
    }*/
    
    @ResourceMapping(value="submitRequestURL")
	public void handleLogServiceRequest(@Valid @ModelAttribute("logSRFormModel") CreateSRForm srForm, Model model,ResourceRequest request, ResourceResponse response) throws Exception{
    	HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpresponse = PortalUtil
				.getHttpServletResponse(response);
		JSONObject json = new JSONObject();
    	try{
			
    		User user = UserLocalServiceUtil.getUser((Long)httprequest.getSession().getAttribute(WebKeys.USER_ID));
			RefreshUtil.setProxyObject();
			createServiceRequestWSCallService callService = new createServiceRequestWSCallService();
			Incident incident = new Incident();
			
			incident.setUserid(user.getEmailAddress()+"");
			incident.setServiceid(srForm.getServiceName());
			incident.setResolvergroupname("");
			incident.setImpact(prop.getProperty("defaultSRImpact"));
			String language = ((String) user.getExpandoBridge().getAttribute("preferredLanguage")).equalsIgnoreCase("en_US") ? "English" : "Bahasa";
			incident.setCustomerpreferedlanguage(language);
			incident.setSummary(srForm.getSummary());
			String category;
			try{
				category = getCategoryForSubmission(srForm.getCategory(),srForm.getServiceName());
			}
			catch(Exception e){
				category="";
			}
			
			if(category == "SaaS Business Service" || category.equals("SaaS Business Service")){
				category = "SaaS";
			}
			
			incident.setCategory(category);
			incident.setSubcategory(srForm.getSubCategory());
			incident.setTicketsource(prop.getProperty("RequestSource"));
			incident.setTickettype(prop.getProperty("ServiceRequestTicketType"));
			incident.setUrgency(prop.getProperty("defaultSRUrgency"));
			
			
			String selectedAction = srForm.getAction();
			if(selectedAction!=null && selectedAction.trim().length()!=0 && selectedAction!="0"){
				String actualNotes = srForm.getDescription();
				String appendActionText = prop.getProperty("ActionText") + " " + selectedAction+".";
				log.info("Action Message to be appended - " + appendActionText);
				incident.setWorklognotes(appendActionText+" "+actualNotes);
			}else{
				log.info("Selected Category -" +srForm.getCategory());
				log.debug("If Category is MACD then the validation has failed as No action has been selected.");
				incident.setWorklognotes(srForm.getDescription());
			}
			
			log.info("Multiple Service Impacted - " + srForm.isMSI());
			if(srForm.isMSI()){
				String actualNotes = incident.getWorklognotes();
				String appendMIM = prop.getProperty("MIMText");
				log.info("MIM Message to be appended - " + appendMIM);
				incident.setWorklognotes(appendMIM+" "+actualNotes);
			}
			
			String prodClassBefore = incident.getWorklognotes();
			
			String prodClassAfter = prodClassBefore+"\nProduct Classification:"+srForm.getProductClassification();
			incident.setWorklognotes(prodClassAfter);
			
			log.info("Description being sent to SNOW - " + incident.getWorklognotes());
			InsertResponse insert = new InsertResponse();
			boolean isException = false;
			try{
				insert= callService.insertIncidentWebService(incident);
			}
			catch(Exception ce){
				isException=true;
			}
			if(!isException){
				log.info("this is the end of call"+insert.getDisplayName()+"::"+insert.getDisplayValue()+""+insert.getErrorMessage());
				
				log.info("Display Response: " + insert.getDisplayValue());
				log.info("Error Response: " + insert.getErrorMessage());
				if(insert.getDisplayValue() !=null && insert.getDisplayValue().trim().length()!=0){
					json.put("requestID", "Success@"+insert.getDisplayValue());
				}
				else{
					json.put("requestID", "Error@"+insert.getErrorMessage());
				}
				json.put("incidentID", insert.getDisplayValue());
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}
			else{
				log.error("Connection timed out");
				json.put("requestID", "Error@"+"Connection Timed Out. Please try again later.");
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}
			
		}
		catch(Exception e){
			log.error("Error while submitting Service Request" + e.getMessage());
		}
    }
	
	
	@ResourceMapping(value="getSiteURL")
	public void handleSiteIDFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		try{
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedRegionJson"));
			String selectedRegion = jsonObj.getString("selectedRegion");
			if(selectedRegion!=null && selectedRegion.trim().length()!=0){
				String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req,httpServletResponse);
				LinkedHashMap<String, String> siteIDListtemp = createSRManager.getSiteFromRegion(selectedRegion,customerUniqueID);
				String responseStr = "";
				if(siteIDListtemp!=null){
					for( Map.Entry<String,String> entry : siteIDListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
		}
		catch(Exception e){
			log.error("Error while fetching Site from Region in controller - " + e.getMessage());
			
		}
	}
	
	@ResourceMapping(value="getDeviceURL")
	public void handleDeviceFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		
        try{
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedServiceJson"));
			String selectedService = jsonObj.getString("selectedService");
			String selectedProductType = jsonObj.getString("selectedProductID");
			String selectedSubCategory = jsonObj.getString("selectedSubCategory");
			
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req,httpServletResponse);
			
			if(selectedService!=null && selectedService.trim().length()!=0){
				log.info("Selected Service in controller for get devices : " + selectedService+ ""
						+ "\n Selected Product type in controller for devices:::"+selectedProductType+""
								+ "\n Selected sub-category in controlelr for devices::"+selectedSubCategory);
				LinkedHashMap<String, String> deviceListtemp = createSRManager.getSRDevices(selectedProductType, selectedService, selectedSubCategory, customerUniqueID);
				String responseStr = "";
				if(deviceListtemp!=null){
					for( Map.Entry<String,String> entry : deviceListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
				else{
					log.info("Getting Zero devices for selected Service");
				}
			}
		}
		catch(Exception e){
			log.error("Error while fetching Devices for service in controller - " + e.getMessage());
			
		}
	}
	
	@ResourceMapping(value="getSubCategoryURL")
	public void handleActionSubCategoryFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
        
		try{
			String responseStr1 = "";
			String responseStr2 = "";
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedServiceJson"));
			String selectedService = jsonObj.getString("selectedService");
			String category = jsonObj.getString("categoryValue");
			String selectedProductType = jsonObj.getString("selectedProductID");
			
			if(selectedService!=null && selectedService.trim().length()!=0){
				log.info("Selected Service in controller for Subcategory Fetch: " + selectedService);
				log.info("Selected Category in controller for Action Fetch: " + category);
				log.info("Selected Service in controller for Subcategory Fetch:" +selectedProductType);
				
				if(category.toUpperCase()!="GENERIC"){
					LinkedHashMap<String, String> actionListtemp = createSRManager.getSRAction(prop.getProperty("lovNameAction"));
					if(actionListtemp!=null){
						for( Map.Entry<String,String> entry : actionListtemp.entrySet()){
							responseStr1 += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
						}
					}
				}
				if(selectedService!="0"){
					LinkedHashMap<String, String> subCategoryListtemp = createSRManager.getSubCategory(selectedProductType, category, selectedService);
					if(subCategoryListtemp!=null){
						for( Map.Entry<String,String> entry : subCategoryListtemp.entrySet()){
							responseStr2 += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
						}
					}
				}
				
				PrintWriter out = response.getWriter();
				out.print(responseStr1+"%%%%%"+responseStr2);
				
			}
		}
		catch(Exception e){
			log.error("Error while fetching Action & Subcategory in controller - " + e.getMessage());
			
		}
	}
	
	public String getCategoryForSubmission(String selectedCateogry, String selectedService) throws Exception{
		try{
			if(selectedCateogry.equalsIgnoreCase("MACD")){
				return createSRManager.fetchCategoryForMACD(selectedService);
			}
			else{
				return prop.getProperty("GenericCategory");
			}
		}
		catch(Exception e){
			log.error("Error while fetching Category for MACD to be submitted to SNOW in controller - " + e.getMessage());
			throw e;
		}
		finally{
			
		}
	}
	
	
	 @ResourceMapping(value = "getStatusURL")
	    public void handleStatusIDFetch(Model model, ResourceRequest request,
		    ResourceResponse response) throws Exception {
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		try {
		    JSONObject jsonObj = new JSONObject(
			    req.getParameter("selectedStatusIDJson"));
		    String selectedStatus = jsonObj.getString("selectedStatusID");
		    if (selectedStatus != null && selectedStatus.trim().length() != 0) {
			log.info("Selected Status in controller : "
				+ selectedStatus);
			Map<String, String> statusListActual = new LinkedHashMap<String, String>();
			statusListActual.put("allStatus","All Status");
			if(selectedStatus.equalsIgnoreCase("activeTab")){
				statusListActual.put(statusNew,statusNew);
				statusListActual.put(statusAssigned,statusAssigned);
				statusListActual.put(statusPending,statusPending);
				statusListActual.put(statusInProgress,statusInProgress);
			}
			else{
				statusListActual.put(statusCompleted,statusCompleted);
				statusListActual.put(statusClosedCancelled,statusClosedCancelled);
			}
			String responseStr = "";
			if (statusListActual != null) {
			    for (Map.Entry<String, String> entry : statusListActual
				    .entrySet()) {
				responseStr += (entry.getKey() + "@@@@@"
					+ entry.getValue() + "#####");
			    }
			    PrintWriter out = response.getWriter();
			    out.print(responseStr);
			}
		    }
		} catch (Exception e) {
		    log.error("Exception while retrieving status "+e.getMessage());
		    throw e;
			}
	    }
	 
	//convert the result set into a json required for datatable to disply with required parameters.
    protected com.liferay.portal.kernel.json.JSONObject getMetricsJson(ServiceRequestResults serviceRequestResults, ServiceRequestResults changeRequestResults) 
    {
    	log.info("ServiceRequestController getMetricsJson Start");
    	
    	com.liferay.portal.kernel.json.JSONObject dtJsonValue = JSONFactoryUtil.createJSONObject();	
    	
    	Map<String, Integer> srHashMap = serviceRequestResults.getServiceResultsMetric();
    	Map<String, Integer> crHashMap = changeRequestResults.getChangeResultsMetric();
    	
    	/*Putting Service Request count in Json Object*/
    	if(srHashMap.containsKey(GenericConstants.SERVICE_REQUEST))
    	{
    		dtJsonValue.put(GenericConstants.SERVICE_REQUEST, srHashMap.get(GenericConstants.SERVICE_REQUEST));
    	}
    	else
    	{
    		dtJsonValue.put(GenericConstants.SERVICE_REQUEST, 0);
    	}
    	
    	if(crHashMap.containsKey(GenericConstants.CUSTOMER_INITIATED_PENDING))
    	{
    		dtJsonValue.put(GenericConstants.CUSTOMER_INITIATED_PENDING, crHashMap.get(GenericConstants.CUSTOMER_INITIATED_PENDING));
    	}
    	else
    	{
    		dtJsonValue.put(GenericConstants.CUSTOMER_INITIATED_PENDING, 0);
    	}
    	/*Putting Customer Initiated count in Json Object*/
    	if(crHashMap.containsKey(GenericConstants.CUSTOMER_INITIATED_APPROVED))
    	{
    		dtJsonValue.put(GenericConstants.CUSTOMER_INITIATED_APPROVED, crHashMap.get(GenericConstants.CUSTOMER_INITIATED_APPROVED));
    	}
    	else
    	{
    		dtJsonValue.put(GenericConstants.CUSTOMER_INITIATED_APPROVED, 0);
    	}
    	
    	
    	
    	/*Putting Telkom Telstra Initiated count in Json Object*/
    	if(crHashMap.containsKey(GenericConstants.TTINITIATED_PENDING))
    	{
    		dtJsonValue.put(GenericConstants.TTINITIATED_PENDING, crHashMap.get(GenericConstants.TTINITIATED_PENDING));
    	}
    	else
    	{
    		dtJsonValue.put(GenericConstants.TTINITIATED_PENDING, 0);
    	}
    	
    	if(crHashMap.containsKey(GenericConstants.TTINITIATED_APPROVED))
    	{
    		dtJsonValue.put(GenericConstants.TTINITIATED_APPROVED, crHashMap.get(GenericConstants.TTINITIATED_APPROVED));
    	}
    	else
    	{
    		dtJsonValue.put(GenericConstants.TTINITIATED_APPROVED, 0);
    	}
    	log.info("ServiceRequestController getMetricsJson End");
    	return dtJsonValue;
    }
	
	//Apply the search sort paging filters to get the result set and respond the json results
	@ResourceMapping(value = "getRequestMetrics")
    public void getRequestMetrics(ResourceRequest request, ResourceResponse resourceResponse, Model model)
	{
		log.info("ServiceRequestController getRequestMetrics Start");
    	PrintWriter out = null;
	    	
    	try
    	{  
	    	HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil.getHttpServletResponse(resourceResponse);

		    String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httprequest, httpresponse);
			
			ServiceRequestResults serviceRequestResults = serviceRequestService.getServiceRequestsMetric(customerUniqueID);
			ServiceRequestResults changeRequestResults = serviceRequestService.getChangeRequestsMetric(customerUniqueID); 
			
			com.liferay.portal.kernel.json.JSONObject dtJsonValue = getMetricsJson(serviceRequestResults, changeRequestResults);
			
			log.debug("Json object :: " + dtJsonValue );
			
			out = resourceResponse.getWriter();			
			out.print(dtJsonValue.toString());	
			
			log.info("ServiceRequestController getRequestMetrics End");
    	}
    	catch(Exception e)
    	{
    		log.error("Error in ServiceRequestController :: " + e.getMessage());
    		throw new TTPortalAppException(e, "SY:2222", TTPortalAppErrors.getErrorMessageByCode("SY:2222"));
    	}
    	finally
    	{
    		if(null != out) 
    		{
    			out.flush();
    			out.close();
    		}
    	}		
	}	
	
	@ResourceMapping(value = "prodClassforSelectedProductTypeURL")
    public void prodClassforSelectedProductType(ResourceRequest request, ResourceResponse response) throws Exception
	{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(response);
		try {
		    String selectedProductType =  req.getParameter("selectedProductID");
		    log.debug("prodClassforSelectedProductType in servreq controller :::: "
		    		+ "\n selectedProductType recieved in controller to fetch service::>> "+selectedProductType);
		
		   // String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req,res);
		    
   				Map<String, List<String>> prodClassListtemp = createSRManager.prodClassforSelectedProductType(selectedProductType);
   				log.info("ProdClass Recieved from dao in getServiceForProductTypeURL : " +prodClassListtemp);
   				String responseStr = "";
   				if (prodClassListtemp != null) {
   					for (Map.Entry<String, List<String>> entry : prodClassListtemp.entrySet()) {
   						
   						for(String productclasification:  entry.getValue()){
   							responseStr += (entry.getKey() + "@@@@@"+ productclasification + "#####");
   						}
   						
   						
   					}
   					PrintWriter out = response.getWriter();
   					out.print(responseStr);
   				}
		    	
    }
	catch (Exception e) {
				throw new TTPortalAppException(e, "SS:1111", TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
	}
		
	}
	
}
