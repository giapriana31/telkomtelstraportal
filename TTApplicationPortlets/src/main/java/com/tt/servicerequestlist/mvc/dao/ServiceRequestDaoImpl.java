package com.tt.servicerequestlist.mvc.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ChangeRequestApprovals;
import com.tt.model.servicerequest.ChangeRequestDevices;
import com.tt.model.servicerequest.ChangeRequestServices;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.servicerequestlist.mvc.model.DatePair;
import com.tt.servicerequestlist.mvc.model.DetailsSearchParameters;
import com.tt.servicerequestlist.mvc.model.SearchParameters;
import com.tt.servicerequestlist.mvc.model.ServiceRequestResults;
import com.tt.servicerequestlist.mvc.web.controller.ServiceRequestController;
import com.tt.utils.PropertyReader;
import com.tt.constants.GenericConstants;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;

import com.tt.utils.TTGenericUtils;

import java.util.Properties;

@Repository(value = "serviceRequestDaoImpl")
@Transactional
public class ServiceRequestDaoImpl implements ServiceRequestDao {
	
	public static Properties prop = PropertyReader.getProperties();
	String statusClosed=prop.getProperty("statusClosed");
	String statusCancelled=prop.getProperty("statusCancelled");
	String statusComplete=prop.getProperty("statusComplete");
	String statusCompleted=prop.getProperty("statusCompleted");
	String statusClosedCancelled=prop.getProperty("statusClosedCancelled");
	private final static Logger log = Logger.getLogger(ServiceRequestDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	public static final String AND = "' AND ";
	public static final String EQUAL_SINGLE = " = '";
	public static final String INOPERATOR = " IN ";
	public static final String OPENBRACE = " ( ";
	public static final String CLOSEBRACE = " ) ";
	public static final String SINGLEQUOTE = "'";
	public static final String ANDOPERATOR = " AND ";
	public static final String BETWEEN = " BETWEEN ";
	public static final String TO_DATE = " TO_DATE ";
	public static final String DATEFORMAT = " 'yyyy-MM-dd HH:mm:ss' ";
	public static final String COMMA = " , ";
	public static final String ORDERBY = " ORDER BY ";
	public static final String OFFSET = " OFFSET ";
	public static final String FETCHNEXT = " FETCH NEXT ";
	public static final String ROWSONLY = " ROWS ONLY ";
	public static final String LIKE = " LIKE ";
	public static final String OROPERATOR = " OR ";
	public static final String PERCENTAGE = "%";
	public static final String LIMIT = " LIMIT ";
	public static final String GREATERTHANEQUALTO = " >= '";
	public static final String LESSTHANEQUALTO = " <= '";
	// Field level constants
	public static final String CMDBCLASS = " cmdbclass ";
	public static final String CUSTOMERIMPACTED = " customerimpacted ";
	public static final String OPENDATETIME = " opendatetime ";
	public static final String RESOLUTIONDATETIME = " resolutiondatetime ";
	public static final String CASEMANAGER = " casemanager ";
	public static final String INCIDENTID = " incidentid ";
	public static final String SHORTDESCRIPTION = " summary ";
	public static final String CUSTOMERUNIQUEID = " customeruniqueid  ";
	public static final String CITYPE = " citype  ";
	public static final String USERID = " userid  ";
	public static final String CINAME = " ciname  ";
	public static final String SITEID = " siteid  ";
	private static final String COMPLETED_TAB = "completedTab";
	private static final String ACTIVE_TAB = "activeTab";
	private static final String CUSTOMER_ID = "customerId";
	
	private static final String CHANGE_SOURCE_CUSTOMER = "Customer";
	private static final String CHANGE_SOURCE_TELKOMTELSTRA = "telkomtelstra";
	private static final String BLANK = "";
	private static final String CUSTOMER_APPROVAL_STATUS = "customerapprovalstatus";

	@Override
	public ServiceRequestResults getChangeRequestsMetrics(String customerUniqueID){
			
		log.info("ServiceRequestDaoImpl getChangeRequestsMetrics Start ");
		log.debug("Customer Unique Id :: " + customerUniqueID);
		
		ServiceRequestResults crMetricResults = new ServiceRequestResults();
		Map<String, Integer> crMetric = new HashMap<String, Integer>();
		
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append("select 'TT' as changesource, customerapprovalstatus, count(*) as count from changerequest cr,changerequestcustomer crc where crc.changerequestid = cr.changerequestid and crc.customeruniqueid=:customerId and changesource not in ('Customer') and cr.status not in ('")
						.append(statusClosed).append("','")
						.append(statusComplete).append("','")
						.append(statusCompleted).append("','")
						.append(statusCancelled).append("','")
						.append(statusClosedCancelled).append("') ")
						.append("group by customerapprovalstatus ")
						.append("union ")
						.append("select changesource, customerapprovalstatus, count(*) as count from changerequest cr,changerequestcustomer crc where crc.changerequestid = cr.changerequestid and crc.customeruniqueid=:customerId  and changesource in ('Customer') and cr.status not in ('")
						.append(statusClosed).append("','")
						.append(statusComplete).append("','")
						.append(statusCompleted).append("','")
						.append(statusCancelled).append("','")
						.append(statusClosedCancelled).append("')")
						.append(" group by customerapprovalstatus");
						
		log.debug("SQL Query :: " + sqlQuery.toString());
		
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery.toString());
		query.setString(CUSTOMER_ID, customerUniqueID);
		
		/*executing query and storing result set in list*/
		List<Object> results = query.list();
		
		if(results != null && !results.isEmpty())
		{
			for(Object object : results)
			{
				Object[] obj = (Object[]) object;
				
				String changeSource = (String) obj[0];
				String customerApprovalStatus = (String)obj[1];
				String concatSourceStatus = changeSource + "Initiated" + customerApprovalStatus;
				log.debug("map key :: " + concatSourceStatus);

				int count = Integer.parseInt(String.valueOf(obj[2]));
				crMetric.put(concatSourceStatus, count);
			}
		}
		else
		{
			crMetric.put(GenericConstants.CUSTOMER_INITIATED_PENDING, 0);
			crMetric.put(GenericConstants.CUSTOMER_INITIATED_APPROVED, 0);
			crMetric.put(GenericConstants.TTINITIATED_PENDING, 0);
			crMetric.put(GenericConstants.TTINITIATED_APPROVED, 0);
		}
		
		//Set the srMetric into srMetricResults
		crMetricResults.setChangeResultsMetric((HashMap<String, Integer>)crMetric);
		log.info("ServiceRequestDaoImpl getChangeRequestsMetrics End ");
		return crMetricResults;
	}
	
	@Override
	public ServiceRequestResults getServiceRequestMetrics(String customerUniqueID)
	{
		log.info("ServiceRequestDaoImpl getServiceRequestMetrics Start");
		log.debug("Customer Unique Id :: " + customerUniqueID);

		ServiceRequestResults srMetricResults = new ServiceRequestResults();
		Map<String, Integer> srMetric = new HashMap<String, Integer>();
		
		StringBuilder sqlQuery = new StringBuilder();
		
		sqlQuery.append("select count(distinct sr.requestid) from servicerequest sr join servicerequestitem sri ")
				.append("where sr.requestid = sri.servicerequestid and ")
				.append("sr.customeruniqueid =:customerId and ")
				.append("(sri.itemtype LIKE '%"+GenericConstants.RITM_LIKE_MAC+"%' OR  sri.itemtype LIKE '%"+GenericConstants.RITM_LIKE_DELETE+"%') and ")
				.append("sr.status not in ('")
						.append(statusClosed).append("','")
						.append(statusComplete).append("','")
						.append(statusCompleted).append("','")
						.append(statusCancelled).append("','")
						.append(statusClosedCancelled).append("')");
		SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery(sqlQuery.toString());
		query.setString(CUSTOMER_ID, customerUniqueID);
		    
		log.info("ServiceRequestDaoImpl getServiceRequestMetrics query to diplay metrics count:::: >>> "+query);
		/*returning only count, hence using uniqueResult()*/
		Object object = query.uniqueResult();
		
		if(object != null)
		{
			int count = Integer.parseInt(String.valueOf(object));
			srMetric.put(GenericConstants.SERVICE_REQUEST, count);
		}
		else
		{
			srMetric.put(GenericConstants.SERVICE_REQUEST, 0);
		}
		
		//Set the srMetric into srMetricResults
		srMetricResults.setServiceResultsMetric(srMetric);
		log.info("ServiceRequestDaoImpl getServiceRequestMetrics End");
		return srMetricResults;
	}
	
	@Override
	public ServiceRequestResults getAllServiceRequests(
			SearchParameters searchParameters, String customerId) {
		
		log.info("ServiceRequestDaoImpl, getAllServiceRequests with parameters, Start");
		ServiceRequestResults serviceRequestResults = new ServiceRequestResults();
		StringBuilder searhQuery = new StringBuilder();
		if(COMPLETED_TAB.equalsIgnoreCase(searchParameters.getTabSelected())) {		
			if(searchParameters.getAdvReqType() != null){/* advance search query */
				searhQuery.append(" from servicerequest sr join servicerequestitem item ")
						  .append("where sr.requestid = item.servicerequestid ")
						  .append("and ")
						  .append("(item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_MAC+"%' OR  item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_DELETE+"%') and ")
						  .append("sr.status in ('")
						  .append(statusClosed).append("','")
						  .append(statusComplete).append("','")
						  .append(statusCompleted).append("','")
						  .append(statusCancelled).append("','")
						  .append(statusClosedCancelled).append("')");
				log.info("advance search query completed tab SR :::>>>  "+searhQuery.toString());
			} else {/*normal search and onload query*/
				/*srcrview is combination of change and service requests*/
				searhQuery.append(" from srcrview sr where sr.status in ('")
						  .append(statusClosed).append("','")
						  .append(statusComplete).append("','")
						  .append(statusCompleted).append("','")
						  .append(statusCancelled).append("','")
						  .append(statusClosedCancelled).append("')");
				log.info("normal search and onload query completed tab SR :::>>>  "+searhQuery.toString());		  
			}
			Calendar cal=Calendar.getInstance();
            cal.add( Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK)-1)); 
            java.util.Date currentDate= cal.getTime();
            int period=(-1)*84;
            cal.add(Calendar.DATE, period);
            java.util.Date startDate= cal.getTime();
            cal.setTime(currentDate); 
			cal.add(Calendar.DATE, -1);
			currentDate = cal.getTime();
            
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
            String currentDateString=simpleDateFormat.format(currentDate);
            currentDateString=currentDateString.concat(" 23:59:59");
            String startDateString=simpleDateFormat.format(startDate);
            startDateString=startDateString.concat(" 00:00:00");
            searhQuery.append(" and sr.createddate between '").append(startDateString).append("' and '").append(currentDateString).append("'");
            log.info("normal search and onload query/advance search query complete in  completed tab SR :::>>>  "+searhQuery.toString());
		} else if(ACTIVE_TAB.equalsIgnoreCase(searchParameters.getTabSelected())){
			if(searchParameters.isSearchBySRCustTT()){/*if clicked from metrics Grid*/
				searhQuery.append(" from servicerequest sr join servicerequestitem item ")
				  		  .append("where sr.requestid = item.servicerequestid ")
				          .append("and ")
				          .append("(item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_MAC+"%' OR  item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_DELETE+"%') and ")
				          .append("sr.status not in ('")
						  .append(statusClosed).append("','")
						  .append(statusComplete).append("','")
						  .append(statusCompleted).append("','")
						  .append(statusCancelled).append("','")
						  .append(statusClosedCancelled).append("')");
				log.info("if clicked from metrics Grid in Active tab SR :::>>>  "+searhQuery.toString());
			} else if(searchParameters.getAdvReqType() != null){/*if advance search*/
				searhQuery.append(" from servicerequest sr join servicerequestitem item ")
				          .append("where sr.requestid = item.servicerequestid ")
				          .append("and ")
				          .append("(item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_MAC+"%' OR  item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_DELETE+"%') and ")
				          .append("sr.status not in ('")
						  .append(statusClosed).append("','")
						  .append(statusComplete).append("','")
						  .append(statusCompleted).append("','")
						  .append(statusCancelled).append("','")
						  .append(statusClosedCancelled).append("')");
				log.info("if advance search in Active tab SR :::>>>  "+searhQuery.toString());
			} else if(searchParameters.isFilterSearch() && searchParameters.getAppliedFilters() != null){
				if(searchParameters.getAppliedFilters().contains(GenericConstants.SERVICE_REQUEST)){
					searhQuery.append(" from servicerequest sr join servicerequestitem item ")
			          .append("where sr.requestid = item.servicerequestid ")
			          .append("and ")
			          .append("(item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_MAC+"%' OR  item.itemtype LIKE '%"+GenericConstants.RITM_LIKE_DELETE+"%') and ")
			          .append("sr.status not in ('")
					  .append(statusClosed).append("','")
					  .append(statusComplete).append("','")
					  .append(statusCompleted).append("','")
					  .append(statusCancelled).append("','")
					  .append(statusClosedCancelled).append("')");
					log.info("if(searchParameters.getAppliedFilters().contains(GenericConstants.SERVICE_REQUEST)) in Active tab SR :::>>>  "+searhQuery.toString());
				} else{
					searhQuery.append(" from srcrview sr where sr.status not in ('")
					  .append(statusClosed).append("','")
					  .append(statusComplete).append("','")
					  .append(statusCompleted).append("','")
					  .append(statusCancelled).append("','")
					  .append(statusClosedCancelled).append("')");
					log.info("else(searchParameters.getAppliedFilters().contains(GenericConstants.SERVICE_REQUEST)) in Active tab SR :::>>>  "+searhQuery.toString());
				}
			} else {/*normal search and onload query*/
				searhQuery.append(" from srcrview sr where sr.status not in ('")
						  .append(statusClosed).append("','")
						  .append(statusComplete).append("','")
						  .append(statusCompleted).append("','")
						  .append(statusCancelled).append("','")
						  .append(statusClosedCancelled).append("')");
				log.info("normal search and onload query in Active tab SR :::>>>  "+searhQuery.toString());
			}
		}
		
		if(customerId != null){
			searhQuery.append(" and sr.customeruniqueid =:customerId");
		}
		
		/* search query searchqueryList for normal search*/
		if (searchParameters.getSearchqueryList() != null && !searchParameters.getSearchqueryList().isEmpty()) {

			searhQuery.append(ANDOPERATOR).append(OPENBRACE)
					.append(" sr.requestid ").append(LIKE).append(SINGLEQUOTE)
					.append(PERCENTAGE)
					.append(searchParameters.getSearchqueryList())
					.append(PERCENTAGE).append(SINGLEQUOTE).append(OROPERATOR)
					.append(" sr.subject ").append(LIKE).append(SINGLEQUOTE)
					.append(PERCENTAGE)
					.append(searchParameters.getSearchqueryList())
					.append(PERCENTAGE).append(SINGLEQUOTE).append(OROPERATOR)
					.append(" sr.requestor ").append(LIKE).append(SINGLEQUOTE)
					.append(PERCENTAGE)
					.append(searchParameters.getSearchqueryList())
					.append(PERCENTAGE).append(SINGLEQUOTE).append(CLOSEBRACE);
		}
		
		/*Advance Search code*/	
		if (searchParameters.getRequestor() != null) {
			searhQuery.append(ANDOPERATOR).append("sr.requestor").append(EQUAL_SINGLE).append(searchParameters.getRequestor()).append(SINGLEQUOTE);
		} if (searchParameters.getStatus() != null) {
			searhQuery.append(ANDOPERATOR).append("sr.status").append(EQUAL_SINGLE).append(searchParameters.getStatus()).append(SINGLEQUOTE);
		} if (searchParameters.getFromDate() != null && searchParameters.getToDate() != null){
			searhQuery.append(ANDOPERATOR).append("sr.createddate").append(BETWEEN).append(SINGLEQUOTE)
			.append(searchParameters.getFromDate()).append(AND).append(SINGLEQUOTE)
			.append(searchParameters.getToDate()).append(SINGLEQUOTE);
		} if (searhQuery.lastIndexOf(ANDOPERATOR) == searhQuery.length() - 5) {
			searhQuery.delete(searhQuery.length() - 5, searhQuery.length());
		}

		log.info("Printing searhQuery for onload getAllRequests/ onclick of AdvancedSearch\n " + searhQuery);
		String selectCountQuery = "Select count(distinct sr.requestid)"  + searhQuery.toString();
		log.info("selectCountQuery :: " + selectCountQuery);
		
		SQLQuery queryCount = sessionFactory.getCurrentSession().createSQLQuery(selectCountQuery);
		if (customerId != null) {
			queryCount.setString(CUSTOMER_ID, customerId);
		}
		
		/* getting count from the query - used for pagination */
		Number recordCounts = (Number) queryCount.uniqueResult();
		
		if (recordCounts != null && recordCounts.intValue() > 0) {

			serviceRequestResults.setRecordCount(recordCounts.intValue());
			log.info("recordCounts.intValue() \n " 	+ recordCounts.intValue());
			// sortCriterion
			if (searchParameters.getSortCriterion() != null  && !searchParameters.getSortCriterion().isEmpty())
				searhQuery.append(ORDERBY).append(searchParameters.getSortCriterion());
			// asc or desc
			if (searchParameters.getSortDirection() != null && !searchParameters.getSortDirection().isEmpty())
				searhQuery.append(" ").append(searchParameters.getSortDirection());
			// page no pageNumber
			Integer offsetInt = searchParameters.getPageNumber();
			Integer maxResults = searchParameters.getMaxResults();
			List srcrList = new ArrayList();
			if(searchParameters.isSearchBySRCustTT() || searchParameters.getAdvReqType() != null) {
				String sql = "Select requestid, subject, description, requesttype, lastupdatedate, requestor, status "+searhQuery.toString();
				Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
				query.setFirstResult(offsetInt);
				query.setMaxResults(maxResults);
				if (customerId != null) {
					query.setString(CUSTOMER_ID, customerId);
				}
				log.info("Printing searhQuery with sorting \n "+ searhQuery);
				srcrList = query.list();
				log.info("list SEARCH size :: " + srcrList.size());
			} else{
				String queryNSString = "select  requestid, subject, description, requesttype, lastupdatedate, requestor, status " + searhQuery.toString();
				SQLQuery queryNS = sessionFactory.getCurrentSession().createSQLQuery(queryNSString);
				queryNS.setFirstResult(offsetInt);
				queryNS.setMaxResults(maxResults);
				if (customerId != null) {
					queryNS.setString(CUSTOMER_ID, customerId);
				}
				log.info("Printing searhQuery for normal search \n " + searhQuery.toString());
				srcrList = queryNS.list();
				log.info("list size :: " + srcrList.size());
				
			}

			List<ServiceRequest> serviceRequestList = new ArrayList<ServiceRequest>();
			for(int i=0;i<srcrList.size();i++){
				ServiceRequest sr = new ServiceRequest();
				Object[] obj = (Object[]) srcrList.get(i);
				sr.setRequestid((String) obj[0]);
				sr.setSubject((String) obj[1]);
				sr.setDescription((String)obj[2]);
				sr.setRequesttype((String) obj[3]);

				DateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				Date updateDate=null;
				try {
					if(null != obj[4]){
						String updatedDateString = d.format(obj[4]);
						updateDate = fromUser.parse(updatedDateString);
					}
				} 
				catch (ParseException e){
					log.error("Error in getting date :: " + e.getMessage());
					throw new TTPortalAppException(e);
				}
				
				sr.setLastupdatedate(updateDate);
				sr.setRequestor((String) obj[5]);
				sr.setStatus((String) obj[6]);
				serviceRequestList.add(sr);
			}
			serviceRequestResults.setServiceResults(serviceRequestList);
		}

		log.info("ServiceRequestDaoImpl, getAllServiceRequests with parameters, End");
		return serviceRequestResults;
	}
	
	@Override
	public ServiceRequestResults getAllChangeRequests(SearchParameters searchParameters, String customerId) {
		
		log.info("ServiceRequestDaoImpl, getAllChangeRequests with parameters, Start");

		ServiceRequestResults serviceRequestResults = new ServiceRequestResults();

		String customerApprovalStatus = BLANK; 
		
		StringBuilder searhQuery = new StringBuilder();
		if(COMPLETED_TAB.equalsIgnoreCase(searchParameters.getTabSelected()))
		{
			advSearchQuery(searhQuery, searchParameters, customerId);
		}
		else if(ACTIVE_TAB.equalsIgnoreCase(searchParameters.getTabSelected()))
		{
			if(searchParameters.isSearchBySRCustTT())
			{
				String clickedCell = searchParameters.getClickedTdId();
				
				searhQuery.append("select cr from ChangeRequest cr join cr.changeRequestCustomer cc where cr.status not in ('")
				  .append(statusClosed).append("','")
				  .append(statusComplete).append("','")
				  .append(statusCompleted).append("','")
				  .append(statusCancelled).append("','")
				  .append(statusClosedCancelled).append("')")
				  .append(" and cr.customerapprovalstatus =:customerapprovalstatus ");
				
				log.info("Clicked TdId-------------> " + clickedCell);
				if(clickedCell.contains(GenericConstants.CUSTOMER_INITIATED))
				{
					log.info("Inside CustomerInitiated---------- >> "+ clickedCell);
					
					searhQuery.append("and cr.changesource='Customer'");  
					if(clickedCell.contains(GenericConstants.PENDING))
					{
						log.info("Clicked cell in CustomerInitiatedSubmitted has value------------>> " +clickedCell);
						customerApprovalStatus = GenericConstants.PENDING;	
					}
					else if(clickedCell.contains(GenericConstants.APPROVED))
					{
						log.info("Clicked cell in CustomerInitiatedApproved has value------------>> " +clickedCell);
						customerApprovalStatus = GenericConstants.APPROVED;	
					}
				}
				else if(clickedCell.contains(GenericConstants.TTINITIATED))
				{
					log.info("Inside TTInitiated---------- >> "+ clickedCell);

					searhQuery.append("and cr.changesource not in ('Customer')");  
					
					if(clickedCell.contains(GenericConstants.PENDING))
					{
						log.info("Inside TTInitiated---------- >> "+ clickedCell);
						customerApprovalStatus = GenericConstants.PENDING;	
					}
					else if(clickedCell.contains(GenericConstants.APPROVED))
					{
						log.info("Clicked cell in TTInitiatedApproved has value------------>> " +clickedCell);
						customerApprovalStatus = GenericConstants.APPROVED;
					}
				}
				
				if(customerId != null)
				{
					searhQuery.append("and cc.customeruniqueid =:customerId");
				}
				
				log.info("Query generated to filter results on click of grid------------>> "+searhQuery);
			}
			else if(searchParameters.isFilterSearch())
			{
				searhQuery.append("select cr from ChangeRequest cr join cr.changeRequestCustomer cc where cr.status not in ('")
				  .append(statusClosed).append("','")
				  .append(statusComplete).append("','")
				  .append(statusCompleted).append("','")
				  .append(statusCancelled).append("','")
				  .append(statusClosedCancelled).append("')");
				
				String filterString = searchParameters.getAppliedFilters();
				
				if(filterString.contains("Customer"))
				{
					searhQuery.append(" and cr.changesource in ('Customer')");
				}
				else if(filterString.contains("TT"))
				{
					searhQuery.append(" and cr.changesource not in ('Customer')");
				}
				
				boolean hasBoth = checkBothStatus(filterString);
				if(hasBoth)
				{
					searhQuery.append(" and cr.customerapprovalstatus in ('Approved','Pending')");
				}
				else
				{
					if(filterString.contains(GenericConstants.APPROVED))
					{
						searhQuery.append(" and cr.customerapprovalstatus='Approved'");
					}
					else if(filterString.contains("Submitted"))
					{
						searhQuery.append(" and cr.customerapprovalstatus='Pending'");
					}
				}
				if(customerId != null)
				{
					searhQuery.append(" and cc.customeruniqueid =:customerId");
				}
			}
			else
			{
				advSearchQuery(searhQuery, searchParameters, customerId);
			}
			
		}

		/* search query searchqueryList for normal search*/
		if (searchParameters.getSearchqueryList() != null && !searchParameters.getSearchqueryList().isEmpty()) {

			  searhQuery.append(ANDOPERATOR).append(OPENBRACE)
						.append(" requestid ").append(LIKE).append(SINGLEQUOTE)
						.append(PERCENTAGE)
						.append(searchParameters.getSearchqueryList())
						.append(PERCENTAGE).append(SINGLEQUOTE).append(OROPERATOR)
						.append(" subject ").append(LIKE).append(SINGLEQUOTE)
						.append(PERCENTAGE)
						.append(searchParameters.getSearchqueryList())
						.append(PERCENTAGE).append(SINGLEQUOTE).append(OROPERATOR)
						.append(" requestor ").append(LIKE).append(SINGLEQUOTE)
						.append(PERCENTAGE)
						.append(searchParameters.getSearchqueryList())
						.append(PERCENTAGE).append(SINGLEQUOTE).append(CLOSEBRACE);
		}

		if (searhQuery.lastIndexOf(ANDOPERATOR) == searhQuery.length() - 5) {
			searhQuery.delete(searhQuery.length() - 5, searhQuery.length());
		}

		log.info("Printing searhQuery of CR\n " + searhQuery);
		String selectCount = searhQuery.substring(9,searhQuery.length());
		Query queryCount = sessionFactory.getCurrentSession().createQuery("Select count(*) " + selectCount.toString());
		if (customerId != null) 
		{
			queryCount.setString(CUSTOMER_ID, customerId);
		}
		
		if(searchParameters.isSearchBySRCustTT())
		{
			queryCount.setString(CUSTOMER_APPROVAL_STATUS, customerApprovalStatus);
		}
		
		/* getting count from the query - used for pagination */
		Number recordCounts = (Number)queryCount.uniqueResult();

		if (recordCounts != null && recordCounts.intValue() > 0) {

			serviceRequestResults.setRecordCount(recordCounts.intValue());
			log.info("recordCounts.intValue() of CR \n " + recordCounts.intValue());

			// sortCriterion
			if (searchParameters.getSortCriterion() != null	&& !searchParameters.getSortCriterion().isEmpty())
			{
				if(ServiceRequestController.REQUESTID.equals(searchParameters.getSortCriterion()))
				{
					searhQuery.append(ORDERBY).append("cr.changerequestid");
				}
				else if(ServiceRequestController.REQUESTTYPE.equals(searchParameters.getSortCriterion()))
				{
					searhQuery.append("");
				}
				else
				{
					searhQuery.append(ORDERBY).append(searchParameters.getSortCriterion());
				}
			}

			// asc or desc
			if(!ServiceRequestController.REQUESTTYPE.equals(searchParameters.getSortCriterion()))
			{
				if (searchParameters.getSortDirection() != null	&& !searchParameters.getSortDirection().isEmpty())
					searhQuery.append(" ").append(searchParameters.getSortDirection());
			}
			
			// page no pageNumber
			Integer offsetInt = searchParameters.getPageNumber();
			Integer maxResults = searchParameters.getMaxResults();

			Query query = sessionFactory.getCurrentSession().createQuery(searhQuery.toString());
			query.setFirstResult(offsetInt);
			query.setMaxResults(maxResults);
			
			if (customerId != null) {
				query.setString(CUSTOMER_ID, customerId);
			}
			
			if(searchParameters.isSearchBySRCustTT())
			{
				query.setString(CUSTOMER_APPROVAL_STATUS, customerApprovalStatus);
			}

			log.info("Printing searhQuery with sorting of CR\n "	+ searhQuery);

			List<ChangeRequest> changeRequestResultset = query.list();
			serviceRequestResults.setChangeResults(changeRequestResultset);
		}
		log.info("ServiceRequestDaoImpl, getAllChangeRequests with parameters, End");
		return serviceRequestResults;
	}

	@Override
	public ServiceRequest getServiceRequestDetailsForRequestId(String requestId, DetailsSearchParameters detailsSearchParameters) {

		StringBuilder hql = new StringBuilder();
		hql.append("from ServiceRequest sr");
		hql.append(" where sr.requestid =:requestId");
	

		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("requestId", requestId);

		log.info(query.toString());
		
		ServiceRequest serviceRequest = null;
		if (query.list() != null && !query.list().isEmpty()) {
			serviceRequest = (ServiceRequest) query.uniqueResult();
			List<ServiceRequestItem> serviceRequestItems = filterServiceRequestItemsByIPType(serviceRequest.getServiceRequestItem());
			serviceRequest.setServiceRequestItem(serviceRequestItems);
		}
		
		return serviceRequest;
	}
	
	private List<ServiceRequestItem> filterServiceRequestItemsByIPType(List<ServiceRequestItem> serviceRequestItemsRecieved){

		List<ServiceRequestItem> newSRItemList = new ArrayList<ServiceRequestItem>();
		
		if (serviceRequestItemsRecieved != null && !serviceRequestItemsRecieved.isEmpty()) 
		{
	
			//iterate over serviceRequestItems
			for (ServiceRequestItem element : serviceRequestItemsRecieved)
			{
				String recieveditemtypes = element.getItemtype();
				//String recievedprojecttypes = element.getProjecttype();
				if(recieveditemtypes.toLowerCase().contains(GenericConstants.RITM_LIKE_MAC)|| recieveditemtypes.toLowerCase().contains(GenericConstants.RITM_LIKE_DELETE))
				{
					newSRItemList.add(element);
				}
			}
		}
		
		log.info("New Item List---------" +newSRItemList);
		return newSRItemList;
	}
	
	@Override
	public ChangeRequest getChangeRequestDetailsForRequestId(String requestId, DetailsSearchParameters detailsSearchParameters, String customerId) {

		String REQUEST_ID = "requestId";
		
		log.info("----------CR Details DaoImpl starts-----------");
		
		//CR Details main query

		StringBuilder hql = new StringBuilder();
		hql.append("select cr from ChangeRequest cr ");
		hql.append(" where cr.changerequestid =:requestId");
		log.info("query for crdetails------->>>>>>>>>"+hql.toString());
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("requestId", requestId);
		
		ChangeRequest changeRequest = null;
		if (query.list() != null && !query.list().isEmpty()) {
			 changeRequest = (ChangeRequest) query.uniqueResult();
		} 
		/*logic for changesource - Customer/telkomtelstra */
		String changeSource = changeRequest.getChangesource();
		if(!CHANGE_SOURCE_CUSTOMER.equalsIgnoreCase(changeSource))
		{
			changeRequest.setChangesource(CHANGE_SOURCE_TELKOMTELSTRA);
		}
		
		//CR Devices query
		StringBuilder crdquery = new StringBuilder();
		crdquery.append("select ci.ciname from configurationitem ci ")
		        .append(" where ci.ciname in ")
		        .append(" (select crd.ciname from changerequestdevices crd ")
		        .append(" where crd.changerequestid in ")
		        .append(" (select cr.changerequestid from changerequest cr , changerequestcustomer crc ")
		        .append(" where cr.changerequestid = crc.changerequestid ")
		        .append(" and cr.changerequestid =:requestId");
		        if (customerId != null) {
		        	crdquery.append(" and crc.customeruniqueid =:customerId");
		        }
		        crdquery.append(")) and ci.citype='Resource'");
		        if (customerId != null) {
		        	crdquery.append(" and ci.customeruniqueid =:customerId");
		        }

		log.info("Query for crdevices------->>>>>>>>>" + crdquery.toString());
	    Query crdresultquery = sessionFactory.getCurrentSession().createSQLQuery(crdquery.toString());
	    crdresultquery.setString(REQUEST_ID, requestId);
		crdresultquery.setString(CUSTOMER_ID, customerId);
		
		List<ChangeRequestDevices> lcrd=new ArrayList<ChangeRequestDevices>();
		List crdlist = crdresultquery.list();
		log.info("list obtained from query :: " + crdlist);
		
		for(int i=0 ; i<crdlist.size() ; i++) {
			
			ChangeRequestDevices changeRequestDevices = new ChangeRequestDevices() ;
			log.info("setting device name :: " + (String)crdlist.get(i));
			changeRequestDevices.setCiname((String)crdlist.get(i));
			lcrd.add(changeRequestDevices);
		}
		log.info("lcrd list size :: " + lcrd.size());
		changeRequest.setChangeRequestDevices(lcrd);
		
		
		//CR services query------ 
		StringBuilder crsquery = new StringBuilder();
		
		crsquery.append("select ci.ciname from configurationitem ci ")
		        .append(" where ci.ciname in ")
		        .append(" (select crs.servicename from changerequestservices crs ")
		        .append(" where crs.changerequestid in ")
		        .append(" (select cr.changerequestid from changerequest cr , changerequestcustomer crc ")
		        .append(" where cr.changerequestid = crc.changerequestid ")
		        .append(" and cr.changerequestid =:requestId");
		        if (customerId != null) {
		        	crsquery.append(" and crc.customeruniqueid =:customerId");
		        }
		        crsquery.append(")) and ci.citype='Service'");
		        if (customerId != null) {
		        	crsquery.append(" and ci.customeruniqueid =:customerId");
		        }

		log.info("Query for crservices------->>>>>>>>>" + crsquery.toString());
	    Query crsresultquery = sessionFactory.getCurrentSession().createSQLQuery(crsquery.toString());
	    crsresultquery.setString(REQUEST_ID, requestId);
		crsresultquery.setString(CUSTOMER_ID, customerId);
		
		List<ChangeRequestServices> lcrs= new ArrayList<ChangeRequestServices>();
		List crslist = crsresultquery.list();
		log.info("list obtained from query :: " + crslist);
		
		for(int i=0 ; i<crslist.size() ; i++) {
			
			ChangeRequestServices changeRequestServices = new ChangeRequestServices() ;
			log.info("setting Service name :: " + (String)crslist.get(i));
			changeRequestServices.setServicename((String)crslist.get(i));
			lcrs.add(changeRequestServices);
		}
		log.info("lcrs list size :: " + lcrs.size());
		changeRequest.setChangeRequestServices(lcrs);
		
		//Query for TT Approvals
		StringBuilder craQuery = new StringBuilder();
		
		craQuery.append("select approver, approvaldate, approvalgroup from changerequestapprovals cra where cra.changerequestid in"); 
		craQuery.append("(select cr.changerequestid from changerequest cr join changerequestcustomer crc where cr.changerequestid = crc.changerequestid "); 
		craQuery.append("and crc.customeruniqueid=:customerId) and cra.changerequestid=:requestId order by approvaldate asc");
		
		log.debug("Query for approval table :: " + craQuery.toString());
		
		Query craresultQuery = sessionFactory.getCurrentSession().createSQLQuery(craQuery.toString());
		craresultQuery.setString(CUSTOMER_ID, customerId);
		craresultQuery.setString(REQUEST_ID, requestId);
		
		List<ChangeRequestApprovals> lcra = new ArrayList<ChangeRequestApprovals>();
		
		List craList = craresultQuery.list();
		log.info("list obtained from query :: " + craList);
		
		for(int i=0;i<craList.size(); i++)
		{
			ChangeRequestApprovals changeRequestApprovals = new ChangeRequestApprovals();
			log.info("setting all the parameters in the object : " + craList.get(i));
			
			/*ChangeRequestApprovals craObject = (ChangeRequestApprovals) craList.get(i);
			changeRequestApprovals.setApprover(craObject.getApprover());
			changeRequestApprovals.setApprovaldate(craObject.getApprovaldate());
			changeRequestApprovals.setApprovalgroup(craObject.getApprovalgroup());*/
			
			Object[] object = (Object[]) craList.get(i);
			log.info("object 0 :: " + object[0].toString());
			log.info("object 1 :: " + object[1].toString());
			log.info("object 2 :: " + object[2].toString());
			
			changeRequestApprovals.setApprover((String)object[0]);
			changeRequestApprovals.setApprovaldate((Date)object[1]);
			changeRequestApprovals.setApprovalgroup((String)object[2]);
			
			lcra.add(changeRequestApprovals);
		}
		log.info("list of TT approvals  :: " + lcra.size());
		changeRequest.setChangeRequestApprovals(lcra);
		
		return changeRequest;	
	}
	
	@Override
	public List<Integer> getServiceRequestTrends(List<DatePair> cycleDateRange,
			String customeruniqueid) {

		log.info("---------getSRTrend DAOImpl start----------------");
		List<Integer> srTrends = null;
		int numberOfWeeks=12;
		try {
			srTrends = new ArrayList<Integer>();
			StringBuilder hsql = new StringBuilder();
			hsql.append("select ");
			// -- Loop to get count of completed incidents for different
			// weeks---------

			for (int i = 0; i < numberOfWeeks; i++) {
				
				hsql.append("(select count(distinct sr.requestid) from servicerequest sr join servicerequestitem sri ");
				hsql.append("where sr.requestid = sri.servicerequestid and ")
				    .append("(sr.createddate between '");
				hsql.append(cycleDateRange.get(i).getStartOfWeek())
					.append("' and '")
					.append(cycleDateRange.get(i).getEndOfWeek())
					.append("')");

				hsql.append(" and (sri.itemtype LIKE '%MAC%' OR  sri.itemtype LIKE '%DELETE%') and ")
				    .append("sr.customeruniqueid =:customerId and sr.status in ('")
					.append(statusClosed).append("','")
					.append(statusComplete).append("','")
					.append(statusCompleted).append("','")
					.append(statusCancelled).append("','")
					.append(statusClosedCancelled).append("'))");

				if (i < (numberOfWeeks - 1)) {
					hsql.append(",");
				}

			}

			hsql.append(" from dual");

			log.info("---------Search query of SRtrends--------->>>>>>>>>"+ hsql);

			Query query = sessionFactory.getCurrentSession().createSQLQuery(hsql.toString());
			query.setString(CUSTOMER_ID, customeruniqueid);
			Object[] result = (Object[]) query.uniqueResult();

			for (int i = 0; i < result.length; i++) {
				Integer count = ((BigInteger) result[i]).intValue();
				srTrends.add(count);
				log.info("count of SrTrends........"+count);
			}
		} 
		catch (Exception e) {
			log.error("Error in getServiceRequestTrends " + e.getMessage());
		}
		return srTrends;
	}
	
	@Override
	public List<Integer> getChangeRequestTrends(List<DatePair> cycleDateRange,
			String customeruniqueid, String source) {

		log.info("------------getCRTrend DAOImpl start---------------");
		List<Integer> crTrends = null;

		int numberOfWeeks = 12;
		try {
			crTrends = new ArrayList<Integer>();
			StringBuilder hsql = new StringBuilder();
			hsql.append("select ");

			// Loop to generate query for getting count of 12 weeks
			for (int i = 0; i < numberOfWeeks; i++) {

				hsql.append("(select count(*) from ChangeRequest cr , changerequestcustomer cc ");

				hsql.append(" where (cr.createddate between '");
				hsql.append(cycleDateRange.get(i).getStartOfWeek())
						.append("' and '")
						.append(cycleDateRange.get(i).getEndOfWeek())
						.append("')");

				if ("crCustInitiated".equalsIgnoreCase(source)) {
					hsql.append(" and cr.changesource in ('Customer') ");
				}

				else if ("crTtInitiated".equalsIgnoreCase(source)) {
					hsql.append(" and cr.changesource in ('Internal','External') ");
				}

				hsql.append(" and cr.status in ('")
						.append(statusClosed)
						.append("','")
						.append(statusComplete)
						.append("','")
						.append(statusCompleted)
						.append("','")
						.append(statusCancelled)
						.append("','")
						.append(statusClosedCancelled)
						.append("')")
						.append(" and cr.changerequestid = cc.changerequestid and cc.customeruniqueid=:customerId ")
						.append(")");

				if (i < (numberOfWeeks - 1)) {
					hsql.append(",");
				}
			}

			hsql.append(" from dual");

			log.info("---------Search query of CRtrends--------->>>>>>>>>"
					+ hsql);

			Query query = sessionFactory.getCurrentSession().createSQLQuery(
					hsql.toString());
			query.setString(CUSTOMER_ID, customeruniqueid);
			Object[] resultcr = (Object[]) query.uniqueResult();

			for (int i = 0; i < resultcr.length; i++) {
				Integer count = ((BigInteger) resultcr[i]).intValue();
				crTrends.add(count);
				log.info("count of CrTrends........" + count);
			}
		} catch (Exception e) {
			log.error("Error in getChangeRequestTrends :: " + e.getMessage());
		}
		return crTrends;
	}
	
	@Override
	public Date getLastRefreshDate(String customerId, String entityType) {

		StringBuilder sql = new StringBuilder();
		sql.append("select max(lastrefreshtime) from lastrefreshtime sa");
		sql.append(" where sa.customeruniqueid=:customerId and entityname=:entityType");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
		
		query.setString(CUSTOMER_ID, customerId);
		query.setString("entityType", entityType);
		if (query.list() != null && !query.list().isEmpty()) {
			Date refreshDate = (Date) query.list().get(0);
			return refreshDate;
		} 
		else {
			return null;
		}
	}

	@Override
	public ServiceRequestItem getServiceRequestItemDetailsForItemId(
			String itemId) {

		StringBuilder hql = new StringBuilder();
		hql.append("from ServiceRequestItem sr");

		hql.append(" where sr.itemid =:itemId");

		Query query = sessionFactory.getCurrentSession().createQuery(
				hql.toString());
		query.setString("itemId", itemId);
		log.info(query.toString());
		
		ServiceRequestItem serviceRequestItem = null;
		if (query.list() != null && !query.list().isEmpty()) {
			serviceRequestItem = (ServiceRequestItem) query
					.uniqueResult();
		}
		return serviceRequestItem;
	}
	
	private void advSearchQuery(StringBuilder searhQuery, SearchParameters searchParameters, String customerId)
	{
		searhQuery.append("select cr from ChangeRequest cr join cr.changeRequestCustomer crc");
		if (searchParameters.getServiceName() != null) {
			searhQuery.append(" join cr.changeRequestServices crs"); 
		}
		if (customerId != null) {
			searhQuery.append(" where crc.customeruniqueid =:customerId");
		}
		if (searchParameters.getPriority() != null){
			searhQuery.append(ANDOPERATOR).append("cr.priority").append(INOPERATOR).append(OPENBRACE);
			for(String str: searchParameters.getPriority()){
				searhQuery.append(SINGLEQUOTE).append(str).append(SINGLEQUOTE).append(COMMA);
			}
			if (searhQuery.lastIndexOf(COMMA) == searhQuery.length() - 3) {
				searhQuery.delete(searhQuery.length() - 3, searhQuery.length());
			}
			searhQuery.append(CLOSEBRACE);
		}
		if (searchParameters.getSiteName() != null) {
			StringBuilder querySiteName = new StringBuilder();
			querySiteName.append("select customersite from Site where siteid=:siteID and customeruniqueid=:custId and status in('")
						 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
						 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query = sessionFactory.getCurrentSession().createQuery(querySiteName.toString());
			query.setString("siteID", searchParameters.getSiteName());
			query.setString("custId", customerId);
			
			String siteName;
			if(query.uniqueResult()!=null){
				siteName = (String) query.uniqueResult();
				searhQuery.append(ANDOPERATOR).append("cr.sitename").append(EQUAL_SINGLE).append(siteName).append(SINGLEQUOTE);
			}
		}
		if (searchParameters.getServiceName() != null) {
			searhQuery.append(ANDOPERATOR).append("crs.servicename").append(EQUAL_SINGLE).append(searchParameters.getServiceName()).append(SINGLEQUOTE);
		}
		if (searchParameters.getAsignedTo() != null) {
			searhQuery.append(ANDOPERATOR).append("cr.casemanager").append(EQUAL_SINGLE).append(searchParameters.getAsignedTo()).append(SINGLEQUOTE);
		}
		if (searchParameters.getChangeType() != null) {
			searhQuery.append(ANDOPERATOR).append("cr.changetype").append(EQUAL_SINGLE).append(searchParameters.getChangeType()).append(SINGLEQUOTE);
		}
		if (searchParameters.getChangeSource() != null) {
			searhQuery.append(ANDOPERATOR).append("cr.changesource").append(EQUAL_SINGLE).append(searchParameters.getChangeSource()).append(SINGLEQUOTE);
		}
		if (searchParameters.getFromDate() != null && searchParameters.getToDate() != null){
			searhQuery.append(ANDOPERATOR).append("cr.createddate").append(BETWEEN).append(SINGLEQUOTE)
			.append(searchParameters.getFromDate()).append(AND).append(SINGLEQUOTE)
			.append(searchParameters.getToDate()).append(SINGLEQUOTE); 
		}
		if(COMPLETED_TAB.equalsIgnoreCase(searchParameters.getTabSelected())){
			searhQuery.append(" and cr.status in ('")
					  .append(statusClosed).append("','")
					  .append(statusComplete).append("','")
					  .append(statusCompleted).append("','")
					  .append(statusCancelled).append("','")
					  .append(statusClosedCancelled).append("')");
			Calendar cal=Calendar.getInstance();
            cal.add( Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK)-1)); 
            java.util.Date currentDate= cal.getTime();
            int period=(-1)*84;
            cal.add(Calendar.DATE, period);
            java.util.Date startDate= cal.getTime();
            
            cal.setTime(currentDate); 
			cal.add(Calendar.DATE, -1);
			currentDate = cal.getTime();
            
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
            String currentDateString=simpleDateFormat.format(currentDate);
            currentDateString=currentDateString.concat(" 23:59:59");
            String startDateString=simpleDateFormat.format(startDate);
            startDateString=startDateString.concat(" 00:00:00");
            searhQuery.append(" and cr.createddate between '").append(startDateString).append("' and '").append(currentDateString).append("'");
			
		}
		else if(ACTIVE_TAB.equalsIgnoreCase(searchParameters.getTabSelected())){
			searhQuery.append(" and cr.status not in ('")
					  .append(statusClosed).append("','")
					  .append(statusComplete).append("','")
					  .append(statusCompleted).append("','")
					  .append(statusCancelled).append("','")
					  .append(statusClosedCancelled).append("')");
		}
	}
	

	private boolean checkBothStatus(String filterString) 
	{
		if(filterString.contains("Approved") && filterString.contains("Submitted"))
		{
			return true;
		}
		return false;
	}

}
