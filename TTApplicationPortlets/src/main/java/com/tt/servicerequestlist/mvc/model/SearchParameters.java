package com.tt.servicerequestlist.mvc.model;

public class SearchParameters {

	private String sortCriterion;
	private String sortDirection;
	private int pageNumber = 1; // default value
	private String searchqueryList;
	private int maxResults = 0;

	// Set it from session
	private String customeruniqueid;
	private String citype = "Service";
	private String tabSelected;

	// by default it will be service request
	private String requesttype = "SR";
	private String advReqType;
	private String srUserData;
	private String[] priority;
	private String siteName;
	private String serviceName;
	private String fromDate;
	private String toDate;
	private String asignedTo;
	private String changeType;
	private String changeSource;
	private String requestor;
	private String status;
	private boolean isNormalSearch;
	private boolean searchBySRCustTT;
	private String clickedTdId;
	private String appliedFilters;
	private boolean isFilterSearch;
	
	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public String getChangeSource() {
		return changeSource;
	}

	public void setChangeSource(String changeSource) {
		this.changeSource = changeSource;
	}

	public String getAdvReqType() {
		return advReqType;
	}

	public void setAdvReqType(String advReqType) {
		this.advReqType = advReqType;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String[] getPriority() {
		return priority;
	}

	public void setPriority(String[] priority) {
		this.priority = priority;
	}

	public String getSrUserData() {
		return srUserData;
	}

	public void setSrUserData(String srUserData) {
		this.srUserData = srUserData;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getAsignedTo() {
		return asignedTo;
	}

	public void setAsignedTo(String asignedTo) {
		this.asignedTo = asignedTo;
	}

	public String getRequesttype() {
		return requesttype;
	}

	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}

	public String getTabSelected() {
		return tabSelected;
	}

	public void setTabSelected(String tabSelected) {
		this.tabSelected = tabSelected;
	}

	public String getSortCriterion() {
		return sortCriterion;
	}

	public void setSortCriterion(String sortCriterion) {
		this.sortCriterion = sortCriterion;
	}

	public String getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getSearchqueryList() {
		return searchqueryList;
	}

	public void setSearchqueryList(String searchqueryList) {
		this.searchqueryList = searchqueryList;
	}

	public String getCustomeruniqueid() {
		return customeruniqueid;
	}

	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}

	public String getCitype() {
		return citype;
	}

	public void setCitype(String citype) {
		this.citype = citype;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public boolean isSearchBySRCustTT() {
		return searchBySRCustTT;
	}

	public void setSearchBySRCustTT(boolean searchBySRCustTT) {
		this.searchBySRCustTT = searchBySRCustTT;
	}

	public String getClickedTdId() {
		return clickedTdId;
	}

	public void setClickedTdId(String clickedTdId) {
		this.clickedTdId = clickedTdId;
	}

	public boolean isNormalSearch() {
		return isNormalSearch;
	}

	public void setNormalSearch(boolean isNormalSearch) {
		this.isNormalSearch = isNormalSearch;
	}

	public String getAppliedFilters() {
		return appliedFilters;
	}

	public void setAppliedFilters(String appliedFilters) {
		this.appliedFilters = appliedFilters;
	}

	public boolean isFilterSearch() {
		return isFilterSearch;
	}

	public void setFilterSearch(boolean isFilterSearch) {
		this.isFilterSearch = isFilterSearch;
	}
}
