package com.tt.servicerequestlist.mvc.model;

import java.util.List;
import java.util.Map;

import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ServiceRequest;

public class ServiceRequestResults {
	
	private int recordCount = 0 ; // default value
	private List<ServiceRequest> serviceResults;
	private List<ChangeRequest> changeResults;
	private int draw = 0 ;
	private int recordsFiltered = 0;
	private Map<String,Integer> serviceResultsMetric;
	private Map<String,Integer> changeResultsMetric;
		
	public Map<String, Integer> getServiceResultsMetric() {
		return serviceResultsMetric;
	}

	public void setServiceResultsMetric(Map<String, Integer> serviceResultsMetric) {
		this.serviceResultsMetric = serviceResultsMetric;
	}

	public Map<String, Integer> getChangeResultsMetric() {
		return changeResultsMetric;
	}

	public void setChangeResultsMetric(Map<String, Integer> changeResultsMetric) {
		this.changeResultsMetric = changeResultsMetric;
	}
	
	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	
	public int getRecordCount() {
		return recordCount;
	}
	
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	
	public List<ServiceRequest> getServiceResults() {
		return serviceResults;
	}
	
	public void setServiceResults(List<ServiceRequest> serviceResults) {
		this.serviceResults = serviceResults;
	}

	public List<ChangeRequest> getChangeResults() {
		return changeResults;
	}

	public void setChangeResults(List<ChangeRequest> changeResults) {
		this.changeResults = changeResults;
	}
	
	

}
