package com.tt.servicerequestlist.mvc.service.common;



import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.servicerequestlist.mvc.dao.CreateSRDaoImpl;


@Service(value="createSRManager")
@Transactional
public class CreateSRManager {

	private CreateSRDaoImpl createSRDaoImpl;
	
	
	/*@Autowired
	@Qualifier("createSRDaoImpl")
	public void setCommonDaoImpl(CreateSRDaoImpl createSRDaoImpl) {
		this.createSRDaoImpl = createSRDaoImpl;
	}*/
	
	@Autowired
	@Qualifier("createSRDaoImpl")
	public void setCreateSRDaoImpl(CreateSRDaoImpl createSRDaoImpl) {
		this.createSRDaoImpl = createSRDaoImpl;
	}

	public LinkedHashMap<String, String> getRegionNameMap(String customeruniqueID) throws Exception{
		try{
			return createSRDaoImpl.getRegionNameMap(customeruniqueID);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public Map<String,String> getServiceFromProductTypeAndSiteID(String productType , String siteID, String customerId) throws Exception{
		try{
			return createSRDaoImpl.getServiceFromProductTypeAndSiteID(productType, siteID, customerId);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSiteFromRegion(String siteID, String customerUniqueID) throws Exception{
		try{
			return createSRDaoImpl.getSiteFromRegion(siteID,customerUniqueID);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSRAction(String category) throws Exception{
		try{
			return createSRDaoImpl.getSRAction(category);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSRDevices(String productType, String serviceName, String selectedSubCategory, String customerId) throws Exception{
		try{
			return createSRDaoImpl.getSRDevices(productType, serviceName, selectedSubCategory, customerId);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSubCategory(String productType, String category,String serviceName) throws Exception{
		try{
			return createSRDaoImpl.getSubCategory(productType, category, serviceName);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public String fetchCategoryForMACD(String serviceName) throws Exception{
		try{
			return createSRDaoImpl.fetchCategoryForMACD(serviceName);
		}
		catch(Exception e){
			
			throw e;
		}
	}	

	public LinkedHashMap<String, String> getSiteNameMap(String customerId) {
		try{
			return createSRDaoImpl.getSiteNameMap(customerId);
		}
		catch(Exception e){
			throw e;
		}
	}	
	
	public List<String> getProductTypeList() throws Exception{
		try{
			
			return createSRDaoImpl.getProductTypeList();
		}
		catch(Exception e){
			throw e;
		}
	}
	public LinkedHashMap<String,List<String>> prodClassforSelectedProductType(String selectedProductType) throws Exception{
		try{
			
			return createSRDaoImpl.prodClassforSelectedProductType(selectedProductType);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	
	public String getServiceNameforLicenses(String customerId)
	{
		
		return createSRDaoImpl.getServiceNameforLicenses(customerId);
	}	
}
