package com.tt.servicerequestlist.mvc.service.common;

import java.util.List;

import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.servicerequestlist.mvc.model.DatePair;
import com.tt.servicerequestlist.mvc.model.DetailsSearchParameters;
import com.tt.servicerequestlist.mvc.model.SearchParameters;
import com.tt.servicerequestlist.mvc.model.ServiceRequestResults;


public interface ServiceRequestService {

	public ServiceRequestResults getAllServiceRequests(SearchParameters searchParameters, String customerId);
	public ServiceRequestResults getAllChangeRequests(SearchParameters searchParameters, String customerId);
	public ServiceRequest getServiceRequestDetailsForRequestId(String requestId, DetailsSearchParameters detailsSearchParameters);
	public ChangeRequest getChangeRequestDetailsForRequestId(String crRequestId, DetailsSearchParameters detailsSearchParameters, String customerId);
	public ServiceRequestItem getServiceRequestItemDetailsForItemId(String itemId);
	public void dataRefresh(String customerId);
	public String getLastRefreshDate(String customerId);
	public List<Integer> getServiceRequestTrends(List<DatePair> cycleDateRange, String customeruniqueid);
	public List<Integer> getChangeRequestTrends(List<DatePair> cycleDateRange, String customeruniqueid, String source);
	public ServiceRequestResults getChangeRequestsMetric(String customerUniqueID);
	public ServiceRequestResults getServiceRequestsMetric(String customerUniqueID);

}
