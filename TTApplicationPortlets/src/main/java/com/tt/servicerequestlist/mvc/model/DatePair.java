package com.tt.servicerequestlist.mvc.model;

public class DatePair {
	private String startOfWeek;
	private String endOfWeek;
	public String getStartOfWeek() {
		return startOfWeek;
	}
	public void setStartOfWeek(String startOfWeek) {
		this.startOfWeek = startOfWeek;
	}
	public String getEndOfWeek() {
		return endOfWeek;
	}
	public void setEndOfWeek(String endOfWeek) {
		this.endOfWeek = endOfWeek;
	}
}
