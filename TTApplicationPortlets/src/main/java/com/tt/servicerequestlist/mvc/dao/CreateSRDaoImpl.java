package com.tt.servicerequestlist.mvc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;
@Repository(value="createSRDaoImpl")
@Transactional

public class CreateSRDaoImpl {
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(CreateSRDaoImpl.class);
	
	
	@Autowired
	private SessionFactory sessionFactory;
	private Session session=null;
	private Transaction tx=null;

		
	public LinkedHashMap<String,String> getRegionNameMap(String customeruniqueID) throws Exception{
		try{
	
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> regionNameMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select distinct s.stateprovince from Site s where s.customeruniqueid=:custId and rootProductId = 'MNS' and s.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE)
				 .append(" order by s.stateprovince asc");
			Query query1 = session.createQuery(query.toString()).setString("custId", customeruniqueID);
			log.info("Query Fired for fetching Region for logged in user : " + customeruniqueID);
			if(query1.list()!=null && !query1.list().isEmpty()){
				log.info("Region data found for user.");
				for(Iterator it=query1.iterate();it.hasNext();){
					String row = (String) it.next();
					regionNameMap.put(row, row);
				}
			}else{
				log.debug("No region data found for user with ID : " + customeruniqueID);
			}
			log.info("CLosing the transaction.");
			tx.commit();
			return regionNameMap;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getRegionNameMap method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getSiteFromRegion(String region, String customerUniqueID) throws Exception{
		try{
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select s.customersite, s.siteid from Site s where s.stateprovince=:region and s.customeruniqueid=:custId and lower(s.servicetier) <> lower('Fully Managed') and rootProductId = 'MNS' and s.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by s.customersite asc");
			Query query1 = session.createQuery(query.toString()).setString("region",region).setString("custId",customerUniqueID);
			if(query1.list()!=null && !query1.list().isEmpty()){
				log.info("Site data found for Region " + region);
				for(Iterator it=query1.iterate();it.hasNext();){
				 	Object[] row = (Object[]) it.next();
				 	siteNameIDMap.put(row[1]+"", row[0]+"");
				}
			}
			else{
				log.debug("No Site data found for region : " + region + "\n and customer id - " + customerUniqueID);
			}
			log.info("CLosing the transaction.");
			tx.commit();
			return siteNameIDMap;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getSiteFromRegion method  - " +e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	
	public Map<String,String> getServiceFromProductTypeAndSiteID(String productType, String siteName, String customerId) throws Exception{
		try{
			
			//Method to fetch services based on product type for "Private Cloud" and based on product type and Site ID for "MNS"
			
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			Map<String, String> serviceIdMap = new LinkedHashMap<String, String>();
			
			StringBuilder hql = new StringBuilder();
			
			log.debug("In CreateSRDaoImpl :: getServiceFromProductTypeAndSiteID Method:::"
					+ "\n ProductType recieved for getServiceFromProductTypeAndSiteID in dao::>>"+productType +""
							+ "\n Site ID recieved for getServiceFromProductTypeAndSiteID in dao:::  "+ siteName+"\n and customruniqueId"+customerId);
			
			if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MNS)){
						
				        //Query to fetch services for MNS based on selected Site ID.
				
						hql.append("select c.ciid, c.ciname from Configuration c where c.rootProductId =:producTypeFetched and c.citype=:ciType and c.status in ('");
						hql.append(GenericConstants.STATUSCOMMISSIONING);
						hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
						hql.append(GenericConstants.STATUSOPERATIONAL);
						hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
						
						if(!(siteName.equalsIgnoreCase("allSites"))){
							hql.append(" AND c.siteid in (select siteid from Site where customersite=:siteNameFetched and status in ('");
							hql.append(GenericConstants.STATUSCOMMISSIONING);
							hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
							hql.append(GenericConstants.STATUSOPERATIONAL);
							hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(")");
						}
						hql.append(" order by c.ciname asc");
						
						Query queryMNS = session.createQuery(hql.toString());
						queryMNS.setString("ciType", prop.getProperty("ciType"))
								.setString("producTypeFetched", productType);
						
						if(!(siteName.equalsIgnoreCase("allSites"))){
							queryMNS.setString("siteNameFetched",siteName);
						}
						
						if(queryMNS.list()!=null && !queryMNS.list().isEmpty()){
							
							log.info("Service data found for Site " + siteName);
							
							for(Iterator it=queryMNS.iterate();it.hasNext();){
								Object[] row = (Object[]) it.next();
								serviceIdMap.put(row[0]+"", row[1]+"");
								
								log.info("getService from Site Id Query for MNS"+ queryMNS);
							}
						}
						else{
							log.debug("No Service Data found for Site ID - " + siteName);
						}
					}else if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_WHISPIR) || productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_IPSCAPE)){
						hql.append("select c.ciid, c.ciname from Configuration c where c.customeruniqueid=:custId and c.rootProductId ='SaaS' and c.productId=:producTypeFetched")
						   .append(" and c.citype=:ciType and c.cmdblass=:cmdbClassPCFetchedProp and c.status in ('");
						hql.append(GenericConstants.STATUSCOMMISSIONING);
						hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
						hql.append(GenericConstants.STATUSOPERATIONAL);
						hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by c.ciname asc");
						
						Query queryPrivateCloud = session.createQuery(hql.toString());
						queryPrivateCloud.setString("ciType", prop.getProperty("ciType"))
										 .setString("custId", customerId)
										 .setString("producTypeFetched", productType)
										 .setString("cmdbClassPCFetchedProp", "SaaS Business Service");
						
						if(queryPrivateCloud.list()!=null && !queryPrivateCloud.list().isEmpty()){
							
							log.info("Service data found for product Type " + productType);
							
							for(Iterator it=queryPrivateCloud.iterate();it.hasNext();){
								Object[] row = (Object[]) it.next();
								serviceIdMap.put(row[0]+"", row[1]+"");
								
								log.info("getServiceQuery for SaaS"+ queryPrivateCloud);
							}
						}
						else{
							log.debug("No Service Data found for SaaS - " + productType);
						}
					}else if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MANDOE)){
						hql.append("select c.ciid, c.ciname from Configuration c where c.customeruniqueid=:custId and c.rootProductId ='SaaS' and c.productId like '%Mandoe%'")
						   .append(" and c.citype=:ciType and c.cmdblass=:cmdbClassPCFetchedProp and c.status in ('");
						hql.append(GenericConstants.STATUSCOMMISSIONING);
						hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
						hql.append(GenericConstants.STATUSOPERATIONAL);
						hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by c.ciname asc");
						
						Query queryPrivateCloud = session.createQuery(hql.toString());
						queryPrivateCloud.setString("ciType", prop.getProperty("ciType"))
										 .setString("custId", customerId)
										 .setString("cmdbClassPCFetchedProp", "SaaS Business Service");
						
						if(queryPrivateCloud.list()!=null && !queryPrivateCloud.list().isEmpty()){
							
							log.info("Service data found for product Type " + productType);
							
							for(Iterator it=queryPrivateCloud.iterate();it.hasNext();){
								Object[] row = (Object[]) it.next();
								serviceIdMap.put(row[0]+"", row[1]+"");
								
								log.info("getServiceQuery for SaaS"+ queryPrivateCloud);
							}
						}
						else{
							log.debug("No Service Data found for SaaS - " + productType);
						}
					}
					else{
						
					//When product type = "Private Cloud"
						
						hql.append("select c.ciid, c.ciname from Configuration c where c.customeruniqueid=:custId and c.rootProductId=:producTypeFetched")
						   .append(" and c.citype=:ciType and c.cmdblass=:cmdbClassPCFetchedProp and c.status in ('");
						hql.append(GenericConstants.STATUSCOMMISSIONING);
						hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
						hql.append(GenericConstants.STATUSOPERATIONAL);
						hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by c.ciname asc");
						
						Query queryPrivateCloud = session.createQuery(hql.toString());
						queryPrivateCloud.setString("ciType", prop.getProperty("ciType"))
										 .setString("custId", customerId)
										 .setString("producTypeFetched", productType)
										 .setString("cmdbClassPCFetchedProp", prop.getProperty("cmdbClassPrivateCloudForCI"));
						
						if(queryPrivateCloud.list()!=null && !queryPrivateCloud.list().isEmpty()){
							
							log.info("Service data found for product Type " + productType);
							
							for(Iterator it=queryPrivateCloud.iterate();it.hasNext();){
								Object[] row = (Object[]) it.next();
								serviceIdMap.put(row[0]+"", row[1]+"");
								
								log.info("getServiceQuery for Private Cloud"+ queryPrivateCloud);
							}
						}
						else{
							log.debug("No Service Data found for Private Cloud - " + productType);
						}
						
					}
			log.info("CLosing the transaction.");
			tx.commit();
			log.info("********** Service name " +serviceIdMap + "\n returned from dao for the selected site name " +siteName+ "********* \n and product Type ::  "+productType );
			return serviceIdMap;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getServiceFromProductTypeAndSiteID method  - " +e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getSRAction(String category) throws Exception{
		try{
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> actionList = new LinkedHashMap<String, String>();
			Query query1 = session.createQuery("select c.value from CommonLov c where c.name=:category order by c.value asc").setString("category",category);
			log.info("Query executed for fetching Action at Render Time");
			if(query1.list()!=null && !query1.list().isEmpty()){
				log.info("Action data found for Category " + category);
				for(Iterator it=query1.iterate();it.hasNext();){
					String row = (String) it.next();
					log.info("Action " + row);
					actionList.put(row, row);
				}
			}
			else{
				log.debug("No Action data found for Category - " + category);
			}
			log.info("CLosing the transaction.");
			tx.commit();
			return actionList;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getSRAction method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	
	public LinkedHashMap<String,String> getSRDevices(String productType, String serviceName, String selectedSubCategory, String customerId) throws Exception{
		try{
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> devicesList = new LinkedHashMap<String, String>();
			
			
			log.info("getSRDevices:::: recieved productType::>> "+productType+""
					+ "\n :: recieved serviceName::: >>"+serviceName+""
							+ "\n :: recieved selectedSubCategory::>>"+selectedSubCategory);
			
			if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MNS)){
				
					StringBuilder queryMNS = new StringBuilder();
					
					queryMNS.append("select ci.ciid, ci.ciname from Configuration ci,  CIRelationship cir where cir.relationShip=:relationship")
							.append(" and cir.dependentCIId=ci.ciid and ci.cmdblass=:cmdbClassProp")
							.append(" and cir.baseCIName =:servicename and ci.rootProductId=:producTypeFetched and ci.status in ('")
					        .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
					        .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by ci.ciname asc");
					
					Query queryMNS1 = session.createQuery(queryMNS.toString())
							                 .setString("servicename",serviceName)
							                 .setString("cmdbClassProp", prop.getProperty("cmdbClassCPE"))
							                 .setString("relationship", prop.getProperty("ciRelationshipCont"))
							                 .setString("producTypeFetched", productType);
					
					log.info("Query for Fetching Devices for MNS " + queryMNS1.getQueryString());
					
					if(queryMNS1.list()!=null && !queryMNS1.list().isEmpty()){
							log.info("Device data found for servicename - " + serviceName);
							
							for(Iterator it=queryMNS1.iterate();it.hasNext();){
									Object[] row = (Object[]) it.next();
									log.info("Device : " + row[0]);
									devicesList.put(row[0]+"", row[1]+"");
							}
					}
					else{
						log.debug("No Devices found for this service name - " + serviceName);
					}
			}
			else{
				
				//When product Type ="Private Cloud"
				
				StringBuilder queryPC = new StringBuilder();
				
				//derive cmdbclass on basis of subcategory selected 
				StringBuilder querycmdbclass = new StringBuilder();
				querycmdbclass.append("select distinct srsc.cmdbClass from ServiceRequestSubCategory srsc where")
							  .append(" srsc.subCategory=:selectedSubCategoryFetched and srsc.rootProductId=:producTypeFetched");
				
				Query querycmdbclass1 = session.createQuery(querycmdbclass.toString())
						                       .setString("selectedSubCategoryFetched", selectedSubCategory)
						                       .setString("producTypeFetched", productType);
				
				log.info("Query to fetch Cmdbclass of subcategory for fetching devices"+querycmdbclass1);
				String cmdbClass = null;
					if(null != querycmdbclass1.uniqueResult()){
						cmdbClass = querycmdbclass1.uniqueResult().toString();
						log.info("cmdbClass of subcategory : " + selectedSubCategory + ""
								+ "\n fetched for getting dependent devices is : " + cmdbClass + ""
										+ "\n for poroduct type ::" +productType);
					}else{
						log.info("Failing to execute cmdbclass query to fetch devices!!");
					}
					
				//derive devices on basis of cmdbclass //customer Id check required
					queryPC.append("select ci.ciid, ci.ciname from Configuration ci where")
						   .append(" ci.rootProductId=:producTypeFetched and ci.cmdblass=:cmdbClassFetched and ci.customeruniqueid=:customerId and ci.status in ('")
						   .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
						   .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by ci.ciname asc");
				
				
				Query queryPC1 = session.createQuery(queryPC.toString())
						                .setString("cmdbClassFetched", cmdbClass)
						                .setString("producTypeFetched", productType)
						                .setString("customerId", customerId);
				
				log.info("Query for Fetching Devices " + queryPC1.getQueryString());
				
				if(queryPC1.list()!=null && !queryPC1.list().isEmpty()){
					
					log.info("Device data found for subcategory - " + selectedSubCategory);
					
					for(Iterator it=queryPC1.iterate();it.hasNext();){
						Object[] row = (Object[]) it.next();
						log.info("Device : " + row[0]);
						devicesList.put(row[0]+"", row[1]+"");
					}
				}
				else{
					log.debug("No Devices found for this subcategory - " + selectedSubCategory);
				}
			}
			log.info("CLosing the transaction.");
			tx.commit();
			log.info("Returning Device List from Dao"+devicesList);
			return devicesList;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getSRDevices method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getSubCategory(String productType, String category, String serviceName) throws Exception{
		try{
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			
			LinkedHashMap<String, String> subCategoryList = new LinkedHashMap<String, String>();
			
			StringBuilder query = new StringBuilder();
			
			if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MNS))
			{
				//Query to fecth cmdbclass of service recieved to fetch subcategory for MNS
				query.append("select distinct c.cmdblass from Configuration c where c.ciname=:serviceName and c.rootProductId=:producTypeFetched and c.status in ('")
					 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
					 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
				
				Query query1 = session.createQuery(query.toString())
									  .setString("serviceName",serviceName)
									  .setString("producTypeFetched", productType);
				String cmdbClass = null;
				if(null != query1.uniqueResult()){
					cmdbClass = query1.uniqueResult().toString();
					log.info("cmdbClass of service : " + serviceName + " fetched for getting dependent devices is : " + cmdbClass);
				}
				
				
				Query query2 = session.createQuery("select s.subCategory from ServiceRequestSubCategory s where s.category=:category "
														+ "and s.cmdbClass=:cmdbClass and s.rootProductId=:producTypeFetched order by s.subCategory asc")
									  .setString("category",category)
						              .setString("cmdbClass",cmdbClass)
						              .setString("producTypeFetched", productType);
				
					if(query2.list()!=null && !query2.list().isEmpty()){
						
						log.info("Subcategory data found for service - " + serviceName + "\n and category - " + category);
						
						for(Iterator it=query2.iterate();it.hasNext();){
							String row = (String) it.next();
							log.info("Subcategory Fetched " + row);
							subCategoryList.put(row, row);
					}
				}
				else{
					log.debug("Subcategory data not found for Category - " + category + " \n and service name - " + serviceName);
				}
			
			}else if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_WHISPIR) || productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_IPSCAPE) || productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MANDOE)){
				Query query2 = session.createQuery("select s.subCategory from ServiceRequestSubCategory s where s.category=:category and "
													+ "s.rootProductId=:producTypeFetched order by s.subCategory asc")
						.setString("category",category)
						.setString("producTypeFetched", "SaaS");
  
				if(query2.list()!=null && !query2.list().isEmpty()){
					
					log.info("Subcategory data found for service - " + serviceName + "\n and category - " + category);
					
					for(Iterator it=query2.iterate();it.hasNext();){
						String row = (String) it.next();
						log.info("Subcategory Fetched " + row);
						subCategoryList.put(row, row);
					}
				}
				else{
					log.debug("Subcategory data not found for Category - " + category + " \n and service name - " + serviceName);
				}
			}else{
					Query query2 = session.createQuery("select s.subCategory from ServiceRequestSubCategory s where s.category=:category and "
														+ "s.rootProductId=:producTypeFetched order by s.subCategory asc")
											.setString("category",category)
											.setString("producTypeFetched", productType);
				    
					if(query2.list()!=null && !query2.list().isEmpty()){
						
						log.info("Subcategory data found for service - " + serviceName + "\n and category - " + category);
						
						for(Iterator it=query2.iterate();it.hasNext();){
							String row = (String) it.next();
							log.info("Subcategory Fetched " + row);
							subCategoryList.put(row, row);
						}
					}
					else{
						log.debug("Subcategory data not found for Category - " + category + " \n and service name - " + serviceName);
					}
			}
			log.info("CLosing the transaction.");
			tx.commit();
			log.info("subCategoryList returned from Dao"+subCategoryList);
			return subCategoryList;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getSubCategory method  - " +e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public String fetchCategoryForMACD(String serviceName) throws Exception{
		try{
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			StringBuilder query = new StringBuilder();
			query.append("select distinct c.cmdblass from Configuration c where c.ciname=:serviceName and c.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query1 = session.createQuery(query.toString()).setString("serviceName",serviceName);
			String cmdbClass = null;
			if(null != query1.uniqueResult()){
				cmdbClass = query1.uniqueResult().toString();
				log.info("cmdbClass of service : " + serviceName + " fetched for submitting Category : " + cmdbClass);
			}
			log.info("CLosing the transaction.");
			tx.commit();
			return cmdbClass;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl fetchCategoryForMACD method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}

	public LinkedHashMap<String, String> getSiteNameMap(String customerId) {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select s.customersite, s.siteid from Site s where s.customeruniqueid=:custId and s.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query1 = session.createQuery(query.toString()).setString("custId", customerId);
			for (Iterator it = query1.iterate(); it.hasNext();) {
				Object[] row = (Object[]) it.next();
				siteNameIDMap.put(row[0] + "", row[0] + "");
			}
			tx.commit();
			return siteNameIDMap;
		} catch (Exception e) {
			log.error("Error in retrieving SiteName list "+e.getMessage());
			throw e;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}
	
	public List<String> getProductTypeList() {
		try {
				List<String> productTypeList = new ArrayList<String>();
			
				//need to add customer unique id check for subscribed servcies list
			
		      productTypeList.add(GenericConstants.PRODUCTTYPE_MNS);
		      productTypeList.add(GenericConstants.PRODUCTTYPE_PRIVATECLOUD);
		      productTypeList.add(GenericConstants.PRODUCTTYPE_WHISPIR);
		      productTypeList.add(GenericConstants.PRODUCTTYPE_IPSCAPE);
		      productTypeList.add(GenericConstants.PRODUCTTYPE_MANDOE);
		      
		      log.info("productTypeList in dao"+ productTypeList);	
		      return productTypeList;
			} catch (Exception e) {
			log.error("Error in retrieving ProductType list in dao "+e.getMessage());
			throw e;
		} 
	}
	
	public LinkedHashMap<String,List<String>> prodClassforSelectedProductType(String selectedProductType) {
		try {
				LinkedHashMap<String, List<String>> productClassList = new LinkedHashMap<String, List<String>>();

				log.info("prodClassforSelectedProductType:::: recieved productType::>> "+selectedProductType);
				
				StringBuilder queryProductClass = new StringBuilder();
				
				if(selectedProductType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_WHISPIR) || selectedProductType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_IPSCAPE) || selectedProductType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MANDOE)){
					
					queryProductClass.append("select rootProductId,productclassificationtype from productclassification where rootProductId=:producTypeFetched").append(" order by productclassificationtype asc");
					Query queryProductClass1 = sessionFactory.getCurrentSession().createSQLQuery(queryProductClass.toString())
					                 .setString("producTypeFetched", "SaaS");
			
						log.info("Query for Fetching productClassList for selectedProductType " + queryProductClass1.getQueryString());
						
						List productTypeList = queryProductClass1.list();
						List<String> productClassification = new ArrayList<String>();
						
						if(productTypeList!=null && !productTypeList.isEmpty()){
								log.info("productClassList data found for selectedProductType - " + selectedProductType);
								String  productType = null;
								for(Object obj : productTypeList)
								{
									    
										Object[] row = (Object[]) obj;
										log.debug("productClassList ************************* : " + (String)row[1]);
										log.debug("productClassList ######################### : " + row[1].toString());
										productClassification.add((String)row[1]);
										productType = row[0].toString();
										
								}
								productClassList.put( productType, productClassification);
						}
						else
						{
							log.debug("No productClassList found for this selectedProductType - " + selectedProductType);
						}
						log.info("productTypeList in dao"+ productClassList);	
						return productClassList;
				}else{
				queryProductClass.append("select rootProductId,productclassificationtype from productclassification where rootProductId=:producTypeFetched").append(" order by productclassificationtype asc");
					Query queryProductClass1 = sessionFactory.getCurrentSession().createSQLQuery(queryProductClass.toString())
					                 .setString("producTypeFetched", selectedProductType);
			
						log.info("Query for Fetching productClassList for selectedProductType " + queryProductClass1.getQueryString());
						
						List productTypeList = queryProductClass1.list();
						List<String> productClassification = new ArrayList<String>();
						
						if(productTypeList!=null && !productTypeList.isEmpty()){
								log.info("productClassList data found for selectedProductType - " + selectedProductType);
								String  productType = null;
								for(Object obj : productTypeList)
								{
									    
										Object[] row = (Object[]) obj;
										log.debug("productClassList ************************* : " + (String)row[1]);
										log.debug("productClassList ######################### : " + row[1].toString());
										productClassification.add((String)row[1]);
										productType = row[0].toString();
										
								}
								productClassList.put( productType, productClassification);
						}
						else
						{
							log.debug("No productClassList found for this selectedProductType - " + selectedProductType);
						}
						log.info("productTypeList in dao"+ productClassList);	
						return productClassList;
				}		
			} catch (Exception e) {
			log.error("Error in retrieving ProductType list in dao "+e.getMessage());
			throw e;
		} 
	}
	
	public String getServiceNameforLicenses(String customerId)
	{
		System.out.println("getServiceNameforLicenses");
		String sql = "select ciname from configurationitem ci where ci.citype = 'Service'  and ci.cmdbclass in ('MNS','Private Cloud') and ci.customeruniqueid = '"+customerId+"' order by ciname asc";
		
		Query query = sessionFactory.openSession().createSQLQuery(sql);
		if(query.list()!=null && query.list().size()>0){
			System.out.println("query output::::::::" +(String) query.list().get(0));
			return (String) query.list().get(0);
		}
			
		else
			return "";
		
		
	}
		
}
