package com.tt.servicerequestlist.mvc.model;

public class DetailsSearchParameters {

	private String sortCriterion;
	private String sortDirection;
	private int pageNumber = 0; // default value

	// Set it from session
	private String customeruniqueid;
	private String citype = "Service";
	private String requestType;
	
	
	
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	public String getSortCriterion() {
		return sortCriterion;
	}
	public void setSortCriterion(String sortCriterion) {
		this.sortCriterion = sortCriterion;
	}
	public String getSortDirection() {
		return sortDirection;
	}
	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public String getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
	public String getCitype() {
		return citype;
	}
	public void setCitype(String citype) {
		this.citype = citype;
	}
	
	
}
