package com.tt.servicerequestlist.mvc.service.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.servicerequest.ChangeRequest;
import com.tt.model.servicerequest.ServiceRequest;
import com.tt.model.servicerequest.ServiceRequestItem;
import com.tt.RefreshServiceRequest;
import com.tt.servicerequestlist.mvc.dao.ServiceRequestDao;
import com.tt.servicerequestlist.mvc.dao.ServiceRequestDaoImpl;
import com.tt.servicerequestlist.mvc.model.DatePair;
import com.tt.servicerequestlist.mvc.model.DetailsSearchParameters;
import com.tt.servicerequestlist.mvc.model.SearchParameters;
import com.tt.servicerequestlist.mvc.model.ServiceRequestResults;

@Service(value = "serviceRequestService")
@Transactional
public class ServiceRequestServiceImpl implements ServiceRequestService {

	private ServiceRequestDao serviceRequestDao;
	private final static Logger log = Logger
			.getLogger(ServiceRequestServiceImpl.class);
	private static final String CHANGE_REQUEST = "changeRequest";

	@Autowired
	@Qualifier("serviceRequestDaoImpl")
	public void setServiceRequestDao(ServiceRequestDao serviceRequestDao) {
		this.serviceRequestDao = serviceRequestDao;
	}

	@Transactional
	@Override
	public ServiceRequestItem getServiceRequestItemDetailsForItemId(
			String itemId) {
		return serviceRequestDao.getServiceRequestItemDetailsForItemId(itemId);
	}

	@Transactional
	@Override
	public ServiceRequest getServiceRequestDetailsForRequestId(
			String requestId, DetailsSearchParameters detailsSearchParameters) {
		return serviceRequestDao.getServiceRequestDetailsForRequestId(
				requestId, detailsSearchParameters);
	}

	@Transactional
	@Override
	public ChangeRequest getChangeRequestDetailsForRequestId(String crRequestId, DetailsSearchParameters detailsSearchParameters, String customerId) {
		log.info("in service --- getChangeRequestDetailsForRequestId  :::: "
				+ crRequestId);
		return serviceRequestDao.getChangeRequestDetailsForRequestId(crRequestId, detailsSearchParameters, customerId);
	}

	@Transactional
	@Override
	public List<Integer> getServiceRequestTrends(List<DatePair> cycleDateRange,
			String customeruniqueid) {

		return serviceRequestDao.getServiceRequestTrends(cycleDateRange,
				customeruniqueid);
	}

	// CR Trends
	@Transactional
	@Override
	public List<Integer> getChangeRequestTrends(List<DatePair> cycleDateRange,
			String customeruniqueid, String source) {

		return serviceRequestDao.getChangeRequestTrends(cycleDateRange,
				customeruniqueid, source);
	}

	@Override
	public ServiceRequestResults getAllServiceRequests(
			SearchParameters searchParameters, String customerId) {

		log.info("ServiceRequestServiceImpl getAllServiceRequests Start");
		
		String filterString = "";
		if(searchParameters.getAppliedFilters() != null)
		{
			filterString = searchParameters.getAppliedFilters();
		}
		
		
		
		ServiceRequestResults requestList = null;
		String clickedTdId = searchParameters.getClickedTdId();

		if ((searchParameters.isSearchBySRCustTT() && GenericConstants.SERVICE_REQUEST.equalsIgnoreCase(clickedTdId))
				|| (GenericConstants.SERVICE_REQUEST.equalsIgnoreCase(searchParameters.getAdvReqType())
				|| (searchParameters.isFilterSearch() && (filterString.isEmpty() || filterString.contains(GenericConstants.SERVICE_REQUEST))))) 
		{
			requestList = serviceRequestDao.getAllServiceRequests(searchParameters, customerId);
		} 
		else if ((searchParameters.isSearchBySRCustTT() && (GenericConstants.CUSTOMER_INITIATED_PENDING.equalsIgnoreCase(clickedTdId) 
				|| GenericConstants.CUSTOMER_INITIATED_APPROVED.equalsIgnoreCase(clickedTdId) 
				|| GenericConstants.TTINITIATED_PENDING.equalsIgnoreCase(clickedTdId) 
				|| GenericConstants.TTINITIATED_APPROVED.equalsIgnoreCase(clickedTdId))
				|| (CHANGE_REQUEST.equalsIgnoreCase(searchParameters.getAdvReqType()))))
		{
			requestList = serviceRequestDao.getAllChangeRequests(searchParameters, customerId);
		} 
		else if(searchParameters.isFilterSearch() && (filterString.contains("Customer") 
														|| filterString.contains("TT") 
														|| filterString.contains("Submitted")
														|| filterString.contains("Approved")))
		{
			requestList = serviceRequestDao.getAllChangeRequests(searchParameters, customerId);
		}
		else 
		{
			requestList = serviceRequestDao.getAllServiceRequests(searchParameters, customerId);
		}

		log.info("ServiceRequestServiceImpl getAllServiceRequests End");

		return requestList;
	}

	// CR Trends
	@Override
	public ServiceRequestResults getAllChangeRequests(
			SearchParameters searchParameters, String customerId) {
		ServiceRequestResults requestList = serviceRequestDao
				.getAllChangeRequests(searchParameters, customerId);

		if (searchParameters.getRequesttype() == "Change") {
			requestList = serviceRequestDao.getAllServiceRequests(
					searchParameters, customerId);
		} else {
			requestList = serviceRequestDao.getAllChangeRequests(
					searchParameters, customerId);
		}

		return requestList;
	}

	@Override
	public void dataRefresh(String customerId) {

		Date lastRefreshDate = serviceRequestDao.getLastRefreshDate(customerId,
				"ServiceRequest");

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		try {
			log.info("Refresh Data......--------->>>>>>");
			RefreshServiceRequest refreshObj = new RefreshServiceRequest();
			String toDate = new String();
			String fromDate = new String();
			Date todate = new Date();
			toDate = df.format(todate);
			log.info("toDate-----------" + toDate);
			if (lastRefreshDate != null)
				fromDate = df.format(lastRefreshDate);

			log.info("fromDate------------" + fromDate);
			refreshObj.refreshServiceRequest(customerId, toDate, fromDate,"CUST");
			log.info("Refresh Data Done......--------->>>>>>");
		} catch (Exception ex) {
			log.error(" Exception while calling data refresh ");
			log.error(ex.getMessage());
		}
	}

	@Override
	public String getLastRefreshDate(String customerId) {

		Date lastRefreshDate = serviceRequestDao.getLastRefreshDate(customerId,
				"ServiceRequest");

		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Jakarta");
		df.setTimeZone(timeZone1);

		if (lastRefreshDate != null) {
			String fromDate = df.format(lastRefreshDate);
			return fromDate;
		}
		return null;
	}

	@Override
	public ServiceRequestResults getServiceRequestsMetric(
			String customerUniqueID) {
		log.info("ServiceRequestServiceImpl getServiceRequestsMetric");
		return serviceRequestDao.getServiceRequestMetrics(customerUniqueID);
	}

	@Override
	public ServiceRequestResults getChangeRequestsMetric(String customerUniqueID) {
		log.info("ServiceRequestServiceImpl getChangeRequestsMetric");
		return serviceRequestDao.getChangeRequestsMetrics(customerUniqueID);
	}
}
