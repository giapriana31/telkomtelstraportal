package com.tt.pvsitelv1.mvc.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;

@Repository(value = "pvSiteLv1Dao")
@Transactional
public class PVSiteLv1Dao {

	private final static Logger log = Logger.getLogger(PVSiteLv1Dao.class);

	@Autowired
	private SessionFactory sessionFactory;

	public Map<String, Integer> getGraphDataProductClassification(String customerId, String productClassification) {

		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(6, "#89C35C");
		deliveryStageToColor.put(5, "#FA8072");
		deliveryStageToColor.put(4, "#00B0F0");
		deliveryStageToColor.put(3, "#7030A0");
		deliveryStageToColor.put(2, "#A40800");
		deliveryStageToColor.put(1, "#000000");

		Map<String,Integer> graphDataProductClassification=new HashMap<String, Integer>();

		StringBuffer sb = new StringBuffer();
		//delivery stage , siteid
		sb.append(" select ci.delivery_stage, s.siteid ");
		sb.append(" from configurationitem ci left join ");
		sb.append(" site s ");
		sb.append(" on s.siteid = ci.siteid ");
		sb.append(" where s.customeruniqueid = '"+customerId+"'  and ci.delivery_stage is not null ");
		sb.append(" and ci.delivery_stage <> '' and ci.productclassification = '"+productClassification+"' ");
		sb.append(" group by s.siteid, ci.delivery_stage ");
		sb.append(" order by ci.delivery_stage, s.siteid ASC; ");
				
		Query innerQuery= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());

		List<Object[]> productStatusListProductClassification=innerQuery.list();
		
		if(null!=productStatusListProductClassification && productStatusListProductClassification.size()>0){
			for (Object[] productDataProductClassification : productStatusListProductClassification) {
				String productDeliveryStageProductClassification=(String)productDataProductClassification[0];
				if(null!=productDeliveryStageProductClassification && productDeliveryStageProductClassification.length()>0){
					String deliveryColor=deliveryStageToColor.get(Integer.parseInt(productDeliveryStageProductClassification));
					if(graphDataProductClassification.size()>0){
						Set<String> mapKeySet=graphDataProductClassification.keySet();
						if(mapKeySet.contains(deliveryColor)){
							int currentCount=graphDataProductClassification.get(deliveryColor);
							graphDataProductClassification.put(deliveryColor, currentCount+1);
						}else{	
							graphDataProductClassification.put(deliveryColor, 1);
						}
					}else{
						graphDataProductClassification.put(deliveryColor, 1);	
					}
				}
			}
		}

		return graphDataProductClassification;

	}
}
