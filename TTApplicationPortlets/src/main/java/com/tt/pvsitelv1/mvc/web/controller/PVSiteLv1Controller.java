package com.tt.pvsitelv1.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.pvsitelv1.mvc.service.common.PVSiteLv1ManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("pvSiteLv1Controller")
@RequestMapping("VIEW")
public class PVSiteLv1Controller {
	
	private final static Logger log = Logger.getLogger(PVSiteLv1Controller.class);
	
	private PVSiteLv1ManagerImpl pvSiteLv1ManagerImpl;
	
	@Autowired
	@Qualifier("pvSiteLv1ManagerImpl")
	public void setPVSiteLv1ManagerImpl(PVSiteLv1ManagerImpl pvSiteLv1ManagerImpl){
		this.pvSiteLv1ManagerImpl = pvSiteLv1ManagerImpl;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws PortalException, SystemException {
    	log.debug("PVSiteLv1Controller * handleRenderRequest * Inside the method");
	    try{
			log.debug("PVSiteLv1Controller * handleRenderRequest * Test Debug Code-------------");
			ThemeDisplay themeDisplay = (ThemeDisplay) request .getAttribute(WebKeys.THEME_DISPLAY);
			String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(renderResponse));
			
			log.debug("PVSiteLv1Controller * handleRenderRequest * customerId......." + customerId);
	
			//MWAN START
			Map<String,Integer> graphDataMapMNSWAN=pvSiteLv1ManagerImpl.getGraphDetailsMNSWAN(customerId);
		
			Set<String> graphDataKeysMWAN=graphDataMapMNSWAN.keySet();
			int subscribedMNSWAN = 0;
			Gson gsonmnswan = new Gson();
			List<String> colorListMNSWAN=new ArrayList<String>();
			colorListMNSWAN.add("#89C35C");
			colorListMNSWAN.add("#FA8072");
			colorListMNSWAN.add("#00B0F0");
			colorListMNSWAN.add("#7030A0");
			colorListMNSWAN.add("#A40800");
			colorListMNSWAN.add("#000000");
			
			for (String string : colorListMNSWAN) 
			{
				if(null != graphDataMapMNSWAN && graphDataMapMNSWAN.keySet().contains(string))
				{
					subscribedMNSWAN = subscribedMNSWAN + graphDataMapMNSWAN.get(string).intValue();
				}
			}
		
			for (String string : colorListMNSWAN) 
			{
				if(null != graphDataMapMNSWAN && !graphDataMapMNSWAN.keySet().contains(string))
				{
					graphDataMapMNSWAN.put(string, 0);
				}
			}
	
			String siteGraphDataJsonMNSWAN = gsonmnswan.toJson(graphDataMapMNSWAN);
			log.info("siteGraphDataJsonMWAN -----"+siteGraphDataJsonMNSWAN);
			String colorListJsonMNSWAN=gsonmnswan.toJson(colorListMNSWAN);
			//MWAN END

			//MWANOPTI START
			Map<String,Integer> graphDataMapMNSWANOPTI=pvSiteLv1ManagerImpl.getGraphDetailsMNSWANOPTI(customerId);
		
			Set<String> graphDataKeysMNSWANOPTI=graphDataMapMNSWANOPTI.keySet();
			int subscribedMNSWANOPTI = 0;
			Gson gsonmnswanopti = new Gson();
			List<String> colorListMNSWANOPTI=new ArrayList<String>();	
			colorListMNSWANOPTI.add("#89C35C");
			colorListMNSWANOPTI.add("#FA8072");
			colorListMNSWANOPTI.add("#00B0F0");
			colorListMNSWANOPTI.add("#7030A0");
			colorListMNSWANOPTI.add("#A40800");
			colorListMNSWANOPTI.add("#000000");
		
		
			for (String string : colorListMNSWANOPTI) 
			{
				if(null != graphDataMapMNSWANOPTI && graphDataMapMNSWANOPTI.keySet().contains(string))
				{
					subscribedMNSWANOPTI = subscribedMNSWANOPTI + graphDataMapMNSWANOPTI.get(string).intValue();
				}	
			}
			
			for (String string : colorListMNSWANOPTI) 
			{
				if(null != graphDataMapMNSWANOPTI && !graphDataMapMNSWANOPTI.keySet().contains(string))
				{
					graphDataMapMNSWANOPTI.put(string, 0);
				}	
			}
		
			String siteGraphDataJsonMNSWANOPTI = gsonmnswanopti.toJson(graphDataMapMNSWANOPTI);
			String colorListJsonMNSWANOPTI=gsonmnswanopti.toJson(colorListMNSWANOPTI);
			//MWANOPTI END			

			//MWLAN START
			Map<String,Integer> graphDataMapMNSWLAN=pvSiteLv1ManagerImpl.getGraphDetailsMNSWLAN(customerId);
		
			Set<String> graphDataKeysMNSWLAN=graphDataMapMNSWLAN.keySet();
			int subscribedMNSWLAN = 0;
			Gson gsonmnswlan = new Gson();
			List<String> colorListMNSWLAN=new ArrayList<String>();
			colorListMNSWLAN.add("#89C35C");
			colorListMNSWLAN.add("#FA8072");
			colorListMNSWLAN.add("#00B0F0");
			colorListMNSWLAN.add("#7030A0");
			colorListMNSWLAN.add("#A40800");
			colorListMNSWLAN.add("#000000");
		
			for (String string : colorListMNSWLAN) 
			{
				if(null != graphDataMapMNSWLAN && graphDataMapMNSWLAN.keySet().contains(string))
				{
					subscribedMNSWLAN = subscribedMNSWLAN + graphDataMapMNSWLAN.get(string).intValue();
				}	
			}
		
			for (String string : colorListMNSWLAN) 
			{
				if(null != graphDataMapMNSWLAN && !graphDataMapMNSWLAN.keySet().contains(string))
				{
					graphDataMapMNSWLAN.put(string, 0);
				}	
			}
		
			String siteGraphDataJsonMNSWLAN = gsonmnswlan.toJson(graphDataMapMNSWLAN);
			String colorListJsonMNSWLAN=gsonmnswlan.toJson(colorListMNSWLAN);
			//MWLAN END			

			//MLAN START
			Map<String,Integer> graphDataMapMNSLAN=pvSiteLv1ManagerImpl.getGraphDetailsMNSLAN(customerId);
		
			Set<String> graphDataKeysMNSLAN=graphDataMapMNSLAN.keySet();
			int subcsribedMNSLAN = 0;
			Gson gsonmnslan = new Gson();
			List<String> colorListMNSLAN=new ArrayList<String>();
			colorListMNSLAN.add("#89C35C");
			colorListMNSLAN.add("#FA8072");
			colorListMNSLAN.add("#00B0F0");
			colorListMNSLAN.add("#7030A0");
			colorListMNSLAN.add("#A40800");
			colorListMNSLAN.add("#000000");
		
			for (String string : colorListMNSLAN) 
			{
				if(null != graphDataMapMNSLAN && graphDataMapMNSLAN.keySet().contains(string))
				{
					subcsribedMNSLAN = subcsribedMNSLAN + graphDataMapMNSLAN.get(string).intValue();
				}	
			}
		
			for (String string : colorListMNSLAN) 
			{
				if(null != graphDataMapMNSLAN && !graphDataMapMNSLAN.keySet().contains(string))
				{
					graphDataMapMNSLAN.put(string, 0);
				}	
			}
		
			String siteGraphDataJsonMNSLAN = gsonmnslan.toJson(graphDataMapMNSLAN);
			String colorListJsonMNSLAN=gsonmnslan.toJson(colorListMNSLAN);
			//MLAN END

			//MFIREWALL START
			Map<String,Integer> graphDataMapMSSFIREWALL=pvSiteLv1ManagerImpl.getGraphDetailsMSSFIREWALL(customerId);
		
			Set<String> graphDataKeysMSSFIREWALL=graphDataMapMSSFIREWALL.keySet();
			int subscribedMSSFIREWALL = 0;
			Gson gsonmssfirewall = new Gson();
			List<String> colorListMSSFIREWALL=new ArrayList<String>();
			colorListMSSFIREWALL.add("#89C35C");
			colorListMSSFIREWALL.add("#FA8072");
			colorListMSSFIREWALL.add("#00B0F0");
			colorListMSSFIREWALL.add("#7030A0");
			colorListMSSFIREWALL.add("#A40800");
			colorListMSSFIREWALL.add("#000000");
		
			for (String string : colorListMSSFIREWALL) 
			{
				if(null != graphDataMapMSSFIREWALL && graphDataMapMSSFIREWALL.keySet().contains(string))
				{
					subscribedMSSFIREWALL = subscribedMSSFIREWALL + graphDataMapMSSFIREWALL.get(string).intValue();
				}	
			}
		
			for (String string : colorListMSSFIREWALL) 
			{
				if(null != graphDataMapMSSFIREWALL && !graphDataMapMSSFIREWALL.keySet().contains(string))
				{
					graphDataMapMSSFIREWALL.put(string, 0);
				}	
			}
		
			String siteGraphDataJsonMSSFIREWALL = gsonmssfirewall.toJson(graphDataMapMSSFIREWALL);
			String colorListJsonMSSFIREWALL=gsonmssfirewall.toJson(colorListMSSFIREWALL);
			//MFIREWALL END
	
			ArrayList<String> graphBottom= new ArrayList<String>();
			//	HashMap<String, String> graphBottom=new HashMap<String, String>();				
			graphBottom.add("#89C35C");
			graphBottom.add("#FA8072");
			graphBottom.add("#00B0F0");
			graphBottom.add("#7030A0");
			graphBottom.add("#A40800");
			graphBottom.add("#000000");

			//	model.addAttribute("bysitejson", officeBuildingSiteManager.getBySite(customerId));
	
			//MWAN
			model.addAttribute("pvSiteStatusGraphDataMWAN", siteGraphDataJsonMNSWAN);
			model.addAttribute("pvGraphColorListSiteStatusMWAN", colorListJsonMNSWAN);
			model.addAttribute("pvSubscribedMWAN", subscribedMNSWAN);
			log.info("siteStatusGraphDataMWAN -----" + siteGraphDataJsonMNSWAN.toString());
			log.info("graphColorListSiteStatusMWAN -----" + colorListJsonMNSWAN.toString());
			log.info("subscribedMWAN -----" + subscribedMNSWAN);
			//MWAN
			
			//MWANOPTI
			model.addAttribute("pvSiteStatusGraphDataMWANOPTI", siteGraphDataJsonMNSWANOPTI);
			model.addAttribute("pvGraphColorListSiteStatusMWANOPTI", colorListJsonMNSWANOPTI);
			model.addAttribute("pvSubscribedMWANOPTI", subscribedMNSWANOPTI);
			//MWANOPTI
		
			//MWLAN
			model.addAttribute("pvSiteStatusGraphDataMWLAN", siteGraphDataJsonMNSWLAN);
			model.addAttribute("pvGraphColorListSiteStatusMWLAN", colorListJsonMNSWLAN);
			model.addAttribute("pvSubscribedMWLAN", subscribedMNSWLAN);
			//MWLAN			
		
			//MLAN
			model.addAttribute("pvSiteStatusGraphDataMLAN", siteGraphDataJsonMNSLAN);
			model.addAttribute("pvGraphColorListSiteStatusMLAN", colorListJsonMNSLAN);
			model.addAttribute("pvSubcsribedMLAN", subcsribedMNSLAN);	
			//MLAN				
			
			//MFIREWALL
			model.addAttribute("pvSiteStatusGraphDataMFIREWALL", siteGraphDataJsonMSSFIREWALL);
			model.addAttribute("pvGraphColorListSiteStatusMFIREWALL", colorListJsonMSSFIREWALL);
			model.addAttribute("pvSubscribedMFIREWALL", subscribedMSSFIREWALL);
			//MFIREWALL
			
			model.addAttribute("pvGraphBottomListSiteStatus", graphBottom);
			
			log.debug("OfficeBuildingSiteDetailController * handleRenderRequest * Exit the method");
	    }catch (Exception e) {
		    log.error("Exception in handleRenderRequest method "+e.getMessage());
		    return null;
		}	
	    return "pvSiteLv1";
	}
}
