package com.tt.pvsitelv1.mvc.service.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.model.Site;
import com.tt.pvsitelv1.mvc.dao.PVSiteLv1Dao;

@Service(value = "pvSiteLv1ManagerImpl")
@Transactional
public class PVSiteLv1ManagerImpl {
	
	private final static Logger log = Logger.getLogger(PVSiteLv1ManagerImpl.class);
	private PVSiteLv1Dao pvSiteLv1Dao;
	
	@Autowired
	@Qualifier("pvSiteLv1Dao")
	public void setPVSiteLv1Dao(PVSiteLv1Dao pvSiteLv1Dao){
		this.pvSiteLv1Dao = pvSiteLv1Dao;
	}
	
	public Map<String, Integer> getGraphDetailsMNSWAN(String customerId) {
		return pvSiteLv1Dao.getGraphDataProductClassification(customerId,"MNS_WAN");
	}


	public Map<String, Integer> getGraphDetailsMNSWANOPTI(String customerId) {
		return pvSiteLv1Dao.getGraphDataProductClassification(customerId,"MNS_WANOPT");
	}


	public Map<String, Integer> getGraphDetailsMNSWLAN(String customerId) {
		return pvSiteLv1Dao.getGraphDataProductClassification(customerId,"MNS_WLAN");
	}


	public Map<String, Integer> getGraphDetailsMNSLAN(String customerId) {
		return pvSiteLv1Dao.getGraphDataProductClassification(customerId,"MNS_LAN");
	}


	public Map<String, Integer> getGraphDetailsMSSFIREWALL(String customerId) {
		return pvSiteLv1Dao.getGraphDataProductClassification(customerId,"NGFW");
	}

}
