package com.tt.network.mvc.web.controller;
import java.io.PrintWriter;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.model.NetworkMapSite;
import com.tt.network.mvc.model.NetworkSiteColor;
import com.tt.network.mvc.service.NetworkMapManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("networkController")
@RequestMapping("VIEW")
public class NetworkController {

    private final static Logger log = Logger.getLogger(NetworkController.class);
    private NetworkMapManagerImpl networkMapManagerImpl;
        
    @Autowired
    @Qualifier("networkMapManagerImpl")
    public void setNetworkMapManagerImpl(NetworkMapManagerImpl networkMapManagerImpl) {
		this.networkMapManagerImpl = networkMapManagerImpl;
	}

	@RenderMapping
    public String render(RenderRequest request, RenderResponse response, Model model) throws PortalException, SystemException {
	log.debug("NetworkController * render * Inside the method");
		//ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		//User user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), themeDisplay.getUser().getDisplayEmailAddress());
		
		try {
			
		String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(response));
		log.debug("NetworkController * render * customerId =" +customerId);
		String province = request.getParameter("province");
		log.debug("NetworkController * render * province"+province);
        List<NetworkMapSite> networkMaplist = networkMapManagerImpl.getNetworkMap(customerId);
        
		Gson gson = new Gson();
		String networkMapSiteList = gson.toJson(networkMaplist);
		
		model.addAttribute("ttNetworkGraph", networkMapSiteList);
//		model.addAttribute("ttNetworkSiteColor", networkSiteColorList);
		request.setAttribute("ttNetworkGraphRequest", networkMapSiteList);
		
		return "networkGraph";
		}catch (Exception e) {
		    log.error("Exception in render method"+e.getMessage());
		}	
		log.debug("NetworkController * render * Exit the method");
		return null;
    }
    
	
	// Method to get the site province details 
	@ResourceMapping(value = "getSiteProvince")
    public void getNetworkSiteProvince(Model model, ResourceRequest request,
    	    ResourceResponse response) throws Exception {
		log.debug("NetworkController * getNetworkSiteProvince * Inside the method");
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
	    PrintWriter out = response.getWriter();

		try {
		    
			String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(response));
			log.debug("NetworkController * getNetworkSiteProvince * customerId =" +customerId);
		    JSONObject jsonObj = new JSONObject(
			    req.getParameter("selectedProvinceJson"));
		    log.debug("NetworkController * getNetworkSiteProvince * req.getParameter(\"selectedProvinceJson\")); =" + req.getParameter("selectedProvinceJson"));
		   
		    String selectedProvince = jsonObj.getString("selectedProvince");
		    log.debug("NetworkController * getNetworkSiteProvince * selectedProvince-----" +selectedProvince);
		   
		    log.debug("NetworkController * getNetworkSiteProvince * selectedProvince.trim().length()-------" +selectedProvince.trim().length());
		    if (selectedProvince != null && selectedProvince.trim().length() != 0) {
		      
    		  List<NetworkSiteColor> networkSiteColorList = networkMapManagerImpl.getNetworkProvinceSites(customerId, selectedProvince);
    		  
   	          Gson gson = new Gson();
		      String networkSiteColorListJson = gson.toJson(networkSiteColorList);		        
		      log.debug("NetworkController * getNetworkSiteProvince * networkSiteColorListJson--------"+networkSiteColorListJson);
		      out.print(networkSiteColorListJson);
		    }		    
	    
		} catch (Exception e) {
		    log.error("Exception in getNetworkSiteProvince method "+e.getMessage());
		    throw e;
		}		
		log.debug("NetworkController * getNetworkSiteProvince * Exit the method");
	} 
}
