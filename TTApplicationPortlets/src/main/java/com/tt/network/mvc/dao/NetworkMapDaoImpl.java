package com.tt.network.mvc.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.NetworkMapSite;

import com.tt.network.mvc.model.NetworkSiteColor;

@Repository(value="networkMapDaoImpl")
@Transactional
public class NetworkMapDaoImpl {
	private final static Logger log = Logger.getLogger(NetworkMapDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<NetworkMapSite> getNetWorkMap(
			String customerId) {
		log.debug("NetworkMapDaoImpl * getNetWorkMap * Inside the method");
		try{
		log.debug("NetworkMapDaoImpl * getNetWorkMap * customerId "+ customerId);
		StringBuilder hql = new StringBuilder();
		hql.append("from NetworkMapSite npm");
		hql.append(" where npm.customeruniqueid =:customerId  ");
	
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		
		if( query.list()!=null &&  query.list().size()>0){
			log.debug("NetworkMapDaoImpl * getNetWorkMap * Result Set size is "+ query.list().size());
			return query.list();	
		}else{
			return null;
		}
		}catch (Exception e) {
		   log.error("Exception in getNetWorkMap method "+e.getMessage());
		}
		log.debug("NetworkMapDaoImpl * getNetWorkMap * Exit the method");
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<NetworkSiteColor> getNetworkProvinceSites(
			String customerId, String province) {
		log.debug("NetworkMapDaoImpl * getNetworkProvinceSites * Inside the method");
		try{
		log.debug("NetworkMapDaoImpl * getNetworkProvinceSites * customerId"+ customerId + "   Province" +province);
		StringBuilder sql = new StringBuilder();
	/*	sql.append("SELECT  ccns.cust_id, ccns.province, (select distinct sitename from incddashboardmetrics where siteid=ccns.site_id) sitename, CASE WHEN ds1.color = 3 THEN 'Red' WHEN 2 THEN 'Amber' WHEN 1 THEN 'Green' END sitecolor ");
		sql.append("FROM (select cns.ciid ci_id ,cns.customeruniqueid cust_id,cns.siteid site_id,cns.networkprovince province from customernetworksites cns group by cns.customeruniqueid,cns.siteid,cns.networkprovince) ccns ");
		sql.append( "LEFT JOIN incddashboardmetrics idm ON ccns.ci_id = idm.serviceid AND lower(idm.incdcategory) = 'network' AND ccns.cust_id = idm.customeruniqueid ");
		sql.append("INNER JOIN (SELECT ds.siteid siteid, MAX(ds.color) color FROM (SELECT   siteid, (CASE sitecolor WHEN 'Red' THEN 3 WHEN 'Amber' THEN 2 WHEN 'Green' THEN 1 ELSE - 1 END) color ");
		sql.append( "FROM incddashboardmetrics WHERE siteid != '' AND lower(incdcategory) != 'network' GROUP BY siteid , sitecolor) ds GROUP BY ds.siteid) ds1 ON ds1.siteid = ccns.site_id ");
		sql.append(" where cust_id= '");
		sql.append(customerId);
		sql.append("' and province='");
		sql.append(province);
		sql.append("'");*/
		
		sql.append("select ccns.cust_id, ccns.province, s.customersite sitename, (CASE WHEN ds1.color = 3 THEN 'Red' WHEN ds1.color = 2 THEN 'Amber' WHEN ds1.color = 1 THEN 'Green' ELSE 'Green' END ) sitecolor ");
		sql.append("from   ((SELECT cns.ciid ci_id, cns.customeruniqueid cust_id, cns.siteid site_id, cns.networkprovince province FROM  customernetworksites cns GROUP BY cns.customeruniqueid , cns.siteid , cns.networkprovince) ccns ");
		sql.append("INNER JOIN site s on s.siteid = ccns.site_id) LEFT JOIN (SELECT ds.customeruniqueid,ds.siteid siteid, MAX(ds.color) color FROM (SELECT siteid,customeruniqueid, (CASE sitecolor WHEN 'Red' THEN 3 WHEN 'Amber' THEN 2 WHEN 'Green' THEN 1 ELSE - 1 END) color ");
		sql.append("FROM incddashboardmetrics WHERE siteid != '' AND LOWER(incdcategory) != 'network' and lower(incidentstatus) not in ('cancelled','closed') GROUP BY siteid , sitecolor) ds GROUP BY ds.siteid) ds1 ON ds1.siteid = ccns.site_id WHERE ");
		sql.append(" cust_id= '");
		sql.append(customerId);
		sql.append("' and province='");
		sql.append(province);
		sql.append("'");
		sql.append(" ORDER BY ds1.color desc");
		
		log.debug("NetworkMapDaoImpl * getNetworkProvinceSites * sql query"+sql.toString());
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
				
		if(query.list()!=null &&  query.list().size()>0){
			log.debug("NetworkMapDaoImpl * getNetworkProvinceSites * Result Set size is "+ query.list().size());
			
			List<Object[]> result = (List<Object[]>) query.list();
			
			List<NetworkSiteColor> siteColorList = new ArrayList<NetworkSiteColor>();
			NetworkSiteColor siteColor = null;
			
			for(Object[] column: result) {
				if(column[0] != null) {
					siteColor = new NetworkSiteColor();
					
					siteColor.setSiteName((String) column[2]);
					siteColor.setColorCode((String) column[3]);
					siteColorList.add(siteColor);
				}				
			}
			return siteColorList;
		}else{
			return null;
		}
		}catch (Exception e) {
		   log.error("Exception in getNetworkProvinceSites method "+e.getMessage());
		}	
		log.debug("NetworkMapDaoImpl * getNetworkProvinceSites * Exit the method");
		return null;
	}
}
