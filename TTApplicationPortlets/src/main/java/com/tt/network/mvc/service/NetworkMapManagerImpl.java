package com.tt.network.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.NetworkMapSite;
import com.tt.logging.Logger;

import com.tt.network.mvc.dao.NetworkMapDaoImpl;
import com.tt.network.mvc.model.NetworkSiteColor;


@Service(value = "networkMapManagerImpl")
@Transactional
public class NetworkMapManagerImpl {
	private final static Logger log = Logger.getLogger(NetworkMapManagerImpl.class);
	private NetworkMapDaoImpl networkMapDaoImpl;

	@Autowired
	@Qualifier("networkMapDaoImpl")
	public void setNetworkMapDaoImpl(
			NetworkMapDaoImpl networkMapDaoImpl) {
		this.networkMapDaoImpl = networkMapDaoImpl;
	}

	public NetworkMapDaoImpl getNetworkMapDaoImpl() {
		return networkMapDaoImpl;
	}
	
	public List<NetworkMapSite> getNetworkMap(String customerId) {
		log.debug("NetworkMapManagerImpl * getNetworkMap * Inside the method");
		try{
		List<NetworkMapSite> sites ;
		log.debug("NetworkMapManagerImpl * getNetworkMap * customerId--------------->>"+customerId);
		sites =  networkMapDaoImpl.getNetWorkMap(customerId);
		return sites;
		}catch(Exception e){
			log.error("Exception in getNetworkMap method "+e.getMessage());
		}
		log.debug("NetworkMapManagerImpl * getNetworkMap * Exit the method");
		return null;
	}
	
	public List<NetworkSiteColor> getNetworkProvinceSites(String customerId, String province) {
		log.debug("NetworkMapManagerImpl * getNetworkProvinceSites * Inside the method");
		try{
		List<NetworkSiteColor> provinces ;
		log.debug("NetworkMapManagerImpl * getNetworkProvinceSites * customerId--------------->>"+customerId);
		provinces =  networkMapDaoImpl.getNetworkProvinceSites(customerId, province);
		return provinces;
		}catch(Exception e){
			log.error("Exception in getNetworkProvinceSites method "+e.getMessage());
		}
		log.debug("NetworkMapManagerImpl * getNetworkProvinceSites * Exit the method");
		return null;
	}
}
