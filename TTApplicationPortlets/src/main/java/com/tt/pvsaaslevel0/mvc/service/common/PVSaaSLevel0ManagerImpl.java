package com.tt.pvsaaslevel0.mvc.service.common;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvsaaslevel0.mvc.dao.PVSaaSLevel0DaoImpl;
import com.tt.pvsaaslevel0.mvc.web.controller.PVSaaSLevel0Controller;


@Service(value = "pvSaaSLevel0ManagerImpl")
@Transactional
public class PVSaaSLevel0ManagerImpl {
	
	
	private final static Logger log = Logger.getLogger(PVSaaSLevel0ManagerImpl.class);

	private PVSaaSLevel0DaoImpl pvSaaSLevel0DaoImpl;
	
	@Autowired
	@Qualifier(value="pvSaaSLevel0DaoImpl")
	public void setPvSaaSLevel0DaoImpl(PVSaaSLevel0DaoImpl pvSaaSLevel0DaoImpl) {
		this.pvSaaSLevel0DaoImpl = pvSaaSLevel0DaoImpl;
	}
	
	
	
	public Map<String,Integer> getGraphData(String customerId){
		
		
		return pvSaaSLevel0DaoImpl.getGraphData(customerId);
	}
	

}
