package com.tt.pvsaaslevel0.mvc.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvsaaslevel0.mvc.service.common.PVSaaSLevel0ManagerImpl;




@Repository(value = "pvSaaSLevel0DaoImpl")
@Transactional
public class PVSaaSLevel0DaoImpl {
	
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private final static Logger log = Logger.getLogger(PVSaaSLevel0DaoImpl.class);
	
	
		public Map<String,Integer> getGraphData(String customerId){
			
			Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
			deliveryStageToColor.put(6, "#89C35C");
			deliveryStageToColor.put(5, "#FA8072");
			deliveryStageToColor.put(4, "#00B0F0");
			deliveryStageToColor.put(3, "#7030A0");
			deliveryStageToColor.put(2, "#A40800");
			deliveryStageToColor.put(1, "#000");
			
			
			Map<String,Integer> graphData=new HashMap<String, Integer>();
		

			
			
				
			
					
			
					Query innerQuery= sessionFactory.getCurrentSession().createSQLQuery("select min(c3.delivery_stage),bci,bcn from configurationitem c3, (select c2.dependentciid as dci, c2.baseciid as bci,c2.baseciname bcn  from cirelationship c2 where c2.baseciid in (select c1.ciid from configurationitem c1  where c1.rootProductId='SaaS' and c1.customeruniqueid='"+customerId+"' and c1.cmdbclass='SaaS Business Service')) c4 where c3.ciid = c4.dci and customeruniqueid='"+customerId+"' group by bci");
					List<Object[]> productStatusList=innerQuery.list();
					
					if(null!=productStatusList && productStatusList.size()>0){
						for (Object[] productData : productStatusList) {
							String productDeliveryStage=(String)productData[0];
							if(null!=productDeliveryStage && productDeliveryStage.length()>0){
								
								String deliveryColor=deliveryStageToColor.get(Integer.parseInt(productDeliveryStage));
								if(graphData.size()>0){
									
									Set<String> mapKeySet=graphData.keySet();
									
									if(mapKeySet.contains(deliveryColor)){
										
										int currentCount=graphData.get(deliveryColor);
										graphData.put(deliveryColor, currentCount+1);
									}else{
										
										graphData.put(deliveryColor, 1);
									}
									
								}else{
									
									graphData.put(deliveryColor, 1);	
								}
								
							}
						}
						
					}
					
						
				
					
					
				
					
					
				
				
				
				
			
		
		return graphData;
		}
	
	

}
