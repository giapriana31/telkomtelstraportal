package com.tt.cvsaassprintlevel0.mvc.service;

import com.tt.cvsaassprintlevel0.mvc.dao.CVSaasSprintLevel0Dao;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("cvSaasSprintLevel0Manager")
@Transactional
public class CVSaasSprintLevel0Manager
{
  private CVSaasSprintLevel0Dao cvSaasSprintLevel0Dao;
  
  @Autowired
  @Qualifier("cvSaasSprintLevel0Dao")
  public void setCVSaasSprintLevel0Dao(CVSaasSprintLevel0Dao cvSaasSprintLevel0Dao)
  {
    this.cvSaasSprintLevel0Dao = cvSaasSprintLevel0Dao;
  }
  
  /*public ArrayList<String> getServiceStatusSpecs(String customerId)
  {
    return this.cvSaasSprintLevel0Dao.getServiceStatusSpecs(customerId);
  }*/
  
  public Map<String,Integer> getGraphData(String customerId){
		return cvSaasSprintLevel0Dao.getGraphData(customerId);
	}
  
}
