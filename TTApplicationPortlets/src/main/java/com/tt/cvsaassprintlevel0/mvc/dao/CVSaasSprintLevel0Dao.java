package com.tt.cvsaassprintlevel0.mvc.dao;

import com.tt.utils.PropertyReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("cvSaasSprintLevel0Dao")
@Transactional
public class CVSaasSprintLevel0Dao
{
  @Autowired
  private SessionFactory sessionFactory;
  public static Properties prop = PropertyReader.getProperties();
  public Map<String,Integer> getGraphData(String customerId){
		
		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(4, "#a3cf62");
		deliveryStageToColor.put(3, "#ffff00");
		deliveryStageToColor.put(2, "#ff9c00");
		deliveryStageToColor.put(1, "#e32212");
		
		
		Map<String,Integer> graphData=new HashMap<String, Integer>();

		StringBuffer sb = new StringBuffer();
		sb.append(" select inc.priority,cfg.siteid from ");
        sb.append(" (SELECT a.delivery_stage,(case ");
		sb.append(" when b.productId like '%Whispir%' then 'Multi Channel Communication' ");
		sb.append(" when b.productId like '%Mandoe%' then 'Digital Proximity' ");
		sb.append(" when b.productId like '%IPScape%' then 'Cloud Contact Centre' ");
		sb.append(" else b.productId ");
		sb.append(" end) as productName,b.ciname,b.siteid from  ");
        sb.append(" (select ciid,delivery_stage from( ");
        sb.append(" SELECT ciid,delivery_stage FROM configurationitem WHERE customeruniqueid = '" + customerId + "' AND cmdbclass='saas Business Service' AND deliverystatus IN ('Operational')  ");
        sb.append(" order by ciid,delivery_stage desc ");
        sb.append(" ) a GROUP BY ciid)a LEFT JOIN ");
        sb.append(" configurationitem b ");
        sb.append(" ON a.ciid = b.ciid and a.delivery_stage = b.delivery_stage ");
        sb.append(" WHERE customeruniqueid = '" + customerId + "' AND cmdbclass='saas Business Service' ");
        sb.append(" order by productName,delivery_stage ");
        sb.append(" ) cfg left join (select * from( ");
        sb.append(" SELECT siteid,priority FROM incident  WHERE customeruniqueid = '" + customerId + "' ");
        sb.append(" order by siteid,priority asc ");
        sb.append(" ) a GROUP BY siteid) inc  ");
        sb.append(" on cfg.siteid = inc.siteid ");
        sb.append(" where inc.priority is not null ");
		
        Query innerQuery= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());

				List<Object[]> productStatusList=innerQuery.list();
				
				if(null!=productStatusList && productStatusList.size()>0){
					for (Object[] productData : productStatusList) {
						//String productDeliveryStage=(String)productData[0];
						String productDeliveryStage=String.valueOf(productData[0]);
						if(null!=productDeliveryStage && productDeliveryStage.length()>0){
							
							String deliveryColor=deliveryStageToColor.get(Integer.parseInt(productDeliveryStage));
							if(graphData.size()>0){
								
								Set<String> mapKeySet=graphData.keySet();
								
								if(mapKeySet.contains(deliveryColor)){
									
									int currentCount=graphData.get(deliveryColor);
									graphData.put(deliveryColor, currentCount+1);
								}else{
									
									graphData.put(deliveryColor, 1);
								}
								
							}else{
								
								graphData.put(deliveryColor, 1);	
							}
							
						}
					}
					
				}
				
	return graphData;
	}
  
  
  
}
