package com.tt.cvsaassprintlevel0.mvc.web.controller;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.cvsaassprintlevel0.mvc.service.CVSaasSprintLevel0Manager;
import com.tt.logging.Logger;
import com.tt.utils.TTGenericUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller("cvSaasSprintLevel0Controller")
@RequestMapping({"VIEW"})
public class CVSaasSprintLevel0Controller
{
  private CVSaasSprintLevel0Manager cvSaasSprintLevel0Manager;
  private final static Logger log = Logger.getLogger(CVSaasSprintLevel0Controller.class);
  
  @Autowired
  @Qualifier("cvSaasSprintLevel0Manager")
  public void setCVSaasSprintLevel0Manager(CVSaasSprintLevel0Manager cvSaasSprintLevel0Manager)
  {
    this.cvSaasSprintLevel0Manager = cvSaasSprintLevel0Manager;
  }
  
  @RenderMapping
  public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model)
    throws PortalException, SystemException
  {
	  HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(renderResponse);
	
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		System.out.println("cust: "+customerId);
		SubscribedService ss = new SubscribedService();

		int count = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_SAAS, customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_SAAS);
			return "unsubscribed_saas_service";
		}
		else
		{
			Map<String,Integer> graphDataMap=cvSaasSprintLevel0Manager.getGraphData(customerId);
			Set<String> graphDataKeys=graphDataMap.keySet();	
			Map<String, Integer> graphColorMap = new HashMap<String, Integer>();
			Gson gson = new Gson();
			List<String> colorList=new ArrayList<String>();

			colorList.add("#a3cf62");
			colorList.add("#e32212");
			colorList.add("#ff9c00");
			colorList.add("#ffff00");

		
			for (String string : colorList) 
			{
				if(null != graphDataMap && !graphDataMap.keySet().contains(string))
				{
					graphDataMap.put(string, 0);
				}	
			}
		
			String saasGraphDataJson = gson.toJson(graphDataMap);
			log.info("saasGraphDataJson -----"+saasGraphDataJson);
			String colorListJson=gson.toJson(colorList);
			ArrayList<String> graphBottom= new ArrayList<String>();
			
			graphBottom.add("Normal-#a3cf62");
			graphBottom.add("Priority 1-#e32212");
			graphBottom.add("Priority 2-#ff9c00");
			graphBottom.add("Priority 3-#ffff00");
			
			model.addAttribute("saasStatusGraphData", saasGraphDataJson);
			
			model.addAttribute("graphColorListSaasStatus", colorListJson);
			model.addAttribute("graphBottomListSaasStatus", graphBottom);
			
			return "cvsaassprintlevel0";
		}
    /*HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
    
    HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
    
    String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
    System.out.println("cust: " + customerId);
    SubscribedService ss = new SubscribedService();
    
    int count = SubscribedService.isSubscribedServices("SaaS", customerId);
    if (count == 0)
    {
      model.addAttribute("title", "SaaS");
      return "unsubscribed_saas_service";
    }
    ArrayList<String> serviceStatusSpecs = this.cvSaasSprintLevel0Manager.getServiceStatusSpecs(customerId);
    model.addAttribute("serviceStatusSpecs", serviceStatusSpecs);
    
    return "cvsaassprintlevel0";*/
	  
	
	  
  }
}
