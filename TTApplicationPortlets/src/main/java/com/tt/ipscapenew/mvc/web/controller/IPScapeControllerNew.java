package com.tt.ipscapenew.mvc.web.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.io.PrintWriter;
import java.text.ParseException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.ClientProtocolException;
import org.hibernate.validator.util.privilegedactions.GetConstructor;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.ipscape.mvc.model.CategoryAPI;
import com.tt.ipscape.mvc.model.IPScape;
import com.tt.ipscape.mvc.service.IpscapeServiceImpl;
import com.tt.ipscapenew.mvc.model.IpscapeChatBean;
import com.tt.ipscapenew.mvc.service.IpscapeServiceImplNew;
import com.tt.logging.Logger;
import com.tt.logticket.mvc.web.controller.TTLogIncidentController;
import com.tt.profileSetting.mvc.dao.ProfileSecurityDao;
import com.tt.profileSetting.mvc.service.ProfileSecurityManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletResponse;

import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.utils.TTGenericUtils;


@Controller("ipScapeControllerNew")
@RequestMapping("VIEW")
public class IPScapeControllerNew {

	Properties properties = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(IPScapeControllerNew.class);
	private static final String LEAD_TYPE = "Call Back - Queue";
	
	private final String CREATE_LEAD_API =  properties.getProperty("createLeadAPI").toString();
	private final String READ_CAMPAIGN_LIST_API =  properties.getProperty("readCampaignListAPI").toString();
	private final String API_KEY = properties.getProperty("apikey").toString();
	private final String IPSCAPE_ORGANISATION = properties.getProperty("ipscapeorganisation").toString();
	private final String AUTHORIZATION = properties.getProperty("authorization").toString();
	private final String IPSCAPE_LOGIN_API = properties.getProperty("ipscapeloginAPI").toString();
	private final String IPSCAPE_LIST_IDS = properties.getProperty("ipscapelistids").toString();
	private final Map<String, String> campaignAndListIds = new HashMap<String, String>(); 
	
	private IpscapeServiceImplNew ipscapeServiceImplnew = new IpscapeServiceImplNew();
	@Autowired
	@Qualifier("ipscapeServiceImplnew")
	public void setProfileSecurityManager(IpscapeServiceImplNew ipscapeServiceImplnew)
	{
		this.ipscapeServiceImplnew = ipscapeServiceImplnew;
	}

	@RenderMapping
	public String handleRenderRequest2(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException, IOException, JSONException {
		
		log.info("RenderMapping function called");
				
		String userName=null;
		String workContact=null;
		HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
		User user = UserLocalServiceUtil.getUser((Long)httpServletRequest.getSession().getAttribute(WebKeys.USER_ID));
		userName=user.getContact().getFullName();
		String firstName = user.getFirstName();
		String lastName = user.getLastName(); 
		String emailId = user.getEmailAddress();
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
		String customerName = ipscapeServiceImplnew.getCustomerName(customerId);
		String isExternalUser = TTGenericUtils.isExternal(user)+"";
		
		
		// TO GET THE COMPANY NAME
		//String companyName=(String)httpServletRequest.getSession().getAttribute("LIFERAY_SHARED_customerName_KEY");
		workContact=(String) user.getExpandoBridge().getAttribute("WorkContact");
		IpscapeChatBean ipscapeObj=new IpscapeChatBean();
		
		ipscapeObj.setTelephone(workContact);
		ipscapeObj.setFirstName(firstName);
		ipscapeObj.setLastName(lastName);
		ipscapeObj.setCompanyName(customerName);
		ipscapeObj.setEmailId(emailId);
		
				
		model.addAttribute("ipscapeObj", ipscapeObj);
		
		return "ipScapeChat";
	}
	
}