package com.tt.ipscapenew.mvc.dao;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;

@Service(value = "ipscapeDaoImplnew")
@Transactional
public class IpscapeDaoImplNew{
	
	private final static Logger log = Logger.getLogger(IpscapeDaoImplNew.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	
	public String getCustomerName(String customerID)
	{
		log.info("in getCustomerName() method");
		String customerName = null;
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"Select accountName from customers where customerUniqueId='" + customerID + "'");

		if (null != query.list() && !query.list().isEmpty())
		{

			customerName = (String) query.list().get(0);
		}
		log.info("Customer Name"+ customerName);
		return customerName;
	}
}
