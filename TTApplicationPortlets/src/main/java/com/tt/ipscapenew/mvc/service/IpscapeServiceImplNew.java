package com.tt.ipscapenew.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.bvdashboard.mvc.dao.BVDashboardDao;
import com.tt.bvdashboard.mvc.web.controller.BVDashBoardController;
import com.tt.ipscape.mvc.dao.IpscapeDaoImpl;
import com.tt.ipscape.mvc.service.IpscapeServiceImpl;
import com.tt.ipscapenew.mvc.dao.IpscapeDaoImplNew;
import com.tt.logging.Logger;
import com.tt.profileSetting.mvc.dao.ProfileSecurityDao;

@Service(value = "ipscapeServiceImplnew")
@Transactional
public class IpscapeServiceImplNew {
	
	private final static Logger log = Logger.getLogger(IpscapeServiceImpl.class);
	private IpscapeDaoImplNew ipscapeDaoImplnew = new IpscapeDaoImplNew();
	
	@Autowired
	@Qualifier("ipscapeDaoImplnew")
	public void setProfileSecurityDao(IpscapeDaoImplNew ipscapeDaoImplnew) {
		this.ipscapeDaoImplnew = ipscapeDaoImplnew;
	}
	
	
	public String getCustomerName(String customerID){
		String customerName = null;
		customerName = ipscapeDaoImplnew.getCustomerName(customerID);
		return customerName;
	}

}
