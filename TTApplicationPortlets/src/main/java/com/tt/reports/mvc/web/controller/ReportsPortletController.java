package com.tt.reports.mvc.web.controller;



import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.WeightedWord;
import com.liferay.portal.util.PortalUtil;
import com.sun.mail.iap.ByteArray;
import com.tt.logging.Logger;
import com.tt.model.SWReports;
import com.tt.reports.mvc.service.common.ReportsPortletManager;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;




@Controller("reportsPortlet")
@RequestMapping("VIEW")
public class ReportsPortletController {
	
	private HashMap<String, String> customerMapping=null;
    private final static Logger log = Logger.getLogger(ReportsPortletController.class);

	private ReportsPortletManager reportsPortletManager;
	
	
	@Autowired
	@Qualifier("reportsPortletManager")
	public void setReportsPortletManager(ReportsPortletManager reportsPortletManager) {
		this.reportsPortletManager = reportsPortletManager;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(renderResponse);
		customerMapping=reportsPortletManager.getCustDetails();
		//UpdateSWReports updateReports= new UpdateSWReports();
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		//String customerId="C008";
	//	addDocumentFromRepository();
		ArrayList<SWReports> reports=reportsPortletManager.getReportDetails(customerId);
		ArrayList<String> categoryList= new  ArrayList<String>();
		
		if (null!=reports && !reports.isEmpty()) {
			for (SWReports swReports : reports) {

				if (!categoryList.contains(swReports.getReportCategory())) {
					categoryList.add(swReports.getReportCategory());
				}

			}
			}
		request.setAttribute("categoryList", categoryList);
		request.setAttribute("reportDetails", reports);
	     //log.info("ReportsPortletController-handleRenderRequest()-"+"report has no data "+reports.isEmpty());


		
			
		return "reports";
	}
	

	

	@ResourceMapping(value="openSWReports")
	public void openSWReports(ResourceRequest rsRequest,
			ResourceResponse rsResponse, Model model) throws PortalException,
			SystemException {
		
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(rsRequest);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(rsResponse);
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		//String customerId = "C008";
	//	String docid = rsRequest.getParameter("docid");
		//String reportName=rsRequest.getParameter("reportName");
		String reportId= rsRequest.getParameter("reportId");
		System.out.println("Report Id: "+reportId);
		PortletSession session = rsRequest.getPortletSession();
	  //   session.setAttribute("reportName", reportName, PortletSession.APPLICATION_SCOPE);
	    // log.info("ReportsPortletController-openSWReports()-"+"Doc id "+docid+" report name "+reportName);
//		respondReportsContents(docid, rsResponse,customerId);

	     
	     //testImage(rsResponse);
	     
	     getReportImages(rsResponse, customerId, reportId);
	     
	}
	
	
	public void getReportImages(ResourceResponse rsResponse,String customerId,String reportId){
		System.out.println("Inside get report Images");
		System.out.println("Report ID: "+reportId);
		JSONArray reportImages= new JSONArray();
		List<Integer> reportImageId=reportsPortletManager.getReportImages(reportId);
		for (Integer id : reportImageId) {
			
			reportImages.add(id);
		}
		try {

			PrintWriter pw = rsResponse.getWriter();
			pw.print(reportImages);
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
	
	
	@ResourceMapping(value="getImage")
	public void getImageData(Model model,ResourceRequest rsRequest,ResourceResponse rsResponse){
		
		String imageId = rsRequest.getParameter("imageId");
	

		SerialBlob imageData=reportsPortletManager.getImageData(imageId);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		ServletOutputStream  outs =null;
		BufferedInputStream  bis = null; 
		BufferedOutputStream bos = null;
		rsResponse.setContentType("image/png");
		try {
			rsResponse.setContentLength((int) imageData.length());
		} catch (SerialException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HttpServletResponse response=PortalUtil.getHttpServletResponse(rsResponse);
		
		
		response.setHeader("Content-Disposition", "inline; filename=\"" + "image" + "\"");
		try {
		if(imageData!=null){
			System.out.println("blob not null");
			System.out.println("Length "+imageData.length());
		
			
		
			
		}else{
			
			System.out.println("blob null");
		}
		InputStream isr=imageData.getBinaryStream();

		outs =  (ServletOutputStream) rsResponse.getPortletOutputStream ();
			System.out.println("blob length "+imageData.length());
		
	    bis = new BufferedInputStream(isr);
	    bos = new BufferedOutputStream(outs);
	    byte[] buff = new byte[2048];
	    int bytesRead;
	    // Simple read/write loop.
	    while(-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
	        bos.write(buff, 0, bytesRead);
	    }
		}catch(Exception e)
		{
			e.printStackTrace();
			log.error("ReportsPortletController.java-getImageData()-Catch "+e.getMessage());
		
		} finally {
		    if (bis != null)
				try {
					bis.close();
				} catch (IOException e) {
					e.printStackTrace();
					log.error("ReportsPortletController.java-getImageData()-Catch "+e.getMessage());
				}
		    if (bos != null)
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
					log.error("ReportsPortletController.java-getImageData()-Catch "+e.getMessage());
				}
		}
	}
	
	
	
	
	
	public void respondReportsContents(String docId,ResourceResponse rsResponse,String customerId){
		
		// get instance of all KM Docs
				 log.info("ReportsPortletController-respondReportsContents()-Start");
				if (null != reportsPortletManager)

				{
					log.info("ReportsPortletController-respondReportsContents()-doc inst not available for id for "+docId);
				}

				// get docpath from docid

				HashMap<String, String> docDetails = reportsPortletManager
						.getDocPathByDocId(docId);
				

				if (null != docDetails) {
					log.info("ReportsPortletController-respondReportsContents()-doc details = "+ docDetails.get("reportType") + "path ="
							+ docDetails.get("reportPath"));
					
				} else {
					log.info("ReportsPortletController-respondReportsContents()-doc details not available for id "+docId);
					System.out.println("inside return!");
					return;
				}
				// output an HTML page
				
				
				 rsResponse.setContentType("text/html;charset=UTF-8");


					ServletOutputStream  outs =null;
			      
						try {
							
							 outs =  (ServletOutputStream) rsResponse.getPortletOutputStream ();
							 System.out.println("Inside out");
						} catch (IOException e) {
							log.error("ReportsPortletController.java-respondReportsContents()-Catch "+e.getMessage());
						}
						
						Properties prop = PropertyReader.getProperties();
						String customerName=customerMapping.get(customerId);
						//String customerName="Infosys";
					/*	File file=new File(prop.getProperty("SWReportsPath")+customerName+"//"+docDetails.get("reportCategory")+"//"
								+ docDetails.get("reportPath"));
					*/	HttpServletResponse response=PortalUtil.getHttpServletResponse(rsResponse);

						response.setHeader("Content-disposition","inline; filename=" +docDetails.get("reportName")+"."+docDetails.get("reportPath").substring(docDetails.get("reportPath").lastIndexOf(".")+1) );

						
			       
				rsResponse.setContentType(docDetails.get("reportType"));
				
				BufferedInputStream  bis = null; 
				BufferedOutputStream bos = null;
				try {
					System.out.println("Input Stream");
//				    InputStream isr=new FileInputStream(file);
					SerialBlob blob=	reportsPortletManager.getReportContentById(docId);
					if(blob!=null){
						System.out.println("blob not null");
						System.out.println("Length "+blob.length());
						
						
					}else{
						
						System.out.println("blob null");
					}
					InputStream isr=blob.getBinaryStream();
					
						System.out.println("blob length "+blob.length());
				    bis = new BufferedInputStream(isr);
				    bos = new BufferedOutputStream(outs);
				    byte[] buff = new byte[2048];
				    int bytesRead;
				    // Simple read/write loop.
				    while(-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				        bos.write(buff, 0, bytesRead);
				    }
				} 
				catch(Exception e)
				{
					log.error("ReportsPortletController.java-respondReportsContents()-Catch "+e.getMessage());
				
				} finally {
				    if (bis != null)
						try {
							bis.close();
						} catch (IOException e) {
							log.error("ReportsPortletController.java-respondReportsContents()-Catch "+e.getMessage());
						}
				    if (bos != null)
						try {
							bos.close();
						} catch (IOException e) {
							log.error("ReportsPortletController.java-respondReportsContents()-Catch "+e.getMessage());
						}
				}


				 log.info("ReportsPortletController-respondReportsContents()-end");

		
	}
	
}