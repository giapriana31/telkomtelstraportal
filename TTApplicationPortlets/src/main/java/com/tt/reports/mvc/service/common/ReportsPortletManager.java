package com.tt.reports.mvc.service.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.sql.rowset.serial.SerialBlob;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.SWReports;
import com.tt.reports.mvc.dao.ReportsDaoImpl;

@Service(value = "reportsPortletManager")
@Transactional
public class ReportsPortletManager {

	
	
	private ReportsDaoImpl reportsDaoImpl;
	
	private static HashMap<String, HashMap> docidToPath = null;

	
	
	@Autowired
	@Qualifier("reportsDaoImpl")
	public void setReportsDaoImpl(ReportsDaoImpl reportsDaoImpl) {
		this.reportsDaoImpl = reportsDaoImpl;
	}



	
	public SerialBlob getReportContentById(String reportId){
		
		
		
		System.out.println("inside manager");
		
		return reportsDaoImpl.getReportContentById(reportId);
		
		
	}
	
	public ArrayList<SWReports> getReportDetails(String customerId){
		
		
		ArrayList<SWReports> reportDetails=reportsDaoImpl.getReportDetails(customerId);
		
		if(null != reportDetails && !reportDetails.isEmpty()){
			System.out.println("Dao deport details not empty");
			
			docidToPath = new HashMap<String, HashMap>();
			
			for (SWReports swReports : reportDetails) {
				
				
				if(docidToPath.get(swReports.getReportId()) == null){
					System.out.println("inside if doc id null");
					
					HashMap<String, String> temHashMap= new HashMap<String, String>();
					temHashMap.put("reportName", swReports.getReportName());
					temHashMap.put("reportId", swReports.getReportId());
					temHashMap.put("reportCategory", swReports.getReportCategory());
					temHashMap.put("reportPath", swReports.getReportPath());
					temHashMap.put("reportType", swReports.getReportType());
					docidToPath.put(swReports.getReportId(), temHashMap);
					
					
					
				}
			}
		}
		
		return reportDetails;
		
		
		
	}
	
	public List<Integer> getReportImages(String reportId){
		
	
		return reportsDaoImpl.getReportImages(reportId);
	}
	
	
	public SerialBlob getImageData(String imageId){
		
		return reportsDaoImpl.getImageData(imageId);
	}
	
	public HashMap<String, String> getCustDetails(){
		
		
		
		return reportsDaoImpl.getCustDetails();
	}
	
	public HashMap getDocPathByDocId(String docId) {
		return docidToPath.get(docId);
	}}
