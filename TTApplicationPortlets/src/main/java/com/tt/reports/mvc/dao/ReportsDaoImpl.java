package com.tt.reports.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.SWReportImages;
import com.tt.model.SWReports;

@Repository(value = "reportsDaoImpl")
@Transactional
public class ReportsDaoImpl {
	@Autowired
	private SessionFactory sessionFactory;
	private final static Logger log = Logger.getLogger(ReportsDaoImpl.class);


	public HashMap<String , String> getCustDetails(){
		
		
		
		SQLQuery query=sessionFactory.getCurrentSession().createSQLQuery("Select customerUniqueId,accountName from customers");
		List<Object[]> customersDetails=query.list();
		HashMap<String, String> customerMapping=new HashMap<String, String>();
		if (!customersDetails.isEmpty() &&  customersDetails!=null) {
			for (Object[] objects : customersDetails) {
				
			customerMapping.put((String)objects[0], (String)objects[1]);	

			}
		}
		
		
		return customerMapping;
	}
		
	public ArrayList<SWReports> getReportDetails(String customerId) {
		System.out.println("Customer ID " + customerId);
		ArrayList<SWReports> reports = null;
		Query query = sessionFactory.getCurrentSession().createQuery(
				"From SWReports sr where sr.customerId=:customerId");
		query.setParameter("customerId", customerId);
		log.info("ReportsDaoImpl.java -> getReportDetails--> Query "
				+ query.getQueryString());

		if (query.list() != null && query.list().size() > 0) {

			reports = (ArrayList<SWReports>) query.list();
		}
		return reports;
	}

	public SerialBlob getReportContentById(String reportId) {
		System.out.println("Inside Dao");

		Query query = sessionFactory.getCurrentSession().createQuery(
				"from SWReports sr where sr.reportId=:reportId");
		query.setParameter("reportId", reportId);
		System.out.println("Query String " + query.getQueryString());
		SWReports reports2 = new SWReports();
		if (query.list() != null && query.list().size() > 0) {
			System.out.println("list is not empty");
			reports2 = (SWReports) query.list().get(0);
			System.out.println(reports2.getReportName());

		}
		SerialBlob blob = reports2.getReportData();
		if (blob != null) {
			System.out.println("Dao not null");
		} else {
			System.out.println("Dao  null");
		}

		return blob;

	}
	
	
	public List<Integer> getReportImages(String reportId){
		System.out.println("Get report Images dao");
		String count=null;
		List<Integer> reportImageId= new ArrayList<Integer>();
		Query query=sessionFactory.getCurrentSession().createSQLQuery("Select swi.imageid from swreportimages swi where swi.reportid=+'"+reportId+"'");
		
			if(null!=query.list() && !query.list().isEmpty()){
				System.out.println("Get report Images dao list size: "+query.list().size());

				
				for (Object imageId : query.list()) {
					
					reportImageId.add((Integer)imageId);
					
				}
				
			}
	
	return reportImageId;
	}
	
	public SerialBlob getImageData(String imageId){
		SerialBlob imageData=null;
		SWReportImages swi= new SWReportImages();
		Query query=sessionFactory.getCurrentSession().createQuery("from SWReportImages swi where swi.imageid='"+imageId+"'");
		if(null!=query.list() && !query.list().isEmpty()){
			
			
			swi=(SWReportImages)query.list().get(0);
		}
	return swi.getImage_data();
	}

}
