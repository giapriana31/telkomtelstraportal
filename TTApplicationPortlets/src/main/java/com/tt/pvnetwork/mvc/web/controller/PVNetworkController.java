package com.tt.pvnetwork.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.pvnetwork.mvc.service.common.PVNetworkManagerImpl;
import com.tt.util.RefreshUtil;
import com.tt.utils.TTGenericUtils;



@Controller("pvNetworkController")
@RequestMapping("VIEW")
public class PVNetworkController {
	
	private final static Logger log = Logger.getLogger(PVNetworkController.class);

	
	private PVNetworkManagerImpl pvNetworkManagerImpl;
	
	@Autowired
	@Qualifier(value="pvNetworkManagerImpl")
	public void setPVNetworkManagerImpl(PVNetworkManagerImpl pvNetworkManagerImpl) {
		this.pvNetworkManagerImpl = pvNetworkManagerImpl;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		
		log.info("PVNetworkController-render-start");

		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		
		int count = RefreshUtil.isTenossMaintenance(GenericConstants.PRODUCTTYPE_MAINTENACE, customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_MAINTENACE);
			return "no_pvtenoss_service";
		}
		else
		{
		
			Map<String,Integer> graphDataMap=pvNetworkManagerImpl.getGraphData(customerId);
			
			Set<String> graphDataKeys=graphDataMap.keySet();
			
			for (String string : graphDataKeys) 
			{
				log.info("PVNetworkController-graph data-"+"Graphkey: "+string+"  Data:"+graphDataMap.get(string));
			}

			Gson gson = new Gson();
			List<String> colorList=new ArrayList<String>();
			colorList.add("#89C35C");
			colorList.add("#F0990D");
			colorList.add("#FF0600");
			colorList.add("#00B0F0");
			colorList.add("#7030A0");
			colorList.add("#A40800");
			colorList.add("#000");
			
		
			for (String string : colorList) 
			{
				if(null != graphDataMap && !graphDataMap.keySet().contains(string))
				{
					graphDataMap.put(string, 0);
				}	
			}
		
			String pvNetworkGraphDataJson = gson.toJson(graphDataMap);
			
			String colorListJson=gson.toJson(colorList);
			ArrayList<String> graphBottom= new ArrayList<String>();
		//	HashMap<String, String> graphBottom=new HashMap<String, String>();
			
			
			graphBottom.add("Proposed-#000");
			graphBottom.add("Recommended-#A40800");
			graphBottom.add("Reviewed-#7030A0");
			graphBottom.add("Approved-#00B0F0");
			graphBottom.add("Cancelled-#FF0600");
			graphBottom.add("Waiters-#F0990D");
			graphBottom.add("Closed-#89C35C");
			
			log.info("PVNetworkController-render-Graphlist Json: "+pvNetworkGraphDataJson);
			log.info("PVNetworkController-render-colorlist Json: "+colorListJson);
			
			model.addAttribute("pvNetworkGraphData", pvNetworkGraphDataJson);
			
			model.addAttribute("graphColorList", colorListJson);
			model.addAttribute("graphBottomList", graphBottom);
			
			return "pvNetwork";
		}
		
	}
}
