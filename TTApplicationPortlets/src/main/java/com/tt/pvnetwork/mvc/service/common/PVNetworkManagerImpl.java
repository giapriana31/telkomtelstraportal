package com.tt.pvnetwork.mvc.service.common;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvnetwork.mvc.dao.PVNetworkDaoImpl;

@Service(value = "pvNetworkManagerImpl")
@Transactional
public class PVNetworkManagerImpl {
	
	private final static Logger log = Logger.getLogger(PVNetworkManagerImpl.class);

	private PVNetworkDaoImpl pvNetworkDaoImpl;
	
	@Autowired
	@Qualifier(value="pvNetworkDaoImpl")
	public void setPVNetworkDaoImpl(PVNetworkDaoImpl pvNetworkDaoImpl) {
		this.pvNetworkDaoImpl = pvNetworkDaoImpl;
	}
	
	
	public Map<String,Integer> getGraphData(String customerId){
		
		
		return pvNetworkDaoImpl.getGraphData(customerId);
	}

}
