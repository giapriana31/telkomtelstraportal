package com.tt.pvnetworklv1.mvc.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.pvnetworklv1.mvc.dao.PVNetworkLv1DaoImpl;
import com.tt.pvsaaslevel1.mvc.model.DrillDownDetails;

@Service(value = "pvNetworkLv1ManagerImpl")
@Transactional
public class PVNetworkLv1ManagerImpl {

	private PVNetworkLv1DaoImpl pvNetworkLv1DaoImpl;
	
	@Autowired
	@Qualifier(value="pvNetworkLv1DaoImpl")
	public void setPVNetworkLv1DaoImpl(PVNetworkLv1DaoImpl pvNetworkLv1DaoImpl) {
		this.pvNetworkLv1DaoImpl = pvNetworkLv1DaoImpl;
	}
	
	public List<DrillDownDetails> getDrillDownDetails(String customerId) {
		
		return pvNetworkLv1DaoImpl.getDrillDownDetails(customerId);
	}

}
