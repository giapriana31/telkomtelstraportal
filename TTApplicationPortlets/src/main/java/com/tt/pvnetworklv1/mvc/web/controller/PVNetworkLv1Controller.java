package com.tt.pvnetworklv1.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.tt.logging.Logger;
import com.tt.pvnetworklv1.mvc.service.common.PVNetworkLv1ManagerImpl;
import com.tt.pvsaaslevel1.mvc.model.DrillDownDetails;
import com.tt.utils.TTGenericUtils;

/**
 * Portlet implementation class PVNetworkLv1Controller
 * author : ahmadi.harahap
 */

@Controller("pvNetworkLv1Controller")
@RequestMapping("VIEW")
public class PVNetworkLv1Controller {
 
	private final static Logger log = Logger.getLogger(PVNetworkLv1Controller.class);
	
	private PVNetworkLv1ManagerImpl pvNetworkLv1ManagerImpl;
	
	@Autowired
	@Qualifier(value = "pvNetworkLv1ManagerImpl")
	public void setPVNetworkLv1ManagerImpl(PVNetworkLv1ManagerImpl pvNetworkLv1ManagerImpl){
		this.pvNetworkLv1ManagerImpl = pvNetworkLv1ManagerImpl;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		
		log.info("PVNetworkLv1Controller-render()");
		
		
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);
		
		ArrayList<String> graphBottom= new ArrayList<String>();
		
		graphBottom.add("Proposed-#000");
		graphBottom.add("Recommended-#A40800");
		graphBottom.add("Reviewed-#7030A0");
		graphBottom.add("Approved-#00B0F0");
		graphBottom.add("Cancelled-#FF0600");
		graphBottom.add("Waiters-#F0990D");
		graphBottom.add("Closed-#89C35C");
		
		model.addAttribute("graphBottomList", graphBottom);
		
		log.info("PVNetworkLv1Controller-render graphBottomList");
		
		return "pvNetworkLv1";
	}
	
	@ResourceMapping(value = "getNetworkDrillDownDetail")
	public void getDrillDownData(ResourceRequest request,ResourceResponse response){
		
		Map<String,String> nameToDeliveryStageMap= new HashMap<String, String>();
		nameToDeliveryStageMap.put("Proposed", "1");
		nameToDeliveryStageMap.put("Recommended", "2");
		nameToDeliveryStageMap.put("Reviewed", "3");
		nameToDeliveryStageMap.put("Approved", "4");
		nameToDeliveryStageMap.put("Cancelled", "5");
		nameToDeliveryStageMap.put("Waiters", "6");
		nameToDeliveryStageMap.put("Closed", "7");
		
		Map<Integer, Integer> progressPercent = new HashMap<Integer, Integer>();
		progressPercent.put(1, 14);
		progressPercent.put(2, 29);
		progressPercent.put(3, 43);
		progressPercent.put(4, 57);
		progressPercent.put(5, 100);
		progressPercent.put(6, 86);
		progressPercent.put(7, 100);
		
		log.info("PVNetworkLv1Controller-getDrillDownData()-start");

		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(response);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		
		List<DrillDownDetails> drillDetailList=pvNetworkLv1ManagerImpl.getDrillDownDetails(customerId);
		
		JSONObject objectList;
		JSONArray drillDownArray= new JSONArray();

		if (null!=drillDetailList) {
			for (DrillDownDetails drillDownDetails : drillDetailList) {
				log.info("PVNetworkLv1Controller-getDrillDownData() Color: " + drillDownDetails.getDeliveryStage()+ " Name: " + drillDownDetails.getCiname());
			}
			LinkedHashMap<String, String> drillDownMap = new LinkedHashMap<String, String>();
		
			for (DrillDownDetails drillDownDetails : drillDetailList) {
				JSONObject tempJsonObj = new JSONObject();
				tempJsonObj.put(drillDownDetails.getCiname()+"***"+drillDownDetails.getDeliveryStage()+"***"+progressPercent.get(Integer.parseInt(drillDownDetails.getDeliveryStage())), drillDownDetails.getDeliveryStage());
				drillDownArray.add(tempJsonObj);
				drillDownMap.put(drillDownDetails.getCiname()+"***"+drillDownDetails.getDeliveryStage()+"***"+progressPercent.get(Integer.parseInt(drillDownDetails.getDeliveryStage())), drillDownDetails.getDeliveryStage());
			}
			
			ArrayList<String> keys= new  ArrayList<String>();
			keys.addAll(drillDownMap.keySet());
			
			objectList= new JSONObject();
			
			for (int i = 0; i < keys.size(); i++) {
				objectList.put(keys.get(i), drillDownMap.get(keys.get(i)));
			}
			
			log.info("PVNetworkLv1Controller-getDrillDownData() Drill Down Json: " + objectList);
		}else{			
			objectList = new JSONObject();			
		}
		PrintWriter out = null;
		try {
			out=response.getWriter();
		} catch (IOException e) {
			log.error("PVNetworkLv1Controller-getDrillDownData()-catch: "+e.getMessage());
		}
		
		//out.print(objectList.toString());
		out.print(drillDownArray.toString());
		log.info("PVNetworkLv1Controller-getDrillDownData()-end");
	}
}
