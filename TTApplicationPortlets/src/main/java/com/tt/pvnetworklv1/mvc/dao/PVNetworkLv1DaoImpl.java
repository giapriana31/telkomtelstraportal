package com.tt.pvnetworklv1.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.pvsaaslevel1.mvc.model.DrillDownDetails;

@Repository(value = "pvNetworkLv1DaoImpl")
@Transactional
public class PVNetworkLv1DaoImpl {

	@Autowired
	private SessionFactory sessionFactory;
	
	public List<DrillDownDetails> getDrillDownDetails(String customerid) {
		/**
		Map<Integer, String> deliveryStageToColor = new HashMap<Integer, String>();
		deliveryStageToColor.put(7, "#F0990D");
		deliveryStageToColor.put(6, "#89C35C");
		deliveryStageToColor.put(5, "#FA8072");
		deliveryStageToColor.put(4, "#00B0F0");
		deliveryStageToColor.put(3, "#7030A0");
		deliveryStageToColor.put(2, "#A40800");
		deliveryStageToColor.put(1, "#000");
		
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(
				"select min(c3.delivery_stage),bci,bcn from configurationitem c3, (select c2.dependentciid as dci, c2.baseciid as bci,c2.baseciname bcn  from cirelationship c2 where c2.baseciid in (select c1.ciid from configurationitem c1  where c1.rootProductId='SaaS' and c1.customeruniqueid='"
						+ customerid
						+ "' and c1.cmdbclass='SaaS Business Service')) c4 where c3.ciid = c4.dci and customeruniqueid='"
						+ customerid + "' group by bci");
		**/
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select task_stage_tenoss, ciname from ten2infi where CustomerUniqueId = '"+customerid+"'");
		//Query query = sessionFactory.getCurrentSession().createSQLQuery("select task_stage_tenoss, ciname from ten2infi where Task_Stage_TENOSS not in ('5', '7') and CustomerUniqueId = '"+customerid+"'");
		
		List<DrillDownDetails> dataList = new ArrayList<DrillDownDetails>();
		List<Object[]> drillDownList = query.list();

		if (null != drillDownList && drillDownList.size() > 0) {

			for (Object[] data : drillDownList) {
				String deliveryStage = (String) data[0];
				if (null != deliveryStage && deliveryStage.length() > 0) {
					System.out.println("Inside Inner");
					System.out.println("Delivery :" + deliveryStage);
					DrillDownDetails details = new DrillDownDetails();
					details.setCiname((String) data[1]);
					details.setDeliveryStage((String) data[0]);
					//details.setDeliveryStage(deliveryStageToColor.get(Integer.parseInt((String) data[0])));
					System.out.println("Dta2: " + details.getCiname());
					System.out.println("Dta3: " + details.getDeliveryStage());
					dataList.add(details);
				}
			}
		}
		return dataList;
	}
}