package com.tt.cvsaassprintlevel1.mvc.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value ="cvSaasSprintLevel1Dao")
@Transactional
public class CVSaasSprintLevel1Dao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	public String getSaasProduct(String customerId){
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		StringBuffer sb = new StringBuffer();
		sb.append(" select inc.priority,cfg.productName,cfg.ciname from ");
        sb.append(" (SELECT a.delivery_stage,(case ");
		sb.append(" when b.productId like '%Whispir%' then 'Multi Channel Communication' ");
		sb.append(" when b.productId like '%Mandoe%' then 'Digital Proximity' ");
		sb.append(" when b.productId like '%IPScape%' then 'Cloud Contact Centre' ");
		sb.append(" else b.productId ");
		sb.append(" end) as productName,b.ciname,b.siteid from  ");
        sb.append(" (select ciid,delivery_stage from( ");
        sb.append(" SELECT ciid,delivery_stage FROM configurationitem WHERE customeruniqueid = '" + customerId + "' AND cmdbclass='saas Business Service' AND deliverystatus IN ('Operational')  ");
        sb.append(" order by ciid,delivery_stage desc ");
        sb.append(" ) a GROUP BY ciid)a LEFT JOIN ");
        sb.append(" configurationitem b ");
        sb.append(" ON a.ciid = b.ciid and a.delivery_stage = b.delivery_stage ");
        sb.append(" WHERE customeruniqueid = '" + customerId + "' AND cmdbclass='saas Business Service' ");
        sb.append(" order by productName,delivery_stage ");
        sb.append(" ) cfg left join (select * from( ");
        sb.append(" SELECT siteid,priority FROM incident  WHERE customeruniqueid = '" + customerId + "' ");
        sb.append(" order by siteid,priority asc ");
        sb.append(" ) a GROUP BY siteid) inc  ");
        sb.append(" on cfg.siteid = inc.siteid ");
        sb.append(" where inc.priority is not null ");
		
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] productlist : (List<Object[]>)query.list()) {
				if (productlist[0]!=null) {
					obj = new JSONObject();
					obj.put("priority", productlist[0].toString());
					obj.put("productname", productlist[1].toString());
					obj.put("ciname", productlist[2].toString());
					obj.put("colour", colorNameToCode(productlist[0].toString()));
					JSONObjArray.add(obj);
				}
				
			}
		}
		return JSONObjArray.toString();
	}
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("4"))
			return "background-color:rgb(163, 207, 98)";
		else if (color.equalsIgnoreCase("3"))
			return "background-color:rgb(255,255,0)";
		else if (color.equalsIgnoreCase("2"))
			return "background-color:rgb(255, 156, 0)";
		else if (color.equalsIgnoreCase("1"))
			return "background-color:rgb(227, 34, 18)";
		else
			return "background-color:rgb(204,204,204)";
	}
	
}
