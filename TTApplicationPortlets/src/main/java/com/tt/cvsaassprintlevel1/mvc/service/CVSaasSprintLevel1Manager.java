package com.tt.cvsaassprintlevel1.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.cvsaassprintlevel1.mvc.dao.CVSaasSprintLevel1Dao;

@Service(value = "cvSaasSprintLevel1Manager")
@Transactional
public class CVSaasSprintLevel1Manager {

	private CVSaasSprintLevel1Dao cvSaasSprintLevel1Dao;

	@Autowired
	@Qualifier("cvSaasSprintLevel1Dao")
	public void setCvSaasSprintLevel1Dao(CVSaasSprintLevel1Dao cvSaasSprintLevel1Dao) {
		this.cvSaasSprintLevel1Dao = cvSaasSprintLevel1Dao;
	}
	
	public String getSaasProduct(String customerId){
		return cvSaasSprintLevel1Dao.getSaasProduct(customerId);
	}
	
}
