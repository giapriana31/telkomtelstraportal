package com.tt.cvsaassprintlevel1.mvc.web.controller;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.cvsaassprintlevel1.mvc.service.CVSaasSprintLevel1Manager;
import com.tt.logging.Logger;
import com.tt.utils.TTGenericUtils;

@Controller("cvSaasSprintLevel1Controller")
@RequestMapping("VIEW")
public class CVSaasSprintLevel1Controller {

	private CVSaasSprintLevel1Manager cvSaasSprintLevel1Manager;
	private final static Logger log = Logger.getLogger(CVSaasSprintLevel1Controller.class);
	
	@Autowired
	@Qualifier("cvSaasSprintLevel1Manager")
	public void setCvSaasSprintLevel1Manager(CVSaasSprintLevel1Manager cvSaasSprintLevel1Manager) {
		this.cvSaasSprintLevel1Manager = cvSaasSprintLevel1Manager;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse Response, Model model) throws PortalException,SystemException 
	{
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(Response));
		log.debug("CVSaasProductLevel1 ----- handleRenderRequest CustomerID "+customerUniqueID);
		log.debug("CVSaasProductLevel1--------------->"+ cvSaasSprintLevel1Manager.getSaasProduct(customerUniqueID));
		model.addAttribute("CVSaasProductLevel1",cvSaasSprintLevel1Manager.getSaasProduct(customerUniqueID));
		
		log.debug("CVSaasProductLevel1 ----- return from handleRenderRequest");
		return "cvsaassprintlevel1";
		
	}
	
}
