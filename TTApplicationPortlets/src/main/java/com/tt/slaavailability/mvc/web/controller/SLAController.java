package com.tt.slaavailability.mvc.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.model.NetworkAvailability;
import com.tt.model.SaaSAvailability;
import com.tt.slaavailability.mvc.service.SLAAvailabilityManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller(value = "slaController")
@RequestMapping("VIEW")
public class SLAController {

	private SLAAvailabilityManagerImpl slaAvailabilityManagerImpl;
	private final static Logger log = Logger.getLogger(SLAController.class);

	@Autowired
	@Qualifier("slaAvailabilityManagerImpl")
	public void setSlaAvailabilityManagerImpl(
			SLAAvailabilityManagerImpl slaAvailabilityManagerImpl) 
	{
		this.slaAvailabilityManagerImpl = slaAvailabilityManagerImpl;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse response, Model model) 
	{
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(response);
		String customerID = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);
		
		NetworkAvailability availability= slaAvailabilityManagerImpl.getNetworkDetails(customerID);
		request.setAttribute("networkAvailability", availability);
		
		/* SaaS Dashboard Availability Code */
		String colorAvail = slaAvailabilityManagerImpl.getSaaSDashboardColor(customerID);
		String[] colorAvgAvail = colorAvail.split("@");
		log.debug("Availbility : " + colorAvgAvail[0] + " --- " + "color :: " + colorAvgAvail[1]);
		model.addAttribute("SaaSAvailability", colorAvgAvail[0]);
		model.addAttribute("SaaSColor", colorAvgAvail[1]);
		
		
		JSONArray saaSAvailability = slaAvailabilityManagerImpl.getSaaSProductAvailability(customerID);
		log.debug("final saas json :: " + saaSAvailability);
		model.addAttribute("SaaSData", saaSAvailability);
	
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		if ((themeDisplay.getURLCurrent().contains("/sladetails") && themeDisplay
				.getURLCurrent().contains("p_p_state=maximized"))
				|| themeDisplay.getURLCurrent().endsWith("/sladetails")) 
		{
			String iaasDashboardAverage= slaAvailabilityManagerImpl.getIaaSDashBoardColor(customerID);
			String iaasColor=null;
				if (null!=iaasDashboardAverage) {
				
				double iaasAverage = Double.parseDouble(iaasDashboardAverage);
				
				if (iaasAverage >= 98.9) {

					iaasColor = "green";
				} else if (iaasAverage > 98.5 && iaasAverage < 98.9) {

					iaasColor = "amber";

				} else if (iaasAverage <= 98.5) {

					iaasColor = "red";

				}
			}
			else{
				
				iaasColor = "grey";
			}
			model.addAttribute("IaaSColorAverage", iaasDashboardAverage);
			model.addAttribute("IaaSColor", iaasColor);
			
			return "sladetailspage";
		} 
		else 
		{
			
			String tempString=slaAvailabilityManagerImpl.getSLADashboardDetails(customerID, 0.0, "");
			String sitesDashboardDetails=slaAvailabilityManagerImpl.getSitesDashboardDetails(customerID, 0.0, "");
			
			
			if (sitesDashboardDetails!=null) {
				String[] strings = sitesDashboardDetails.split("######");
				if(Double.parseDouble(strings[0])==999.0){
					
					model.addAttribute("checkDataSites", false);
				}
				else{
				
				model.addAttribute("sitesPercentage", strings[0]);
				model.addAttribute("SitesColor", strings[1]);
				model.addAttribute("checkDataSites", true);
				
				System.out.println("Controler String  Sites"+strings[0]+strings[1]);
				}
			}
			else{
				
				model.addAttribute("checkDataSites", false);
				
			}
			
			
			if (tempString!=null) {
				String[] strings = tempString.split("######");
				if(Double.parseDouble(strings[0])==999.0){
					
					model.addAttribute("checkDataSites", false);
				}
				else{
				
				model.addAttribute("remPercentage", 100.0-Double.parseDouble(strings[0]));
				model.addAttribute("percentage", strings[0]);
				model.addAttribute("graphColor", strings[1]);
				log.info("percentage ---color -> "+strings[0]+"----"+strings[1]);

				}
				/*model.addAttribute("remPercentage",40);
				model.addAttribute("percentage", 60);
				model.addAttribute("graphColor", "red");*/
				
				
				
			}
			
			
			
			/* code to show IaaS color on dashboard */
			String iaasColor=null;

			String iaasDashboardAverage= slaAvailabilityManagerImpl.getIaaSDashBoardColor(customerID);
			if (null!=iaasDashboardAverage) {
				
				double iaasAverage = Double.parseDouble(iaasDashboardAverage);
				
				if (iaasAverage >= 98.9) {

					iaasColor = "green";
				} else if (iaasAverage > 98.5 && iaasAverage < 98.9) {

					iaasColor = "amber";

				} else if (iaasAverage <= 98.5) {

					iaasColor = "red";

				}
			}
			else{
				
				iaasColor = "grey";
			}
			
			
			if(!(null!=tempString) && (null!=iaasDashboardAverage)&&(null!=availability)){
				
				model.addAttribute("checkData", false);

				
			}else{
				
				model.addAttribute("checkData", true);

			}
			
			model.addAttribute("IaaSColor", iaasColor);

			return "sladashboard";
		}

	}

	/*@ResourceMapping(value = "getPercentage")
	public void getPercentage(ResourceRequest request, ResourceResponse response)
			throws IOException {
		log.info("SLAController.java -> in getPercentage()");
		HttpServletRequest servletReq = PortalUtil
				.getOriginalServletRequest(PortalUtil
						.getHttpServletRequest(request));
		String processData = slaAvailabilityManagerImpl.processData(servletReq);
		log.debug("SLAController.java -> " + processData);
		response.getWriter().println(processData);
	}*/

	@ResourceMapping(value = "getSlaDetails")
	public void getSlaDetails(ResourceRequest request, ResourceResponse response)
			throws IOException {
		log.info("SLAController.java -> in getSlaDetails()");
		HttpServletRequest servletReq = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		
		String processData = slaAvailabilityManagerImpl.getProcessDataSLADetails(servletReq);
		
		log.debug("SLAController.java -> " + processData);
		response.getWriter().println(processData);
	}
	/*
	@ResourceMapping(value = "getCloudAvailability")
	public void getCloudAvailabilityData(ResourceRequest request, ResourceResponse response) throws IOException
	{
		log.info("Class: SLAController, Method: getCloudAvailabilityData Start");
		
		HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);
		String customerId = TTGenericUtils.getCustomerIDFromSession(servletRequest, servletResponse);
		
		JSONArray jsonObj = slaAvailabilityManagerImpl.getCloudAvailability(customerId);
		response.getWriter().print(jsonObj);
		
	}*/
}
