package com.tt.slaavailability.mvc.service;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.NetworkAvailability;
import com.tt.model.SaaSAvailability;
import com.tt.slaavailability.mvc.dao.SLAAvailabilityDaoImpl;

@Service(value = "slaAvailabilityManagerImpl")
@Transactional
public class SLAAvailabilityManagerImpl {

    private SLAAvailabilityDaoImpl slaAvailabilityDaoImpl;
    private final static Logger log = Logger.getLogger(SLAAvailabilityManagerImpl.class);
    
    @Autowired
    @Qualifier("slaAvailabilityDaoImpl")
    public void setSlaAvailabilityDaoImpl(
	    SLAAvailabilityDaoImpl slaAvailabilityDaoImpl) {
	this.slaAvailabilityDaoImpl = slaAvailabilityDaoImpl;
    }

    public double per = 0.0;
    public String color = "";
    public String sites = "";
    
    public NetworkAvailability getNetworkDetails(String customerId)
    {
    	log.info("SLAAvailabilityManagerImpl.java -> in getNetworkDetails()");
    	NetworkAvailability availability=slaAvailabilityDaoImpl.getNetworkDetails(customerId);
    	return availability;
    }
    
    public String getSLADashboardDetails(String customerId,double per,String color){
   	 log.info("SLAAvailabilityManagerImpl.java -> in getSLADashboardDetails()");

    	String dashboardDetails=slaAvailabilityDaoImpl.calculate(customerId, per, color);
    	
    	
    	
    	return dashboardDetails;
    	
    }
    
    
    public String getSitesDashboardDetails(String customerId,double per,String color){
      	 log.info("SLAAvailabilityManagerImpl.java -> in getSitesDashboardDetails()");

    	String detailsString=slaAvailabilityDaoImpl.getSitesDashboardDetails(customerId, per, color);
    	return detailsString;
    	
    }

    public String processData(HttpServletRequest request) throws IOException {
    log.info("SLAAvailabilityManagerImpl.java -> in processData()");
    log.debug("SLAAvailabilityManagerImpl.java -> userID "+request.getParameter("userId"));
    log.debug("SLAAvailabilityManagerImpl.java -> isIE "+request.getParameter("isIE"));
    log.debug("SLAAvailabilityManagerImpl.java -> length "+request.getParameter("length"));
	
    if (request.getParameter("length") != null && request.getParameter("isIE") != null) 
	{
	    String code = "";
	    
	    if (request.getParameter("userId") != null) {
	    	
		String returnData = slaAvailabilityDaoImpl.calculate(request.getParameter("userId").toString(), per, color);
		
		per = Double.parseDouble(returnData.split("######")[0]);
		color =	returnData.split("######")[1];
		
		if (per != 999) {
		    if (request.getParameter("isIE").toString().equals("1"))
			code = getGraph(
				per,
				10,
				4,
				9,
				Double.parseDouble(request.getParameter(
					"length").toString()), 0.5);

		    if (request.getParameter("isIE").toString().equals("0"))
			code = getGraph(
				per,
				10,
				4,
				18,
				Double.parseDouble(request.getParameter(
					"length").toString()), 0.5);
		    
		}
	    }

	    else if (request.getParameter("per") != null) {
		if (request.getParameter("isIE").toString().equals("1"))
		    code = getGraph(Double.parseDouble(request
			    .getParameter("per").toString().replace("%", "")),
			    10, 4, 9, Double.parseDouble(request.getParameter(
				    "length").toString()), 0.5);

		if (request.getParameter("isIE").toString().equals("0"))
		    code = getGraph(Double.parseDouble(request
			    .getParameter("per").toString().replace("%", "")),
			    10, 4, 18, Double.parseDouble(request.getParameter(
				    "length").toString()), 0.5);
	    }

	    if (per != 999) {
	    	log.debug("SLAAvailabilityManagerImpl.java -> values = "+new DecimalFormat("0.00").format(per) + "%" + "@@@@@"+ code + "@@@@@" + color);
		return new DecimalFormat("0.00").format(per) + "%" + "@@@@@"
			+ code + "@@@@@" + color;
	    } else {
	    	log.debug("SLAAvailabilityManagerImpl.java -> values = no data");
		return "no data";
	    }
	}

	return "";
    }

    public static String getGraph(double raw_per, int top, int total_bar,
	    int total_height, double total_width_actual,
	    double bar_end_space_per) {
	double bar_end_space_pix = total_width_actual * bar_end_space_per / 100;
	double total_width = total_width_actual
		- (total_width_actual * bar_end_space_per * total_bar / 100);
	double per = raw_per * total_width / 100;

	StringBuilder str = new StringBuilder();

	String complete_color = "rgb(163, 207, 98)";
	String white_bar_color = "rgb(255, 255, 255)";
	String remaining_color = "rgb(204, 204, 204)";
	String fill_color = "";

	if (raw_per <= 25)
	    complete_color = "rgb(227, 34, 18)";
	else if (raw_per > 25 && raw_per <= 75)
	    complete_color = "rgb(255, 156, 0)";
	else
	    complete_color = "rgb(163, 207, 98)";

	double single_bar_size = total_width / total_bar;

	if (per > 0)
	    fill_color = complete_color;
	else
	    fill_color = remaining_color;

	str.append("<svg width='" + total_width_actual + "' height='"
		+ total_height + "'>");

	for (int i = 1; i <= total_bar; i++) {
	    double current_bar_start = (single_bar_size * (i - 1))
		    + (bar_end_space_pix * (i - 1));
	    double current_bar_end = single_bar_size * i
		    + (bar_end_space_pix * (i - 1));
	    double white_start = current_bar_end;
	    double white_end = white_start + bar_end_space_pix;
	    if (per >= current_bar_start && per <= current_bar_end) {
		str.append("<g style='fill: " + fill_color + ";'>");
		str.append("<rect x='" + current_bar_start + "' y='" + top
			+ "' height='" + total_height + "' width='"
			+ (per - current_bar_start) + "'></rect>");
		str.append("</g>");
		fill_color = remaining_color;
		str.append("<g style='fill: " + fill_color + ";'>");
		str.append("<rect x='" + per + "' y='" + top + "' height='"
			+ total_height + "' width='" + (current_bar_end - per)
			+ "'></rect>");
		str.append("</g>");
	    } else {
		str.append("<g style='fill: " + fill_color + ";'>");
		str.append("<rect x='" + current_bar_start + "' y='" + top
			+ "' height='" + total_height + "' width='"
			+ (current_bar_end - current_bar_start) + "'></rect>");
		str.append("</g>");
	    }
	    str.append("<g style='fill: " + white_bar_color + ";'>");
	    str.append("<rect x='" + white_start + "' y='" + top + "' height='"
		    + total_height + "' width='" + (white_end - white_start)
		    + "'></rect>");
	    str.append("</g>");
	    per = per + bar_end_space_pix;
	}
	str.append("</svg>");
	log.debug("SLAAvailabilityManagerImpl.java -> HTML code = "+str.toString());
	return str.toString();
    }

    public String getProcessDataSLADetails(HttpServletRequest request) throws IOException
    {
    	processData(request);
    	return slaAvailabilityDaoImpl.processDataSLADetails(request, 0.0, "");
    }
    
   /* public JSONArray getCloudAvailability(String customerId)
    {
    	log.info("Class: SLAAvailabilityManagerImpl, Method:getCloudAvailability Start ");
    	JSONArray finalObject = JSONFactoryUtil.createJSONArray();
    	JSONObject jsonObject;
    	
    	List cloudAvailabilityData = slaAvailabilityDaoImpl.getCloudAvailability(customerId);
    	if(cloudAvailabilityData != null && !cloudAvailabilityData.isEmpty())
    	{
    		for(Object row : cloudAvailabilityData)
    		{
    			jsonObject = JSONFactoryUtil.createJSONObject();
    			
    			Object[] dataObject = (Object[]) row;
    			
    			log.debug("ciname :: " + dataObject[0].toString());
    			log.debug("Availability :: " + dataObject[1].toString());
    			log.debug("Color :: " + dataObject[2].toString());
    			
    			jsonObject.put("Ciname", dataObject[0].toString());
    			jsonObject.put("Availability", dataObject[1].toString());
    			jsonObject.put("Color", dataObject[2].toString());
    			
    			finalObject.put(jsonObject);
    		}
    		
    		
    	}
    	else
    	{
			jsonObject = JSONFactoryUtil.createJSONObject();
			jsonObject.put("Availability", "no data");

			finalObject.put(jsonObject);


    		
    	}
    	log.info("Json Object :: " + finalObject.toString());
    	
    	log.info("Class: SLAAvailabilityManagerImpl, Method:getCloudAvailability End ");
		return finalObject;
    }*/
    
    public String getIaaSDashBoardColor(String customerId)
    {
    	return slaAvailabilityDaoImpl.getIaaSDashBoardColor(customerId);
    }

	public String getSaaSDashboardColor(String customerID)
	{
		Object[] colorAvail = slaAvailabilityDaoImpl.getSaaSDashboardColor(customerID);
		
		StringBuilder colorAvgAvail = new StringBuilder(); 
		if(colorAvail != null && colorAvail.length != 0)
		{
			colorAvgAvail.append(colorAvail[0].toString());
			colorAvgAvail.append("@");
			colorAvgAvail.append(colorAvail[1].toString());
		}
		else
		{
			colorAvgAvail.append("NA");
			colorAvgAvail.append("@");
			colorAvgAvail.append("Grey");
		
		}
		
		log.debug("Dashboard data from service layer :: " + colorAvgAvail.toString());
		return colorAvgAvail.toString();
	}

	public JSONArray getSaaSProductAvailability(String customerID)
	{
		List<SaaSAvailability> saaSAvailabilityList = slaAvailabilityDaoImpl.getSaaSProductDetails(customerID);
		JSONArray jsonData = JSONFactoryUtil.createJSONArray();
		JSONObject jsonObj = null;
		
		if(saaSAvailabilityList != null && !saaSAvailabilityList.isEmpty())
		{
			for(SaaSAvailability saas : saaSAvailabilityList)
			{
				jsonObj = JSONFactoryUtil.createJSONObject();
				jsonObj.put("name", saas.getProductname());
				jsonObj.put("avail", saas.getAvailability());
				jsonObj.put("color", saas.getColor());
				jsonData.put(jsonObj);
			}
		}
		return jsonData;
	}
}
