package com.tt.slaavailability.mvc.model;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tt.logging.Logger;

public class slaDashboard 
{
    public double per = 0.0;
    public String color = "";
    public String sites = "";
    private final static Logger log = Logger.getLogger(slaDashboard.class);
    
    @Autowired
    private SessionFactory sessionFactory;

    public String processData(HttpServletRequest request) throws IOException 
    {
    	log.info("slaDashboard.java -> in processData()");
    	log.debug("slaDashboard.java -> length = "+request.getParameter("length"));
	    log.debug("slaDashboard.java -> isIE = "+request.getParameter("isIE"));
	    log.debug("slaDashboard.java -> userID = "+request.getParameter("userId"));
	    
		if (request.getParameter("length") != null && request.getParameter("isIE") != null) 
		{
		    String code = "";		    
		    
		    if (request.getParameter("userId") != null) 
		    {
				calculate(request.getParameter("userId").toString());
				if (per != 999) 
				{
				    if (request.getParameter("isIE").toString().equals("1"))
					code = getGraph(per,10,4,9,Double.parseDouble(request.getParameter("length").toString()), 0.5);
		
				    if (request.getParameter("isIE").toString().equals("0"))
					code = getGraph(per,10,4,18,Double.parseDouble(request.getParameter("length").toString()), 0.5);
				}
		    }
	
		    else if (request.getParameter("per") != null) 
		    {
				if (request.getParameter("isIE").toString().equals("1"))
				    code = getGraph(Double.parseDouble(request.getParameter("per").toString().replace("%", "")),10, 4, 9, Double.parseDouble(request.getParameter("length").toString()), 0.5);
		
				if (request.getParameter("isIE").toString().equals("0"))
				    code = getGraph(Double.parseDouble(request.getParameter("per").toString().replace("%", "")),10, 4, 18, Double.parseDouble(request.getParameter("length").toString()), 0.5);
		    }
	
		    if (per != 999) 
		    {
		    	log.debug("slaDashboard.java -> value = "+ new DecimalFormat("0.00").format(per) + "%" + "@@@@@"+ code + "@@@@@" + color);
		    	return new DecimalFormat("0.00").format(per) + "%" + "@@@@@"+ code + "@@@@@" + color;
		    } 
		    else 
		    {
		    	log.debug("slaDashboard.java -> value = "+ new DecimalFormat("0.00").format(per) + "%" + "@@@@@"+ code + "@@@@@" + color);
		    	return "no data";
		    }
		}

		return "";
    }

    public static String getGraph(double raw_per, int top, int total_bar,int total_height, double total_width_actual,double bar_end_space_per) 
    {
		double bar_end_space_pix = total_width_actual * bar_end_space_per / 100;
		double total_width = total_width_actual
			- (total_width_actual * bar_end_space_per * total_bar / 100);
		double per = raw_per * total_width / 100;
	
		StringBuilder str = new StringBuilder();
	
		String complete_color = "rgb(163, 207, 98)";
		String white_bar_color = "rgb(255, 255, 255)";
		String remaining_color = "rgb(204, 204, 204)";
		String fill_color = "";
	
		if (raw_per <= 25)
		    complete_color = "rgb(227, 34, 18)";
		else if (raw_per > 25 && raw_per <= 75)
		    complete_color = "rgb(255, 156, 0)";
		else
		    complete_color = "rgb(163, 207, 98)";
	
		double single_bar_size = total_width / total_bar;
	
		if (per > 0)
		    fill_color = complete_color;
		else
		    fill_color = remaining_color;
	
		str.append("<svg width='" + total_width_actual + "' height='"
			+ total_height + "'>");
	
		for (int i = 1; i <= total_bar; i++) {
		    double current_bar_start = (single_bar_size * (i - 1))
			    + (bar_end_space_pix * (i - 1));
		    double current_bar_end = single_bar_size * i
			    + (bar_end_space_pix * (i - 1));
		    double white_start = current_bar_end;
		    double white_end = white_start + bar_end_space_pix;
		    if (per >= current_bar_start && per <= current_bar_end) {
			str.append("<g style='fill: " + fill_color + ";'>");
			str.append("<rect x='" + current_bar_start + "' y='" + top
				+ "' height='" + total_height + "' width='"
				+ (per - current_bar_start) + "'></rect>");
			str.append("</g>");
			fill_color = remaining_color;
			str.append("<g style='fill: " + fill_color + ";'>");
			str.append("<rect x='" + per + "' y='" + top + "' height='"
				+ total_height + "' width='" + (current_bar_end - per)
				+ "'></rect>");
			str.append("</g>");
		    } else {
			str.append("<g style='fill: " + fill_color + ";'>");
			str.append("<rect x='" + current_bar_start + "' y='" + top
				+ "' height='" + total_height + "' width='"
				+ (current_bar_end - current_bar_start) + "'></rect>");
			str.append("</g>");
		    }
		    str.append("<g style='fill: " + white_bar_color + ";'>");
		    str.append("<rect x='" + white_start + "' y='" + top + "' height='"
			    + total_height + "' width='" + (white_end - white_start)
			    + "'></rect>");
		    str.append("</g>");
		    per = per + bar_end_space_pix;
		}
		str.append("</svg>");
		log.debug("slaDashboard.java -> code = "+str.toString());
		return str.toString();
    }

    @SuppressWarnings("unchecked")
    public void calculate(String userId) 
    {	
	
    	log.info("slaDashboard.java -> in calculate()");
    	Query query = sessionFactory.getCurrentSession().createSQLQuery("select sla_average,dashboard_color from serviceslaavailability where customerID = \""+ userId + "\"");
    	log.debug("slaDashboard.java -> query = "+"select sla_average,dashboard_color from serviceslaavailability where customerID = "+ userId);
    	
		List<Object[]> per_color = (List<Object[]>) query.list();
		log.debug("slaDashboard.java -> List(per_color) size = "+per_color==null?"null":per_color.size()+"");
		if (per_color.size() > 0) 
		{
		    for (Object[] row : per_color) 
		    {
		    	if (row[0] != null) 
		    	{
		    		per = ((Double) row[0]).doubleValue();
		    		color = getSlaDetails.colorNameToCode((String) row[1]);
		    	} 
		    	else 
		    	{
				    per = 999;
				    color = null;
		    	}
		    }
		}
		else 
		{
		    per = 999;
		    color = null;
		}
		log.debug("slaDashboard.java -> Per = "+per+" & Color = "+color);
	}
}
