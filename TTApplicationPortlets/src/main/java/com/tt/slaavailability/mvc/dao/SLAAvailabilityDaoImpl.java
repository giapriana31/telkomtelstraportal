package com.tt.slaavailability.mvc.dao;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.NetworkAvailability;
import com.tt.model.SaaSAvailability;
import com.tt.slaavailability.mvc.model.getSlaDetails;

@Repository(value = "slaAvailabilityDaoImpl")
@Transactional
public class SLAAvailabilityDaoImpl {

	private final static Logger log = Logger.getLogger(SLAAvailabilityDaoImpl.class);
	
    @Autowired
    private SessionFactory sessionFactory;
    public String color = "";
    public double per = 0.0;
    
    
    public NetworkAvailability getNetworkDetails(String customerId){
   	 log.info("SLAAvailabilityDaoImpl.java -> in getNetworkDetails()");
   	 
   	 	Calendar calendar = Calendar.getInstance();
   	 	calendar.add(Calendar.MONTH, -1);
   	 	String monthString = new SimpleDateFormat("yyyy-MM").format(calendar.getTime());
   	 	log.debug("monthString :: " + monthString);
   	 
    	NetworkAvailability availability;
    	Query query=sessionFactory.getCurrentSession().createQuery("From NetworkAvailability na where na.customerId=:customerId and monthyear=:monthyear");
    	query.setParameter("customerId", customerId);
    	query.setParameter("monthyear", monthString);
     	 log.info("SLAAvailabilityDaoImpl.java -> getNetworkDetails --> Query "+query.getQueryString());

    	if(query.list()!=null && query.list().size()>0){
    		
    		 availability=(NetworkAvailability)query.list().get(0);	
    	}else{
    		
    		availability= new NetworkAvailability();
    		availability.setAvgAvail("no data");
    		availability.setColor("no data");
    		availability.setCustomerId("no data");
    	}
    	
      	 log.info("SLAAvailabilityDaoImpl.java -> Network Object "+availability.getAvgAvail()+" "+availability.getCustomerId()+" "+availability.getColor());

    	return availability;
    }
    
    
    @SuppressWarnings("unchecked")
    public String getSitesDashboardDetails(String userId, double per, String color){
    	
    	 log.info("SLAAvailabilityDaoImpl.java -> in calculate()");
    		
    		
    		Query query = sessionFactory
    				.getCurrentSession()
    				.createSQLQuery("select sla_average,dashboard_color from serviceslaavailability where customerID = \""
    						+ userId + "\" and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m')");
    		
    		log.debug("SLAAvailabilityDaoImpl.java -> query = "+"select dashboard_percentage,graph_color from serviceslaavailability where customerID = "+userId + " and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m')");
    		List<Object[]> per_color = (List<Object[]>) query.list();
    		
    		log.debug("SLAAvailabilityDaoImpl.java -> List size(per_color) = "+per_color==null?"null":per_color.size()+"");
    		
    		if (per_color.size() > 0) 
    		{
    		    
    			for (Object[] row : per_color) 
    		    {
    			if (row[0] != null) 
    			{
    			    per = ((Double) row[0]).doubleValue();
    			    color = getSlaDetails.colorNameToCode((String) row[1]);
    			    
    			} else {
    			    per = 999;
    			    color = null;
    			}
    		    }
    		} else {
    			per = 999;
    			color = null;
    		    
    		}
    		
    		
    		return per+"######"+color;
    }
    
    @SuppressWarnings("unchecked")
    public String calculate(String userId, double per, String color) {

    log.info("SLAAvailabilityDaoImpl.java -> in calculate()");
	Query query = sessionFactory
		.getCurrentSession()
		.createSQLQuery("select dashboard_percentage,graph_color from serviceslaavailability where customerID = \""
				+ userId + "\" and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m')");
	

	
	log.debug("SLAAvailabilityDaoImpl.java -> query = "+"select dashboard_percentage,graph_color from serviceslaavailability where customerID = "+userId);
	List<Object[]> per_color = (List<Object[]>) query.list();
	
	log.debug("SLAAvailabilityDaoImpl.java -> List size(per_color) = "+per_color==null?"null":per_color.size()+"");
	
	if (per_color.size() > 0) 
	{
	    
		for (Object[] row : per_color) 
	    {
		if (row[0] != null) 
		{
		    per = ((Double) row[0]).doubleValue();
		    color = getSlaDetails.colorNameToCode((String) row[1]);
		    
		} else {
		    per = 999;
		    color = null;
		}
	    }
	} else {
		per = 999;
		color = null;
	    
	}
	
	
	return per+"######"+color;
    }

    @SuppressWarnings("unchecked")
    public String processDataSLADetails(HttpServletRequest request, double per, String color)
	    throws IOException {
    	
    log.info("SLAAvailablityDaoImpl.java -> in processDataSLADetails()");
	String userId = request.getParameter("userId").toString();
	log.debug("SLAAvailablityDaoImpl.java -> Userid = "+userId);
	
	if (userId != null) 
	{
	    String returnString = "";
	    SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("select sla_average,dashboard_color from serviceslaavailability where customerID = \"" + userId + "\" and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m')");
	    
	    log.debug("SLAAvailablityDaoImpl.java -> Query = "+"select sla_average,dashboard_color from serviceslaavailability where customerID = \""+ userId + " and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m')");
	    List<Object[]> row = (List<Object[]>) query.list();
	    log.debug("SLAAvailablityDaoImpl.java -> List(row) size = "+row==null?"null":row.size()+"");
	    
	    if (row.size() > 0) 
	    {
			for (Object[] column1 : row) 
			{
			    if (column1[0] != null) 
			    {
				per = ((Double) column1[0]).doubleValue();
				color = colorNameToCode((String) column1[1]);
				log.debug("SLAAvailablityDaoImpl.java -> per = "+per+" & color = "+color);
				query = sessionFactory
					.getCurrentSession()
					.createSQLQuery(
						"select calculation_gold, gold_service_color, calculation_silver, silver_service_color, calculation_bronze, bronze_service_color from serviceslaavailability  where customerID = \""
							+ userId + "\" and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m')");
				
				log.debug("SLAAvailablityDaoImpl.java -> Query = select calculation_gold, gold_service_color, calculation_silver, silver_service_color, calculation_bronze, bronze_service_color from serviceslaavailability  where customerID = \""+ userId + "\" and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m')");
				returnString += new DecimalFormat("0.00").format(per)
					+ "%@@@@@" + "NULL" + "@@@@@" + "NULL"
					+ "#####";
	
				row = (List<Object[]>) query.list();
				for (Object[] column : row) {
				    if (column[0] != null) {
					per = ((Double) column[0]).doubleValue();
					color = colorNameToCode((String) column[1]);
					returnString += "Gold Sites" + "@@@@@"
						+ new DecimalFormat("0.00").format(per)
						+ "%@@@@@" + color + "#####";
				    } else {
					returnString += "Gold Sites"
						+ "@@@@@NULL@@@@@NULL#####";
				    }
	
				    if (column[2] != null) {
					per = ((Double) column[2]).doubleValue();
					color = colorNameToCode((String) column[3]);
					returnString += "Silver Sites" + "@@@@@"
						+ new DecimalFormat("0.00").format(per)
						+ "%@@@@@" + color + "#####";
				    } else {
					returnString += "Silver Sites"
						+ "@@@@@NULL@@@@@NULL#####";
				    }
	
				    if (column[4] != null) {
					per = ((Double) column[4]).doubleValue();
					color = colorNameToCode((String) column[5]);
					returnString += "Bronze Sites" + "@@@@@"
						+ new DecimalFormat("0.00").format(per)
						+ "%@@@@@" + color + "#####";
				    } else {
					returnString += "Bronze Sites"
						+ "@@@@@NULL@@@@@NULL#####";
				    }
	
				    break;
				}
				returnString = returnString.substring(0,returnString.length() - 5);
				
			    } else {
				returnString = "no data";
			    }
			    log.debug("SLAAvailablityDaoImpl.java -> returnString"+returnString);
			    break;
			}
	    } 
	    else 
	    {
	    	returnString = "no data";
	    	log.debug("SLAAvailablityDaoImpl.java -> returnString"+returnString);
	    }

	    return returnString;
	}

	return "";
    }

	public static String colorNameToCode(String color) 
	{
		if(null != color)
		{
			if (color.equalsIgnoreCase("green"))
				return "rgb(163, 207, 98)";
			else if (color.equalsIgnoreCase("amber"))
				return "rgb(255, 156, 0)";
			else if (color.equalsIgnoreCase("red"))
				return "rgb(227, 34, 18)";
			else
				return "";
		}
		return "";
	}


/*	public List getCloudAvailability(String customerId) 
	{
		log.info("Class: SLAAvailablityDaoImpl, Method:getCloudAvailability Start");
		List<Object> cloudAvailabilityList=null;
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select ciname,averageavailability,color from cloudavailability where customerId='"+customerId+"'");
		if(null!=query.list() && query.list().size()>0){
			
			cloudAvailabilityList=query.list();
			
		}
		
		log.info("Class: SLAAvailablityDaoImpl, Method:getCloudAvailability End");
		return cloudAvailabilityList;
	}*/
	
	public String getIaaSDashBoardColor(String customerId)
	{	
		
		Calendar cal =  Calendar.getInstance();
		cal.add(Calendar.MONTH ,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM");
			String dates=sdf.format(cal.getTime());
		
		
		log.info("Class: SLAAvailablityDaoImpl, Method:getIaaSDashBoardColor Start");
		Double average=null;
		String averageString=null;
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select averageavailability from cloudavailability where customerId='"+customerId+"'and month='"+dates+"'");
		if(null !=query.list() && query.list().size()>0 && null!=query.list().get(0)){
			
			average= (Double)query.list().get(0);
			if(null!=average){
				averageString=average.toString();	
			}
			
			
		}
		
		log.info("Class: SLAAvailablityDaoImpl, Method:getIaaSDashBoardColor End");
		
		return averageString;
	}


	public Object[] getSaaSDashboardColor(String customerID)
	{
		log.info("Class: SLAAvailablityDaoImpl, Method:getSaaSDashboardColor Start");
		StringBuilder saasColorQuery = new StringBuilder();
		saasColorQuery.append("select (case when averageavailability = -1.00 then 'NA' else averageavailability end) saasavailability, "); 
		saasColorQuery.append("color from saasdashboard where customerid=:customerid ");
		saasColorQuery.append("and monthyear=DATE_FORMAT(DATE_ADD(now(),INTERVAL -1 MONTH), '%Y-%m') ");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(saasColorQuery.toString());
		query.setString("customerid", customerID);
		
		Object[] dashboardColorAvail = (Object[]) query.uniqueResult();
		
		
		log.info("Class: SLAAvailablityDaoImpl, Method:getSaaSDashboardColor End");
		return dashboardColorAvail;
	}


	public List<SaaSAvailability> getSaaSProductDetails(String customerID)
	{
		log.info("Class: SLAAvailablityDaoImpl, Method:getSaaSDashboardColor Start");
		
		Calendar calendar = Calendar.getInstance();
   	 	calendar.add(Calendar.MONTH, -1);
   	 	String monthString = new SimpleDateFormat("yyyy-MM").format(calendar.getTime());
   	 	log.debug("monthString :: " + monthString);
   	 	
		StringBuilder saasProductDetails = new StringBuilder();
		saasProductDetails.append("from SaaSAvailability where customerid=:customerid ");
		saasProductDetails.append("and monthyear=:monthyear ");
		
		Query query = sessionFactory.getCurrentSession().createQuery(saasProductDetails.toString());
		query.setString("customerid", customerID);
		query.setString("monthyear", monthString);
		
		List<SaaSAvailability> saasProductList = query.list();
		
		log.info("Class: SLAAvailablityDaoImpl, Method:getSaaSDashboardColor End");
		return saasProductList;
	}
}
