package com.tt.slaavailability.mvc.model;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.tt.logging.Logger;

public class getSlaDetails  
{
	public double per = 0.0;
	public String color = "";	
	private final static Logger log = Logger.getLogger(getSlaDetails.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public getSlaDetails() {super();}
    
	@SuppressWarnings("unchecked")
	public String processData(HttpServletRequest request) throws IOException
	{
		log.info("getSlaDetails.java -> in processData()");
		String userId = request.getParameter("userId").toString();
		log.debug("getSlaDetails.java -> User id = "+userId);
		if(userId !=null)
		{
			String returnString = "";
			
//			Configuration cfg = new Configuration();		
//		    cfg.configure("hibernate.cfg.xml");
//		    
//		    @SuppressWarnings("deprecation")
//			Session session = cfg.buildSessionFactory().openSession();
		    
//		    SQLQuery query = session.createSQLQuery("select sla_average,dashboard_color from serviceslaavailability where customerID = \""+userId+"\"");
		    SQLQuery query = sessionFactory.getCurrentSession().createSQLQuery("select sla_average,dashboard_color from serviceslaavailability where customerID = \""+userId+"\"");
		   		    
			List<Object[]> row = (List<Object[]>)query.list();
			log.debug("getSlaDetails.java -> "+row==null?"null":row.size()+"");
			
			if(row.size()>0)
			{
			    for(Object[] column1: row)
			    {
			    	if(column1[0]!=null)
			        {	
			    		per = ((Double)column1[0]).doubleValue();
			        	color = colorNameToCode((String)column1[1]);
			        	log.debug("getSlaDetails.java -> Per = "+per+" & Color "+color);
			        	
			        	query = sessionFactory.getCurrentSession().createSQLQuery("select calculation_gold, gold_service_color, calculation_silver, silver_service_color, calculation_bronze, bronze_service_color from serviceslaavailability  where customerID = \""+userId+"\"");
					    returnString += new DecimalFormat("0.00").format(per)+"%@@@@@"+"NULL"+"@@@@@"+"NULL"+"#####";
					    
					    row = (List<Object[]>)query.list();
					    for(Object[] column: row)
					    {
					    	if(column[0]!=null)
					    	{
					    		per = ((Double)column[0]).doubleValue();
					    		color = colorNameToCode((String)column[1]);  
					    		returnString += "Gold Sites"+"@@@@@"+new DecimalFormat("0.00").format(per)+"%@@@@@"+color+"#####";
					    	}
					    	else
					    	{
					    		returnString += "Gold Sites"+"@@@@@NULL@@@@@NULL#####";
					    	}

					    	if(column[2]!=null)
					    	{
							    per = ((Double)column[2]).doubleValue();
							    color = colorNameToCode((String)column[3]);  
							    returnString += "Silver Sites"+"@@@@@"+new DecimalFormat("0.00").format(per)+"%@@@@@"+color+"#####";
					    	}
					    	else
					    	{
					    		returnString += "Silver Sites"+"@@@@@NULL@@@@@NULL#####";
					    	}
					    	
					    	if(column[4]!=null)
					    	{
							    per = ((Double)column[4]).doubleValue();
							    color = colorNameToCode((String)column[5]);  
							    returnString += "Bronze Sites"+"@@@@@"+new DecimalFormat("0.00").format(per)+"%@@@@@"+color+"#####";
					    	}
					    	else
					    	{
					    		returnString += "Bronze Sites"+"@@@@@NULL@@@@@NULL#####";
					    	}
					    	
						    break;
					    }				    
					    returnString = returnString.substring(0, returnString.length() - 5);
			        }
			    	else
			    	{
			    		returnString = "no data" ;
			    	}
			        break;
			    }	    
			}
			else
			{
				returnString = "no data" ;
			}
			
			log.debug("getSlaDetails -> returnString = "+returnString); 
		    return returnString;
		}
		
		return "";
	}	
		
	public static String colorNameToCode(String color)
	{ 
		if(color.equalsIgnoreCase("green"))
			return "rgb(163, 207, 98)";
		else if(color.equalsIgnoreCase("amber"))
			return "rgb(255, 156, 0)";
		else if(color.equalsIgnoreCase("red"))
			return "rgb(227, 34, 18)";
		else
			return "";
	}
}
