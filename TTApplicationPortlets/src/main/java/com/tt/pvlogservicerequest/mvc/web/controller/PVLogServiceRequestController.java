package com.tt.pvlogservicerequest.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.client.model.createServiceRequestapi.InsertResponse;
import com.tt.client.service.incident.createServiceRequestWSCallService;
import com.tt.constants.TTPortalAppErrors;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;
import com.tt.model.Incident;
import com.tt.pvlogservicerequest.mvc.model.PVCreateSRForm;
import com.tt.pvlogservicerequest.mvc.service.common.PVCreateSRManager;
import com.tt.pvlogservicerequest.mvc.service.common.PVLogServiceRequestService;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.utils.TTGenericUtils;
import com.tt.constants.GenericConstants;


@Controller("pvLogServiceRequestController")
@RequestMapping("VIEW")
public class PVLogServiceRequestController {
	
	public static final Properties prop = PropertyReader.getProperties();
	String statusNew=prop.getProperty("statusNew");
	String statusAssigned=prop.getProperty("statusAssigned");
	String statusPending=prop.getProperty("statusPending");
	String statusInProgress=prop.getProperty("statusInProgress");
	String statusClosed=prop.getProperty("statusClosed");
	String statusCancelled=prop.getProperty("statusCancelled");
	String statusComplete=prop.getProperty("statusComplete");
	String statusCompleted=prop.getProperty("statusCompleted");
	String statusClosedCancelled=prop.getProperty("statusClosedCancelled");
	String changeTypeRoutine=prop.getProperty("changeTypeRoutine");
	String changeTypeComprehensive=prop.getProperty("changeTypeComprehensive");
	String changeTypeEmergency=prop.getProperty("changeTypeEmergency");
	String changeSourceInternal=prop.getProperty("changeSourceInternal");
	String changeSourceExternal=prop.getProperty("changeSourceExternal");
	String changeSourceCustomer=prop.getProperty("changeSourceCustomer");
	private final static Logger log = Logger.getLogger(PVLogServiceRequestController.class);

	private PVCreateSRManager pvCreateSRManager;
	private PVLogServiceRequestService pvLogServiceRequestService;
	
	
	@Autowired
	@Qualifier("pvCreateSRManager")
	public void setPVCreateSRManager(PVCreateSRManager pvCreateSRManager) {
		this.pvCreateSRManager = pvCreateSRManager;
		
	}
	
	@Autowired
    @Qualifier("pvLogServiceRequestService")
    public void setPVLogServiceRequestService(PVLogServiceRequestService pvLogServiceRequestService) {
		this.pvLogServiceRequestService = pvLogServiceRequestService;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		
		try {		
			
			String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request), PortalUtil.getHttpServletResponse(renderResponse));
			
			//Data refresh
            String lastRefreshDate = pvLogServiceRequestService.getLastRefreshDate(customerId);
            model.addAttribute("lastRefreshDate", lastRefreshDate);
            
            
			RenderLogServiceRequest(request, renderResponse, model);

            //Getting site name list
    	    Map<String, String> siteNameMap = pvCreateSRManager.getSiteNameMap(customerId);
    	    
			log.info("**********SiteNameList IN Controller********** " + siteNameMap);
			if (siteNameMap != null && !(siteNameMap.isEmpty())) {
				
				Map<String, String> siteNameMapActual = new LinkedHashMap<String, String>();
				siteNameMapActual.put("allSites", "All Sites");
				for (Map.Entry<String, String> entry : siteNameMap.entrySet()) {
					siteNameMapActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("sitenamelist", siteNameMapActual);
			} else {
				Map<String, String> siteNameListActual = new LinkedHashMap<String, String>();
				siteNameListActual.put("allSites", "All Sites");
				model.addAttribute("sitenamelist", siteNameListActual);
			}
			
			return "pvLogServiceRequest";
			
		} catch (Exception e) {
			log.error("In render mapping:"+e.getMessage());
			throw new TTPortalAppException(e, "SS:1111", TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}		
	
	}
	
	 public void RenderLogServiceRequest (RenderRequest request, RenderResponse renderResponse, Model model) {
	    	try{
			
	    		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request), PortalUtil.getHttpServletResponse(renderResponse));
				if(customerUniqueID != null){
					
					log.info("*********Rendering Service Request Modal for user : " + customerUniqueID);
					
					List<String> productTypeList =pvCreateSRManager.getProductTypeList();
					
					productTypeList.add(0, "--Select--");
					model.addAttribute("productTypeList",productTypeList);
					
					LinkedHashMap<String, String> regionList = pvCreateSRManager.getRegionNameMap(customerUniqueID);
					LinkedHashMap<String, String> regionListActual = new LinkedHashMap<String, String>();
					regionListActual.put("0", "--Select--");
					for( Map.Entry<String,String> entry : regionList.entrySet()){
						regionListActual.put(entry.getKey(), entry.getValue());
					}
					model.addAttribute("regionList", regionListActual);
					
					LinkedHashMap<String, String> siteNameList = new LinkedHashMap<String, String>();
					siteNameList.put("0", "--Select--");
					model.addAttribute("siteNameList", siteNameList);
					
					LinkedHashMap<String, String> serviceIDList = new LinkedHashMap<String, String>();
					serviceIDList.put("0", "--Select--");
					model.addAttribute("serviceIDList", serviceIDList);
					
					/*log.info("Testing Property file : " + prop.getProperty("lovNameAction"));*/
					
					LinkedHashMap<String, String> subCategoryList = new LinkedHashMap<String, String>();
					subCategoryList.put("0", "--Select--");
					model.addAttribute("subCateogryListMap", subCategoryList);
					
					LinkedHashMap<String, String> deviceList = new LinkedHashMap<String, String>();
					deviceList.put("0", "--Select--");
					model.addAttribute("deviceNameListMap", deviceList);
					
					LinkedHashMap<String, String> prodClassList = new LinkedHashMap<String, String>();
					prodClassList.put("0", "--Select--");
					model.addAttribute("productClassificationListMap", prodClassList);
					
					log.info("********** Region list : " +regionList + "\n ********* siteNameList : " +siteNameList+ " \n ******serviceIDList " +serviceIDList+ " \n subCategoryList : " +subCategoryList+ "\n *********deviceNameListMap : " +deviceList);
					model.addAttribute("logSRFormModel", new PVCreateSRForm());
				}
				else{
					log.debug("Rendering without any user details - CustomerUniqueID not found.");
					model.addAttribute("logSRFormModel", new PVCreateSRForm());
				}
				
			}
			catch (Exception e){
				log.error("Error while rendering for Log A New Request - " + e.getMessage());
			}
	    }
		
	
	@ResourceMapping(value="getSiteURL")
	public void handleSiteIDFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		try{
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedRegionJson"));
			String selectedRegion = jsonObj.getString("selectedRegion");
			
		log.info("********* Selected Region in controller to fetch Site Name:: " +selectedRegion );

			if(selectedRegion!=null && selectedRegion.trim().length()!=0){
				String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req,httpServletResponse);
				LinkedHashMap<String, String> siteIDListtemp = pvCreateSRManager.getSiteFromRegion(selectedRegion,customerUniqueID);
				String responseStr = "";
				if(siteIDListtemp!=null){
					for( Map.Entry<String,String> entry : siteIDListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
						log.info("*******************Site List in Controller ---------- >> " + siteIDListtemp);
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
		}
		catch(Exception e){
			log.error("Error while fetching Site from Region in controller - " + e.getMessage());
			
		}
	}
	
	@ResourceMapping(value = "getServiceForProductTypeURL")
    public void handleSericeIDFetch(Model model, ResourceRequest request, ResourceResponse response) throws Exception {
			
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(response);
		try {
		    JSONObject jsonObj = new JSONObject(req.getParameter("selectedSiteIDJson"));
		    
		    String selectedSiteID = jsonObj.getString("selectedSiteID");
		    String selectedProductType = jsonObj.getString("selectedProductID");
		    
		    log.debug("getServiceForProductTypeURL in servreq controller :::: \n selectedSiteID recieved in controller to fetch service::>>"+selectedSiteID+""
		    		+ "\n selectedProductType recieved in controller to fetch service::>> "+selectedProductType);
		 
		    String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req,res);

				Map<String, String> serviceIDListtemp = pvCreateSRManager.getServiceFromProductTypeAndSiteID(selectedProductType , selectedSiteID, customerUniqueID);;
				log.info("Service Recieved from getServiceForProductTypeURL: " + serviceIDListtemp);
				String responseStr = "";
				if (serviceIDListtemp != null) {
					for (Map.Entry<String, String> entry : serviceIDListtemp
							.entrySet()) {
						responseStr += (entry.getKey() + "@@@@@"
								+ entry.getValue() + "#####");
						log.info("*******************Service List in Controller ---------- >> " + serviceIDListtemp);
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
		
	} catch (Exception e) {
		throw new TTPortalAppException(e, "SS:1111", TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
	}
}
	
	@ResourceMapping(value="getDeviceURL")
	public void handleDeviceFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
        try{
        	
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedServiceJson"));
			String selectedService = jsonObj.getString("selectedService");
			String selectedProductType = jsonObj.getString("selectedProductID");
			String selectedSubCategory = jsonObj.getString("selectedSubCategory");
			
			if(selectedService!=null && selectedService.trim().length()!=0){
				log.info("********* Selected service in controller to fetch  device name: " + selectedService);
				log.info("********* Selected selectedProductType in controller to fetch  device name: " + selectedProductType);
				log.info("********* Selected selectedSubCategory in controller to fetch  device name: " + selectedSubCategory);
				LinkedHashMap<String, String> deviceListtemp = pvCreateSRManager.getSRDevices(selectedProductType, selectedService, selectedSubCategory);
				String responseStr = "";
				if(deviceListtemp!=null){
					for( Map.Entry<String,String> entry : deviceListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
						log.info("*******************Device List in Controller ---------- >> " + deviceListtemp);
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
				else{
					log.info("Getting Zero devices for selected Service");
				}
			}
		}
		catch(Exception e){
			log.error("Error while fetching Devices for service in controller - " + e.getMessage());
			
		}
	}
	
	@ResourceMapping(value="getSubCategoryURL")
	public void handleActionSubCategoryFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
        
		try{
			String responseStr1 = "";
			String responseStr2 = "";
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedServiceJson"));
			String selectedService = jsonObj.getString("selectedService");
			String category =  jsonObj.getString("categoryValue");
			String selectedProductType = jsonObj.getString("selectedProductID");
			
			log.info("************** Servicename " +selectedService+ "and "
					+ "\n category recieved in controller to fetch subcategory" +category+""
							+ "\n and selectedProductType ::>> "+selectedProductType);
			if(selectedService!=null && selectedService.trim().length()!=0){
				if(selectedService!="0"){
					LinkedHashMap<String, String> subCategoryListtemp = pvCreateSRManager.getSubCategory(selectedProductType, category, selectedService);
					if(subCategoryListtemp!=null){
						for( Map.Entry<String,String> entry : subCategoryListtemp.entrySet()){
							responseStr2 += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
							
							log.info("*******************Subcategory List in Controller ---------- >> " + subCategoryListtemp);
						}
					}
				}
				
				PrintWriter out = response.getWriter();
				out.print(responseStr1+"%%%%%"+responseStr2);
				
			}
		}
		catch(Exception e){
			log.error("Error while fetching Action & Subcategory in controller - " + e.getMessage());
			
		}
	}
		

	public String getCategoryForSubmission(String selectedCateogry, String selectedService) throws Exception{
		try{
			/*if(selectedCateogry.equalsIgnoreCase("INFLIGHTMACD")){*/
				
				log.info(" Value of Category Recieved in controller try ..  --------------->>>>>"+ pvCreateSRManager.fetchCategoryForMACD(selectedService));
				return pvCreateSRManager.fetchCategoryForMACD(selectedService);
			/*}*/
		}
		catch(Exception e){
			log.error("Error while fetching Category for MACD to be submitted to SNOW in controller - " + e.getMessage());
			throw e;
		}
		finally{
			
		}
	}
	
	@ResourceMapping(value="submitRequestURL")
	public void handleLogServiceRequest(@Valid @ModelAttribute("logSRFormModel") PVCreateSRForm srForm, Model model,ResourceRequest request, ResourceResponse response) throws Exception{
    	HttpServletRequest httprequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpresponse = PortalUtil.getHttpServletResponse(response);
		JSONObject json = new JSONObject();
    	try{
			
    		User user = UserLocalServiceUtil.getUser((Long)httprequest.getSession().getAttribute(WebKeys.USER_ID));
			RefreshUtil.setProxyObject();
			createServiceRequestWSCallService callService = new createServiceRequestWSCallService();
			Incident incident = new Incident();
					
			incident.setUserid(user.getEmailAddress()+"");
			incident.setServiceid(srForm.getServiceName());
			incident.setResolvergroupname("");
			incident.setImpact(prop.getProperty("defaultSRImpact"));
			
			String language = ((String) user.getExpandoBridge().getAttribute("preferredLanguage")).equalsIgnoreCase("en_US") ? "English" : "Bahasa";
			incident.setCustomerpreferedlanguage(language);
			
			incident.setSummary(srForm.getSummary());
			String category;
			try{
				category = getCategoryForSubmission(srForm.getCategory(),srForm.getServiceName());
			}
			catch(Exception e){
				category="";
			}
			
			if(category == "SaaS Business Service" || category.equals("SaaS Business Service")){
				category = "SaaS";
			}
			
			incident.setCategory(category);
			incident.setSubcategory(srForm.getSubCategory());
			incident.setTicketsource(prop.getProperty("RequestSource"));
			//incident.setTickettype(prop.getProperty("PVServiceRequestTicketType"));
			incident.setTickettype(prop.getProperty("ServiceRequestTicketType"));
			incident.setUrgency(prop.getProperty("defaultSRUrgency"));
			
			
			incident.setWorklognotes(srForm.getDescription());
			
			String prodClassBefore = incident.getWorklognotes();
			
			String prodClassAfter = prodClassBefore+"\nProduct Classification:"+srForm.getProductClassification();
			incident.setWorklognotes(prodClassAfter);
			
			log.info("Description being sent to SNOW - " + incident.getWorklognotes());
			InsertResponse insert = new InsertResponse();
			boolean isException = false;
			try{
				insert= callService.insertIncidentWebService(incident);
			}
			catch(Exception ce){
				isException=true;
			}
			if(!isException){
				log.info("this is the end of call"+insert.getDisplayName()+"::"+insert.getDisplayValue()+""+insert.getErrorMessage());
				
				log.info("Display Response: " + insert.getDisplayValue());
				log.info("Error Response: " + insert.getErrorMessage());
				if(insert.getDisplayValue() !=null && insert.getDisplayValue().trim().length()!=0){
					json.put("requestID", "Success@"+insert.getDisplayValue());
				}
				else{
					json.put("requestID", "Error@"+insert.getErrorMessage());
				}
				json.put("incidentID", insert.getDisplayValue());
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}
			else{
				log.error("Connection timed out");
				json.put("requestID", "Error@"+"Connection Timed Out. Please try again later.");
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}
			
		}
		catch(Exception e){
			log.error("Error while submitting Service Request" + e.getMessage());
		}
    }

	@ActionMapping(value = "refreshSR")
    public void dataRefresh(ActionRequest actionRequest,
	    ActionResponse actionResponse) throws IOException,
	    PortletException, PortalException, SystemException {

	log.info("refreshSR.------------------");
	String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(actionRequest), PortalUtil.getHttpServletResponse(actionResponse));
	log.info("customerId--------------->>" + customerId);
	pvLogServiceRequestService.dataRefresh(customerId);
	String url = prop.getProperty("tt.pv.homeURL") + "/" + prop.getProperty("ttPVServiceRequest");
	actionResponse.sendRedirect(url);
    }
	
	@ResourceMapping(value = "pvprodClassforSelectedProductTypeURL")
    public void prodClassforSelectedProductType(ResourceRequest request, ResourceResponse response) throws Exception
	{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(response);
		try {
		    String selectedProductType =  req.getParameter("selectedProductID");
		    log.debug("prodClassforSelectedProductType in pvlogservreq controller :::: "
		    		+ "\n selectedProductType recieved in pvlogservreq controller to fetch service::>> "+selectedProductType);
		
		   // String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req,res);
		    
   				Map<String, List<String>> prodClassListtemp = pvCreateSRManager.prodClassforSelectedProductType(selectedProductType);
   				log.info("ProdClass Recieved from dao in getServiceForProductTypeURL in pvlogservreq controller: " +prodClassListtemp);
   				String responseStr = "";
   				if (prodClassListtemp != null) {
   					for (Map.Entry<String, List<String>> entry : prodClassListtemp.entrySet()) {
   						
   						for(String productclasification:  entry.getValue()){
   							responseStr += (entry.getKey() + "@@@@@"+ productclasification + "#####");
   						}
   						
   						
   					}
   					PrintWriter out = response.getWriter();
   					out.print(responseStr);
   				}
		    	
    }
	catch (Exception e) {
				throw new TTPortalAppException(e, "SS:1111", TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
	}
		
	}
	


}
