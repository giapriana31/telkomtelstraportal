package com.tt.pvlogservicerequest.mvc.dao;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.utils.PropertyReader;

import java.util.Date;
import java.util.Properties;


@Repository(value = "pvLogSRDaoImpl")
@Transactional
public class PVLogSRDaoImpl implements PVLogSRDao{
	
	public static Properties prop = PropertyReader.getProperties();
	
	@Autowired
	private SessionFactory sessionFactory;
	private static final String CUSTOMER_ID = "customerId";
	
	@Override
	public Date getLastRefreshDate(String customerId, String entityType) {
		
		StringBuilder sql = new StringBuilder();
		sql.append("select max(lastrefreshtime) from lastrefreshtime sa");
		sql.append(" where sa.customeruniqueid=:customerId and sa.entityname=:entityType");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
		query.setString(CUSTOMER_ID, customerId);
		query.setString("entityType", entityType);
		
		if (query.list() != null && !query.list().isEmpty()) {
			Date refreshDate = (Date) query.list().get(0);
			return refreshDate;
		} 
		else {
			return null;
		}
	}

}
