package com.tt.pvlogservicerequest.mvc.service.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;

import com.tt.pvlogservicerequest.mvc.dao.PVLogSRDao;
import com.tt.RefreshServiceRequest;

@Service(value = "pvLogServiceRequestService")
@Transactional
public class PVLogServiceRequestServiceImpl implements PVLogServiceRequestService {

	private PVLogSRDao pvLogSRDao;
	private final static Logger log = Logger
			.getLogger(PVLogServiceRequestServiceImpl.class);


	@Autowired
	@Qualifier("pvLogSRDaoImpl")
	public void setPVLogSRDaoImpl(PVLogSRDao pvLogSRDao) {
		this.pvLogSRDao = pvLogSRDao;
	}

	

	@Override
	public void dataRefresh(String customerId) {

		Date lastRefreshDate = pvLogSRDao.getLastRefreshDate(customerId,
				"ServiceRequest");

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		try {
			log.info("Refresh Data......--------->>>>>>");
			RefreshServiceRequest refreshObj = new RefreshServiceRequest();
			String toDate = new String();
			String fromDate = new String();
			Date todate = new Date();
			toDate = df.format(todate);
			log.info("toDate-----------" + toDate);
			if (lastRefreshDate != null)
				fromDate = df.format(lastRefreshDate);

			log.info("fromDate------------" + fromDate);
			refreshObj.refreshServiceRequest(customerId, toDate, fromDate,"CUST");
			log.info("Refresh Data Done......--------->>>>>>");
		} catch (Exception ex) {
			log.error(" Exception while calling data refresh ");
			log.error(ex.getMessage());
		}
	}

	@Override
	public String getLastRefreshDate(String customerId) {

		Date lastRefreshDate = pvLogSRDao.getLastRefreshDate(customerId,
				"ServiceRequest");

		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Jakarta");
		df.setTimeZone(timeZone1);

		if (lastRefreshDate != null) {
			String fromDate = df.format(lastRefreshDate);
			return fromDate;
		}
		return null;
	}

}
