package com.tt.pvlogservicerequest.mvc.dao;

import java.util.Date;

public interface PVLogSRDao {
	public Date getLastRefreshDate(String customerId, String entityType);

}
