package com.tt.pvlogservicerequest.mvc.service.common;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tt.logging.Logger;
import com.tt.pvlogservicerequest.mvc.dao.PVCreateSRDaoImpl;
import com.tt.pvlogservicerequest.mvc.web.controller.PVLogServiceRequestController;

@Service(value="pvCreateSRManager")
@Transactional
public class PVCreateSRManager {
	
private final static Logger log = Logger.getLogger(PVLogServiceRequestController.class);	
	
private PVCreateSRDaoImpl pvCreateSRDaoImpl;

	@Autowired
	@Qualifier("pvCreateSRDaoImpl")
	public void setCreateSRDaoImpl(PVCreateSRDaoImpl pvCreateSRDaoImpl) {
		this.pvCreateSRDaoImpl = pvCreateSRDaoImpl;
	}

	public LinkedHashMap<String, String> getRegionNameMap(String customeruniqueID) throws Exception{
		
		try{
			return pvCreateSRDaoImpl.getRegionNameMap(customeruniqueID);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public Map<String,String> getServiceFromProductTypeAndSiteID(String productType , String siteID, String customerId) throws Exception{
		try{
			return pvCreateSRDaoImpl.getServiceFromProductTypeAndSiteID(productType, siteID, customerId);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSiteFromRegion(String siteID, String customerUniqueID) throws Exception{
		try{
			return pvCreateSRDaoImpl.getSiteFromRegion(siteID,customerUniqueID);
		}
		catch(Exception e){
			
			throw e;
		}
	}

	public LinkedHashMap<String,String> getSRDevices(String productType, String serviceName, String selectedSubCategory) throws Exception{
		try{
			return pvCreateSRDaoImpl.getSRDevices(productType, serviceName, selectedSubCategory);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSubCategory(String productType, String category,String serviceName) throws Exception{
		try{
			return pvCreateSRDaoImpl.getSubCategory(productType, category, serviceName);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public String fetchCategoryForMACD(String serviceName) throws Exception{
		try{
			return pvCreateSRDaoImpl.fetchCategoryForMACD(serviceName);
		}
		catch(Exception e){
			
			throw e;
		}
	}	

	public LinkedHashMap<String, String> getSiteNameMap(String customerId) {
		try{
			return pvCreateSRDaoImpl.getSiteNameMap(customerId);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public List<String> getProductTypeList() throws Exception{
		try{
			
			return pvCreateSRDaoImpl.getProductTypeList();
		}
		catch(Exception e){
			throw e;
		}
	}
	public LinkedHashMap<String,List<String>> prodClassforSelectedProductType(String selectedProductType) throws Exception{
		try{
			
			return pvCreateSRDaoImpl.prodClassforSelectedProductType(selectedProductType);
		}
		catch(Exception e){
			throw e;
		}
	}

}
