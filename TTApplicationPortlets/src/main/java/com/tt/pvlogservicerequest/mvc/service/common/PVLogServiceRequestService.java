package com.tt.pvlogservicerequest.mvc.service.common;

public interface PVLogServiceRequestService {
	
	public void dataRefresh(String customerId);
	public String getLastRefreshDate(String customerId);

}
