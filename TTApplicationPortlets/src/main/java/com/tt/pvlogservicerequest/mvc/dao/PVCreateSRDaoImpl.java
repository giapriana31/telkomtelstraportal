package com.tt.pvlogservicerequest.mvc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;
@Repository(value="pvCreateSRDaoImpl")
@Transactional

public class PVCreateSRDaoImpl {
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(PVCreateSRDaoImpl.class);
	
	
	@Autowired
	private SessionFactory sessionFactory;
	private Session session=null;
		
	public LinkedHashMap<String,String> getRegionNameMap(String customeruniqueID) throws Exception{
		try{
	
			session=sessionFactory.openSession();
			
			LinkedHashMap<String, String> regionNameMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select distinct s.stateprovince from Site s where s.customeruniqueid=:custId and rootProductId = 'MNS' and s.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE)
				 .append(" order by s.stateprovince asc");
			Query query1 = session.createQuery(query.toString()).setString("custId", customeruniqueID);
			
			log.info("******************Query "+query1+" \nFired for fetching Region for logged in user : " + customeruniqueID);
			
			if(query1.list()!=null && !query1.list().isEmpty()){
				for(Iterator it=query1.iterate();it.hasNext();){
					String row = (String) it.next();
					regionNameMap.put(row, row);
				}
			}else{
				log.debug("No region data found for user with ID : " + customeruniqueID);
			}
			log.info("*******************Region list in dao --------------"+regionNameMap);
				return regionNameMap;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getRegionNameMap method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction for fetching region");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getSiteFromRegion(String region, String customerUniqueID) throws Exception{
		try{
			session=sessionFactory.openSession();

			LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select s.customersite, s.siteid from Site s where s.stateprovince=:region and s.customeruniqueid=:custId and lower(s.servicetier) <> lower('Fully Managed') and rootProductId = 'MNS' and s.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by s.customersite asc");
			Query query1 = session.createQuery(query.toString()).setString("region",region).setString("custId",customerUniqueID);
			
			log.info("******************Query "+query1+" \nFired for fetching Site for Selected Region: " +region+ "\n and customer id - " + customerUniqueID);
			
			if(query1.list()!=null && !query1.list().isEmpty()){
				log.info("Site data found for Region " + region);
				for(Iterator it=query1.iterate();it.hasNext();){
				 	Object[] row = (Object[]) it.next();
				 	siteNameIDMap.put(row[1]+"", row[0]+"");
				}
			}
			else{
				log.debug("No Site data found for region : " + region + "\n and customer id - " + customerUniqueID);
			}
			
			log.info("******* Site name " +siteNameIDMap+ "returned from dao for selected region " +region+ "*****");
			return siteNameIDMap;
			
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getSiteFromRegion method  - " +e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction for fetching SiteID");
				session.close();
			}
		}
	}
	
	
	public Map<String,String> getServiceFromProductTypeAndSiteID(String productType, String siteName, String customerId) throws Exception{
		try{
			session=sessionFactory.openSession();
		
			Map<String, String> serviceIdMap = new LinkedHashMap<String, String>();
			StringBuilder hql = new StringBuilder();
			
			log.debug("In Create SR DaoImpl :: getServiceFromProductTypeAndSiteID Method:::"
					+ "\n ProductType recieved for getServiceFromProductTypeAndSiteID in dao::>>"+productType +""
							+ "\n Site ID recieved for getServiceFromProductTypeAndSiteID in dao:::  "+ siteName);
			
			if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MNS)){
				
				hql.append("select c.ciid, c.ciname from Configuration c where c.rootProductId =:producTypeFetched and c.citype=:ciType and c.status in ('");
				hql.append(GenericConstants.STATUSCOMMISSIONING);
				hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
				hql.append(GenericConstants.STATUSOPERATIONAL);
				hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
				
				if(!(siteName.equalsIgnoreCase("allSites"))){
					hql.append(" AND c.siteid in (select siteid from Site where customersite=:siteNameFetched and status in ('");
					hql.append(GenericConstants.STATUSCOMMISSIONING);
					hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
					hql.append(GenericConstants.STATUSOPERATIONAL);
					hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(")");
				}
				
				hql.append(" order by c.ciname asc");
				
				Query queryMNS = session.createQuery(hql.toString());
				
				queryMNS.setString("ciType", prop.getProperty("ciType"))
				        .setString("producTypeFetched", productType);
				
				if(!(siteName.equalsIgnoreCase("allSites"))){
					queryMNS.setString("siteNameFetched",siteName);
				}
				log.info("******************Query "+queryMNS+" \nFired for fetching Service for Selected Site: " +siteName);
				
				if(queryMNS.list()!=null && !queryMNS.list().isEmpty()){
					
					log.info("Service data found for Site " + siteName);
					
					for(Iterator it=queryMNS.iterate();it.hasNext();){
						Object[] row = (Object[]) it.next();
						serviceIdMap.put(row[0]+"", row[1]+"");
						
						log.info("getServiceQuery for MNS"+ queryMNS);
					}
				}
				else{
					log.debug("No Service Data found for Site ID - " + siteName);
				}
			}else if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_WHISPIR) || productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_IPSCAPE)){
				//When Product Type = "Whispir" and "IPScape"
				hql.append("select c.ciid, c.ciname from Configuration c where c.customeruniqueid=:custId")
				   .append(" and c.rootProductId ='SaaS' and c.productId=:producTypeFetched and c.citype=:ciType and c.cmdblass=:cmdbClassPCFetchedProp and c.status in ('");
				hql.append(GenericConstants.STATUSCOMMISSIONING);
				hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
				hql.append(GenericConstants.STATUSOPERATIONAL);
				hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by c.ciname asc");
				
				Query querySaas = session.createQuery(hql.toString())
												 .setString("custId", customerId)
				                                 .setString("ciType", prop.getProperty("ciType"))
				                                 .setString("producTypeFetched", productType)
				                                 .setString("cmdbClassPCFetchedProp", "SaaS Business Service");
				
				if(querySaas.list()!=null && !querySaas.list().isEmpty()){
					
					log.info("Service data found for Saas ");
					
					for(Iterator it=querySaas.iterate();it.hasNext();){
						Object[] row = (Object[]) it.next();
						serviceIdMap.put(row[0]+"", row[1]+"");
						
						log.info("getServiceQuery for Private Cloud"+ querySaas);
					}
				}
				else{
					log.debug("No Service Data found for SaaS - " + siteName);
				}
			}else if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MANDOE)){
				//When Product Type = "Private cloud" 
				hql.append("select c.ciid, c.ciname from Configuration c where c.customeruniqueid=:custId")
				   .append(" and c.rootProductId ='SaaS' and c.productId like '%Mandoe%' and c.citype=:ciType and c.cmdblass=:cmdbClassPCFetchedProp and c.status in ('");
				hql.append(GenericConstants.STATUSCOMMISSIONING);
				hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
				hql.append(GenericConstants.STATUSOPERATIONAL);
				hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by c.ciname asc");
				
				Query querySaas = session.createQuery(hql.toString())
												 .setString("custId", customerId)
				                                 .setString("ciType", prop.getProperty("ciType"))
				                                 .setString("cmdbClassPCFetchedProp", "SaaS Business Service");
				
				if(querySaas.list()!=null && !querySaas.list().isEmpty()){
					
					log.info("Service data found for Saas ");
					
					for(Iterator it=querySaas.iterate();it.hasNext();){
						Object[] row = (Object[]) it.next();
						serviceIdMap.put(row[0]+"", row[1]+"");
						
						log.info("getServiceQuery for Private Cloud"+ querySaas);
					}
				}
				else{
					log.debug("No Service Data found for SaaS - " + siteName);
				}
			}
			else{
				//When Product Type = "Private cloud" 
				hql.append("select c.ciid, c.ciname from Configuration c where c.customeruniqueid=:custId")
				   .append(" and c.rootProductId =:producTypeFetched and c.citype=:ciType and c.cmdblass=:cmdbClassPCFetchedProp and c.status in ('");
				hql.append(GenericConstants.STATUSCOMMISSIONING);
				hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
				hql.append(GenericConstants.STATUSOPERATIONAL);
				hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by c.ciname asc");
				
				Query queryPrivateCloud = session.createQuery(hql.toString())
												 .setString("custId", customerId)
				                                 .setString("ciType", prop.getProperty("ciType"))
				                                 .setString("producTypeFetched", productType)
				                                 .setString("cmdbClassPCFetchedProp", prop.getProperty("cmdbClassPrivateCloudForCI"));
				
				if(queryPrivateCloud.list()!=null && !queryPrivateCloud.list().isEmpty()){
					
					log.info("Service data found for Private Cloud ");
					
					for(Iterator it=queryPrivateCloud.iterate();it.hasNext();){
						Object[] row = (Object[]) it.next();
						serviceIdMap.put(row[0]+"", row[1]+"");
						
						log.info("getServiceQuery for Private Cloud"+ queryPrivateCloud);
					}
				}
				else{
					log.debug("No Service Data found for Private Cloud - " + siteName);
				}
				
			}
			log.info("********** Service name " +serviceIdMap + "returned from dao for the selected site name " +siteName+ "\n and product Type ::  "+productType  );
			return serviceIdMap;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getServiceFromProductTypeAndSiteID method  - " +e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction fore fetching service name");
				session.close();
			}
		}
	}

	
	public LinkedHashMap<String,String> getSRDevices(String productType, String serviceName, String selectedSubCategory) throws Exception{
		try{
			session=sessionFactory.openSession();
			
			LinkedHashMap<String, String> devicesList = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			
			log.info("getSRDevices:::: recieved productType::>> "+productType+""
					+ "\n :: recieved serviceName::: >>"+serviceName+""
							+ "\n :: recieved selectedSubCategory::>>"+selectedSubCategory);
			
			if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MNS)){
				
				query.append("select ci.ciid, ci.ciname from Configuration ci,  CIRelationship cir where cir.relationShip=:relationship and cir.dependentCIId=ci.ciid")
					 .append(" and ci.cmdblass =:cmdbClassProp and cir.baseCIName =:servicename and ci.rootProductId=:producTypeFetched and ci.status in ('")
				     .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				     .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by ci.ciname asc");
				
				Query queryMNS = session.createQuery(query.toString())
										.setString("servicename",serviceName)
										.setString("cmdbClassProp", prop.getProperty("cmdbClassCPE"))
										.setString("relationship", prop.getProperty("ciRelationshipCont"))
										.setString("producTypeFetched", productType);
			
				log.info("********Query for Fetching Devices for MNS " + queryMNS.getQueryString());
			
				if(queryMNS.list()!=null && !queryMNS.list().isEmpty()){
					
					log.info("Device data found for servicename - " + serviceName);
					
					for(Iterator it=queryMNS.iterate();it.hasNext();){
						Object[] row = 	(Object[]) it.next();
						log.info("Device : " + row[0]);
						devicesList.put(row[0]+"", row[1]+"");
					}
				}
				else{
					log.debug("No Devices found for this service name - " + serviceName);
				}
			}else{
				//When product Type ="Private Cloud"
				//derive cmdbclass on basis of subcategory selected 
				
				StringBuilder querycmdbclass = new StringBuilder();
				querycmdbclass.append("select distinct srsc.cmdbClass from ServiceRequestSubCategory srsc where srsc.subCategory=:selectedSubCategoryFetched")
							  .append(" and srsc.rootProductId=:producTypeFetched");
				
				Query querycmdbclass1 = session.createQuery(querycmdbclass.toString())
											   .setString("selectedSubCategoryFetched", selectedSubCategory)
										   	   .setString("producTypeFetched", productType);
				
				log.info("Query to fetch Cmdbclass of subcategory for fetching devices"+querycmdbclass1);
				String cmdbClass = null;
				
					if(null != querycmdbclass1.uniqueResult()){
						cmdbClass = querycmdbclass1.uniqueResult().toString();
						log.info("cmdbClass of subcategory : " + selectedSubCategory + ""
								+ "\n fetched for getting dependent devices is : " + cmdbClass + ""
										+ "\n for poroduct type ::" +productType);
					}else{
						log.info("Failing to execute cmdbclass query to fetch devices!!");
					}
				
					//derive devices on basis of cmdbclass
					query.append("select ci.ciid, ci.ciname from Configuration ci where ci.rootProductId=:producTypeFetched and ci.cmdblass=:cmdbClassFetched and ci.status in ('")
						 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				         .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE).append(" order by ci.ciname asc");
				
				Query queryPC = session.createQuery(query.toString())
									   .setString("cmdbClassFetched", cmdbClass)
						               .setString("producTypeFetched", productType);
			
				log.info("********Query for Fetching Devices for Private Cloud " + queryPC.getQueryString());
			
				if(queryPC.list()!=null && !queryPC.list().isEmpty()){
					log.info("Device data found for servicename - " + serviceName);
					for(Iterator it=queryPC.iterate();it.hasNext();){
						Object[] row = 	(Object[]) it.next();
						log.info("Device : " + row[0]);
						devicesList.put(row[0]+"", row[1]+"");
					}
				}
				else{
					log.debug("No Devices found for this service name - " + serviceName);
				}
				
			}
		
			log.info("******* Device name " +devicesList+ "returned from dao for selected service" +serviceName+ "*****");
			return devicesList;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getSRDevices method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction for fecthing device list");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getSubCategory(String productType, String category, String serviceName) throws Exception{
		try{
			session=sessionFactory.openSession();

			LinkedHashMap<String, String> subCategoryList = new LinkedHashMap<String, String>();
			
			if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MNS))
			{
				//Query to fecth cmdbclass of service recieved to fetch subcategory for MNS
				StringBuilder query = new StringBuilder();
				query.append("select distinct c.cmdblass from Configuration c where c.ciname=:serviceName and c.rootProductId=:producTypeFetched and c.status in ('")
					 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
					 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
				Query query1 = session.createQuery(query.toString())
									  .setString("serviceName",serviceName)
									  .setString("producTypeFetched", productType);
				
				String cmdbClass = null;
				if(null != query1.uniqueResult()){
					cmdbClass = query1.uniqueResult().toString();
					log.info("cmdbClass of service : " + serviceName + " fetched for getting dependent devices is : " + cmdbClass);
				}
				
			    String queryString="select s.subCategory from ServiceRequestSubCategory s where s.category=:categoryINFLIGHTMACDProp and s.cmdbClass =:cmdbClassParameter and s.rootProductId=:producTypeFetched order by s.subCategory asc";
				Query query2 = session.createSQLQuery(queryString)
				                      .setString("cmdbClassParameter",cmdbClass)
				                      .setString("categoryINFLIGHTMACDProp", prop.getProperty("categoryINFLIGHTMACD"))
				                      .setString("producTypeFetched", productType);
				
				log.info("**** Query " +query2.toString()+ " fired to fetch subgategory values for category INFLIGHTMACD "
						+ "\n and selected service name "+ serviceName+" \n whose cmdbclass is :" + cmdbClass);
				
				if(null!=query2.list() || !(query2.list().isEmpty())){					
					for(int i=0;i<query2.list().size();i++){
						String subCategory= query2.list().get(i).toString();
						log.info("*****received subcategories in Dao"+subCategory);
						subCategoryList.put(subCategory, subCategory);
					}
					
				}
				else{
					log.debug("Subcategory data not found for Category - " + category + " \n and service name - " + serviceName);
				}
			}else if(productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_WHISPIR) || productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_IPSCAPE) || productType.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MANDOE)){
				
				String queryString="select s.subCategory from ServiceRequestSubCategory s where s.category=:categoryINFLIGHTMACDProp and s.rootProductId=:producTypeFetched order by s.subCategory asc";
				Query query2 = session.createSQLQuery(queryString)
									  .setString("categoryINFLIGHTMACDProp", prop.getProperty("categoryINFLIGHTMACD"))
									  .setString("producTypeFetched", "SaaS");
				
				log.info("**** Query " +query2.toString()+ " fired to fetch subgategory values for category INFLIGHTMACD "
						+ "\n and selected service name "+ serviceName+"for SaaS");
				
				if(null!=query2.list() || !(query2.list().isEmpty())){					
					for(int i=0;i<query2.list().size();i++){
						String subCategory= query2.list().get(i).toString();
						log.info("*****received subcategories in Dao for Private Cloud"+subCategory);
						subCategoryList.put(subCategory, subCategory);
					}
					
				}
				else{
					log.debug("Subcategory data not found for Category - " + category + " \n and service name - " + serviceName);
				}
			}
			else{
				
				String queryString="select s.subCategory from ServiceRequestSubCategory s where s.category=:categoryINFLIGHTMACDProp and s.rootProductId=:producTypeFetched order by s.subCategory asc";
				Query query2 = session.createSQLQuery(queryString)
									  .setString("categoryINFLIGHTMACDProp", prop.getProperty("categoryINFLIGHTMACD"))
									  .setString("producTypeFetched", productType);
				
				log.info("**** Query " +query2.toString()+ " fired to fetch subgategory values for category INFLIGHTMACD "
						+ "\n and selected service name "+ serviceName+"for Private Cloud");
				
				if(null!=query2.list() || !(query2.list().isEmpty())){					
					for(int i=0;i<query2.list().size();i++){
						String subCategory= query2.list().get(i).toString();
						log.info("*****received subcategories in Dao for Private Cloud"+subCategory);
						subCategoryList.put(subCategory, subCategory);
					}
					
				}
				else{
					log.debug("Subcategory data not found for Category - " + category + " \n and service name - " + serviceName);
				}
				
			}
			log.info("******* SubCategory list" +subCategoryList+ "returned from dao for selected category " +category+ "*****");
			return subCategoryList;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl getSubCategory method  - " +e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction for fecthing subcategory");
				session.close();
			}
		}
	}
	
	public String fetchCategoryForMACD(String serviceName) throws Exception{
		try{
			session=sessionFactory.openSession();
		
			StringBuilder query = new StringBuilder();
			query.append("select distinct c.cmdblass from Configuration c where c.ciname=:serviceName and c.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query1 = session.createQuery(query.toString()).setString("serviceName",serviceName);
			
			log.info("Query "+query1+" to fetch cmdbclass of selectd servicename" +serviceName);
			
			String cmdbClass = null;
			if(null != query1.uniqueResult()){
				cmdbClass = query1.uniqueResult().toString();
			}
			log.info("*********** value recieved in dao for fetchCategoryForMACD  returning cmdbClass " +cmdbClass+ " for selected service name :: " +serviceName );
			return cmdbClass;
		}
		catch(Exception e){
			log.error("Error in SR DaoImpl fetchCategoryForMACD method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}

	public LinkedHashMap<String, String> getSiteNameMap(String customerId) {
		try {
			session = sessionFactory.openSession();
	
			LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select s.customersite, s.siteid from Site s where s.customeruniqueid=:custId and s.status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query1 = session.createQuery(query.toString()).setString("custId", customerId);
			
			log.info("Query to fetch getSiteNameMap::  " +query1);
			
			for (Iterator it = query1.iterate(); it.hasNext();) {
				Object[] row = (Object[]) it.next();
				siteNameIDMap.put(row[0] + "", row[0] + "");
			}
	
			log.info("******* SiteNameID list" +siteNameIDMap+ "returned from dao for customerID " +customerId+ "*****");
			return siteNameIDMap;
		} catch (Exception e) {
			log.error("Error in retrieving SiteName list "+e.getMessage());
			throw e;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}
	
	public List<String> getProductTypeList() {
		try {
			List<String> productTypeList = new ArrayList<String>();
			
			//need to add customer unique id check for subscribed servcies list
			
		      productTypeList.add("MNS");
		      productTypeList.add("Private Cloud");
		      productTypeList.add("Whispir");
		      productTypeList.add("IPScape");
		      productTypeList.add("Mandoe");
		      
		      log.info("productTypeList in dao"+ productTypeList);	
			return productTypeList;
			} catch (Exception e) {
			log.error("Error in retrieving ProductType list in dao "+e.getMessage());
			throw e;
		} 
	}
	public LinkedHashMap<String,List<String>> prodClassforSelectedProductType(String selectedProductType) {
		try {
			LinkedHashMap<String, List<String>> productClassList = new LinkedHashMap<String, List<String>>();

			log.info("prodClassforSelectedProductType:::: recieved productType::>> "+selectedProductType);
			
			StringBuilder queryProductClass = new StringBuilder();
				
			queryProductClass.append("select rootProductId,productclassificationtype from productclassification where rootProductId=:producTypeFetched").append(" order by productclassificationtype asc");
				Query queryProductClass1 = sessionFactory.getCurrentSession().createSQLQuery(queryProductClass.toString())
				                 .setString("producTypeFetched", selectedProductType);
		
					log.info("Query for Fetching productClassList for selectedProductType " + queryProductClass1.getQueryString());
					
					List productTypeList = queryProductClass1.list();
					List<String> productClassification = new ArrayList<String>();
					
					if(productTypeList!=null && !productTypeList.isEmpty()){
							log.info("productClassList data found for selectedProductType in pvcraetesrdaoimpl - " + selectedProductType);
							String  productType = null;
							for(Object obj : productTypeList)
							{
								    
									Object[] row = (Object[]) obj;
									//log.debug("productClassList ************************* : " + (String)row[1]);
									log.debug("productClassList ######################### : " + row[1].toString());
									productClassification.add((String)row[1]);
									productType = row[0].toString();
									
							}
							productClassList.put( productType, productClassification);
					}
					else
					{
						log.debug("No productClassList found for this selectedProductType in pvcraetesrdaoimpl- " + selectedProductType);
					}
					log.info("productTypeList in dao"+ productClassList);	
					return productClassList;
			} catch (Exception e) {
			log.error("Error in retrieving ProductType list in dao "+e.getMessage());
			throw e;
		} 
	}
}
