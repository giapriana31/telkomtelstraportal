package com.tt.bvdashboard.mvc.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.tt.bvdashboard.mvc.dao.BVDashboardDao;
import com.tt.bvdashboard.mvc.model.AllMapsBean;
import com.tt.bvdashboard.mvc.web.controller.BVDashBoardController;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.BVSiteManagement;
import com.tt.model.BVSubscribedServices;
import com.tt.utils.TTGenericUtils;

@Service(value = "bvDashboardService")
@Transactional
public class BVDashboardServiceImpl implements BVDashboardService 
{
	private final static Logger log = Logger.getLogger(BVDashBoardController.class);
	private BVDashboardDao bvDashboardDao;
	
	@Autowired
	@Qualifier("bvDashboardtDaoImpl")
	public void setBvDashboardDaoImpl(BVDashboardDao bvDashboardDao) {
		this.bvDashboardDao = bvDashboardDao;
	}
	
	@Override
	public AllMapsBean getSubscribedServices(String customerId) 
	{
		AllMapsBean allMapsBean = new AllMapsBean();
		Map<String, Integer> subscribedServicesMapWithColors = new HashMap<String, Integer>();
		Map<String, Integer> sitesManagementMapWithColors = new HashMap<String, Integer>();
		Map<String, Integer> subscribedServicesMap = bvDashboardDao.getSubscribedServices(customerId);
		Map<String, Integer> sitesManagementMap = bvDashboardDao.getSiteManagement(customerId);
		List<String> legendListForSS = new ArrayList<String>();
		
		int total = getTotalSize(subscribedServicesMap, sitesManagementMap);
		log.debug("Total :: " + total);
		
		List<String> colorListForSubscribedServices = getListOfColors("SS");
		int i = 0;
		for(Map.Entry<String, Integer> entry : subscribedServicesMap.entrySet())
		{
			String key = entry.getKey();
			int value = entry.getValue();
			log.debug("SS Key :: " + key);
			log.debug("SS Value :: " + value);
			
			String[] keyColor = key.split("-");
			legendListForSS.add(key);
			subscribedServicesMapWithColors.put(keyColor[1], value);
		}
		
		List<String> colorListForSitesManagement = getLegendList("SM");
		i=0;
		for(Map.Entry<String, Integer> entry : sitesManagementMap.entrySet())
		{
			String key = entry.getKey();
			int value = entry.getValue();
			log.debug("SM Key :: " + key);
			log.debug("SM Value :: " + value);
			log.debug("SM Color :: " + colorListForSitesManagement.get(i));
			
			//String[] keyColor = colorListForSitesManagement.get(i).split("-");
			
			for(String colorSite : colorListForSitesManagement)
			{
				String[] keyAndColor = colorSite.split("-");
				
				if(keyAndColor[0].equalsIgnoreCase(key))
				{
					String color = keyAndColor[1];
					sitesManagementMapWithColors.put(color, value);
				}
				else
				{
					continue;
				}
			}
		}
		
		List<String> legendListForSM = getLegendList("SM");
		
		List<String> colorListFinal = new ArrayList<String>();
		List<String> colorListSiteManagement = getListOfColors("SM");
		colorListFinal.addAll(colorListForSubscribedServices);
		colorListFinal.addAll(colorListSiteManagement);
		log.debug("size of union of both lists :: " + colorListFinal.size());
		
		Set<String> keySetOfSS = subscribedServicesMapWithColors.keySet();
		Set<String> keySetOfSM = sitesManagementMapWithColors.keySet();
		log.debug("key set of SS :: " + keySetOfSS);
		log.debug("key set for SM :: " + keySetOfSM);
			
		for(String color : colorListFinal)
		{
			if(!keySetOfSS.contains(color))
			{
				subscribedServicesMapWithColors.put(color, 0);
			}
			if(!keySetOfSM.contains(color))
			{
				sitesManagementMapWithColors.put(color, 0);
			}
		}
		
		log.debug("size of SM Map modified :: " + subscribedServicesMapWithColors.size());
		log.debug("size of SS Map modified :: " + sitesManagementMapWithColors.size());
		
		allMapsBean.setLegendListForSM(legendListForSM);
		allMapsBean.setLengendListForSS(legendListForSS);
		allMapsBean.setSitesManagementMap(sitesManagementMapWithColors);
		allMapsBean.setSubscribedServicesMap(subscribedServicesMapWithColors);
		allMapsBean.setColorList(colorListFinal);
		
		return allMapsBean;
	}

	private List<String> getLegendList(String type) 
	{
		List<String> legend = new ArrayList<String>();
		
		if(type.equalsIgnoreCase("SS"))
		{
			legend.add("MNS-#9C4BE7");
			legend.add("Private Cloud-#CD5C5C");
			legend.add("SaaS-#8B008B");
		}
		else if(type.equalsIgnoreCase("SM"))
		{
			legend.add("Delivery-#4B0082");
			legend.add("Operational-#89C35C");
			legend.add("Unmanaged-#DB4437");
		}
		
		return legend;
	}

	private int getTotalSize(Map<String, Integer> subscribedServicesMap, Map<String, Integer> sitesManagementMap) 
	{
		int subscribedServicesSize = 0;
		int siteManagementSize = 0;
				
		if(subscribedServicesMap != null)
			subscribedServicesSize = subscribedServicesMap.size();
		
		if(sitesManagementMap != null)
			siteManagementSize = sitesManagementMap.size();
		
		return subscribedServicesSize + siteManagementSize;
	}

	private List<String> getListOfColors(String type) 
	{
		List<String> colorList = new ArrayList<String>();
		if(type.equalsIgnoreCase("SS"))
		{
			colorList.add("#CD5C5C");
			colorList.add("#9C4BE7");
			colorList.add("#006400");
			colorList.add("#8B008B");
			colorList.add("#FF69B4");
		}
		else if(type.equalsIgnoreCase("SM"))
		{
			colorList.add("#4B0082");
			colorList.add("#89C35C");
			colorList.add("#DB4437");
		}
		return colorList;
	}

	@Override
	public JSONArray getSitesManagementTableData(String customerId, List<String> regionFiltersList, String regionSearchTerm) 
	{
		log.info("Class:BVDashboardServiceImpl Method:getSitesManagementTableData Start");
		JSONArray jsonData = JSONFactoryUtil.createJSONArray();
		JSONArray rowJson = null;
		
		List<BVSiteManagement> resultList = bvDashboardDao.getSiteManagementTableData(customerId, regionFiltersList, regionSearchTerm);
		if(resultList != null && !resultList.isEmpty())
		{
			/*for(Object row : resultList)
			{
				rowJson = JSONFactoryUtil.createJSONArray();
				Object[] data = (Object[]) row;
				
				for(int i=0; i<data.length; i++)
				{
					log.debug("data index :: " + i + ", data :: " + data[i].toString());
					rowJson.put(data[i].toString());
				}
				jsonData.put(rowJson);
			}*/
			
			
			for(BVSiteManagement siteManagementRow : resultList)
			{
				rowJson = JSONFactoryUtil.createJSONArray();
				
				rowJson.put(siteManagementRow.getRegion());
				rowJson.put(siteManagementRow.getSitename());
				rowJson.put(siteManagementRow.getServicetier());
				rowJson.put(siteManagementRow.getStatus());
				
				if(siteManagementRow.getPerformance().equalsIgnoreCase(GenericConstants.NA_VALUE))
				{
					rowJson.put(GenericConstants.HYPHEN);
				}
				else
				{
					rowJson.put(siteManagementRow.getPerformance()+"% ("+siteManagementRow.getCommitment()+"%)");
				}
				
				if(siteManagementRow.getNetworkAverage().equalsIgnoreCase(GenericConstants.NA_VALUE))
				{
					rowJson.put(GenericConstants.NA);
				}
				else if(siteManagementRow.getNetworkAverage().equalsIgnoreCase(GenericConstants.ZERO_VALUE))
				{
					rowJson.put(GenericConstants.HYPHEN);
				}
				else
				{
					rowJson.put(siteManagementRow.getNetworkAverage());
				}
				
				if(siteManagementRow.getNetworkPeak().equalsIgnoreCase(GenericConstants.NA_VALUE))
				{
					rowJson.put(GenericConstants.NA);
				}
				else if(siteManagementRow.getNetworkPeak().equalsIgnoreCase(GenericConstants.ZERO_VALUE))
				{
					rowJson.put(GenericConstants.HYPHEN);
				}
				else
				{
					rowJson.put(siteManagementRow.getNetworkPeak());
				}
				
				if(siteManagementRow.getCPEAverage().equalsIgnoreCase(GenericConstants.NA_VALUE))
				{
					rowJson.put(GenericConstants.NA);
				}
				else if(siteManagementRow.getCPEAverage().equalsIgnoreCase(GenericConstants.ZERO_VALUE))
				{
					rowJson.put(GenericConstants.HYPHEN);
				}
				else
				{
					rowJson.put(siteManagementRow.getCPEAverage());
				}
				
				if(siteManagementRow.getCPEPeak().equalsIgnoreCase(GenericConstants.NA_VALUE))
				{
					rowJson.put(GenericConstants.NA);
				}
				else if(siteManagementRow.getCPEPeak().equalsIgnoreCase(GenericConstants.ZERO_VALUE))
				{
					rowJson.put(GenericConstants.HYPHEN);
				}
				else
				{
					rowJson.put(siteManagementRow.getCPEPeak());
				}
				
				jsonData.put(rowJson);
			}
			
			
			log.debug("Json Data :: " + jsonData.toString());
		}
		log.info("Class:BVDashboardServiceImpl Method:getSitesManagementTableData End");
		return jsonData;
	}
	
	@Override
	public JSONArray getSubscribedServicesTableData(String customerId, List<String> servicesFiltersList, HttpServletRequest request) throws PortalException, SystemException 
	{
		log.info("Class:BVDashboardServiceImpl Method:getSubscribedServicesTableData Start");
		
		JSONArray finalJson = JSONFactoryUtil.createJSONArray();
		JSONArray rowJson;
		
		List<BVSubscribedServices> resultList = (List<BVSubscribedServices>) bvDashboardDao.getSubscribedServicesTableData(customerId, servicesFiltersList);
		
		
		if(resultList != null && !resultList.isEmpty())
		{
			for(BVSubscribedServices row : resultList)
			{
				rowJson = JSONFactoryUtil.createJSONArray();
				
				rowJson.put(row.getProductname());
				
				String contractRemainingTime = row.getContractremainingtime();
				String contractDuration = row.getContractduration();
			
				String contractTerm = "";
				
				if(contractDuration.equalsIgnoreCase(GenericConstants.NA))
				{
					if(GenericConstants.NA_CONTRACT_REMAINING_TIME.equalsIgnoreCase(contractRemainingTime))
					{
						contractRemainingTime = "";
						contractTerm = contractDuration;
					}
					else if(GenericConstants.BLANK.equalsIgnoreCase(contractRemainingTime))
					{
						contractTerm = contractDuration;
					}
					else if(GenericConstants.EXPIRED.equalsIgnoreCase(contractRemainingTime))
					{
						contractTerm = contractDuration;
					}
					else 
					{
						contractRemainingTime = "-";
						contractTerm = contractRemainingTime + " (" + contractDuration + ")";
					}
				}
				else
				{
					
					
					if(GenericConstants.NA_CONTRACT_REMAINING_TIME.equalsIgnoreCase(contractRemainingTime))
					{
						contractRemainingTime = "-";
						contractDuration = convertDaystoMonthsInDecimal(contractDuration);
						if(Float.parseFloat(contractDuration) == Math.floor(Float.parseFloat(contractDuration))){
							
							contractDuration=""+Math.round(Double.parseDouble(contractDuration));
						
						}
						contractTerm = contractRemainingTime + " (" + contractDuration + ")";
					}
					else if(GenericConstants.BLANK.equalsIgnoreCase(contractRemainingTime))
					{
						contractRemainingTime = "-";
						contractDuration = convertDaystoMonthsInDecimal(contractDuration);
						if(Float.parseFloat(contractDuration) == Math.floor(Float.parseFloat(contractDuration))){
							
							contractDuration=""+Math.round(Double.parseDouble(contractDuration));
						
						}
						contractTerm = contractRemainingTime + "(" + contractDuration + ")";
					}
					else if(GenericConstants.EXPIRED.equalsIgnoreCase(contractRemainingTime))
					{
						contractDuration = convertDaystoMonthsInDecimal(contractDuration);
						
						if(Float.parseFloat(contractDuration) == Math.floor(Float.parseFloat(contractDuration))){
							
							contractDuration=""+Math.round(Double.parseDouble(contractDuration));
						
						}
						contractTerm = GenericConstants.EXPIRED + " (" + contractDuration +")";
					}
					else 
					{
						contractDuration = convertDaystoMonthsInDecimal(contractDuration);
						contractRemainingTime = convertDaystoMonths(contractRemainingTime);
						if(Float.parseFloat(contractDuration) == Math.floor(Float.parseFloat(contractDuration))){
							
							contractDuration=""+Math.round(Double.parseDouble(contractDuration));
						
						}
				
						contractTerm = contractRemainingTime + " " + GenericConstants.MONTHS + " (" + contractDuration + ")";
					}
				}
				
				rowJson.put(contractTerm);
				rowJson.put(row.getStatus());
				rowJson.put(row.getCapacity());
				
				if(GenericConstants.NA_BV_SS_PERFORMANCE.equalsIgnoreCase(row.getPerformance()))
				{
					rowJson.put(GenericConstants.NA);
				}
				else
				{
					
					if(row.getPerformance().equalsIgnoreCase("NA")||row.getCommitment().equalsIgnoreCase("NA")){
						
						rowJson.put("NA");
					}else{
					rowJson.put(row.getPerformance()+"% ("+row.getCommitment()+"%)");
					}
				}
				
				
				/* if user role is EndCustomerAdmin */
				boolean isEndCustomerAdmin = TTGenericUtils.checkRolePortallTTAdmin(request);
				String isEndCustomerAdminString = String.valueOf(isEndCustomerAdmin);
				String finalString = row.getContact() + "-" + isEndCustomerAdminString;
				
				rowJson.put(finalString);
				
				finalJson.put(rowJson);
			}
			
			log.debug("Final Json :: " + finalJson);
		}
		
		log.info("Class:BVDashboardServiceImpl Method:getSubscribedServicesTableData End");
		return finalJson;
	}
	
	private String convertDaystoMonths(String numberOfDays)
	{
		int durationInDays = Integer.parseInt(numberOfDays);
		int contractDurationInMonths = durationInDays/30;
		
		String months = String.valueOf(contractDurationInMonths);
		return months;
	}
	
	private String convertDaystoMonthsInDecimal(String numberOfDays)
	{
		float durationInDays = Float.parseFloat(numberOfDays);
		float contractDurationInMonths = durationInDays/(float)30;
		
		String months = String.format("%.1f",contractDurationInMonths);
		return months;
	}

	@Override
	public List<String> getServiceFilters(String customerId){
		List<String> servicesList = null;
		servicesList = bvDashboardDao.getServiceFilters(customerId);
		
		return servicesList;
	}

	@Override
	public List<String> getRegionFilters(String customerId) {
		List<String> regionList = null;
		regionList = bvDashboardDao.getRegionFilters(customerId);
		
		return regionList;
	}
	
	@Override
	public JSONObject getAddContact(String username , String servciename, String customerId){
		
		log.info("Class : BVDashboradServiceImpl, Method: getAddContact, Start >>>>>"
				+ " Parameters Recieved:: Username: "+username+ "ServiceName: "+servciename+ "customerID: "+customerId);
		
		String columnName = GenericConstants.PRODUCTTYPE_MNS;
		
		if (servciename.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MNS))
		{
			columnName = GenericConstants.MNS_CONTACTPERSON;
		}
		else if (servciename.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_PRIVATECLOUD))
		{
			columnName = GenericConstants.PRIVATECLOUD_CONTACTPERSON;
		}
		else if (servciename.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_UC))
		{
			columnName = GenericConstants.UC_CONTACTPERSON;
		}
		else if (servciename.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_WHISPIR))
		{
			columnName = GenericConstants.WHISPIR_CONTACTPERSON;
		}
		else if (servciename.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_IPSCAPE))
		{
			columnName = GenericConstants.IPSCAPE_CONTACTPERSON;
		}
		else if (servciename.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_MANDOE))
		{
			columnName = GenericConstants.MANDOE_CONTACTPERSON;
		}
		else if (servciename.equalsIgnoreCase(GenericConstants.PRODUCTTYPE_SECURITY))
		{
			columnName = GenericConstants.SECURITY_CONTACTPERSON;
		}

		String updateStatusForCustomer = bvDashboardDao.getAddContactInCustomers(columnName, username , servciename,customerId);
		
		log.info("Recieved JsonObject from bvDashboardDao.getAddContactInCustomers:: "+updateStatusForCustomer.toString());
		
		JSONObject updateStatusJsonObject = JSONFactoryUtil.createJSONObject();
		updateStatusJsonObject.put("Status", updateStatusForCustomer);
	
		log.info("Class : BVDashboradServiceImpl, Method: getAddContact, End");
		
		return updateStatusJsonObject;
	}
}
