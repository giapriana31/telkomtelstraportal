package com.tt.bvdashboard.mvc.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONObject;
import com.tt.bvdashboard.mvc.model.AllMapsBean;

public interface BVDashboardService 
{
	public AllMapsBean getSubscribedServices(String customerId);
	public JSONArray getSitesManagementTableData(String customerId, List<String> regionFiltersList, String regionSearchTerm);
	public JSONArray getSubscribedServicesTableData(String customerId, List<String> servicesFiltersList, HttpServletRequest request) throws PortalException, SystemException;
	public List<String> getServiceFilters(String customerId);
	public List<String> getRegionFilters(String customerId);
	public JSONObject getAddContact(String username , String servciename, String customerId);
}
