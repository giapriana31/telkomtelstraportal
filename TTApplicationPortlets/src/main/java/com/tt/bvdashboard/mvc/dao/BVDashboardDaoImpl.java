package com.tt.bvdashboard.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.bvdashboard.mvc.web.controller.BVDashBoardController;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.BVSiteManagement;
import com.tt.model.BVSubscribedServices;

@Repository(value = "bvDashboardtDaoImpl")
@Transactional
public class BVDashboardDaoImpl implements BVDashboardDao 
{
	private final static Logger log = Logger.getLogger(BVDashBoardController.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private static final String CUSTOMERID = "customerId";
	
	@Override
	public Map<String, Integer> getSubscribedServices(String customerId) 
	{
		log.info("Class:BVDashboardDaoImpl, Method:getSubscribedServices Start");
		
		Map<String, Integer> subscribedServicesMap = new HashMap<String, Integer>();
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT ci.rootProductId, pt.productcolor, count(1) FROM configurationitem ci, (select distinct(rootproductname), productcolor from producttype) pt ");
		queryString.append("where citype='Service' and customeruniqueid=:customerId and ci.rootProductId = pt.rootproductname ");
		queryString.append("group by rootProductId order by rootProductId ");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryString.toString());
		query.setString(CUSTOMERID, customerId);
		List resultList = query.list();
		
		if(resultList != null && !resultList.isEmpty())
		{
			for(Object row : resultList)
			{
				Object[] data = (Object[]) row;
				subscribedServicesMap.put(data[0].toString() + "-" + data[1].toString(), Integer.parseInt(data[2].toString()));
			}
		}
		log.debug("Size of subscribed services Map is " + subscribedServicesMap != null ? "" + subscribedServicesMap.size() : "--map is null--");
		
		log.info("Class:BVDashboardDaoImpl, Method:getSubscribedServices End");
		
		return subscribedServicesMap;
	}

	@Override
	public Map<String, Integer> getSiteManagement(String customerId) 
	{
		log.info("Class:BVDashboardDaoImpl, Method:getSiteManagement Start");
		Map<String, Integer> sitesManagementMap = new HashMap<String, Integer>();
		
		StringBuilder queryString = new StringBuilder();
		
		queryString.append("SELECT sitestatus, sitecount from (SELECT customeruniqueid, status as sitestatus, count(1) as sitecount FROM site ");
		queryString.append("where status in ('Operational') and (isManaged in ('Yes','') or isManaged is null) group by status, customeruniqueid ");
		queryString.append("UNION ");
		queryString.append("SELECT customeruniqueid, 'Delivery' as status, count(1) as sitecount from site ");
		queryString.append("where status in ('Order Received','Detailed Design','Procurement','Delivery & Activation','Monitoring') ");
		queryString.append("and (isManaged in ('Yes','') or isManaged is null) group by customeruniqueid ");
		queryString.append("UNION ");
		queryString.append("select customeruniqueid, 'Unmanaged' as sitestatus, count(1) as sitecount from site ");
		queryString.append("where isManaged = 'No' or (status is null or status='') group by customeruniqueid) unionData ");
		queryString.append("where customeruniqueid=:customerId");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryString.toString());
		query.setString(CUSTOMERID, customerId);
		List resultList = query.list();
		
		if(resultList != null && !resultList.isEmpty())
		{
			for(Object row : resultList)
			{
				Object[] data = (Object[]) row;
				
				if(null == data[0].toString() || GenericConstants.BLANK.equalsIgnoreCase(data[0].toString()))
				{
					sitesManagementMap.put(GenericConstants.STATUSUNMANAGED, Integer.parseInt(data[1].toString()));
				}
				else
				{
					sitesManagementMap.put(data[0].toString(), Integer.parseInt(data[1].toString()));
				}
			}
		}
		log.debug("Size of the Sites Management map is " + sitesManagementMap != null ? "" + sitesManagementMap.size() : "--map is null--");
		log.info("Class:BVDashboardDaoImpl, Method:getSiteManagement End");
		return sitesManagementMap;
	}

	@Override
	public List getSiteManagementTableData(String customerId, List<String> regionFiltersList, String regionSearchTerm) 
	{	
		String statusString = "";
		log.info("Class:BVDashboardDaoImpl, Method:getSiteManagementTableData Start");
		
		/* Search Term for region, site name and service tier */
		if("Delivery".contains(regionSearchTerm))
		{
			statusString = "'Order Received','Detailed Design','Procurement','Delivery & Activation','Monitoring'";
		}
		else if("Unmanaged".contains(regionSearchTerm))
		{
			statusString = "'',NULL";
		}
		else
		{
			statusString = "'"+regionSearchTerm+"'";
		}
		log.info("Status String after editing for SQL::"+ statusString);
		
		
		String regionFilterQueryString ="";
		for(String service: regionFiltersList)
		{
			String tempService = "'"+service+"',";
			regionFilterQueryString = regionFilterQueryString+tempService;
		}
		String comaTrimmedString = regionFilterQueryString.substring(0, regionFilterQueryString.length()-1);
		
		log.info("The services filter query::"+comaTrimmedString);
		StringBuilder query = new StringBuilder();
		if(regionSearchTerm!=null && !(regionSearchTerm.equalsIgnoreCase(GenericConstants.BLANK)) && !(regionSearchTerm.equalsIgnoreCase("null")) && (!((regionFiltersList.size()==1 && regionFiltersList.get(0).equals("")) || regionFiltersList.contains("all"))))
		{	
			log.info("--Both Search Term and Filter selected--");
			query.append("FROM BVSiteManagement WHERE customerid=:customerId AND region in("+comaTrimmedString+") ");
			query.append("AND (region like '%"+regionSearchTerm+"%' OR sitename like '%"+regionSearchTerm+"%' OR servicetier like '%"+regionSearchTerm+"%' ");
			query.append("OR status in("+statusString+")) "); 
			query.append("ORDER BY performance asc");
					
			log.debug("Query 1 :: " + query.toString());
		}
		else if(!((regionFiltersList.size()==1 && regionFiltersList.get(0).equals("")) || regionFiltersList.contains("all")) && !(regionSearchTerm!=null && !(regionSearchTerm.equalsIgnoreCase(GenericConstants.BLANK)) && !(regionSearchTerm.equalsIgnoreCase("null"))))
		{	
			log.info("--Filter selected--");
			query.append("FROM BVSiteManagement WHERE customerid=:customerId ");
			query.append("AND region in("+comaTrimmedString+") ORDER BY performance asc");
			
			log.debug("Query 2 :: " + query.toString());
		}
		
		else if(((regionFiltersList.size()==1 && regionFiltersList.get(0).equals("")) || regionFiltersList.contains("all")) && regionSearchTerm!=null && !(regionSearchTerm.equalsIgnoreCase(GenericConstants.BLANK)) && !(regionSearchTerm.equalsIgnoreCase("null")))
		{	
			log.info("--Search Term--");
			
			query.append("FROM BVSiteManagement WHERE customerid=:customerId ");
			query.append("AND (region like '%"+regionSearchTerm+"%' OR sitename like '%"+regionSearchTerm+"%' OR servicetier like '%"+regionSearchTerm+"%' ");
			query.append("OR status in("+statusString+")) "); 
			query.append("ORDER BY performance asc");
			
			log.debug("Query 3 :: " + query.toString());
		}
		else if((regionFiltersList.size() == 1 && regionFiltersList.get(0).equals("") && !regionFiltersList.contains("all")) && (regionSearchTerm == null || regionSearchTerm.equalsIgnoreCase(GenericConstants.BLANK) || regionSearchTerm.equalsIgnoreCase("null"))) 
		{
			log.info("-- no filters and no search term --");
			List<BVSiteManagement> blankList = new ArrayList<BVSiteManagement>();
			return blankList;
		}
		else
		{	
			log.info("--Nothing Selected--");
			
			query.append("FROM BVSiteManagement WHERE customerid=:customerId ORDER BY performance asc");
			log.debug("Query 4 :: " + query.toString());
		}
		
		log.debug("Query :: " + query.toString());
		Query sqlQuery = sessionFactory.getCurrentSession().createQuery(query.toString());
		sqlQuery.setString(CUSTOMERID, customerId);
		
		List<BVSiteManagement> resultList = sqlQuery.list();
		
		log.info("Class:BVDashboardDaoImpl, Method:getSiteManagementTableData End");
		return resultList;
	}
	
	@Override
	public List<BVSubscribedServices> getSubscribedServicesTableData(String customerId, List<String> servicesFiltersList) 
	{	
		log.info("Class:BVDashboardDaoImpl, Method:getSubscribedServicesTableData Start");
		String servicesFilterQueryString ="";
		for(String service: servicesFiltersList){
			String tempService = "'"+service+"',";
			servicesFilterQueryString = servicesFilterQueryString+tempService;
		}
		String comaTrimmedString = servicesFilterQueryString.substring(0, servicesFilterQueryString.length()-1);
		
		log.info("The services filter query::"+comaTrimmedString);
		
		StringBuilder queryString = new StringBuilder();
	
		
		if(servicesFiltersList.contains("all"))
		{
			queryString.append("FROM BVSubscribedServices where customeruniqueid=:customerId ORDER BY performance, capacity");
			
		}
		else if(servicesFiltersList.size()==1 && servicesFiltersList.get(0).equals("") && !servicesFiltersList.contains("all"))
		{
			return new ArrayList<BVSubscribedServices>();
		}
		else
		{
			queryString.append("FROM BVSubscribedServices where customeruniqueid=:customerId and productname in("+comaTrimmedString+") ORDER BY performance, capacity");
		}
		log.debug("Query :: " + queryString.toString());
		Query sqlQuery = sessionFactory.getCurrentSession().createQuery(queryString.toString());
		sqlQuery.setString(CUSTOMERID, customerId);
		
		List<BVSubscribedServices> resultList = sqlQuery.list();
		
		log.debug("Result Set :: " + resultList.size());
		
		log.info("Class:BVDashboardDaoImpl, Method:getSubscribedServicesTableData End");
		return resultList;
	}
	
	@Override
	public List<String> getServiceFilters(String customerId) {
		List<String> servicesList = new ArrayList<String>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT distinct p.productname FROM producttype p");
		
		log.debug("Query ::" + query.toString());
		Query sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(query.toString());
		
		List resulList = sqlQuery.list();
		
		for(Object service:resulList){
			servicesList.add(service.toString());
		}
		log.info("services list::"+servicesList);
		return servicesList;
	}

	@Override
	public List<String> getRegionFilters(String customerId) {
		List<String> regionsList = new ArrayList<String>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT  distinct s.stateprovince FROM site s where s.customeruniqueid =:customerId order by s.stateprovince asc");
		
		log.debug("Query ::" + query.toString());
		Query sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(query.toString());
		sqlQuery.setString(CUSTOMERID, customerId);
		
		List resulList = sqlQuery.list();
		
		for(Object service:resulList){
			regionsList.add(service.toString());
		}
		log.info("services list::"+regionsList);
		return regionsList;
	}
	
	@Override
	public String getAddContactInCustomers(String columnname , String username, String servicename , String customerId){
		
		log.info("Class : BVDashboradDaoImpl, Method: getAddContactInCustomers, Start"
				+ " Parameters Recieved:: Columnname: "+columnname+ "UserName: "+username+ "customerID: "+customerId);
		
		StringBuilder updateCustomersQuery = new StringBuilder();
		updateCustomersQuery.append("UPDATE customers SET ")
							.append(columnname+"="+ "'" + username+"'")
							.append("WHERE customeruniqueid=:customerId");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(updateCustomersQuery.toString());
		query.setString(CUSTOMERID, customerId);
		
		log.info("Update Query on Customers table >>  : " +query);
		
		int i = query.executeUpdate();
		log.debug("Number of Rows updated in Customers for Add contact name: "+i);
		
		String updateStatus = "Failure";
		
		if(i > 0){
			
			updateStatus = "Success";
		}
		else{
			updateStatus = "Failure";
		}
		
		log.debug("Update Status Value: "+updateStatus);
		
		//Updating the BVSubscribedServices
		StringBuilder updateBVSubscribedServicesQuery = new StringBuilder();
		updateBVSubscribedServicesQuery.append("UPDATE bvsubscribedservices SET ")
							.append("contact= "+ "'" + username+"' ")
							.append("WHERE customeruniqueid=:customerId ")
							.append("AND productname= "+ "'" +servicename+"'");
		
		Query query2 = sessionFactory.getCurrentSession().createSQLQuery(updateBVSubscribedServicesQuery.toString());
		query2.setString(CUSTOMERID, customerId);
		
		query2.executeUpdate();
		
		log.info("Update Query on bvsubscribedservices table >>  : " +query2);
		
		log.info("Class : BVDashboradDaoImpl, Method: getAddContactInCustomers, End");
		
		return updateStatus;
	}
}
