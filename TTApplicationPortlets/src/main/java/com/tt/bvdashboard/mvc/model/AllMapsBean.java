package com.tt.bvdashboard.mvc.model;

import java.util.List;
import java.util.Map;

public class AllMapsBean {
	private Map<String, Integer> subscribedServicesMap;
	private Map<String, Integer> sitesManagementMap;
	private List<String> lengendListForSS;
	private List<String> legendListForSM;
	private List<String> colorList;
	
	public Map<String, Integer> getSubscribedServicesMap() {
		return subscribedServicesMap;
	}

	public void setSubscribedServicesMap(
			Map<String, Integer> subscribedServicesMap) {
		this.subscribedServicesMap = subscribedServicesMap;
	}

	public Map<String, Integer> getSitesManagementMap() {
		return sitesManagementMap;
	}

	public void setSitesManagementMap(Map<String, Integer> sitesManagementMap) {
		this.sitesManagementMap = sitesManagementMap;
	}

	public List<String> getLengendListForSS() {
		return lengendListForSS;
	}

	public void setLengendListForSS(List<String> lengendListForSS) {
		this.lengendListForSS = lengendListForSS;
	}

	public List<String> getLegendListForSM() {
		return legendListForSM;
	}

	public void setLegendListForSM(List<String> legendListForSM) {
		this.legendListForSM = legendListForSM;
	}

	public List<String> getColorList() {
		return colorList;
	}

	public void setColorList(List<String> colorList) {
		this.colorList = colorList;
	}
}
