package com.tt.bvdashboard.mvc.dao;

import java.util.List;
import java.util.Map;

public interface BVDashboardDao 
{
	public Map<String, Integer> getSubscribedServices(String customerId);
	public Map<String, Integer> getSiteManagement(String customerId);
	public List getSiteManagementTableData(String customerId, List<String> regionFiltersList, String regionSearchTerm);
	public List getSubscribedServicesTableData(String customerId, List<String> servicesFiltersList);
	public List<String> getServiceFilters(String customerId);
	public List<String> getRegionFilters(String customerId);
	public String getAddContactInCustomers(String columnname , String username, String servicename ,String customerId);
}
