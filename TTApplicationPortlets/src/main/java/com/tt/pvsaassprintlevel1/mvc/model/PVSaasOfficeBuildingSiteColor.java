package com.tt.pvsaassprintlevel1.mvc.model;


public class PVSaasOfficeBuildingSiteColor implements
	Comparable<PVSaasOfficeBuildingSiteColor> {

		private String deliveryStage;
		private String siteName;
		private String colorCode;
		private String siteTier;
		private String ciName;

		public String getSiteTier() {
			return siteTier;
		}

		public void setSiteTier(String siteTier) {
			this.siteTier = siteTier;
		}

		public void setCiName(String ciName) {
			this.ciName = ciName;
		}	

		public String getCiName() {
			return ciName;
		}	
		
		public String getDeliveryStage() {
			return deliveryStage;
		}

		public void setDeliveryStage(String deliveryStage) {
			this.deliveryStage = deliveryStage;
		}

		public String getSiteName() {
			return siteName;
		}

		public void setSiteName(String siteName) {
			this.siteName = siteName;
		}

		public String getColorCode() {
			return colorCode;
		}

		public void setColorCode(String colorCode) {
			this.colorCode = colorCode;
		}

		@Override
		public int compareTo(PVSaasOfficeBuildingSiteColor buildingSiteColor) {
			int priorityId = Integer.parseInt(((PVSaasOfficeBuildingSiteColor) buildingSiteColor).getDeliveryStage());
			return (int) (priorityId - Integer.parseInt(((PVSaasOfficeBuildingSiteColor) buildingSiteColor).getDeliveryStage()));
		}

}
