package com.tt.pvsaassprintlevel1.mvc.model;

import java.util.List;



public class PVSaaSLAList implements Comparable<PVSaaSLAList>{

	private int id;
	private String siteTier;
	private String siteCount;
	private List<PVSaaSLAList> slaColorList;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiteTier() {
		return siteTier;
	}
	public void setSiteTier(String siteTier) {
		this.siteTier = siteTier;
	}
	public String getSiteCount() {
		return siteCount;
	}
	public void setSiteCount(String siteCount) {
		this.siteCount = siteCount;
	}
	public List<PVSaaSLAList> getSlaColorList() {
		return slaColorList;
	}
	public void setSlaColorList(List<PVSaaSLAList> slaColorList) {
		this.slaColorList = slaColorList;
	}
	
	@Override
	public  int compareTo(PVSaaSLAList slaList) {
		int siteTierId = ((PVSaaSLAList) slaList).getId();
		return (int) (siteTierId - this.id);
	}
	
	
}
