package com.tt.pvsaassprintlevel1.mvc.service.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tt.pvsaassprintlevel1.mvc.dao.PVSaasSprintLevel1Dao;

@Service(value = "pvSaasSprintLevel1Manager")
@Transactional

public class PVSaasSprintLevel1Manager {

	private PVSaasSprintLevel1Dao pvSaasSprintLevel1Dao;

	
	@Autowired
	@Qualifier(value="pvSaasSprintLevel1Dao")
	public void PvSaasSprintLevel1Dao(PVSaasSprintLevel1Dao pvSaasSprintLevel1Dao) {
		this.pvSaasSprintLevel1Dao = pvSaasSprintLevel1Dao;
	}
	
	public String getSaasProduct(String customerId){
		return pvSaasSprintLevel1Dao.getSaasProduct(customerId);
	}	
	

}
