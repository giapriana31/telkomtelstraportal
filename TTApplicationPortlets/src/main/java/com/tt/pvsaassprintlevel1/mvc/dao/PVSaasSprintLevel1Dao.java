package com.tt.pvsaassprintlevel1.mvc.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository(value="pvSaasSprintLevel1Dao")
@Transactional

public class PVSaasSprintLevel1Dao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public String getSaasProduct(String customerId){
		JSONObject obj;
		ArrayList<JSONObject> JSONObjArray = new ArrayList<JSONObject>();
		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT a.delivery_stage,(case ");
		sb.append(" when b.productId like '%Whispir%' then 'Multi Channel Communication' ");
		sb.append(" when b.productId like '%Mandoe%' then 'Digital Proximity' ");
		sb.append(" when b.productId like '%IPScape%' then 'Cloud Contact Centre' ");
		sb.append(" else b.productId ");
		sb.append(" end) as productName,b.ciname from  ");
        sb.append(" (select ciid,delivery_stage from( ");
        sb.append(" SELECT ciid,delivery_stage FROM configurationitem WHERE customeruniqueid = '" + customerId + "' AND cmdbclass='saas Business Service' ");
        sb.append(" order by ciid,delivery_stage desc ");
        sb.append(" ) a GROUP BY ciid)a LEFT JOIN ");
        sb.append(" configurationitem b ");
        sb.append(" ON a.ciid = b.ciid and a.delivery_stage = b.delivery_stage ");
        sb.append(" WHERE customeruniqueid = '" + customerId + "' AND cmdbclass='saas Business Service' ");
        sb.append(" order by productName,delivery_stage ");
		
		Query query= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
		
		if (query.list()!=null && query.list().size()>0) {
			for (Object[] productlist : (List<Object[]>)query.list()) {
				if (productlist[0]!=null) {
					obj = new JSONObject();
					obj.put("deliverystage", productlist[0].toString());
					obj.put("productname", productlist[1].toString());
					obj.put("ciname", productlist[2].toString());
					obj.put("colour", colorNameToCode(productlist[0].toString()));
					JSONObjArray.add(obj);
				}
				
			}
		}
		return JSONObjArray.toString();
	}
	
	private String colorNameToCode(String color) 
	{
		if (color.equalsIgnoreCase("6"))
			return "background-color:rgb(137, 195, 92)";
		else if (color.equalsIgnoreCase("5"))
			return "background-color:rgb(250, 128, 114)";
		else if (color.equalsIgnoreCase("4"))
			return "background-color:rgb(0, 176, 240)";
		else if (color.equalsIgnoreCase("3"))
			return "background-color:rgb(112, 48, 160)";
		else if (color.equalsIgnoreCase("2"))
			return "background-color:rgb(164, 8, 0)";
		else if (color.equalsIgnoreCase("1"))
			return "background-color:rgb(0, 0, 0)";
		else
			return "background-color:rgb(204,204,204)";
	}
	

}
