package com.tt.pvsaassprintlevel1.mvc.model;

public class PVSaasDrillDownDetails
{
  private String ciid;
  private String ciname;
  private String deliveryStage;
  
  public String getCiid()
  {
    return this.ciid;
  }
  
  public void setCiid(String ciid)
  {
    this.ciid = ciid;
  }
  
  public String getCiname()
  {
    return this.ciname;
  }
  
  public void setCiname(String ciname)
  {
    this.ciname = ciname;
  }
  
  public String getDeliveryStage()
  {
    return this.deliveryStage;
  }
  
  public void setDeliveryStage(String deliveryStage)
  {
    this.deliveryStage = deliveryStage;
  }
}
