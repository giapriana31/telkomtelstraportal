package com.tt.pvsaassprintlevel1.mvc.model;

import java.util.ArrayList;


public class PVSaasOfficeBuildingSiteList  implements Comparable<PVSaasOfficeBuildingSiteList>{
	private String siteTier;
	private String siteCount;
	private int id;
	
	private ArrayList<PVSaasOfficeBuildingSiteColor> officeBuildingSiteColorList;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSiteTier() {
		return siteTier;
	}
	public void setSiteTier(String siteTier) {
		this.siteTier = siteTier;
	}
	public String getSiteCount() {
		return siteCount;
	}
	public void setSiteCount(String siteCount) {
		this.siteCount = siteCount;
	}


	public ArrayList<PVSaasOfficeBuildingSiteColor> getOfficeBuildingSiteColorList() {
		return officeBuildingSiteColorList;
	}
	public void setOfficeBuildingSiteColorList(
			ArrayList<PVSaasOfficeBuildingSiteColor> officeBuildingSiteColorList) {
		this.officeBuildingSiteColorList = officeBuildingSiteColorList;
		
	}
	@Override
	public String toString() {
		return "OfficeBuildingSiteList [siteTier=" + siteTier + ", siteCount="
				+ siteCount + ", id=" + id + ", officeBuildingSiteColorList="
				+ officeBuildingSiteColorList + "]";
	}
	@Override
	public  int compareTo(PVSaasOfficeBuildingSiteList slaList) {
		int siteTierId = ((PVSaasOfficeBuildingSiteList) slaList).getId();
		return (int) (siteTierId - this.id);
	}
}
