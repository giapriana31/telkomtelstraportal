package com.tt.pvsaassprintlevel1.mvc.web.controller;
import java.util.Properties;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.pvsaassprintlevel1.mvc.service.common.PVSaasSprintLevel1Manager;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("pvSaasSprintLevel1Controller")
@RequestMapping("VIEW")

public class PVSaasSprintLevel1Controller {

	private PVSaasSprintLevel1Manager pvSaasSprintLevel1Manager;
	public static Properties prop = PropertyReader.getProperties();

	@Autowired
	@Qualifier(value = "pvSaasSprintLevel1Manager")
	public void setPvSiteSprintLevel2Manager(
			PVSaasSprintLevel1Manager pvSaasSprintLevel1Manager) {
		this.pvSaasSprintLevel1Manager = pvSaasSprintLevel1Manager;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse Response, Model model) throws PortalException,
			SystemException {
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(Response));
		model.addAttribute("PVSaasProductLevel1",pvSaasSprintLevel1Manager.getSaasProduct(customerUniqueID));
		return "pvSaasSprintLevel1";
		
	}

}
