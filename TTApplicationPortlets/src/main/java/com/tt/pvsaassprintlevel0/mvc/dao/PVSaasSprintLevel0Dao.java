package com.tt.pvsaassprintlevel0.mvc.dao;

import com.tt.logging.Logger;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("pvSaasSprintLevel0Dao")
@Transactional
public class PVSaasSprintLevel0Dao
{
  private static final Logger log = Logger.getLogger(PVSaasSprintLevel0Dao.class);
  @Autowired
  private SessionFactory sessionFactory;
  
  public Map<String, Integer> getGraphData(String customerId)
  {
    log.info("pvSaasSprintLevel0Dao-getGraphData-start");
    
    Map<Integer, String> deliveryStageToColor = new HashMap();
    deliveryStageToColor.put(Integer.valueOf(7), "#848484");
    deliveryStageToColor.put(Integer.valueOf(6), "#89C35C");
    deliveryStageToColor.put(Integer.valueOf(5), "#FA8072");
    deliveryStageToColor.put(Integer.valueOf(4), "#00B0F0");
    deliveryStageToColor.put(Integer.valueOf(3), "#7030A0");
    deliveryStageToColor.put(Integer.valueOf(2), "#A40800");
    deliveryStageToColor.put(Integer.valueOf(1), "#000");
    
    Map<String, Integer> graphData = new HashMap();
    
    StringBuffer sb = new StringBuffer();
    sb.append(" select delivery_stage,count(1) from( ");
    sb.append(" select ciid,delivery_stage from( ");
    sb.append(" SELECT ciid,delivery_stage FROM configurationitem WHERE customeruniqueid = '" + customerId + "' AND cmdbclass='saas Business Service' ");
    sb.append(" order by ciid,delivery_stage desc ");
    sb.append(" ) a GROUP BY ciid ");
    sb.append(" )b group by delivery_stage ");
    
    Query query = this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString());
    
    log.info("pvSaasSprintLevel0Dao-getGraphData-Query: " + query.getQueryString());
    if ((null != query.list()) && (query.list().size() > 0))
    {
      List<Object[]> data = query.list();
      for (Object[] objects : data) {
        if (null != objects[0]) {
          graphData.put(deliveryStageToColor.get(Integer.valueOf(Integer.parseInt((String)objects[0]))), Integer.valueOf(((BigInteger)objects[1]).intValue()));
        }
      }
    }
    log.info("pvSaasSprintLevel0Dao-getGraphData-end");
    
    return graphData;
  }
}
