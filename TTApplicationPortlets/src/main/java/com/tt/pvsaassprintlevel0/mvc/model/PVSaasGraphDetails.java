package com.tt.pvsaassprintlevel0.mvc.model;

public class PVSaasGraphDetails
{
  private String siteId;
  private String ciId;
  private String deliveryStatus;
  private String delivery_Stage;
  private String color;
  private String serviceTier;
  
  public String getServiceTier()
  {
    return this.serviceTier;
  }
  
  public void setServiceTier(String serviceTier)
  {
    this.serviceTier = serviceTier;
  }
  
  public String getSiteId()
  {
    return this.siteId;
  }
  
  public void setSiteId(String siteId)
  {
    this.siteId = siteId;
  }
  
  public String getCiId()
  {
    return this.ciId;
  }
  
  public void setCiId(String ciId)
  {
    this.ciId = ciId;
  }
  
  public String getDeliveryStatus()
  {
    return this.deliveryStatus;
  }
  
  public void setDeliveryStatus(String deliveryStatus)
  {
    this.deliveryStatus = deliveryStatus;
  }
  
  public String getDelivery_Stage()
  {
    return this.delivery_Stage;
  }
  
  public void setDelivery_Stage(String delivery_Stage)
  {
    this.delivery_Stage = delivery_Stage;
  }
  
  public String getColor()
  {
    return this.color;
  }
  
  public void setColor(String color)
  {
    this.color = color;
  }
}
