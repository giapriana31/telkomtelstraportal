package com.tt.pvsaassprintlevel0.mvc.web.controller;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.pvsaassprintlevel0.mvc.service.PVSaasSprintLevel0Manager;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller("pvSaasSprintLevel0Controller")
@RequestMapping({"VIEW"})
public class PVSaasSprintLevel0Controller
{
  private static final Logger log = Logger.getLogger(PVSaasSprintLevel0Controller.class);
  public static Properties prop = PropertyReader.getProperties();
  private PVSaasSprintLevel0Manager pvSaasSprintLevel0Manager;
  
  @Autowired
  @Qualifier("pvSaasSprintLevel0Manager")
  public void setPvSaasSprintLevel0Manager(PVSaasSprintLevel0Manager pvSaasSprintLevel0Manager)
  {
    this.pvSaasSprintLevel0Manager = pvSaasSprintLevel0Manager;
  }
  
  @RenderMapping
  public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model)
    throws PortalException, SystemException
  {
    log.debug("pvSaasSprintLevel0Controller * handleRenderRequest * Inside the method");
    
    HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
    HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);
    
    String customerId = TTGenericUtils.getCustomerIDFromSession(req, res);
    
    SubscribedService ss = new SubscribedService();
    
    int serviceCount = SubscribedService.isSubscribedServices("SaaS", customerId);
    if (serviceCount == 0)
    {
      model.addAttribute("title", "SaaS");
      return "unsubscribed_pvsaas_service";
    }
    Map<String, Integer> graphDataMap = this.pvSaasSprintLevel0Manager.getGraphDetails(customerId);
    
    Set<String> graphDataKeys = graphDataMap.keySet();
    Gson gson = new Gson();
    List<String> colorList = new ArrayList();
    colorList.add("#848484");
    colorList.add("#89C35C");
    colorList.add("#FA8072");
    colorList.add("#00B0F0");
    colorList.add("#7030A0");
    colorList.add("#A40800");
    colorList.add("#000");
    for (String string : colorList) {
      if ((null != graphDataMap) && (!graphDataMap.keySet().contains(string))) {
        graphDataMap.put(string, Integer.valueOf(0));
      }
    }
    String saasGraphDataJson = gson.toJson(graphDataMap);
    
    String colorListJson = gson.toJson(colorList);
    ArrayList<String> graphBottom = new ArrayList();
    graphBottom.add("Order Received-#000");
    graphBottom.add("Detailed Design-#A40800");
    graphBottom.add("Procurement-#7030A0");
    graphBottom.add("Delivery & Activation-#00B0F0");
    graphBottom.add("Monitoring-#FA8072");
    graphBottom.add("Operational-#89C35C");
    graphBottom.add("Unknow-#848484");
    
    model.addAttribute("saasStatusGraphDataSaasSprint", saasGraphDataJson);
    
    model.addAttribute("graphColorListSaasSprint", colorListJson);
    model.addAttribute("graphBottomListSaasStatus", graphBottom);
    
    return "pvSaasSprintLevel0";
  }
}
