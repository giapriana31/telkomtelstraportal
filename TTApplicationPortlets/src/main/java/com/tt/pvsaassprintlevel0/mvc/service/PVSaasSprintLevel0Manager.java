package com.tt.pvsaassprintlevel0.mvc.service;

import com.tt.logging.Logger;
import com.tt.pvsaassprintlevel0.mvc.dao.PVSaasSprintLevel0Dao;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("pvSaasSprintLevel0Manager")
@Transactional
public class PVSaasSprintLevel0Manager
{
  private PVSaasSprintLevel0Dao pvSaasSprintLevel0Dao;
  private static final Logger log = Logger.getLogger(PVSaasSprintLevel0Manager.class);
  
  @Autowired
  @Qualifier("pvSaasSprintLevel0Dao")
  public void setPvSaasSprintLevel0Dao(PVSaasSprintLevel0Dao pvSaasSprintLevel0Dao)
  {
    this.pvSaasSprintLevel0Dao = pvSaasSprintLevel0Dao;
  }
  
  public Map<String, Integer> getGraphDetails(String customerId)
  {
    return this.pvSaasSprintLevel0Dao.getGraphData(customerId);
  }
}
