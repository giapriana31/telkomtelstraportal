package com.tt.logticket.mvc.service.common;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.tt.logging.Logger;
import com.tt.logticket.mvc.dao.LogIncidentDaoImpl;
import com.tt.model.IncidentDashboardMapping;


@Service(value="logIncidentManager")
@Transactional
public class LogIncidentManagerImpl {

	private LogIncidentDaoImpl logIncidentDaoImpl;
	private final static Logger log = Logger.getLogger(LogIncidentManagerImpl.class);
		
	@Autowired
	@Qualifier("logIncidentDaoImpl")
	public void setLogIncidentDaoImpl(LogIncidentDaoImpl logIncidentDaoImpl) {
		this.logIncidentDaoImpl = logIncidentDaoImpl;
	}

	public LinkedHashMap<String, String> getTicketPriorityCount(String customeruniqueId){
		return logIncidentDaoImpl.getCombinedPriorityCount(customeruniqueId);
	}
	
	public List<IncidentDashboardMapping> getTicketSummaryIncidentList(String customeruniqueId/*, String countShotTicket*/) throws Exception{
		try{
			return logIncidentDaoImpl.getTicketSummaryIncidentList(customeruniqueId/*, countShotTicket*/);
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public LinkedHashMap<String, String> getSiteNameMap(String customeruniqueID) throws Exception{
		try{
			return logIncidentDaoImpl.getSiteNameMap(customeruniqueID);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getServiceFromSiteName(String siteID, String customerId) throws Exception{
		try{
			return logIncidentDaoImpl.getServiceFromSiteName(siteID,customerId);
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public LinkedHashMap<String, String> getRegionNameMap(String customeruniqueID) throws Exception{
		try{
			return logIncidentDaoImpl.getRegionNameMap(customeruniqueID);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSiteFromRegion(String region, String customeruniqueID) throws Exception{
		try{
			return logIncidentDaoImpl.getSiteFromRegion(region,customeruniqueID);
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getImpactList() throws Exception{
		try{
			return logIncidentDaoImpl.getImpactList();
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getUrgencyList() throws Exception{
		try{
			return logIncidentDaoImpl.getUrgencyList();
		}
		catch(Exception e){
			
			throw e;
		}
	}
	
	public LinkedHashMap<String, String> getPrivateCloudBusinessServiceValues(String customerId)
	{
		List resultList = logIncidentDaoImpl.getPrivateCloudBusinessServiceValues(customerId);
		LinkedHashMap<String, String> resultMap = new LinkedHashMap<String, String>();
		
		for(Object object : resultList)
		{
			Object[] row = (Object[]) object;
			resultMap.put(row[0].toString(), row[1].toString());
		}
		log.debug("Map size :: " + resultMap.size());
		
		return resultMap;
	}
	
	public JSONArray getVMList(String pcbsId)
	{
		log.info("Class:LogIncidentManagerImpl, Method:getPrivateCloudBusinessServiceValues Start");
		
		List resultList = logIncidentDaoImpl.getVMList(pcbsId);
		JSONArray resultJson = convertResultListToJson(resultList);
		
		log.info("Class:LogIncidentManagerImpl, Method:getPrivateCloudBusinessServiceValues End");
		return resultJson;
	}
	
	private JSONArray convertResultListToJson(List resultList) 
	{
		log.info("Class:LogIncidentManagerImpl, Method: convertResultListToJson Start");
		JSONArray parentJson = JSONFactoryUtil.createJSONArray();
		
		if(resultList != null && !resultList.isEmpty())
		{
			for(Object object : resultList)
			{
				JSONObject childJson = JSONFactoryUtil.createJSONObject();
				Object[] row = (Object[]) object;
				
				childJson.put("ciid", row[0].toString());
				log.debug(" " + row[0].toString());
				childJson.put("ciname", row[1].toString());
				log.debug(" " + row[1].toString());
				
				parentJson.put(childJson);
			}
		}
		log.debug("Final Json :: " + parentJson.toString());
		log.info("Class:LogIncidentManagerImpl, Method:convertResultListToJson End");
		
		return parentJson;
	}

	public String getCIName(String privateCloudBusinessService) {
		
		return logIncidentDaoImpl.getCIName(privateCloudBusinessService);
	}

	public LinkedHashMap<String, String> getSaasWhispirBsValue(String customerId) {
		List resultList = logIncidentDaoImpl.getSaasWhispirBsValue(customerId);
		LinkedHashMap<String, String> resultMap = new LinkedHashMap<String, String>();
		
		for(Object object : resultList)
		{
			Object[] row = (Object[]) object;
			resultMap.put(row[0].toString(), row[1].toString());
		}
		log.debug("Map size :: " + resultMap.size());
		
		return resultMap;
	}
	
	public LinkedHashMap<String, String> getSaasIPScapeBsValue(String customerId) {
		List resultList = logIncidentDaoImpl.getSaasIPScapeBsValue(customerId);
		LinkedHashMap<String, String> resultMap = new LinkedHashMap<String, String>();
		
		for(Object object : resultList)
		{
			Object[] row = (Object[]) object;
			resultMap.put(row[0].toString(), row[1].toString());
		}
		log.debug("Map size :: " + resultMap.size());
		
		return resultMap;
	}
	
	public LinkedHashMap<String, String> getSaasMandoeBsValue(String customerId) {
		List resultList = logIncidentDaoImpl.getSaasMandoeBsValue(customerId);
		LinkedHashMap<String, String> resultMap = new LinkedHashMap<String, String>();
		
		for(Object object : resultList)
		{
			Object[] row = (Object[]) object;
			resultMap.put(row[0].toString(), row[1].toString());
		}
		log.debug("Map size :: " + resultMap.size());
		
		return resultMap;
	}

	public JSONArray getSaasWhispirResourcesList(String saasWhispirBsId) {
		log.info("Class:LogIncidentManagerImpl, Method:getSaasWhispirResourcesList Start");
		
		List resultList = logIncidentDaoImpl.getSaasWhispirResourcesList(saasWhispirBsId);
		JSONArray resultJson = convertResultListToJson(resultList);
		
		log.info("Class:LogIncidentManagerImpl, Method:getSaasWhispirResourcesList End");
		return resultJson;
	}
	
	public JSONArray getSaasIPScapeResourcesList(String saasIPScapeBsId) {
		log.info("Class:LogIncidentManagerImpl, Method:getSaasIpscapeResourcesList Start");
		
		List resultList = logIncidentDaoImpl.getSaasIPScapeResourcesList(saasIPScapeBsId);
		JSONArray resultJson = convertResultListToJson(resultList);
		
		log.info("Class:LogIncidentManagerImpl, Method:getSaasIpscapeResourcesList End");
		return resultJson;
	}
	
	public JSONArray getSaasMandoeResourcesList(String saasMandoeBsId) {
		log.info("Class:LogIncidentManagerImpl, Method:getSaasMandoeResourcesList Start");
		
		List resultList = logIncidentDaoImpl.getSaasMandoeResourcesList(saasMandoeBsId);
		JSONArray resultJson = convertResultListToJson(resultList);
		
		log.info("Class:LogIncidentManagerImpl, Method:getSaasMandoeResourcesList End");
		return resultJson;
	}
}
