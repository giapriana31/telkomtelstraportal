package com.tt.logticket.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.client.model.incidentapi.InsertResponse;
import com.tt.client.service.incident.IncidentWSCallService;
import com.tt.logging.Logger;
import com.tt.logticket.mvc.service.common.LogIncidentManagerImpl;
import com.tt.model.Incident;
import com.tt.model.IncidentDashboardMapping;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.utils.TTGenericUtils;

@Controller("logIncidentController")
@RequestMapping("VIEW")

public class TTLogIncidentController {
	
	Properties properties = PropertyReader.getProperties();
	
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(TTLogIncidentController.class);
	
	private LogIncidentManagerImpl logIncidentManager;
	
	@Autowired
	@Qualifier("logIncidentManager")
	public void setLogIncidentManager(LogIncidentManagerImpl logIncidentManager) {
		this.logIncidentManager = logIncidentManager;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws Exception{
		try{
			log.info("Rendering Dashboard Incident Portlet");
			HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
			
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
			
			if(customerUniqueID != null){
				log.info("Cust Id " + customerUniqueID);
				log.info("Fetched Customer Unique Id : "+customerUniqueID);
				LinkedHashMap<String, String> regionList = logIncidentManager.getRegionNameMap(customerUniqueID);
				LinkedHashMap<String, String> regionListActual = new LinkedHashMap<String, String>();
				regionListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : regionList.entrySet()){
					regionListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("regionList", regionListActual);
				
				LinkedHashMap<String, String> pcbsList = logIncidentManager.getPrivateCloudBusinessServiceValues(customerUniqueID);
				LinkedHashMap<String, String> pcbsListActual = new LinkedHashMap<String, String>();
				pcbsListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : pcbsList.entrySet()){
					pcbsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("pcbsList", pcbsListActual);
				
				
				LinkedHashMap<String, String> siteNameList = new LinkedHashMap<String, String>();
				siteNameList.put("0", "--Select--");
				model.addAttribute("siteNameList", siteNameList);
				
				//Calling method to fetch Impact/Urgency List and set it in model				
				fetchUrgencyandImpactList(model);
				
				log.info("Model setting done for impact, urgency, site, region");
				
				//For getting count of P1P2 & P3P4 - Start
				LinkedHashMap<String, String> countPriorityMap = logIncidentManager.getTicketPriorityCount(customerUniqueID);
				log.info("map "+countPriorityMap+" map");
				for( Map.Entry<String,String> entry : countPriorityMap.entrySet()){
					model.addAttribute(entry.getKey(), entry.getValue());
				}
				//For getting count of P1P2 & P3P4 - End
				model.addAttribute("logIncident", new Incident());
				
				//for getting SaaS Whispir Business Service - Start
				LinkedHashMap<String, String> saasWhispirBsList = logIncidentManager.getSaasWhispirBsValue(customerUniqueID);
				LinkedHashMap<String, String> saasWhispirBsListActual = new LinkedHashMap<String, String>();
				saasWhispirBsListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : saasWhispirBsList.entrySet()){
					saasWhispirBsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("saasWhispirBsList", saasWhispirBsListActual);
				//for getting SaaS Whispir Business Service - End
				
				//for getting SaaS IPScape Business Service - Start
				LinkedHashMap<String, String> saasIPScapeBsList = logIncidentManager.getSaasIPScapeBsValue(customerUniqueID);
				LinkedHashMap<String, String> saasIPScapeBsListActual = new LinkedHashMap<String, String>();
				saasIPScapeBsListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : saasIPScapeBsList.entrySet()){
					saasIPScapeBsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("saasIPScapeBsList", saasIPScapeBsListActual);
				//for getting SaaS IPScape Business Service - End
				
				//for getting SaaS Mandoe Business Service - Start
				LinkedHashMap<String, String> saasMandoeBsList = logIncidentManager.getSaasMandoeBsValue(customerUniqueID);
				LinkedHashMap<String, String> saasMandoeBsListActual = new LinkedHashMap<String, String>();
				saasMandoeBsListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : saasMandoeBsList.entrySet()){
					saasMandoeBsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("saasMandoeBsList", saasMandoeBsListActual);
				//for getting SaaS Mandoe Business Service - End

							
				LinkedHashMap<String, String> serviceIDList = new LinkedHashMap<String, String>();
				serviceIDList.put("0", "--Select--");
				model.addAttribute("LogIncidentDisclaimerMsg", prop.getProperty("LogIncidentDisclaimerMsg"));
				model.addAttribute("serviceIDList", serviceIDList);
				
				model.addAttribute("UrgencyDescription", prop.getProperty("UrgencyDescription"));
				model.addAttribute("ImpactDescription", prop.getProperty("ImpactDescription"));
				log.info("Model setting for disclaimer and tooltip message done.");
				
				model.addAttribute("incidentRowDetailsList", getDashTicketDetails(customerUniqueID)); //Row details
			}
			else{
				log.info("Customer Unique Id not fetched and hence setting blank data in Incident portlet");
				Incident incident = new Incident();
				model.addAttribute("logIncident", incident);
				
				fetchUrgencyandImpactList(model);
			}
			return "logTicket";
		}
		catch (Exception e){
			log.error("Error in Log Incident Render- " + e.getMessage());
			throw e;
		}
	}
	
	@ResourceMapping(value="submitRequestURLLI")
	public void handleLogIncidentRequest(@ModelAttribute("logIncident") Incident incident, Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		JSONObject json = new JSONObject();
		try{
			log.info("In controller for handling Submit Incident Request");
			
			
			HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
			
			User user = UserLocalServiceUtil.getUser((Long)httpServletRequest.getSession().getAttribute(WebKeys.USER_ID));
			
			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
			
			Map<String, String[]> parameters = uploadRequest.getParameterMap();
			String[] saasBS = parameters.get("privateCloudBusinessService");
			String[] saasComponet = parameters.get("virtualMachines");
			String saasWhispirBS = saasBS[1];
			String saasWhispirComp = saasComponet[1];
			String saasIPScapeBS = saasBS[2];
			String saasIPScapeComp = saasComponet[2];
			String saasMandoeBS = saasBS[3];
			String saasMandoeComp = saasComponet[3];
			
			String productType = uploadRequest.getParameter("productType");
			String summary = uploadRequest.getParameter("summary");
			String notes = uploadRequest.getParameter("worklognotes");
			String siteId = uploadRequest.getParameter("siteid");
			String impact =  uploadRequest.getParameter("impact");
			String urgency = uploadRequest.getParameter("urgency");
			String serviceId = uploadRequest.getParameter("serviceid");
			
			String privateCloudBusinessService = uploadRequest.getParameter("privateCloudBusinessService");
			String virtualMachines = "";
			if ("Whispir".equalsIgnoreCase(productType)){
				virtualMachines = saasWhispirComp;
			}else if ("IPScape".equalsIgnoreCase(productType)){
				virtualMachines = saasIPScapeComp;
			}else if ("Mandoe".equalsIgnoreCase(productType)){
				virtualMachines = saasMandoeComp;
			}else{
				virtualMachines = uploadRequest.getParameter("virtualMachines");
			}
			
			boolean mimCheck = uploadRequest.getParameter("mimCheck") != null;
			
			log.info("productType :: "+productType);
			log.info("privateCloudBusinessService :: "+privateCloudBusinessService);
			log.info("virtualMachines :: "+virtualMachines);
			
			log.info("Data fetched from JSP");
			log.debug("summary :: " + summary);
			log.debug("notes :: " + notes);
			log.debug("site id :: " + siteId);
			log.debug("impact :: " + impact);
			log.debug("urgency :: " + urgency);
			log.debug("service id :: " + serviceId);
			log.debug("MIM :: " + mimCheck);
			
			incident.setSummary(summary);
			incident.setWorklognotes(notes);
			incident.setSiteid(siteId);
			incident.setImpact(impact);
			incident.setUrgency(urgency);
			incident.setMimCheck(mimCheck);
			incident.setVirtualMachines(logIncidentManager.getCIName(virtualMachines));
			incident.setRootProductId(productType);
			
			if("MNS".equalsIgnoreCase(productType)){
				incident.setServiceid(serviceId);
			}else if ("PrivateCloud".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(privateCloudBusinessService));
			}else if ("Whispir".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(saasWhispirBS));
			}else if ("IPScape".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(saasIPScapeBS));
			}else if ("Mandoe".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(saasMandoeBS));
			}
			
			if(incident.getMimCheck()){
				String originalNotes = incident.getWorklognotes();
				String appendMIM = prop.getProperty("MIMText");
				log.info("MIM Message to be appended - " + appendMIM);
				incident.setWorklognotes(appendMIM+" "+originalNotes);
			}
			
			RefreshUtil.setProxyObject();
			
			incident.setTickettype(prop.getProperty("ticketTypeProperty"));
			incident.setTicketsource(prop.getProperty("ticketSourceProperty"));
			incident.setUserid(user.getEmailAddress()+"");
			
			incident.setResolvergroupname(prop.getProperty("resolverGroup"));
			
			String language = ((String) user.getExpandoBridge().getAttribute("preferredLanguage")).equalsIgnoreCase("en_US") ? "English" : "Bahasa";
			incident.setCustomerpreferedlanguage(language);
//			incident.setCustomerpreferedlanguage((String)user.getExpandoBridge().getAttribute("preferredLanguage"));
			
			log.debug("Making call to SNOW for inserting incident through controller");
			InsertResponse insertResponse = new InsertResponse();
			boolean isException = false;
			IncidentWSCallService callService = new IncidentWSCallService();
			try{
				insertResponse= callService.insertIncidentWebService(incident);
			}
			catch(Exception c){
				isException=true;
			}
			if(!isException){
				log.debug("Display Response: " + insertResponse.getDisplayValue());
				log.debug("Error Response: " + insertResponse.getErrorMessage());
				
				if(insertResponse.getDisplayValue() !=null && insertResponse.getDisplayValue().trim().length()!=0){
					json.put("incidentID", "Success@@@"+insertResponse.getDisplayValue());
				}
				else if(insertResponse.getErrorMessage()!=null){
					json.put("incidentID", "Error@@@"+insertResponse.getErrorMessage());
				}
				
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}else{
				log.error("Connection timed out");
				json.put("incidentID", "Error@"+"Connection Timed Out. Please try again later.");
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}
		}
		catch(Exception e){
			log.error("Error while Processing Submit- " + e.getMessage());
			throw e;
		}
	}
	
	@ResourceMapping(value="getServiceURLLI")
	public void handleServiceIDFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
        
		try{
			
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req, httpServletResponse);
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedSiteIDJson"));
			String selectedSiteID = jsonObj.getString("selectedSiteID");
			if(selectedSiteID!=null && selectedSiteID.trim().length()!=0){
				log.info("Selected SiteName in controller : " + selectedSiteID);
				LinkedHashMap<String, String> serviceIDListtemp = logIncidentManager.getServiceFromSiteName(selectedSiteID, customerUniqueID);
				
				String responseStr = "";
				if(serviceIDListtemp!=null){
					for( Map.Entry<String,String> entry : serviceIDListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}else{
				log.debug("Selected Site ID is null");
			}
		}
		catch(Exception e){
			
			log.error("Error while Getting Service Names from Site- " + e.getMessage());
			throw e;
		}
	}
	
	
	public List<Incident> getDashTicketDetails(String customerUniqueID) throws Exception{
		try{	
			log.info("In method to fetch the data of Incidents on Incident Dashboard");
				Date date = new Date();
				java.sql.Timestamp timeStampDate;
				List<IncidentDashboardMapping> incList = logIncidentManager.getTicketSummaryIncidentList(customerUniqueID);
				log.info("At controller called function return data from service call - "+incList);
				List<Incident> incidentList = new ArrayList<Incident>();
				if(incList!=null && incList.size()!=0){
					int loopIteratorCount;
					if(incList.size() > Integer.parseInt(prop.getProperty("countShowTicketsDashboard"))){
						loopIteratorCount = Integer.parseInt(prop.getProperty("countShowTicketsDashboard"));
					}
					else{
						loopIteratorCount = incList.size();
					}
		
					for(int i = 0; i< loopIteratorCount; i++){
						timeStampDate = new Timestamp(date.getTime());
						Incident incident = new Incident();
						if(incList.get(i)!=null && incList.get(i).getServiceleveltarget() != null){
							Long minutesDiff =  ((incList.get(i).getServiceleveltarget().getTime())/60000)-((timeStampDate.getTime())/60000);
							Long hour = minutesDiff/60;
							Long hourTemp = minutesDiff/60;
							Long min = minutesDiff%60;
							String hourStr = hour.toString().replace("-", "");
							String minStr = min.toString().replace("-", "");
							if(hourStr.trim().length()==1){
								if(hourTemp.toString().contains("-")){
									hourStr="-0"+hourStr;
								}else{
									hourStr=" 0"+hourStr;
								}
							}
							else{
								hourStr=" "+hourTemp.toString();
							}
							if(minStr.trim().length()==1){
								minStr="0"+minStr;
							}
							incident.setDueByTime((incList.get(i).getServiceleveltarget().toString().split("\\.")[0]));
						}
						else{
							incident.setDueByTime("");
						}
						//------------------------------------------------------setting the category
						log.info("Incident Category:::"+incList.get(i).getIncdcategory());
						incident.setCategory(incList.get(i).getIncdcategory());
						incident.setSummary(incList.get(i).getSummary());
						incident.setPriority(Long.parseLong(incList.get(i).getPriority()));
						
						if(incList.get(i).getHierEscalationFlag() || (!incList.get(i).getHierEscalationFlag() && incList.get(i).getMIM().toUpperCase().equals(prop.getProperty("checkMIMFlag")))){
							incident.setShowEscalationFlag(true);
						
						}
						else{
							incident.setShowEscalationFlag(false);
						
						}
						if(incList.get(i).getCustomerimpacted().equals("TRUE")){
							incident.setCustomerimpacted(true);
						}
						else{
							incident.setCustomerimpacted(false);
						}
						if(incList.get(i).getIncidentstatus()!=null && incList.get(i).getIncidentstatus().length()!=0 ){
							incident.setIncidentstatus(incList.get(i).getIncidentstatus());
						}
						else{
							incident.setIncidentstatus("");
						}
						if(incList.get(i).getIncidentid()!=null && incList.get(i).getIncidentid().length()!=0){
							incident.setIncidentid(incList.get(i).getIncidentid());
						}else{
							incident.setIncidentid("");
						}
						
						incidentList.add(incident);
					}
				}else{
					//No incident for this user
					log.debug("No incident data is available for the user " + customerUniqueID);
				}
				log.info("Data for Priority, Summary, SLT, Status, Escalation Flag fetched");
			return incidentList;
		}
		catch(Exception e){
			log.error("Error in separate method to fetch Dashboard LogTicket data - " + e.getMessage());
			throw e;
		}
	}
	
		
	@ResourceMapping(value="getSiteURL")
	public void handleSiteIDFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		try{
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedRegionJson"));
			String selectedRegion = jsonObj.getString("selectedRegion");
			if(selectedRegion!=null && selectedRegion.trim().length()!=0){
								
				String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req, httpServletResponse);
				log.info("Selected Region in controller : " + selectedRegion);
				LinkedHashMap<String, String> siteIDListtemp = logIncidentManager.getSiteFromRegion(selectedRegion,customerUniqueID);
				String responseStr = "";
				if(siteIDListtemp!=null){
					for( Map.Entry<String,String> entry : siteIDListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
			else{
				log.debug("Selected Region from jsp is null");
			}
		}
		catch(Exception e){
			log.error("Error in getSiteURL mapped controller method handleSiteIDFetch - " + e.getMessage());
			
		}
	}
	
	private void fetchUrgencyandImpactList(Model model){
		try{
			
			LinkedHashMap<String, String> urgencyListMap = new LinkedHashMap<String, String>();
			urgencyListMap.put("0", "--Select--");
			LinkedHashMap<String, String> urgencyListMapTemp = logIncidentManager.getUrgencyList();
			
			for( Map.Entry<String,String> entry : urgencyListMapTemp.entrySet()){
				urgencyListMap.put(entry.getKey()+"",entry.getValue()+"");
			}
			
		
			LinkedHashMap<String, String> impactListMap = new LinkedHashMap<String, String>();
			impactListMap.put("0", "--Select--");
			LinkedHashMap<String, String> impactListMapTemp = logIncidentManager.getImpactList();
			
			for( Map.Entry<String,String> entry : impactListMapTemp.entrySet()){
				impactListMap.put(entry.getKey()+"",entry.getValue()+"");
			}
			
			
			model.addAttribute("urgencyListMap", urgencyListMap);
			model.addAttribute("impactListMap", impactListMap);
		}
		catch(Exception e){
			log.error("Error while fetching Impact & urgency in controller - " + e.toString());
		}
	}
	
	@ResourceMapping(value="getVMURL")
	public void getValuesFromProductType(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{
		String pcbsId = resourceRequest.getParameter("pcbs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + pcbsId);
		
		responseJson = logIncidentManager.getVMList(pcbsId);
		resourceResponse.getWriter().print(responseJson);
	}
	
	@ResourceMapping(value="getSaasWhispirResources")
	public void getSaasWhispirResources(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{				
		String saasWhispirBsId = resourceRequest.getParameter("saasWhispirBs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + saasWhispirBsId);
		
		responseJson = logIncidentManager.getSaasWhispirResourcesList(saasWhispirBsId);
		resourceResponse.getWriter().print(responseJson);
	}
	
	@ResourceMapping(value="getSaasIPScapeResources")
	public void getSaasIPScapeResources(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{				
		String saasIPScapeBsId = resourceRequest.getParameter("saasIPScapeBs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + saasIPScapeBsId);
		
		responseJson = logIncidentManager.getSaasIPScapeResourcesList(saasIPScapeBsId);
		resourceResponse.getWriter().print(responseJson);
	}
	
	@ResourceMapping(value="getSaasMandoeResources")
	public void getSaasMandoeResources(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{				
		String saasMandoeBsId = resourceRequest.getParameter("saasMandoeBs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + saasMandoeBsId);
		
		responseJson = logIncidentManager.getSaasMandoeResourcesList(saasMandoeBsId);
		resourceResponse.getWriter().print(responseJson);
	}
}
