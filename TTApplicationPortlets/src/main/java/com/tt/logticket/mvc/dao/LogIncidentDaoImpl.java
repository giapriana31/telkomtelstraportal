package com.tt.logticket.mvc.dao;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.utils.PropertyReader;

@Repository(value="logIncidentDaoImpl")
@Transactional
public class LogIncidentDaoImpl {
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(LogIncidentDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	private Session session=null;
	private Transaction tx=null;

	public LinkedHashMap<String, String> getCombinedPriorityCount(String customeruniqueID){
		try{
			log.info("Fetching Count of incidents to be displayed on Incident Portlet Dashboard");
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			Query query1 = session.createSQLQuery("select count(distinct(i.incidentid)) from incddashboardmetrics i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId and i.citype='Service' and i.priority in ('1','2') and i.incidentstatus not in (:closed,:cancelled,:completed)").setString("custId", customeruniqueID).setString("cimpacted", "1").setString("closed", prop.getProperty("incStatusClose")).setString("cancelled", prop.getProperty("incStatusCancel")).setString("completed", prop.getProperty("statusCompleted"));
			Query query2 = session.createSQLQuery("select count(distinct(i.incidentid)) from incddashboardmetrics i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId and i.citype='Service' and i.priority in ('3') and i.incidentstatus not in (:closed,:cancelled,:completed)").setString("custId", customeruniqueID).setString("cimpacted", "1").setString("closed", prop.getProperty("incStatusClose")).setString("cancelled", prop.getProperty("incStatusCancel")).setString("completed", prop.getProperty("statusCompleted"));
			
			LinkedHashMap<String, String> countPriorityMap = new LinkedHashMap<String, String>();
			
			if(null != query1.uniqueResult()){
				log.info("Count 1 : " + query1.uniqueResult().toString() + "\nCount 2 : " + query2.uniqueResult().toString());
				countPriorityMap.put("countP1P2", query1.uniqueResult().toString()+"");
				countPriorityMap.put("countP3P4", query2.uniqueResult().toString()+"");
			}
			else{
				log.info("Setting count as zero");
				countPriorityMap.put("countP1P2", "0");
				countPriorityMap.put("countP3P4", "0");
			}
			tx.commit();
			return countPriorityMap;
		}
		catch(Exception e){
			log.error("Exception in Service class getCombinedPriorityCount method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				session.close();
			}
		}
	}
	
	public List<IncidentDashboardMapping> getTicketSummaryIncidentList(String customeruniqueID/*, String countShotTicket*/) throws Exception{
		try{
			
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date date = new Date();
			java.sql.Timestamp timeStampDate;
			List<IncidentDashboardMapping> incList = new ArrayList<IncidentDashboardMapping>();
			
			Query query1 = session.createQuery("select i.priority, i.summary, i.serviceleveltarget, i.hierEscalationFlag, i.MIM, i.incidentstatus, i.category, i.incidentid from Incident i where i.customeruniqueid=:custId and i.customerimpacted=:cimpacted and i.incidentstatus not in (:closed,:cancelled,:completed) order by priority asc").setBoolean("cimpacted", true).setString("custId", customeruniqueID).setString("closed", prop.getProperty("incStatusClose")).setString("cancelled", prop.getProperty("incStatusCancel")).setString("completed", prop.getProperty("statusCompleted")).setMaxResults(5);
			Query query2 = session.createQuery("select i.priority, i.summary, i.serviceleveltarget, i.hierEscalationFlag, i.MIM, i.incidentstatus, i.category, i.incidentid from Incident i where i.customeruniqueid=:custId and i.customerimpacted=:cimpacted and i.incidentstatus not in (:closed,:cancelled,:completed) order by priority asc").setBoolean("cimpacted", false).setString("custId", customeruniqueID).setString("closed", prop.getProperty("incStatusClose")).setString("cancelled", prop.getProperty("incStatusCancel")).setString("completed", prop.getProperty("statusCompleted")).setMaxResults(5);
						
			List results1 = query1.list();
			List results2 = query2.list();
			if(results1!=null){
				log.info("Fetched data for Customer Impacted = True");
				for(Iterator it=query1.iterate();it.hasNext();){
					
					Object[] row = (Object[]) it.next();
					IncidentDashboardMapping incident = new IncidentDashboardMapping();
					 incident.setPriority(row[0]+"");
					 incident.setSummary(row[1]+"");
					 if(row[2]==null){
						 //nothing to be done
					 }
					 else{
						 date = formatter.parse(row[2]+"");
					     timeStampDate = new Timestamp(date.getTime());
						 incident.setServiceleveltarget(timeStampDate);
					 }
					 
					 incident.setCustomerimpacted("TRUE");
					 if(row[3].toString().toUpperCase().equals("TRUE"))
					 {
						 incident.setHierEscalationFlag(true);
					 }
					 else{
						 incident.setHierEscalationFlag(false);
					 }
					 incident.setMIM(row[4]+"");
					 incident.setIncidentstatus(row[5]+"");
					 incident.setIncdcategory(row[6]+"");
					 incident.setIncidentid(row[7]+"");
					 incList.add(incident);
				}
			}
			else{
				log.info("No incidents for this user - " +customeruniqueID);
				//No incident for this user
			}
			
			if(results2 !=null){
				log.info("Fetched data for Customer Impacted = False");
				for(Iterator it=query2.iterate();it.hasNext();){
					 Object[] row = (Object[]) it.next();
					 IncidentDashboardMapping incident = new IncidentDashboardMapping();
					 incident.setPriority(row[0]+"");
					 incident.setSummary(row[1]+"");
					
					 if(row[2]==null){
						//nothing to be done
					 }
					 else{
						 date = formatter.parse(row[2]+"");
					     timeStampDate = new Timestamp(date.getTime());
						 incident.setServiceleveltarget(timeStampDate);
					 }
					 incident.setCustomerimpacted("FALSE");
					 if(row[3].toString().toUpperCase().equals("TRUE"))
					 {
						 incident.setHierEscalationFlag(true);
					 }
					 else{
						 incident.setHierEscalationFlag(false);
					 }
					 incident.setMIM(row[4]+"");
					 incident.setIncidentstatus(row[5]+"");
					 incident.setIncdcategory(row[6]+"");
					 incident.setIncidentid(row[7]+"");
					 incList.add(incident);
				
				}
			}
			else{
				log.info("No incidents for this user - " +customeruniqueID);
				//No incident for this user
			}

			tx.commit();
			return incList;
		}
		catch(Exception e){
			log.error("Exception in Service class getTicketSummaryIncidentList method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getSiteNameMap(String customeruniqueID) throws Exception{
		try{
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select s.customersite, s.siteid from Site s where s.customeruniqueid=:custId and status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query1 = session.createQuery(query.toString()).setString("custId", customeruniqueID);
			
			for(Iterator it=query1.iterate();it.hasNext();){
				 Object[] row = (Object[]) it.next();
				 siteNameIDMap.put(row[1]+"", row[0]+"");
				 
			}
			
			tx.commit();
			return siteNameIDMap;
		}
		catch(Exception e){
			log.error("Exception in Service class getSiteNameMap method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				session.close();
			}
		}
		
		
	}
	public LinkedHashMap<String,String> getServiceFromSiteName(String siteName, String customerId) throws Exception{
		try{
			log.info("In Service method to fetch Service related to site ID " + siteName);
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> serviceIdMap = new LinkedHashMap<String, String>();

			StringBuilder query = new StringBuilder();
			query.append("select c.ciid, c.ciname from Configuration c where c.siteid in (select siteid from Site where customersite=:siteNameFetched and status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE)
				 .append(") and c.citype=:ciType and status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query1 = session.createQuery(query.toString()).setString("siteNameFetched",siteName).setString("ciType", prop.getProperty("ciType"));
			
			for(Iterator it=query1.iterate();it.hasNext();){
				 Object[] row = (Object[]) it.next();
				 serviceIdMap.put(row[0]+"", row[1]+"");
			}
			tx.commit();
			return serviceIdMap;
		}
		catch(Exception e){
			log.error("Exception in Service Class getServiceFromSiteName method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getRegionNameMap(String customeruniqueID) throws Exception{
		try{
			log.info("Fetching Region Data");
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> regionNameMap = new LinkedHashMap<String, String>();
			StringBuilder query = new StringBuilder();
			query.append("select distinct s.stateprovince from Site s where s.customeruniqueid=:custId and status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE)
				 .append(" order by s.stateprovince asc");
			Query query1 = session.createQuery(query.toString()).setString("custId", customeruniqueID);
			log.info("Query Fired for fetching Region for logged in user : " + customeruniqueID);
			if(query1.list()!=null && !query1.list().isEmpty()){
				log.info("Region data found for user.");
				for(Iterator it=query1.iterate();it.hasNext();){
					String row = (String) it.next();
					regionNameMap.put(row, row);
				}
			}else{
				log.debug("No region data found for user with ID : " + customeruniqueID);
			}
			log.info("CLosing the transaction.");
			tx.commit();
			return regionNameMap;
		}
		catch(Exception e){
			log.error("Exception in Service Class getRegionNameMap method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getSiteFromRegion(String region, String customeruniqueID) throws Exception{
		try{
			log.info("In Service Method for fetching Site for selected region " + region);
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
			
			StringBuilder query = new StringBuilder();
			query.append("select s.customersite, s.siteid from Site s where s.stateprovince=:region and s.customeruniqueid=:custId and lower(s.servicetier) <> lower('Fully Managed') and status in ('")
				 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
				 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
			Query query1 = session.createQuery(query.toString()).setString("region",region).setString("custId",customeruniqueID);

			if(query1.list()!=null)
				log.info("Result size-"+query1.list().size());
			for(Iterator it=query1.iterate();it.hasNext();){
				 Object[] row = (Object[]) it.next();
				 siteNameIDMap.put(row[1]+"", row[0]+"");
			}
			log.info("CLosing the transaction.");
			tx.commit();
			return siteNameIDMap;
		}
		catch(Exception e){
			log.error("Exception in Service Class getSiteFromRegion method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getImpactList() throws Exception{
		try{
			log.info("In Service Method for fetching Impact List ");
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> impactListMap = new LinkedHashMap<String, String>();
			Query query1 = session.createQuery("select c.value from CommonLov c where c.name=:impact order by c.value asc").setString("impact",prop.getProperty("lovNameImpact"));
			log.info("Query executed for fetching ImpactList at Render Time");
			for(Iterator it=query1.iterate();it.hasNext();){
				 String row = (String) it.next();
				 log.info("Impact " + row);
				 impactListMap.put(row, row);
			}
			
			log.info("CLosing the transaction.");
			tx.commit();
			return impactListMap;
		}
		catch(Exception e){
			log.error("Exception in Service class getImpactList method - " + e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public LinkedHashMap<String,String> getUrgencyList() throws Exception{
		try{
			log.info("In Service Method for fetching Urgency List");
			session=sessionFactory.openSession();
			tx=session.beginTransaction();
			LinkedHashMap<String, String> urgencyListMap = new LinkedHashMap<String, String>();
			Query query1 = session.createQuery("select c.value from CommonLov c where c.name=:urgency order by c.value asc").setString("urgency",prop.getProperty("lovNameUrgency"));
			log.info("Query executed for fetching UrgencyList at Render Time");
			for(Iterator it=query1.iterate();it.hasNext();){
				 String row = (String) it.next();
				 log.info("Urgency " + row);
				 urgencyListMap.put(row, row);
			}
			log.info("CLosing the transaction.");
			tx.commit();
			return urgencyListMap;
		}
		catch(Exception e){
			log.error("Exception in Service Class  getUrgencyList method - " +e.getMessage());
			throw e;
		}
		finally{
			if(session!=null && session.isOpen()){
				log.info("Closing the session for this transaction");
				session.close();
			}
		}
	}
	
	public List getPrivateCloudBusinessServiceValues(String customerId)
	{
		log.info("Class: LogIncidentDaoImpl, Method: getPrivateCloudBusinessServiceValues Start");
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("select ci.ciid, ci.ciname from configurationitem ci where ci.citype='Service' ");
		queryString.append("and ci.cmdbclass='Private Cloud' and customeruniqueid=:customerid");
		log.debug("Query :: " + queryString.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryString.toString());
		query.setString("customerid", customerId);
		
		List resultList = query.list();
		log.debug("size of list :: " + resultList.size());
	
		log.info("Class: LogIncidentDaoImpl, Method: getPrivateCloudBusinessServiceValues End");
		return resultList;
	}
	
	public List getVMList(String pcbsciid)
	{
		log.info("Class: LogIncidentDaoImpl, Method: getVMList Start");
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select dependentciid, dependentciname from (");
		queryBuilder.append(" SELECT distinct(cire.dependentciid), cire.dependentciname FROM cirelationship cire WHERE cire.dependentciclass = 'cmdb_ci_vmware_instance' and cire.baseciid IN (");
		queryBuilder.append(" SELECT cir.dependentciid FROM cirelationship cir WHERE cir.baseciid =:pcbsciid)");
		queryBuilder.append(" )vm, configurationitem c where vm.dependentciid = c.ciid and c.status = 'Operational' order by dependentciname asc");
		
		log.debug("Query for VMList :: " + queryBuilder.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
		query.setString("pcbsciid", pcbsciid);
		
		List resultList = query.list();
		log.debug("Result List :: " + resultList + ", Size :: " + resultList != null ? "" + resultList.size() : null);

		log.info("Class: LogIncidentDaoImpl, Method: getVMList End");
		return resultList;
	}

	public String getCIName(String privateCloudBusinessService) {
		
		
		String sql = "select ciname from configurationitem where ciid = '"+privateCloudBusinessService+"'";
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql);
		
		if(query.list()!=null && query.list().size()>0)
			return (String)query.list().get(0);
		return "";
	}
	public List getSaasWhispirBsValue(String customerId) {
		log.info("Class: LogIncidentDaoImpl, Method: getSaasWhispirBsValue Start");
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("select ci.ciid, ci.ciname from configurationitem ci where ci.citype='Service' ");
		queryString.append("and ci.productId = 'Whispir' and ci.status = 'Operational' and customeruniqueid=:customerid");
		log.debug("Query :: " + queryString.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryString.toString());
		query.setString("customerid", customerId);
		
		List resultList = query.list();
		log.debug("size of list :: " + resultList.size());
	
		log.info("Class: LogIncidentDaoImpl, Method: getSaasWhispirBsValue End");
		return resultList;
	}
	
	public List getSaasIPScapeBsValue(String customerId) {
		log.info("Class: LogIncidentDaoImpl, Method: getSaasIPScapeBsValue Start");
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("select ci.ciid, ci.ciname from configurationitem ci where ci.citype='Service' ");
		queryString.append("and ci.productId = 'IPScape' and ci.status = 'Operational' and customeruniqueid=:customerid");
		log.debug("Query :: " + queryString.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryString.toString());
		query.setString("customerid", customerId);
		
		List resultList = query.list();
		log.debug("size of list :: " + resultList.size());
	
		log.info("Class: LogIncidentDaoImpl, Method: getSaasIPScapeBsValue End");
		return resultList;
	}
	
	public List getSaasMandoeBsValue(String customerId) {
		log.info("Class: LogIncidentDaoImpl, Method: getSaasMandoeBsValue Start");
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("select ci.ciid, ci.ciname from configurationitem ci where ci.citype='Service' ");
		queryString.append("and ci.productId like '%Mandoe%' and ci.status = 'Operational' and customeruniqueid=:customerid");
		log.debug("Query :: " + queryString.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryString.toString());
		query.setString("customerid", customerId);
		
		List resultList = query.list();
		log.debug("size of list :: " + resultList.size());
	
		log.info("Class: LogIncidentDaoImpl, Method: getSaasMandoeBsValue End");
		return resultList;
	}

	public List getSaasWhispirResourcesList(String saasWhispirBsId) {
		log.info("Class: LogIncidentDaoImpl, Method: getSaasWhispirResourcesList Start");
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select	cia.ciid, cia.ciname from configurationitem cia, configurationitem cib ");
		queryBuilder.append("where 	cia.siteid = cib.siteid and cia.status = 'Operational' and cia.citype='Resource' ");
		queryBuilder.append("and cia.productid = 'Whispir' and cib.ciid=:SaasBsCiid");
		
		log.debug("Query for getSaasResourcesList :: " + queryBuilder.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
		query.setString("SaasBsCiid", saasWhispirBsId);
		
		List resultList = query.list();
		log.debug("Result List :: " + resultList + ", Size :: " + resultList != null ? "" + resultList.size() : null);

		log.info("Class: LogIncidentDaoImpl, Method: getSaasWhispirResourcesList End");
		return resultList;
	}
	
	public List getSaasIPScapeResourcesList(String saasIPScapeBsId) {
		log.info("Class: LogIncidentDaoImpl, Method: getSaasIpscapeResourcesList Start");
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select	cia.ciid, cia.ciname from configurationitem cia, configurationitem cib ");
		queryBuilder.append("where 	cia.siteid = cib.siteid and cia.status = 'Operational' and cia.citype='Resource' ");
		queryBuilder.append("and cia.productid = 'IPScape' and cib.ciid=:SaasBsCiid");
		
		log.debug("Query for getSaasResourcesList :: " + queryBuilder.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
		query.setString("SaasBsCiid", saasIPScapeBsId);
		
		List resultList = query.list();
		log.debug("Result List :: " + resultList + ", Size :: " + resultList != null ? "" + resultList.size() : null);

		log.info("Class: LogIncidentDaoImpl, Method: getSaasIpscapeResourcesList End");
		return resultList;
	}
	
	public List getSaasMandoeResourcesList(String saasMandoeBsId) {
		log.info("Class: LogIncidentDaoImpl, Method: getSaasMandoeResourcesList Start");
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select	cia.ciid, cia.ciname from configurationitem cia, configurationitem cib ");
		queryBuilder.append("where 	cia.siteid = cib.siteid and cia.status = 'Operational' and cia.citype='Resource' ");
		queryBuilder.append("and cia.productid like '%Mandoe%' and cib.ciid=:SaasBsCiid");
		
		log.debug("Query for getSaasResourcesList :: " + queryBuilder.toString());
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
		query.setString("SaasBsCiid", saasMandoeBsId);
		
		List resultList = query.list();
		log.debug("Result List :: " + resultList + ", Size :: " + resultList != null ? "" + resultList.size() : null);

		log.info("Class: LogIncidentDaoImpl, Method: getSaasMandoeResourcesList End");
		return resultList;
	}
}
