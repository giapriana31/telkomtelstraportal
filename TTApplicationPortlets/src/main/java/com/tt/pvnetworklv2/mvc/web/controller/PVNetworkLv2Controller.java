package com.tt.pvnetworklv2.mvc.web.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.model.Tenoss;
import com.tt.pvnetworklv2.mvc.service.common.PVNetworkLv2ManagerImpl;
import com.tt.utils.TTGenericUtils;

/**
 * Portlet implementation class PVNetworkLv2Controller
 * author : ahmadi.harahap
 */

@Controller("pvNetworkLv2Controller")
@RequestMapping("VIEW")
public class PVNetworkLv2Controller {
	
	private PVNetworkLv2ManagerImpl pvNetworkLv2ManagerImpl;
	
	private final static Logger log = Logger.getLogger(PVNetworkLv2Controller.class);
	
	@Autowired
	@Qualifier(value = "pvNetworkLv2ManagerImpl")
	public void setPVNetworkLv2ManagerImpl(PVNetworkLv2ManagerImpl pvNetworkLv2ManagerImpl){
		this.pvNetworkLv2ManagerImpl = pvNetworkLv2ManagerImpl;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		
		log.info("PVNetworkLv2Controller-render()");
		
		
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);
		
		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		
		List<Tenoss> listNetworkTenoss = pvNetworkLv2ManagerImpl.getListNetworkTenossData(customerId);
		
		JSONArray dtJsonValue = JSONFactoryUtil.createJSONArray();
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		for(Tenoss networkTennos : listNetworkTenoss){
			
			JSONObject mediaItemsJsonObject = JSONFactoryUtil.createJSONObject();
			
			mediaItemsJsonObject.put("tenid", networkTennos.getTenId());
			mediaItemsJsonObject.put("customeruniqueid", networkTennos.getCustomerUniqueId());
			mediaItemsJsonObject.put("sitename", networkTennos.getSiteName());
			mediaItemsJsonObject.put("ciname", networkTennos.getCiName());
			mediaItemsJsonObject.put("carriageserviceid", networkTennos.getCarriageServiceId());
			mediaItemsJsonObject.put("cideliverystatus", networkTennos.getCiDeliveryStatus());
			mediaItemsJsonObject.put("nsorder_tq", networkTennos.getNsOrderTq()!=null?networkTennos.getNsOrderTq():"");
			mediaItemsJsonObject.put("nsorder_ao", networkTennos.getNsOrderAo()!=null?networkTennos.getNsOrderAo():"");
			mediaItemsJsonObject.put("status_ticares", networkTennos.getStatusTicares()!=null?networkTennos.getStatusTicares():"");
			mediaItemsJsonObject.put("task_name_ticares", networkTennos.getTaskNameTicares()!=null?networkTennos.getTaskNameTicares():"");
			mediaItemsJsonObject.put("task_stage_tenoss", networkTennos.getTaskStageTenoss()!=null?networkTennos.getTaskStageTenoss():"");
			mediaItemsJsonObject.put("task_status_tenoss", networkTennos.getTaskStatusTenoss()!=null?networkTennos.getTaskStatusTenoss():"");
			mediaItemsJsonObject.put("task_name_tenoss", networkTennos.getTaskNameTenoss()!=null?networkTennos.getTaskNameTenoss():"");
			mediaItemsJsonObject.put("modified_by", networkTennos.getModifiedBy()!=null?networkTennos.getModifiedBy():"");
			mediaItemsJsonObject.put("modified_date", networkTennos.getModifiedDate()!=null?df.format(networkTennos.getModifiedDate()):"");
			mediaItemsJsonObject.put("targetProvisioningDate", networkTennos.getTargetProvisioningDate()!=null?df.format(networkTennos.getTargetProvisioningDate()):"");
			mediaItemsJsonObject.put("targetInstallDate", networkTennos.getTargetInstallDate()!=null?df.format(networkTennos.getTargetInstallDate()):"");
						
			dtJsonValue.put(mediaItemsJsonObject);
		}
		
		log.info("PVNetworkLv2Controller-Json : " + dtJsonValue.toString());
		
		model.addAttribute("listNetworkTenossDatatable", dtJsonValue);
		
		return "pvNetworkLv2";
	}
}
