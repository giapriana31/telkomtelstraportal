package com.tt.pvnetworklv2.mvc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.Tenoss;

@Repository(value = "pvNetworkLv2DaoImpl")
@Transactional
public class PVNetworkLv2DaoImpl {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private final static Logger log = Logger.getLogger(PVNetworkLv2DaoImpl.class);
	
	public List<Tenoss> getListNetworkTenossData(String customerid){
		//List<NetworkTenoss> tenossList = new ArrayList<NetworkTenoss>();
		
		log.info("PVNetworkLv2DaoImpl, getListNetworkTenossData with customerUniqueId : " + customerid);
		
		
		StringBuilder queryStr = new StringBuilder();
		queryStr.append("from Tenoss t where t.customerUniqueId = :customeruniqueid");
		Query queryResult = sessionFactory.getCurrentSession().createQuery(queryStr.toString());
		queryResult.setString("customeruniqueid", customerid);
		
		@SuppressWarnings("unchecked")
		List<Tenoss> tenossList = (List<Tenoss>) queryResult.list();
		
		return tenossList;
	}
}
