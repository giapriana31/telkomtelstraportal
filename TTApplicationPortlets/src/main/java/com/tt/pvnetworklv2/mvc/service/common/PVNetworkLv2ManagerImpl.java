package com.tt.pvnetworklv2.mvc.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.Tenoss;
import com.tt.pvnetworklv2.mvc.dao.PVNetworkLv2DaoImpl;

@Service(value = "pvNetworkLv2ManagerImpl")
@Transactional
public class PVNetworkLv2ManagerImpl {
	
	private PVNetworkLv2DaoImpl pvNetworkLv2DaoImpl;
	
	@Autowired
	@Qualifier(value="pvNetworkLv2DaoImpl")
	public void setPVNetworkLv2DaoImpl(PVNetworkLv2DaoImpl pvNetworkLv2DaoImpl) {
		this.pvNetworkLv2DaoImpl = pvNetworkLv2DaoImpl;
	}
	
	public List<Tenoss> getListNetworkTenossData(String customerid) {
		return pvNetworkLv2DaoImpl.getListNetworkTenossData(customerid);
	}
}
