package com.tt.evdashboardview.mvc.service.common;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.evdashboardview.mvc.dao.EVDashboardIncidentDaoImpl;
import com.tt.evdashboardview.mvc.dao.EVDashboardPerformanceDaoImpl;

@Service(value="evDashboardPerformanceManagerImpl")
@Transactional
public class EVDashboardPerformanceManagerImpl {

	private EVDashboardPerformanceDaoImpl evDashboardPerformanceDaoImpl;

	@Autowired
	@Qualifier("evDashboardPerformanceDaoImpl")
	public void setEvDashboardPerformanceDaoImpl(EVDashboardPerformanceDaoImpl evDashboardPerformanceDaoImpl) {
		this.evDashboardPerformanceDaoImpl = evDashboardPerformanceDaoImpl;
	}
	
	public LinkedHashMap<String, LinkedHashMap<String, String>> getPerformanceOverviewResults(LinkedHashMap<String, ArrayList<String>> timeWindows,String customerUniqueID){
		
		LinkedHashMap<String, LinkedHashMap<String, String>> spdGraphData=null;
		
		if(evDashboardPerformanceDaoImpl.getPerformanceOverviewResults(timeWindows,customerUniqueID)!=null){
			LinkedHashMap<String, ArrayList<String>> result= evDashboardPerformanceDaoImpl.getPerformanceOverviewResults(timeWindows,customerUniqueID);
			
			ArrayList<String> speedometerList= result.get("speedometer");
			
			spdGraphData= new LinkedHashMap<String, LinkedHashMap<String,String>>();
			
			LinkedHashMap<String, String> spdGraphDataMNS = new LinkedHashMap<String, String>();
			
			spdGraphDataMNS.put("MNSMon", speedometerList.get(0));
			spdGraphDataMNS.put("MNSQtr", speedometerList.get(1));
			spdGraphDataMNS.put("MNSYr", speedometerList.get(2));
			
			spdGraphData.put("MNS", spdGraphDataMNS);
			
			LinkedHashMap<String, String> spdGraphDataCloud = new LinkedHashMap<String, String>();
			
			spdGraphDataCloud.put("CloudMon", speedometerList.get(3));
			spdGraphDataCloud.put("CloudQtr", speedometerList.get(4));
			spdGraphDataCloud.put("CloudYr", speedometerList.get(5));
			
			spdGraphData.put("Cloud", spdGraphDataCloud);
			
			LinkedHashMap<String, String> spdGraphDataSaaS = new LinkedHashMap<String, String>();
			
			spdGraphDataSaaS.put("SaaSMon", speedometerList.get(6));
			spdGraphDataSaaS.put("SaaSQtr", speedometerList.get(7));
			spdGraphDataSaaS.put("SaaSYr", speedometerList.get(8));
			
			spdGraphData.put("SaaS", spdGraphDataSaaS);
			
			LinkedHashMap<String, String> spdGraphDataUC = new LinkedHashMap<String, String>();
			
			spdGraphDataUC.put("UCMon","null");
			spdGraphDataUC.put("UCQtr", "null");
			spdGraphDataUC.put("UCYr", "null");
			
			spdGraphData.put("UC", spdGraphDataUC);
			
			LinkedHashMap<String, String> spdGraphDataManageSecurity = new LinkedHashMap<String, String>();
			
			spdGraphDataManageSecurity.put("ManageSecurityMon","null");
			spdGraphDataManageSecurity.put("ManageSecurityQtr", "null");
			spdGraphDataManageSecurity.put("ManageSecurityYr", "null");
			
			spdGraphData.put("ManageSecurity", spdGraphDataManageSecurity);
		}
		return spdGraphData;
	}
	
	public ArrayList<LinkedHashMap<String, String>> getMonthlySingleBarData(ArrayList<String> monthWindows,String customerUniqueId){
		
		return evDashboardPerformanceDaoImpl.getMonthlySingleBarData(monthWindows, customerUniqueId);
	}
	
	public ArrayList<LinkedHashMap<String, String>> getMonthlySingleBarDataForService(ArrayList<String> monthWindows,String customerUniqueId, String service){
		
		return evDashboardPerformanceDaoImpl.getMonthlySingleBarDataForService(monthWindows, customerUniqueId, service);
	}
	
	public ArrayList<LinkedHashMap<String, String>> getQuarterlySingleBarDataForService(LinkedHashMap<String, ArrayList<String>> quarterWindows,String customerUniqueID, String service){
		
		return evDashboardPerformanceDaoImpl.getQuarterlySingleBarDataForService(quarterWindows, customerUniqueID, service);
	}
	
}
