
package com.tt.evdashboardview.mvc.web.controller;

import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.metamodel.source.annotations.attribute.type.TemporalTypeResolver;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.common.subscribedservices.SubscribedService;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.evdashboardview.mvc.service.common.EVDashboardDeliveryManagerImpl;
import com.tt.evdashboardview.mvc.service.common.EVDashboardIncidentManagerImpl;
import com.tt.evdashboardview.mvc.service.common.EVDashboardPerformanceManagerImpl;
import com.tt.evdashboardview.mvc.service.common.EVDashboardUtilizationManagerImpl;
import com.tt.logging.Logger;
import com.tt.model.DeliveryEVMetadata;
import com.tt.model.DeliverySpeedometerMetadata;
import com.tt.util.RefreshUtil;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("evDashboardViewController")
@RequestMapping("VIEW")
public class EVDashboardViewController {

    private EVDashboardIncidentManagerImpl evDashboardIncidentManagerImpl;
    private EVDashboardDeliveryManagerImpl evDashboardDeliveryManagerImpl;
    private EVDashboardPerformanceManagerImpl evDashboardPerformanceManagerImpl;
    private EVDashboardUtilizationManagerImpl evDashboardUtilizationManagerImpl;
    private List<String> productList;
    private HashMap<String, String> productMap= new HashMap<String, String>();    
    
	public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(EVDashboardViewController.class);
    private static final String[] quarterCounters= {"Quarter1","Quarter2","Quarter3"};
    
    
    @Autowired
    @Qualifier("evDashboardIncidentManagerImpl")
    public void setEvDashboardIncidentManagerImpl(EVDashboardIncidentManagerImpl evDashboardIncidentManagerImpl) {
		this.evDashboardIncidentManagerImpl = evDashboardIncidentManagerImpl;
	}
    
    @Autowired
    @Qualifier("evDashboardDeliveryManagerImpl")
    public void setEvDashboardDeliveryManagerImpl(EVDashboardDeliveryManagerImpl evDashboardDeliveryManagerImpl) {
		this.evDashboardDeliveryManagerImpl = evDashboardDeliveryManagerImpl;
	}
    
    
    @Autowired
    @Qualifier("evDashboardPerformanceManagerImpl")
    public void setEvDashboardPerformanceManagerImpl(EVDashboardPerformanceManagerImpl evDashboardPerformanceManagerImpl) {
		this.evDashboardPerformanceManagerImpl = evDashboardPerformanceManagerImpl;
	}
    

    @Autowired
    @Qualifier("evDashboardUtilizationManagerImpl")
    public void setEvDashboardUtilizationManagerImpl(EVDashboardUtilizationManagerImpl evDashboardUtilizationManagerImpl) {
		this.evDashboardUtilizationManagerImpl = evDashboardUtilizationManagerImpl;
	}
    
    private static Map<String, String> mapServiceNames() {
 		Map<String, String> serviceNames = new HashMap<String, String>();
 		serviceNames.put("MNS", "MNS");
 		serviceNames.put("CLOUD", "Private Cloud");
 		serviceNames.put("SAAS", "SaaS");
		

 		return Collections.unmodifiableMap(serviceNames);
 	}

    // todo:needs to move it to constants
 	// html table column order is important
 	private static final Map<String, String> DTSERVICENAMES = mapServiceNames();
    
    @RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws Exception {
		try {
			HttpServletRequest httprequest = PortalUtil
					.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil
					.getHttpServletResponse(renderResponse);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(
					httprequest, httpresponse);
			
			productList=evDashboardDeliveryManagerImpl.getProductList();
			
			productMap.put("MNS", "MNS");
			productMap.put("Private Cloud", "Cloud");
			productMap.put("SaaS", "SaaS");
			productMap.put("Unified Comm", "UC");
			productMap.put("Security", "ManageSecurity");

			return "evDashboardView";
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
    
    @ResourceMapping(value = "getSummaryBarGraphData")
	public void getSummaryGraphData(ResourceRequest req, ResourceResponse res,
			Model model) {
		log.info("EVDashboardViewController ResourceMap starts");

		try {

			HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
			HttpServletResponse response = PortalUtil
					.getHttpServletResponse(res);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(request, response);
			log.info("setting Cust ID..." + customerUniqueID);
			
			
			String selectedEntity=(String)request.getParameter("currentSelectedEntity");			
			String windowUsed=(String)request.getParameter("serviceIdentifier");
			String service=(String)request.getParameter("windowIdentifier");
			
			log.info("service:"+service+" window:"+windowUsed);
			
			String serviceParameter=DTSERVICENAMES.get(service);
			
			SubscribedService ss = new SubscribedService();

			int count=ss.isSubscribedServices(serviceParameter, customerUniqueID);
			
			
			if (count!=0) {
				
				if(windowUsed.equalsIgnoreCase("yearly")){
					
					
					com.liferay.portal.kernel.json.JSONObject obj = JSONFactoryUtil.createJSONObject();
					obj.put("Service", "Yearly");
					
					PrintWriter out;
					out = response.getWriter();
					out.print(obj);
					
				}
			else {
					ArrayList<LinkedHashMap<String, String>> barGraphs = null;
					ArrayList<String> periodIntervals = null;
					JSONObject parentJsonData = new JSONObject();
					if (selectedEntity.equalsIgnoreCase("Incident")) {
						switch (windowUsed) {
						case "MONTHLY":
							barGraphs = evDashboardIncidentManagerImpl
									.getMonthlySingleBarDataForService(
											getBarMonthWindows(),
											customerUniqueID, service);
							periodIntervals = getMonthlyPeriodIntervals();

							break;
						case "QUARTERLY":
							barGraphs = evDashboardIncidentManagerImpl
									.getQuarterlySingleBarDataForService(
											getBarQuarterWindows(),
											customerUniqueID, service);
							periodIntervals = getQuarterPeriodIntervals();

							break;
						case "YEARLY":
							break;
						}

						for (int i = 0; i < barGraphs.size(); i++) {
							parentJsonData.put("barGraph" + (i + 1),
									getSingleBarGraphJson(barGraphs.get(i)));
						}

					}

					else if (selectedEntity.equalsIgnoreCase("Delivery")) {
						
						if (service.equalsIgnoreCase("MNS")) {
							List<String> monthWindow = getDeliveryMonthWindow(windowUsed);
							monthWindow.remove(monthWindow.size() - 1);
							List<DeliveryEVMetadata> graphData = evDashboardDeliveryManagerImpl
									.getBargraphData(customerUniqueID,
											monthWindow, service);
							barGraphs = getSingleGraphData(graphData);

							int ctr = 0;

							for (int i = barGraphs.size() - 1; i >= 0; i--) {
								parentJsonData.put("barGraph" + (i + 1),
										getSingleBarGraphJson(barGraphs
												.get(ctr)));
								ctr++;
							}
						}else if(service.equalsIgnoreCase("SaaS")){
							
							List<String> monthWindow = getDeliveryMonthWindow(windowUsed);
							monthWindow.remove(monthWindow.size() - 1);
							List<DeliveryEVMetadata> graphData = evDashboardDeliveryManagerImpl
									.getBargraphData(customerUniqueID,
											monthWindow, service);
							barGraphs = getSingleGraphData(graphData);

							int ctr = 0;

							for (int i = barGraphs.size() - 1; i >= 0; i--) {
								parentJsonData.put("barGraph" + (i + 1),
										getSingleBarGraphJson(barGraphs
												.get(ctr)));
								ctr++;
							}
	
							
							
							
						}

						else {

							switch (windowUsed) {
							case "MONTHLY":
								barGraphs = evDashboardDeliveryManagerImpl
										.getMonthlySingleBarDataForService(
												getBarMonthWindows(),
												customerUniqueID, service);

								break;
							case "QUARTERLY":
								barGraphs = evDashboardDeliveryManagerImpl
										.getQuarterlySingleBarDataForService(
												getBarQuarterWindows(),
												customerUniqueID, service);

								break;
							case "YEARLY":
								break;
							}

							for (int i = 0; i < barGraphs.size(); i++) {
								parentJsonData
										.put("barGraph" + (i + 1),
												getSingleBarGraphJson(barGraphs
														.get(i)));
							}
						}

						switch (windowUsed) {
						case "MONTHLY":

							periodIntervals = getMonthlyPeriodIntervals();

							break;
						case "QUARTERLY":

							periodIntervals = getQuarterPeriodIntervals();

							break;
						case "YEARLY":
							break;
						}

					}

					else if (selectedEntity.equalsIgnoreCase("Performance")) {
						switch (windowUsed) {
						case "MONTHLY":
							barGraphs = evDashboardPerformanceManagerImpl
									.getMonthlySingleBarDataForService(
											getBarMonthWindows(),
											customerUniqueID, service);
							periodIntervals = getMonthlyPeriodIntervals();

							break;
						case "QUARTERLY":
							barGraphs = evDashboardPerformanceManagerImpl
									.getQuarterlySingleBarDataForService(
											getBarQuarterWindows(),
											customerUniqueID, service);
							periodIntervals = getQuarterPeriodIntervals();

							break;
						case "YEARLY":
							break;
						}

						for (int i = 0; i < barGraphs.size(); i++) {
							parentJsonData.put("barGraph" + (i + 1),
									getSingleBarGraphJson(barGraphs.get(i)));
						}
					}

					else if (selectedEntity.equalsIgnoreCase("Utilization")) {
						switch (windowUsed) {
						case "MONTHLY":
							barGraphs = evDashboardUtilizationManagerImpl
									.getMonthlySingleBarDataForService(
											getBarMonthWindows(),
											customerUniqueID, service);
							periodIntervals = getMonthlyPeriodIntervals();

							break;
						case "QUARTERLY":
							barGraphs = evDashboardUtilizationManagerImpl
									.getQuarterlySingleBarDataForService(
											getBarQuarterWindows(),
											customerUniqueID, service);
							periodIntervals = getQuarterPeriodIntervals();

							break;
						case "YEARLY":
							break;
						}

						for (int i = 0; i < barGraphs.size(); i++) {
							parentJsonData.put("barGraph" + (i + 1),
									getSingleBarGraphJson(barGraphs.get(i)));
						}
					}
					for (int i = 0; i < periodIntervals.size(); i++) {
						parentJsonData.put("graphInterval" + (i + 1),
								periodIntervals.get(i));
					}
					PrintWriter out;
					out = response.getWriter();
					out.print(parentJsonData.toString());
					
					
				}
				
			}
			else if(service.equalsIgnoreCase("SaaS")){

				com.liferay.portal.kernel.json.JSONObject obj = JSONFactoryUtil.createJSONObject();
				obj.put("Service", "SaaS NS");
				
				PrintWriter out;
				out = response.getWriter();
				out.print(obj);
				
			}
			
			
			else if(service.equalsIgnoreCase("Cloud")){

				com.liferay.portal.kernel.json.JSONObject obj = JSONFactoryUtil.createJSONObject();
				obj.put("Service", "Cloud NS");
				
				PrintWriter out;
				out = response.getWriter();
				out.print(obj);
				
			}else if(service.equalsIgnoreCase("MNS")){
				
				com.liferay.portal.kernel.json.JSONObject obj = JSONFactoryUtil.createJSONObject();
				obj.put("Service", "MNS NS");
				
				PrintWriter out;
				out = response.getWriter();
				out.print(obj);
				
			}else if(service.equalsIgnoreCase("SAAS")){
				
				com.liferay.portal.kernel.json.JSONObject obj = JSONFactoryUtil.createJSONObject();
				obj.put("Service", "SAAS NS");
				
				PrintWriter out;
				out = response.getWriter();
				out.print(obj);
				
			}
			
			
		
		}

		catch (Exception e) {
			log.error(e.getMessage());
		}

	}
    
    public ArrayList<String> getMonthlyPeriodIntervals(){
    	
    	ArrayList<String> monthlyIntervals= new ArrayList<String>();
    	
    	ArrayList<String> singleBarMonthWindows=getBarMonthWindows();
    	
    	for (int i = 0; i < singleBarMonthWindows.size(); i++) { 
			SimpleDateFormat simpleDateFormatIn= new SimpleDateFormat("yyyy-MM");
			Date date= new Date();
			try {
				date = simpleDateFormatIn.parse(singleBarMonthWindows.get(i));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SimpleDateFormat simpleDateFormatOut= new SimpleDateFormat("MMMMM yyyy");
			String monthIntervals= simpleDateFormatOut.format(date);
			log.info("new period intervals--"+monthIntervals);
			monthlyIntervals.add(monthIntervals);
		}
    	
    	return monthlyIntervals;
    }
    
    
    public ArrayList<String> getQuarterPeriodIntervals(){
    	
    	ArrayList<String> periodIntervals= new ArrayList<String>();
		
    	LinkedHashMap<String, ArrayList<String>> quarters=getBarQuarterWindows();
    	
		for (int i = 0; i < quarterCounters.length; i++) {
			String quarterMonth=quarters.get(quarterCounters[i]).get(0);
			SimpleDateFormat simpleDateFormatIn= new SimpleDateFormat("yyyy-MM");
			Date date= new Date();
			try {
				date = simpleDateFormatIn.parse(quarterMonth);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Calendar cal= Calendar.getInstance();
			cal.setTime(date);
			int quarter = (cal.get(Calendar.MONTH) / 3) + 1;
			String quarterInterval="Quarter"+quarter+" "+cal.get(Calendar.YEAR);
			log.info("quarterInterval--"+quarterInterval);
			periodIntervals.add(quarterInterval);
			
		}
		
		return periodIntervals;
    }
    
    public ArrayList<LinkedHashMap<String, LinkedHashMap<String, String>>> getDualGraphData(String customerId){
 /*   	HashMap<String, String> productMap= new HashMap<String, String>(); 
		productMap.put("MNS", "MNS");
		productMap.put("Private Cloud", "Cloud");
		productMap.put("SaaS", "SaaS");
		productMap.put("Unified Comm", "UC");
		productMap.put("Security", "ManageSecurity");
    	
    	List<String> productList= new ArrayList<String>();
    	productList.add("MNS");
    	productList.add("Private Cloud");
    	productList.add("SaaS");
    	productList.add("UC");
    	productList.add("ManageSecurity");
    	List<List<DeliveryEVMetadata>> baseProductData= new ArrayList<List<DeliveryEVMetadata>>();
    	for (String product : productList) {
			
    		
    		baseProductData.add(getGraphList(customerId, "Monthly", product));
    		
    		
		}
    	
    	
    	for (List<DeliveryEVMetadata> productDataList : baseProductData) {
			
		}
    	*/
    	
    	
    	ArrayList<LinkedHashMap<String, LinkedHashMap<String, String>>> dualGraphDataList= new ArrayList<LinkedHashMap<String,LinkedHashMap<String,String>>>();	
    	
 		List<DeliveryEVMetadata>graphListMNS= getGraphList(customerId,"Monthly","MNS");
		List<DeliveryEVMetadata>graphListCloud= getGraphList(customerId,"Monthly","Private Cloud");
		List<DeliveryEVMetadata>graphListSaaS= getGraphList(customerId,"Monthly","SaaS");
		
		if (null!=graphListMNS && graphListMNS.size()>0 && null!=graphListCloud && graphListCloud.size()>0 && null!=graphListSaaS && graphListSaaS.size()>0) {
			 
			for (int i =graphListMNS.size()-1; i >=0; i--) {
				
				
				LinkedHashMap<String, String> innerLinkedHashMapTotal = new LinkedHashMap<String, String>();
				LinkedHashMap<String, String> innerLinkedHashMapNew = new LinkedHashMap<String, String>();
				LinkedHashMap<String, LinkedHashMap<String, String>> dualGraphData = new LinkedHashMap<String, LinkedHashMap<String, String>>();

		    	
				if (i-1>=0 ) {

					
					
					DeliveryEVMetadata evMetadata = graphListMNS.get(i-1);
					DeliveryEVMetadata evMetadataAhead = graphListMNS.get(i);
					if (!evMetadata.getTotalCount().equalsIgnoreCase("null")) {

										if (!evMetadataAhead.getTotalCount().equalsIgnoreCase("null")) {
											innerLinkedHashMapTotal
													.put(evMetadata.getCmdbClass()
															+ "Total",
															evMetadata
																	.getOperationalSiteCount());
											dualGraphData.put("Total",
													innerLinkedHashMapTotal);
										
										
											int newSites = Integer
													.parseInt(evMetadata
															.getOperationalSiteCount())
													- Integer
															.parseInt(evMetadataAhead
																	.getOperationalSiteCount());
											if (newSites > 0) {
												
												innerLinkedHashMapNew.put(
														evMetadata.getCmdbClass(),
														"" + newSites);
											} else {
										
												innerLinkedHashMapNew.put(
														evMetadata.getCmdbClass(),
														"" + 0);

											}
											dualGraphData.put("New",
													innerLinkedHashMapNew);
										}
										
										else{
											
											innerLinkedHashMapTotal.put("MNSTotal",""+evMetadata.getOperationalSiteCount());
											innerLinkedHashMapNew.put("MNS", "" + evMetadata.getOperationalSiteCount());
											
										}
					}
					else{
						innerLinkedHashMapTotal.put("MNSTotal",""+0);
						innerLinkedHashMapNew.put("MNS", "" + 0);
							
					
					}
		
	}
	
	
				
				
				if ( i -1>=0) {

					
					DeliveryEVMetadata evMetadata = graphListCloud.get(i-1);
					DeliveryEVMetadata evMetadataAhead = graphListCloud.get(i);

					if (!evMetadata.getTotalCount().equalsIgnoreCase("null")) {

										if (!evMetadataAhead.getTotalCount().equalsIgnoreCase("null")) {
											
											
											innerLinkedHashMapTotal
													.put("PrivateCloudTotal",
															evMetadata
																	.getOperationalSiteCount());
											dualGraphData.put("Total",
													innerLinkedHashMapTotal);
										
										
											int newSites = Integer
													.parseInt(evMetadata
															.getOperationalSiteCount())
													- Integer
															.parseInt(evMetadataAhead
																	.getOperationalSiteCount());
											if (newSites > 0) {
											
												innerLinkedHashMapNew.put(
														"Private Cloud",
														"" + newSites);
											} else {
												innerLinkedHashMapNew.put(
														"Private Cloud",
														"" + 0);

											}
											dualGraphData.put("New",
													innerLinkedHashMapNew);
										}
										
										else{
											
											innerLinkedHashMapTotal.put("Private CloudTotal",""+evMetadata.getOperationalSiteCount());
											innerLinkedHashMapNew.put("Private Cloud", "" + evMetadata.getOperationalSiteCount());
											
										}
					}
					else{
						innerLinkedHashMapTotal.put("Private CloudTotal",""+0);
						innerLinkedHashMapNew.put("Private Cloud", "" + 0);
							
					
					}
		
	}
	

				if ( i -1>=0) {

					
					DeliveryEVMetadata evMetadata = graphListSaaS.get(i-1);
					DeliveryEVMetadata evMetadataAhead = graphListSaaS.get(i);

					if (!evMetadata.getTotalCount().equalsIgnoreCase("null")) {

										if (!evMetadataAhead.getTotalCount().equalsIgnoreCase("null")) {
											
											
											innerLinkedHashMapTotal
													.put("SaaSTotal",
															evMetadata
																	.getOperationalSiteCount());
											dualGraphData.put("Total",
													innerLinkedHashMapTotal);
										
										
											int newSites = Integer
													.parseInt(evMetadata
															.getOperationalSiteCount())
													- Integer
															.parseInt(evMetadataAhead
																	.getOperationalSiteCount());
											if (newSites > 0) {
											
												innerLinkedHashMapNew.put(
														"SaaS",
														"" + newSites);
											} else {
												innerLinkedHashMapNew.put(
														"SaaS",
														"" + 0);

											}
											dualGraphData.put("New",
													innerLinkedHashMapNew);
										}
										
										else{
											
											innerLinkedHashMapTotal.put("SaaSTotal",""+evMetadata.getOperationalSiteCount());
											innerLinkedHashMapNew.put("SaaS", "" + evMetadata.getOperationalSiteCount());
											
										}
					}
					else{
						innerLinkedHashMapTotal.put("SaaSTotal",""+0);
						innerLinkedHashMapNew.put("SaaS", "" + 0);
							
					
					}
		
	}
	
	

/*
			innerLinkedHashMapTotal.put("CloudTotal", "0");
			
	    	innerLinkedHashMapNew.put("Cloud", "0");
			
			*/
				
				
				/*innerLinkedHashMapTotal.put("SaaSTotal", "0");
				
				innerLinkedHashMapNew.put("SaaS", "0");
				
				*/
			
			
				innerLinkedHashMapTotal.put("UCTotal", "0");
				innerLinkedHashMapNew.put("UC", "0");
				
				innerLinkedHashMapTotal.put("Manage SecurityTotal", "0");
				innerLinkedHashMapNew.put("Manage Security", "0");
				
				
				dualGraphData.put("Total", innerLinkedHashMapTotal);
				dualGraphData.put("New", innerLinkedHashMapNew);
				dualGraphDataList.add(dualGraphData);
			
		

		}
			
		/*	innerLinkedHashMapTotal.put("SaaSTotal", "0");
				
			innerLinkedHashMapNew.put("SaaS", "0");
			
			
			
			
			
		
			innerLinkedHashMapTotal.put("UCTotal", "0");
			innerLinkedHashMapNew.put("UC", "0");
			
			dualGraphData.put("Total", innerLinkedHashMapTotal);
			dualGraphData.put("New", innerLinkedHashMapNew);
			dualGraphDataList.add(dualGraphData);
			*/

	}
	
	
	
	
	return dualGraphDataList;
    }

    public ArrayList<LinkedHashMap<String, String> > getSingleGraphData( List<DeliveryEVMetadata> graphDataList){
    	
    	System.out.println("List size get single: "+graphDataList);
    	
    	
    	ArrayList<LinkedHashMap<String, String> > singleGraphContainerMap= new ArrayList<LinkedHashMap<String, String> >();

		if (null!=graphDataList && graphDataList.size()>0) {
			for (DeliveryEVMetadata deliveryEVMetadata : graphDataList) {

				LinkedHashMap<String, String> singleGraphMap = new LinkedHashMap<String, String>();

				if (!deliveryEVMetadata.getTotalCount()
						.equalsIgnoreCase("null")) {
					
				
					singleGraphMap
					.put("Order Received", deliveryEVMetadata
							.getOrderreceivedSitesCount());
					singleGraphMap.put("Detailed Design", deliveryEVMetadata
							.getDetailedDesignSitesCount());
					singleGraphMap.put("Procurement",
							deliveryEVMetadata.getProcurementSitesCount());
			
					singleGraphMap.put("Delivery and Activation",
							deliveryEVMetadata
									.getDeliveryActivationSitesCount());
					
					singleGraphMap.put("Monitoring",
							deliveryEVMetadata.getMonitoringSitesCount());
				
					
					singleGraphMap.put("Operational",
							deliveryEVMetadata.getOperationalSiteCount());
					
					
				
					

				
				
				} else {
					
					singleGraphMap.put("Order Received", "0");
					singleGraphMap.put("Detailed Design", "0");
					singleGraphMap.put("Procurement", "0");
					singleGraphMap.put("Delivery and Activation", "0");
					singleGraphMap.put("Monitoring", "0");
				
					singleGraphMap.put("Operational", "0");
					
					
				
				
					
				}
				
				singleGraphContainerMap.add(singleGraphMap);
			}
		}
    	return singleGraphContainerMap;
    	
    	
    }
    
	@ResourceMapping(value = "getSpeedoMeterGraphData")
	public void getSpeedoMeterData(ResourceRequest req, ResourceResponse res,
			Model model) {
		log.info("EVDashboardViewController ResourceMap starts");

		try {

			HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
			HttpServletResponse response = PortalUtil
					.getHttpServletResponse(res);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(request, response);
			log.info("setting Cust ID..." + customerUniqueID);
			
			
			String selectedEntity=(String)request.getParameter("smGraphDataIdentifier");
			
			log.info("Received entity--"+selectedEntity);
			
			LinkedHashMap<String, LinkedHashMap<String, String>> spdGraphData=null;
			ArrayList<LinkedHashMap<String, String>> singleBarMonthData= null;
			ArrayList<LinkedHashMap<String, LinkedHashMap<String, String>>> dualBarMonthData= null;
			JSONObject parentJsonData= new JSONObject();
			ArrayList<JSONArray> barGraphs= new ArrayList<JSONArray>();
			
			if(selectedEntity.equalsIgnoreCase("Incident")){
				LinkedHashMap<String, ArrayList<String>> timeWindows=getSpdMonthWindows();
				ArrayList<String> singleBarMonthWindows= getBarMonthWindows();
				
				
				for (String key : timeWindows.keySet()) {
					for (String listValue : timeWindows.get(key)) {
						log.info("windows--"+key+":"+listValue);
					}
				}
				
				log.info("for Dao");
				spdGraphData=evDashboardIncidentManagerImpl.getIncidentOverviewResults(timeWindows, customerUniqueID);
				singleBarMonthData=evDashboardIncidentManagerImpl.getMonthlySingleBarData(singleBarMonthWindows, customerUniqueID);
				for (int i = 0; i < singleBarMonthData.size(); i++) {
					barGraphs.add(getSingleBarGraphJson(singleBarMonthData.get(i)));
				}
				
				for (int i = 0; i < barGraphs.size(); i++) {
					parentJsonData.put("barGraph"+(i+1), barGraphs.get(i));
				}
				
			}
			
			else if(selectedEntity.equalsIgnoreCase("Delivery")){
				
		    	//List<DeliveryEVMetadata>graphList= getGraphList(customerUniqueID,"Monthly");
				
				dualBarMonthData= getDualGraphData(customerUniqueID);
		    	
		    	spdGraphData=getDeliverySpeedometerData(customerUniqueID);
		    	
		    	for (int i = 0; i < dualBarMonthData.size(); i++) {
					parentJsonData.put("barGraph"+(i+1), getDualBarGraphJson(dualBarMonthData.get(i)));
				}
		    	
				
			}
			
			else if(selectedEntity.equalsIgnoreCase("Performance")){
				LinkedHashMap<String, ArrayList<String>> timeWindows=getSpdMonthWindows();
				ArrayList<String> singleBarMonthWindows= getBarMonthWindows();
				
				
				for (String key : timeWindows.keySet()) {
					for (String listValue : timeWindows.get(key)) {
						log.info("windows--"+key+":"+listValue);
					}
				}
				
				log.info("for Dao");
				spdGraphData=evDashboardPerformanceManagerImpl.getPerformanceOverviewResults(timeWindows, customerUniqueID);
				singleBarMonthData=evDashboardPerformanceManagerImpl.getMonthlySingleBarData(singleBarMonthWindows, customerUniqueID);
				for (int i = 0; i < singleBarMonthData.size(); i++) {
					barGraphs.add(getSingleBarGraphJson(singleBarMonthData.get(i)));
				}
				
				for (int i = 0; i < barGraphs.size(); i++) {
					parentJsonData.put("barGraph"+(i+1), barGraphs.get(i));
				}
			}
			
			else if(selectedEntity.equalsIgnoreCase("Utilization")){
				LinkedHashMap<String, ArrayList<String>> timeWindows=getSpdMonthWindows();
				ArrayList<String> singleBarMonthWindows= getBarMonthWindows();
				
				
				for (String key : timeWindows.keySet()) {
					for (String listValue : timeWindows.get(key)) {
						log.info("windows--"+key+":"+listValue);
					}
				}
				
				log.info("for Dao");
				spdGraphData=evDashboardUtilizationManagerImpl.getUtilizationOverviewResults(timeWindows, customerUniqueID);
				singleBarMonthData=evDashboardUtilizationManagerImpl.getMonthlySingleBarData(singleBarMonthWindows, customerUniqueID);
				for (int i = 0; i < singleBarMonthData.size(); i++) {
					barGraphs.add(getSingleBarGraphJson(singleBarMonthData.get(i)));
				}
				
				for (int i = 0; i < barGraphs.size(); i++) {
					parentJsonData.put("barGraph"+(i+1), barGraphs.get(i));
				}
			}
			
			ArrayList<String> periodIntervals= getMonthlyPeriodIntervals();
			
			for (int i = 0; i < periodIntervals.size(); i++) {
				parentJsonData.put("graphInterval"+(i+1), periodIntervals.get(i));
			}
			
			JSONObject jsonObject= new JSONObject(spdGraphData);
			
			log.info("result json--"+jsonObject);
			
			
			
			
			parentJsonData.put("speedometerGraph", jsonObject);
			

			
			
			PrintWriter out;
			out=response.getWriter();
			
			out.print(parentJsonData.toString());
			
		
		}

		catch (Exception e) {
			log.error(e.getMessage());
		}

	}
	
	public JSONArray getDualBarGraphJson(LinkedHashMap<String, LinkedHashMap<String, String>> dualGraphMap){
		
		JSONArray jsonDualData= new JSONArray();
		
		
		LinkedHashMap<String, String> newMap = dualGraphMap.get("New");
		LinkedHashMap<String, String> totalMap = dualGraphMap.get("Total");
		
		ArrayList<String> serviceList= new ArrayList<String>();
		
		serviceList.addAll(newMap.keySet());
		
		for (int i = 0; i < serviceList.size(); i++) {
			String newCount= newMap.get(serviceList.get(i));
			String totalCount= totalMap.get(serviceList.get(i)+"Total");
			
			JSONObject jsonNew= new JSONObject();
			JSONObject jsonTotal = new JSONObject();
			
			try {
				jsonNew.put("key",serviceList.get(i));
				jsonNew.put("value",newCount);
				jsonNew.put("type","New");
				
				jsonTotal.put("key",serviceList.get(i)+"Total");
				jsonTotal.put("value",totalCount);
				jsonTotal.put("type","Total");
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			jsonDualData.put(jsonNew);
			jsonDualData.put(jsonTotal);
			
		}
		
		log.info("result dual array--"+jsonDualData);
		return jsonDualData;
	
		
	}
	
	public JSONArray getSingleBarGraphJson(LinkedHashMap<String, String> singleBarMap){
			
		JSONArray jsonSingleData= new JSONArray();
		
		for (String service : singleBarMap.keySet()) {
			
				JSONObject values= new JSONObject();
				try {
					values.put("value",singleBarMap.get(service));
					values.put("key", service);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				jsonSingleData.put(values);
			
		}
		
		log.info("single bar array--"+jsonSingleData);
		return jsonSingleData;
		
			
		}
	
	public ArrayList<String> getBarMonthWindows(){
		ArrayList<String> monthWindows= new ArrayList<String>();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
		cal.add(Calendar.MONTH, -3);
		
		for (int i = 0; i < 3; i++) {
			Date date= cal.getTime();
			String monthWindowStr = simpleDateFormat.format(date);
			monthWindows.add(monthWindowStr);
			cal.add(Calendar.MONTH, 1);
			
		}
		
		for (String string : monthWindows) {
			log.info("monthbarwindows--"+string);
		}
		
		return monthWindows;
		
		
	}
	
	public LinkedHashMap<String, ArrayList<String>> getBarQuarterWindows(){
		ArrayList<String> quarter1Window= new ArrayList<String>();
		ArrayList<String> quarter2Window= new ArrayList<String>();
		ArrayList<String> quarter3Window= new ArrayList<String>();
		
		LinkedHashMap<String, ArrayList<String>> quarters= new LinkedHashMap<String, ArrayList<String>>();
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
		
		int currMonth= cal.get(Calendar.MONTH);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		
		if(currMonth>=0 && currMonth<=2){
			cal.set(Calendar.MONTH, 11);
			cal.add(Calendar.YEAR, -1);
		}else if(currMonth>=3 && currMonth<=5){
			cal.set(Calendar.MONTH, 2);
		}else if(currMonth>=6 && currMonth<=8){
			cal.set(Calendar.MONTH, 5);
		}else if(currMonth>=9 && currMonth<=11){
			cal.set(Calendar.MONTH, 8);
		}
		
		cal.add(Calendar.MONTH, -8);
		
		for (int i = 0; i < 3; i++) {
			
			Date date= cal.getTime();
			String monthOfQuarter = simpleDateFormat.format(date);
			quarter1Window.add(monthOfQuarter);
			cal.add(Calendar.MONTH, 1);
			
		}
		
		for (int i = 0; i < 3; i++) {
					
					Date date= cal.getTime();
					String monthOfQuarter = simpleDateFormat.format(date);
					quarter2Window.add(monthOfQuarter);
					cal.add(Calendar.MONTH, 1);
					
				}

		for (int i = 0; i < 3; i++) {
			
			Date date= cal.getTime();
			String monthOfQuarter = simpleDateFormat.format(date);
			quarter3Window.add(monthOfQuarter);
			cal.add(Calendar.MONTH, 1);
			
		}
		
		quarters.put("Quarter1", quarter1Window);
		quarters.put("Quarter2", quarter2Window);
		quarters.put("Quarter3", quarter3Window);
		
		for (String qtr : quarters.keySet()) {
			for (String qtrmnth : quarters.get(qtr)) {
				log.info("quarterWindows--"+qtr+":"+qtrmnth);
			}
		}
		
		return quarters;
		
		
	}
	
	
	public LinkedHashMap<String, ArrayList<String>> getSpdMonthWindows(){
		
		LinkedHashMap<String, ArrayList<String>> spdMonthWindows= new LinkedHashMap<String, ArrayList<String>>();
		
		ArrayList<String> monthWindow= new ArrayList<String>();
		ArrayList<String> quarterWindow= new ArrayList<String>();
		ArrayList<String> yearWindow= new ArrayList<String>();
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		
		Date date = cal.getTime();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
		String monthWindowStr = simpleDateFormat.format(date);
		monthWindow.add(monthWindowStr);
		
		spdMonthWindows.put("Month", monthWindow);
		
		cal= Calendar.getInstance();
		
		int currMonth= cal.get(Calendar.MONTH);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		
		if(currMonth>=0 && currMonth<=2){
			cal.set(Calendar.MONTH, 11);
			cal.add(Calendar.YEAR, -1);
		}else if(currMonth>=3 && currMonth<=5){
			cal.set(Calendar.MONTH, 2);
		}else if(currMonth>=6 && currMonth<=8){
			cal.set(Calendar.MONTH, 5);
		}else if(currMonth>=9 && currMonth<=11){
			cal.set(Calendar.MONTH, 8);
		}
		
		for(int i=0;i<3;i++){
			date= cal.getTime();
			String quarterWindowStr= simpleDateFormat.format(date);
			quarterWindow.add(quarterWindowStr);
			yearWindow.add(quarterWindowStr);
			cal.add(Calendar.MONTH, -1);
		}
		
		spdMonthWindows.put("Quarter", quarterWindow);
		
		for(int i=0;i<9;i++){
			date= cal.getTime();
			String yearWindowStr= simpleDateFormat.format(date);
			yearWindow.add(yearWindowStr);
			cal.add(Calendar.MONTH, -1);
		}
				
		spdMonthWindows.put("Year", yearWindow);
		
		return spdMonthWindows;
	}
	
	public List<DeliveryEVMetadata> getGraphList(String customerId,String windowType,String productType){
    	
		List<String>	monthWindow=	getDeliveryMonthWindow(windowType);
    	
    	List<DeliveryEVMetadata>graphList= evDashboardDeliveryManagerImpl.getBargraphData(customerId, monthWindow,productType);
    	
    	return graphList;
	}
	
	
	
	public List<String> getDeliveryMonthWindow(String windowType){
		
		List<String> monthWindow= new ArrayList<String>();
    	Map<Integer,String> quaterMap=new LinkedHashMap<Integer, String>();
		quaterMap.put(1, "03");
		quaterMap.put(2, "06");
		quaterMap.put(3, "09");
		quaterMap.put(4, "12");
		final Calendar cal = Calendar.getInstance();
    	
    	
    	if(windowType.equalsIgnoreCase("Monthly")){
    				int count = 4;
    		
    		for (int i = 0; i < count; i++) {
    		  cal.add(Calendar.MONTH, -1);
    		  int month = (cal.get(Calendar.MONTH) ) + 1;
    		  String monthString=""+month;
    		  if(month<10){
    			  
    			  monthString="0"+month;
    			  
    		  }
    		  System.out.println("Monthly  " + cal.get(Calendar.YEAR)+"-"+monthString );
    		  monthWindow.add( cal.get(Calendar.YEAR)+"-"+ monthString);
    		}
    		
    		
    	}
    	if(windowType.equalsIgnoreCase("Quarterly")){
    		
    		int count = 4;
    		
    		for (int i = 0; i < count; i++) {
    		  cal.add(Calendar.MONTH, -3);
    		  int quarter = (cal.get(Calendar.MONTH) / 3) + 1;
    		  System.out.println("Newsletter quater " + cal.get(Calendar.YEAR)+"-"+ quaterMap.get(quarter));
    		  monthWindow.add( cal.get(Calendar.YEAR)+"-"+ quaterMap.get(quarter));
    		}
    		
    	}
    	
    	
    	return monthWindow;
		
	}
	
	
	
	   public LinkedHashMap<String, LinkedHashMap<String, String>> getDeliverySpeedometerData(String customerId){
			final Calendar cal = Calendar.getInstance();
			final Calendar calMonth = Calendar.getInstance();

			
			
			List<String> mnsData= new ArrayList<String>();
			List<String> cloudData= new ArrayList<String>();
			
			
			List<String> monthlyTimeline= getMonthYearList("monthly");

			List<String> quaterTimeline= getMonthYearList("quaterly");

			List<String> yearlyTimeline= getMonthYearList("yearly");
			  LinkedHashMap<String, LinkedHashMap<String, String>> speedometerDataMap=new LinkedHashMap<String, LinkedHashMap<String,String>>();

			for (String product : productList) {
				
				
				
				
				
				List<String> speedometerData= new ArrayList<String>();
				speedometerData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, monthlyTimeline, product));
				speedometerData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, quaterTimeline, product));
				speedometerData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, yearlyTimeline, product));
				
	
				  LinkedHashMap<String, String> speedometerDataInnerMap= new  LinkedHashMap<String, String>();


				  int dataCount=0;
				
				
				  if (null!=speedometerData && speedometerData.size()>0) {
						for (String deliveryEVMetadata : speedometerData) {

						String avg ="";
							if (!deliveryEVMetadata.equalsIgnoreCase("null")) {
								avg =deliveryEVMetadata;
								
								
								if (dataCount == 0) {
									
									if(avg.equalsIgnoreCase("null")){
										
										speedometerDataInnerMap.put(productMap.get(product)+"Mon", "null");

									}
									else if (Float.parseFloat(avg)/100 < 0.70) {

										speedometerDataInnerMap.put(productMap.get(product)+"Mon", "" + 0.83);

									} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

										speedometerDataInnerMap.put(productMap.get(product)+"Mon", "" + 0.5);

									} else if (Float.parseFloat(avg)/100 > 0.80) {

										speedometerDataInnerMap.put(productMap.get(product)+"Mon", "" + 0.17);

									} 

								} else if (dataCount == 1) {
									 if(avg.equalsIgnoreCase("null")){
										 speedometerDataInnerMap.put(productMap.get(product)+"Qtr", "null");

									}
									 else if (Float.parseFloat(avg)/100 < 0.70) {
										 speedometerDataInnerMap.put(productMap.get(product)+"Qtr", "" + 0.83);

									} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

										speedometerDataInnerMap.put(productMap.get(product)+"Qtr", "" + 0.5);

									} else if (Float.parseFloat(avg)/100 > 0.80) {

										speedometerDataInnerMap.put(productMap.get(product)+"Qtr", "" + 0.17);

									}

								
								} else if (dataCount == 2) {
									 if(avg.equalsIgnoreCase("null")){
										 speedometerDataInnerMap.put(productMap.get(product)+"Yr", "null");

										}
									 else if (Float.parseFloat(avg)/100 < 0.70) {

										 speedometerDataInnerMap.put(productMap.get(product)+"Yr", "" + 0.83);

									} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

										speedometerDataInnerMap.put(productMap.get(product)+"Yr", "" + 0.5);

									} else if (Float.parseFloat(avg)/100 > 0.80) {

										speedometerDataInnerMap.put(productMap.get(product)+"Yr", "" + 0.17);

									} 

								}

							}else{
								if (dataCount == 0) {
									
									speedometerDataInnerMap.put(productMap.get(product)+"Mon", "null");
								}
								else if (dataCount == 1) {
									speedometerDataInnerMap.put(productMap.get(product)+"Qtr", "null");
									
								}
								
								else if (dataCount == 2) {
									
									speedometerDataInnerMap.put(productMap.get(product)+"Yr", "null");
								}
								
							}
							
							log.info("EvDashboardContoller-getSpeedometerData-Avg-"+product+": "+avg);
							log.info("EvDashboardContoller-getSpeedometerData-"+product+" Count: "+dataCount);
				

						
							dataCount++;

						}
						
						
					  }
				  
				  
				  speedometerDataMap.put(productMap.get(product), speedometerDataInnerMap);
				  
				
			}
			
			
			
/*			
			Map<Integer,String> quaterMap=new LinkedHashMap<Integer, String>();
	    	List<String> monthWindow= new ArrayList<String>();

	    	
			quaterMap.put(1, "03");
			quaterMap.put(2, "06");
			quaterMap.put(3, "09");
			quaterMap.put(4, "12");
	    	cal.add(Calendar.MONTH, -3);
			int quarter = (cal.get(Calendar.MONTH) / 3) + 1;
		
			
			
			calMonth.add(Calendar.MONTH, -1);
			  int month = (calMonth.get(Calendar.MONTH) ) + 1;
			  String monthString=""+month;
			  if(month<10){
				  
				  monthString="0"+month;
				  
			  }
			  
			  
			  monthWindow.add( calMonth.get(Calendar.YEAR)+"-"+ monthString);
			  monthWindow.add( cal.get(Calendar.YEAR)+"-"+ quaterMap.get(quarter));
			  monthWindow.add( cal.get(Calendar.YEAR)+"-"+ quaterMap.get(quarter));
			  */
			
	/*		mnsData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, monthlyTimeline, "MNS"));
			mnsData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, quaterTimeline, "MNS"));
			mnsData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, yearlyTimeline, "MNS"));
			
			cloudData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, monthlyTimeline, "Private Cloud"));
			cloudData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, quaterTimeline, "Private CLoud"));
			cloudData.add(evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, yearlyTimeline, "Private Cloud"));*/
			
			

		/*	  List<DeliverySpeedometerMetadata> speedometerDataMNS=evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, monthWindow,"MNS");
			  List<DeliverySpeedometerMetadata> speedometerDataCloud=evDashboardDeliveryManagerImpl.getDeliverySpeedometerData(customerId, monthWindow,"Private Cloud");
*/
			  
			//  System.out.println("List MNS Data: "+speedometerDataMNS.size());
			 // System.out.println("List Cloud Data: "+speedometerDataCloud.size());
/*			  LinkedHashMap<String, LinkedHashMap<String, String>> speedometerDataMap=new LinkedHashMap<String, LinkedHashMap<String,String>>();
			  int mnsCount=0;
			  int cloudCount=0;

			  LinkedHashMap<String, String> speedometerMNSInnerMap= new  LinkedHashMap<String, String>();
			  LinkedHashMap<String, String> speedometerCloudInnerMap= new  LinkedHashMap<String, String>();
			  LinkedHashMap<String, String> speedometerSaaSInnerMap= new  LinkedHashMap<String, String>();
			  LinkedHashMap<String, String> speedometerUCInnerMap= new  LinkedHashMap<String, String>();
			  LinkedHashMap<String, String> speedometerManageSecurityInnerMap= new  LinkedHashMap<String, String>();


				

				  if (null!=mnsData && mnsData.size()>0) {
					for (String deliveryEVMetadata : mnsData) {

					String avg ="";
						if (!deliveryEVMetadata.equalsIgnoreCase("null")) {
							avg =deliveryEVMetadata;
							
							
							if (mnsCount == 0) {
								
								if(avg.equalsIgnoreCase("null")){
									
									speedometerMNSInnerMap.put("MNSMon", "null");

								}
								else if (Float.parseFloat(avg)/100 < 0.70) {

									speedometerMNSInnerMap.put("MNSMon", "" + 0.83);

								} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

									speedometerMNSInnerMap.put("MNSMon", "" + 0.5);

								} else if (Float.parseFloat(avg)/100 > 0.80) {

									speedometerMNSInnerMap.put("MNSMon", "" + 0.17);

								} 

							} else if (mnsCount == 1) {
								 if(avg.equalsIgnoreCase("null")){
									speedometerMNSInnerMap.put("MNSQtr", "null");

								}
								 else if (Float.parseFloat(avg)/100 < 0.70) {
									speedometerMNSInnerMap.put("MNSQtr", "" + 0.83);

								} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

									speedometerMNSInnerMap.put("MNSQtr", "" + 0.5);

								} else if (Float.parseFloat(avg)/100 > 0.80) {

									speedometerMNSInnerMap.put("MNSQtr", "" + 0.17);

								}

							
							} else if (mnsCount == 2) {
								 if(avg.equalsIgnoreCase("null")){
										speedometerMNSInnerMap.put("MNSYr", "null");

									}
								 else if (Float.parseFloat(avg)/100 < 0.70) {

									speedometerMNSInnerMap.put("MNSYr", "" + 0.83);

								} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

									speedometerMNSInnerMap.put("MNSYr", "" + 0.5);

								} else if (Float.parseFloat(avg)/100 > 0.80) {

									speedometerMNSInnerMap.put("MNSYr", "" + 0.17);

								} 

							}

						}else{
							if (mnsCount == 0) {
								
								speedometerMNSInnerMap.put("MNSMon", "null");
							}
							else if (mnsCount == 1) {
								speedometerMNSInnerMap.put("MNSQtr", "null");
								
							}
							
							else if (mnsCount == 2) {
								
								speedometerMNSInnerMap.put("MNSYr", "null");
							}
							
						}
						
						log.info("EvDashboardContoller-getSpeedometerData-Avg: "+avg);
						log.info("EvDashboardContoller-getSpeedometerData-MNS Count: "+mnsCount);
			

					
						mnsCount++;

					}
					
					
				  }
					
				  if (null!=cloudData && cloudData.size()>0) {

					for (String deliveryEVMetadata : cloudData) {

						String avg ="";
							if (!deliveryEVMetadata.equalsIgnoreCase("null")) {
								avg =deliveryEVMetadata;
								
								
								if (cloudCount == 0) {
									
									if(avg.equalsIgnoreCase("null")){
										speedometerCloudInnerMap.put("CloudMon", "null");

									}
									else if (Float.parseFloat(avg)/100 < 0.70) {
									
										speedometerCloudInnerMap.put("CloudMon", "" + 0.83);

									} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

										speedometerCloudInnerMap.put("CloudMon", "" + 0.5);

									} else if (Float.parseFloat(avg)/100 > 0.80) {

										speedometerCloudInnerMap.put("CloudMon", "" + 0.17);

									} 

								} else if (cloudCount == 1) {
									 if(avg.equalsIgnoreCase("null")){
										 speedometerCloudInnerMap.put("CloudQtr", "null");

									}
									 else if (Float.parseFloat(avg)/100 < 0.70) {

										 speedometerCloudInnerMap.put("CloudQtr", "" + 0.83);

									} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

										speedometerCloudInnerMap.put("CloudQtr", "" + 0.5);

									} else if (Float.parseFloat(avg)/100 > 0.80) {

										speedometerCloudInnerMap.put("CloudQtr", "" + 0.17);

									}

								
								} else if (cloudCount == 2) {
									 if(avg.equalsIgnoreCase("null")){
										 speedometerCloudInnerMap.put("CloudYr", "null");

										}
									 else if (Float.parseFloat(avg)/100 < 0.70) {

										 speedometerCloudInnerMap.put("CloudYr", "" + 0.83);

									} else if (Float.parseFloat(avg)/100 >= 0.70 && Float.parseFloat(avg)/100 <= 0.80) {

										speedometerCloudInnerMap.put("CloudYr", "" + 0.5);

									} else if (Float.parseFloat(avg)/100 > 0.80) {

										speedometerCloudInnerMap.put("CloudYr", "" + 0.17);

									} 

								}

							}else{
								if (cloudCount == 0) {
									
									speedometerCloudInnerMap.put("CloudMon", "null");
								}
								else if (cloudCount == 1) {
									speedometerCloudInnerMap.put("CloudQtr", "null");
									
								}
								
								else if (cloudCount == 2) {
									
									speedometerCloudInnerMap.put("CloudYr", "null");
								}
								
							}
							log.info("EvDashboardContoller-getSpeedometerData-Avg: "+avg);
							log.info("EvDashboardContoller-getSpeedometerData-Cloud Count: "+cloudCount);


					
						
							cloudCount++;

						}
				  }
*/			  
			/*  speedometerCloudInnerMap.put("CloudMon", "null");
			  speedometerCloudInnerMap.put("CloudQtr", "null");
			  speedometerCloudInnerMap.put("CloudYr", "null");*/
			  
/*			  speedometerSaaSInnerMap.put("SaaSMon", "null");
			  speedometerSaaSInnerMap.put("SaaSQtr", "null");
			  speedometerSaaSInnerMap.put("SaaSYr", "null");
			  
			  speedometerUCInnerMap.put("UCMon", "null");
			  speedometerUCInnerMap.put("UCQtr", "null");
			  speedometerUCInnerMap.put("UCYr", "null");
			  
			  
			  speedometerManageSecurityInnerMap.put("ManageSecurityMon", "null");
			  speedometerManageSecurityInnerMap.put("ManageSecurityQtr", "null");
			  speedometerManageSecurityInnerMap.put("ManageSecurityYr", "null");
*/			  
			  
			  
			  
			  

			  
			  
	
			  
			  
	    	
	   
				  return speedometerDataMap;
	  
}
	  
	   
	   public List<String> getMonthYearList(String timeline){
		   List<String> monthYear= new ArrayList<String>();
		   
			Map<Integer,String> quaterMap=new HashMap<Integer, String>();
			quaterMap.put(1, "03");
			quaterMap.put(2, "06");
			quaterMap.put(3, "09");
			quaterMap.put(4, "12");
			
			
		   if(timeline.equalsIgnoreCase("monthly")){
				log.info("Monthly");
				final Calendar cal2 = Calendar.getInstance();

				final Calendar cal = Calendar.getInstance();
				 cal2.add(Calendar.MONTH, -1);

				  String time=new SimpleDateFormat("yyyy-MM").format(cal2.getTime());
				  monthYear.add(time);
					log.info("Time--"+time);


			   
		   }else if(timeline.equalsIgnoreCase("quaterly")){
				log.info("Quaterly");

				final Calendar cal2 = Calendar.getInstance();

				final Calendar cal = Calendar.getInstance();
				 cal.add(Calendar.MONTH, -3);
				  int quarter = (cal.get(Calendar.MONTH) / 3) + 1;
					cal2.set(cal.get(Calendar.YEAR), Integer.parseInt(quaterMap.get(quarter)), 1);
					
					int j=12;
					for (j = 0; j < 9; j++) {
						
						  cal2.add(Calendar.MONTH, -1);
						  String time=new SimpleDateFormat("yyyy-MM").format(cal2.getTime());
						  monthYear.add(time);
						  System.out.println("TIme: "+time);


					}

			   
		   }
		   else if(timeline.equalsIgnoreCase("Yearly")){
				log.info("Yearly");


				final Calendar cal2 = Calendar.getInstance();

				final Calendar cal = Calendar.getInstance();
				 cal.add(Calendar.MONTH, -3);
				  int quarter = (cal.get(Calendar.MONTH) / 3) + 1;
				  
				  cal2.set(cal.get(Calendar.YEAR), Integer.parseInt(quaterMap.get(quarter)), 1);
					
					int j=3;
					for (j = 0; j < 12; j++) {
						
						  cal2.add(Calendar.MONTH, -1);
						  String time=new SimpleDateFormat("yyyy-MM").format(cal2.getTime());
						  monthYear.add(time);
						  System.out.println("TIme: "+time);


					}
				
			   
		   }
		   
		   return monthYear;
	   }
	   
	   
}
