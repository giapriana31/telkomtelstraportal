package com.tt.evdashboardview.mvc.dao;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value = "evDashboardUtilizationDaoImpl")
@Transactional
public class EVDashboardUtilizationDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;
    public static Properties prop = PropertyReader.getProperties();
    private static final String saasServiceCmdb=prop.getProperty("saasServiceCmdb");
	private static final String saasRootProduct=prop.getProperty("saasRootProduct");
    private final static Logger log = Logger.getLogger(EVDashboardUtilizationDaoImpl.class);
    private static final String[] services={"'MNS','Network'","'Cloud'","'SaaS'","'UC'","'Manage Security'"};
    private static final String[] consolidatedServices={"MNS","MNSTotal","Private Cloud","Private CloudTotal","SaaS","SaaSTotal","UC","UCTotal","Manage Security","Manage SecurityTotal"};
    private static final BigDecimal percentTotal= new BigDecimal("100");
    private static final BigDecimal redPercentage= new BigDecimal("0.83");
    private static final BigDecimal amberPercentage= new BigDecimal("0.5");
    private static final BigDecimal greenPercentage= new BigDecimal("0.17");
    private static final BigDecimal greenLimit= new BigDecimal("0.65");
    private static final BigDecimal amberLimit= new BigDecimal("0.75");
    
    private static final String[] serviceDivisionsMNS= {"'MNS'","'Network'"};
    private static final String[] serviceDivisionsConMNS= {"CPE","Network"};
    
    private static final String[] serviceDivisionsCloud= {"'Private Cloud'"};
    private static final String[] serviceDivisionsConCloud= {"Private Cloud"};
    
    private static final String[] quarterCounter= {"Quarter1","Quarter2","Quarter3"};
    
    private static final String nthPercentileForMNS=prop.getProperty("nthPercentileForMNS");
    private static final String nthPercentileForCloud=prop.getProperty("nthPercentileForCloud");
    

    public String calculateNthPercentileQueryStringForMNS(String customerId,String monthyear,String alias){
    	String queryString="(SELECT  case  when ((select count(1)*("+nthPercentileForMNS+"/100) from utilization_mns AS um"+alias+" where um"+alias+".customeruniqueid='"+customerId+"' and um"+alias+".monthyear='"+monthyear+"' and um"+alias+".percentile_utilization is not null)<>(select floor(count(1)*("+nthPercentileForMNS+"/100)) from utilization_mns AS um"+alias+" where um"+alias+".customeruniqueid='"+customerId+"' and um"+alias+".monthyear='"+monthyear+"' and um"+alias+".percentile_utilization is not null)) then (SELECT  SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(um"+alias+".percentile_utilization ORDER BY um"+alias+".percentile_utilization SEPARATOR ','),',',"+nthPercentileForMNS+"/100 * COUNT(*)),',',  -1) FROM  utilization_mns AS um"+alias+" where um"+alias+".customeruniqueid='"+customerId+"' and um"+alias+".monthyear='"+monthyear+"' and um"+alias+".percentile_utilization is not null) else ((SELECT  SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(um"+alias+".percentile_utilization ORDER BY um"+alias+".percentile_utilization SEPARATOR ','),',',"+nthPercentileForMNS+"/100 * COUNT(*)),',',  -1) FROM  utilization_mns AS um"+alias+" where um"+alias+".customeruniqueid='"+customerId+"' and um"+alias+".monthyear='"+monthyear+"' and um"+alias+".percentile_utilization is not null)+(SELECT  SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(um"+alias+".percentile_utilization ORDER BY um"+alias+".percentile_utilization SEPARATOR ','),',',"+nthPercentileForMNS+"/100 * COUNT(*) + 1),',',  -1) FROM  utilization_mns AS um"+alias+" where um"+alias+".customeruniqueid='"+customerId+"' and um"+alias+".monthyear='"+monthyear+"' and um"+alias+".percentile_utilization is not null))/2 end as um_perc_"+alias+")";
    	
    	return queryString;
    }
    
    public String calculateNthPercentileQueryStringForCloud(String customerId,String monthyear,String alias){
    	String queryString="(SELECT  case  when ((select count(1)*("+nthPercentileForCloud+"/100) from utilization_month_customer_pc umcp"+alias+" where umcp"+alias+".customeruniqueid='"+customerId+"' and umcp"+alias+".monthyear='"+monthyear+"' and umcp"+alias+".utilizations is not null)<>(select floor(count(1)*("+nthPercentileForCloud+"/100)) from utilization_month_customer_pc umcp"+alias+" where umcp"+alias+".customeruniqueid='"+customerId+"' and umcp"+alias+".monthyear='"+monthyear+"' and umcp"+alias+".utilizations is not null)) then (SELECT  SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(umcp"+alias+".utilizations ORDER BY umcp"+alias+".utilizations SEPARATOR ','),',',"+nthPercentileForCloud+"/100 * COUNT(*)),',',  -1) FROM  utilization_month_customer_pc AS umcp"+alias+" where umcp"+alias+".customeruniqueid='"+customerId+"' and umcp"+alias+".monthyear='"+monthyear+"' and umcp"+alias+".utilizations is not null) else ((SELECT  SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(umcp"+alias+".utilizations ORDER BY umcp"+alias+".utilizations SEPARATOR ','),',',"+nthPercentileForCloud+"/100 * COUNT(*)),',',  -1) FROM  utilization_month_customer_pc AS umcp"+alias+" where umcp"+alias+".customeruniqueid='"+customerId+"' and umcp"+alias+".monthyear='"+monthyear+"' and umcp"+alias+".utilizations is not null)+(SELECT  SUBSTRING_INDEX(SUBSTRING_INDEX(GROUP_CONCAT(umcp"+alias+".utilizations ORDER BY umcp"+alias+".utilizations SEPARATOR ','),',',"+nthPercentileForCloud+"/100 * COUNT(*)),',',  -1) FROM  utilization_month_customer_pc AS umcp"+alias+" where umcp"+alias+".customeruniqueid='"+customerId+"' and umcp"+alias+".monthyear='"+monthyear+"' and umcp"+alias+".utilizations is not null))/2 end as umcp_perc_"+alias+")";
    	
    	return queryString;
    }
    
    public BigDecimal getSpeedometerDeviation(BigDecimal count){
    	if (count.compareTo(greenLimit) < 0) {
			count = greenPercentage;
		} else if (count.compareTo(amberLimit) < 0
				|| count.compareTo(amberLimit) == 0) {
			count = amberPercentage;
		} else {
			count = redPercentage;
		}
    	return count;
    }
    
    public LinkedHashMap<String, ArrayList<String>> getUtilizationOverviewResults(LinkedHashMap<String, ArrayList<String>> timeWindows,String customerUniqueID){
    	
    	log.info("dao implementation for evd");
    	LinkedHashMap<String, ArrayList<String>> results= null;
    	
    	try {
    		results= new LinkedHashMap<String, ArrayList<String>>();
    		
    		ArrayList<String> spdList= new ArrayList<String>();
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		queryBuilder.append(calculateNthPercentileQueryStringForMNS(customerUniqueID, timeWindows.get("Month").get(0),"m")+",");
    		
    		
    		for (int i1 = 0; i1 < timeWindows.get("Quarter").size(); i1++) {
    			queryBuilder.append(calculateNthPercentileQueryStringForMNS(customerUniqueID, timeWindows.get("Quarter").get(i1),"q"+i1)).append(",");
			}
    		
    		for (int i1 = 0; i1 < timeWindows.get("Year").size(); i1++) {
    			queryBuilder.append(calculateNthPercentileQueryStringForMNS(customerUniqueID, timeWindows.get("Year").get(i1),"y"+i1)).append(",");

			}
    		
    		
    		queryBuilder.append(calculateNthPercentileQueryStringForCloud(customerUniqueID, timeWindows.get("Month").get(0),"m")+",");
    		
    		
    		for (int i1 = 0; i1 < timeWindows.get("Quarter").size(); i1++) {
    			queryBuilder.append(calculateNthPercentileQueryStringForCloud(customerUniqueID, timeWindows.get("Quarter").get(i1),"q"+i1)).append(",");
			}
    		   		
    		for (int i1 = 0; i1 < timeWindows.get("Year").size(); i1++) {
    			queryBuilder.append(calculateNthPercentileQueryStringForCloud(customerUniqueID, timeWindows.get("Year").get(i1),"y"+i1));
				if(i1<(timeWindows.get("Year").size()-1)){
					queryBuilder.append(",");
				}
			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for utilizationevmetadata speedometer: "+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
    		Object[] result=(Object[])query.uniqueResult();
			

			if (null != result) {
				
				for (int oi = 0; oi < result.length; oi+=16) {
					
					if (null != result[oi]) {
						String dataNumString= result[oi].toString();
						
						BigDecimal dataNum= new BigDecimal(dataNumString);
						
						BigDecimal count = (dataNum).divide(percentTotal, 2, RoundingMode.CEILING);
						log.info("count.."+count);
						
						spdList.add(getSpeedometerDeviation(count).toString());
					} else {
						spdList.add("null");
					}
					int denQuarter=0;
					BigDecimal sumQuarter= new BigDecimal("0");
					for (int i = oi+1; i <= oi+3; i++) {
						if(result[i]!=null){
							String dataNumString= result[i].toString();
							BigDecimal dataNum= new BigDecimal(dataNumString);
							BigDecimal count = (dataNum).divide(percentTotal, 2, RoundingMode.CEILING);
							sumQuarter=sumQuarter.add(count);
							denQuarter++;
						}
					}
					if(denQuarter!=0){
						BigDecimal value=sumQuarter.divide(new BigDecimal(denQuarter));
						spdList.add(getSpeedometerDeviation(value).toString());
					}else{
						spdList.add("null");
					}
					
					int denYear=0;
					BigDecimal sumYear= new BigDecimal("0");
					for (int i = oi+4; i <= oi+15; i++) {
						if(result[i]!=null){
							String dataNumString= result[i].toString();
							BigDecimal dataNum= new BigDecimal(dataNumString);
							BigDecimal count = (dataNum).divide(percentTotal, 2, RoundingMode.CEILING);
							sumYear=sumYear.add(count);
							denYear++;
						}
					}
					if(denYear!=0){
						BigDecimal value=sumYear.divide(new BigDecimal(denYear));
						spdList.add(getSpeedometerDeviation(value).toString());
					}else{
						spdList.add("null");
					}
				}
				log.info("populated spd list-"+spdList);

				results.put("speedometer", spdList);

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getMonthlySingleBarData(ArrayList<String> monthWindows,String customerUniqueID){
    	log.info("dao impl for performance monthsinglebardata");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int j = 0; j < monthWindows.size(); j++) {
    			
    			queryBuilder.append("(select count(distinct(siteid)) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and percentile_utilization>75 and monthyear='"+monthWindows.get(j)+"')");
    			
    			queryBuilder.append(",(select count(distinct(siteid)) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(j)+"')");
    			
    			queryBuilder.append(",(select count(distinct(vblock)) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(j)+"' and (vcpu>75 or storage>75 or memory>75))");
    			
    			queryBuilder.append(",(select count(distinct(vblock)) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(j)+"')");
    			
    			//-- temporary null values for other services except MNS and Private Cloud-- start 
    			
    			for (int i = 0; i < consolidatedServices.length-4; i++) {
					queryBuilder.append(",(select 0 from networkavailability a").append(i).append(j).append(" where a").append(i).append(j).append(".CUSTOMER_ID='C008' and a").append(i).append(j).append(".monthyear in ('2016-01'))");
				}
    			
    			//-- temporary null values for other services except MNS and Private Cloud-- end
    			
    			if(j<monthWindows.size()-1){
					queryBuilder.append(",");
				}   

			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for utilization monthSingleBarGraph"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])query.uniqueResult();
			
			if(null!=result){
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i]){
						barGraph1.put(consolidatedServices[i],result[i].toString());
					}
					else{
						barGraph1.put(consolidatedServices[i],"0");
					}
				}
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i+consolidatedServices.length]){
						barGraph2.put(consolidatedServices[i],(result[i+consolidatedServices.length]).toString());
					}
					else{
						barGraph2.put(consolidatedServices[i],"0");
					}
				}
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i+(2*(consolidatedServices.length))]){
						barGraph3.put(consolidatedServices[i],(result[i+(2*(consolidatedServices.length))]).toString());
					}
					else{
						barGraph3.put(consolidatedServices[i],"0");
					}
				}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				
			}
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getMonthlySingleBarDataForService(ArrayList<String> monthWindows,String customerUniqueID, String service){
    	
		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);

    	log.info("dao impl for monthsinglebardata for service - Performance");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder1= new StringBuilder();
    		StringBuilder queryBuilder2= new StringBuilder();
    		
    		switch (service) {
			case "MNS":
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
	    		
				Query query3 = sessionFactory.getCurrentSession().createSQLQuery("select sitename,max(percentile_utilization) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(2)+"' group by siteid order by max(percentile_utilization) desc limit 10");
				
	    		if(null!=query3.list() && !(query3.list().isEmpty())){
	    			ArrayList<Object> result3=(ArrayList<Object>) query3.list();
					
					
					for (int i = 0; i < result3.size(); i++) {
						Object[] row=(Object[])result3.get(i);
						if(null!=row[0] && null!=row[1]){
							barGraph3.put(row[0].toString(), row[1].toString());
						}
						
					}
					
					ArrayList<String> sitesList= new ArrayList<String>();
		    		sitesList.addAll(barGraph3.keySet());
		    		
		    		if(sitesList.size()>0){
		    			
		    			queryBuilder1.append("select ");
			    		
			    		for (int i = 0; i < sitesList.size(); i++) {
							queryBuilder1.append("(select max(percentile_utilization) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(0)+"' and sitename='"+sitesList.get(i)+"' group by siteid)");
							if(i<sitesList.size()-1){
								queryBuilder1.append(",");
							}
						}
			    		
			    		queryBuilder1.append(" from dual");
			    		
			    		log.info("query for monthSingleBarGraph Utilization MNS 1- "+queryBuilder1.toString());
			    		
			    		Query query1 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder1.toString());
						Object[] result1=(Object[])query1.uniqueResult();
						
						if(null!=result1){
							for (int i = 0; i < result1.length; i++) {
								if (null != result1[i]) {
									barGraph1.put(sitesList.get(i),result1[i].toString());
								} else {
									barGraph1.put(sitesList.get(i), "0");
								}
							}
						}
						
			    		
						queryBuilder2.append("select ");
			    		
			    		for (int i = 0; i < sitesList.size(); i++) {
							queryBuilder2.append("(select max(percentile_utilization) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(1)+"' and sitename='"+sitesList.get(i)+"' group by siteid)");
							if(i<sitesList.size()-1){
								queryBuilder2.append(",");
							}
						}
			    		
			    		queryBuilder2.append(" from dual");
			    		
			    		log.info("query for monthSingleBarGraph Utilization MNS 2- "+queryBuilder2.toString());
			    		
			    		Query query2 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder2.toString());
						Object[] result2=(Object[])query2.uniqueResult();
						
						if(null!=result2){
							for (int i = 0; i < result2.length; i++) {
								if (null != result2[i]) {
									barGraph2.put(sitesList.get(i),result2[i].toString());
								} else {
									barGraph2.put(sitesList.get(i), "0");
								}
							}
						}
		    			
		    		}
		    		
		    		
	    		}
	    		
	    		
	    		
	    		
	    		
	    		
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
					
					
	    		
				
				break;

			case "CLOUD":
				
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				query3 = sessionFactory.getCurrentSession().createSQLQuery("select vblock,vcpu,storage,memory from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(2)+"' and vblock in (select distinct(vblock) from utilization_month_customer_pc where utilizations >75 and customeruniqueid='"+customerUniqueID+"' and monthyear ='"+monthWindows.get(2)+"' group by vblock order by utilizations desc) limit 5");
				
				if(null!=query3.list() && !(query3.list().isEmpty())){
	    			ArrayList<Object> result3=(ArrayList<Object>) query3.list();
	    			
	    			ArrayList<String> vBlockList= new ArrayList<String>();
					
					
					for (int i = 0; i < result3.size(); i++) {
						Object[] row=(Object[])result3.get(i);
						if(null!=row[0] && null!=row[1] && null!=row[2] && null!=row[3]){
							barGraph3.put(row[0].toString()+" VCPU", row[1].toString());
							barGraph3.put(row[0].toString()+" Storage", row[2].toString());
							barGraph3.put(row[0].toString()+" Memory", row[3].toString());
							vBlockList.add(row[0].toString());
						}
						
					}
					if(vBlockList.size()>0){
					
						queryBuilder1.append("select ");
						
						for (int i = 0; i < vBlockList.size(); i++) {
							queryBuilder1.append("(select vcpu from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(0)+"' and vblock='"+vBlockList.get(i)+"')");
							queryBuilder1.append(",(select storage from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(0)+"' and vblock='"+vBlockList.get(i)+"')");
							queryBuilder1.append(",(select memory from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(0)+"' and vblock='"+vBlockList.get(i)+"')");
							
							if(i<vBlockList.size()-1){
								queryBuilder1.append(",");
							}
						}
						
						log.info("query for monthSingleBarGraph Utilization Cloud 1- "+queryBuilder1.toString());
			    		
			    		Query query1 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder1.toString());
			    		Object[] result1=(Object[])query1.uniqueResult();
						
						if(null!=result1){
							int ctr=0;
							for (int i = 0; i < result1.length; i+=3) {
								if (null != result1[i]) {
									barGraph1.put(vBlockList.get(ctr)+" VCPU",result1[i].toString());
								} else {
									barGraph1.put(vBlockList.get(ctr)+" VCPU", "0");
								}
								if (null != result1[i+1]) {
									barGraph1.put(vBlockList.get(ctr)+" Storage",result1[i+1].toString());
								} else {
									barGraph1.put(vBlockList.get(ctr)+" Storage", "0");
								}
								if (null != result1[i+2]) {
									barGraph1.put(vBlockList.get(ctr)+" Memory",result1[i+2].toString());
								} else {
									barGraph1.put(vBlockList.get(ctr)+" Memory", "0");
								}
								ctr++;
							}
						}
						
						queryBuilder2.append("select ");
						
						for (int i = 0; i < vBlockList.size(); i++) {
							queryBuilder2.append("(select vcpu from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(1)+"' and vblock='"+vBlockList.get(i)+"')");
							queryBuilder2.append(",(select storage from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(1)+"' and vblock='"+vBlockList.get(i)+"')");
							queryBuilder2.append(",(select memory from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear='"+monthWindows.get(1)+"' and vblock='"+vBlockList.get(i)+"')");
							
							if(i<vBlockList.size()-1){
								queryBuilder2.append(",");
							}
						}
						
						log.info("query for monthSingleBarGraph Utilization Cloud 2- "+queryBuilder2.toString());
			    		
			    		Query query2 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder2.toString());
			    		Object[] result2=(Object[])query2.uniqueResult();
						
						if(null!=result2){
							int ctr=0;
							for (int i = 0; i < result2.length; i+=3) {
								if (null != result2[i]) {
									barGraph2.put(vBlockList.get(ctr)+" VCPU",result2[i].toString());
								} else {
									barGraph2.put(vBlockList.get(ctr)+" VCPU", "0");
								}
								if (null != result2[i+1]) {
									barGraph2.put(vBlockList.get(ctr)+" Storage",result2[i+1].toString());
								} else {
									barGraph2.put(vBlockList.get(ctr)+" Storage", "0");
								}
								if (null != result2[i+2]) {
									barGraph2.put(vBlockList.get(ctr)+" Memory",result2[i+2].toString());
								} else {
									barGraph2.put(vBlockList.get(ctr)+" Memory", "0");
								}
								ctr++;
							}
						}
					}
					
	    		}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				

				break;
				
			default:
				break;
			}
    		
    		
			
			
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getQuarterlySingleBarDataForService(LinkedHashMap<String, ArrayList<String>> quarterWindows,String customerUniqueID, String service){

		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);
		
    	log.info("dao impl for quartersinglebardata for service");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		
    		StringBuilder queryBuilder1= new StringBuilder();
    		StringBuilder queryBuilder2= new StringBuilder();
    		
    		switch (service) {
			case "MNS":
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
	    		
				Query query3 = sessionFactory.getCurrentSession().createSQLQuery("select sitename,max(percentile_utilization) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and monthyear='"+quarterWindows.get("Quarter3").get(quarterWindows.get("Quarter3").size()-1)+"' group by siteid order by max(percentile_utilization) desc limit 10");
				
	    		if(null!=query3.list() && !(query3.list().isEmpty())){
	    			ArrayList<Object> result3=(ArrayList<Object>) query3.list();
					
					
					for (int i = 0; i < result3.size(); i++) {
						Object[] row=(Object[])result3.get(i);
						if(null!=row[0] && null!=row[1]){
							barGraph3.put(row[0].toString(), row[1].toString());
						}
						
					}
					
					ArrayList<String> sitesList= new ArrayList<String>();
		    		sitesList.addAll(barGraph3.keySet());
		    		
		    		if(sitesList.size()>0){
		    			
		    			queryBuilder1.append("select ");
			    		
			    		for (int i = 0; i < sitesList.size(); i++) {
							queryBuilder1.append("(select max(percentile_utilization) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and monthyear='"+quarterWindows.get("Quarter1").get(quarterWindows.get("Quarter1").size()-1)+"' and sitename='"+sitesList.get(i)+"' group by siteid)");
							if(i<sitesList.size()-1){
								queryBuilder1.append(",");
							}
						}
			    		
			    		queryBuilder1.append(" from dual");
			    		
			    		log.info("query for quarterSingleBarGraph Utilization MNS 1- "+queryBuilder1.toString());
			    		
			    		Query query1 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder1.toString());
						Object[] result1=(Object[])query1.uniqueResult();
						
						if(null!=result1){
							for (int i = 0; i < result1.length; i++) {
								if (null != result1[i]) {
									barGraph1.put(sitesList.get(i),result1[i].toString());
								} else {
									barGraph1.put(sitesList.get(i), "0");
								}
							}
						}
						
			    		
						queryBuilder2.append("select ");
			    		
			    		for (int i = 0; i < sitesList.size(); i++) {
							queryBuilder2.append("(select max(percentile_utilization) from utilization_mns where customeruniqueid='"+customerUniqueID+"' and monthyear='"+quarterWindows.get("Quarter2").get(quarterWindows.get("Quarter2").size()-1)+"' and sitename='"+sitesList.get(i)+"' group by siteid)");
							if(i<sitesList.size()-1){
								queryBuilder2.append(",");
							}
						}
			    		
			    		queryBuilder2.append(" from dual");
			    		
			    		log.info("query for quarterSingleBarGraph Utilization MNS 2- "+queryBuilder2.toString());
			    		
			    		Query query2 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder2.toString());
						Object[] result2=(Object[])query2.uniqueResult();
						
						if(null!=result2){
							for (int i = 0; i < result2.length; i++) {
								if (null != result2[i]) {
									barGraph2.put(sitesList.get(i),result2[i].toString());
								} else {
									barGraph2.put(sitesList.get(i), "0");
								}
							}
						}
			    		
		    		}
	    		}
	    		
	    		
	    		
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
					
					
	    		
				
				break;
				
			case "CLOUD" : 
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				StringBuilder queryBuilder3= new StringBuilder();
				
				queryBuilder3.append("select vblock,avg(vcpu),avg(storage),avg(memory) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
				for (int i = 0; i < quarterWindows.get("Quarter3").size(); i++) {
					queryBuilder3.append("'").append(quarterWindows.get("Quarter3").get(i)).append("'");
					if(i<quarterWindows.get("Quarter3").size()-1){
						queryBuilder3.append(",");
					}
				}
				
				queryBuilder3.append(") and vblock in (select distinct(vblock) from utilization_month_customer_pc where utilizations >75 and customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
				for (int i = 0; i < quarterWindows.get("Quarter3").size(); i++) {
					queryBuilder3.append("'").append(quarterWindows.get("Quarter3").get(i)).append("'");
					if(i<quarterWindows.get("Quarter3").size()-1){
						queryBuilder3.append(",");
					}
				}
				
				queryBuilder3.append(") order by utilizations desc) group by vblock limit 5");
				
				query3 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder3.toString());
				
				if(null!=query3.list() && !(query3.list().isEmpty())){
	    			ArrayList<Object> result3=(ArrayList<Object>) query3.list();
	    			
	    			ArrayList<String> vBlockList= new ArrayList<String>();
					
					
					for (int i = 0; i < result3.size(); i++) {
						Object[] row=(Object[])result3.get(i);
						if(null!=row[0] && null!=row[1] && null!=row[2] && null!=row[3]){
							barGraph3.put(row[0].toString()+" VCPU", row[1].toString());
							barGraph3.put(row[0].toString()+" Storage", row[2].toString());
							barGraph3.put(row[0].toString()+" Memory", row[3].toString());
							vBlockList.add(row[0].toString());
						}
						
					}
					if(vBlockList.size()>0){
					
						queryBuilder1.append("select ");
						
						for (int i = 0; i < vBlockList.size(); i++) {
							queryBuilder1.append("(select avg(vcpu) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
							for (int i1 = 0; i1 < quarterWindows.get("Quarter1").size(); i1++) {
								queryBuilder1
										.append("'")
										.append(quarterWindows.get("Quarter1")
												.get(i1)).append("'");
								if (i1 < quarterWindows.get("Quarter1").size() - 1) {
									queryBuilder1.append(",");
								}
							}
				
							queryBuilder1.append(") and vblock='"+vBlockList.get(i)+"')");
							queryBuilder1.append(",(select avg(storage) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
							for (int i1 = 0; i1 < quarterWindows
									.get("Quarter1").size(); i1++) {
								queryBuilder1
										.append("'")
										.append(quarterWindows.get("Quarter1")
												.get(i1)).append("'");
								if (i1 < quarterWindows.get("Quarter1").size() - 1) {
									queryBuilder1.append(",");
								}
							}
				
							queryBuilder1.append(") and vblock='"+vBlockList.get(i)+"')");
							queryBuilder1.append(",(select avg(memory) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
							for (int i1 = 0; i1 < quarterWindows
									.get("Quarter1").size(); i1++) {
								queryBuilder1
										.append("'")
										.append(quarterWindows.get("Quarter1")
												.get(i1)).append("'");
								if (i1 < quarterWindows.get("Quarter1").size() - 1) {
									queryBuilder1.append(",");
								}
							}
				
							queryBuilder1.append(") and vblock='"+vBlockList.get(i)+"')");
							
							if(i<vBlockList.size()-1){
								queryBuilder1.append(",");
							}
						}
						
						log.info("query for monthSingleBarGraph Utilization Cloud 1- "+queryBuilder1.toString());
			    		
			    		Query query1 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder1.toString());
						Object[] result1=(Object[])query1.uniqueResult();
						
						if(null!=result1){
							int ctr=0;
							for (int i = 0; i < result1.length; i+=3) {
								if (null != result1[i]) {
									barGraph1.put(vBlockList.get(ctr)+" VCPU",result1[i].toString());
								} else {
									barGraph1.put(vBlockList.get(ctr)+" VCPU", "0");
								}
								if (null != result1[i+1]) {
									barGraph1.put(vBlockList.get(ctr)+" Storage",result1[i+1].toString());
								} else {
									barGraph1.put(vBlockList.get(ctr)+" Storage", "0");
								}
								if (null != result1[i+2]) {
									barGraph1.put(vBlockList.get(ctr)+" Memory",result1[i+2].toString());
								} else {
									barGraph1.put(vBlockList.get(ctr)+" Memory", "0");
								}
								ctr++;
							}
						}
						
						queryBuilder2.append("select ");
						
						for (int i = 0; i < vBlockList.size(); i++) {
							queryBuilder2.append("(select avg(vcpu) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
							for (int i1 = 0; i1 < quarterWindows
									.get("Quarter2").size(); i1++) {
								queryBuilder2
										.append("'")
										.append(quarterWindows.get("Quarter2")
												.get(i1)).append("'");
								if (i1 < quarterWindows.get("Quarter2").size() - 1) {
									queryBuilder2.append(",");
								}
							}
				
							queryBuilder2.append(") and vblock='"+vBlockList.get(i)+"')");
							queryBuilder2.append(",(select avg(storage) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
							for (int i1 = 0; i1 < quarterWindows
									.get("Quarter2").size(); i1++) {
								queryBuilder2
										.append("'")
										.append(quarterWindows.get("Quarter2")
												.get(i1)).append("'");
								if (i1 < quarterWindows.get("Quarter2").size() - 1) {
									queryBuilder2.append(",");
								}
							}
				
							queryBuilder2.append(") and vblock='"+vBlockList.get(i)+"')");
							queryBuilder2.append(",(select avg(memory) from utilizationmetadata_pc_monthly where customeruniqueid='"+customerUniqueID+"' and monthyear in (");
				
							for (int i1 = 0; i1 < quarterWindows
									.get("Quarter2").size(); i1++) {
								queryBuilder2
										.append("'")
										.append(quarterWindows.get("Quarter2")
												.get(i1)).append("'");
								if (i1 < quarterWindows.get("Quarter2").size() - 1) {
									queryBuilder2.append(",");
								}
							}
				
							queryBuilder2.append(") and vblock='"+vBlockList.get(i)+"')");
							
							if(i<vBlockList.size()-1){
								queryBuilder2.append(",");
							}
						}
						
						log.info("query for monthSingleBarGraph Utilization Cloud 2- "+queryBuilder2.toString());
			    		
			    		Query query2 = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder2.toString());
						Object[] result2=(Object[])query2.uniqueResult();
						
						if(null!=result2){
							int ctr=0;
							for (int i = 0; i < result2.length; i+=3) {
								if (null != result2[i]) {
									barGraph2.put(vBlockList.get(ctr)+" VCPU",result2[i].toString());
								} else {
									barGraph2.put(vBlockList.get(ctr)+" VCPU", "0");
								}
								if (null != result2[i+1]) {
									barGraph2.put(vBlockList.get(ctr)+" Storage",result2[i+1].toString());
								} else {
									barGraph2.put(vBlockList.get(ctr)+" Storage", "0");
								}
								if (null != result2[i+2]) {
									barGraph2.put(vBlockList.get(ctr)+" Memory",result2[i+2].toString());
								} else {
									barGraph2.put(vBlockList.get(ctr)+" Memory", "0");
								}
								ctr++;
							}
						}
					}
					
	    		}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				

			default:
				break;
    		}
    		
    	
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    
    
    public ArrayList<String[]> getServiceSections(String service,String customerId){
    	
    	ArrayList<String[]> servicesList= new ArrayList<String[]>();
    	String[] serviceSections=null;
    	String[] serviceConSections=null;
    	switch (service) {
		case "MNS":
			serviceSections=serviceDivisionsMNS;
			serviceConSections=serviceDivisionsConMNS;
			break;
		case "CLOUD":
			serviceSections=serviceDivisionsCloud;
			serviceConSections=serviceDivisionsConCloud;
			break;
		case "SAAS":
			Query query= sessionFactory.getCurrentSession().createSQLQuery("select distinct(productname) from producttype where rootproductname='"+saasRootProduct+"' and productname in (select (case when productId like 'Mandoe%' then 'Mandoe' else productId end) from configurationitem where rootProductId='"+saasRootProduct+"' and customeruniqueid='"+customerId+"')");
			ArrayList<String> subscribedServices= (ArrayList<String>) query.list();
			String[] serviceDivisionsSaas = subscribedServices.toArray(new String[0]);
			serviceSections=serviceDivisionsSaas;
			serviceConSections=serviceDivisionsSaas;
			break;
			
		default:
			break;
		}
    	
    	servicesList.add(serviceSections);
    	servicesList.add(serviceConSections);
    	
    	return servicesList;
    	
    }
    
    

}
