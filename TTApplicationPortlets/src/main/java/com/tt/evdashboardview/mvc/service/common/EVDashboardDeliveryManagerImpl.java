package com.tt.evdashboardview.mvc.service.common;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.evdashboardview.mvc.dao.EVDashboardDeliveryDaoImpl;
import com.tt.model.DeliveryEVMetadata;
import com.tt.model.DeliverySpeedometerMetadata;

@Service(value="evDashboardDeliveryManagerImpl")
@Transactional
public class EVDashboardDeliveryManagerImpl {

	private EVDashboardDeliveryDaoImpl evDashboardDeliveryDaoImpl;

	@Autowired
	@Qualifier("evDashboardDeliveryDaoImpl")
	public void setEvDashboardDeliveryDaoImpl(EVDashboardDeliveryDaoImpl evDashboardDeliveryDaoImpl) {
		this.evDashboardDeliveryDaoImpl = evDashboardDeliveryDaoImpl;
	}
	

	public List<DeliveryEVMetadata> getBargraphData(String customerId,List<String> monthWindow,String productType){
		
		
		return evDashboardDeliveryDaoImpl.getBarGraphData(customerId, monthWindow,productType);
	}
	
	
	public String getDeliverySpeedometerData(String customerId,List<String> monthWindow,String productType){
		
		return evDashboardDeliveryDaoImpl.getDeliverySpeedometerData(customerId, monthWindow, productType);
		
	}
	
	public List<String> getProductList(){
		
		return evDashboardDeliveryDaoImpl.getProductList();
		
	}
	public ArrayList<LinkedHashMap<String, String>> getMonthlySingleBarDataForService(ArrayList<String> monthWindows,String customerUniqueId, String service){
		
		return evDashboardDeliveryDaoImpl.getMonthlySingleBarDataForService(monthWindows, customerUniqueId, service);
	}
	
	public ArrayList<LinkedHashMap<String, String>> getQuarterlySingleBarDataForService(LinkedHashMap<String, ArrayList<String>> quarterWindows,String customerUniqueID, String service){
		
		return evDashboardDeliveryDaoImpl.getQuarterlySingleBarDataForService(quarterWindows, customerUniqueID, service);
	}
	
}
