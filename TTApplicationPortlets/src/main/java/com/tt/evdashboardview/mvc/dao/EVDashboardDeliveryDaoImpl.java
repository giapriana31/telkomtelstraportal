package com.tt.evdashboardview.mvc.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import javax.swing.text.DefaultEditorKit.CutAction;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.DeliveryEVMetadata;
import com.tt.model.DeliverySpeedometerMetadata;
import com.tt.utils.PropertyReader;

@Repository(value = "evDashboardDeliveryDaoImpl")
@Transactional
public class EVDashboardDeliveryDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;
    public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(EVDashboardIncidentDaoImpl.class);
    private static final String saasServiceCmdb=prop.getProperty("saasServiceCmdb");
	private static final String saasRootProduct=prop.getProperty("saasRootProduct");
    private static final String[] services={"'MNS','Network'","'Private Cloud','Data Center-SIGMA','Data Center-Customer','Private Cloud Network'","'SaaS'","'UC'","'Manage Security'"};
    private static final String[] consolidatedServices={"MNS","Private Cloud","SaaS","UC","Manage Security"};
    private static final BigDecimal redPercentage= new BigDecimal("0.83");
    private static final BigDecimal amberPercentage= new BigDecimal("0.5");
    private static final BigDecimal greenPercentage= new BigDecimal("0.17");
    private static final BigDecimal redLimit= new BigDecimal("0.8");
    private static final BigDecimal amberLimit= new BigDecimal("0.9");
    
    private static final String[] serviceDivisionsMNS= {"'MNS'","'Network'"};
    private static final String[] serviceDivisionsConMNS= {"CPE","Network"};
    
    private static final String[] serviceDivisionsCloud={"'Private Cloud'"};
    private static final String[] serviceDivisionsConCloud={"Private Cloud"};
    
    private static final String[] deliveryStatus= {"orderreceivedsitescount","detaileddesignsitescount","procurementsitescount","deliveryactivationsitescount","monitoringsitescount","operationalsitescount"};
    private static final String[] deliveryStatusCon= {"Order Received","Detailed Design","Procurement","Delivery and Activation","Monitoring","Operational"};
    
    private static final String[] quarterCounter= {"Quarter1","Quarter2","Quarter3"};
   
    

    public List<DeliveryEVMetadata> getBarGraphData(String customerId,List<String> monthwindow,String productType){
    	List<DeliveryEVMetadata> graphDataList= new ArrayList<DeliveryEVMetadata>();
    	for (String string : monthwindow) {
			

    	System.out.println("Month Window: "+string);
    	Query query = sessionFactory.getCurrentSession().createQuery("from DeliveryEVMetadata d where d.customerUniqueId='"+customerId+"' and monthWindow='"+string+"' and productType='"+productType+"'");
    	
    	if(null !=query.list() && !query.list().isEmpty()){
    		System.out.println("Not empty List");
    		DeliveryEVMetadata tempData= (DeliveryEVMetadata) query.list().get(0);
    	

    		graphDataList.add(tempData);
    	}
    	
    	else{
    		System.out.println("Else");
    		DeliveryEVMetadata tempData= new DeliveryEVMetadata();
    		tempData.setTotalCount("null");
    		graphDataList.add(tempData);
    		
    	}
    		
    	}
    	
    	
    	return graphDataList;
    }
    
    
    
    public List<DeliveryEVMetadata> getDualBarGraphData(String customerId,String monthwindow,String productType){
    	List<DeliveryEVMetadata> graphDataList= new ArrayList<DeliveryEVMetadata>();
    	
			

    	System.out.println("Month Window: "+monthwindow);
    	Query query = sessionFactory.getCurrentSession().createQuery("from DeliveryEVMetadata d where d.customerUniqueId='"+customerId+"' and monthWindow='"+monthwindow+"' and productType='"+productType+"'");
    	
    	if(null !=query.list() && !query.list().isEmpty()){
    		System.out.println("Not empty List");
    		DeliveryEVMetadata tempData= (DeliveryEVMetadata) query.list().get(0);
    	

    		graphDataList.add(tempData);
    	}
    	
    	else{
    		System.out.println("Else");
    		DeliveryEVMetadata tempData= new DeliveryEVMetadata();
    		tempData.setTotalCount("null");
    		graphDataList.add(tempData);
    		
    	
    		
    	}
    	
    	
    	return graphDataList;
    }
    
    
    public ArrayList<LinkedHashMap<String,String>> getMonthlySingleBarDataForService(ArrayList<String> monthWindows,String customerUniqueID, String service){
    	
		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);
		
    	log.info("dao impl for monthsinglebardata for service- Delivery");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int j = 0; j < monthWindows.size(); j++) {
							
    			for (int i = 0; i < servicesList.get(0).length; i++) {
        			for (int k = 0; k < deliveryStatus.length; k++) {
						queryBuilder.append("(select sum(a").append(i).append(".").append(deliveryStatus[k]).append(")");
						queryBuilder.append(" from deliverymetadata a").append(i).append(" where a").append(i).append(".customeruniqueid='")
	            		.append(customerUniqueID).append("' and a").append(i).append(".monthwindow in('")
	            		.append(monthWindows.get(j)).append("'");
	        			
	            		queryBuilder.append(") and a").append(i).append(".producttype in (").append(servicesList.get(0)[i]).append("))");
						if(k<deliveryStatus.length-1){
							queryBuilder.append(",");
						}
					}
        			
            		
            		
            		if(i<(servicesList.get(0).length-1)){
            			queryBuilder.append(",");
            		}
    			}
    			
    			if(j<monthWindows.size()-1){
					queryBuilder.append(",");
				}   

			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for incevmetadata monthSingleBarGraph"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])query.uniqueResult();
			
			if(null!=result){
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				
				for (int i = 0; i < deliveryStatusCon.length; i++) {
					if(null!=result[i]){
						barGraph1.put(deliveryStatusCon[i],((BigDecimal)result[i]).toString());
					}
					else{
						barGraph1.put(deliveryStatusCon[i],"0");
					}
				}
				
				for (int i = 0; i < deliveryStatusCon.length; i++) {
					if(null!=result[i+deliveryStatusCon.length]){
						barGraph2.put(deliveryStatusCon[i],((BigDecimal)result[i+deliveryStatusCon.length]).toString());
					}
					else{
						barGraph2.put(deliveryStatusCon[i],"0");
					}
				}
				
				for (int i = 0; i < deliveryStatusCon.length; i++) {
					if(null!=result[i+(2*(deliveryStatusCon.length))]){
						barGraph3.put(deliveryStatusCon[i],((BigDecimal)result[i+(2*(deliveryStatusCon.length))]).toString());
					}
					else{
						barGraph3.put(deliveryStatusCon[i],"0");
					}
				}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				
			}
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getQuarterlySingleBarDataForService(LinkedHashMap<String, ArrayList<String>> quarterWindows,String customerUniqueID, String service){

		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);
		
    	log.info("dao impl for quartersinglebardata for service");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int j = 0; j < quarterCounter.length; j++) {
							
    			for (int i = 0; i < servicesList.get(0).length; i++) {
    				for (int l = 0; l < deliveryStatus.length; l++) {
    					queryBuilder.append("(select sum(a").append(i).append(".").append(deliveryStatus[l]).append(")");
    					queryBuilder.append(" from deliverymetadata a").append(i).append(" where a").append(i).append(".customeruniqueid='")
                		.append(customerUniqueID).append("' and a").append(i).append(".monthwindow in('");
            			
            			
    					queryBuilder.append(quarterWindows.get(quarterCounter[j]).get(2)).append("'");
    						
    					
            			
                		queryBuilder.append(") and a").append(i).append(".cmdbclass in (").append(servicesList.get(0)[i]).append("))");
						if(l<deliveryStatus.length-1){
							queryBuilder.append(",");
						}
					}
    				
            		
            		
            		if(i<(servicesList.get(0).length-1)){
            			queryBuilder.append(",");
            		}
    			}
    			
    			if(j<quarterCounter.length-1){
					queryBuilder.append(",");
				}   

			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for incevmetadata quarterSingleBarGraphService"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])query.uniqueResult();
			
			if(null!=result){
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				
				for (int i = 0; i < deliveryStatusCon.length; i++) {
					if(null!=result[i]){
						barGraph1.put(deliveryStatusCon[i],((BigDecimal)result[i]).toString());
					}
					else{
						barGraph1.put(deliveryStatusCon[i],"0");
					}
				}
				
				for (int i = 0; i < deliveryStatusCon.length; i++) {
					if(null!=result[i+deliveryStatusCon.length]){
						barGraph2.put(deliveryStatusCon[i],((BigDecimal)result[i+deliveryStatusCon.length]).toString());
					}
					else{
						barGraph2.put(deliveryStatusCon[i],"0");
					}
				}
				
				for (int i = 0; i < deliveryStatusCon.length; i++) {
					if(null!=result[i+(2*(deliveryStatusCon.length))]){
						barGraph3.put(deliveryStatusCon[i],((BigDecimal)result[i+(2*(deliveryStatusCon.length))]).toString());
					}
					else{
						barGraph3.put(deliveryStatusCon[i],"0");
					}
				}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				
			}
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    
    
    public ArrayList<String[]> getServiceSections(String service,String customerId){
    	
    	ArrayList<String[]> servicesList= new ArrayList<String[]>();
    	String[] serviceSections=null;
    	String[] serviceConSections=null;
    	switch (service) {
		case "MNS":
			serviceSections=serviceDivisionsMNS;
			serviceConSections=serviceDivisionsConMNS;
			break;
		case "CLOUD":
			serviceSections=serviceDivisionsCloud;
			serviceConSections=serviceDivisionsConCloud;
			break;
		case "SAAS":
			Query query= sessionFactory.getCurrentSession().createSQLQuery("select distinct(productname) from producttype where rootproductname='"+saasRootProduct+"' and productname in (select (case when productId like 'Mandoe%' then 'Mandoe' else productId end) from configurationitem where rootProductId='"+saasRootProduct+"' and customeruniqueid='"+customerId+"')");
			ArrayList<String> subscribedServices= (ArrayList<String>) query.list();
			String[] serviceDivisionsSaas = subscribedServices.toArray(new String[0]);
			String[] serviceDivisionsConSaas = subscribedServices.toArray(new String[0]);
			for (int i = 0; i < serviceDivisionsSaas.length; i++)
			{
				serviceDivisionsSaas[i]="'"+serviceDivisionsSaas[i]+"'";
			}
			serviceSections=serviceDivisionsSaas;
			serviceConSections=serviceDivisionsConSaas;
			break;
		default:
			break;
		}
    	
    	servicesList.add(serviceSections);
    	servicesList.add(serviceConSections);
    	
    	return servicesList;
    	
    }
    
    
    
    public String getDeliverySpeedometerData(String customerId,List<String> monthYear,String productType){
    	
    	List<DeliverySpeedometerMetadata> speedoMeterData= new ArrayList<DeliverySpeedometerMetadata>();
    	String percentage=null;
    	float percentageFloat=0.0f;
    	int counter=0;
    	float finalPercentage=0.0f;
    	percentage="null";
    	
    				for (String string : monthYear) {
    					Query query= sessionFactory.getCurrentSession().createQuery("from DeliverySpeedometerMetadata where customerid='"+customerId+"' and monthyear='"+string+"' and producttype='"+productType+"'");
						log.info("EVDashboardDeliveryDaoImpl-getDeliverySpeedometerData-Query String: "+query.getQueryString());

    					if(null!=query.list() && query.list().size()>0){
    						DeliverySpeedometerMetadata dv=(DeliverySpeedometerMetadata)query.list().get(0);
    						
    						percentageFloat=percentageFloat+Float.parseFloat(dv.getPercentage());
    						counter++;
    						 finalPercentage=percentageFloat/counter;
    						 percentage=""+finalPercentage;
    					}
    					else{
    						log.info("EVDashboardDeliveryDaoImpl-getDeliverySpeedometerData-Percentage: "+percentage);

    						return percentage;
    						
    						
    						
    					}
    					
    					
    				}
    	
    	 finalPercentage=percentageFloat/counter;
    	
    	percentage=""+finalPercentage;
		log.info("EVDashboardDeliveryDaoImpl-getDeliverySpeedometerData- "+"Customer= "+customerId+" Percentage: "+percentage+" Product: "+productType);

    	
    	return percentage;
    }

    
    
    public List<String> getProductList(){
    	
    	
    	Query query=sessionFactory.getCurrentSession().createSQLQuery("Select distinct(rootproductname) from producttype");
    	List<String> productList=query.list();
    return productList;	
    }

    
}
