package com.tt.evdashboardview.mvc.dao;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;





import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value = "evDashboardPerformanceDaoImpl")
@Transactional
public class EVDashboardPerformanceDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;
    public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(EVDashboardPerformanceDaoImpl.class);
    private static final String saasServiceCmdb=prop.getProperty("saasServiceCmdb");
	private static final String saasRootProduct=prop.getProperty("saasRootProduct");
    private static final String[] services={"'MNS','Network'","'Cloud'","'SaaS'","'UC'","'Manage Security'"};
    private static final String[] consolidatedServices={"MNS"," Private Cloud","SaaS","UC","Manage Security"};
    private static final BigDecimal percentTotal= new BigDecimal("100");
    private static final BigDecimal redPercentage= new BigDecimal("0.83");
    private static final BigDecimal amberPercentage= new BigDecimal("0.5");
    private static final BigDecimal greenPercentage= new BigDecimal("0.17");
    private static final BigDecimal redLimit= new BigDecimal("0.985");
    private static final BigDecimal amberLimit= new BigDecimal("0.989");
    private static final String neglectedValues=prop.getProperty("neglectedValues");
    
    private static final String[] serviceDivisionsMNS= {"'MNS'","'Network'"};
    private static final String[] serviceDivisionsConMNS= {"CPE","Network"};
    
    private static final String[] serviceDivisionsCloud= {"'Private Cloud'"};
    private static final String[] serviceDivisionsConCloud= {"Private Cloud"};
    
    private static final String[] quarterCounter= {"Quarter1","Quarter2","Quarter3"};
    

    public LinkedHashMap<String, ArrayList<String>> getPerformanceOverviewResults(LinkedHashMap<String, ArrayList<String>> timeWindows,String customerUniqueID){
    	
    	log.info("dao implementation for evd");
    	LinkedHashMap<String, ArrayList<String>> results= null;
    	
    	try {
    		results= new LinkedHashMap<String, ArrayList<String>>();
    		
    		ArrayList<String> spdList= new ArrayList<String>();
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		queryBuilder.append("(((select AVG_AVAIL from networkavailability where CUSTOMER_ID=:customerId and monthyear in ('")
    		.append(timeWindows.get("Month").get(0))
    		.append("'))+(select sla_average from serviceslaavailability where customerID=:customerId and monthyear in ('")
    		.append(timeWindows.get("Month").get(0))
    		.append("')))/2),");
    		
    		queryBuilder.append("(((select avg(AVG_AVAIL) from networkavailability where CUSTOMER_ID=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Quarter").size(); i1++) {
				queryBuilder.append(timeWindows.get("Quarter").get(i1)).append("'");
				if(i1<(timeWindows.get("Quarter").size()-1)){
					queryBuilder.append(",'");
				}
			}
    		
    		queryBuilder.append("))+(select avg(sla_average) from serviceslaavailability where customerID=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Quarter").size(); i1++) {
				queryBuilder.append(timeWindows.get("Quarter").get(i1)).append("'");
				if(i1<(timeWindows.get("Quarter").size()-1)){
					queryBuilder.append(",'");
				}
			}
    		
    		queryBuilder.append(")))/2),");
    		
    		queryBuilder.append("(((select avg(AVG_AVAIL) from networkavailability where CUSTOMER_ID=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Year").size(); i1++) {
				queryBuilder.append(timeWindows.get("Year").get(i1)).append("'");
				if(i1<(timeWindows.get("Year").size()-1)){
					queryBuilder.append(",'");
				}
			}
    		
    		queryBuilder.append("))+(select avg(sla_average) from serviceslaavailability where customerID=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Year").size(); i1++) {
				queryBuilder.append(timeWindows.get("Year").get(i1)).append("'");
				if(i1<(timeWindows.get("Year").size()-1)){
					queryBuilder.append(",'");
				}
			
    		}
    		queryBuilder.append(")))/2),");
    		
    		queryBuilder.append("(select avgerage_availability from clouddashboard where customerId=:customerId and monthyear in ('")
    		.append(timeWindows.get("Month").get(0)).append("')),");
    		
    		queryBuilder.append("(select avg(avgerage_availability) from clouddashboard where customerId=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Quarter").size(); i1++) {
				queryBuilder.append(timeWindows.get("Quarter").get(i1)).append("'");
				if(i1<(timeWindows.get("Quarter").size()-1)){
					queryBuilder.append(",'");
				}
			}
    		
    		queryBuilder.append(")),(select avg(avgerage_availability) from clouddashboard where customerId=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Year").size(); i1++) {
				queryBuilder.append(timeWindows.get("Year").get(i1)).append("'");
				if(i1<(timeWindows.get("Year").size()-1)){
					queryBuilder.append(",'");
				}
			}
    		
    		queryBuilder.append(")),(select averageavailability from saasdashboard where customerid=:customerId and monthyear in ('")
    		.append(timeWindows.get("Month").get(0)).append("')),");
    		
    		queryBuilder.append("(select avg(averageavailability) from saasdashboard where customerid=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Quarter").size(); i1++) {
				queryBuilder.append(timeWindows.get("Quarter").get(i1)).append("'");
				if(i1<(timeWindows.get("Quarter").size()-1)){
					queryBuilder.append(",'");
				}
			}
    		
    		queryBuilder.append(")),(select avg(averageavailability) from saasdashboard where customerid=:customerId and monthyear in ('");
    		
    		for (int i1 = 0; i1 < timeWindows.get("Year").size(); i1++) {
				queryBuilder.append(timeWindows.get("Year").get(i1)).append("'");
				if(i1<(timeWindows.get("Year").size()-1)){
					queryBuilder.append(",'");
				}
			}
    		
    		queryBuilder.append("))");
    		
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for prfmncevmetadata speedometer: "+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			query.setString("customerId", customerUniqueID);
    		Object[] result=(Object[])query.uniqueResult();
			

			if (null != result) {
					for (int i = 0; i < result.length; i++) {
						if (null != result[i]) {
							
							if(!(result[i].toString().equalsIgnoreCase(neglectedValues))){
															
								String dataNumString= result[i].toString();
								
								log.info("performance value- "+dataNumString);
	
								BigDecimal dataNum= new BigDecimal(dataNumString);
								
								BigDecimal count = (dataNum).divide(percentTotal, 2, RoundingMode.CEILING);
								log.info("count.."+count);
								if (count.compareTo(redLimit) < 0) {
									count = redPercentage;
								} else if (count.compareTo(amberLimit) < 0
										|| count.compareTo(amberLimit) == 0) {
									count = amberPercentage;
								} else {
									count = greenPercentage;
								}
								spdList.add(count.toString());
							}
							else{
								spdList.add("null");
							}
						} else {
							spdList.add("null");
						}
					}

				results.put("speedometer", spdList);

			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getMonthlySingleBarData(ArrayList<String> monthWindows,String customerUniqueID){
    	log.info("dao impl for performance monthsinglebardata");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int j = 0; j < monthWindows.size(); j++) {
    			
    			queryBuilder.append("(((select AVG_AVAIL from networkavailability where CUSTOMER_ID=:customerId and monthyear in ('")
        		.append(monthWindows.get(j))
        		.append("'))+(select sla_average from serviceslaavailability where customerID=:customerId and monthyear in ('")
        		.append(monthWindows.get(j))
        		.append("')))/2)");
    			
    			queryBuilder.append(",(select avgerage_availability from clouddashboard where customerId=:customerId and monthyear in ('")
    			.append(monthWindows.get(j)).append("'))");
    			
    			queryBuilder.append(",(select averageavailability from saasdashboard where customerid=:customerId and monthyear in ('")
    			.append(monthWindows.get(j)).append("'))");
    			
    			//-- temporary null values for other services except MNS and Private Cloud-- start 
    			
    			for (int i = 0; i < consolidatedServices.length-3; i++) {
					queryBuilder.append(",(select 0 from networkavailability a").append(i).append(j).append(" where a").append(i).append(j).append(".CUSTOMER_ID='C008' and a").append(i).append(j).append(".monthyear in ('2016-01'))");
				}
    			
    			//-- temporary null values for other services except MNS and Private Cloud-- end
    			
    			if(j<monthWindows.size()-1){
					queryBuilder.append(",");
				}   

			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for performanceevmetadata monthSingleBarGraph"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
    		query.setString("customerId", customerUniqueID);
			Object[] result=(Object[])query.uniqueResult();
			
			if(null!=result){
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i]){
						barGraph1.put(consolidatedServices[i],result[i].toString());
					}
					else{
						barGraph1.put(consolidatedServices[i],"0");
					}
				}
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i+consolidatedServices.length]){
						barGraph2.put(consolidatedServices[i],(result[i+consolidatedServices.length]).toString());
					}
					else{
						barGraph2.put(consolidatedServices[i],"0");
					}
				}
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i+(2*(consolidatedServices.length))]){
						barGraph3.put(consolidatedServices[i],(result[i+(2*(consolidatedServices.length))]).toString());
					}
					else{
						barGraph3.put(consolidatedServices[i],"0");
					}
				}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				
			}
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getMonthlySingleBarDataForService(ArrayList<String> monthWindows,String customerUniqueID, String service){
    	
		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);

    	log.info("dao impl for monthsinglebardata for service - Performance");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		
    		switch (service) {
			case "MNS":
				
	    		queryBuilder.append("select ");
	    		
	    		for (int j = 0; j < monthWindows.size(); j++) {
					
	    			queryBuilder.append("(select sla_average from serviceslaavailability where customerID=:customerId and monthyear in ('")
	        		.append(monthWindows.get(j))
	        		.append("')),(select AVG_AVAIL from networkavailability where CUSTOMER_ID=:customerId and monthyear in ('")
	        		.append(monthWindows.get(j))
	        		.append("'))");
	    			
	    			if(j<monthWindows.size()-1){
						queryBuilder.append(",");
					}   

				}
	    		
	    		queryBuilder.append(" from dual");
	    		
	    		log.info("query for incevmetadata monthSingleBarGraph Performance MNS"+queryBuilder.toString());
	    		
	    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
	    		query.setString("customerId", customerUniqueID);
				Object[] result=(Object[])query.uniqueResult();
				
				if(null!=result){
					
					results= new ArrayList<LinkedHashMap<String,String>>();
					barGraph1= new LinkedHashMap<String, String>();
					barGraph2= new LinkedHashMap<String, String>();
					barGraph3= new LinkedHashMap<String, String>();
					
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=result[i]){
							barGraph1.put(servicesList.get(1)[i],(result[i]).toString());
						}
						else{
							barGraph1.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=result[i+servicesList.get(1).length]){
							barGraph2.put(servicesList.get(1)[i],(result[i+servicesList.get(1).length]).toString());
						}
						else{
							barGraph2.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=result[i+(2*(servicesList.get(1).length))]){
							barGraph3.put(servicesList.get(1)[i],(result[i+(2*(servicesList.get(1).length))]).toString());
						}
						else{
							barGraph3.put(servicesList.get(1)[i],"0");
						}
					}
					
					results.add(barGraph1);
					results.add(barGraph2);
					results.add(barGraph3);
					
				}
				
				break;

			case "CLOUD":
				
				
	    		queryBuilder.append("select ");
	    		
	    		for (int j = 0; j < monthWindows.size(); j++) {
					
	    			queryBuilder.append("(select avgerage_availability from clouddashboard where customerId=:customerId and monthyear in ('")
	        		.append(monthWindows.get(j))
	        		.append("'))");
	    			
	    			if(j<monthWindows.size()-1){
						queryBuilder.append(",");
					}   

				}
	    		
	    		queryBuilder.append(" from dual");
	    		
	    		log.info("query for incevmetadata monthSingleBarGraph Performance CLOUD"+queryBuilder.toString());
	    		
	    		Query queryCloud = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
	    		queryCloud.setString("customerId", customerUniqueID);
				Object[] resultCloud=(Object[])queryCloud.uniqueResult();
				
				if(null!=resultCloud){
					
					results= new ArrayList<LinkedHashMap<String,String>>();
					barGraph1= new LinkedHashMap<String, String>();
					barGraph2= new LinkedHashMap<String, String>();
					barGraph3= new LinkedHashMap<String, String>();
					
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultCloud[i]){
							barGraph1.put(servicesList.get(1)[i],(resultCloud[i]).toString());
						}
						else{
							barGraph1.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultCloud[i+servicesList.get(1).length]){
							barGraph2.put(servicesList.get(1)[i],(resultCloud[i+servicesList.get(1).length]).toString());
						}
						else{
							barGraph2.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultCloud[i+(2*(servicesList.get(1).length))]){
							barGraph3.put(servicesList.get(1)[i],(resultCloud[i+(2*(servicesList.get(1).length))]).toString());
						}
						else{
							barGraph3.put(servicesList.get(1)[i],"0");
						}
					}
					
					results.add(barGraph1);
					results.add(barGraph2);
					results.add(barGraph3);
					
				}
				
				break;
				
			case "SAAS":
				
				queryBuilder.append("select ");
	    		
	    		for (int j = 0; j < monthWindows.size(); j++) {
					
	    			for (int a = 0; a < servicesList.get(0).length; a++) {
	    				queryBuilder.append("(select avg(availability) from saasavailability where customerId=:customerId and productname in ('"+servicesList.get(0)[a]+"') and monthyear in ('")
		        		.append(monthWindows.get(j))
		        		.append("'))");
	    				
	    				if(a<servicesList.get(0).length-1){
	    					queryBuilder.append(",");
	    				}
					}
	    			
	    			
	    			
	    			if(j<monthWindows.size()-1){
						queryBuilder.append(",");
					}   

				}
	    		
	    		queryBuilder.append(" from dual");
	    		
	    		log.info("query for performance monthSingleBarGraph Performance SAAS"+queryBuilder.toString());
	    		
	    		Query querySaas = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
	    		querySaas.setString("customerId", customerUniqueID);
				Object[] resultSaas=(Object[])querySaas.uniqueResult();
				
				if(null!=resultSaas){
					
					results= new ArrayList<LinkedHashMap<String,String>>();
					barGraph1= new LinkedHashMap<String, String>();
					barGraph2= new LinkedHashMap<String, String>();
					barGraph3= new LinkedHashMap<String, String>();
					
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultSaas[i]){
							barGraph1.put(servicesList.get(1)[i],(resultSaas[i]).toString());
						}
						else{
							barGraph1.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultSaas[i+servicesList.get(1).length]){
							barGraph2.put(servicesList.get(1)[i],(resultSaas[i+servicesList.get(1).length]).toString());
						}
						else{
							barGraph2.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultSaas[i+(2*(servicesList.get(1).length))]){
							barGraph3.put(servicesList.get(1)[i],(resultSaas[i+(2*(servicesList.get(1).length))]).toString());
						}
						else{
							barGraph3.put(servicesList.get(1)[i],"0");
						}
					}
					
					results.add(barGraph1);
					results.add(barGraph2);
					results.add(barGraph3);
					
				}
				
				break;
				
			default:
				break;
			}
    		
    		
			
			
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getQuarterlySingleBarDataForService(LinkedHashMap<String, ArrayList<String>> quarterWindows,String customerUniqueID, String service){

		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);
		
    	log.info("dao impl for quartersinglebardata for service");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		switch (service) {
			case "MNS":
				
				
	    		queryBuilder.append("select ");
	    		
	    		for (int j = 0; j < quarterCounter.length; j++) {
								
	    			queryBuilder.append("(select avg(sla_average) from serviceslaavailability where customerID=:customerId and monthyear in ('");
	    			
	    			for (int k = 0; k < quarterWindows.get(quarterCounter[j]).size(); k++) {
						queryBuilder.append(quarterWindows.get(quarterCounter[j]).get(k)).append("'");
						if(k<quarterWindows.get(quarterCounter[j]).size()-1){
							queryBuilder.append(",'");
						}
					}
	    			
	        		queryBuilder.append(")),(select avg(AVG_AVAIL) from networkavailability where CUSTOMER_ID=:customerId and monthyear in ('");
	        		
	        		for (int k = 0; k < quarterWindows.get(quarterCounter[j]).size(); k++) {
						queryBuilder.append(quarterWindows.get(quarterCounter[j]).get(k)).append("'");
						if(k<quarterWindows.get(quarterCounter[j]).size()-1){
							queryBuilder.append(",'");
						}
					}
	        		
	        		queryBuilder.append("))");
	        			
	        			
	        			
	            	
	    			
	    			if(j<quarterCounter.length-1){
						queryBuilder.append(",");
					}   

				}
	    		
	    		queryBuilder.append(" from dual");
	    		
	    		log.info("query for performanceevmetadata quarterSingleBarGraphService MNS"+queryBuilder.toString());
	    		
	    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
	    		query.setString("customerId", customerUniqueID);
				Object[] result=(Object[])query.uniqueResult();
				
				if(null!=result){
					
					results= new ArrayList<LinkedHashMap<String,String>>();
					barGraph1= new LinkedHashMap<String, String>();
					barGraph2= new LinkedHashMap<String, String>();
					barGraph3= new LinkedHashMap<String, String>();
					
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=result[i]){
							barGraph1.put(servicesList.get(1)[i],(result[i]).toString());
						}
						else{
							barGraph1.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=result[i+servicesList.get(1).length]){
							barGraph2.put(servicesList.get(1)[i],(result[i+servicesList.get(1).length]).toString());
						}
						else{
							barGraph2.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=result[i+(2*(servicesList.get(1).length))]){
							barGraph3.put(servicesList.get(1)[i],(result[i+(2*(servicesList.get(1).length))]).toString());
						}
						else{
							barGraph3.put(servicesList.get(1)[i],"0");
						}
					}
					
					results.add(barGraph1);
					results.add(barGraph2);
					results.add(barGraph3);
					
				}
				
				break;
				
			case "CLOUD" : 
				
				queryBuilder.append("select ");
	    		
	    		for (int j = 0; j < quarterCounter.length; j++) {
								
	    			queryBuilder.append("(select avg(avgerage_availability) from clouddashboard where customerId=:customerId and monthyear in ('");
	    			
	    			for (int k = 0; k < quarterWindows.get(quarterCounter[j]).size(); k++) {
						queryBuilder.append(quarterWindows.get(quarterCounter[j]).get(k)).append("'");
						if(k<quarterWindows.get(quarterCounter[j]).size()-1){
							queryBuilder.append(",'");
						}
					}
	        		
	        		queryBuilder.append("))");
	        			
	    			
	    			if(j<quarterCounter.length-1){
						queryBuilder.append(",");
					}   

				}
	    		
	    		queryBuilder.append(" from dual");
	    		
	    		log.info("query for performanceevmetadata quarterSingleBarGraphService CLOUD"+queryBuilder.toString());
	    		
	    		Query queryCloud = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
	    		queryCloud.setString("customerId", customerUniqueID);
				Object[] resultCloud=(Object[])queryCloud.uniqueResult();
				
				if(null!=resultCloud){
					
					results= new ArrayList<LinkedHashMap<String,String>>();
					barGraph1= new LinkedHashMap<String, String>();
					barGraph2= new LinkedHashMap<String, String>();
					barGraph3= new LinkedHashMap<String, String>();
					
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultCloud[i]){
							barGraph1.put(servicesList.get(1)[i],(resultCloud[i]).toString());
						}
						else{
							barGraph1.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultCloud[i+servicesList.get(1).length]){
							barGraph2.put(servicesList.get(1)[i],(resultCloud[i+servicesList.get(1).length]).toString());
						}
						else{
							barGraph2.put(servicesList.get(1)[i],"0");
						}
					}
					
					for (int i = 0; i < servicesList.get(1).length; i++) {
						if(null!=resultCloud[i+(2*(servicesList.get(1).length))]){
							barGraph3.put(servicesList.get(1)[i],(resultCloud[i+(2*(servicesList.get(1).length))]).toString());
						}
						else{
							barGraph3.put(servicesList.get(1)[i],"0");
						}
					}
					
					results.add(barGraph1);
					results.add(barGraph2);
					results.add(barGraph3);
					
				}
				
			case "SAAS":

				queryBuilder.append("select ");

				for (int j = 0; j < quarterCounter.length; j++) {
					
					for (int a = 0; a < servicesList.get(0).length; a++) {
						
						queryBuilder.append("(select avg(availability) from saasavailability where customerId=:customerId and productname in ('"+servicesList.get(0)[a]+"') and monthyear in ('");

						for (int k = 0; k < quarterWindows.get(
								quarterCounter[j]).size(); k++) {
							queryBuilder.append(
									quarterWindows.get(quarterCounter[j])
											.get(k)).append("'");
							if (k < quarterWindows.get(quarterCounter[j])
									.size() - 1) {
								queryBuilder.append(",'");
							}
						}
						
						queryBuilder.append("))");
						if(a<servicesList.get(0).length-1){
							queryBuilder.append(",");
						}
					}
					
					if (j < quarterCounter.length - 1) {
						queryBuilder.append(",");
					}

				}

				queryBuilder.append(" from dual");

				log.info("query for performanceevmetadata quarterSingleBarGraphService SAAS"
						+ queryBuilder.toString());

				Query querySaas = sessionFactory.getCurrentSession()
						.createSQLQuery(queryBuilder.toString());
				querySaas.setString("customerId", customerUniqueID);
				Object[] resultSaas = (Object[]) querySaas.uniqueResult();

				if (null != resultSaas) {

					results = new ArrayList<LinkedHashMap<String, String>>();
					barGraph1 = new LinkedHashMap<String, String>();
					barGraph2 = new LinkedHashMap<String, String>();
					barGraph3 = new LinkedHashMap<String, String>();

					for (int i = 0; i < servicesList.get(1).length; i++) {
						if (null != resultSaas[i]) {
							barGraph1.put(servicesList.get(1)[i],
									(resultSaas[i]).toString());
						} else {
							barGraph1.put(servicesList.get(1)[i], "0");
						}
					}

					for (int i = 0; i < servicesList.get(1).length; i++) {
						if (null != resultSaas[i + servicesList.get(1).length]) {
							barGraph2
									.put(servicesList.get(1)[i], (resultSaas[i
											+ servicesList.get(1).length])
											.toString());
						} else {
							barGraph2.put(servicesList.get(1)[i], "0");
						}
					}

					for (int i = 0; i < servicesList.get(1).length; i++) {
						if (null != resultSaas[i
								+ (2 * (servicesList.get(1).length))]) {
							barGraph3
									.put(servicesList.get(1)[i],
											(resultSaas[i
													+ (2 * (servicesList.get(1).length))])
													.toString());
						} else {
							barGraph3.put(servicesList.get(1)[i], "0");
						}
					}

					results.add(barGraph1);
					results.add(barGraph2);
					results.add(barGraph3);

				}

			default:
				break;
			}
    		
    		
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    
    
    public ArrayList<String[]> getServiceSections(String service,String customerId){
    	
    	ArrayList<String[]> servicesList= new ArrayList<String[]>();
    	String[] serviceSections=null;
    	String[] serviceConSections=null;
    	switch (service) {
		case "MNS":
			serviceSections=serviceDivisionsMNS;
			serviceConSections=serviceDivisionsConMNS;
			break;
		case "CLOUD":
			serviceSections=serviceDivisionsCloud;
			serviceConSections=serviceDivisionsConCloud;
			break;
		case "SAAS":
			Query query= sessionFactory.getCurrentSession().createSQLQuery("select distinct(productname) from producttype where rootproductname='"+saasRootProduct+"' and productname in (select (case when productId like 'Mandoe%' then 'Mandoe' else productId end) from configurationitem where rootProductId='"+saasRootProduct+"' and customeruniqueid='"+customerId+"')");
			ArrayList<String> subscribedServices= (ArrayList<String>) query.list();
			String[] serviceDivisionsSaas = subscribedServices.toArray(new String[0]);
			serviceSections=serviceDivisionsSaas;
			serviceConSections=serviceDivisionsSaas;
			break;
		default:
			break;
		}
    	
    	servicesList.add(serviceSections);
    	servicesList.add(serviceConSections);
    	
    	return servicesList;
    	
    }
    
    
    

}
