package com.tt.evdashboardview.mvc.dao;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value = "evDashboardIncidentDaoImpl")
@Transactional
public class EVDashboardIncidentDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;
    public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(EVDashboardIncidentDaoImpl.class);
    private static final String saasServiceCmdb=prop.getProperty("saasServiceCmdb");
	private static final String saasRootProduct=prop.getProperty("saasRootProduct");
    private static final String[] services={"'MNS'","'Private Cloud','Data Center-SIGMA','Data Center-Customer','Private Cloud Network'","'SaaS'","'UC'","'Manage Security'"};
    private static final String[] consolidatedServices={"MNS","Private Cloud","SaaS","UC","Manage Security"};
    private static final BigDecimal redPercentage= new BigDecimal("0.83");
    private static final BigDecimal amberPercentage= new BigDecimal("0.5");
    private static final BigDecimal greenPercentage= new BigDecimal("0.17");
    private static final BigDecimal redLimit= new BigDecimal("0.8");
    private static final BigDecimal amberLimit= new BigDecimal("0.9");
    
    private static final String[] serviceDivisionsMNS= {"'CPE'","'Network'","'Environmental'"};
    private static final String[] serviceDivisionsConMNS= {"CPE","Network","Environmental"};
    
    private static final String[] serviceDivisionsCloud= {"'Private Cloud'","'Data Center-SIGMA'","'Data Center-Customer'","'Private Cloud Network'"};
    private static final String[] serviceDivisionsConCloud= {"Private Cloud","Data Center-SIGMA","Data Center-Customer","Network"};
    
    private static final String[] quarterCounter= {"Quarter1","Quarter2","Quarter3"};
    

    public LinkedHashMap<String, ArrayList<String>> getIncidentOverviewResults(LinkedHashMap<String, ArrayList<String>> timeWindows,String customerUniqueID){
    	
    	log.info("dao implementation for evd");
    	LinkedHashMap<String, ArrayList<String>> results= null;
    	
    	try {
    		results= new LinkedHashMap<String, ArrayList<String>>();
    		
    		ArrayList<String> spdList= new ArrayList<String>();
    		
    		String saasProducts="";
    		
    		Query queryForSaas= sessionFactory.getCurrentSession().createSQLQuery("select distinct(productname) from producttype where rootproductname='"+saasRootProduct+"' and productname in (select distinct(case when productId like 'Mandoe%' then 'Mandoe' else productId end) from configurationitem where rootProductId='"+saasRootProduct+"' and customeruniqueid='"+customerUniqueID+"')");
			ArrayList<String> subscribedServices= (ArrayList<String>) queryForSaas.list();
			String[] serviceDivisionsSaas = subscribedServices.toArray(new String[0]);
			for (int i = 0; i < serviceDivisionsSaas.length; i++)
			{
				saasProducts+="'"+serviceDivisionsSaas[i]+"'";
				if(i<serviceDivisionsSaas.length-1){
					saasProducts+=",";
				}
			}
			
			services[2]=saasProducts;
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int i = 0; i < services.length; i++) {
    			
    			String categoryField="resolutionCategory";
    			if(consolidatedServices[i].equalsIgnoreCase("Private Cloud")){
    				categoryField="resolutionCategory";
    			}else if(consolidatedServices[i].equalsIgnoreCase("SaaS")){
    				categoryField="category";
    			}
    			
    			queryBuilder.append("(select sum(a").append(i).append(".completedincidentcount) from incidentevmetadata a").append(i).append(" where a").append(i).append(".customeruniqueid='")
        		.append(customerUniqueID).append("' and a").append(i).append(".monthwindow in('")
        		.append(timeWindows.get("Month").get(0))
        		.append("') and a").append(i).append(".").append(categoryField).append(" in (").append(services[i]).append(")),(select sum(b").append(i).append(".totalincidentcount) from incidentevmetadata b").append(i).append(" where b").append(i).append(".customeruniqueid='")
        		.append(customerUniqueID).append("' and b").append(i).append(".monthwindow in('")
    			.append(timeWindows.get("Month").get(0))
    			.append("') and b").append(i).append(".").append(categoryField).append(" in (").append(services[i]).append(")),(select sum(c").append(i).append(".completedincidentcount) from incidentevmetadata c").append(i).append(" where c").append(i).append(".customeruniqueid='")
        		.append(customerUniqueID).append("' and c").append(i).append(".monthwindow in('");
        		
        		for (int i1 = 0; i1 < timeWindows.get("Quarter").size(); i1++) {
    				queryBuilder.append(timeWindows.get("Quarter").get(i1)).append("'");
    				if(i1<(timeWindows.get("Quarter").size()-1)){
    					queryBuilder.append(",'");
    				}
    			}
        		
        		queryBuilder.append(") and c").append(i).append(".").append(categoryField).append(" in (").append(services[i]).append(")),(select sum(d").append(i).append(".totalincidentcount) from incidentevmetadata d").append(i).append(" where d").append(i).append(".customeruniqueid='")
        		.append(customerUniqueID).append("' and d").append(i).append(".monthwindow in('");
        		
        		
    			queryBuilder.append(timeWindows.get("Quarter").get(0)).append("'");
    				
        		
        		queryBuilder.append(") and d").append(i).append(".").append(categoryField).append(" in (").append(services[i]).append(")),(select sum(e").append(i).append(".completedincidentcount) from incidentevmetadata e").append(i).append(" where e").append(i).append(".customeruniqueid='")
        		.append(customerUniqueID).append("' and e").append(i).append(".monthwindow in('");
        		
        		for (int i1 = 0; i1 < timeWindows.get("Year").size(); i1++) {
    				queryBuilder.append(timeWindows.get("Year").get(i1)).append("'");
    				if(i1<(timeWindows.get("Year").size()-1)){
    					queryBuilder.append(",'");
    				}
    			}
        		
        		queryBuilder.append(") and e").append(i).append(".").append(categoryField).append(" in (").append(services[i]).append(")),(select sum(f").append(i).append(".totalincidentcount) from incidentevmetadata f").append(i).append(" where f").append(i).append(".customeruniqueid='")
        		.append(customerUniqueID).append("' and f").append(i).append(".monthwindow in('");
        		
        		
    			queryBuilder.append(timeWindows.get("Year").get(0)).append("'");
    				
        		
        		queryBuilder.append(") and f").append(i).append(".").append(categoryField).append(" in (").append(services[i]).append("))");
        	
        		
        		if(i<(services.length-1)){
        			queryBuilder.append(",");
        		}
			}
    		
    		queryBuilder.append(" from dual");
    		
    		
    		
//    				+customerUniqueID+ "' and monthwindow in ('"
//    				+timeWindows.get("Month").get(0)+ "') and category in ('MNS','Network')),(select sum(totalincidentcount) from incidentevmetadata where customeruniqueid='123' and monthwindow in ('"
//    						+ "') and category in ('MNS','Network')) from dual");
    		
    		log.info("query for incevmetadata"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])query.uniqueResult();
			

			if(null!=result){
				for (int k = 0; k < result.length-1; k+=2) {
					if(null!=result[k+1]){
						if(!(((BigDecimal)result[k+1]).compareTo(BigDecimal.ZERO)==0 && ((BigDecimal)result[k]).compareTo(BigDecimal.ZERO)==0)){
							if(null!=result[k]){
								BigDecimal completed=(BigDecimal)result[k];
								BigDecimal open=(BigDecimal)result[k+1];
								BigDecimal sum=completed.add(open);
								BigDecimal count=(completed).divide(sum, 2, RoundingMode.CEILING);
								if(count.compareTo(redLimit)<0){
									count=redPercentage;
								}
								else if(count.compareTo(amberLimit)<0 || count.compareTo(amberLimit)==0){
									count=amberPercentage;
								}
								else{
									count=greenPercentage;
								}
								spdList.add(count.toString());
							}
							else{
								spdList.add("null");
							}
						}
						else{
							spdList.add("null");
						}
					}
					else{
						spdList.add("null");
					}
					
				}
			}
			
			
			
			
			results.put("speedometer", spdList);
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getMonthlySingleBarData(ArrayList<String> monthWindows,String customerUniqueID){
    	log.info("dao impl for monthsinglebardata");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		String saasProducts="";
    		
    		Query queryForSaas= sessionFactory.getCurrentSession().createSQLQuery("select distinct(productname) from producttype where rootproductname='"+saasRootProduct+"' and productname in (select distinct(case when productId like 'Mandoe%' then 'Mandoe' else productId end) from configurationitem where rootProductId='"+saasRootProduct+"' and customeruniqueid='"+customerUniqueID+"')");
			ArrayList<String> subscribedServices= (ArrayList<String>) queryForSaas.list();
			String[] serviceDivisionsSaas = subscribedServices.toArray(new String[0]);
			for (int i = 0; i < serviceDivisionsSaas.length; i++)
			{
				saasProducts+="'"+serviceDivisionsSaas[i]+"'";
				if(i<serviceDivisionsSaas.length-1){
					saasProducts+=",";
				}
			}
			
			services[2]=saasProducts;
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int j = 0; j < monthWindows.size(); j++) {
							
    			for (int i = 0; i < services.length; i++) {
    				String categoryField="resolutionCategory";
        			if(consolidatedServices[i].equalsIgnoreCase("Cloud")){
        				categoryField="resolutionCategory";
        			}else if(consolidatedServices[i].equalsIgnoreCase("SaaS")){
        				categoryField="category";
        			}
    				
    				queryBuilder.append("(select sum(a").append(i).append(".completedincidentcount) from incidentevmetadata a").append(i).append(" where a").append(i).append(".customeruniqueid='")
            		.append(customerUniqueID).append("' and a").append(i).append(".monthwindow in('")
            		.append(monthWindows.get(j)).append("'");
        			
            		queryBuilder.append(") and a").append(i).append(".").append(categoryField).append(" in (").append(services[i]).append("))");
            		
            		
            		if(i<(services.length-1)){
            			queryBuilder.append(",");
            		}
    			}
    			
    			if(j<monthWindows.size()-1){
					queryBuilder.append(",");
				}   

			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for incevmetadata monthSingleBarGraph"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])query.uniqueResult();
			
			if(null!=result){
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i]){
						barGraph1.put(consolidatedServices[i],((BigDecimal)result[i]).toString());
					}
					else{
						barGraph1.put(consolidatedServices[i],"0");
					}
				}
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i+consolidatedServices.length]){
						barGraph2.put(consolidatedServices[i],((BigDecimal)result[i+consolidatedServices.length]).toString());
					}
					else{
						barGraph2.put(consolidatedServices[i],"0");
					}
				}
				
				for (int i = 0; i < consolidatedServices.length; i++) {
					if(null!=result[i+(2*(consolidatedServices.length))]){
						barGraph3.put(consolidatedServices[i],((BigDecimal)result[i+(2*(consolidatedServices.length))]).toString());
					}
					else{
						barGraph3.put(consolidatedServices[i],"0");
					}
				}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				
			}
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getMonthlySingleBarDataForService(ArrayList<String> monthWindows,String customerUniqueID, String service){
    	
		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);
		
    	log.info("dao impl for monthsinglebardata for service");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int j = 0; j < monthWindows.size(); j++) {
							
    			for (int i = 0; i < servicesList.get(0).length; i++) {
        			queryBuilder.append("(select sum(a").append(i).append(".completedincidentcount) from incidentevmetadata a").append(i).append(" where a").append(i).append(".customeruniqueid='")
            		.append(customerUniqueID).append("' and a").append(i).append(".monthwindow in('")
            		.append(monthWindows.get(j)).append("'");
        			
            		queryBuilder.append(") and a").append(i);
            		
            		if(service.equalsIgnoreCase("MNS")){
            			queryBuilder.append(".resolutionCategory in ('").append(service).append("') and a").append(i).append(".resolutionSubCategory1 in (").append(servicesList.get(0)[i]).append("))");
            		}
            		else if(service.equalsIgnoreCase("SAAS")){
            			queryBuilder.append(".category in (").append(servicesList.get(0)[i]).append("))");
            		}
            		else{
            			queryBuilder.append(".resolutionCategory in (").append(servicesList.get(0)[i]).append("))");
            		}
            		
            		
            		
            		if(i<(servicesList.get(0).length-1)){
            			queryBuilder.append(",");
            		}
    			}
    			
    			if(j<monthWindows.size()-1){
					queryBuilder.append(",");
				}   

			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for incevmetadata monthSingleBarGraph"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])query.uniqueResult();
			
			if(null!=result){
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				
				for (int i = 0; i < servicesList.get(1).length; i++) {
					if(null!=result[i]){
						barGraph1.put(servicesList.get(1)[i],((BigDecimal)result[i]).toString());
					}
					else{
						barGraph1.put(servicesList.get(1)[i],"0");
					}
				}
				
				for (int i = 0; i < servicesList.get(1).length; i++) {
					if(null!=result[i+servicesList.get(1).length]){
						barGraph2.put(servicesList.get(1)[i],((BigDecimal)result[i+servicesList.get(1).length]).toString());
					}
					else{
						barGraph2.put(servicesList.get(1)[i],"0");
					}
				}
				
				for (int i = 0; i < servicesList.get(1).length; i++) {
					if(null!=result[i+(2*(servicesList.get(1).length))]){
						barGraph3.put(servicesList.get(1)[i],((BigDecimal)result[i+(2*(servicesList.get(1).length))]).toString());
					}
					else{
						barGraph3.put(servicesList.get(1)[i],"0");
					}
				}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				
			}
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    public ArrayList<LinkedHashMap<String,String>> getQuarterlySingleBarDataForService(LinkedHashMap<String, ArrayList<String>> quarterWindows,String customerUniqueID, String service){

		ArrayList<String[]> servicesList= getServiceSections(service,customerUniqueID);
		
    	log.info("dao impl for quartersinglebardata for service");
    	ArrayList<LinkedHashMap<String, String>> results=null;
    	LinkedHashMap<String, String> barGraph1=null;
    	LinkedHashMap<String, String> barGraph2=null;
    	LinkedHashMap<String, String> barGraph3=null;
    	
    	
    	try {
    		
    		StringBuilder queryBuilder= new StringBuilder();
    		queryBuilder.append("select ");
    		
    		for (int j = 0; j < quarterCounter.length; j++) {
							
    			for (int i = 0; i < servicesList.get(0).length; i++) {
        			queryBuilder.append("(select sum(a").append(i).append(".completedincidentcount) from incidentevmetadata a").append(i).append(" where a").append(i).append(".customeruniqueid='")
            		.append(customerUniqueID).append("' and a").append(i).append(".monthwindow in('");
        			
        			for (int k = 0; k < quarterWindows.get(quarterCounter[j]).size(); k++) {
						queryBuilder.append(quarterWindows.get(quarterCounter[j]).get(k)).append("'");
						if(k<quarterWindows.get(quarterCounter[j]).size()-1){
							queryBuilder.append(",'");
						}
					}
        			
            		queryBuilder.append(") and a").append(i);
            		
            		if(service.equalsIgnoreCase("MNS")){
            			queryBuilder.append(".resolutionCategory in ('").append(service).append("') and a").append(i).append(".resolutionSubCategory1 in (").append(servicesList.get(0)[i]).append("))");
            		}
            		else if(service.equalsIgnoreCase("SAAS")){
            			queryBuilder.append(".category in (").append(servicesList.get(0)[i]).append("))");
            		}
            		else{
            			queryBuilder.append(".resolutionCategory in (").append(servicesList.get(0)[i]).append("))");
            		}
            		
            		
            		
            		if(i<(servicesList.get(0).length-1)){
            			queryBuilder.append(",");
            		}
    			}
    			
    			if(j<quarterCounter.length-1){
					queryBuilder.append(",");
				}   

			}
    		
    		queryBuilder.append(" from dual");
    		
    		log.info("query for incevmetadata quarterSingleBarGraphService"+queryBuilder.toString());
    		
    		Query query = sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])query.uniqueResult();
			
			if(null!=result){
				
				results= new ArrayList<LinkedHashMap<String,String>>();
				barGraph1= new LinkedHashMap<String, String>();
				barGraph2= new LinkedHashMap<String, String>();
				barGraph3= new LinkedHashMap<String, String>();
				
				
				for (int i = 0; i < servicesList.get(1).length; i++) {
					if(null!=result[i]){
						barGraph1.put(servicesList.get(1)[i],((BigDecimal)result[i]).toString());
					}
					else{
						barGraph1.put(servicesList.get(1)[i],"0");
					}
				}
				
				for (int i = 0; i < servicesList.get(1).length; i++) {
					if(null!=result[i+servicesList.get(1).length]){
						barGraph2.put(servicesList.get(1)[i],((BigDecimal)result[i+servicesList.get(1).length]).toString());
					}
					else{
						barGraph2.put(servicesList.get(1)[i],"0");
					}
				}
				
				for (int i = 0; i < servicesList.get(1).length; i++) {
					if(null!=result[i+(2*(servicesList.get(1).length))]){
						barGraph3.put(servicesList.get(1)[i],((BigDecimal)result[i+(2*(servicesList.get(1).length))]).toString());
					}
					else{
						barGraph3.put(servicesList.get(1)[i],"0");
					}
				}
				
				results.add(barGraph1);
				results.add(barGraph2);
				results.add(barGraph3);
				
			}
    		
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	
    	return results;
    }
    
    
    
    public ArrayList<String[]> getServiceSections(String service,String customerId){
    	
    	ArrayList<String[]> servicesList= new ArrayList<String[]>();
    	String[] serviceSections=null;
    	String[] serviceConSections=null;
    	switch (service) {
		case "MNS":
			serviceSections=serviceDivisionsMNS;
			serviceConSections=serviceDivisionsConMNS;
			break;
		case "CLOUD":
			serviceSections=serviceDivisionsCloud;
			serviceConSections=serviceDivisionsConCloud;
			break;
		case "SAAS":
			Query query= sessionFactory.getCurrentSession().createSQLQuery("select distinct(productname) from producttype where rootproductname='"+saasRootProduct+"' and productname in (select distinct(case when productId like 'Mandoe%' then 'Mandoe' else productId end) from configurationitem where rootProductId='"+saasRootProduct+"' and customeruniqueid='"+customerId+"')");
			ArrayList<String> subscribedServices= (ArrayList<String>) query.list();
			String[] serviceDivisionsSaas = subscribedServices.toArray(new String[0]);
			String[] serviceDivisionsConSaas = subscribedServices.toArray(new String[0]);
			for (int i = 0; i < serviceDivisionsSaas.length; i++)
			{
				serviceDivisionsSaas[i]="'"+serviceDivisionsSaas[i]+"'";
			}
			serviceSections=serviceDivisionsSaas;
			serviceConSections=serviceDivisionsConSaas;
			break;
		default:
			break;
		}
    	
    	servicesList.add(serviceSections);
    	servicesList.add(serviceConSections);
    	
    	return servicesList;
    	
    }

}
