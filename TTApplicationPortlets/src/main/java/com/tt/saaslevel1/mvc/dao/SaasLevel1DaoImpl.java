package com.tt.saaslevel1.mvc.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;


@Repository(value = "saasLevel1DaoImpl")
@Transactional
public class SaasLevel1DaoImpl {

	
	
	@Autowired
	private SessionFactory sessionFactory;
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(SaasLevel1DaoImpl.class);
	private static final String saasServiceCmdb=prop.getProperty("saasServiceCmdb");
	private static final String saasRootProduct=prop.getProperty("saasRootProduct");
	
	private static final String[] numberOfFields={"statusColor","availability","color"};
	private static final String greyOut="greyOut";
	
	public ArrayList<ArrayList<String>> getServiceSpecs(String customerId){
		ArrayList<ArrayList<String>> serviceSpecs= new ArrayList<ArrayList<String>>();
		
		
		Query query= sessionFactory.getCurrentSession().createSQLQuery("SELECT distinct(case when productId Like 'Mandoe%' then 'Mandoe' else productId end) FROM configurationitem WHERE rootProductId = '"+saasRootProduct+"' and customeruniqueid='"+customerId+"'");
		List subscribedServices= query.list();
		
		if(null!=subscribedServices && !(subscribedServices.isEmpty())){
			
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			Date date = cal.getTime();
			SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM");
			String monthYear= simpleDateFormat.format(date);
			StringBuilder queryBuilder=new StringBuilder();
			queryBuilder.append("select ");
			for (int i = 0; i < subscribedServices.size(); i++) {
				queryBuilder.append("(select case when(select count(distinct(i1"+i+".incidentid)) from incident i1"+i+" where i1"+i+".incidentstatus not in ('Cancelled','Completed', 'Closed') and  i1"+i+".priority in ('1','2') and i1"+i+".customerimpacted='1' and i1"+i+".customeruniqueid='"+customerId+"' and i1"+i+".rootProductId='"+subscribedServices.get(i)+"')>=1 then 'red' when ((select count(distinct(i2"+i+".incidentid)) from incident i2"+i+" where i2"+i+".incidentstatus not in ('Cancelled','Completed', 'Closed') and i2"+i+".priority in ('3','4') and i2"+i+".customerimpacted='1' and i2"+i+".customeruniqueid='"+customerId+"' and i2"+i+".rootProductId='"+subscribedServices.get(i)+"')>=1 or (select count(distinct(i3"+i+".incidentid)) from incident i3"+i+" where i3"+i+".incidentstatus not in ('Cancelled','Completed', 'Closed') and i3"+i+".priority in ('1','2') and i3"+i+".customerimpacted='0' and i3"+i+".customeruniqueid='"+customerId+"' and i3"+i+".rootProductId='"+subscribedServices.get(i)+"')>=1) then 'amber' when ((select count(distinct(i4"+i+".incidentid)) from incident i4"+i+" where i4"+i+".incidentstatus not in ('Cancelled','Completed', 'Closed') and i4"+i+".priority in ('1','2') and i4"+i+".customerimpacted='0' and i4"+i+".customeruniqueid='"+customerId+"' and i4"+i+".rootProductId='"+subscribedServices.get(i)+"')>=1 or (select count(distinct(i5"+i+".incidentid)) from incident i5"+i+" where i5"+i+".incidentstatus not in ('Cancelled','Completed', 'Closed') and i5"+i+".customeruniqueid='"+customerId+"' and i5"+i+".rootProductId='"+subscribedServices.get(i)+"')=0) then 'green' end),(select availability from saasavailability where customerid='"+customerId+"' and productname='"+subscribedServices.get(i)+"' and monthyear ='"+monthYear+"'),(select color from saasavailability where customerid='"+customerId+"' and productname='"+subscribedServices.get(i)+"' and monthyear ='"+monthYear+"')");
				if(i<subscribedServices.size()-1){
					queryBuilder.append(",");
				}
			}
			queryBuilder.append(" from dual");
			log.info("query for saaslevel 1 for serviceSpecs- "+queryBuilder.toString());
			
			Query specsQuery= sessionFactory.getCurrentSession().createSQLQuery(queryBuilder.toString());
			Object[] result=(Object[])specsQuery.uniqueResult();
			
			if(null!=result){
				for (int i = 0; i < subscribedServices.size(); i++) {
					ArrayList<String> specsList=new ArrayList<String>();
					specsList.add(subscribedServices.get(i).toString());
					specsList.add(null!=result[(i*numberOfFields.length)]?result[(i*numberOfFields.length)].toString():greyOut);
					if(null!=result[(i*numberOfFields.length)+1] && result[(i*numberOfFields.length)+1].toString().startsWith("-1.0")){
						specsList.add("");
					}else{
						specsList.add(null!=result[(i*numberOfFields.length)+1]?result[(i*numberOfFields.length)+1].toString():"0");
					}
					
					specsList.add(null!=result[(i*numberOfFields.length)+2]?result[(i*numberOfFields.length)+2].toString():greyOut);
					serviceSpecs.add(specsList);
				}
			}
			
			
		}
		
		Query query1= sessionFactory.getCurrentSession().createSQLQuery("select distinct(productname) from producttype where rootproductname='"+saasRootProduct+"' and productname not in (select case when productid like 'Mandoe%' then 'Mandoe' else productId end from configurationitem where rootProductId='"+saasRootProduct+"' and customeruniqueid='"+customerId+"')");
		List unsubscribedServices= query1.list();
		
		if(null!=unsubscribedServices && !(unsubscribedServices.isEmpty())){
			for (int i = 0; i < unsubscribedServices.size(); i++) {
				ArrayList<String> specsList=new ArrayList<String>();
				specsList.add(unsubscribedServices.get(i).toString());
				specsList.add(greyOut);
				specsList.add("");
				specsList.add(greyOut);
				serviceSpecs.add(specsList);
			}
		}
		
		for (int i = 0; i < serviceSpecs.size(); i++) {
			for (int j = 0; j < serviceSpecs.get(i).size(); j++) {
				log.info("el- "+serviceSpecs.get(i).get(j));
			}
		}
		
		return serviceSpecs;
	}
}
