package com.tt.saaslevel1.mvc.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.saaslevel1.mvc.dao.SaasLevel1DaoImpl;


@Service(value = "saasLevel1Manager")
@Transactional
public class SaasLevel1ManagerImpl {
	
	private SaasLevel1DaoImpl saasLevel1DaoImpl;

	@Autowired
	@Qualifier("saasLevel1DaoImpl")
	public void setSaasLevel1DaoImpl(SaasLevel1DaoImpl saasLevel1DaoImpl) {
		this.saasLevel1DaoImpl = saasLevel1DaoImpl;
	}

	
	public ArrayList<ArrayList<String>> getServiceSpecs(String customerId){
		return saasLevel1DaoImpl.getServiceSpecs(customerId);
	}

}
