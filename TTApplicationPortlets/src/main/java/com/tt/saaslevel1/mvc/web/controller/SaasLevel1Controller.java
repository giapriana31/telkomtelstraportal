package com.tt.saaslevel1.mvc.web.controller;

import java.util.ArrayList;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.saaslevel1.mvc.service.SaasLevel1ManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("saasLevel1Controller")
@RequestMapping("VIEW")
public class SaasLevel1Controller {
	
	
	private SaasLevel1ManagerImpl saasLevel1ManagerImpl;

	@Autowired
	@Qualifier("saasLevel1Manager")
	public void setSaasLevel1ManagerImpl(SaasLevel1ManagerImpl saasLevel1ManagerImpl) {
		this.saasLevel1ManagerImpl = saasLevel1ManagerImpl;
	}
	
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(renderResponse);
	
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		System.out.println("cust: "+customerId);
		
		ArrayList<ArrayList<String>> serviceSpecs= saasLevel1ManagerImpl.getServiceSpecs(customerId);
		model.addAttribute("serviceSpecs",serviceSpecs);
				
		return "saasLevel1";
	}

}
