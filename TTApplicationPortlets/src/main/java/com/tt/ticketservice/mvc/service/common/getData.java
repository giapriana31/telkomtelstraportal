package com.tt.ticketservice.mvc.service.common;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;

import com.tt.model.Incident;
import com.tt.model.Notes;

public class getData {
    
    public getData() {
    }

    @SuppressWarnings("unchecked")
    public List<Notes> fetch_notes(String incident_id) {
	try {
	    //incident_id = "INC0046858";
	    Session session = new AnnotationConfiguration().configure()
		    .buildSessionFactory().openSession();
	    SQLQuery query = session
		    .createSQLQuery("SELECT id , portalusername ,custinfonotes ,DATE_FORMAT(modifiedtime, '%d-%m-%y') as modifiedtime FROM incdnotes where incidentid LIKE \""
			    + incident_id
			    + "\" ORDER BY modifiedtime ASC, portalusername ASC");
	    query.addEntity(Notes.class);
	    List<Notes> list = query.list();
	    session.close();
	    return list;
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }

    @SuppressWarnings("unchecked")
    public List<Incident> fetch_incident(String incident_id) {
	try {
	    //incident_id = "INC0046858";
	    Session session = new AnnotationConfiguration().configure()
		    .buildSessionFactory().openSession();
	    SQLQuery query = session
		    .createSQLQuery("select * from incident where incidentid LIKE \""
			    + incident_id + "\"");
	    query.addEntity(Incident.class);
	    List<Incident> list = query.list();
	    session.close();
	    return list;
	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
	}
    }

}
