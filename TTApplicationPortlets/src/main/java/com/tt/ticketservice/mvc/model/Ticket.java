package com.tt.ticketservice.mvc.model;


public class Ticket {
	
	private String ticketNumber;
	private String status;
	private String subject;
	private String lastUpdated;
	private String assignedTo;
	private String dueBy;
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getDueBy() {
		return dueBy;
	}
	public void setDueBy(String dueBy) {
		this.dueBy = dueBy;
	}
	public Ticket(String ticketNumber, String status, String subject,
			String lastUpdated, String assignedTo, String dueBy) {
		super();
		this.ticketNumber = ticketNumber;
		this.status = status;
		this.subject = subject;
		this.lastUpdated = lastUpdated;
		this.assignedTo = assignedTo;
		this.dueBy = dueBy;
	}

	
	

	

}
