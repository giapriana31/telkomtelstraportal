package com.tt.ticketservice.mvc.model;

import java.io.Serializable;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;

import com.liferay.portal.util.PortalUtil;

public class TicketUIPaginatedListForm implements PaginatedList, Serializable {

    /** default page size */
    private static int DEFAULT_PAGE_SIZE = 10;

    /** current page index, starts at 0 */
    private int index;

    /** number of results per page */
    private int pageSize;

    /** total results (records, not pages) */
    private int fullListSize;

    /** list of results in the current page */
    private List list;

    /** default sorting order */
    private SortOrderEnum sortDirection = SortOrderEnum.ASCENDING;

    /** sort criteria (sorting property name) */
    private String sortCriterion;

    public TicketUIPaginatedListForm(RenderRequest request) {
	this(PortalUtil.getHttpServletRequest(request));
    }

    public TicketUIPaginatedListForm(HttpServletRequest request) {
	this.sortCriterion = request.getParameter("sort");
	this.sortDirection = "desc".equals(request.getParameter("dir")) ? SortOrderEnum.DESCENDING
		: SortOrderEnum.ASCENDING;
	String page = request.getParameter("page");

	System.out.println(page);

	index = (page == null || "".equals(page)) ? 0
		: Integer.parseInt(page) - 1;
	pageSize = 10;
    }

    /**
     * @return A readable description of this instance state for logs
     */
    public String toString() {
	return "PageResponse { index = " + index + ", pageSize = " + pageSize
		+ ", total = " + fullListSize + " }";
    }

    public int getFirstRecordIndex() {
	return index * pageSize;
    }

    public int getLastRecordIndex() {
	return (index * pageSize) + pageSize;
    }

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public int getPageSize() {
	return pageSize;
    }

    public void setPageSize(int pageSize) {
	this.pageSize = pageSize;
    }

    public List getList() {
	return list;
    }

    public void setList(List results) {
	this.list = results;
    }

    public int getFullListSize() {
	return fullListSize;
    }

    public void setTotal(int total) {
	this.fullListSize = total;
    }

    public int getTotalPages() {
	return (int) Math.ceil(((double) fullListSize) / pageSize);
    }

    public int getObjectsPerPage() {
	return pageSize;
    }

    public int getPageNumber() {
	return index + 1;
    }

    public String getSearchId() {

	return null;
    }

    public String getSortCriterion() {
	return sortCriterion;
    }

    public SortOrderEnum getSortDirection() {
	return sortDirection;
    }

    public String getSqlSortDirection() {
	return SortOrderEnum.DESCENDING.equals(sortDirection) ? "desc" : "asc";
    }

    public void setSortCriterion(String sortCriterion) {
	this.sortCriterion = sortCriterion;
    }

    public void setSortDirection(SortOrderEnum sortDirection) {
	this.sortDirection = sortDirection;
    }

}
