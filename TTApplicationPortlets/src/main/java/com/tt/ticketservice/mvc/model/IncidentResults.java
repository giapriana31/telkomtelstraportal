package com.tt.ticketservice.mvc.model;

import java.util.List;

import com.tt.model.Incident;

public class IncidentResults {
	private int recordCount =0 ; // default value
	private List<Incident> incidentResults;
	
	public int getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
	}
	public List<Incident> getIncidentResults() {
		return incidentResults;
	}
	public void setIncidentResults(List<Incident> incidentResults) {
		this.incidentResults = incidentResults;
	}

}
