package com.tt.ticketservice.mvc.dao;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.Configuration;
import com.tt.model.Incident;
import com.tt.model.IncidentDashboardMapping;
import com.tt.ticketservice.mvc.model.IncidentResults;
import com.tt.ticketservice.mvc.model.SearchInputs;
import com.tt.ticketservice.mvc.web.controller.SearchController;


@Repository(value="ticketDAO")
@Transactional
public class TicketDAOImpl implements TicketDAO{
	
	private final static Logger log = Logger.getLogger(TicketDAOImpl.class);
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public static final String AND = "' AND ";
	public static final String EQUAL_SINGLE = " = '";
	public static final String INOPERATOR = " IN ";
	public static final String OPENBRACE = " ( ";
	public static final String CLOSEBRACE = " ) ";
	public static final String SINGLEQUOTE = "'";
	public static final String ANDOPERATOR = " AND ";
	public static final String BETWEEN = " BETWEEN ";
	public static final String TO_DATE = " TO_DATE ";
	public static final String DATEFORMAT = " 'yyyy-MM-dd HH:mm:ss' ";
	public static final String COMMA = " , ";
	public static final String ORDERBY = " ORDER BY ";
	public static final String OFFSET = " OFFSET ";
	public static final String FETCHNEXT = " FETCH NEXT ";
	public static final String ROWSONLY = " ROWS ONLY ";
	public static final String LIKE = " LIKE ";
	public static final String OROPERATOR = " OR ";
	public static final String PERCENTAGE = "%";
	public static final String LIMIT = " LIMIT ";
	public static final String NOTEQUAL = " <> ";
	public static final String EQUAL = " = ";
	public static final String NULLVALUE = " '' ";
	public static final String TRUEVALUE = " 1 ";


	// Field level constants
	public static final String CMDBCLASS = " cmdbclass ";
	public static final String ROOTPRODUCTID = " rootproductid ";
	public static final String CUSTOMERIMPACTED = " customerimpacted ";
	public static final String OPENDATETIME = " opendatetime ";
	public static final String RESOLUTIONDATETIME = " resolutiondatetime ";
	public static final String CASEMANAGER = " casemanager ";
	//public static final String OPENDATETIME = " opendatetime "; 
	public static final String INCIDENTID = " incidentid ";
	public static final String SITENAME = " sitename ";
	public static final String SITESERVICETIER = " siteservicetier ";
	public static final String PRIORITY = " priority ";
	public static final String SHORTDESCRIPTION = " summary ";
	public static final String INCIDENTSTART = " opendatetime ";
	public static final String LASTUPDATED = " lastupdated ";
	public static final String TICKETSTATUS = " incidentstatus ";
	
	public static final String CUSTOMERUNIQUEID = " customeruniqueid  ";
	public static final String CITYPE = " citype  ";
	public static final String USERID = " userid  ";
	public static final String CINAME = " ciname  ";
	public static final String SITEID = " siteid  ";
	public static final String PRODUCTID = " productId  "; 
	public static final String PRODUCTCLASSIFICATION = " productclassification  ";
	public static final String PRODUCTFLAG = " productflag  ";
	public static final String CREATEDDATE = " createddate  ";
	public static final String ALL_SERVICES = "allServices";
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Incident> listTickets() {
		log.info("Get all the incidents");
		return sessionFactory.getCurrentSession().createQuery("from Incident").list();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Incident> searchIncidents(String impact, String priority, String serviceName, String State, String siteName, Date dateFrom, Date dateTo, String assignedTo  ) {
		
		log.info("searchIncidents");
	
		StringBuilder searhQuery = new StringBuilder();
		searhQuery.append(" from Incident where " );
		log.debug("Start the query");
		if ( impact != null && !impact.isEmpty() ) 
			searhQuery.append(SearchController.IMPACT).append(EQUAL_SINGLE).append(impact).append(AND);
		if ( priority != null && !priority.isEmpty() ) 
			searhQuery.append(SearchController.PRIORITY).append(EQUAL_SINGLE).append(priority).append(AND);			
		if ( serviceName != null && !serviceName.isEmpty() ) 		{	
			// process through list if multi rows
			String ciid = findCiid(serviceName).get(0).getCiid();
			String incidentid =  findServiceId( ciid );
			searhQuery.append(TicketDAOImpl.INCIDENTID).append(EQUAL_SINGLE).append(incidentid).append(AND);						
		}
		log.debug("Impact and priority checked");
		if ( State != null && !State.isEmpty() ) 
			searhQuery.append(TicketDAOImpl.CUSTOMERIMPACTED).append(EQUAL_SINGLE).append(State).append(AND);
		if ( siteName != null && !siteName.isEmpty() ) 
			searhQuery.append(SearchController.SITENAME).append(EQUAL_SINGLE).append(siteName).append(AND);			
		if ( dateFrom != null) 
			searhQuery.append(TicketDAOImpl.OPENDATETIME).append(EQUAL_SINGLE).append(dateFrom).append(AND);	
		if ( dateTo != null  ) 
			searhQuery.append(TicketDAOImpl.RESOLUTIONDATETIME).append(EQUAL_SINGLE).append(dateTo).append(AND);			
		if ( assignedTo != null && !assignedTo.isEmpty() ) 
			searhQuery.append(TicketDAOImpl.USERID).append(EQUAL_SINGLE).append(assignedTo).append("'");	
		    
		if ( searhQuery.lastIndexOf(AND) == searhQuery.length()-6 )
			searhQuery.delete(searhQuery.length()-5, searhQuery.length());
		
		
		
		log.debug("After --->" +searhQuery);
		
		List<Incident>  results = sessionFactory.getCurrentSession().createQuery(searhQuery.toString() ).list();
		log.info("results size" + results.size());
		log.info("SearchIncident END");
		return results;
	}
	
	 
	@SuppressWarnings("unchecked")
	private List<Configuration> findCiid(String ciname) {
		log.debug("findCiid starts");
		StringBuilder query = new StringBuilder();
		query.append(" from Configuration where ciname=:ciName and status in ('")
			 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
			 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
		return sessionFactory.getCurrentSession().createQuery(query.toString()).setString("ciName", ciname).list();  
	}
	
	private String findServiceId(String ciid) {
		log.debug("findServiceId");
		Query query = sessionFactory.getCurrentSession().createQuery("select incidentid from CIIncidentMapping where ciid= '" + ciid +"'" );
		if(!query.list().isEmpty() && query.list().get(0) != null){
		    return (String) query.list().get(0);    
		}else
		    return null;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public IncidentResults searchIncidents( SearchInputs searchInputs,String status ) {
		//	List<String> impact, List<String>  priority, String serviceName,  String siteName, Date dateFrom, Date dateTo, String assignedTo, String sortCriterion, String sortDirection, int pageNumber,String searchqueryList  ) {
		log.info("SearchIncidents has started");
		if(searchInputs.getPriority()!=null){
			log.debug("SearchInput is not null:: "+searchInputs.getPriority());
		}
		IncidentResults incidentResults = new IncidentResults();
		
		StringBuilder searhQuery = new StringBuilder();
		
		String objFetchQueryString="select incidentid, summary, notes, priority, customerimpacted, userid, lastupdated, incidentstatus, rootproductid, category, serviceleveltarget, hierescalationflag, sitename, siteservicetier, opendatetime, resolutiondatetime, serviceid, bussinesduration ";
		String countQueryString="select count(1) ";
		
		/*searhQuery.append(" from incident where incidentid IN ");*/
		searhQuery.append(" from incidentdashboard where incidentid IN ");
		
		if(searchInputs.getVmIdentifier()!=null && !searchInputs.getVmIdentifier().isEmpty()){
									
			searhQuery.append("(select incidentid from incdtodevicemapping where deviceciid='"+searchInputs.getVmIdentifier()+"') AND ");
			
		}
		else{
			
			/*searhQuery.append("(select incidentid from incddashboardmetrics where " );*/
			searhQuery.append("(select incidentid from incidentdashboard where " );
			if(status.equalsIgnoreCase("active")){
				searhQuery.append("incidentstatus not in ('Closed','Cancelled','Completed') and ");
			}
			else{
				searhQuery.append("incidentstatus in ('Closed','Cancelled','Completed') and ");
			}
			
			
			if ( searchInputs.getServiceName() != null && !searchInputs.getServiceName().isEmpty() && !searchInputs.getServiceName().equalsIgnoreCase(ALL_SERVICES) ){
				
				//searhQuery.append(ROOTPRODUCTID).append(EQUAL_SINGLE).append(searchInputs.getServiceName()).append(AND);
				boolean isSaas = false;
				String productId = "";
				
				if(searchInputs.getServiceName().equalsIgnoreCase("Multi Channel Communication")){
					productId = "Whispir";
					isSaas = true;
				}else if(searchInputs.getServiceName().equalsIgnoreCase("Digital Proximity")){
					productId = "Mandoe";
					isSaas = true;
				}else if(searchInputs.getServiceName().equalsIgnoreCase("Cloud Contact Centre")){
					productId = "IPScape";
					isSaas = true;
				}else if(searchInputs.getServiceName().equalsIgnoreCase("Cloud productivity")){
					productId = "Cloud productivity";
					isSaas = true;
				}
				
				if(isSaas){
					searhQuery.append(PRODUCTID).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(productId).append(PERCENTAGE).append(SINGLEQUOTE).append(ANDOPERATOR);
				}else{
					searhQuery.append(PRODUCTCLASSIFICATION).append(EQUAL_SINGLE).append(searchInputs.getServiceName()).append(SINGLEQUOTE).append(ANDOPERATOR);
				}
			}else{
					searhQuery.append(PRODUCTFLAG).append(EQUAL_SINGLE).append(TRUEVALUE).append(SINGLEQUOTE).append(ANDOPERATOR);
				
			}
			
			
			log.debug("cust-----------------"+searchInputs.getCustomeruniqueid());
			
			if ( searchInputs.getCustomeruniqueid() != null && !searchInputs.getCustomeruniqueid().isEmpty() ) 
				searhQuery.append(CUSTOMERUNIQUEID).append(EQUAL_SINGLE).append(searchInputs.getCustomeruniqueid()).append(AND);
			
			if ( searchInputs.getCustomeruniqueid() != null && !searchInputs.getCustomeruniqueid().isEmpty() ) 
				searhQuery.append(CITYPE).append(EQUAL_SINGLE).append(searchInputs.getCitype()).append(AND);	
			
			if ( searchInputs.getSiteName() != null && !searchInputs.getSiteName().isEmpty() ) 
				searhQuery.append(SITEID).append(EQUAL_SINGLE).append(searchInputs.getSiteName()).append(AND);
			
			if(!(status.equalsIgnoreCase("active"))){
				
				Calendar cal=Calendar.getInstance();
				cal.add( Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK)-1)); 
				java.util.Date currentDate= cal.getTime();
				int period=(-1)*84;
				cal.add(Calendar.DATE, period);
				java.util.Date startDate= cal.getTime();
				
				cal.setTime(currentDate); 
				cal.add(Calendar.DATE, -1);
				currentDate = cal.getTime();
				
				SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
				String currentDateString=simpleDateFormat.format(currentDate);
				currentDateString=currentDateString.concat(" 23:59:59");
				String startDateString=simpleDateFormat.format(startDate);
				startDateString=startDateString.concat(" 00:00:00");
				
				searhQuery.append(CREATEDDATE).append(BETWEEN).append(SINGLEQUOTE).append(startDateString).append(AND).append(SINGLEQUOTE).append(currentDateString).append(AND); 
			}
			
			if ( searchInputs.getCiname() != null && !searchInputs.getCiname().isEmpty() ) 
				searhQuery.append(CINAME).append(EQUAL_SINGLE).append(searchInputs.getCiname()).append(SINGLEQUOTE);
			
			if ( searhQuery.lastIndexOf(AND) == searhQuery.length()-6 )
				searhQuery.delete(searhQuery.length()-5, searhQuery.length()); 
			
			searhQuery.append(") AND ");
			
			
			log.info("incdmetrics query---"+searhQuery);

			
			
		}
		
		
		//iterate 
		/*if ( searchInputs.getImpact() != null && !searchInputs.getImpact().isEmpty() ) {
			
			searhQuery.append(CUSTOMERIMPACTED).append(INOPERATOR).append(OPENBRACE);
			for (String value:searchInputs.getImpact() ){
				searhQuery.append(SINGLEQUOTE).append(value).append(SINGLEQUOTE).append(COMMA); 
			}		
			searhQuery.delete(searhQuery.length()-3, searhQuery.length());
			searhQuery.append(CLOSEBRACE).append(ANDOPERATOR);
		}*/
		//iterate
		if (searchInputs.getPriority() != null && !searchInputs.getPriority().isEmpty() )  {
			searhQuery.append(SearchController.PRIORITY).append(INOPERATOR).append(OPENBRACE);
			for (String value:searchInputs.getPriority() ){
				searhQuery.append(SINGLEQUOTE).append(value).append(SINGLEQUOTE).append(COMMA); 
			}			
			searhQuery.delete(searhQuery.length()-3, searhQuery.length());
			searhQuery.append(CLOSEBRACE).append(ANDOPERATOR);
		}
		
		
		
		if ( searchInputs.getAssignedTo() != null && !searchInputs.getAssignedTo().isEmpty() ) 
			searhQuery.append(USERID).append(EQUAL_SINGLE).append(searchInputs.getAssignedTo()).append(AND);	
		
		
		// date to and fropm between
		if ( searchInputs.getDateFrom() != null && searchInputs.getDateTo() != null ) 
			searhQuery.append(OPENDATETIME).append(BETWEEN).append(SINGLEQUOTE)
			.append(searchInputs.getDateFrom()).append(AND).append(SINGLEQUOTE).append(searchInputs.getDateTo()).append(AND); 
		
		/*moved to inside incddashboardmetrics in above*/
		/*if(!(status.equalsIgnoreCase("active"))){
			
			Calendar cal=Calendar.getInstance();
			cal.add( Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK)-1)); 
			java.util.Date currentDate= cal.getTime();
			int period=(-1)*84;
			cal.add(Calendar.DATE, period);
			java.util.Date startDate= cal.getTime();
			
			cal.setTime(currentDate); 
			cal.add(Calendar.DATE, -1);
			currentDate = cal.getTime();
			
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
			String currentDateString=simpleDateFormat.format(currentDate);
			currentDateString=currentDateString.concat(" 23:59:59");
			String startDateString=simpleDateFormat.format(startDate);
			startDateString=startDateString.concat(" 00:00:00");
			
			searhQuery.append(CREATEDDATE).append(BETWEEN).append(SINGLEQUOTE).append(startDateString).append(AND).append(SINGLEQUOTE).append(currentDateString).append(AND); 
		}*/
		
		log.info("incdmetrics query active---"+searhQuery);

		if(searchInputs.getProductType() != null && !searchInputs.getProductType().isEmpty() && !searchInputs.getProductType().equalsIgnoreCase(ALL_SERVICES)){
			String productType= searchInputs.getProductType();
			searhQuery.append(ROOTPRODUCTID).append(EQUAL_SINGLE).append(productType).append(AND);
		}
		
		
		// search query searchqueryList
		if ( searchInputs.getSearchqueryList() != null && !searchInputs.getSearchqueryList().isEmpty() )		

			searhQuery.append(OPENBRACE).append(INCIDENTID).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE)
			.append(OROPERATOR).append(SITENAME).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE)
			.append(OROPERATOR).append(SITESERVICETIER).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE)
			.append(OROPERATOR).append(PRIORITY).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE)
			.append(OROPERATOR).append(SHORTDESCRIPTION).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE)
			.append(OROPERATOR).append(INCIDENTSTART).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE)
			.append(OROPERATOR).append(LASTUPDATED).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE)
			.append(OROPERATOR).append(TICKETSTATUS).append(LIKE).append(SINGLEQUOTE).append(PERCENTAGE).append(searchInputs.getSearchqueryList()).append(PERCENTAGE).append(SINGLEQUOTE).append(CLOSEBRACE);
		
		  // Where incidentid like ‘%{ searchquery }%’ or shortdescription like ‘%{ searchquery }%’ or casemanager like ‘%{ searchquery }%’
		
		
		 if ( searhQuery.lastIndexOf(ANDOPERATOR) == searhQuery.length()-5 )
			searhQuery.delete(searhQuery.length()-5, searhQuery.length());
		 
		 log.debug("Printing searhQuery \n "  + searhQuery ); 
		
		 Number recordCounts = (Number) sessionFactory.getCurrentSession().createSQLQuery(countQueryString+ searhQuery.toString()).uniqueResult();
		
		 if (recordCounts !=null && recordCounts.intValue() > 0 ) {
			 
			 incidentResults.setRecordCount(recordCounts.intValue()); 
			 log.info("recordCounts.intValue() \n "  + recordCounts.intValue() ); 
			// sortCriterion
			if ( searchInputs.getSortCriterion() != null && !searchInputs.getSortCriterion().isEmpty() ) 
				searhQuery.append(ORDERBY).append(searchInputs.getSortCriterion());
		
			// asc or desc 
			if ( searchInputs.getSortDirection() != null && !searchInputs.getSortDirection().isEmpty() ) 
				searhQuery.append(" ").append(searchInputs.getSortDirection());
			
			// page no pageNumber
			log.debug("Pagination calculation started");
			
			Integer offsetInt=0;
			offsetInt = (searchInputs.getPageNumber()-1 ) * 10 ;
			
			searhQuery.append(" limit 10 offset "+offsetInt);
			
			Query query = sessionFactory.getCurrentSession().createSQLQuery(objFetchQueryString+searhQuery.toString());
			
			
			log.info("Printing searhQuery with sorting \n "  + searhQuery ); 
			
			List<Incident> incidentList = new ArrayList<Incident>();
			SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			List incList = query.list();
			log.info("list obtained from incident query :: " + incList);
			
			for(int i=0;i<incList.size(); i++)
			{
				Incident incident= new Incident();
				log.info("setting all the parameters in the object : " + incList.get(i));
				
				/*ChangeRequestApprovals craObject = (ChangeRequestApprovals) configList.get(i);
				changeRequestApprovals.setApprover(craObject.getApprover());
				changeRequestApprovals.setApprovaldate(craObject.getApprovaldate());
				changeRequestApprovals.setApprovalgroup(craObject.getApprovalgroup());*/
				
				Object[] object = (Object[]) incList.get(i);
				
				incident.setIncidentid(null!=object[0] ? object[0].toString(): null);
				incident.setSummary(null!=object[1] ? object[1].toString(): null);
				incident.setNotes(null!=object[2] ? object[2].toString(): null);
				incident.setPriority(null!=object[3] ? Long.parseLong(object[3].toString()): null);
				incident.setCustomerimpacted(null!=object[4] ? Boolean.parseBoolean(object[4].toString()): null);
				incident.setUserid(null!=object[5] ? object[5].toString(): "");
				try {
					incident.setLastupdated(null!=object[6] ? simpleDateFormat.parse(object[6].toString()): null);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				incident.setIncidentstatus(null!=object[7] ? object[7].toString(): "");
				incident.setRootProductId(null!=object[8] ? object[8].toString(): "");
				incident.setCategory(null!=object[9] ? object[9].toString(): "");
				try {
					incident.setServiceleveltarget(null!=object[10] ? simpleDateFormat.parse(object[10].toString()): null);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				incident.setHierEscalationFlag(null!=object[11] ? Boolean.parseBoolean(object[11].toString()): null);
				
				//add new field
				incident.setSitename(null!=object[12] ? object[12].toString(): null);
				incident.setSiteservicetier(null!=object[13] ? object[13].toString(): null);
				try {
					incident.setOpendatetime(null!=object[14] ? simpleDateFormat.parse(object[14].toString()): null);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					incident.setResolutiondatetime(null!=object[15] ? simpleDateFormat.parse(object[15].toString()): null);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				incident.setServiceid(null!=object[16] ? object[16].toString(): null);
				incident.setWorklognotes(null!=object[17] ? object[17].toString(): null);
				
				
				
				incidentList.add(incident);
			}
			
			List<Incident>  incidentResultset = incidentList;
			log.debug("results size is:::" + incidentResultset.size());
			incidentResults.setIncidentResults(incidentResultset);
		 }
		 
		 else{
			 incidentResults.setRecordCount(0);
			 incidentResults.setIncidentResults(null);
		 }
		 
		 
		 
		 return incidentResults;
		 
	}

}
