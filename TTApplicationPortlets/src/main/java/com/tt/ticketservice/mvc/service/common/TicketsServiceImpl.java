package com.tt.ticketservice.mvc.service.common;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.Incident;
import com.tt.ticketservice.mvc.dao.TicketDAO;
import com.tt.ticketservice.mvc.model.IncidentResults;
import com.tt.ticketservice.mvc.model.SearchInputs;

@Service(value="ticketService")
@Transactional
public class TicketsServiceImpl implements TicketsService{

	@Autowired
	@Qualifier("ticketDAO")
	private TicketDAO ticketDAO;
	
	@Transactional
	public List<Incident> listTickets() {
		return ticketDAO.listTickets();
	}
	
	@Transactional
	public List<Incident> searchIncidents(String impact, String priority,
			String serviceName, String State, String siteName, Date dateFrom,
			Date dateTo, String assignedTo) {
		return ticketDAO.searchIncidents(impact, priority, serviceName, State, siteName, dateFrom, dateTo, assignedTo);
	}
	
	@Transactional
	public IncidentResults searchIncidents( SearchInputs searchInputs,String status ) {
		return ticketDAO.searchIncidents(searchInputs,status);
	}

	@Override
	public List<String> getSiteNames(String customeruniqueId) {
		return null;
	}

}
