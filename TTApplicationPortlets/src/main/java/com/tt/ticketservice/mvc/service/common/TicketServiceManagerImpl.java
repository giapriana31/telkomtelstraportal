package com.tt.ticketservice.mvc.service.common;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.Configuration;
import com.tt.model.Incident;
import com.tt.model.VirtualMachine;
import com.tt.ticketservice.mvc.dao.TicketServiceDaoImpl;
import com.tt.ticketservice.mvc.model.TicketUIPaginatedListForm;

@Service(value="ticketServiceManager")
@Transactional
public class TicketServiceManagerImpl {

	private TicketServiceDaoImpl commonDaoImpl;

	@Autowired
	@Qualifier("ticketServiceDaoImpl")
	public void setCommonDaoImpl(TicketServiceDaoImpl commonDaoImpl) {
		this.commonDaoImpl = commonDaoImpl;
	}
	
	public List<Incident> getcommonDBCall(){
		System.out.println("commonmanagerimpl");
		return commonDaoImpl.getcommonDBCall();
	}
	
	public List<Incident> getcustomDBCall(TicketUIPaginatedListForm paginatedList){
		System.out.println("custommanagerimpl");
		return commonDaoImpl.getcustomDBCall(paginatedList);
	}
	
	public List<Incident> getcustomDBCall(){
		return commonDaoImpl.getcommonDBCall();
	}
	
	public LinkedHashMap<String,String> getServiceFromSiteID(String siteID) throws Exception{
		try{
			return commonDaoImpl.getServiceFromSiteID(siteID);
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public LinkedHashMap<String, String> getSiteNameMap(String customeruniqueID) throws Exception{
		try{
			return commonDaoImpl.getSiteNameMap(customeruniqueID);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getAllServices() throws Exception{
		try{
			return commonDaoImpl.getAllServices();
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public String getSiteName(String siteName) throws Exception{
		try{
			return commonDaoImpl.getSiteName(siteName);
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	
	}
	
	public LinkedHashMap<String, String> getVBlocks() throws Exception{
		try{
			return commonDaoImpl.getVBlocks();	
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
	}
	
	public LinkedHashMap<String, String> getVM(String selectedVBlock) throws Exception{
		try{
			return commonDaoImpl.getVM(selectedVBlock);	
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
	}
	
	public ArrayList<String> getAllServices(String customerId){
		return commonDaoImpl.getAllServices(customerId);
	}

	public LinkedHashMap<String, String> getVirtualMachineList(String customerUniqueID) {
	
		LinkedHashMap<String, String> virtualMachine = new LinkedHashMap<String, String>();
		List<Configuration> configurations = commonDaoImpl.getVirtualMachineList(customerUniqueID);
		virtualMachine.put("allVM", "All Virtual Machine");
		if(configurations!=null && configurations.size()>0){
			for (Configuration configuration : configurations) {
				virtualMachine.put(configuration.getCiid(), configuration.getCiname());
			}
		}
		
		
		return virtualMachine;
	}
}
