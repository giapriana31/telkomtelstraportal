package com.tt.ticketservice.mvc.model;

import java.sql.Timestamp;
import java.util.List;

public class SearchInputs {

    private List<String> priority;
    private String serviceName;
    private String ciname;
    private String siteName;
    private String dateFrom;
    private String dateTo;
    private String assignedTo;
    private String sortCriterion;
    private String sortDirection;
    private int pageNumber = 1; // default value
    private String searchqueryList;
    private String productType;
    private String vmIdentifier;

    public String getVmIdentifier() {
		return vmIdentifier;
	}

	public void setVmIdentifier(String vmIdentifier) {
		this.vmIdentifier = vmIdentifier;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	// Set it from session
    private String customeruniqueid;
    private String citype = "Service"; // default value
    private List<String> impact;

    public String getCustomeruniqueid() {
	return customeruniqueid;
    }

    public void setCustomeruniqueid(String customeruniqueid) {
	this.customeruniqueid = customeruniqueid;
    }

    public String getCitype() {
	return citype;
    }

    public void setCitype(String citype) {
	this.citype = citype;
    }

    public List<String> getImpact() {
	return impact;
    }

    public void setImpact(List<String> impact) {
	this.impact = impact;
    }

    public List<String> getPriority() {
	return priority;
    }

    public void setPriority(List<String> priority) {
	this.priority = priority;
    }

    public String getServiceName() {
	return serviceName;
    }

    public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
    }

    public String getSiteName() {
	return siteName;
    }

    public void setSiteName(String siteName) {
	this.siteName = siteName;
    }

    public String getDateFrom() {
	return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
	this.dateFrom = dateFrom;
    }

    public String getDateTo() {
	return dateTo;
    }

    public void setDateTo(String dateTo) {
	this.dateTo = dateTo;
    }

    public String getAssignedTo() {
	return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
	this.assignedTo = assignedTo;
    }

    public String getSortCriterion() {
	return sortCriterion;
    }

    public void setSortCriterion(String sortCriterion) {
	this.sortCriterion = sortCriterion;
    }

    public String getSortDirection() {
	return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
	this.sortDirection = sortDirection;
    }

    public int getPageNumber() {
	return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
	this.pageNumber = pageNumber;
    }

    public String getSearchqueryList() {
	return searchqueryList;
    }

    public void setSearchqueryList(String searchqueryList) {
	this.searchqueryList = searchqueryList;
    }

    public String getCiname() {
	return ciname;
    }

    public void setCiname(String ciname) {
	this.ciname = ciname;
    }

}
