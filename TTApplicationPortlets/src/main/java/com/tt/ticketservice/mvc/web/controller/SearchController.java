package com.tt.ticketservice.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.portlet.util.PortletUtils;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.model.Configuration;
import com.tt.model.Incident;
import com.tt.model.VirtualMachine;
import com.tt.ticketservice.mvc.model.SearchInputs;
import com.tt.ticketservice.mvc.service.common.TicketServiceManagerImpl;
import com.tt.ticketservice.mvc.service.common.TicketsService;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("searchController")
@RequestMapping("VIEW")
public class SearchController {

	private TicketServiceManagerImpl commonManager;

	private TicketsService ticketService;
	private String siteNameGlobal;
	private String siteNameLabelGlobal;
	private String statusGlobal;
	private String headerTab = "active";

	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(SearchController.class);

	@Autowired
	@Qualifier("ticketService")
	public void setTicketService(TicketsService ticketService) {
		this.ticketService = ticketService;
	}

	@Autowired
	@Qualifier("ticketServiceManager")
	public void setCommonManager(TicketServiceManagerImpl commonManager) {
		this.commonManager = commonManager;
	}

	public static final String IMPACT = "impact";
	public static final String PRIORITY = "priority";
	public static final String SERVICENAME = "serviceName";
	public static final String STATE = "state";
	public static final String SITENAME = "siteName";
	public static final String DATEFROM = "dateFrom";
	public static final String DATETO = "dateTo";
	public static final String ASSIGNEDTO = "assignedTo";

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws Exception {
		try {
			HttpServletRequest httprequest = PortalUtil
					.getHttpServletRequest(request);
			HttpServletResponse httpresponse = PortalUtil
					.getHttpServletResponse(renderResponse);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(
					httprequest, httpresponse);
			LinkedHashMap<String, String> siteNameList = commonManager
					.getSiteNameMap(customerUniqueID);

			log.info("SiteNameList " + siteNameList);
			if (siteNameList != null && siteNameList.size() != 0) {
				log.info("Null Handled");
				LinkedHashMap<String, String> siteNameListActual = new LinkedHashMap<String, String>();
				siteNameListActual.put("allSites", "All Sites");
				for (Map.Entry<String, String> entry : siteNameList.entrySet()) {
					siteNameListActual.put(entry.getKey(), entry.getValue());

				}
				model.addAttribute("sitenamelist", siteNameListActual);
			} else {
				LinkedHashMap<String, String> siteNameListActual = new LinkedHashMap<String, String>();
				siteNameListActual.put("allSites", "All Sites");
				model.addAttribute("sitenamelist", siteNameListActual);
			}
			LinkedHashMap<String, String> serviceIDList = new LinkedHashMap<String, String>();
			serviceIDList.put("allServices", "All Services");
			model.addAttribute("servicenamelist", serviceIDList);
			Incident incident = new Incident();
			model.addAttribute("logIncident", incident);

			if (siteNameGlobal != null) {
				siteNameLabelGlobal = commonManager.getSiteName(siteNameGlobal);
				if (siteNameLabelGlobal != null) {
					model.addAttribute("siteNameLabelCheck",
							siteNameLabelGlobal);
				} else {
					model.addAttribute("siteNameLabelCheck", "");
				}
				model.addAttribute("siteNameCheck", siteNameGlobal);
			} else {
				model.addAttribute("siteNameLabelCheck", "");
				model.addAttribute("siteNameCheck", "");
			}

			List<String> productType = commonManager.getAllServices(customerUniqueID);
			
			
			LinkedHashMap<String, String> virtualMachineMap = new LinkedHashMap<String, String>();
			virtualMachineMap = commonManager.getVirtualMachineList(customerUniqueID);
			model.addAttribute("VMList", virtualMachineMap);
			
			if (productType != null) {
				model.addAttribute("productType", productType.toString()
						.replace("[", "").replace("]", ""));
			}

			siteNameGlobal = null;
			siteNameLabelGlobal = null;

			return "ticketServiceMapping";
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unused")
	@ResourceMapping(value = "getTicketList")
	public void getTicketList(ResourceRequest req, ResourceResponse res,
			Model model) {
		log.info("SearchController ResourceMap starts");

		try {

			HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
			HttpServletResponse response = PortalUtil
					.getHttpServletResponse(res);

			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(
					request, response);
			log.info("setting Cust ID");
			JSONObject obj = new JSONObject(
					request.getParameter("ticketFilterIdentifier"));

			List<String> ticketFilterList = new ArrayList<String>();

			String priority = obj.getString("priority");
			ticketFilterList.add(priority);
			String service = obj.getString("service");
			ticketFilterList.add(service);
			String impact = obj.getString("impact");
			ticketFilterList.add(impact);
			String siteName = obj.getString("siteName");
			ticketFilterList.add(siteName);
			String assignedTo = obj.getString("assignedTo");
			ticketFilterList.add(assignedTo);
			String dateFrom = obj.getString("dateFrom");
			ticketFilterList.add(dateFrom);
			String dateTo = obj.getString("dateTo");
			ticketFilterList.add(dateTo);
			String searchQuery = obj.getString("searchQuery");
			ticketFilterList.add(searchQuery);
			String pageNumber = obj.getString("pageNumber");
			ticketFilterList.add(pageNumber);
			String sortCriterion = obj.getString("sortCriterion");
			ticketFilterList.add(sortCriterion);
			String sortDirection = obj.getString("sortDirection");
			ticketFilterList.add(sortDirection);
			String ciname = obj.getString("ciname");
			ticketFilterList.add(ciname);
			String productType = obj.getString("productType");
			ticketFilterList.add(productType);
			String vmIdentifier = obj.getString("vmIdentifier");
			ticketFilterList.add(vmIdentifier);

			List<String> priorityList = new ArrayList<String>();

			if (!(priority.equalsIgnoreCase(""))) {
				String[] priorityArray = priority.split("&");

				int countPriorityList = priorityArray.length;
				for (int i = 0; i < countPriorityList; i++) {
					String temp = priorityArray[i].substring(1);
					priorityList.add(temp);
				}

			}

			else {
				priorityList = null;
			}

			List<String> impactList = new ArrayList<String>();

			if (!(impact.equalsIgnoreCase(""))) {

				String[] impactArray = impact.split("&");

				int countImpactList = impactArray.length;
				for (int i = 0; i < countImpactList; i++) {
					if (impactArray[i].equalsIgnoreCase("With Impacts")) {
						String priorityNumber = "1";
						impactList.add(priorityNumber);
					} else if (impactArray[i].equalsIgnoreCase("At Risk")) {
						String priorityNumber = "0";
						impactList.add(priorityNumber);
					}
				}
			}

			else {
				impactList = null;
			}
			String fromDate;

			if (!(dateFrom.equalsIgnoreCase(""))) {

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						"MM/dd/yyyy");
				Date fromDateParameter = simpleDateFormat.parse(dateFrom);

				simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				fromDate = simpleDateFormat.format(fromDateParameter);

			} else {
				fromDate = null;
			}

			Date dateToParameter;
			String toDate;
			if (!(dateTo.equalsIgnoreCase(""))) {

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						"MM/dd/yyyy");
				Date toDateParameter = simpleDateFormat.parse(dateTo);

				simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				toDate = simpleDateFormat.format(toDateParameter);

			} else {
				dateToParameter = new Date();
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						"yyyy-MM-dd");
				toDate = simpleDateFormat.format(dateToParameter);
			}

			String timeFactor = " 23:59:59";
			toDate = toDate.concat(timeFactor);

			SearchInputs inputs = new SearchInputs();

			inputs.setImpact(impactList);

			inputs.setPriority(priorityList);

			if (ciname != null && !(ciname.equals(""))
					&& !(ciname.equalsIgnoreCase("allServices")))
				inputs.setCiname(ciname);
			else if (service.equalsIgnoreCase("allServices")) {
				inputs.setCiname(null);
			}

			if (service != null && !(service.equals(""))
					&& !(service.equalsIgnoreCase("allServices")))
				inputs.setServiceName(service);
			else if (service.equalsIgnoreCase("allServices")) {
				inputs.setServiceName(null);
			} else {
				inputs.setServiceName(null);
			}

			if (siteName != null && !(siteName.equals(""))
					&& !(siteName.equalsIgnoreCase("allSites")))
				inputs.setSiteName(siteName);
			else if (siteName.equalsIgnoreCase("allSites")) {
				inputs.setSiteName(null);
			} else {
				inputs.setSiteName(null);
			}

			inputs.setDateFrom(fromDate);
			inputs.setDateTo(toDate);

			if (assignedTo != null && !(assignedTo.equals("")))
				inputs.setAssignedTo(assignedTo);
			else {
				inputs.setAssignedTo(null);
			}

			inputs.setCitype("Service");
			log.info("set custID" + customerUniqueID);
			inputs.setCustomeruniqueid(customerUniqueID);

			if (searchQuery != null && !(searchQuery.equals("")))
				inputs.setSearchqueryList(searchQuery);
			else {
				inputs.setSearchqueryList(null);
			}
			
			if (productType != null && !(productType.equals("")))
				inputs.setProductType(productType);
			else {
				inputs.setProductType(null);
			}
			
			if (vmIdentifier != null && !(vmIdentifier.equals("")))
				inputs.setVmIdentifier(vmIdentifier);
			else {
				inputs.setVmIdentifier(null);
			}
			
			Integer pageNumberInt = Integer.parseInt(pageNumber);

			inputs.setPageNumber(pageNumberInt);

			if (sortCriterion != null && !(sortCriterion.equals("")))
				inputs.setSortCriterion(sortCriterion);
			else {
				inputs.setSortCriterion(null);
			}

			if (sortDirection != null && !(sortDirection.equals("")))
				inputs.setSortDirection(sortDirection);
			else {
				inputs.setSortDirection(null);
			}

			String statusParameter = headerTab;
			//String statusParameter = "active";
			/*if (PortletUtils.getSessionAttribute(req, "statusGlobal") != null) {
				statusParameter = (String) PortletUtils.getSessionAttribute(
						req, "statusGlobal");
			}*/
			
			log.info("status in searchcontorller-- "+statusParameter);

			log.info("received status for search incidents. Status Parameter--"
					+ statusParameter);

			log.info("Before entering persistence layer for Search Controller");

			List<Incident> listOfIncidents = ticketService.searchIncidents(
					inputs, statusParameter).getIncidentResults();

			Integer reccount = ticketService.searchIncidents(inputs,
					statusParameter).getRecordCount();

			log.info("Total number of records:" + reccount);

			com.liferay.portal.kernel.json.JSONArray resultJsonArray = JSONFactoryUtil
					.createJSONArray();

			PrintWriter out;
			out = response.getWriter();

			log.info("Before populating JSON to be sent to VIEW");

			if (listOfIncidents == null || listOfIncidents.size() == 0) {
				com.liferay.portal.kernel.json.JSONObject jsonValue = JSONFactoryUtil
						.createJSONObject();
				jsonValue.put("noDataIdentifier", "nullReturned");
				resultJsonArray.put(jsonValue);
				out.print(resultJsonArray.toString());
			}

			else {

				Calendar calendar = Calendar.getInstance();

				java.util.Date now = calendar.getTime();

				Timestamp timeStampDate = new Timestamp(now.getTime());
				// if(listOfIncidents!=null && listOfIncidents.size()!=0){
				for (Incident incident : listOfIncidents) {

					com.liferay.portal.kernel.json.JSONObject jsonValue = JSONFactoryUtil
							.createJSONObject();
					if (incident.getIncidentid() != null) {
						jsonValue.put("incidentId", incident.getIncidentid());
					} else {
						jsonValue.put("incidentId", "");
					}
					if (incident.getSummary() != null) {
						jsonValue
								.put("shortDescription", incident.getSummary());
					} else {
						jsonValue.put("shortDescription", "");
					}
					if (incident.getNotes() != null) {
						jsonValue.put("longDescription", incident.getNotes());
					} else {
						jsonValue.put("longDescription", "");
					}
					if (incident.getPriority() != null) {
						/*String priorityDisplay = "P" + incident.getPriority();
						jsonValue.put("priority", priorityDisplay);*/
						jsonValue.put("priority", incident.getPriority());
					} else {
						jsonValue.put("priority", "");
					}
					String stateDisplay = "";
					if (incident.getCustomerimpacted() != null) {
						if (incident.getCustomerimpacted()) {
							stateDisplay = "With Impact";
						} else {
							stateDisplay = "At Risk";
						}
					}
					jsonValue.put("state", stateDisplay);
					if (incident.getUserid() != null) {
						jsonValue.put("assignee", incident.getUserid());
					} else {
						jsonValue.put("assignee", "");
					}
					if (incident.getLastupdated() != null) {
						Date lastUpdatedDate = incident.getLastupdated();
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");

						String lastUpdatedString = dateFormat
								.format(lastUpdatedDate);

						jsonValue.put("lastUpdated", lastUpdatedString);
					} else {
						jsonValue.put("lastUpdated", "");
					}
					if (incident.getIncidentstatus() != null) {
						jsonValue.put("status", incident.getIncidentstatus());
					} else {
						jsonValue.put("status", "");
					}
					// ---------------------------------------Category Starts
					if (incident.getRootProductId() != null) {
						jsonValue.put("category", incident.getRootProductId());
					} else {
						jsonValue.put("category", "");
					}
					log.info("The JJ Category is:::" + incident.getCategory());
					// ---------------------------------------Category Ends
					if (incident.getServiceleveltarget() != null) {

						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						String serviceLevelTargetString = dateFormat
								.format(incident.getServiceleveltarget());
						jsonValue.put("dueBy", serviceLevelTargetString);
					} else {
						jsonValue.put("dueBy", "");
					}

					String showFlag = "";
					if (incident.getHierEscalationFlag() != null
							&& incident.getMIM() != null) {
						if (incident.getHierEscalationFlag()
								|| (!incident.getHierEscalationFlag() && incident
										.getMIM().toUpperCase()
										.equals("ENGAGED"))) {
							showFlag = "true";

						} else {
							showFlag = "false";

						}
					}

					jsonValue.put("showEscalationFlag", showFlag);
					
					if (incident.getSitename() != null) {
						jsonValue.put("siteName", incident.getSitename());
					} else {
						jsonValue.put("siteName", "");
					}
					
					if (incident.getSiteservicetier() != null) {
						jsonValue.put("siteServiceTier", incident.getSiteservicetier());
					} else {
						jsonValue.put("siteServiceTier", "");
					}
					
					if (incident.getOpendatetime() != null) {
						Date openDate = incident.getOpendatetime();
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");

						String openDateTime = dateFormat
								.format(openDate);

						jsonValue.put("openDateTime", openDateTime);
					} else {
						jsonValue.put("openDateTime", "");
					}
					
					if (incident.getResolutiondatetime() != null) {
						Date resolutionDate = incident.getResolutiondatetime();
						DateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");

						String resolutionDateTime = dateFormat
								.format(resolutionDate);

						jsonValue.put("resolutionDateTime", resolutionDateTime);
					} else {
						jsonValue.put("resolutionDateTime", "");
					}
					
					if (incident.getServiceid() != null) {
						jsonValue.put("serviceId", incident.getServiceid());
					} else {
						jsonValue.put("serviceId", "");
					}
					
					if (incident.getWorklognotes() != null) {
						jsonValue.put("bussinesDuration", incident.getWorklognotes());
					} else {
						jsonValue.put("bussinesDuration", "");
					}

					resultJsonArray.put(jsonValue);
				}

				com.liferay.portal.kernel.json.JSONObject jsonValue = JSONFactoryUtil
						.createJSONObject();

				jsonValue.put("totalCount", reccount.toString());
				resultJsonArray.put(jsonValue);

				log.info("Populated JSON:" + resultJsonArray);
				PortletUtils.setSessionAttribute(req, "statusGlobal", null);
				out.print(resultJsonArray.toString());

			}
		}

		catch (Exception e) {
			log.error(e.getMessage());
		}

	}

	@ActionMapping(value = "updateInc")
	public void updateInc(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		String incident = actionRequest.getParameter("incident");
		log.info("Recieved Id from overview to detail--" + incident);
		javax.xml.namespace.QName siteNameBean = new QName(
				"http://localhost:8080/updateInc", "Incident", "event");
		actionResponse.setEvent(siteNameBean, incident);
		String url = prop.getProperty("tt.homeURL") + "/"
				+ prop.getProperty("ttUpdateTicket");

		actionResponse.sendRedirect(url);
	}

	@EventMapping(value = "{http://localhost:8080/siteTicket}siteName")
	public void processEvent(EventRequest request, EventResponse response)
			throws PortletException, IOException {

		log.info("Event recieved and processed through IPC for sitename");

		javax.portlet.Event event = request.getEvent();
		String siteName = (String) event.getValue();
		siteNameGlobal = siteName;
		log.info("siteName--" + siteName);
		response.setRenderParameter("action", "list");
	}

	@EventMapping(value = "{http://localhost:8080/ticketHeader}headerTab")
	public void processEventStatus(EventRequest request, EventResponse response)
			throws PortletException, IOException {

		log.info("Event recieved and processed through IPC for incStatus");

		javax.portlet.Event event = request.getEvent();
		String status = (String) event.getValue();
		headerTab = status;
		log.info("status--" + status);
		PortletUtils.setSessionAttribute(request, "statusGlobal", status);

	}

	@EventMapping(value = "{http://localhost:8080/updateTicket}backTo")
	public void processEventStatusFromUpdateTicket(EventRequest request,
			EventResponse response) throws PortletException, IOException {

		log.info("Process event for status from updateTicket in SearchController");

		javax.portlet.Event event = request.getEvent();
		String status = (String) event.getValue();

		log.debug("status from updateTicket received in SearchController----"
				+ status);

		PortletUtils.setSessionAttribute(request, "statusGlobal", status);

	}

	@ResourceMapping(value = "getServiceURL")
	public void handleSericeIDFetch(Model model, ResourceRequest request,
			ResourceResponse response) throws Exception {
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		/* HttpServletResponse res= PortalUtil.getHttpServletResponse(response); */
		try {
			/* List<ServiceMap> serviceObjectList = new ArrayList<ServiceMap>(); */
			JSONObject jsonObj = new JSONObject(
					req.getParameter("selectedSiteIDJson"));
			String selectedSiteID = jsonObj.getString("selectedSiteID");
			if (selectedSiteID != null && selectedSiteID.trim().length() != 0) {
				log.info("Selected Site ID in controller : " + selectedSiteID);
				LinkedHashMap<String, String> serviceIDListtemp = commonManager
						.getServiceFromSiteID(selectedSiteID);
				log.info("Service Returned: " + serviceIDListtemp);
				String responseStr = "";
				if (serviceIDListtemp != null) {
					for (Map.Entry<String, String> entry : serviceIDListtemp
							.entrySet()) {
						responseStr += (entry.getKey() + "@@@@@"
								+ entry.getValue() + "#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	// Get Category based on the product type
	@ResourceMapping(value = "getCategoryURL")
	public void fetchCategory(Model model, ResourceRequest request,
			ResourceResponse response) throws Exception {
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		String responseStr = "";
		try {
			List<String> category = new ArrayList<String>();
			JSONObject jsonObj = new JSONObject(
					req.getParameter("selectedProductTypeJson"));
			String selectedProductType = jsonObj
					.getString("selectedProductType");
			System.out.println("selectedProductType" + selectedProductType);
			if (selectedProductType != null
					&& selectedProductType.trim().length() != 0) {
				if (selectedProductType.equalsIgnoreCase("MNS")) {
					category.add("MNS");
					category.add("Network");
				} else if (selectedProductType
						.equalsIgnoreCase("Private Cloud")) {
					category.add("Private Cloud Virtual Infrastructure");
					category.add("Private Physical Infrastructure");
					category.add("Private Cloud Network");
				}

				if (category != null) {
					responseStr = category.toString().replace("[", "")
							.replace("]", "");
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	// Get the vblocks
	@ResourceMapping(value = "getDataURL")
	public void getVBlock(Model model, ResourceResponse response)
			throws Exception {
		String responseStr = "";
		try {
			LinkedHashMap<String, String> vBlocks = commonManager.getVBlocks();
			if (vBlocks != null) {
				for (Map.Entry<String, String> entry : vBlocks.entrySet()) {
					responseStr += (entry.getKey() + "####" + entry.getValue() + "&&&&");
				}
				PrintWriter out = response.getWriter();
				out.print(responseStr);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@ResourceMapping(value = "getVMURL")
	public void getVM(Model model, ResourceRequest request,
			ResourceResponse response) throws Exception {
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		try {

			JSONObject jsonObj = new JSONObject(
					req.getParameter("selectedVBlockJson"));
			String selectedVBlock = jsonObj.getString("selectedVBlock");
			if (selectedVBlock != null && selectedVBlock.trim().length() != 0) {
				log.info("selected VBlock in controller : " + selectedVBlock);
				LinkedHashMap<String, String> vm = commonManager
						.getVM(selectedVBlock);

				String responseStr = "";
				if (vm != null) {
					for (Map.Entry<String, String> entry : vm.entrySet()) {
						responseStr += (entry.getKey() + "####"
								+ entry.getValue() + "&&&&");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
