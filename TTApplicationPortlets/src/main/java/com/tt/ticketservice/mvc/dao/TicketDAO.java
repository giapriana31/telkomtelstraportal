package com.tt.ticketservice.mvc.dao;

import java.sql.Date;
import java.util.List;

import com.tt.model.Incident;
import com.tt.ticketservice.mvc.model.IncidentResults;
import com.tt.ticketservice.mvc.model.SearchInputs;


public interface TicketDAO {
	public List<Incident> listTickets();
	public List<Incident> searchIncidents(String impact, String priority, String serviceName, String State, String siteName, Date dateFrom, Date dateTo, String assignedTo  );
	public IncidentResults searchIncidents( SearchInputs searchInputs,String status );
		
}
