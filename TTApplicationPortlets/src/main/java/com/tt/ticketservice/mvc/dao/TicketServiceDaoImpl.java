package com.tt.ticketservice.mvc.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.utils.PropertyReader;
import com.tt.constants.GenericConstants;
import com.tt.model.Configuration;
import com.tt.model.Incident;
import com.tt.model.VirtualMachine;
import com.tt.ticketservice.mvc.model.TicketUIPaginatedListForm;

@Repository(value = "ticketServiceDaoImpl")
@Transactional
public class TicketServiceDaoImpl {

    @Autowired
    private SessionFactory sessionFactory;
    public static Properties prop = PropertyReader.getProperties();
    private Session session = null;
    private Transaction tx = null;

    @SuppressWarnings("unchecked")
    public List<Incident> getcommonDBCall() {
	System.out.println("In commondiaoimpl");
	Session session = this.sessionFactory.openSession();
	List<Incident> personList = session.createQuery("from Incident").list();
	System.out.println("count..." + personList.size());
	session.close();
	return personList;
    }

	public List<Incident> getcustomDBCall(
			TicketUIPaginatedListForm paginatedList) {
		if (paginatedList == null) {
			System.out.println("aa gaya commondiaoimpl mei null");
			Session session = this.sessionFactory.openSession();
			List<Incident> personList = session.createQuery(
					"from Incident i where i.id<11)").list();
			System.out.println("count..." + personList.size());
			session.close();
			return personList;
		} else {
			System.out.println("aa gaya commondiaoimpl mei normal");
			Session session = this.sessionFactory.openSession();
			List<Incident> personList = session.createQuery(
					"from Incident i where i.id<11").list();
			System.out.println("count..." + personList.size());
			for (Incident incident : personList) {
				incident.setIncidentid(incident.getIncidentid());
			}
			session.close();
			return personList;
		}
	}

    public LinkedHashMap<String, String> getServiceFromSiteID(String siteName)
	    throws Exception {
	try {
	    session = sessionFactory.openSession();
	    tx = session.beginTransaction();
	    LinkedHashMap<String, String> serviceIdMap = new LinkedHashMap<String, String>();
	    StringBuilder query = new StringBuilder();
	    query.append("select c.ciid, c.ciname from Configuration c where c.siteid in (select siteid from Site where customersite=:siteNameFetched and status in ('")
		 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
		 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE)
		 .append(") and c.citype=:ciType and status in ('")
		 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
		 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
	    Query query1 = session.createQuery(query.toString()).setString("siteNameFetched",siteName).setString("ciType", prop.getProperty("ciType"));
	

	    for (Iterator it = query1.iterate(); it.hasNext();) {
		Object[] row = (Object[]) it.next();
		serviceIdMap.put(row[0] + "", row[1] + "");
	    }
	    tx.commit();
	    return serviceIdMap;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    if (session != null && session.isOpen()) {
		session.close();
	    }
	}
    }

    public LinkedHashMap<String, String> getSiteNameMap(String customeruniqueID)
	    throws Exception {
	try {
	    session = sessionFactory.openSession();
	    tx = session.beginTransaction();
	    LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
	    StringBuilder query = new StringBuilder();
		query.append("select s.customersite, s.siteid from Site s where s.customeruniqueid=:custId and status in ('")
			 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
			 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
	    Query query1 = session.createQuery(query.toString()).setString("custId", customeruniqueID);
	    for (Iterator it = query1.iterate(); it.hasNext();) {
		Object[] row = (Object[]) it.next();
		siteNameIDMap.put(row[0] + "", row[0] + "");
	    }
	    tx.commit();
	    return siteNameIDMap;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    if (session != null && session.isOpen()) {
		session.close();
	    }
	}

    }

    public LinkedHashMap<String, String> getAllServices() throws Exception {
	try {
	    session = sessionFactory.openSession();
	    tx = session.beginTransaction();
	    LinkedHashMap<String, String> serviceIdMap = new LinkedHashMap<String, String>();
	    /* System.out.println("1"); */
	    StringBuilder query = new StringBuilder();
		query.append("select c.ciid, c.ciname from Configuration c where c.citype=:ciType and status in ('")
			 .append(GenericConstants.STATUSCOMMISSIONING).append(GenericConstants.COMMAINSIDESINGLEQUOTES)
			 .append(GenericConstants.STATUSOPERATIONAL).append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
	    Query query1 = session.createQuery(query.toString()).setString("ciType", "Service");
	    for (Iterator it = query1.iterate(); it.hasNext();) {
		Object[] row = (Object[]) it.next();
		serviceIdMap.put(row[0] + "", row[1] + "");
	    }
	    tx.commit();
	    return serviceIdMap;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    if (session != null && session.isOpen()) {
		session.close();
	    }
	}
    }

    public String getSiteName(String siteName) throws Exception {
	try {
	    session = sessionFactory.openSession();
	    tx = session.beginTransaction();
	    StringBuilder hql = new StringBuilder();
	    hql.append("select customersite from Site sa ");
	    hql.append("where sa.siteid=:siteName and status in ('");
	    hql.append(GenericConstants.STATUSCOMMISSIONING);
	    hql.append(GenericConstants.COMMAINSIDESINGLEQUOTES);
	    hql.append(GenericConstants.STATUSOPERATIONAL);
	    hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);

	    Query query = sessionFactory.getCurrentSession().createQuery(
		    hql.toString());
	    query.setString("siteName", siteName);
	    if (query.list() != null && query.list().size() > 0) {
		return (String) query.list().get(0);
	    } else {
		return null;
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	    throw e;
	} finally {
	    if (session != null && session.isOpen()) {
		session.close();
	    }
	}

    }
    
    public LinkedHashMap<String, String> getVBlocks() {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			//List<String> vBlocks = new ArrayList<String>();
			LinkedHashMap<String, String> vBlocks = new LinkedHashMap<String, String>();
			Query query = sessionFactory.getCurrentSession().
					createQuery("select c.baseCIName,c.baseCIId from CIRelationship c where c.dependentCIId in(select ci.ciid from Configuration ci where ci.cmdblass =:cmdbClass AND ci.citype =:ciType) AND c.relationShip =:relationShip ");
			query.setString("cmdbClass", "PRIVATE_CLOUD_IAAS_SERVICE");
			query.setString("ciType", "Service");
			query.setString("relationShip", "Contains::Contained by");
			

			for (Iterator it = query.iterate(); it.hasNext();) {
				Object[] data = (Object[]) it.next();
				vBlocks.put(data[1]+"", data[0]+"");
			
			}
			tx.commit();
			return vBlocks;

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	public LinkedHashMap<String, String> getVM(String selectedVBlock) {
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
		
			StringBuilder queryBuilder = new StringBuilder();
			queryBuilder.append("SELECT distinct(cire.dependentciid), cire.dependentciname FROM cirelationship cire WHERE cire.dependentciclass = 'cmdb_ci_vmware_instance' and cire.baseciid IN (");
			queryBuilder.append("SELECT cir.dependentciid FROM cirelationship cir WHERE cir.baseciid =:pcbsciid ) ");

			LinkedHashMap<String, String> vm = new LinkedHashMap<String, String>();
			Query query = sessionFactory.getCurrentSession().
					createQuery(queryBuilder.toString());
			query.setString("dependentciid", selectedVBlock);
			
			

			for (Iterator it = query.iterate(); it.hasNext();) {
				Object[] data = (Object[]) it.next();
				vm.put(data[1]+"", data[0]+"");
			
			}
			tx.commit();
			return vm;	

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}
	

	public ArrayList<String> getAllServices(String customerId){
		ArrayList<String> services=null;
		Query query= sessionFactory.getCurrentSession().createSQLQuery("select distinct(rootproductid) from incident where customeruniqueid='"+customerId+"'");
		if(null!=query.list() && !(query.list().isEmpty())){
			services=(ArrayList<String>) query.list();
		}
		return services;
	}

	public List<Configuration> getVirtualMachineList(String customerUniqueID) {
		
		List<Configuration> configurations = new ArrayList<Configuration>();
		String hql = "select ciid, ciname from configurationitem where rootProductId = 'Private Cloud' and cmdbclass = 'VM Instance' and customeruniqueid ='"+customerUniqueID+"'";
		
		Query query = sessionFactory.openSession().createSQLQuery(hql);
		if(query.list()!=null && query.list().size()>0){
			for (Object[] config : (List<Object[]>)query.list()) {
				
				Configuration configuration = new Configuration();
				configuration.setCiid(config[0].toString());
				configuration.setCiname(config[1].toString());
				configurations.add(configuration);
			}
			
			return configurations;
		}
		
		return configurations;
	}
}
