package com.tt.knowledgebase.mvc.service.common;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.knowledgebase.mvc.dao.KnowledgeBaseDaoImpl;
import com.tt.knowledgebase.mvc.model.TMList;
import com.tt.knowledgebase.mvc.model.TMMasterList;
import com.tt.logging.Logger;
import com.tt.model.knowledgebase.KnowledgeBaseArtifactsList;
import com.tt.profileSetting.mvc.web.controller.ProfileSecurityController;
import com.tt.utils.PropertyReader;

@Service(value = "knowledgeBaseManager")
@Transactional
public class KnowledgeBaseManager {
    private final static Logger log = Logger.getLogger(ProfileSecurityController.class);

	private ArrayList<KnowledgeBaseArtifactsList> artifactsList = null;
	private KnowledgeBaseDaoImpl knowledgeBaseDaoImpl;
	private static HashMap<String, HashMap> docidToPath = null;

	@Autowired
	@Qualifier("knowledgeBaseDaoImpl")
	public void setKnowledgeBaseDaoImpl(
			KnowledgeBaseDaoImpl knowledgeBaseDaoImpl) {
		this.knowledgeBaseDaoImpl = knowledgeBaseDaoImpl;
	}

	public ArrayList<KnowledgeBaseArtifactsList> getArtifactsList() {
		 log.info("KnowledgeBaseManager-getArtifactsList()-start");
		KnowledgeBaseDaoImpl kmd = new KnowledgeBaseDaoImpl();
		artifactsList = knowledgeBaseDaoImpl.getArtifactsList();

		 log.info("KnowledgeBaseManager-getArtifactsList()-Artifacts List"+artifactsList == null ? "NULL" : artifactsList.toString());
		 log.info("KnowledgeBaseManager-getArtifactsList()-end");
		return artifactsList;
	}

	public boolean addDocument(String id, KnowledgeBaseArtifactsList artifactsList) {

		Boolean isSaved = knowledgeBaseDaoImpl.addDocument(id,artifactsList);

		return isSaved;
	}

	public ArrayList<String> getCategoryList() {

		ArrayList<String> categoryList = new ArrayList<String>();
		artifactsList = this.getArtifactsList();

		if (artifactsList !=null) {
			for (KnowledgeBaseArtifactsList knowledgeBaseArtifactsList : artifactsList) {

				if (!categoryList.contains(knowledgeBaseArtifactsList
						.getDocumentCategory()))

				{
					categoryList.add(knowledgeBaseArtifactsList
							.getDocumentCategory());
					

				}
			}
		}
		else{
			
			
		}
		 log.info("KnowledgeBaseManager-getCategoryList()-Category List"+categoryList == null ? "NULL" : categoryList.toString());

		return categoryList;
	}

	public HashMap<String, String> getDocumentList(String category) {

		HashMap<String, String> documentMap = new HashMap<String, String>();
		artifactsList = this.getArtifactsList();
		if (artifactsList !=null) {
			for (KnowledgeBaseArtifactsList knowledgeBaseArtifactsList : artifactsList) {
				if (category.equalsIgnoreCase(knowledgeBaseArtifactsList
						.getDocumentCategory())) {

					documentMap.put(knowledgeBaseArtifactsList.getDocumentId(),
							knowledgeBaseArtifactsList.getDocumentName());
				}
			}
		}
		 log.info("KnowledgeBaseManager-getDocumentList()-Document List"+documentMap == null ? "NULL" : documentMap.toString());

		return documentMap;
	}

	public boolean deleteDocument(String[] documentList) {
		PropertyReader prop = new PropertyReader();
		Properties properties = prop.getProperties();
	
		Boolean isDeleted = false;
		ArrayList<String> docList = knowledgeBaseDaoImpl
				.deleteDocument(documentList);
		 log.info("KnowledgeBaseManager-deleteDocument()-Document List to delete"+docList == null ? "NULL" : docList.toString());

		if (!docList.isEmpty()) {
			for (String string : docList) {

				File tempFile = new File(
						properties.getProperty("DocumentRepositoryPath")
								+ string);
				
				isDeleted = tempFile.delete();
			}
		}

		return isDeleted;

	}

	public ArrayList<TMList> getSortedCategoryList() {

		 log.info("KnowledgeBaseManager-getSortedCategoryList()-start");

		ArrayList<String> categoryList = null;
		docidToPath = new HashMap<String, HashMap>();
		ArrayList<TMList> tempTM = new ArrayList<TMList>();
		TMMasterList tmm = new TMMasterList();
		String tempdocId = null;
		categoryList = getCategoryList();


		if (categoryList !=null) {
			for (String category : categoryList) {

				ArrayList<HashMap<String, String>> tempCategoryListTM = new ArrayList<HashMap<String, String>>();
				TMList tm = new TMList();
				for (KnowledgeBaseArtifactsList knowledgeBaseArtifactsList : artifactsList) {

					if (category.equals(knowledgeBaseArtifactsList
							.getDocumentCategory())) {
						HashMap<String, String> tempDocHashMap = new HashMap<String, String>();
						tm.setCategoryNameTM(category);
						tempdocId = knowledgeBaseArtifactsList.getDocumentId();
						tempDocHashMap.put("DocumentId",
								knowledgeBaseArtifactsList.getDocumentId());
						tempDocHashMap.put("DocumentTitle",
								knowledgeBaseArtifactsList.getDocumentName());
				
						tempDocHashMap.put("DocumentPath",
								knowledgeBaseArtifactsList.getDocumentPath());
						tempDocHashMap.put("DocumentType",
								knowledgeBaseArtifactsList.getDocumentType());
						tempCategoryListTM.add(tempDocHashMap);

						docidToPath.put(tempdocId, tempDocHashMap);

					}

				}

				tm.setContent(tempCategoryListTM);
				tempTM.add(tm);
			

			}
		}
		tmm.setTMList(tempTM);
		 log.info("KnowledgeBaseManager-getSortedCategoryList()- Last List"+tmm.getTMList() == null ? "NULL" : tmm.getTMList().toString());
		 log.info("KnowledgeBaseManager-getSortedCategoryList()- Doc to id List"+docidToPath == null ? "NULL" : docidToPath.toString());
		 log.info("KnowledgeBaseManager-getSortedCategoryList()-end");

		
		return tempTM;

	}
		
	public String getNewDocumentId(KnowledgeBaseArtifactsList artifactsList){
		
		return knowledgeBaseDaoImpl.getNewDocumentId(artifactsList);
	}
	
	public HashMap getDocPathByDocId(String docId) {
		return docidToPath.get(docId);
	}
}
