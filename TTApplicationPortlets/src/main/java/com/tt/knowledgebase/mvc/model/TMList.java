package com.tt.knowledgebase.mvc.model;

import java.util.ArrayList;
import java.util.HashMap;


public class TMList {
	
private String categoryNameTM;
private ArrayList<HashMap<String,String>> content =new ArrayList<HashMap<String,String>>();
public String getCategoryNameTM() {
	return categoryNameTM;
}
public void setCategoryNameTM(String categoryNameTM) {
	this.categoryNameTM = categoryNameTM;
}
public ArrayList<HashMap<String, String>> getContent() {
	return content;
}
public void setContent(ArrayList<HashMap<String, String>> content) {
	this.content = content;
}



}
