package com.tt.knowledgebase.mvc.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tt.utils.PropertyReader;

@Component("fileReadKnowledge")
@Scope("singleton")
public class FileReadKnowledge {
	
	

	private static FileReadKnowledge instance = null;
	public FAQMasterList fqm = new FAQMasterList();
	
	
	

	protected FileReadKnowledge(){
		
		String currentLine;
		String page = "";
		try {
			Properties prop = PropertyReader.getProperties();
			System.out.println("path "+prop.getProperty("FAQFilePath"));
			BufferedReader br = new BufferedReader(new FileReader(prop.getProperty("FAQFilePath")));
		//	BufferedReader br = new BufferedReader(new FileReader("D:\\KnowledgeFile\\knowledgeContent.txt"));
			
			 ArrayList<FAQList> tempFAQ = new ArrayList<FAQList>();
			while ((currentLine = br.readLine()) != null) {
				
				
				if(currentLine.startsWith("Category:")){
					String[] temp=currentLine.split(":");
					FAQList fq =new FAQList();
					fq.setCategoryName(temp[1]);
					currentLine = br.readLine();
					 ArrayList<HashMap<String,String>> tempCategoryList =new ArrayList<HashMap<String,String>>();
					while(currentLine != null){
						if(currentLine.startsWith("Question:") || currentLine.startsWith("Answer:") ||currentLine.startsWith("Contents:")){
							String[] temp2=currentLine.split(":"); 
							HashMap<String, String> tempHash=new HashMap<String, String>();
							//System.out.println("Inside"+temp2[0]+" " + temp2[1]);
							tempHash.put(temp2[0], temp2[1]);
							
						//System.out.println("temHash"+tempHash);
							//fq.setKnowledgeContent(fq.getKnowledgeContent().add(tempHash));
							tempCategoryList.add(tempHash);
							currentLine = br.readLine();
						}else{
							
							break;
						}
						
					}
					
					fq.setKnowledgeContent(tempCategoryList);
					tempFAQ.add(fq);
					System.out.println(fq.getCategoryName());
					System.out.println(fq.getKnowledgeContent());
					
				}
			}
			/*String splitValues[]=page.split("Category:");
			for (int i = 0; i < splitValues.length; i++) {
				System.out.println(splitValues[i]);
			}*/
			fqm.setFAQList(tempFAQ);
			System.out.println(fqm.getFAQList());
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	/*public static void main(String[] args) {
		
		getInstance();
		//FAQMasterList fqm = new FAQMasterList();
		
			
	}*/

	public static FileReadKnowledge getInstance(){
		if(instance==null){
			
			instance=new FileReadKnowledge();
			
		}
		return instance;
	}
}
