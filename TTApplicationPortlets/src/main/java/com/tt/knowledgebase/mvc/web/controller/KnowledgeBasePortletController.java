package com.tt.knowledgebase.mvc.web.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.knowledgebase.mvc.model.FAQList;
/*import com.tt.knowledgebase.mvc.model.TMList;*/
import com.tt.knowledgebase.mvc.model.FileReadKnowledge;
import com.tt.knowledgebase.mvc.model.TMList;
import com.tt.knowledgebase.mvc.service.common.KnowledgeBaseManager;
import com.tt.logging.Logger;
import com.tt.model.knowledgebase.KnowledgeBaseArtifactsList;
import com.tt.profileSetting.mvc.web.controller.ProfileSecurityController;
import com.tt.utils.PropertyReader;

@Controller("knowledgeBasePortlet")
@RequestMapping("VIEW")
public class KnowledgeBasePortletController {
    private final static Logger log = Logger.getLogger(ProfileSecurityController.class);

	private KnowledgeBaseManager knowledgeBaseManager;
	private FileReadKnowledge fileReadKnowledge;
	
	
	@Autowired
	@Qualifier("knowledgeBaseManager")
	public void setKnowledgeBaseManager(
			KnowledgeBaseManager knowledgeBaseManager) {
		this.knowledgeBaseManager = knowledgeBaseManager;
	}
	
	
	@Autowired
	@Qualifier("fileReadKnowledge")
	public void setFileReadKnowledge(FileReadKnowledge fileReadKnowledge) {
		this.fileReadKnowledge = fileReadKnowledge;
	}



	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		
		

		log.info("KnowledgeBasePortletController-render()-Inside Render-Start");
		request.setAttribute("contentList", fileReadKnowledge.fqm.getFAQList());
		 log.debug( ("KnowledgeBasePortletController.java-render()-FAQ List " + fileReadKnowledge.fqm.getFAQList() == null ? "NULL" : fileReadKnowledge.fqm.getFAQList().toString()));
		
	
	

		ArrayList<TMList> tempListArtifacts = knowledgeBaseManager
				.getSortedCategoryList();
		 log.debug( ("KnowledgeBasePortletController.java-render()-Artifacts List " + tempListArtifacts == null ? "NULL" : tempListArtifacts.toString()));

			request.setAttribute("contentListTM", tempListArtifacts);

	
		
		ArrayList<String> categoryList= knowledgeBaseManager.getCategoryList();
		request.setAttribute("categoryList", categoryList);
		log.info("KnowledgeBasePortletController-render()-Inside Render-End");
		return "knowledgeBase";
	}

	@ActionMapping(value = "handleActionRequest")
	public void handleActionRequest(ActionRequest aRequest,
			ActionResponse aResponse, Model model) throws PortalException,
			SystemException {
		log.info("KnowledgeBasePortletController-handleActionRequest()");
		searchFAQ(aRequest, aResponse, model);

		searchTM(aRequest, aResponse, model);

	}

	@ActionMapping(value = "uploadDocument")
	public void uploadDocument(ActionRequest aRequest,
			ActionResponse aResponse, Model model) throws PortalException,
			SystemException {
		
		
		
		
		
		log.info("KnowledgeBasePortletController-uploadDocument()-Inside uploadDocument-Start");

		SessionErrors.add(aRequest, "error-key");
		SessionMessages.add(aRequest, PortalUtil.getPortletId(aRequest) + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);		
		HttpServletRequest request = PortalUtil.getHttpServletRequest(aRequest);
		
		
		
		  FileItemFactory factory = new DiskFileItemFactory();
		  ServletFileUpload upload = new ServletFileUpload( factory );
		  String documentName = null;
		  String documentCategory = null;
		  String documentType = null;
		  String filepath=null;
		  String tempCategory="";
		  List<FileItem> uploadItems = null;
		try {
			uploadItems = upload.parseRequest( request );
		} catch (FileUploadException e) {
			log.error("KnowledgeBasePortletController.java-uploadDocument()-Catch "+e.getMessage());

		}

		  for( FileItem uploadItem : uploadItems )
		  {
		    if( uploadItem.isFormField() )
		    {
		      String fieldName = uploadItem.getFieldName();
		      String value = uploadItem.getString();
		      
		      if(fieldName.equalsIgnoreCase("documentName")){
		    	  
		    	  

		    	  
		    	  
		    	  documentName=value;
		    	  log.info("KnowledgeBasePortletController-uploadDocument()-DocumentName"+documentName);
		    	 
		    	  
		      }
		      
		      if(fieldName.equalsIgnoreCase("addCategory")){
		    	  tempCategory=value;
		    	  
		    	  log.info("KnowledgeBasePortletController-uploadDocument()-Add category"+tempCategory);
		    	
		    	  
		      }
		      

		      if(fieldName.equalsIgnoreCase("categoryName")){
		    	  
		    	 
		    	  documentCategory=value;
		    	  log.info("KnowledgeBasePortletController-uploadDocument()-Selected category"+documentCategory);
		    	  
		    	  
		      }
		     
		    }
		  }
		  
		  

			ArrayList<TMList> tempListArtifacts = knowledgeBaseManager.getSortedCategoryList();
			boolean documentExist=false;
		  
			for(TMList list:tempListArtifacts){
				
				if(documentCategory.equals(list.getCategoryNameTM())){
					
					ArrayList<HashMap<String, String>> lHashMaps=list.getContent();
					
				for (HashMap<String, String> map:lHashMaps){
					
					
						if(map.get("DocumentTitle").equalsIgnoreCase(documentName)){
							documentExist=true;
							 SessionErrors.add(aRequest, "documentAlreadyExist");
						
					}
						
					}
					
					
				}
				
			}
		  
			
			if (!documentExist) {
				KnowledgeBaseArtifactsList artifactsList = new KnowledgeBaseArtifactsList();
				if (documentCategory.equalsIgnoreCase("Add Category")) {

					artifactsList.setDocumentCategory(tempCategory.trim());

				} else {

					artifactsList.setDocumentCategory(documentCategory.trim());
				}
				artifactsList.setDocumentName(documentName);
				String extension = "";
				Boolean isSaved = false;
				String id = knowledgeBaseManager
						.getNewDocumentId(artifactsList);
				if (id != null || id != "") {
					if (uploadItems != null && uploadItems.size() > 0) {

						// iterates over form's fields
						for (FileItem item : uploadItems) {

							// processes only fields that are not form fields
							if (!item.isFormField()) {
								String fileName = new File(item.getName())
										.getName();

								String tempExtension = fileName
										.substring(fileName.lastIndexOf(".") + 1);
								extension = tempExtension;

								log.info("KnowledgeBasePortletController-uploadDocument()-file name"
										+ fileName);
								documentType = item.getContentType();
								artifactsList.setDocumentPath(id.toString()
										+ "." + extension);
								artifactsList.setDocumentType(documentType);

								log.info("KnowledgeBasePortletController-uploadDocument()-Content Type"
										+ item.getContentType());

								Properties prop = PropertyReader
										.getProperties();
								String filePath = prop
										.getProperty("DocumentRepositoryPath")
										+ File.separator
										+ id.toString()
										+ "."
										+ tempExtension;
								log.info("KnowledgeBasePortletController-uploadDocument()-File Path"
										+ filepath);
								File storeFile = new File(filePath);

								// saves the file on disk
								try {
									item.write(storeFile);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									log.error("KnowledgeBasePortletController.java-uploadDocument()-Catch "+ e.getMessage());
									;
								}
							}
						}

					}

					isSaved = knowledgeBaseManager.addDocument(id,
							artifactsList);
				}
				if (isSaved) {
					SessionMessages.add(aRequest, "tmAddedSuccess");
				} else {

					SessionErrors.add(aRequest, "tmAddedFailed");
				}
				log.info("KnowledgeBasePortletController-uploadDocument()-Document Saved or not"
						+ isSaved);
			}

	

	}

	@ActionMapping(value = "deleteDocument")
	public void deleteDocument(ActionRequest aRequest,
			ActionResponse aResponse, Model model) throws PortalException,
			SystemException {
		
		 log.info("KnowledgeBasePortletController-deleteDocument()-Start");
		
		String documentToDelete[]=aRequest.getParameterValues("documentTODelete");
		 log.debug( ("KnowledgeBasePortletController-deleteDocument()-documents to delete List " + documentToDelete == null ? "NULL" : documentToDelete.toString()));

		
	
	
		Boolean isDeleted=knowledgeBaseManager.deleteDocument(documentToDelete);
		if(isDeleted){
			SessionMessages.add(aRequest, "tmDeletedSuccess");	
		}else{
			
			 SessionErrors.add(aRequest, "tmDeletedFailed");
		}
		
		 log.info("KnowledgeBasePortletController-deleteDocument()-Deleted or not "+isDeleted);
	}

	@ResourceMapping
	public void openKMContents(ResourceRequest rsRequest,
			ResourceResponse rsResponse, Model model) throws PortalException,
			SystemException {
		
		 log.info("KnowledgeBasePortletController-openKMContents()-start");
		String docid = rsRequest.getParameter("docid");
		 log.info("KnowledgeBasePortletController-openKMContents()-documentID to open "+docid);
		respondKMContents(docid, rsResponse);

		 log.info("KnowledgeBasePortletController-openKMContents()-END");
	}
	

	private void respondKMContents(String docid, ResourceResponse rsResponse) {

		// get instance of all KM Docs
		 log.info("KnowledgeBasePortletController-respondKMContents()-Start");
		if (null != knowledgeBaseManager)

		{
			log.info("KnowledgeBasePortletController-respondKMContents()-doc inst not available for id for "+docid);
		}

		// get docpath from docid

		HashMap<String, String> docDetails = knowledgeBaseManager
				.getDocPathByDocId(docid);
		
		 log.debug( ("KnowledgeBasePortletController-respondKMContents()-Doc hashmap " + docDetails == null ? "NULL" : docDetails.toString()));

		if (null != docDetails) {
			log.info("KnowledgeBasePortletController-respondKMContents()-doc details = "+ docDetails.get("DocumentType") + "path ="
					+ docDetails.get("DocumentPath"));
			
		} else {
			log.info("KnowledgeBasePortletController-respondKMContents()-doc details not available for id "+docid);

			return;
		}
		// output an HTML page
		
		
		 rsResponse.setContentType("text/html;charset=UTF-8");


			ServletOutputStream  outs =null;
	      
				try {
					 outs =  (ServletOutputStream) rsResponse.getPortletOutputStream ();
				} catch (IOException e) {
					log.error("KnowledgeBasePortletController.java-respondKMContents()-Catch "+e.getMessage());
				}
				
				Properties prop = PropertyReader.getProperties();
				File file=new File(prop.getProperty("DocumentRepositoryPath")
						+ docDetails.get("DocumentPath"));
				HttpServletResponse response=PortalUtil.getHttpServletResponse(rsResponse);

				response.setHeader("Content-disposition","inline; filename=" +docDetails.get("DocumentTitle")+"."+docDetails.get("DocumentPath").substring(docDetails.get("DocumentPath").lastIndexOf(".")+1) );

				
	       
		rsResponse.setContentType(docDetails.get("DocumentType"));
		
		BufferedInputStream  bis = null; 
		BufferedOutputStream bos = null;
		try {

		    InputStream isr=new FileInputStream(file);
		    bis = new BufferedInputStream(isr);
		    bos = new BufferedOutputStream(outs);
		    byte[] buff = new byte[2048];
		    int bytesRead;
		    // Simple read/write loop.
		    while(-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
		        bos.write(buff, 0, bytesRead);
		    }
		} 
		catch(Exception e)
		{
			log.error("KnowledgeBasePortletController.java-respondKMContents()-Catch "+e.getMessage());
		
		} finally {
		    if (bis != null)
				try {
					bis.close();
				} catch (IOException e) {
					log.error("KnowledgeBasePortletController.java-respondKMContents()-Catch "+e.getMessage());
				}
		    if (bos != null)
				try {
					bos.close();
				} catch (IOException e) {
					log.error("KnowledgeBasePortletController.java-respondKMContents()-Catch "+e.getMessage());
				}
		}


		 log.info("KnowledgeBasePortletController-openKMContents()-end");


	}

	public void searchFAQ(ActionRequest aRequest, ActionResponse aResponse,
			Model model) {
		/*
		 * Note: In the property file of KM at the end of every category
		 * name,question, and answer add an extra space character then save.
		 */
		// String searchTerm = "Telkom";
		 log.info("KnowledgeBasePortletController-searchFAQ()-start");

		String searchTerm = (ParamUtil.getString(aRequest, "searchTerm"))
				.trim();
		aRequest.setAttribute("searchTerm", searchTerm);
		 log.info("KnowledgeBasePortletController-searchFAQ()-Search term "+searchTerm);

		// get all FAQMasterList
		FileReadKnowledge fileReadKnowledge = FileReadKnowledge.getInstance();
		
		String checkCategory = "";
		// start iterating over FAQMasterList
		ArrayList<FAQList> masterList = new ArrayList<FAQList>();
		ArrayList<FAQList> afterSearchList = new ArrayList<FAQList>();
		masterList = fileReadKnowledge.fqm.getFAQList();
		ArrayList<String> searchResults = new ArrayList<String>();
		for (FAQList faqList : masterList) {
			FAQList tempFAQList = new FAQList();
			if (faqList.getCategoryName().toLowerCase()
					.contains(searchTerm.toLowerCase())) {
				
				
				String lowerCaseCat=faqList.getCategoryName().toLowerCase();
				String lowercaseSearchTerm=searchTerm.toLowerCase();
				
				ArrayList<Integer> indexes=new ArrayList<Integer>();
				for (int i = -1; (i = lowerCaseCat.indexOf(lowercaseSearchTerm.toLowerCase(), i + 1)) != -1; ) {
				
					indexes.add(i);
				
				}
				
				String afterSearch="";
				String orignal=faqList.getCategoryName();
				int startString=0;
				for (int i = 0; i < indexes.size(); i++) {
					if(i+1!=indexes.size()){
					afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>";
					startString=indexes.get(i)+lowercaseSearchTerm.length();
					}else{
						
						afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>"+orignal.substring(indexes.get(i)+lowercaseSearchTerm.length(),orignal.length());
					}
				}
				

				tempFAQList.setCategoryName(afterSearch);

				searchResults.add(afterSearch.toLowerCase()
						.replaceAll("\\s+", ""));
				 log.debug( ("KnowledgeBasePortletController-searchFAQ()-Search results " + searchResults == null ? "NULL" : searchResults.toString()));

				

			} else {
				tempFAQList.setCategoryName(faqList.getCategoryName());

			}

			ArrayList<HashMap<String, String>> tempList = new ArrayList<HashMap<String, String>>();
		
			 for (HashMap<String, String> map : faqList
					.getKnowledgeContent()) {
				HashMap<String, String> tempHashMap = new HashMap<String, String>();
				for (String key : map.keySet()) {
					if ((key.equalsIgnoreCase("Question")||(key.equalsIgnoreCase("Answer"))) && map.get(key).toLowerCase()
							.contains(searchTerm.toLowerCase())) {
						
						
						
						
						String contentTemp=map.get(key);
						String lowercaseContent=contentTemp.toLowerCase();
						String lowercaseSearchTerm=searchTerm.toLowerCase();

						ArrayList<Integer> indexes=new ArrayList<Integer>();
						for (int i = -1; (i = lowercaseContent.indexOf(lowercaseSearchTerm.toLowerCase(), i + 1)) != -1; ) {
						
							indexes.add(i);
						
						}
						String afterSearch="";
						String orignal=contentTemp;
						int startString=0;
						for (int i = 0; i < indexes.size(); i++) {
							if(i+1!=indexes.size()){
							afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>";
							startString=indexes.get(i)+lowercaseSearchTerm.length();
							}else{
								
								afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>"+orignal.substring(indexes.get(i)+lowercaseSearchTerm.length(),orignal.length());
							}
						}
				
						

						tempHashMap.put(key, afterSearch);

					
						searchResults.add(faqList.getCategoryName()
								.toLowerCase().replaceAll("\\s+", ""));
						
					} else {

						tempHashMap.put(key, map.get(key));
					}

				}
				tempList.add(tempHashMap);
			}

			tempFAQList.setKnowledgeContent(tempList);

			afterSearchList.add(tempFAQList);
		}

		// check for the search term in list name and contents in any one
		// child/categotry

		// add the category name in new hahmap with value whatever
	

		// set it for jsp
		 log.info("KnowledgeBasePortletController-searchFAQ()-Search results "+searchResults == null ? "NULL" : searchResults.toString());
		
		aRequest.setAttribute("searchResults", searchResults);

		if(!(searchResults.size()>0 && searchResults!=null)){
			aRequest.setAttribute("noResultsFAQ", "true");
			
		}else{
			aRequest.setAttribute("noResultsFAQ", "false");

			
		}
		

		aRequest.setAttribute("contentList", fileReadKnowledge.fqm.getFAQList());
		aRequest.setAttribute("contentListSearch", afterSearchList);
		 log.info("KnowledgeBasePortletController-searchFAQ()-End");
	}

	public void searchTM(ActionRequest aRequest, ActionResponse aResponse,
			Model model) {
		 log.info("KnowledgeBasePortletController-searchTM()-Start");


		String searchTerm = (ParamUtil.getString(aRequest, "searchTerm"))
				.trim();
		 log.info("KnowledgeBasePortletController-searchTM()-Search term "+searchTerm);
		aRequest.setAttribute("searchTerm", searchTerm);

		// get all FAQMasterList

		String checkCategory = "";
		// start iterating over FAQMasterList
		ArrayList<TMList> masterList = new ArrayList<TMList>();
		ArrayList<TMList> afterSearchList = new ArrayList<TMList>();
		masterList = knowledgeBaseManager.getSortedCategoryList();
		ArrayList<String> searchResults = new ArrayList<String>();
		for (TMList tmList : masterList) {
			TMList tempTMList = new TMList();
			if (tmList.getCategoryNameTM().toLowerCase()
					.contains(searchTerm.toLowerCase())) {
				String categoryTemp=tmList.getCategoryNameTM();
				String lowerCaseCat=categoryTemp.toLowerCase();
				String lowercaseSearchTerm=searchTerm.toLowerCase();
				
				ArrayList<Integer> indexes=new ArrayList<Integer>();
				for (int i = -1; (i = lowerCaseCat.indexOf(lowercaseSearchTerm.toLowerCase(), i + 1)) != -1; ) {
				
					indexes.add(i);
				
				}
				
				String afterSearch="";
				String orignal=categoryTemp;
				int startString=0;
				for (int i = 0; i < indexes.size(); i++) {
					if(i+1!=indexes.size()){
					afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>";
					startString=indexes.get(i)+lowercaseSearchTerm.length();
					}else{
						
						afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>"+orignal.substring(indexes.get(i)+lowercaseSearchTerm.length(),orignal.length());
					}
				}
				
				
				
				tempTMList.setCategoryNameTM(afterSearch);

			searchResults.add(afterSearch.toLowerCase().replaceAll("\\s+", ""));
				log.info("KnowledgeBasePortletController-searchTM()-Search Results "+searchResults == null ? "NULL" : searchResults.toString());


			} else {
				tempTMList.setCategoryNameTM(tmList.getCategoryNameTM());

			}

			ArrayList<HashMap<String, String>> tempList = new ArrayList<HashMap<String, String>>();
			
			 for (HashMap<String, String> map : tmList.getContent()) {
				HashMap<String, String> tempHashMap = new HashMap<String, String>();
				for (String key : map.keySet()) {
					log.info("KnowledgeBasePortletController-searchTM()-Map Key and Value "+"Key:"+key+"value: "+map.get(key));

					if (key.equalsIgnoreCase("DocumentTitle")&& map.get(key).toLowerCase()
							.contains(searchTerm.toLowerCase())) {
						
							String contentTemp=map.get(key);
							String lowercaseContent=contentTemp.toLowerCase();
							String lowercaseSearchTerm=searchTerm.toLowerCase();

							ArrayList<Integer> indexes=new ArrayList<Integer>();
							for (int i = -1; (i = lowercaseContent.indexOf(lowercaseSearchTerm.toLowerCase(), i + 1)) != -1; ) {
							
								indexes.add(i);
							
							}
							String afterSearch="";
							String orignal=contentTemp;
							int startString=0;
							for (int i = 0; i < indexes.size(); i++) {
								if(i+1!=indexes.size()){
								afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>";
								startString=indexes.get(i)+lowercaseSearchTerm.length();
								}else{
									
									afterSearch=afterSearch+orignal.substring(startString,indexes.get(i))+"<span style='background-color:yellow; white-space: pre;'>"+orignal.substring(indexes.get(i),indexes.get(i)+lowercaseSearchTerm.length())+"</span>"+orignal.substring(indexes.get(i)+lowercaseSearchTerm.length(),orignal.length());
								}
							}
					
								
						tempHashMap.put(key, afterSearch);

						if (!searchResults.contains(tmList.getCategoryNameTM()))
							if (!checkCategory.equalsIgnoreCase(tmList
									.getCategoryNameTM())) {
								searchResults.add(tmList.getCategoryNameTM()
										.toLowerCase().replaceAll("\\s+", ""));
							}
						
						searchResults.add(tmList.getCategoryNameTM()
								.toLowerCase().replaceAll("\\s+", ""));

					} else {

						tempHashMap.put(key, map.get(key));
					}

				}
				tempList.add(tempHashMap);
			}

			tempTMList.setContent(tempList);

			afterSearchList.add(tempTMList);
		}

		// check for the search term in list name and contents in any one
		// child/categotry

		// add the category name in new hahmap with value whatever

		// set it for jsp
		
		 log.info("KnowledgeBasePortletController-searchTM()-Search Results "+searchResults == null ? "NULL" : searchResults.toString());

		aRequest.setAttribute("searchResultsTM", searchResults);
		
		if(!(searchResults.size()>0 && searchResults!=null)){
			aRequest.setAttribute("noResultsTM", "true");
			
		}else{
			aRequest.setAttribute("noResultsTM", "false");

			
		}
		aRequest.setAttribute("contentListTM",
				knowledgeBaseManager.getSortedCategoryList());
		aRequest.setAttribute("contentListSearchTM", afterSearchList);
		 log.info("KnowledgeBasePortletController-searchTM()-END");

	}
}
