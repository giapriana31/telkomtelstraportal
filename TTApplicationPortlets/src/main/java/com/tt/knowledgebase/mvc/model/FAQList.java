package com.tt.knowledgebase.mvc.model;

import java.util.ArrayList;
import java.util.HashMap;


public class FAQList {
	
private String categoryName;
private ArrayList<HashMap<String,String>> knowledgeContent =new ArrayList<HashMap<String,String>>();


public String getCategoryName() {
	return categoryName;
}
public void setCategoryName(String categoryName) {
	this.categoryName = categoryName;
}
public ArrayList<HashMap<String, String>> getKnowledgeContent() {
	return knowledgeContent;
}
public void setKnowledgeContent(
		ArrayList<HashMap<String, String>> knowledgeContent) {
	this.knowledgeContent = knowledgeContent;
}

}
