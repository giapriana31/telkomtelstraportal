package com.tt.knowledgebase.mvc.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.model.knowledgebase.KnowledgeBaseArtifactsList;
import com.tt.profileSetting.mvc.web.controller.ProfileSecurityController;
import com.tt.utils.PropertyReader;

@Repository(value = "knowledgeBaseDaoImpl")
@Transactional
public class KnowledgeBaseDaoImpl {
    private final static Logger log = Logger.getLogger(ProfileSecurityController.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	

	public ArrayList<KnowledgeBaseArtifactsList> getArtifactsList() {
		System.out.println("Inside Dao");
		 log.info("KnowledgeBaseDaoImpl-getArtifactsList()-start");

		

		ArrayList<KnowledgeBaseArtifactsList> artifactsList = new ArrayList<KnowledgeBaseArtifactsList>();

		Query query = sessionFactory.openSession().createQuery("From KnowledgeBaseArtifactsList");
		 log.info("KnowledgeBaseDaoImpl-getArtifactsList()-Query List"+query.list() == null ? "NULL" : query.list().toString());
		if (query.list() != null && !query.list().isEmpty()) {

			return (ArrayList<KnowledgeBaseArtifactsList>) query.list();

		}
		 log.info("KnowledgeBaseDaoImpl-getArtifactsList()-end");

		return artifactsList;

	}
	
	
	public String getNewDocumentId(KnowledgeBaseArtifactsList artifactsList){
		
		Session session = sessionFactory.openSession();
		Transaction	tx = session.beginTransaction();
		
		
			String id=(String)session.save(artifactsList);
			 log.info("KnowledgeBaseDaoImpl-getNewDocumentId()-Doc id "+id);

	
			session.flush();
			tx.commit();
			return id;
	}
	
	public boolean addDocument(String id,KnowledgeBaseArtifactsList artifactsList){
		
		Session session = sessionFactory.openSession();
		Transaction	tx = session.beginTransaction();
			
			
		
		  
		 
		
		
		
		  KnowledgeBaseArtifactsList artifactsListUpdated=(KnowledgeBaseArtifactsList)session.load(KnowledgeBaseArtifactsList.class, id);
		  artifactsListUpdated.setDocumentType(artifactsList.getDocumentType());
		  artifactsListUpdated.setDocumentPath(artifactsList.getDocumentPath());
			id=(String)session.save(artifactsListUpdated);
		session.flush();
		tx.commit();
	
		if(id !=null)
		{
			return true;
		}
		
		return false;
		
	}
	
	
	public ArrayList<String> deleteDocument(String documentList[]){
		
		Session session = sessionFactory.openSession();
		Transaction	tx = session.beginTransaction();
		Boolean isDeleted=true;
		ArrayList<String> docList=new ArrayList<String>();

		for (String string : documentList) {
			
			KnowledgeBaseArtifactsList km = new KnowledgeBaseArtifactsList();
			
		

		
			km.setDocumentId(string);

		
			
			
			
			
			try {
				session.delete(km);
				
				
				Session session2 = sessionFactory.openSession();
				Transaction	tx2 = session2.beginTransaction();
					KnowledgeBaseArtifactsList km2 = new KnowledgeBaseArtifactsList();
					km2=(KnowledgeBaseArtifactsList)session2.load(KnowledgeBaseArtifactsList.class,string);
					docList.add(km2.getDocumentPath());
					
				
			} catch (HibernateException e) {
				 log.info("KnowledgeBaseDaoImpl-deleteDocument()-not deleted ");

				log.error("KnowledgeBaseDaoImpl.java-deleteDocument()-Catch "+e.getMessage());

				
			e.printStackTrace();
				isDeleted=false;
			}
		
		}
		
		
		session.flush();
		tx.commit();
		 log.info("KnowledgeBaseDaoImpl-deleteDocument()-docs deleted"+docList == null ? "NULL" : docList.toString());

		
		return  docList;
	}

}
