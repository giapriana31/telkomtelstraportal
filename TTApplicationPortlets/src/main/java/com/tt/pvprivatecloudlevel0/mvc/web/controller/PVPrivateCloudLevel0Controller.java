package com.tt.pvprivatecloudlevel0.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.pvprivatecloudlevel0.mvc.service.common.PVPrivateCloudLevel0Manager;
import com.tt.subscribedservices.mvc.web.controller.SubscribedServicesController;
import com.tt.util.RefreshUtil;
import com.tt.utils.TTGenericUtils;


@Controller("pvPrivateCloudLevel0Controller")
@RequestMapping("VIEW")
public class PVPrivateCloudLevel0Controller {
	
	
	
	private PVPrivateCloudLevel0Manager pvPrivateCloudLevel0Manager;
	private final static Logger log = Logger.getLogger(PVPrivateCloudLevel0Controller.class);

	
	
	
	@Autowired
	@Qualifier(value="pvPrivateCloudLevel0Manager")
	public void setPvPrivateCloudLevel0Manager(
			PVPrivateCloudLevel0Manager pvPrivateCloudLevel0Manager) {
		this.pvPrivateCloudLevel0Manager = pvPrivateCloudLevel0Manager;
	}





	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		
		
		log.info("PVPrivateCloudLevel0Controller-render-start");
		
		

		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		SubscribedService ss = new SubscribedService();
		int count = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_PRIVATECLOUD, customerId);
	//	int count = RefreshUtil.isSubscribedServices(GenericConstants.PRODUCTTYPE_PRIVATECLOUD, customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_PRIVATECLOUD);
			model.addAttribute("additionalClass", "pvCloud-container");
			return "unsubscribed_pvcloud_service";
		}
		else
		{
			//String customerId="TT001";;
			Map<String,Integer> graphDataMap=pvPrivateCloudLevel0Manager.getGraphData(customerId);
			

			Set<String> graphDataKeys=graphDataMap.keySet();
			
			for (String string : graphDataKeys) {
				
				
				
				log.info("PVPrivateCloudLevel0Controller-graph data-"+"Graphkey: "+string+"  Data:"+graphDataMap.get(string));
		
				
			}
			
			
			
			Map<String,Integer> colorToDeliveryStageMap= new HashMap<String, Integer>();
			colorToDeliveryStageMap.put("#89C35C", 6);
			colorToDeliveryStageMap.put("#FA8072", 5);
			colorToDeliveryStageMap.put("#00B0F0", 4);
			colorToDeliveryStageMap.put("#7030A0", 3);
			colorToDeliveryStageMap.put("#A40800", 2);
			colorToDeliveryStageMap.put("#000", 1);
			
			Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
			deliveryStageToColor.put(6, "#89C35C");
			deliveryStageToColor.put(5, "#FA8072");
			deliveryStageToColor.put(4, "#00B0F0");
			deliveryStageToColor.put(3, "#7030A0");
			deliveryStageToColor.put(2, "#A40800");
			deliveryStageToColor.put(1, "#000");
			

			Map<String, Integer> graphColorMap = new HashMap<String, Integer>();

		
			
			Gson gson = new Gson();
			List<String> colorList=new ArrayList<String>();
		
			
			
			colorList.add("#89C35C");
			colorList.add("#FA8072");
			colorList.add("#00B0F0");
			colorList.add("#7030A0");
			colorList.add("#A40800");
			colorList.add("#000");
			
		
		for (String string : colorList) {
			if(null != graphDataMap && !graphDataMap.keySet().contains(string)){
				graphDataMap.put(string, 0);
			}
			
			
			
		}
		
			String cloudGraphDataJson = gson.toJson(graphDataMap);
			
			String colorListJson=gson.toJson(colorList);
			ArrayList<String> graphBottom= new ArrayList<String>();
		//	HashMap<String, String> graphBottom=new HashMap<String, String>();
			
			
			graphBottom.add("Order Received-#000");
			graphBottom.add("Detailed Design-#A40800");
			graphBottom.add("Procurement-#7030A0");
			graphBottom.add("Delivery & Activation-#00B0F0");
			graphBottom.add("Monitoring-#FA8072");
			graphBottom.add("Operational-#89C35C");
			
		


			
			log.info("PVPrivateCloudLevel0Controller-render-Graphlist Json: "+cloudGraphDataJson);
			log.info("PVPrivateCloudLevel0Controller-render-colorlist Json: "+colorListJson);
			model.addAttribute("cloudGraphData", cloudGraphDataJson);
			
			model.addAttribute("graphColorList", colorListJson);
			model.addAttribute("graphBottomList", graphBottom);
			
			
			
					return "pvPrivateCloudLevel0";
			
		}
		
	}

}
