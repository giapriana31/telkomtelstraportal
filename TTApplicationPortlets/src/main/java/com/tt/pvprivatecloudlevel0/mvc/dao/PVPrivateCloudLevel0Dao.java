package com.tt.pvprivatecloudlevel0.mvc.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;



@Repository(value = "pvPrivateCloudLevel0Dao")
@Transactional
public class PVPrivateCloudLevel0Dao {
	
	
	private final static Logger log = Logger.getLogger(PVPrivateCloudLevel0Dao.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	
	
	public Map<String,Integer> getGraphData(String customerId){
		
		
		log.info("pvPrivateCloudLevel0Dao-getGraphData-start");

		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(6, "#89C35C");
		deliveryStageToColor.put(5, "#FA8072");
		deliveryStageToColor.put(4, "#00B0F0");
		deliveryStageToColor.put(3, "#7030A0");
		deliveryStageToColor.put(2, "#A40800");
		deliveryStageToColor.put(1, "#000");
		
		
		Map<String,Integer> graphData=new HashMap<String, Integer>();
		Query query= sessionFactory.getCurrentSession().createSQLQuery("Select distinct(delivery_stage),count(ciid) from configurationitem where customeruniqueid='"+customerId+"' and lower(rootProductId)=lower('Private Cloud') and lower(cmdbclass)=lower('vBLOCK') group by delivery_stage");
		
		log.info("pvPrivateCloudLevel0Dao-getGraphData-Query: "+query.getQueryString());

		
		if(null!=query.list() && query.list().size()>0){
			List<Object[]> data=query.list();
		
			for (Object[] objects : data) {
				
				if(null!=objects[0]){
				graphData.put(deliveryStageToColor.get(Integer.parseInt((String)objects[0])),((BigInteger)objects[1]).intValue());
				}
			}
			
			
			
			
		}
		
		log.info("pvPrivateCloudLevel0Dao-getGraphData-end");

		return graphData;
	}

}
