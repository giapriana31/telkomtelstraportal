package com.tt.pvprivatecloudlevel0.mvc.service.common;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvprivatecloudlevel0.mvc.dao.PVPrivateCloudLevel0Dao;




@Service(value = "pvPrivateCloudLevel0Manager")
@Transactional
public class PVPrivateCloudLevel0Manager {

	@Autowired
	@Qualifier(value="pvPrivateCloudLevel0Dao")
	private PVPrivateCloudLevel0Dao privateCloudLevel0Dao;
	private final static Logger log = Logger.getLogger(PVPrivateCloudLevel0Manager.class);
	
	
	
	public Map<String, Integer> getGraphData(String customerId){
		
		log.info("PVPrivateCloudLevel0Manager-getGraphData-start");

		
		return privateCloudLevel0Dao.getGraphData(customerId);
	}
}
