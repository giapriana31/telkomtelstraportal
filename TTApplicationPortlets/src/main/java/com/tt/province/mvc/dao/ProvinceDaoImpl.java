package com.tt.province.mvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tt.model.ProvinceModel;


@Repository(value="provinceDaoImpl")
public class ProvinceDaoImpl implements ProvinceDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<ProvinceModel> get() throws Exception {
		Session session = sessionFactory.getCurrentSession();
		List<ProvinceModel> result = session.createQuery("from ProvinceModel").list();
		return result;
	}

	@Override
	public void insert(ProvinceModel prov) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(prov);

	}

	@Override
	public void delete(ProvinceModel prov) throws Exception {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(prov);

	}

}
