package com.tt.province.mvc.dao;

import java.util.List;

import com.tt.model.ProvinceModel;



public interface ProvinceDao {

	public List<ProvinceModel> get() throws Exception;
	public void insert(ProvinceModel prov) throws Exception;
	public void delete(ProvinceModel prov) throws Exception;
}
