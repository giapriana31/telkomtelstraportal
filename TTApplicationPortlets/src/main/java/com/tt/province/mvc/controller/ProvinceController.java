package com.tt.province.mvc.controller;


import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.util.ParamUtil;
import com.tt.model.ProvinceModel;
import com.tt.province.mvc.service.ProvinceService;

@Controller("provinceController")
@RequestMapping("VIEW")
public class ProvinceController {
	private Log log = LogFactory.getLog(getClass());
	@Autowired
	private ProvinceService service;
	
	@RenderMapping
	public String list(Model model){
		List<ProvinceModel> listProvince = null;
		try {
			listProvince = this.service.get();

		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		model.addAttribute("listProvince", listProvince);
		return "province";
	}
	
	@ActionMapping(params = "action=add")
	public void addProvince(ActionRequest actionRequest, ActionResponse actionResponse, Model model, @ModelAttribute("province") ProvinceModel province, BindingResult result) throws Exception, PortletException{
		System.out.println("result"+ParamUtil.getString(actionRequest, "provincename"));
		System.out.println("province"+province.getProvincename());
		service.insert(province);
		
	}
}
