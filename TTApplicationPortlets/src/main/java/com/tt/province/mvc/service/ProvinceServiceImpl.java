package com.tt.province.mvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.ProvinceModel;
import com.tt.province.mvc.dao.ProvinceDao;

@Service
@Transactional
public class ProvinceServiceImpl implements ProvinceService {
	@Autowired
	private ProvinceDao dao;
	@Override
	public List<ProvinceModel> get() throws Exception {
		
		return this.dao.get();
	}

	@Override
	public void insert(ProvinceModel prov) throws Exception {
		this.dao.insert(prov);

	}

	@Override
	public void delete(ProvinceModel prov) throws Exception {
		this.dao.delete(prov);

	}

}
