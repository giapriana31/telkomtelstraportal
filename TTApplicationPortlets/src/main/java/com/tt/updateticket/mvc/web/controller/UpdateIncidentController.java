package com.tt.updateticket.mvc.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.util.PortletUtils;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.client.model.updateInc.InsertResponse;
import com.tt.client.service.incident.UpdateIncidentWSCall;
import com.tt.client.service.incident.viewIncidentWSCallService;
import com.tt.constants.TTPortalAppErrors;
import com.tt.exception.TTPortalAppException;
import com.tt.logging.Logger;
import com.tt.model.Configuration;
import com.tt.model.Incident;
import com.tt.model.Notes;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;


@Controller("updateIncidentController")
@RequestMapping("VIEW")
public class UpdateIncidentController {

	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger
			.getLogger(UpdateIncidentController.class);
	


	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws Exception {
		try {
				HttpServletRequest httpServletReq = PortalUtil.getHttpServletRequest(request);
				log.info("handleRenderRequest start....");
				log.debug("incidentModel in session ----"+ PortletUtils.getSessionAttribute(request, "incidentModel"));
				
				Incident incidentReceived = (Incident) PortletUtils.getSessionAttribute(request, "incidentModel");
				if (incidentReceived != null) {
					
					/*//bydefault isSaasProduct false
					String isSaasProduct = "false";
					//if rootProductId contains any SaaS product , isSaaSProduct true
					
					log.debug("Value of RootproductId from call service"+incidentReceived.getRootProductId());
					
					//List<String> saasProductsListBeforeIf = new ArrayList<String>(updateIncidentDaoImpl.saasProductList());
					//log.debug("Recieved saasProductsListBeforeIf from Product Type************"+saasProductsListBeforeIf);
					
					if(incidentReceived.getRootProductId() !=null && incidentReceived.getRootProductId().length()>0 && 
							"SaaS".equalsIgnoreCase(incidentReceived.getRootProductId())){
								isSaasProduct = "true";
					}
					model.addAttribute("isSaasProduct", isSaasProduct);*/
	
					model.addAttribute("incident", incidentReceived);
					log.info("Received incident is not null-- " + model.toString());
					System.out.println("************** Status " + incidentReceived.getIncidentstatus() + " " + incidentReceived.getIncidentid() + " " + incidentReceived.getPriority());
					String statusCheck = "active";
					if (null != incidentReceived.getIncidentstatus() && !incidentReceived.getIncidentstatus().isEmpty()) {
						if (incidentReceived.getIncidentstatus().equalsIgnoreCase("Completed") || incidentReceived.getIncidentstatus().equalsIgnoreCase("Cancelled")
								|| incidentReceived.getIncidentstatus().equalsIgnoreCase("Closed")) {
							statusCheck = "completed";
						}
					}
					model.addAttribute("checkStatusOfIncident", statusCheck);
				} else {
					log.debug("Not receiving from ticketlist page");
					if (null != httpServletReq.getAttribute("javax.servlet.forward.query_string")) {
						String incidentIdReceived = httpServletReq.getAttribute("javax.servlet.forward.query_string").toString();
						String incidentIdDecrypted = new String(DatatypeConverter.parseBase64Binary(incidentIdReceived));
						log.info("id received from not panel..." + incidentIdDecrypted);
	
						Incident incidentReq = new Incident();
						incidentReq.setIncidentid(incidentIdDecrypted);
						Incident incident = new Incident();
	
						RefreshUtil.setProxyObject();
						viewIncidentWSCallService viewIncidentWSCallService = new viewIncidentWSCallService();
						incident = viewIncidentWSCallService.viewIncident(incidentReq);
	
						String isSaasProduct = "false";
						//if rootProductId contains any SaaS product , isSaaSProduct true
						
						log.debug("Value of RootproductId from call service"+incident.getRootProductId());
						
						//List<String> saasProductsListBeforeIf = new ArrayList<String>(updateIncidentDaoImpl.saasProductList());
						//log.debug("Recieved saasProductsListBeforeIf from Product Type************"+saasProductsListBeforeIf);
						
						if(incident.getRootProductId() !=null && incident.getRootProductId().length()>0 && 
								"SaaS".equalsIgnoreCase(incident.getRootProductId())){
									isSaasProduct = "true";
						}
						model.addAttribute("isSaasProduct", isSaasProduct);
						
						log.info("This is here for the Update...");
						String affectedResourceStr = "";
						log.info("incident.getAffectedResource()rettt::"
								+ incident.getAffectedResource());
						if (incident != null && incident.getAffectedResource() != null) {
							for (Configuration configuration : incident.getAffectedResource()) {
								log.debug("Configuration.getCiid()::::" + configuration.getCiid());
								if (configuration.getCiid() != null && !configuration.getCiid().equals("")) {
									affectedResourceStr = configuration.getCiid() + ", ";
								}
								log.info("affectedResourceStr ret 1:::" + affectedResourceStr);
							}
							log.info("This is affectedResource................. in UPDATE tkt");
	
							if (!affectedResourceStr.equals("")) {
								affectedResourceStr = affectedResourceStr.substring(0, affectedResourceStr.length() - 2);
								log.info("affectedResourceStr ret2:::" + affectedResourceStr);
								incident.setAffectedResourceString(affectedResourceStr);
							}
						}
	
						log.info("affectedResourceStr ret:::" + affectedResourceStr);
	
						String affectedServiceStr = "";
						log.info("incident.getAffectedService()ret::" + incident.getAffectedService());
						if (incident != null
								& incident.getAffectedService() != null) {
							for (Configuration ciIncidentMapping : incident.getAffectedService()) {
								if (ciIncidentMapping.getCiid() != null && !ciIncidentMapping.getCiid().equals("")) {
									affectedServiceStr = affectedServiceStr + ciIncidentMapping.getCiid() + ", ";
								}
							}
	
							if (!affectedServiceStr.equals("")) {
								affectedServiceStr = affectedServiceStr.substring(0, affectedServiceStr.length() - 2);
								incident.setAffectedServiceString(affectedServiceStr);
							}
						}
	
						log.info("affectedServiceStr ret::" + affectedServiceStr);
						if (incident.getMIM() == null || (incident.getMIM() != null && incident.getMIM().length() < 0))
							incident.setMIM("");
						if (incident.getCloseddate() == null) {
							incident.setCloseddate(null);
						}
						
	                    //formatting of comments
						List<Notes> formattedNoteList = new ArrayList<Notes>();
						if (incident.getComments() != null && incident.getComments().size() != 0) {
							
							for (Notes note : incident.getComments()) {
								Notes formattedNotes = new Notes();
								
								StringBuffer capturingFormattingNotes = new StringBuffer();
								capturingFormattingNotes.append("<pre>"+note.getNote()+"</pre>");
								
								/*String newFormatNote= capturingFormattingNotes.toString();*/
								
								/*String[] newFormatNoteArray=null;
								String  newFormatNotes=newFormatNote;
								 if(newFormatNote.contains(")")){
									 newFormatNoteArray=newFormatNote.split(")");
									 newFormatNotes = newFormatNoteArray[1];
								 }*/
						
								formattedNotes.setId(note.getId());
								formattedNotes.setIncident(note.getIncident());
								formattedNotes.setName(note.getName());
								formattedNotes.setNote(capturingFormattingNotes.toString());
								formattedNotes.setTime(note.getTime());
								
								formattedNoteList.add(formattedNotes);
							}
							incident.setComments(formattedNoteList);
							
							Collections.sort(incident.getComments(), new Datecompare());
						}
	
						log.info("Incident---------------->>>>>>>>>" + incident);
	
						String statusCheck = "active";
						if (incident.getIncidentstatus().equalsIgnoreCase("Completed") || incident.getIncidentstatus().equalsIgnoreCase("Cancelled")
								|| incident.getIncidentstatus().equalsIgnoreCase("Closed")) {
							statusCheck = "completed";
						}
						model.addAttribute("checkStatusOfIncident", statusCheck);
						model.addAttribute("incident", incident);
					}
	
					else {
						model.addAttribute("checkStatusOfIncident", "active");
					}
	
				}
	
				log.info("handleRenderRequest end....");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR in handleRenderRequest..." + e.getMessage());
			throw new TTPortalAppException(e, "SS:1111",
					TTPortalAppErrors.getErrorMessageByCode("SS:1111"));

		}
		return "updateTicket";
	}

	@ActionMapping(value = "incidentStatusSender")
	public void incidentTypeSelector(ActionRequest actionRequest,
			ActionResponse actionResponse, Model model) throws IOException,
			PortletException {

		log.info("Action control for incidentStatusSender");
		String status = actionRequest.getParameter("status");
		log.debug("status from updateTicket----" + status);

		javax.xml.namespace.QName statusBean = new QName(
				"http://localhost:8080/updateTicket", "backTo", "event");
		actionResponse.setEvent(statusBean, status);

		String url = prop.getProperty("tt.homeURL") + "/"
				+ prop.getProperty("ttIncidentPageName");
		actionResponse.sendRedirect(url);
	}

	@ActionMapping(value = "actionMethod1")
	public void actionMethod1(ActionRequest request, ActionResponse response)
			throws TTPortalAppException {

		try {
			log.info("actionMethod1 start....");
			ThemeDisplay td = (ThemeDisplay) request
					.getAttribute(WebKeys.THEME_DISPLAY);
			User user = UserLocalServiceUtil.getUserByEmailAddress(
					td.getCompanyId(), td.getUser().getDisplayEmailAddress());

			RefreshUtil.setProxyObject();

			String incidentID = ParamUtil.get(request, "incidentID_ticket", "");
			String userid = user.getEmailAddress() + "";
			// ParamUtil.get(request, "userid_ticket", "");
			String username = ParamUtil.get(request, "name_ticket", "");
			String note = ParamUtil.get(request, "note_ticket", "");
			log.info("note---------" + note + "incidentID-------" + incidentID);

			StringBuffer noteFormatted = new StringBuffer();
			noteFormatted.append("<pre>/"+note+"/</pre>");
		    String noteFormattedString = noteFormatted.toString();
		    String[] formattedNote = noteFormattedString.split("/");
		    
		    String formattedComment= formattedNote[1];
              
		    System.out.println("note---------------------------"+note);
			System.out.println("noteFormattedString----++++"
					+ noteFormattedString);
			log.info("noteFormattedString---------" + noteFormattedString
					+ "incidentID-------" + incidentID);

			Incident incident = new Incident();
			incident.setUserid(userid);
			incident.setIncidentid(incidentID);
			incident.setWorklognotes(formattedComment);
			log.debug("incidentID sent for Update-------"
					+ incident.getIncidentid());
			UpdateIncidentWSCall updateIncidentWSCall = new UpdateIncidentWSCall();
			InsertResponse wsCallResponse = updateIncidentWSCall
					.updateIncident(incident);
			log.debug("wsCallResponse.getErrorMessage()------"
					+ wsCallResponse.getErrorMessage());
			log.debug("wsCallResponse.dis()------"
					+ wsCallResponse.getDisplayValue());

			Incident inci1 = new Incident();

			inci1.setIncidentid(wsCallResponse.getDisplayValue());

			viewIncidentWSCallService viewIncidentWSCallService = new viewIncidentWSCallService();
			incident = viewIncidentWSCallService.viewIncident(inci1);

			String affectedResourceStr = "";
			log.info("incident.getAffectedResource():::"
					+ incident.getAffectedResource());
			if (incident != null & incident.getAffectedResource() != null) {
				for (Configuration ciIncidentMapping : incident
						.getAffectedResource()) {
					affectedResourceStr = ciIncidentMapping.getCiid() + ", ";
				}
				if (!affectedResourceStr.equals("")) {
					affectedResourceStr = affectedResourceStr.substring(0,
							affectedResourceStr.length() - 2);
					incident.setAffectedResourceString(affectedResourceStr);
				}
			}
			log.info("affectedResourceStr::" + affectedResourceStr);

			String affectedServiceStr = "";
			log.info("incident.getAffectedService():::"
					+ incident.getAffectedService());
			if (incident != null & incident.getAffectedService() != null) {
				for (Configuration ciIncidentMapping : incident
						.getAffectedService()) {
					affectedServiceStr = ciIncidentMapping.getCiid() + ", ";
				}
				if (!affectedServiceStr.equals("")) {
					affectedServiceStr = affectedServiceStr.substring(0,
							affectedServiceStr.length() - 2);
					incident.setAffectedServiceString(affectedServiceStr);
				}
			}
			log.info("affectedServiceStr::" + affectedServiceStr);

			if (incident.getMIM() == null
					|| (incident.getMIM() != null && incident.getMIM().length() < 0))
				incident.setMIM("");
			if (incident.getCloseddate() == null) {
				incident.setCloseddate(null);
			}

			//formatting of comments
			List<Notes> formattedNoteList = new ArrayList<Notes>();
			if (incident.getComments() != null && incident.getComments().size() != 0) {
				
				for (Notes notes : incident.getComments()) {
					Notes formattedNotes = new Notes();
					
					StringBuffer capturingFormattingNotes = new StringBuffer();
					capturingFormattingNotes.append("<pre>"+notes.getNote()+"</pre>");
					
					/*String newFormatNote= capturingFormattingNotes.toString();*/
					
					/*String[] newFormatNoteArray=null;
					String  newFormatNotes=newFormatNote;
					 if(newFormatNote.contains(")")){
						 newFormatNoteArray=newFormatNote.split(")");
						 newFormatNotes = newFormatNoteArray[1];
					 }*/
			
					          
					  
			
					formattedNotes.setId(notes.getId());
					formattedNotes.setIncident(notes.getIncident());
					formattedNotes.setName(notes.getName());
					formattedNotes.setNote(capturingFormattingNotes.toString());
					formattedNotes.setTime(notes.getTime());
					
					formattedNoteList.add(formattedNotes);
				}
				incident.setComments(formattedNoteList);
				
				Collections.sort(incident.getComments(), new Datecompare());
			}
			
			
			incident.setUserid(userid);
			log.debug("Incident after Update---------------->>>>>>>>>"
					+ incident);

			PortletUtils
					.setSessionAttribute(request, "incidentModel", incident);

			response.setRenderParameter("action", "list");
			log.info("actionMethod1 end....");
		} catch (Exception e) {
			// e.printStackTrace();
			throw new TTPortalAppException(e, "SS:1111",
					TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}
	}

	@EventMapping(value = "{http://localhost:8080/updateInc}Incident")
	public void processEvent(EventRequest request, EventResponse response,
			Model model) throws PortletException, IOException, Exception {
		try {

			log.info("**************************************************************PROCESSEVENT*******clicked on Tkt*********************************************");

			javax.portlet.Event event = request.getEvent();
			String incidentid = (String) event.getValue();
			log.info("Incident----" + incidentid);
			Incident incidentReq = new Incident();
			incidentReq.setIncidentid(incidentid);
			Incident incident = new Incident();

			RefreshUtil.setProxyObject();
			viewIncidentWSCallService viewIncidentWSCallService = new viewIncidentWSCallService();
			incident = viewIncidentWSCallService.viewIncident(incidentReq);

			log.info("This is here for the Update...");
			String affectedResourceStr = "";
			log.info("incident.getAffectedResource()rettt::"
					+ incident.getAffectedResource());
			if (incident != null && incident.getAffectedResource() != null) {
				for (Configuration configuration : incident
						.getAffectedResource()) {
					log.debug("Configuration.getCiid()::::"
							+ configuration.getCiid());
					if (configuration.getCiid() != null
							&& !configuration.getCiid().equals("")) {
						affectedResourceStr = configuration.getCiid() + ", ";
					}
					log.info("affectedResourceStr ret 1:::"
							+ affectedResourceStr);
				}
				log.info("This is affectedResource................. in UPDATE tkt");

				if (!affectedResourceStr.equals("")) {
					affectedResourceStr = affectedResourceStr.substring(0,
							affectedResourceStr.length() - 2);
					log.info("affectedResourceStr ret2:::"
							+ affectedResourceStr);
					incident.setAffectedResourceString(affectedResourceStr);
				}
			}

			log.info("affectedResourceStr ret:::" + affectedResourceStr);

			String affectedServiceStr = "";
			log.info("incident.getAffectedService()ret::"
					+ incident.getAffectedService());
			if (incident != null & incident.getAffectedService() != null) {
				for (Configuration ciIncidentMapping : incident
						.getAffectedService()) {
					if (ciIncidentMapping.getCiid() != null
							&& !ciIncidentMapping.getCiid().equals("")) {
						affectedServiceStr = affectedServiceStr
								+ ciIncidentMapping.getCiid() + ", ";
					}
				}

				if (!affectedServiceStr.equals("")) {
					affectedServiceStr = affectedServiceStr.substring(0,
							affectedServiceStr.length() - 2);

					incident.setAffectedServiceString(affectedServiceStr);
				}
			}

			log.info("affectedServiceStr ret::" + affectedServiceStr);
			if (incident.getMIM() == null
					|| (incident.getMIM() != null && incident.getMIM().length() < 0))
				incident.setMIM("");
			if (incident.getCloseddate() == null) {
				incident.setCloseddate(null);
			}

			//formatting of comments
			List<Notes> formattedNoteList = new ArrayList<Notes>();
			if (incident.getComments() != null && incident.getComments().size() != 0) {
				
				for (Notes note : incident.getComments()) {
					Notes formattedNotes = new Notes();
					
					StringBuffer capturingFormattingNotes = new StringBuffer();
					capturingFormattingNotes.append("<pre>"+note.getNote()+"</pre>");
					
					/*String newFormatNote= capturingFormattingNotes.toString();*/
					
					/*String[] newFormatNoteArray=null;
					String  newFormatNotes=newFormatNote;
					 if(newFormatNote.contains(")")){
						 newFormatNoteArray=newFormatNote.split(")");
						 newFormatNotes = newFormatNoteArray[1];
					 }*/
			
					
			
					formattedNotes.setId(note.getId());
					formattedNotes.setIncident(note.getIncident());
					formattedNotes.setName(note.getName());
					formattedNotes.setNote(capturingFormattingNotes.toString());
					formattedNotes.setTime(note.getTime());
					
					formattedNoteList.add(formattedNotes);
				}
				incident.setComments(formattedNoteList);
				
				Collections.sort(incident.getComments(), new Datecompare());
			}

			log.info("Incident---------------->>>>>>>>>" + incident);
			PortletUtils
					.setSessionAttribute(request, "incidentModel", incident);
		} catch (Exception e) {
			e.printStackTrace();
			throw new TTPortalAppException(e, "SS:1111",
					TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
		}
	}

	@RenderMapping(params = "value=update")
	public String updateRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws Exception {

		log.info("updateRenderRequest");
		return "updateTicket";

	}
}

class Datecompare implements Comparator<Notes> {
	@Override
	public int compare(Notes o1, Notes o2) {

		return (o1.getTime()).compareTo(o2.getTime()) * -1;
	}
}