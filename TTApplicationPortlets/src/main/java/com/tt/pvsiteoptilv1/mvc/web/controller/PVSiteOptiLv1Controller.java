package com.tt.pvsiteoptilv1.mvc.web.controller;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.pvnetwork.mvc.web.controller.PVNetworkController;
import com.tt.utils.TTGenericUtils;

@Controller("pvSiteOptiLv1Controller")
@RequestMapping("VIEW")
public class PVSiteOptiLv1Controller {
	
	private final static Logger log = Logger.getLogger(PVNetworkController.class);
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		
		log.info("PVSiteOptiLv0Controller-render-start");

		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		
		return "pvSiteOptiLv1";
	}
	
	

}
