package com.tt.pvsitelv0.mvc.service.common;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;
import com.tt.pvsitelv0.mvc.dao.PVSiteLv0Dao;
import com.tt.pvsitelv1.mvc.dao.PVSiteLv1Dao;

@Service(value = "pvSiteLv0ManagerImpl")
@Transactional
public class PVSiteLv0ManagerImpl {
	
	private final static Logger log = Logger.getLogger(PVSiteLv0ManagerImpl.class);
	private PVSiteLv0Dao pvSiteLv0Dao;
	
	@Autowired
	@Qualifier("pvSiteLv0Dao")
	public void setPVSiteLv0Dao(PVSiteLv0Dao pvSiteLv0Dao){
		this.pvSiteLv0Dao = pvSiteLv0Dao;
		
	}
	
	
	public Map<String,Integer> getGraphDetails(String customerId) {
		log.debug("PVSiteLv0Manager * getGraphDetails * Inside the method");

		return pvSiteLv0Dao.getGraphDetails(customerId);
	}

}
