package com.tt.pvsitelv0.mvc.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logging.Logger;

@Repository(value = "pvSiteLv0Dao")
@Transactional
public class PVSiteLv0Dao {

	private final static Logger log = Logger.getLogger(PVSiteLv0Dao.class);

	@Autowired
	private SessionFactory sessionFactory;
	public Map<String,Integer> getGraphDetails(String customerId) {
		
		Map<Integer,String> deliveryStageToColor= new HashMap<Integer, String>();
		deliveryStageToColor.put(6, "#89C35C");
		deliveryStageToColor.put(5, "#FA8072");
		deliveryStageToColor.put(4, "#00B0F0");
		deliveryStageToColor.put(3, "#7030A0");
		deliveryStageToColor.put(2, "#A40800");
		deliveryStageToColor.put(1, "#000000");
		
		
		Map<String,Integer> graphData=new HashMap<String, Integer>();
		
		//FOLLOW LOGIC LIKE sitehealthlevel0dao
		StringBuffer sb = new StringBuffer();
		//delivery stage , siteid
		
		sb.append(" select ci.delivery_stage, s.siteid ");
		sb.append(" from site s left join ");
		sb.append(" configurationitem ci ");
		sb.append(" on s.siteid = ci.siteid ");
		sb.append(" where s.customeruniqueid = '"+customerId+"'  and ci.delivery_stage is not null ");
		sb.append(" and ci.delivery_stage <> '' and ci.productclassification is not null and ci.productclassification <> '' ");
		sb.append(" group by s.siteid, ci.delivery_stage ");
		sb.append(" order by ci.delivery_stage, s.siteid ASC; ");
		Query innerQuery= sessionFactory.getCurrentSession().createSQLQuery(sb.toString());

		List<Object[]> productStatusList= removeHigerDeliveryStageOnSite(innerQuery.list());
				
				if(null!=productStatusList && productStatusList.size()>0){
					for (Object[] productData : productStatusList) {
						String productDeliveryStage=(String)productData[0];
						if(null!=productDeliveryStage && productDeliveryStage.length()>0){
							
							String deliveryColor=deliveryStageToColor.get(Integer.parseInt(productDeliveryStage));
							if(graphData.size()>0){
								
								Set<String> mapKeySet=graphData.keySet();
								
								if(mapKeySet.contains(deliveryColor)){
									
									int currentCount=graphData.get(deliveryColor);
									graphData.put(deliveryColor, currentCount+1);
								}else{
									
									graphData.put(deliveryColor, 1);
								}
								
							}else{
								
								graphData.put(deliveryColor, 1);	
							}
						}
					}
				}
				
		return graphData;
	}
	
	private List<Object[]> removeHigerDeliveryStageOnSite(List<Object[]> siteLeftJoinCIResultList) {
		List <Object[]> newSiteLeftJoinCIResultList = new ArrayList();
		String siteIdBlackList="|";
		if(siteLeftJoinCIResultList !=null && siteLeftJoinCIResultList.size() > 0) {
			for (Object[] productData : siteLeftJoinCIResultList) {
				if(siteIdBlackList.equals("|")) {
					newSiteLeftJoinCIResultList.add(productData);
					siteIdBlackList = siteIdBlackList + productData[1] + "|";
				}else if(siteIdBlackList.contains(String.valueOf(productData[1]))) {
					siteIdBlackList = siteIdBlackList + productData[1] + "|";
				}else {
					newSiteLeftJoinCIResultList.add(productData);
				}
			}
		}
		return newSiteLeftJoinCIResultList;
	}
}
