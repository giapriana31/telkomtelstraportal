package com.tt.pvsitelv0.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.pvsitelv0.mvc.service.common.PVSiteLv0ManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("pvSiteLv0Controller")
@RequestMapping("VIEW")
public class PVSiteLv0Controller {
	
	private final static Logger log = Logger.getLogger(PVSiteLv0Controller.class);
	
	private PVSiteLv0ManagerImpl pvSiteLv0ManagerImpl;
	
	@Autowired
	@Qualifier("pvSiteLv0ManagerImpl")
	public void setPVSiteLv0ManagerImpl(PVSiteLv0ManagerImpl pvSiteLv0ManagerImpl){
		this.pvSiteLv0ManagerImpl = pvSiteLv0ManagerImpl;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		
		log.info("PVSiteLv0Controller-render-start");

		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);
		
		SubscribedService ss = new SubscribedService();

		int serviceCount = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_MNS, customerId);

		if(serviceCount == 0)
		{
			model.addAttribute(GenericConstants.TITLE, "SITES");
			return "unsubscribed_pvsites_service";
		}
		else
		{
			Map<String,Integer> graphDataMap=pvSiteLv0ManagerImpl.getGraphDetails(customerId);
			
			Set<String> graphDataKeys=graphDataMap.keySet();			
		
			Map<String, Integer> graphColorMap = new HashMap<String, Integer>();

			Gson gson = new Gson();
			List<String> colorList=new ArrayList<String>();
			
			colorList.add("#89C35C");
			colorList.add("#FA8072");
			colorList.add("#00B0F0");
			colorList.add("#7030A0");
			colorList.add("#A40800");
			colorList.add("#000000");

		
			for (String string : colorList) 
			{
				if(null != graphDataMap && !graphDataMap.keySet().contains(string))
				{
					graphDataMap.put(string, 0);
				}	
			}
		
			String siteGraphDataJson = gson.toJson(graphDataMap);
			log.info("siteGraphDataJson -----"+siteGraphDataJson);
			
			String colorListJson=gson.toJson(colorList);
			ArrayList<String> graphBottom= new ArrayList<String>();
			graphBottom.add("#89C35C");
			graphBottom.add("#FA8072");
			graphBottom.add("#00B0F0");
			graphBottom.add("#7030A0");
			graphBottom.add("#A40800");
			graphBottom.add("#000000");
			
			model.addAttribute("pvSiteLv0StatusGraphData", siteGraphDataJson);
			model.addAttribute("pvGraphColorLv0ListSiteStatus", colorListJson);
			model.addAttribute("pvGraphBottomLv0ListSiteStatus", graphBottom);
		
	    	return "pvSiteLv0";
		}
	}
}
