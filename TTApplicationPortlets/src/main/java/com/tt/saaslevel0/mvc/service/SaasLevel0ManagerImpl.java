package com.tt.saaslevel0.mvc.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.saaslevel0.mvc.dao.SaasLevel0DaoImpl;


@Service(value = "saasLevel0Manager")
@Transactional
public class SaasLevel0ManagerImpl {
	
	private SaasLevel0DaoImpl saasLevel0DaoImpl;

	@Autowired
	@Qualifier("saasLevel0DaoImpl")
	public void setSaasLevel0DaoImpl(SaasLevel0DaoImpl saasLevel0DaoImpl) {
		this.saasLevel0DaoImpl = saasLevel0DaoImpl;
	}
	
	public ArrayList<String> getServiceStatusSpecs(String customerId){
		return saasLevel0DaoImpl.getServiceStatusSpecs(customerId);
	}

}
