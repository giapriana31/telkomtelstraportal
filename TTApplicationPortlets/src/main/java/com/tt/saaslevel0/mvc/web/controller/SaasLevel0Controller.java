package com.tt.saaslevel0.mvc.web.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;
import com.tt.saaslevel0.mvc.service.SaasLevel0ManagerImpl;
import com.tt.utils.TTGenericUtils;

@Controller("saasLevel0Controller")
@RequestMapping("VIEW")
public class SaasLevel0Controller {
	
	
	private SaasLevel0ManagerImpl saasLevel0ManagerImpl;

	@Autowired
	@Qualifier("saasLevel0Manager")
	public void setSaasLevel0ManagerImpl(SaasLevel0ManagerImpl saasLevel0ManagerImpl) {
		this.saasLevel0ManagerImpl = saasLevel0ManagerImpl;
	}
	
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {
		HttpServletRequest httpServletRequest = PortalUtil
				.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil
				.getHttpServletResponse(renderResponse);
	
		String customerId = TTGenericUtils.getCustomerIDFromSession(httpServletRequest,httpServletResponse);	
		System.out.println("cust: "+customerId);
		SubscribedService ss = new SubscribedService();

		int count = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_SAAS, customerId);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_SAAS);
			return "unsubscribed_saas_service";
		}
		else
		{
			ArrayList<String> serviceStatusSpecs= saasLevel0ManagerImpl.getServiceStatusSpecs(customerId);
			model.addAttribute("serviceStatusSpecs", serviceStatusSpecs);
				
			return "saasLevel0";
		}
	}

}
