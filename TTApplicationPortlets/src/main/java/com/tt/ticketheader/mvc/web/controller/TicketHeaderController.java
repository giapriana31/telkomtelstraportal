
package com.tt.ticketheader.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.web.portlet.util.PortletUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.client.model.incidentapi.InsertResponse;
import com.tt.client.service.incident.IncidentWSCallService;
import com.tt.logging.Logger;
import com.tt.logticket.mvc.service.common.LogIncidentManagerImpl;
import com.tt.model.Incident;
import com.tt.ticketheader.mvc.service.common.TicketHeaderService;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;
import com.tt.utils.TTGenericUtils;

@Controller("ticketHeaderController")
@RequestMapping("VIEW")
public class TicketHeaderController {
	
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(TicketHeaderController.class);
	private LogIncidentManagerImpl logIncidentManager;
	private static final String PRIVATE_CLOUD = "PrivateCloud";
	
    private TicketHeaderService ticketHeaderService;

    @Autowired
    @Qualifier("ticketHeaderService")
    public void setServiceRequestService(TicketHeaderService ticketHeaderService) {
		this.ticketHeaderService = ticketHeaderService;
	}
    
    @Autowired
	@Qualifier("logIncidentManager")
	public void setLogIncidentManager(LogIncidentManagerImpl logIncidentManager) {
		this.logIncidentManager = logIncidentManager;
	}
    
    @RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse renderResponse, Model model){
		try {
			HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
			/*ThemeDisplay td = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);*/
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
			log.info("Customer Unique ID " + customerUniqueID);
			String statusGlobal="active";
			if(PortletUtils.getSessionAttribute(request, "statusGlobal")!=null){
				statusGlobal=(String)PortletUtils.getSessionAttribute(request, "statusGlobal");
			}
			
			log.info("status in ticketheadercontorller-- "+statusGlobal);
			if(statusGlobal.equalsIgnoreCase("active")){
				model.addAttribute("ttActiveIdentifier", "selected");
				model.addAttribute("ttCompletedIdentifer", "inactiveTab");
			}
			else if(statusGlobal.equalsIgnoreCase("completed")){
				model.addAttribute("ttActiveIdentifier", "inactiveTab");
				model.addAttribute("ttCompletedIdentifer", "selected");
			}
			
			String lastRefreshDate = ticketHeaderService.getLastRefreshDate(customerUniqueID);
			model.addAttribute("lastRefreshDate", lastRefreshDate);
			
			PortletUtils.setSessionAttribute(request, "statusGlobal", null);
	         			
			RenderLogIncident(request, renderResponse, model);
			
			return "ticketHeaderView";
			
		} catch (Exception e) {
			log.error("Error in Render of Ticket Header of Incident Overview Page - " +e.toString());
			return "ticketHeaderView";
		}
	}
    
    @ActionMapping(value = "incidentTypeSelector")
    public void incidentTypeSelector(ActionRequest actionRequest,
	    ActionResponse actionResponse, Model model) throws IOException, PortletException {

    	log.info("Action control for IncTypeSelector");
    	String status = actionRequest.getParameter("status");
    	log.debug("status----"+status);
    	PortletUtils.setSessionAttribute(actionRequest, "statusGlobal", status);
    	javax.xml.namespace.QName statusBean = new QName("http://localhost:8080/ticketHeader", "headerTab", "event");
    	actionResponse.setEvent(statusBean, status);
    	
    	String url = prop.getProperty("tt.homeURL") + "/" + prop.getProperty("ttIncidentPageName");
    	actionResponse.sendRedirect(url);
    }
    
    @EventMapping(value = "{http://localhost:8080/updateTicket}backTo")
	public void processEventStatusFromUpdateTicket(EventRequest request, EventResponse response)
			throws PortletException, IOException {

		log.info("Process event for status from updateTicket in TicketHeaderController");

		javax.portlet.Event event = request.getEvent();
		String status = (String) event.getValue();

		log.debug("status from updateTicket received in TicketHeaderController----" + status);
		
		PortletUtils.setSessionAttribute(request, "statusGlobal", status);

	}

    public void RenderLogIncident(RenderRequest request, RenderResponse renderResponse, Model model){
		try{
			
			HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(renderResponse);
			String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(httpServletRequest, httpServletResponse);
			if(customerUniqueID != null){
				log.info("Customer Unique Id " + customerUniqueID);
				
				LinkedHashMap<String, String> regionList = logIncidentManager.getRegionNameMap(customerUniqueID);
				LinkedHashMap<String, String> regionListActual = new LinkedHashMap<String, String>();
				regionListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : regionList.entrySet()){
					regionListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("regionList", regionListActual);
				
				LinkedHashMap<String, String> siteNameList = new LinkedHashMap<String, String>();
				siteNameList.put("0", "--Select--");
				model.addAttribute("siteNameList", siteNameList);
				
				fetchUrgencyandImpactList(model);
				/*String[] urgencyList = prop.getProperty("UrgencyList").split(",");
				String[] impactList = prop.getProperty("ImpactList").split(",");
			
				LinkedHashMap<String, String> urgencyListMap = new LinkedHashMap<String, String>();
				urgencyListMap.put("0", "--Select--");
				for (int i =0;i<urgencyList.length;i++) {
					urgencyListMap.put(urgencyList[i], urgencyList[i]);
				}
			
				LinkedHashMap<String, String> impactListMap = new LinkedHashMap<String, String>();
				impactListMap.put("0", "--Select--");
				for (int i =0;i<impactList.length;i++) {
					impactListMap.put(impactList[i], impactList[i]);
				}*/
				
				
				
				LinkedHashMap<String, String> pcbsList = logIncidentManager.getPrivateCloudBusinessServiceValues(customerUniqueID); 
				LinkedHashMap<String, String> pcbsListActual = new LinkedHashMap<String, String>();
				pcbsListActual.put("NONE", "--Select--");
				for( Map.Entry<String,String> entry : pcbsList.entrySet()){
					pcbsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("pcbsList", pcbsListActual);
				
				//for getting SaaS Whispir Business Service - Start
				LinkedHashMap<String, String> saasWhispirBsList = logIncidentManager.getSaasWhispirBsValue(customerUniqueID);
				LinkedHashMap<String, String> saasWhispirBsListActual = new LinkedHashMap<String, String>();
				saasWhispirBsListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : saasWhispirBsList.entrySet()){
					saasWhispirBsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("saasWhispirBsList", saasWhispirBsListActual);
				//for getting SaaS Whispir Business Service - End
				
				//for getting SaaS IPScape Business Service - Start
				LinkedHashMap<String, String> saasIPScapeBsList = logIncidentManager.getSaasIPScapeBsValue(customerUniqueID);
				LinkedHashMap<String, String> saasIPScapeBsListActual = new LinkedHashMap<String, String>();
				saasIPScapeBsListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : saasIPScapeBsList.entrySet()){
					saasIPScapeBsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("saasIPScapeBsList", saasIPScapeBsListActual);
				//for getting SaaS IPScape Business Service - End
				
				//for getting SaaS Mandoe Business Service - Start
				LinkedHashMap<String, String> saasMandoeBsList = logIncidentManager.getSaasMandoeBsValue(customerUniqueID);
				LinkedHashMap<String, String> saasMandoeBsListActual = new LinkedHashMap<String, String>();
				saasMandoeBsListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : saasMandoeBsList.entrySet()){
					saasMandoeBsListActual.put(entry.getKey(), entry.getValue());
				}
				model.addAttribute("saasMandoeBsList", saasMandoeBsListActual);
				//for getting SaaS Mandoe Business Service - End
				
				model.addAttribute("logIncident", new Incident());
					
				LinkedHashMap<String, String> serviceIDList = new LinkedHashMap<String, String>();
				serviceIDList.put("0", "--Select--");
				model.addAttribute("LogIncidentDisclaimerMsg", prop.getProperty("LogIncidentDisclaimerMsg"));
				model.addAttribute("serviceIDList", serviceIDList);
				/*model.addAttribute("urgencyListMap", urgencyListMap);
				model.addAttribute("impactListMap", impactListMap);*/
				model.addAttribute("UrgencyDescription", prop.getProperty("UrgencyDescription"));
				model.addAttribute("ImpactDescription", prop.getProperty("ImpactDescription"));
	
			}
			else{
				log.info("Customer Unique Id not fetched and hence setting blank data");
				Incident incident = new Incident();
				model.addAttribute("logIncident", incident);
				
				fetchUrgencyandImpactList(model);
				/*String[] urgencyList = prop.getProperty("UrgencyList").split(",");
				String[] impactList = prop.getProperty("ImpactList").split(",");
			
				LinkedHashMap<String, String> urgencyListMap = new LinkedHashMap<String, String>();
				urgencyListMap.put("0", "--Select--");
				for (int i =0;i<urgencyList.length;i++) {
					urgencyListMap.put(urgencyList[i], urgencyList[i]);
				}
			
				LinkedHashMap<String, String> impactListMap = new LinkedHashMap<String, String>();
				impactListMap.put("0", "--Select--");
				for (int i =0;i<impactList.length;i++) {
					impactListMap.put(impactList[i], impactList[i]);
				}
				model.addAttribute("urgencyListMap", urgencyListMap);
				model.addAttribute("impactListMap", impactListMap);*/
			}
			
		}
		catch (Exception e){
			log.error("Error in Rendering Log Incident in Incident Overview Page- " + e.toString());
			
		}
	}
    
    @ResourceMapping(value="submitRequestURLLI")
	public void handleLogIncidentRequest(@ModelAttribute("logIncident") Incident incident, Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		try{
			JSONObject json = new JSONObject();
			
			HttpServletRequest httpServletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
			User user = UserLocalServiceUtil.getUser((Long)httpServletRequest.getSession().getAttribute(WebKeys.USER_ID));
			

			UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
			
			Map<String, String[]> parameters = uploadRequest.getParameterMap();
			String[] saasBS = parameters.get("privateCloudBusinessService");
			String[] saasComponet = parameters.get("virtualMachines");
			String saasWhispirBS = saasBS[1];
			String saasWhispirComp = saasComponet[1];
			String saasIPScapeBS = saasBS[2];
			String saasIPScapeComp = saasComponet[2];
			String saasMandoeBS = saasBS[3];
			String saasMandoeComp = saasComponet[3];
			
			String productType = uploadRequest.getParameter("productType");
			String summary = uploadRequest.getParameter("summary");
			String notes = uploadRequest.getParameter("worklognotes");
			String siteId = uploadRequest.getParameter("siteid");
			String impact =  uploadRequest.getParameter("impact");
			String urgency = uploadRequest.getParameter("urgency");
			String serviceId = uploadRequest.getParameter("serviceid");
			
			String privateCloudBusinessService = uploadRequest.getParameter("privateCloudBusinessService");
			String virtualMachines = "";
			if ("Whispir".equalsIgnoreCase(productType)){
				virtualMachines = uploadRequest.getParameter("saasWhispirComp");
			}else if ("IPScape".equalsIgnoreCase(productType)){
				virtualMachines = uploadRequest.getParameter("saasIPScapeComp");
			}else if ("Mandoe".equalsIgnoreCase(productType)){
				virtualMachines = uploadRequest.getParameter("saasMandoeComp");
			}else{
				virtualMachines = uploadRequest.getParameter("virtualMachines");
			}
			
			boolean mimCheck = uploadRequest.getParameter("mimCheck") != null;
			
			log.info("productType :: "+productType);
			log.info("privateCloudBusinessService :: "+privateCloudBusinessService);
			log.info("virtualMachines :: "+virtualMachines);
			
			
			log.info("Data fetched from JSP");
			log.debug("summary :: " + summary);
			log.debug("notes :: " + notes);
			log.debug("site id :: " + siteId);
			log.debug("impact :: " + impact);
			log.debug("urgency :: " + urgency);
			log.debug("service id :: " + serviceId);
			log.debug("MIM :: " + mimCheck);
			
			incident.setSummary(summary);
			incident.setWorklognotes(notes);
			incident.setSiteid(siteId);
			incident.setImpact(impact);
			incident.setUrgency(urgency);
			incident.setMimCheck(mimCheck);
			incident.setVirtualMachines(logIncidentManager.getCIName(virtualMachines));
			incident.setRootProductId(productType);
			
			if("MNS".equalsIgnoreCase(productType)){
				incident.setServiceid(serviceId);
			}else if ("PrivateCloud".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(privateCloudBusinessService));
			}else if ("Whispir".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(saasWhispirBS));
			}else if ("IPScape".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(saasIPScapeBS));
			}else if ("Mandoe".equalsIgnoreCase(productType)){
				incident.setServiceid(logIncidentManager.getCIName(saasMandoeBS));
			}
			
			
			if(incident.getMimCheck()){
				String originalNotes = incident.getWorklognotes();
				String appendMIM = prop.getProperty("MIMText");
				log.info("MIM Message to be appended - " + appendMIM);
				incident.setWorklognotes(appendMIM+" "+originalNotes);
			}
			RefreshUtil.setProxyObject();
			
			
			log.info("Caller - " + user.getFullName());
			incident.setTickettype(prop.getProperty("ticketTypeProperty"));
			incident.setTicketsource(prop.getProperty("ticketSourceProperty"));
			incident.setUserid(user.getEmailAddress()+"");
			incident.setResolvergroupname(prop.getProperty("resolverGroup"));
			
			String language = ((String) user.getExpandoBridge().getAttribute("preferredLanguage")).equalsIgnoreCase("en_US") ? "English" : "Bahasa";
			incident.setCustomerpreferedlanguage(language);
			//incident.setCustomerpreferedlanguage((String)user.getExpandoBridge().getAttribute("preferredLanguage"));
			
			log.debug("Making call to SNOW for inserting incident through controller");
			InsertResponse insertResponse = new InsertResponse();
			boolean isException = false;
			IncidentWSCallService callService = new IncidentWSCallService();
			try{
				insertResponse= callService.insertIncidentWebService(incident);
			}
			catch(Exception c){
				
				log.debug("Error display in webservice is as " + c.getMessage());
				isException=true;
			}
			if(!isException){
				log.debug("Display Response: " + insertResponse.getDisplayValue());
				log.debug("Error Response: " + insertResponse.getErrorMessage());
				
				if(insertResponse.getDisplayValue() !=null && insertResponse.getDisplayValue().trim().length()!=0){
					json.put("incidentID", "Success@@@"+insertResponse.getDisplayValue());
				}
				else if(insertResponse.getErrorMessage()!=null){
					json.put("incidentID", "Error@@@"+insertResponse.getErrorMessage());
				}
				
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}else{
				log.error("Connection timed out");
				json.put("incidentID", "Error@"+"Connection Timed Out. Please try again later.");
				PrintWriter out = response.getWriter();
				out.print(json.toString());
			}
		}
		catch(Exception e){
			log.error("Error while Processing Submit- " + e.toString());
		}
	}
	
	@ResourceMapping(value="getServiceURLLI")
	public void handleSericeIDFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		
		try{
			
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedSiteIDJson"));
			String selectedSiteID = jsonObj.getString("selectedSiteID");
			if(selectedSiteID!=null && selectedSiteID.trim().length()!=0 && selectedSiteID != "0"){
				
				String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req, httpServletResponse);
				log.info("Selected SiteName in controller : " + selectedSiteID);
				LinkedHashMap<String, String> serviceIDListtemp = logIncidentManager.getServiceFromSiteName(selectedSiteID, customerUniqueID);
				log.info("Result in controller for ServiceFromSite : " + serviceIDListtemp.size());
				String responseStr = "";
				if(serviceIDListtemp!=null){
					for( Map.Entry<String,String> entry : serviceIDListtemp.entrySet()){
						log.info("Service details : " + entry.getKey() + " == " + entry.getValue());
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
		}
		catch(Exception e){
			log.error("Error while Getting Service Names from Site- " + e.toString());
		}
	}
	
	
		
	@ResourceMapping(value="getSiteURL")
	public void handleSiteIDFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(response);
		try{
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedRegionJson"));
			String selectedRegion = jsonObj.getString("selectedRegion");
			if(selectedRegion!=null && selectedRegion.trim().length()!=0){
				
				String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(req, httpServletResponse);
				log.info("Selected Region in controller : " + selectedRegion);
				LinkedHashMap<String, String> siteIDListtemp = logIncidentManager.getSiteFromRegion(selectedRegion,customerUniqueID);
				String responseStr = "";
				if(siteIDListtemp!=null){
					for( Map.Entry<String,String> entry : siteIDListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
		}
		catch(Exception e){
			
			log.error("Error in controller while fetching Site Id from region - " +e.toString());
		}
	}
	
	private void fetchUrgencyandImpactList(Model model){
		try{
			
			LinkedHashMap<String, String> urgencyListMap = new LinkedHashMap<String, String>();
			urgencyListMap.put("0", "--Select--");
			LinkedHashMap<String, String> urgencyListMapTemp = logIncidentManager.getUrgencyList();
			
			for( Map.Entry<String,String> entry : urgencyListMapTemp.entrySet()){
				urgencyListMap.put(entry.getKey()+"",entry.getValue()+"");
			}
			
		
			LinkedHashMap<String, String> impactListMap = new LinkedHashMap<String, String>();
			impactListMap.put("0", "--Select--");
			LinkedHashMap<String, String> impactListMapTemp = logIncidentManager.getImpactList();
			
			for( Map.Entry<String,String> entry : impactListMapTemp.entrySet()){
				impactListMap.put(entry.getKey()+"",entry.getValue()+"");
			}
			
			
			model.addAttribute("urgencyListMap", urgencyListMap);
			model.addAttribute("impactListMap", impactListMap);
		}
		catch(Exception e){
			log.error("Error while fetching Impact & urgency in controller - " + e.toString());
		}
	}
	
	@ActionMapping(value = "refreshInc")
	public void dataRefresh(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException, PortalException, SystemException{
		
		log.debug("TicketHeaderController * dataRefresh * Inside the method");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), themeDisplay.getUser().getDisplayEmailAddress());
		String customerId = (String)user.getExpandoBridge().getAttribute("CustomerUniqueID");
		log.debug("TicketHeaderController * dataRefresh * customerId--------------->>"+customerId);
		ticketHeaderService.dataRefresh(customerId);
		String url = prop.getProperty("tt.homeURL") + "/" + prop.getProperty("ttIncidentPageName");
		log.debug("TicketHeaderController * dataRefresh * Exit the method");
		actionResponse.sendRedirect(url);
	}
	
	@ResourceMapping(value="getVMURL")
	public void getValuesFromProductType(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{
		String pcbsId = resourceRequest.getParameter("pcbs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + pcbsId);
		
		responseJson = logIncidentManager.getVMList(pcbsId);
		resourceResponse.getWriter().print(responseJson);
	}
	
	@ResourceMapping(value="getSaasWhispirResources")
	public void getSaasWhispirResources(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{				
		String saasWhispirBsId = resourceRequest.getParameter("saasWhispirBs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + saasWhispirBsId);
		
		responseJson = logIncidentManager.getSaasWhispirResourcesList(saasWhispirBsId);
		resourceResponse.getWriter().print(responseJson);
	}
	
	@ResourceMapping(value="getSaasIPScapeResources")
	public void getSaasIPScapeResources(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{				
		String saasIPScapeBsId = resourceRequest.getParameter("saasIPScapeBs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + saasIPScapeBsId);
		
		responseJson = logIncidentManager.getSaasIPScapeResourcesList(saasIPScapeBsId);
		resourceResponse.getWriter().print(responseJson);
	}
	
	@ResourceMapping(value="getSaasMandoeResources")
	public void getSaasMandoeResources(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException
	{				
		String saasMandoeBsId = resourceRequest.getParameter("saasMandoeBs");
		com.liferay.portal.kernel.json.JSONArray responseJson;
		
		log.debug("product type :: " + saasMandoeBsId);
		
		responseJson = logIncidentManager.getSaasMandoeResourcesList(saasMandoeBsId);
		resourceResponse.getWriter().print(responseJson);
	}
}