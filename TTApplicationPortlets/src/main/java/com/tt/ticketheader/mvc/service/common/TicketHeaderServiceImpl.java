package com.tt.ticketheader.mvc.service.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.RefreshIncident;
import com.tt.logging.Logger;
import com.tt.ticketheader.mvc.dao.TicketHeaderDao;

@Service(value = "ticketHeaderService")
@Transactional
public class TicketHeaderServiceImpl implements TicketHeaderService {

	private final static Logger log = Logger.getLogger(TicketHeaderServiceImpl.class);
	private TicketHeaderDao ticketHeaderDao;

	@Autowired
	@Qualifier("ticketHeaderDaoImpl")
	public void setTicketHeaderDao(TicketHeaderDao ticketHeaderDao) {
		this.ticketHeaderDao = ticketHeaderDao;
	}

	@Override
	public String getLastRefreshDate(String customerId) {
		
		log.debug("TicketHeaderServiceImpl * getLastRefreshDate * Inside the method");
		Date lastRefreshDate = ticketHeaderDao.getLastRefreshDate(customerId,"Incident");
		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Jakarta");
		df.setTimeZone(timeZone1);
		if (lastRefreshDate != null) {
			String fromDate = df.format(lastRefreshDate);
			return fromDate;
		}
		log.debug("TicketHeaderServiceImpl * getLastRefreshDate * Exit the method");
		return null;
	}

	@Override
	public void dataRefresh(String customerId) {

		log.debug("TicketHeaderServiceImpl * dataRefresh * Inside the method");
		Date lastRefreshDate = ticketHeaderDao.getLastRefreshDate(customerId,"Incident");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		try {
			RefreshIncident refreshObj = new RefreshIncident();
			String toDate = new String();
			String fromDate = new String();
			Date todate = new Date();
			toDate = df.format(todate);
			log.debug("TicketHeaderServiceImpl * dataRefresh * toDate------>"+toDate);
			if (lastRefreshDate != null)
				fromDate = df.format(lastRefreshDate);
			log.debug("TicketHeaderServiceImpl * dataRefresh * fromDate------>"+fromDate);
			refreshObj.refreshIncident(customerId, toDate, fromDate, "CUST");
			log.debug("TicketHeaderServiceImpl * dataRefresh * Exit the method");
		} catch (Exception ex) {
			log.error(" Exception while calling data refresh ");
			ex.printStackTrace();
		}
	}

}
