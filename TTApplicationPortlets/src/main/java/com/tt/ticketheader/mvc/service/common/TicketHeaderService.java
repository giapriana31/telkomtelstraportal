package com.tt.ticketheader.mvc.service.common;



public interface TicketHeaderService {

	public String getLastRefreshDate(String customerId);

	public void dataRefresh(String customerId);
}
