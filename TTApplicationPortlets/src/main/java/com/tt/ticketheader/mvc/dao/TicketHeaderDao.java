package com.tt.ticketheader.mvc.dao;

import java.util.Date;

public interface TicketHeaderDao {

	public Date getLastRefreshDate(String customerId, String entityType);
}
