package com.tt.ticketheader.mvc.dao;

import java.util.Date;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Query;

import com.tt.logging.Logger;
import com.tt.ticketheader.mvc.service.common.TicketHeaderServiceImpl;



@Repository(value="ticketHeaderDaoImpl")
@Transactional
public class TicketHeaderDaoImpl implements TicketHeaderDao{
	
	private final static Logger log = Logger.getLogger(TicketHeaderServiceImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public static final String AND = "' AND ";
	public static final String EQUAL_SINGLE = " = '";
	public static final String INOPERATOR = " IN ";
	public static final String OPENBRACE = " ( ";
	public static final String CLOSEBRACE = " ) ";
	public static final String SINGLEQUOTE = "'";
	public static final String ANDOPERATOR = " AND ";
	public static final String BETWEEN = " BETWEEN ";
	public static final String TO_DATE = " TO_DATE ";
	public static final String DATEFORMAT = " 'yyyy-MM-dd HH:mm:ss' ";
	public static final String COMMA = " , ";
	public static final String ORDERBY = " ORDER BY ";
	public static final String OFFSET = " OFFSET ";
	public static final String FETCHNEXT = " FETCH NEXT ";
	public static final String ROWSONLY = " ROWS ONLY ";
	public static final String LIKE = " LIKE ";
	public static final String OROPERATOR = " OR ";
	public static final String PERCENTAGE = "%";
	public static final String LIMIT = " LIMIT ";


	// Field level constants
	public static final String CMDBCLASS = " cmdbclass ";
	public static final String CUSTOMERIMPACTED = " customerimpacted ";
	public static final String OPENDATETIME = " opendatetime ";
	public static final String RESOLUTIONDATETIME = " resolutiondatetime ";
	public static final String CASEMANAGER = " casemanager ";
	//public static final String OPENDATETIME = " opendatetime "; 
	public static final String INCIDENTID = " incidentid ";  
	public static final String SHORTDESCRIPTION = " summary ";
	public static final String CUSTOMERUNIQUEID = " customeruniqueid  ";
	public static final String CITYPE = " citype  ";
	public static final String USERID = " userid  ";
	public static final String CINAME = " ciname  ";
	public static final String SITEID = " siteid  ";
	

	@Override
	public Date getLastRefreshDate(String customerId, String entityType) {

		log.debug("TicketHeaderDaoImpl * getLastRefreshDate * Inside the method");
		StringBuilder sql = new StringBuilder();
		sql.append("select max(lastrefreshtime) from lastrefreshtime sa");
		sql.append(" where sa.customeruniqueid=:customerId and sa.entityname=:entityType");
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
		query.setString("customerId", customerId);
		query.setString("entityType", entityType);
		log.debug("TicketHeaderDaoImpl * getLastRefreshDate * Customer id and entity type----->"+customerId+" and "+entityType);
		if (query.list() != null && query.list().size() > 0) {
			Date refreshDate = (Date) query.list().get(0);
			log.debug("TicketHeaderDaoImpl * getLastRefreshDate * Exit the method");
			return refreshDate;
		} else {
			log.debug("TicketHeaderDaoImpl * getLastRefreshDate * Exit the method");
			return null;
		}
	}
		
}
