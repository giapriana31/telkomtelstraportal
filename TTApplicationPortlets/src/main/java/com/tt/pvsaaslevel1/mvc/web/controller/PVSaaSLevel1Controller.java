package com.tt.pvsaaslevel1.mvc.web.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.logging.Logger;
import com.tt.pvprivatecloudlevel1.mvc.model.drillDownDetails;
import com.tt.pvsaaslevel0.mvc.service.common.PVSaaSLevel0ManagerImpl;
import com.tt.pvsaaslevel1.mvc.model.DrillDownDetails;
import com.tt.pvsaaslevel1.mvc.service.common.PVSaaSLevel1ManagerImpl;
import com.tt.utils.TTGenericUtils;


@Controller("pvSaasLevel1Controller")
@RequestMapping("VIEW")
public class PVSaaSLevel1Controller {
	
	
	private PVSaaSLevel1ManagerImpl pvSaaSLevel1ManagerImpl;
	
	
	
	@Autowired
	@Qualifier(value="pvSaaSLevel1ManagerImpl")
	public void setPvSaaSLevel1ManagerImpl(
			PVSaaSLevel1ManagerImpl pvSaaSLevel0ManagerImpl) {
		this.pvSaaSLevel1ManagerImpl = pvSaaSLevel0ManagerImpl;
	}
	
	
	private final static Logger log = Logger.getLogger(PVSaaSLevel1Controller.class);
	
	



	



	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {

		
		log.info("PVSaaSLevel1Controller-render()");
		
		
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(renderResponse);

		
		//String customerId="TT001";
		
		
		
		
		
			return "pvSaaSLevel1";
		
		
		
		
		
		
	}

	
	
	@ResourceMapping(value="geSaaStDrillDownDetail")
	public void getDrillDownData(ResourceRequest request,ResourceResponse response){
	
		
		Map<String,String> nameToDeliveryStageMap= new HashMap<String, String>();
		nameToDeliveryStageMap.put("Order Received", "1");
		nameToDeliveryStageMap.put("Detailed Design", "2");
		nameToDeliveryStageMap.put("Procurement", "3");
		nameToDeliveryStageMap.put("DeliveryActivation", "4");
		nameToDeliveryStageMap.put("Monitoring", "5");
		nameToDeliveryStageMap.put("Operational", "6");
		
		log.info("PVSaaSLevel1Controller-getDrillDownData()-start");

		

		
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse res = PortalUtil.getHttpServletResponse(response);

		String customerId=TTGenericUtils.getCustomerIDFromSession(req, res);

		//String customerId="TT001";
		






		
		List<DrillDownDetails> drillDetailList=pvSaaSLevel1ManagerImpl.getDrillDownDetails(customerId);
		
		JSONObject objectList;
		JSONArray drillDownArray= new JSONArray();

		if (null!=drillDetailList) {
			for (DrillDownDetails drillDownDetails : drillDetailList) {

				log.info("PVSaaSLevel1Controller-getDrillDownData() "
						+ "Color: " + drillDownDetails.getDeliveryStage()+ " Name: "
						+ drillDownDetails.getCiname());

			}
			LinkedHashMap<String, String> drillDownMap = new LinkedHashMap<String, String>();
		
			for (DrillDownDetails drillDownDetails : drillDetailList) {
				JSONObject tempJsonObj = new JSONObject();
				tempJsonObj.put(drillDownDetails.getCiname()+"***"+drillDownDetails.getCiid(), drillDownDetails.getDeliveryStage());
				drillDownArray.add(tempJsonObj);
				drillDownMap.put(drillDownDetails.getCiname()+"***"+drillDownDetails.getCiid(),
						drillDownDetails.getDeliveryStage());
			}
			ArrayList<String> keys= new  ArrayList<String>();
			keys.addAll(drillDownMap.keySet());
			
			objectList= new JSONObject();
			
			for (int i = 0; i < keys.size(); i++) {
				objectList.put(keys.get(i), drillDownMap.get(keys.get(i)));
			}
			
			log.info("PVSaaSLevel1Controller-getDrillDownData() Drill Down Json: "
					+ objectList);
		}
		
		else{
			
			objectList = new JSONObject();
			
		}
		PrintWriter out = null;
		try {
			out=response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			log.error("PVSaaSLevel1Controller-getDrillDownData()-catch: "+e.getMessage());

		}
		
		//out.print(objectList.toString());
		out.print(drillDownArray.toString());
		
		
		log.info("PVSaaSLevel1Controller-getDrillDownData()-end");

		
	}
	
	
}
