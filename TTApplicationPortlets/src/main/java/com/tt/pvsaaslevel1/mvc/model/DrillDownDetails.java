package com.tt.pvsaaslevel1.mvc.model;

public class DrillDownDetails {
	
	private String ciid;
	private String ciname;
	private String deliveryStage;
	
	
	public String getCiid() {
		return ciid;
	}
	public void setCiid(String ciid) {
		this.ciid = ciid;
	}
	public String getCiname() {
		return ciname;
	}
	public void setCiname(String ciname) {
		this.ciname = ciname;
	}
	public String getDeliveryStage() {
		return deliveryStage;
	}
	public void setDeliveryStage(String deliveryStage) {
		this.deliveryStage = deliveryStage;
	}
	
	
	

}
