package com.tt.pvsaaslevel1.mvc.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.pvprivatecloudlevel1.mvc.model.drillDownDetails;
import com.tt.pvsaaslevel1.mvc.dao.PVSaaSLevel1DaoImpl;
import com.tt.pvsaaslevel1.mvc.model.DrillDownDetails;


@Service(value = "pvSaaSLevel1ManagerImpl")
@Transactional
public class PVSaaSLevel1ManagerImpl {
	
	
	private PVSaaSLevel1DaoImpl pvSaaSLevel1DaoImpl;
	
	
	
	@Autowired
	@Qualifier(value="pvSaaSLevel1DaoImpl")
	public void setPvSaaSLevel1DaoImpl(PVSaaSLevel1DaoImpl pvSaaSLevel1DaoImpl) {
		this.pvSaaSLevel1DaoImpl = pvSaaSLevel1DaoImpl;
	}




	public List<DrillDownDetails> getDrillDownDetails(String customerId) {
		// TODO Auto-generated method stub
		return pvSaaSLevel1DaoImpl.getDrillDownDetails(customerId);
	}

	
	
	
	
	
}
