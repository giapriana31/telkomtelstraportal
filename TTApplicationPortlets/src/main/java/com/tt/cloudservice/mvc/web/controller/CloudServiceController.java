package com.tt.cloudservice.mvc.web.controller;

import java.text.DecimalFormat;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.cloudservice.mvc.dao.CloudServiceDaoImpl;
import com.tt.cloudservice.mvc.model.CloudServiceModel;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.utils.TTGenericUtils;

@Controller("cloudServiceController")
@RequestMapping("VIEW")
public class CloudServiceController 
{
	
	private CloudServiceDaoImpl cloudServiceDaoImpl;
	private final static Logger log = Logger.getLogger(CloudServiceController.class);
	
	@Autowired
	@Qualifier("cloudServiceDao")
	public void setCloudServiceDaoImpl(CloudServiceDaoImpl cloudServiceDaoImpl) {
		this.cloudServiceDaoImpl = cloudServiceDaoImpl;
	}



	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response, Model model) throws PortalException,SystemException 
	{
		log.debug("CloudServiceController ----- handleRenderRequest Called");
		String customerUniqueID = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request), PortalUtil.getHttpServletResponse(response));
		log.debug("CloudServiceController ----- CustomerID "+customerUniqueID);
		SubscribedService ss = new SubscribedService();
		int count = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_PRIVATECLOUD, customerUniqueID);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, GenericConstants.PRODUCTTYPE_PRIVATECLOUD);
			return "unsubscribed_cloud_service";
		}
		else
		{
			String colors = cloudServiceDaoImpl.getTotalVMJSONArray(customerUniqueID);
			model.addAttribute("colors",colors);
			log.debug("CloudServiceController ----- Model set -- colors "+colors);
			
			CloudServiceModel obj = cloudServiceDaoImpl.getVMConfigurationInfo(customerUniqueID);
			log.debug("CloudServiceController ----- Model set -- CloudServiceModel "+obj.toString());
			
			DecimalFormat formatZero = new DecimalFormat("0.#");
			
			model.addAttribute("RemainCPU",formatZero.format(obj.getRemainCPU()));
			model.addAttribute("UsedCPU",formatZero.format(obj.getUsedCPU()));
			model.addAttribute("RemainCPUPer",obj.getRemainCPUPer());
			model.addAttribute("UsedCPUPer",obj.getUsedCPUPer());
			
			model.addAttribute("RemainMemory",obj.getRemainMemory());
			model.addAttribute("UsedMemory",obj.getUsedMemory());
			model.addAttribute("RemainMemoryPer",obj.getRemainMemoryPer());
			model.addAttribute("UsedMemoryPer",obj.getUsedMemoryPer());
			
			model.addAttribute("RemainStorage",obj.getRemainStorage());
			model.addAttribute("UsedStorage",obj.getUsedStorage());
			model.addAttribute("RemainStoragePer",obj.getRemainStoragePer());
			model.addAttribute("UsedStoragePer",obj.getUsedStoragePer());
			
			return "cloudService";
		}
	}
	
}