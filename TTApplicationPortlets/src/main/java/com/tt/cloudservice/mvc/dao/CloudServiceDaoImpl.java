package com.tt.cloudservice.mvc.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.cloudservice.mvc.model.CloudServiceModel;
import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;

@Repository(value = "cloudServiceDao")
@Transactional
public class CloudServiceDaoImpl {
	
	private final static Logger log = Logger.getLogger(CloudServiceDaoImpl.class);
	Properties prop = PropertyReader.getProperties();

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public String getTotalVMJSONArray(String CustomerID) 
	{
		int red = 0;
		int green= 0;
		int amber= 0;
		
		log.debug("CloudServiceDaoImpl ----- getTotalVMJSONArray() ----- Method Start");
		List<String> vmWithIncident = new ArrayList<String>();
		try{		
			//CustomerID = "TT001"; // REMOVE THIS, NO NEED TO OVERRIDE IN PROD
			StringBuilder sql = new StringBuilder();
			sql.append("Select device.ciid, case when Min(device.color) = 1 then 'Red' when Min(device.color) = 2 then 'Amber' else 'Green' end as vmcolor");
			sql.append(" from ( Select c.ciid,c.ciname, i.priority,");
			sql.append(" CASE WHEN i.priority IN (1 , 2) AND i.customerimpacted = 1 THEN 1");
			sql.append(" WHEN i.priority IN (1 , 2) AND  i.customerimpacted = 0 OR i.priority IN (3 , 4, 5) AND i.customerimpacted = 1 THEN 2");
			sql.append(" ELSE 3 END as color FROM  configurationitem c, incdtodevicemapping id, incident i");
			sql.append(" WHERE id.deviceciid = c.ciid AND id.incidentid = i.incidentid and c.cmdbclass='VM Instance'   AND c.customeruniqueid = '"+CustomerID+"'");
			sql.append(" and c.status = 'Operational' and lower(i.incidentstatus) not in ( 'Closed','Cancelled','Completed')) ");
			sql.append(" device GROUP BY device.ciid");
			
			Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
	
			List<Object[]> totalVMs = (List<Object[]>) query.list();
			log.debug("CloudServiceDaoImpl ------  getTotalVMJSONArray() ----- totalVMs List is "+totalVMs==null?"NULL":totalVMs.size()+"");
			
			if (totalVMs != null) 
			{
				for (Object[] row : totalVMs) 
				{
					if (row[0] != null && row[0].toString().trim() != "" && row[1] != null && row[1].toString().trim() != "") 
					{
						vmWithIncident.add(row[0].toString());
						if(row[1].toString().equals("Red"))
							red++;
						else if(row[1].toString().equals("Green"))
							green++;
						else if(row[1].toString().equals("Amber"))
							amber++;
						log.debug("CloudServiceDaoImpl ------ getTotalVMJSONArray() ----- Red "+ red +" Green "+ green +" Amber"+ amber);
					}
				}
			}
			
			String sqlWithoutInc = "select count(*) from configurationitem where ciid not in (:ciidwithincident) and customeruniqueid= :customerid and status = 'Operational' and cmdbclass='VM Instance'";
			Query queryWithoutInc = sessionFactory.getCurrentSession().createSQLQuery(sqlWithoutInc.toString());
			if(vmWithIncident!=null && vmWithIncident.size()>0)
				queryWithoutInc.setParameterList("ciidwithincident", vmWithIncident);
			else
				queryWithoutInc.setParameter("ciidwithincident", "''");
			queryWithoutInc.setString("customerid", CustomerID);
			if(queryWithoutInc!=null && queryWithoutInc.list()!=null && queryWithoutInc.list().size()>0){
				
				Object greenCount = queryWithoutInc.list().get(0);
				green = green + Integer.parseInt(greenCount.toString());
			
			}
		}catch(Exception e)	{
			log.error("CloudServiceDaoImpl ------ getTotalVMJSONArray() -----"+e);
		}
		
		log.debug("CloudServiceDaoImpl ------ getTotalVMJSONArray() ----- Return "+red+"@@@@"+green+"@@@@"+amber);
		return red+"@@@@"+green+"@@@@"+amber;
	}
	
	
	@SuppressWarnings("unchecked")
	public CloudServiceModel getVMConfigurationInfo(String CustomerID) 
	{
		double storageTotal = 0;
		double cpuTotal = 0;
		double memoryTotal = 0;
		double storageUsed = 0;
		double cpuUsed = 0;
		double memoryUsed = 0;
		
		log.debug("CloudServiceDaoImpl ------ getVMConfigurationInfo() ----- Method Start");
		
		//CustomerID = "TT001"; // REMOVE THIS, NO NEED TO OVERRIDE IN PROD
		CloudServiceModel obj = new CloudServiceModel();
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery("select sum(maxnumberofvms),TRUNCATE(sum(maxsizeofstorage),2),sum(numberofvcpu),TRUNCATE(sum(maxmemory),2) "+ 
					"from vblock where ciid in "+
					"(select ciid from configurationitem where cmdbclass ='vBLOCK' and status = 'Operational' and customeruniqueid = '"+CustomerID+"')");

		List<Object[]> query_result = (List<Object[]>) query.list();
		log.debug("CloudServiceDaoImpl ----- getVMConfigurationInfo() ----- Query_result is "+query_result==null?"NULL":query_result.size()+"");
		
		if (query_result != null) 
		{
			for (Object[] row : query_result) 
			{
				try { storageTotal += Double.parseDouble(row[1].toString());	} catch(Exception e) {	storageTotal += 0;	}
				try { cpuTotal += Double.parseDouble(row[2].toString());		} catch(Exception e) {	cpuTotal += 0;		}
				try { memoryTotal += Double.parseDouble(row[3].toString());	} catch(Exception e) {	memoryTotal += 0;	}
			}
			log.debug("CloudServiceDaoImpl ----- getVMConfigurationInfo ----- StorageTotal "+storageTotal+" CPUTotal "+cpuTotal+" MemoryTotal "+memoryTotal);
		}
		
		query = sessionFactory.getCurrentSession().createSQLQuery("SELECT TRUNCATE(disks * disksize,2),cpus,TRUNCATE(memory/1024,2)"+ 
					" FROM virtualmachine where ciid in (select ciid from configurationitem where cmdbclass ='VM Instance' and customeruniqueid =  '"+CustomerID+"')");
		
		query_result = (List<Object[]>) query.list();
		log.debug("CloudServiceDaoImpl ----- getVMConfigurationInfo() ----- Second Query is "+query_result==null?"NULL":query_result.size()+"");
		
		if (query_result != null) 
		{
			for (Object[] row : query_result) 
			{
				try { storageUsed += Double.parseDouble(row[0].toString());	} catch(Exception e) {	storageUsed += 0;	}
				try { cpuUsed += Double.parseDouble(row[1].toString());		} catch(Exception e) {	cpuUsed += 0;		}
				try { memoryUsed += Double.parseDouble(row[2].toString());	} catch(Exception e) {	memoryUsed += 0;	}				
			}
			log.debug("CloudServiceDaoImpl ----- getVMConfigurationInfo ----- StorageUsed "+storageUsed+" CPUUsed "+cpuUsed+" MemoryUsed "+memoryUsed);
		}
		
		obj.setUsedStorage(storageUsed);
		obj.setUsedStoragePer(((storageUsed*100)/storageTotal));
		obj.setRemainStorage((storageTotal-storageUsed));
		obj.setRemainStoragePer(100-obj.getUsedStoragePer());
		
		obj.setUsedCPU(cpuUsed);		
		obj.setUsedCPUPer(((cpuUsed*100)/cpuTotal));
		obj.setRemainCPU((cpuTotal-cpuUsed));		
		obj.setRemainCPUPer(100-obj.getUsedCPUPer());
		
		obj.setUsedMemory(memoryUsed);		
		obj.setUsedMemoryPer(((memoryUsed*100)/memoryTotal));
		obj.setRemainMemory((memoryTotal-memoryUsed));
		obj.setRemainMemoryPer(100-obj.getUsedMemoryPer());
		
		obj.removeNaN();
		
		return obj;	
	}
}
