package com.tt.officebuildingdetail.mvc.model.officebuilding2;

import java.util.List;

public class OfficeBuildingSiteList {

	private String siteTier;
	private String siteCount;
	private List<OfficeBuildingSiteColor> officeBuildingSiteColorList;
	
	public String getSiteTier() {
		return siteTier;
	}
	public void setSiteTier(String siteTier) {
		this.siteTier = siteTier;
	}
	public String getSiteCount() {
		return siteCount;
	}
	public void setSiteCount(String siteCount) {
		this.siteCount = siteCount;
	}
	public List<OfficeBuildingSiteColor> getOfficeBuildingSiteColorList() {
		return officeBuildingSiteColorList;
	}
	public void setOfficeBuildingSiteColorList(
			List<OfficeBuildingSiteColor> officeBuildingSiteColorList) {
		this.officeBuildingSiteColorList = officeBuildingSiteColorList;
	}
	
	
	
}
