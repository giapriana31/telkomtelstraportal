package com.tt.officebuildingdetail.mvc.service.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.RefreshIncident;
import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.model.Site;
import com.tt.officebuildingdetail.mvc.dao.OfficeBuildingDetailDaoImpl;

@Service(value="officeBuildingDetailManagerImpl")
@Transactional
public class OfficeBuildingDetailManagerImpl {

	private final static Logger log = Logger.getLogger(OfficeBuildingDetailManagerImpl.class);
	private OfficeBuildingDetailDaoImpl officeBuildingDetailDaoImpl;

	@Autowired
	@Qualifier("officeBuildingDetailDaoImpl")
	public void setOfficeBuildingDetailDaoImpl(
		OfficeBuildingDetailDaoImpl officeBuildingDetailDaoImpl) {
	    this.officeBuildingDetailDaoImpl = officeBuildingDetailDaoImpl;
	}
	
	public String getcommonDBCall(){
		return officeBuildingDetailDaoImpl.getcommonDBCall();
	}

	public Map<String, Map<String, Integer>> getOfficeBuildingFirstGraph(String customerId) {
		
		Map<String, Map<String, Integer>> colorMapBuilding = new HashMap<String, Map<String,Integer>>();
		
		//By default whether the site or incidence is present or absent we are going to show the user 
		//site donut graph. So initialise the base map to all zeroes 
		ArrayList<String> siteTypes = new ArrayList<String>(Arrays.asList("Gold", "Silver", "Bronze"));
		for (String siteType : siteTypes ) {
			
			Map<String, Integer> colorCountMap = new HashMap<String, Integer>();
			
			colorCountMap.put("Green", 0);
			colorCountMap.put("Red", 0);
			colorCountMap.put("Amber", 0);
			colorMapBuilding.put(siteType, colorCountMap);	
		}
		
		try{
				log.debug("OfficeBuildingDetailManagerImpl * getOfficeBuildingFirstGraph * Inside the method");
			
		
			List<Site> sites = new ArrayList<Site>();
			sites = officeBuildingDetailDaoImpl.getSites(customerId);
			log.debug("OfficeBuildingDetailManagerImpl * getOfficeBuildingFirstGraph * Sites--------------->>"+ sites == null ? "NULL" : sites.toString());
			List<String> siteMapped = new ArrayList<String>();
			List<IncidentDashboardMapping> incidentList = officeBuildingDetailDaoImpl.getOfficeBuildingFirstGraph(customerId);
			Map<String, List<IncidentDashboardMapping>> siteTierIncidentMap = new HashMap<String, List<IncidentDashboardMapping>>();
			if(incidentList!=null && incidentList.size()>0){
				for (IncidentDashboardMapping incident : incidentList) {
				
					if (siteTierIncidentMap.containsKey(incident.getSitetier())) {
						siteTierIncidentMap.get(incident.getSitetier()).add(
								incident);
					} else {
						
						List<IncidentDashboardMapping> incidentSiteTierList = new ArrayList<IncidentDashboardMapping>();
						log.debug("OfficeBuildingDetailManagerImpl * getOfficeBuildingFirstGraph * incidentSiteTierList---" + incidentSiteTierList == null ? "NULL" : incidentSiteTierList.toString());
						incidentSiteTierList.add(incident);
						siteTierIncidentMap.put(incident.getSitetier(),incidentSiteTierList);
					}
				}
			}
			
			if (null !=siteTierIncidentMap && !siteTierIncidentMap.isEmpty()) {
				for (String siteTier : siteTierIncidentMap.keySet()) {
					Map<String, String> siteColorMap = new HashMap<String, String>();
					Map<String, Integer> colorCountMap = new HashMap<String, Integer>();

					for (IncidentDashboardMapping incidentDashboardMapping : siteTierIncidentMap
							.get(siteTier)) {
						if (siteColorMap.containsKey(incidentDashboardMapping
								.getSitename())) {
							if (isCurrentSiteGreater(
									incidentDashboardMapping.getSitecolor(),
									siteColorMap.get(incidentDashboardMapping
											.getSitename()))) {
								String oldColorCode = siteColorMap
										.get(incidentDashboardMapping
												.getSitename());
								if (colorCountMap.containsKey(oldColorCode)) {
									colorCountMap
											.put(oldColorCode, colorCountMap
													.get(oldColorCode) - 1);
								}
								if (colorCountMap
										.containsKey(incidentDashboardMapping
												.getSitecolor())) {
									colorCountMap.put(incidentDashboardMapping
											.getSitecolor(), colorCountMap
											.get(incidentDashboardMapping
													.getSitecolor()) + 1);
								} else {
									colorCountMap.put(incidentDashboardMapping
											.getSitecolor(), 1);
								}
								siteColorMap
										.put(incidentDashboardMapping
												.getSitename(),
												incidentDashboardMapping
														.getSitecolor());
							}
						} else {
							siteMapped.add(incidentDashboardMapping
									.getSitename());
							siteColorMap.put(
									incidentDashboardMapping.getSitename(),
									incidentDashboardMapping.getSitecolor());
							if (colorCountMap
									.containsKey(incidentDashboardMapping
											.getSitecolor())) {
								colorCountMap
										.put(incidentDashboardMapping
												.getSitecolor(), colorCountMap
												.get(incidentDashboardMapping
														.getSitecolor()) + 1);
							} else {
								colorCountMap
										.put(incidentDashboardMapping
												.getSitecolor(), 1);
							}
						}
					}
					colorMapBuilding.put(siteTier, colorCountMap);
				}
			}
			log.debug("OfficeBuildingDetailManagerImpl * getOfficeBuildingFirstGraph * sites---" + sites == null ? "NULL" : sites.toString());
			log.debug("OfficeBuildingDetailManagerImpl * getOfficeBuildingFirstGraph * sites.size()"+sites.size());
			if(sites!=null && sites.size()>0){
				
				for (Site site : sites) {
					if (siteMapped != null	&& !siteMapped.contains(site.getCustomersite())) {
				
						log.info("incoming site name.. "+site.getCustomersite());
						for (String siteMappedRow : siteMapped) {
							log.info("site mapped rows.. "+siteMappedRow);
						}
						if (colorMapBuilding.containsKey(site.getServicetier())) {
							Map<String, Integer> colorCountMap = new HashMap<String, Integer>();
							colorCountMap = colorMapBuilding.get(site.getServicetier());
							if (colorCountMap.containsKey("Green")) {
				

								colorCountMap.put("Green", colorCountMap.get("Green") + 1);
				
				
							} else {
				

								colorCountMap.put("Green", 1);
							}
							colorMapBuilding.put(site.getServicetier(),	colorCountMap);
						} else {
				

							Map<String, Integer> colorCountMap = new HashMap<String, Integer>();
							colorCountMap.put("Green", 1);
							colorMapBuilding.put(site.getServicetier(),	colorCountMap);
						}
	
					}
				}
			}
			
			
			}catch(Exception e){
			
			log.error(" Exception in getOfficeBuildingFirstGraph method "+e.getMessage());
		}
		
		log.debug("OfficeBuildingDetailManagerImpl * getOfficeBuildingFirstGraph * Exit the method");
		
		return colorMapBuilding;
	}

	private boolean isCurrentSiteGreater(String currentSitecolor, String oldSitecolor) {
		
		try{
			log.debug("OfficeBuildingDetailManagerImpl * isCurrentSiteGreater * Inside the method");
			if(getPriority(currentSitecolor)>getPriority(oldSitecolor)){
		
			return true;
		}
		return false;
		}catch(Exception e){
			log.error(" Exception in isCurrentSiteGreater method "+e.getMessage());
		}
		log.debug("OfficeBuildingDetailManagerImpl * isCurrentSiteGreater * Exit the method");
		return false;
	}
	
	
	public int getPriority(String siteColor) {
		try{
			log.debug("OfficeBuildingDetailManagerImpl * getPriority * Inside the method");
		if("Red".equalsIgnoreCase(siteColor)){
			return 3;
		} else if("Green".equalsIgnoreCase(siteColor)){
			return 1;
		} else if("Amber".equalsIgnoreCase(siteColor)){
			return 2;
		}
		
		return 1;
		}catch(Exception e){
			log.error(" Exception in getPriority method "+e.getMessage());
		}
		log.debug("OfficeBuildingDetailManagerImpl * getPriority * Exit the method");
		return 0;
	}

	public void dataRefresh(String customerId) {
		
		log.debug("OfficeBuildingDetailManagerImpl * dataRefresh * Inside the method");
		Date lastRefreshDate = officeBuildingDetailDaoImpl.getLastRefreshDate(customerId);
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		try{
		log.debug("OfficeBuildingDetailManagerImpl * dataRefresh * Refresh Data......--------->>>>>>");
		RefreshIncident refreshObj = new RefreshIncident();
		String toDate = new String();
		String fromDate = new String();
		Date todate  = new Date();
		toDate = df.format(todate);
		log.debug("OfficeBuildingDetailManagerImpl * dataRefresh * toDate-----------"+toDate);
		if(lastRefreshDate!=null)
			fromDate = df.format(lastRefreshDate);
		
		log.debug("OfficeBuildingDetailManagerImpl * dataRefresh * fromDate------------"+fromDate);
		refreshObj.refreshIncident(customerId, toDate, fromDate, "CUST");
		log.info("OfficeBuildingDetailManagerImpl * dataRefresh * Exit the method");
		}catch(Exception ex){
			log.error(" Exception in dataRefresh method "+ex.getMessage());			
		}
	}	
}