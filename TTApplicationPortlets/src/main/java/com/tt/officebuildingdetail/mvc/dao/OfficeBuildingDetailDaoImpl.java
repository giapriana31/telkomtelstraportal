package com.tt.officebuildingdetail.mvc.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.model.IncidentDashboardMapping;
import com.tt.model.Site;

@Repository(value="officeBuildingDetailDaoImpl")
@Transactional
public class OfficeBuildingDetailDaoImpl {
	private final static Logger log = Logger.getLogger(OfficeBuildingDetailDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	public String getcommonDBCall(){
		log.debug("OfficeBuildingDetailDaoImpl * getcommonDBCall * Inside the method");
		try{
		Query query = sessionFactory.getCurrentSession().createQuery("select i.category from Incident i where i.assigneeName = 'Vidya'");
		log.debug("OfficeBuildingDetailDaoImpl * getcommonDBCall * query.list().get(0)"+(String) query.list().get(0));
		return (String) query.list().get(0);
		}catch (Exception e) {
		    log.error("Exception in getcommonDBCall method "+e.getMessage());
		}	
		log.debug("OfficeBuildingDetailDaoImpl * getcommonDBCall * Exit the method");
		return null;
	}


	@SuppressWarnings("unchecked")
	public List<IncidentDashboardMapping> getOfficeBuildingFirstGraph(
			String customerId) {
		log.debug("OfficeBuildingDetailDaoImpl * getOfficeBuildingFirstGraph * Inside the method");
		try{
		StringBuilder hql = new StringBuilder();
		hql.append("from IncidentDashboardMapping idm");
		hql.append(" where idm.sitestatus='Operational' and lower(idm.incdcategory) <> :incdcategory and idm.customeruniqueid =:customerId and ");
		hql.append(" lower(idm.incidentstatus) not in ('cancelled','closed') and idm.rootproductid = 'MNS'");
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
		query.setString("incdcategory", "network");
		log.debug("OfficeBuildingDetailDaoImpl * getOfficeBuildingFirstGraph * query.list()---" + query.list() == null ? "NULL" : query.list().toString());
		if( query.list()!=null &&  query.list().size()>0){
			return query.list();	
		}else{
			return null;
		}
		}catch (Exception e) {
		   log.error("Exception in getOfficeBuildingFirstGraph method "+e.getMessage());
		}		
		log.debug("OfficeBuildingDetailDaoImpl * getOfficeBuildingFirstGraph * Exit the method");
		return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Site> getSites(String customerId) {
		log.debug("OfficeBuildingDetailDaoImpl * getSites * Inside the method");
		try{
		StringBuilder hql = new StringBuilder();
		hql.append("from Site sa ");
		hql.append("where sa.isManaged <>'No' and sa.isDataCenter <> 'Yes' and sa.rootProductId = 'MNS' and sa.customeruniqueid=:customerId and status in ('");
	    hql.append(GenericConstants.STATUSOPERATIONAL);
	    hql.append(GenericConstants.SINGLEQUOTECLOSINGBRACE);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql.toString());
		query.setString("customerId", customerId);
	
		log.debug("query.list()---" + query.list() == null ? "NULL" : query.list().toString());
		if( query.list()!=null &&  query.list().size()>0){
			
		
			return query.list();	
		}else
		{
		
			return null;
		}
		}catch (Exception e) {
		
			 log.error("Exception in getSites method "+e.getMessage());
		}	
		log.debug("OfficeBuildingDetailDaoImpl * getSites * Exit the method");
		return null;
	}

	public Date getLastRefreshDate(String customerId) {
		log.debug("OfficeBuildingDetailDaoImpl * getLastRefreshDate * Inside the method");
		try{
		StringBuilder sql = new StringBuilder();
		sql.append("select max(lastrefreshtime) from lastrefreshtime sa");
		sql.append(" where sa.customeruniqueid=:customerId and sa.entityname ='Incident'");
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
		query.setString("customerId", customerId);
		log.debug("query.list()---" + query.list() == null ? "NULL" : query.list().toString());
		if( query.list()!=null &&  query.list().size()>0){
			Date refreshDate = (Date) query.list().get(0);
			return refreshDate;
		}else{
			return null;
		}
     }catch (Exception e) {
		   log.error("Exception in getLastRefreshDate method "+e.getMessage());
		}	
		log.debug("OfficeBuildingDetailDaoImpl * getLastRefreshDate * Exit the method");
		return null;
	}
}
