package com.tt.officebuildingdetail.mvc.web.controller;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.common.subscribedservices.SubscribedService;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tt.common.RefreshUtil;
import com.tt.constants.GenericConstants;
import com.tt.logging.Logger;
import com.tt.officebuildingdetail.mvc.service.common.OfficeBuildingDetailManagerImpl;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

@Controller("officeBuildingSiteMapping")
@RequestMapping("VIEW")
public class OfficeBuildingSiteMappingController {
	private final static Logger log = Logger.getLogger(OfficeBuildingSiteMappingController.class);
	public static Properties prop = PropertyReader.getProperties();
	private OfficeBuildingDetailManagerImpl  officeBuildingDetailManagerImpl;
	
	@Autowired
	@Qualifier("officeBuildingDetailManagerImpl")
	public void setOfficeBuildingDetailManagerImpl(
		OfficeBuildingDetailManagerImpl officeBuildingDetailManagerImpl) {
	    this.officeBuildingDetailManagerImpl = officeBuildingDetailManagerImpl;
	}
	
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException, SystemException {
		log.debug("OfficeBuildingSiteMappingController * handleRenderRequest * Inside the method");
		
		String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(request),PortalUtil.getHttpServletResponse(renderResponse));
		log.debug("OfficeBuildingSiteMappingController * handleRenderRequest * customerId--------------->>"+customerId);
		
		log.debug(" XYZDEBUG OfficeBuildingSiteMappingController * handleRenderRequest * before color map");
		SubscribedService ss = new SubscribedService();

		int count = ss.isSubscribedServices(GenericConstants.PRODUCTTYPE_MNS,customerId);
		
		log.debug("Count of service :: " + count);
		
		if(count == 0)
		{
			model.addAttribute(GenericConstants.TITLE, "SITES");
			return "unsubscribed_mns_service";
		}
		else
		{
	//		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	//		User user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), themeDisplay.getUser().getDisplayEmailAddress());
			try{
	
			Map<String, Map<String, Integer>> colorMap = officeBuildingDetailManagerImpl.getOfficeBuildingFirstGraph(customerId);
			
			log.debug("XYZDEBUG OfficeBuildingSiteMappingController * handleRenderRequest * after color map");
			log.debug("XYZDEBUG OfficeBuildingSiteMappingController * handleRenderRequest * is color map  empty"+colorMap.isEmpty());
	
	
			if(!colorMap.isEmpty())
			for(String Map: colorMap.keySet()){
				for(String innnerMap: colorMap.get(Map).keySet()){
					log.debug("OfficeBuildingSiteMappingController * handleRenderRequest * colorMap Listing--"+innnerMap+"--kgkjkj--"+colorMap.get(Map).get(innnerMap));	
				}
					
			}
			
			
			log.debug("OfficeBuildingSiteMappingController * handleRenderRequest * colorMap --"+colorMap.isEmpty());
	
			Map<String, Integer> silverColor = new HashMap<String, Integer>();
			Map<String, Integer> goldColor = new HashMap<String, Integer>();
			Map<String, Integer> bronzeColor = new HashMap<String, Integer>();
			
			for (String siteTier : colorMap.keySet()) {
				if("Silver".equalsIgnoreCase(siteTier)){
					silverColor.putAll(colorMap.get(siteTier));
				} else if("Gold".equalsIgnoreCase(siteTier)){
					goldColor.putAll(colorMap.get(siteTier));
				} else if("Bronze".equalsIgnoreCase(siteTier)){
					bronzeColor.putAll(colorMap.get(siteTier));
				}
			}
			
			Gson gson = new Gson();
			String ttOfficeBuildingSilverColorMap = gson.toJson(silverColor);
			String ttOfficeBuildingGoldColorMap = gson.toJson(goldColor);
			String ttOfficeBuildingBronzeColorMap = gson.toJson(bronzeColor);
			log.debug("XYZDEBUG OfficeBuildingSiteMappingController * handleRenderRequest * silver map json "+ttOfficeBuildingSilverColorMap );
			log.debug("XYZDEBUG OfficeBuildingSiteMappingController * handleRenderRequest * gold map json "+ttOfficeBuildingGoldColorMap);
			log.debug("XYZDEBUG OfficeBuildingSiteMappingController * handleRenderRequest * bronze map json "+ttOfficeBuildingBronzeColorMap);
	
			log.debug("OfficeBuildingSiteMappingController * handleRenderRequest * ttOfficeBuildingSilverColorMap--"+ttOfficeBuildingSilverColorMap.toString());
			model.addAttribute("ttOfficeBuildingSilverColorMap", ttOfficeBuildingSilverColorMap);
			model.addAttribute("ttOfficeBuildingGoldColorMap", ttOfficeBuildingGoldColorMap);
			model.addAttribute("ttOfficeBuildingBronzeColorMap", ttOfficeBuildingBronzeColorMap);
			
			
			}catch(Exception e){
				log.error("Exception in handleRenderRequest method "+e.getMessage());
			}
			log.debug("OfficeBuildingSiteMappingController * handleRenderRequest * Exit the method");
			return "incidentList";
		}
	}
	
	@ActionMapping(value = "refresh")
	public void dataRefresh(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException, PortalException, SystemException{
		log.debug("OfficeBuildingSiteMappingController * dataRefresh * Inside the method");
		try{
		
		String customerId = TTGenericUtils.getCustomerIDFromSession(PortalUtil.getHttpServletRequest(actionRequest),PortalUtil.getHttpServletResponse(actionResponse));
		log.debug("OfficeBuildingSiteMappingController * dataRefresh * customerId--------------->>"+customerId);
		officeBuildingDetailManagerImpl.dataRefresh(customerId);
		
		
		String url = prop.getProperty("tt.homeURL") + "/" + prop.getProperty("ttDashboardPageName");
		log.debug("OfficeBuildingSiteMappingController * dataRefresh * Exit the method");
		actionResponse.sendRedirect(url);
	}
		catch(Exception e){
			log.error("Exception in dataRefresh method "+e.getMessage());
		}
	}

}
