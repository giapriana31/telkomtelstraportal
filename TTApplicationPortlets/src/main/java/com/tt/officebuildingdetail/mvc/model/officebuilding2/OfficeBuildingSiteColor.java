package com.tt.officebuildingdetail.mvc.model.officebuilding2;

public class OfficeBuildingSiteColor {
	
	private String siteName;
	private String colorCode;
	
	
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
}
