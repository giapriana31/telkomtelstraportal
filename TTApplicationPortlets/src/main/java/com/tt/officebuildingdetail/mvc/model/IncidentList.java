package com.tt.officebuildingdetail.mvc.model;

import java.util.List;

import com.tt.model.Incident;

public class IncidentList {

	private List<Incident> incidents;

	public List<Incident> getIncidents() {
		return incidents;
	}

	public void setIncidents(List<Incident> incidents) {
		this.incidents = incidents;
	}
	
	
}
