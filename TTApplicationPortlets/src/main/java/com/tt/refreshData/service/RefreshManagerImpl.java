package com.tt.refreshData.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.RefreshIncident;
import com.tt.refreshData.dao.RefreshDaoImpl;


@Service("refreshManagerImpl")
@Transactional
public class RefreshManagerImpl {

    private RefreshDaoImpl refreshDaoImpl;

    @Autowired
    @Qualifier("refreshDaoImpl")
    public void setRefreshDaoImpl(RefreshDaoImpl refreshDaoImpl) {
	this.refreshDaoImpl = refreshDaoImpl;
    }

    /**
     * OnLoad Refresh Date
     * 
     * @param customerId
     * @return
     */
    public String getLastRefreshDate(String customerId) {

	Date lastRefreshDate = refreshDaoImpl.getLastRefreshDate(customerId, "Incident");

	DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
	TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Jakarta");
	df.setTimeZone(timeZone1);

	if (lastRefreshDate != null) {
	    String fromDate = df.format(lastRefreshDate);
	    return fromDate;
	}
	return null;
    }

    /**
     * refresh mechanism for Incidents
     * @param customerId
     */
    public void dataRefresh(String customerId) {

	Date lastRefreshDate = refreshDaoImpl.getLastRefreshDate(customerId, "Incident");

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	df.setTimeZone(TimeZone.getTimeZone("GMT"));
	try {
	    System.out.println("Refresh Data......--------->>>>>>");
	    RefreshIncident refreshObj = new RefreshIncident();
	    String toDate = new String();
	    String fromDate = new String();
	    Date todate = new Date();
	    toDate = df.format(todate);
	    System.out.println("toDate-----------" + toDate);
	    if (lastRefreshDate != null)
	    	fromDate = df.format(lastRefreshDate);
	    else 
	    	fromDate = "2015-01-01 00:00:00";

	    System.out.println("fromDate------------" + fromDate);
	    refreshObj.refreshIncident(customerId, toDate, fromDate, "CUST");
	    System.out.println("Refresh Data Done......--------->>>>>>");
	} catch (Exception ex) {
	    System.out.println(" Exception while calling data refresh ");
	    ex.printStackTrace();
	}
    }



}
