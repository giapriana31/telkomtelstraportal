package com.tt.refreshData.dao;

import java.util.Date;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("refreshDaoImpl")
@Transactional
public class RefreshDaoImpl {

	@Autowired
	private SessionFactory sessionFactory;

	public Date getLastRefreshDate(String customerId, String entityType) {

		StringBuilder sql = new StringBuilder();
		sql.append("select max(lastrefreshtime) from lastrefreshtime sa");
		
		if(entityType!=null && ("Customer".equalsIgnoreCase(entityType) || "Site".equalsIgnoreCase(entityType)))
		    sql.append(" where sa.entityname=:entityType");
		else
		    sql.append(" where sa.customeruniqueid=:customerId and entityname=:entityType");
		
		
		Query query = sessionFactory.getCurrentSession().createSQLQuery(sql.toString());
		if("Incident".equalsIgnoreCase(entityType))
		    query.setString("customerId", customerId);
		query.setString("entityType", entityType);
		if( query.list()!=null &&  query.list().size()>0){
			Date refreshDate = (Date) query.list().get(0);
			return refreshDate;
		}else{
			return null;
		}
		
	}
	
	


}
