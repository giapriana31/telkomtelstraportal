package com.tt.refreshData.web.controller;

import java.io.IOException;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.tt.refreshData.service.RefreshManagerImpl;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;


@Controller("updateDataController")
@RequestMapping("VIEW")
public class TTUpdateDataController {
	
	
	private RefreshManagerImpl refreshManagerImpl;
	public static Properties prop = PropertyReader.getProperties();
	
	@Autowired
	@Qualifier("refreshManagerImpl")
	public void setRefreshManagerImpl(RefreshManagerImpl refreshManagerImpl) {
		this.refreshManagerImpl = refreshManagerImpl;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws Exception{
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		User user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), themeDisplay.getUser().getDisplayEmailAddress());
		String customerId = (String)user.getExpandoBridge().getAttribute("CustomerUniqueID");
		String lastRefreshDate = refreshManagerImpl.getLastRefreshDate(customerId);
		
		model.addAttribute("lastRefreshDate", lastRefreshDate);
		
		ThemeDisplay themeDisplayRender = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String currentURL = themeDisplayRender.getURLCurrent();
		System.out.println("Current URL :: " + currentURL);

		String homeURL = "home1";
		if(currentURL.contains(homeURL))
		{
		return "homeUpdate";
		}
		else{
		return "update";
		}
		
	}
	
	@ActionMapping(value = "refresh")
	public void dataRefresh(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException, PortalException, SystemException{
		
		System.out.println("DEmooooooo.------------------");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User user = UserLocalServiceUtil.getUserByEmailAddress(themeDisplay.getCompanyId(), themeDisplay.getUser().getDisplayEmailAddress());
		String customerId = (String)user.getExpandoBridge().getAttribute("CustomerUniqueID");
		System.out.println("customerId--------------->>"+customerId);
		refreshManagerImpl.dataRefresh(customerId);
		
		String url = prop.getProperty("tt.homeURL") + "/" + prop.getProperty("ttHomePageName");
		actionResponse.sendRedirect(url);
	}
	

	
	
}