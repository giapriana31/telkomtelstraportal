package com.tt.logticket.mvc.web.controller;

import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;



/*import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;*/
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.client.model.incidentapi.InsertResponse;
import com.tt.client.service.incident.IncidentWSCallService;
import com.tt.logticket.mvc.service.common.CommonManagerImpl;
import com.tt.model.Incident;
import com.tt.model.IncidentDashboardMapping;
import com.tt.utils.RefreshUtil;


@Controller("logIncidentController")
@RequestMapping("VIEW")
@Configuration
@PropertySource("classpath:config_logTicket.properties")
public class TTLogIncidentController {
	
	private CommonManagerImpl commonManager;
	
	@Autowired
	private Environment env;
	
	/*private static final Logger LOGGER = Logger.getLogger(TTLogIncidentController.class);*/
	
	@Autowired
	@Qualifier("commonManager")
	public void setCommonManager(CommonManagerImpl commonManager) {
		this.commonManager = commonManager;
	}
	
	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse renderResponse, Model model) throws Exception{
		try{
			/*LOGGER.info("Testing Logger");*/
			ThemeDisplay td  = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
			User user = UserLocalServiceUtil.getUserByEmailAddress(td.getCompanyId(), td.getUser().getDisplayEmailAddress());
			String customerUniqueID = (String)user.getExpandoBridge().getAttribute("CustomerUniqueID");
			if(customerUniqueID != null){
			System.out.println("cust Id " + customerUniqueID);
			
			LinkedHashMap<String, String> siteNameList = commonManager.getSiteNameMap(customerUniqueID);
				
			String[] urgencyList = env.getProperty("UrgencyList").split(",");
			String[] impactList = env.getProperty("ImpactList").split(",");
			
			LinkedHashMap<String, String> urgencyListMap = new LinkedHashMap<String, String>();
			urgencyListMap.put("0", "--Select--");
			for (int i =0;i<urgencyList.length;i++) {
				urgencyListMap.put(urgencyList[i], urgencyList[i]);
			}
			
			LinkedHashMap<String, String> impactListMap = new LinkedHashMap<String, String>();
			impactListMap.put("0", "--Select--");
			for (int i =0;i<impactList.length;i++) {
				impactListMap.put(impactList[i], impactList[i]);
			}
			
			//For getting count of P1P2 & P3P4 - Start
			LinkedHashMap<String, String> countPriorityMap = commonManager.getTicketPriorityCount(customerUniqueID);
			System.out.println("map "+countPriorityMap+" map");
			for( Map.Entry<String,String> entry : countPriorityMap.entrySet()){
				model.addAttribute(entry.getKey(), entry.getValue());
			}
			//For getting count of P1P2 & P3P4 - End
			
			model.addAttribute("logIncident", new Incident());
			if(siteNameList != null && siteNameList.size()!=0){
				LinkedHashMap<String, String> siteNameListActual = new LinkedHashMap<String, String>();
				siteNameListActual.put("0", "--Select--");
				for( Map.Entry<String,String> entry : siteNameList.entrySet()){
					siteNameListActual.put(entry.getKey(), entry.getValue());
					/*responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");*/
				}
				model.addAttribute("siteNameList", siteNameListActual);
			}
			
			LinkedHashMap<String, String> serviceIDList = new LinkedHashMap<String, String>();
			serviceIDList.put("0", "--Select--");
			model.addAttribute("LogIncidentDisclaimerMsg", env.getProperty("LogIncidentDisclaimerMsg"));
			model.addAttribute("serviceIDList", serviceIDList);
			model.addAttribute("urgencyListMap", urgencyListMap);
			model.addAttribute("impactListMap", impactListMap);
			model.addAttribute("UrgencyDescription", env.getProperty("UrgencyDescription"));
			model.addAttribute("ImpactDescription", env.getProperty("ImpactDescription"));
			model.addAttribute("incidentRowDetailsList", getDashTicketDetails(customerUniqueID)); //Row details
			}
			else{
				Incident incident = new Incident();
				model.addAttribute("logIncident", incident);
			}
			return "logTicket";
		}
		catch (Exception e){
			System.out.println("Error in controller: " + e.toString());
			/*LOGGER.error("Error in Render- " + e.toString());*/
			e.printStackTrace();
			throw e;
		}
	}
	
	@ResourceMapping(value="submitRequestURL")
	public void handleLogIncidentRequest(@Valid @ModelAttribute("logIncident") Incident incident, Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		try{
			JSONObject json = new JSONObject();
			ThemeDisplay td  = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
			User user = UserLocalServiceUtil.getUserByEmailAddress(td.getCompanyId(), td.getUser().getDisplayEmailAddress());
			/*String customerUniqueID = (String)user.getExpandoBridge().getAttribute("CustomerUniqueID");*/
			System.out.println("summary-" + incident.getSummary());
			System.out.println("notes-"+ incident.getWorklognotes());
			System.out.println("site name-" + incident.getSitename());
			System.out.println("impact-" + incident.getImpact());
			System.out.println("urgency-" + incident.getUrgency());
			System.out.println("service id -" + incident.getServiceid());
			
			RefreshUtil.setProxyObject();
			
			model.addAttribute("logIncident", incident);
			
			System.out.println("Caller - " + user.getFullName());
			incident.setTickettype(env.getProperty("ticketTypeProperty"));
			incident.setTicketsource(env.getProperty("ticketSourceProperty"));
			incident.setUserid(user.getEmailAddress()+"");
			/*incident.setUserid("abel.tuter");*/
			/*System.out.println("User ID: " + user.getUserId());*/
			
			/*incident.setServiceid("MNS 45");*/ //Remove the hardcoding once perfect data comes in DB
			/*incident.setServiceid("MNS 45"); */
			incident.setResolvergroupname("Level 1 Group");
					
			incident.setCustomerpreferedlanguage((String)user.getExpandoBridge().getAttribute("preferredLanguage"));
			
			IncidentWSCallService callService = new IncidentWSCallService();
			InsertResponse insertResponse= callService.insertIncidentWebService(incident);
			
			System.out.println("Display Response: " + insertResponse.getDisplayValue());
			System.out.println("Error Response: " + insertResponse.getErrorMessage());
			if(insertResponse.getDisplayValue() !=null && insertResponse.getDisplayValue().trim().length()!=0){
				json.put("incidentID", "Success@"+insertResponse.getDisplayValue());
			}
			else{
				json.put("incidentID", "Error@"+insertResponse.getErrorMessage());
			}
			/*json.put("incidentID", insertResponse.getDisplayValue());*/
			PrintWriter out = response.getWriter();
			out.print(json.toString());
			
		}
		catch(Exception e){
			e.printStackTrace();
			/*LOGGER.error("Error while Processing Submit- " + e.toString());*/
			throw e;
		}
		
	}
	
	@ResourceMapping(value="getServiceURL")
	public void handleSericeIDFetch(Model model,ResourceRequest request, ResourceResponse response) throws Exception{
		HttpServletRequest req = PortalUtil.getHttpServletRequest(request);
        /*HttpServletResponse res= PortalUtil.getHttpServletResponse(response);*/
		try{
			/*List<ServiceMap> serviceObjectList = new ArrayList<ServiceMap>();*/
			JSONObject jsonObj = new JSONObject(req.getParameter("selectedSiteIDJson"));
			String selectedSiteID = jsonObj.getString("selectedSiteID");
			if(selectedSiteID!=null && selectedSiteID.trim().length()!=0){
				System.out.println("Selected Site ID in controller : " + selectedSiteID);
				LinkedHashMap<String, String> serviceIDListtemp = commonManager.getServiceFromSiteID(selectedSiteID);
				
				String responseStr = "";
				if(serviceIDListtemp!=null){
					for( Map.Entry<String,String> entry : serviceIDListtemp.entrySet()){
						responseStr += (entry.getKey()+"@@@@@"+entry.getValue()+"#####");
					}
					PrintWriter out = response.getWriter();
					out.print(responseStr);
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			/*LOGGER.error("Error while Getting Service Names from Site- " + e.toString());*/
			throw e;
		}
	}
	
	
	public List<Incident> getDashTicketDetails(String customerUniqueID) throws Exception{
		try{	
				Date date = new Date();
				java.sql.Timestamp timeStampDate;
				List<IncidentDashboardMapping> incList = commonManager.getTicketSummaryIncidentList(customerUniqueID);
				System.out.println("at controller called function return data from service call - "+incList);
				List<Incident> incidentList = new ArrayList<Incident>();
				for(int i = 0; i< Integer.parseInt(env.getProperty("countShowTicketsDashboard")); i++){
					timeStampDate = new Timestamp(date.getTime());
					Incident incident = new Incident();
					Long minutesDiff =  ((incList.get(i).getServiceleveltarget().getTime())/60000)-((timeStampDate.getTime())/60000);
					Long hour = minutesDiff/60;
					Long hourTemp = minutesDiff/60;
					Long min = minutesDiff%60;
					String hourStr = hour.toString().replace("-", "");
					String minStr = min.toString().replace("-", "");
					if(hourStr.trim().length()==1){
						/*System.out.println("Hour: " + hourTemp);*/
						if(hourTemp.toString().contains("-")){
							hourStr="-0"+hourStr;
						}else{
							hourStr=" 0"+hourStr;
						}
						
					}
					else{
						hourStr=" "+hourTemp.toString();
					}
					if(minStr.trim().length()==1){
						minStr="0"+minStr;
					}
					incident.setDueByTime(hourStr+":"+minStr);
					incident.setSummary(incList.get(i).getSummary());
					incident.setPriority(Long.parseLong(incList.get(i).getPriority()));
					/*incList.get(i).setDueByTime(hour+":"+minStr);*/
					if(incList.get(i).getHierEscalationFlag() || (!incList.get(i).getHierEscalationFlag() && incList.get(i).getMIM().toUpperCase().equals(env.getProperty("checkMIMFlag")))){
						incident.setShowEscalationFlag(true);
						/*incList.get(i).setShowEscalationFlag(true);*/
					}
					else{
						incident.setShowEscalationFlag(false);
						/*incList.get(i).setShowEscalationFlag(false);*/
					}
					if(incList.get(i).getCustomerimpacted().equals("TRUE")){
						incident.setCustomerimpacted(true);
					}
					else{
						incident.setCustomerimpacted(false);
					}
					incidentList.add(incident);
				}
			return incidentList;
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public List<Incident> sortOnPriority(List<Incident> incidentList) throws Exception{
		try{
			List<Incident> returnIncidentList = new ArrayList<Incident>();
			for(int i = 0 ; i<incidentList.size();i++){
				if(incidentList.get(i).getPriority().equals(incidentList.get(i+1).getPriority())){
					/*if(){
						
					}
					else{
						
					}*/
				}
				else{
					returnIncidentList.add(incidentList.get(i));
				}
			}
			return returnIncidentList;
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
}