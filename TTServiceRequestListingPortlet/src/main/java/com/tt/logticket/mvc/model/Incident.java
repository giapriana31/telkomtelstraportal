package com.tt.logticket.mvc.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="incidence")
public class Incident implements Serializable {
	@Id
	private Long id;
	@Column(name="number")
	private String incidentId;//Number
	@Column(name="caller")
	private String caller;
	@Column(name="priority")
	private Long priority;
	@Column(name="impact")
	private String impact;
	@Column(name="urgency")
	private String urgency;
	@Column(name="category")
	private String category;
	@Column(name="location")
	private String location;
	@Column(name="customername")
	private String customerName;
	@Column(name="customeruniqueid")
	private String customerId;//Customer Unique ID
	@Column(name="externalcustomerid")
	private String externalCustomerId;
	@Column(name="affecteddeviceci")
	private String affectedDeviceCI;
	@Column(name="ticketstatus")
	private String ticketStatus;
	@Column(name="statusreason")
	private String statusReason;
	@Column(name="affectedbusinessservices")
	private String affectedBusinessServices;
	@Column(name="summary")
	private String summary;// Summary (Short Description) of Symptom
	@Column(name="notes")
	private String notes;//Notes(Description)
	@Column(name="tickettype")
	private String ticketType;
	@Column(name="ticketsource")
	private String ticketSource;
	@Column(name="mim")
	private String mim;
	@Column(name="customerimpacted")
	private Boolean customerImpacted;
	@Column(name="siteid")
	private String siteId;
	@Column(name="sitename")
	private String siteName;
	@Column(name="sitezone")
	private String siteZone;
	@Column(name="siteservicetier")
	private String siteServiceTier;
	@Column(name="timezone")
	private String timeZone;
	@Column(name="customerpreferredlanguage")
	private String preferredLanguage;//Customer Preferred Language
	@Column(name="subcategory")
	private String subcategory;
	@Column(name="assignmentresolvergroupname")
	private String assignmentResolverGroupName;
	@Column(name="assigneename")
	private String assigneeName;
	@Column(name="watchlist")
	private String watchList;
	@Column(name="worknotes")
	private String workNotesList;
	@Column(name="additionalcomments")
	private String additionalcomments;
	@Column(name="opendatetime")
	private String openDateTime;
	@Column(name="openedby")
	private String openedBy;
	@Column(name="reporteddatetime")
	private String reportedDateTime;
	@Column(name="pendingclock")
	private String pendingClock;
	@Column(name="userid")
	private String userId;
	@Transient
	private String userName;
	@Column(name="fullname")
	private String fullName;
	@Transient
	private String cmdbClass;
	@Transient
	private String colorcode;
	/*@Transient
	private Boolean customerImpactedCheck;*/
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getIncidentId() {
		return incidentId;
	}
	public void setIncidentId(String incidentId) {
		this.incidentId = incidentId;
	}
	
	public String getCaller() {
		return caller;
	}
	public void setCaller(String caller) {
		this.caller = caller;
	}
	
	public Long getPriority() {
		return priority;
	}
	public void setPriority(Long priority) {
		this.priority = priority;
	}
	
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	
	public String getUrgency() {
		return urgency;
	}
	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	public String getExternalCustomerId() {
		return externalCustomerId;
	}
	public void setExternalCustomerId(String externalCustomerId) {
		this.externalCustomerId = externalCustomerId;
	}
	
	public String getAffectedDeviceCI() {
		return affectedDeviceCI;
	}
	public void setAffectedDeviceCI(String affectedDeviceCI) {
		this.affectedDeviceCI = affectedDeviceCI;
	}
	
	public String getTicketStatus() {
		return ticketStatus;
	}
	public void setTicketStatus(String ticketStatus) {
		this.ticketStatus = ticketStatus;
	}
	
	public String getStatusReason() {
		return statusReason;
	}
	public void setStatusReason(String statusReason) {
		this.statusReason = statusReason;
	}
	
	public String getAffectedBusinessServices() {
		return affectedBusinessServices;
	}
	public void setAffectedBusinessServices(String affectedBusinessServices) {
		this.affectedBusinessServices = affectedBusinessServices;
	}
	
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	
	public String getTicketSource() {
		return ticketSource;
	}
	public void setTicketSource(String ticketSource) {
		this.ticketSource = ticketSource;
	}
	
	public String getMim() {
		return mim;
	}
	public void setMim(String mim) {
		this.mim = mim;
	}
	
	
	/*public String getCustomerImpacted() {
		return customerImpacted;
	}
	public void setCustomerImpacted(String customerImpacted) {
		this.customerImpacted = customerImpacted;
	}*/
	
	public Boolean getCustomerImpacted() {
		return customerImpacted;
	}
	public void setCustomerImpacted(Boolean customerImpacted) {
		this.customerImpacted = customerImpacted;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteZone() {
		return siteZone;
	}
	public void setSiteZone(String siteZone) {
		this.siteZone = siteZone;
	}
	
	public String getSiteServiceTier() {
		return siteServiceTier;
	}
	public void setSiteServiceTier(String siteServiceTier) {
		this.siteServiceTier = siteServiceTier;
	}
	
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
	public String getPreferredLanguage() {
		return preferredLanguage;
	}
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	
	public String getSubcategory() {
		return subcategory;
	}
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	
	public String getAssignmentResolverGroupName() {
		return assignmentResolverGroupName;
	}
	public void setAssignmentResolverGroupName(String assignmentResolverGroupName) {
		this.assignmentResolverGroupName = assignmentResolverGroupName;
	}
	
	public String getAssigneeName() {
		return assigneeName;
	}
	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}
	
	public String getWatchList() {
		return watchList;
	}
	public void setWatchList(String watchList) {
		this.watchList = watchList;
	}
	
	public String getWorkNotesList() {
		return workNotesList;
	}
	public void setWorkNotesList(String workNotesList) {
		this.workNotesList = workNotesList;
	}
	
	public String getAdditionalcomments() {
		return additionalcomments;
	}
	public void setAdditionalcomments(String additionalcomments) {
		this.additionalcomments = additionalcomments;
	}
	
	public String getOpenDateTime() {
		return openDateTime;
	}
	public void setOpenDateTime(String openDateTime) {
		this.openDateTime = openDateTime;
	}
	
	public String getOpenedBy() {
		return openedBy;
	}
	public void setOpenedBy(String openedBy) {
		this.openedBy = openedBy;
	}
	
	public String getReportedDateTime() {
		return reportedDateTime;
	}
	public void setReportedDateTime(String reportedDateTime) {
		this.reportedDateTime = reportedDateTime;
	}
	
	public String getPendingClock() {
		return pendingClock;
	}
	public void setPendingClock(String pendingClock) {
		this.pendingClock = pendingClock;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	//@Column(name="")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public String getCmdbClass() {
		return cmdbClass;
	}
	public void setCmdbClass(String cmdbClass) {
		this.cmdbClass = cmdbClass;
	}
	
	public String getColorcode() {
		return colorcode;
	}
	public void setColorcode(String colorcode) {
		this.colorcode = colorcode;
	}
	/*public Boolean getCustomerImpactedCheck() {
		return customerImpactedCheck;
	}
	public void setCustomerImpactedCheck(Boolean customerImpactedCheck) {
		this.customerImpactedCheck = customerImpactedCheck;
	}*/
	
}
