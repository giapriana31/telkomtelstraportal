package com.tt.logticket.mvc.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="site")
public class Site {
	
	@Column(name="customersite")
	private String customerSiteName;
	@Column(name="Customer_Name")
	private String customerName;
	@Column(name="ID")
	private String iD;
	@Column(name="City")
	private String city;
	@Column(name="Country")
	private String country;
	@Column(name="Customer_Contact")
	private String customerContact;
	
	@Id
	@Column(name="siteid")
	private String customerSiteID;
	@Column(name="Fax")
	private String fax;
	@Column(name="Phone")
	private String phone;
	@Column(name="Service_Tier")
	private String serviceTier;
	@Column(name="Site_Location_(Latitude)")
	private String siteLocationLat;
	@Column(name="Site_Location_(Longitude)")
	private String siteLocationLong;
	@Column(name="Site_operational_hours")
	private String siteOperationalHour;
	@Column(name="State/Province")
	private String province;
	@Column(name="Street")
	private String street;
	@Column(name="Time_Zone")
	private String timeZone;
	@Column(name="Zip/Postal_Code")
	private String postalCode;
	@Column(name="Zone")
	private String zone;
	@Column(name="Owner_Name")
	private String ownerName;
	@Column(name="Owner_Alias")
	private String ownerAlias;
	@Column(name="Created_By")
	private String createdBy;
	@Column(name="Owner_Role")
	private String ownerRole;
	@Column(name="Created_Alias")
	private String createdAlias;
	@Column(name="Created_Date")
	private String createdDate;
	@Column(name="Last_Modified_By")
	private String lastModifiedBy;
	@Column(name="Last_Activity_Date")
	private String lastActivityDate;
	@Column(name="Last_Modified_Date")
	private String lastModifiedDate;
	@Column(name="Last_Modified_Alias")
	private String lastModifiedAlias;
	@Column(name="customeruniqueid")
	private String customerUniqueID;
	public String getCustomerSiteName() {
		return customerSiteName;
	}
	public void setCustomerSiteName(String customerSiteName) {
		this.customerSiteName = customerSiteName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getiD() {
		return iD;
	}
	public void setiD(String iD) {
		this.iD = iD;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(String customerContact) {
		this.customerContact = customerContact;
	}
	public String getCustomerSiteID() {
		return customerSiteID;
	}
	public void setCustomerSiteID(String customerSiteID) {
		this.customerSiteID = customerSiteID;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getServiceTier() {
		return serviceTier;
	}
	public void setServiceTier(String serviceTier) {
		this.serviceTier = serviceTier;
	}
	public String getSiteLocationLat() {
		return siteLocationLat;
	}
	public void setSiteLocationLat(String siteLocationLat) {
		this.siteLocationLat = siteLocationLat;
	}
	public String getSiteLocationLong() {
		return siteLocationLong;
	}
	public void setSiteLocationLong(String siteLocationLong) {
		this.siteLocationLong = siteLocationLong;
	}
	public String getSiteOperationalHour() {
		return siteOperationalHour;
	}
	public void setSiteOperationalHour(String siteOperationalHour) {
		this.siteOperationalHour = siteOperationalHour;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	public String getOwnerAlias() {
		return ownerAlias;
	}
	public void setOwnerAlias(String ownerAlias) {
		this.ownerAlias = ownerAlias;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getOwnerRole() {
		return ownerRole;
	}
	public void setOwnerRole(String ownerRole) {
		this.ownerRole = ownerRole;
	}
	public String getCreatedAlias() {
		return createdAlias;
	}
	public void setCreatedAlias(String createdAlias) {
		this.createdAlias = createdAlias;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public String getLastActivityDate() {
		return lastActivityDate;
	}
	public void setLastActivityDate(String lastActivityDate) {
		this.lastActivityDate = lastActivityDate;
	}
	public String getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getLastModifiedAlias() {
		return lastModifiedAlias;
	}
	public void setLastModifiedAlias(String lastModifiedAlias) {
		this.lastModifiedAlias = lastModifiedAlias;
	}
	public String getCustomerUniqueID() {
		return customerUniqueID;
	}
	public void setCustomerUniqueID(String customerUniqueID) {
		this.customerUniqueID = customerUniqueID;
	}
	
	
	
}

