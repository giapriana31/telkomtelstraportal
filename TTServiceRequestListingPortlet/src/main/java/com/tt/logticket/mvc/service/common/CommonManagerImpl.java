package com.tt.logticket.mvc.service.common;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tt.logticket.mvc.dao.CommonDaoImpl;
import com.tt.model.Incident;
import com.tt.model.IncidentDashboardMapping;


@Service(value="commonManager")
@Transactional
public class CommonManagerImpl {

	private CommonDaoImpl commonDaoImpl;

	@Autowired
	@Qualifier("commonDaoImpl")
	public void setCommonDaoImpl(CommonDaoImpl commonDaoImpl) {
		this.commonDaoImpl = commonDaoImpl;
	}
	
		
	public LinkedHashMap<String, String> getTicketPriorityCount(String customeruniqueId){
		return commonDaoImpl.getCombinedPriorityCount(customeruniqueId);
	}
	
	public List<IncidentDashboardMapping> getTicketSummaryIncidentList(String customeruniqueId/*, String countShotTicket*/) throws Exception{
		try{
			return commonDaoImpl.getTicketSummaryIncidentList(customeruniqueId/*, countShotTicket*/);
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public LinkedHashMap<String, String> getSiteNameMap(String customeruniqueID) throws Exception{
		try{
			return commonDaoImpl.getSiteNameMap(customeruniqueID);
		}
		catch(Exception e){
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getServiceFromSiteID(String siteID) throws Exception{
		try{
			return commonDaoImpl.getServiceFromSiteID(siteID);
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
}
