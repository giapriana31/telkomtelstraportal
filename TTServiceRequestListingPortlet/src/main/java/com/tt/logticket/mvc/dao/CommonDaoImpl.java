package com.tt.logticket.mvc.dao;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.tt.model.Incident;
import com.tt.model.IncidentDashboardMapping;

@Repository(value="commonDaoImpl")
@Transactional
@Configuration
@PropertySource("classpath:config_logTicket.properties")
public class CommonDaoImpl {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private SessionFactory sessionFactory;

	public LinkedHashMap<String, String> getCombinedPriorityCount(String customeruniqueID){
		Query query1 = sessionFactory.getCurrentSession().createQuery("select count(*) from Incident i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId").setString("custId", customeruniqueID).setBoolean("cimpacted", true);
		Query query2 = sessionFactory.getCurrentSession().createQuery("select count(*) from Incident i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId").setString("custId", customeruniqueID).setBoolean("cimpacted", false);
		/*Query query1 = sessionFactory.getCurrentSession().createQuery("select count(*) from IncidentDashboardMapping i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId").setString("cimpacted", "TRUE").setString("custId", customeruniqueID);
		Query query2 = sessionFactory.getCurrentSession().createQuery("select count(*) from IncidentDashboardMapping i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId").setString("cimpacted", "FALSE").setString("custId", customeruniqueID);*/
		LinkedHashMap<String, String> countPriorityMap = new LinkedHashMap<String, String>();
		/*System.out.println(query1);*/
		if(null != query1.uniqueResult()){
		countPriorityMap.put("countP1P2", (Long)query1.uniqueResult()+"");
		countPriorityMap.put("countP3P4", (Long)query2.uniqueResult()+"");
		}
		else{
			countPriorityMap.put("countP1P2", "0");
			countPriorityMap.put("countP3P4", "0");
		}
		return countPriorityMap;
	}
	
	public List<IncidentDashboardMapping> getTicketSummaryIncidentList(String customeruniqueID/*, String countShotTicket*/) throws Exception{
		try{
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date date = new Date();
			java.sql.Timestamp timeStampDate;
			List<IncidentDashboardMapping> incList = new ArrayList<IncidentDashboardMapping>();
			/*System.out.println("Count in DAO: "+env.getProperty("countShowTicketsDashboard"));*/
			Query query1 = sessionFactory.getCurrentSession().createQuery("select i.priority, i.summary, i.serviceleveltarget, i.hierEscalationFlag, i.MIM from Incident i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId order by priority asc limit 5").setBoolean("cimpacted", true).setString("custId", customeruniqueID);
			Query query2 = sessionFactory.getCurrentSession().createQuery("select i.priority, i.summary, i.serviceleveltarget, i.hierEscalationFlag, i.MIM from Incident i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId order by priority asc limit 5").setBoolean("cimpacted", false).setString("custId", customeruniqueID);
			/*Query query1 = sessionFactory.getCurrentSession().createQuery("select i.priority, i.summary, i.serviceleveltarget, i.hierEscalationFlag, i.MIM, i.citype from IncidentDashboardMapping i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId order by priority asc limit 5").setString("cimpacted", "TRUE").setString("custId", customeruniqueID);
			Query query2 = sessionFactory.getCurrentSession().createQuery("select i.priority, i.summary, i.serviceleveltarget, i.hierEscalationFlag, i.MIM, i.citype from IncidentDashboardMapping i where i.customerimpacted=:cimpacted and i.customeruniqueid=:custId order by priority asc limit 5").setString("cimpacted", "FALSE").setString("custId", customeruniqueID);*/
			
			for(Iterator it=query1.iterate();it.hasNext();){
				Object[] row = (Object[]) it.next();
				IncidentDashboardMapping incident = new IncidentDashboardMapping();
				 incident.setPriority(row[0]+"");
				 incident.setSummary(row[1]+"");
				 date = formatter.parse(row[2]+"");
			     timeStampDate = new Timestamp(date.getTime());
				 incident.setServiceleveltarget(timeStampDate);
				 incident.setCustomerimpacted("TRUE");
				 if(row[3].toString().toUpperCase().equals("TRUE"))
				 {
					 incident.setHierEscalationFlag(true);
				 }
				 else{
					 incident.setHierEscalationFlag(false);
				 }
				 incident.setMIM(row[4]+"");
				 incList.add(incident);
			}
			for(Iterator it=query2.iterate();it.hasNext();){
				 Object[] row = (Object[]) it.next();
				 IncidentDashboardMapping incident = new IncidentDashboardMapping();
				 incident.setPriority(row[0]+"");
				 incident.setSummary(row[1]+"");
				 date = formatter.parse(row[2]+"");
			     timeStampDate = new Timestamp(date.getTime());
				 incident.setServiceleveltarget(timeStampDate);
				 incident.setCustomerimpacted("FALSE");
				 if(row[3].toString().toUpperCase().equals("TRUE"))
				 {
					 incident.setHierEscalationFlag(true);
				 }
				 else{
					 incident.setHierEscalationFlag(false);
				 }
				 incident.setMIM(row[4]+"");
				 incList.add(incident);
			}
			return incList;
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
	
	public LinkedHashMap<String,String> getSiteNameMap(String customeruniqueID) throws Exception{
		try{
			LinkedHashMap<String, String> siteNameIDMap = new LinkedHashMap<String, String>();
			Query query1 = sessionFactory.getCurrentSession().createQuery("select s.customersite, s.siteid from Site s where s.customeruniqueid=:custId").setString("custId", customeruniqueID);
			/*Query query1 = sessionFactory.getCurrentSession().createQuery("select s.sitename, s.siteid from IncidentDashboardMapping s where s.customeruniqueid=:custId").setString("custId", customeruniqueID);*/
			/*siteNameIDMap.put("0", "--Select--");*/
			for(Iterator it=query1.iterate();it.hasNext();){
				 Object[] row = (Object[]) it.next();
				 siteNameIDMap.put(row[1]+"", row[0]+"-"+row[1]);
				 
			}
			return siteNameIDMap;
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		
		
	}
	public LinkedHashMap<String,String> getServiceFromSiteID(String siteID) throws Exception{
		try{
			LinkedHashMap<String, String> serviceIdMap = new LinkedHashMap<String, String>();
			/*System.out.println("1");*/
			Query query1 = sessionFactory.getCurrentSession().createQuery("select c.ciid, c.ciname from Configuration c where c.siteid=:siteID and c.citype=:ciType").setString("siteID",siteID).setString("ciType", env.getProperty("ciType"));
			/*Query query1 = sessionFactory.getCurrentSession().createQuery("select c.serviceid, c.ciname from IncidentDashboardMapping c where c.siteid=:siteID and c.citype=:ciType").setString("siteID",siteID).setString("ciType", env.getProperty("ciType"));*/
			/*System.out.println("2");*/
			for(Iterator it=query1.iterate();it.hasNext();){
				 Object[] row = (Object[]) it.next();
				 serviceIdMap.put(row[0]+"", row[1]+"");
				 /*System.out.println("CIID : " + row[0] + "\n"+"CINAME : " + row[1]);*/
			}
			return serviceIdMap;
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
}
