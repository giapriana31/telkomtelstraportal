	function addCommentNotes(element){
		if(element.checked){
			 var textbox = document.getElementById("dashTickNotesTextArea")
			 textbox.value =  "Multiple services are impacted. " + textbox.value;
		}
		else{
			var textbox = document.getElementById("dashTickNotesTextArea")
			textbox.value =  "";
		}
	}
	
	
	$('#LogIncidentModal').on( 'shown', function ( event ) {
		$(this).css('z-index', 1041);
		/*document.getElementById("submitRequestButtonID").disabled = false;*/
		/* validator.resetForm(); */
	});
	
	$('#LogIncidentModal').on( 'hidden', function ( event ) {
		$(this).css('z-index', -10);
		/* alert('Hiding'); */
		$("label.error").hide();
		$(".error").removeClass("error"); 
		$('#serviceNameSelect option[value!="0"]').remove();
		$('#submitRequestButtonID').removeClass("disableButton");
		/*document.getElementById("submitRequestButtonID").disabled = false;*/
		$("#logIncForm").validate().resetForm();
		$("#logIncForm")[0].reset();
		
	});
	
	$('#logIncSubmitResponse').on( 'shown', function ( event ) {
		$(this).css('z-index', 1042);
		$('#LogIncidentModal').css('z-index', 1039);
		/* alert('Hi I am here response'); */
	});
	
	$('#logIncSubmitResponse').on( 'hidden', function ( event ) {
		$(this).css('z-index', -10);
		$('#LogIncidentModal').css('z-index', 1041);
	});

	$(function() {
	    $('.helpIconImg').tooltip();
	});