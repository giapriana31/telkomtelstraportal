
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.css" rel="stylesheet"> -->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="http://getbootstrap.com/2.3.2/assets/js/bootstrap-modal.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="submitFormURL" name="handleLogIncident" />
<portlet:resourceURL var="submitRequestURL" id="submitRequestURL"/>
<portlet:resourceURL var="getServiceURL" id="getServiceURL"/>
<script type="text/javascript">
$(document).ready(function () {
	var incID;
	var submitRequestURL = "${submitRequestURL}"
	/* document.getElementById("submitRequestButtonID").disabled = false; */
	jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '0');
    }, "Please Select Valid Option");
	
	var validator = $("#logIncForm").validate({
        rules : {
            summary :"required",
            worklognotes :"required",
            siteIDSelect :{selectcheck: true},
            serviceid :{selectcheck: true},
            impact :{selectcheck: true},
            urgency :{selectcheck: true},
        },
    	messages: {
    		summary : "Please enter Summary of Incident",
    		worklognotes: "Please enter Description of Incident",
        },
        highlight: function (element, errorClass, validClass) {
            return false;  // ensure this function stops
        },
        unhighlight: function (element, errorClass, validClass) {
            return false;  // ensure this function stops
        },
        submitHandler: function (form) {
        	$('#submitRequestButtonID').attr('disabled','disabled');
        	$('#submitRequestButtonID').addClass("disableButton");
        	/* document.getElementById("submitRequestButtonID").disabled = true; */
        	$.ajax({
                url: submitRequestURL,
                type: "POST",
                data: $('#logIncForm').serialize(),
                success: function(response){
                	incID = JSON.parse(response)
                	var status = incID.incidentID.split("@")[0];
                	/* alert('Response - ' + status); */
                	if(status=='Success'){
                		document.getElementById("responseStatus").innerHTML='SUCCESSFUL';
                		document.getElementById("incidentID").innerHTML='Your ticket - '+incID.incidentID.split("@")[1]+' has been submitted successfully!';
                	}
                	else{
                		document.getElementById("responseStatus").innerHTML='ERROR';
                		document.getElementById("incidentID").innerHTML='Incident submission failed. Please try again.';
                	}
                	$('#logIncSubmitResponse').modal('show');
                	$('#submitRequestButtonID').removeAttr('disabled');
                	$('#submitRequestButtonID').removeClass("disableButton");
                    $("#logIncForm")[0].reset();
                    $('#serviceNameSelect option[value!="0"]').remove();
                    /* $('.submitRequestButton').prop('disabled', false); */
                },
				error: function(xhr){
		        	alert('Error' + xhr);
		        	/* $('.submitRequestButton').prop('disabled', false); */
		  		}
            });
        }
    });
});
</script>
 
<!-- <script type="text/javascript">
	
	
	$(function(){
		
		$('#logIncForm').submit(function(e){
			e.preventDefault();
			/* alert(validator + "Whats validator"); */
			/* if(validator){
				alert('Nice here');
			} */
			$("#logIncForm").validate({
			        rules : {
			            summary :"required",
			            worklognotes :"required",
			            siteIDSelect :{selectcheck: true},
			            serviceid :{selectcheck: true},
			            impact :{selectcheck: true},
			            urgency :{selectcheck: true},
			        },
			    	messages: {
			    		summary : "Please enter Summary of Incident",
			    		worklognotes: "Please enter Description of Incident",
			        },
			        highlight: function (element, errorClass, validClass) {
			            return false;  // ensure this function stops
			        },
			        unhighlight: function (element, errorClass, validClass) {
			            return false;  // ensure this function stops
			        },
			        submitHandler: function (form) {
			        	$.ajax({
			                url: submitRequestURL,
			                type: "POST",
			                data: $('#logIncForm').serialize(),
			                success: function(response){
			                	incID = JSON.parse(response);
			                	if(response.status=='Success'){
			                		document.getElementById("responseStatus").innerHTML='Success';
			                		document.getElementById("incidentID").innerHTML='Your ticket - '+incID.incidentID+' has been submitted successfully.';
			                	}
			                	else{
			                		document.getElementById("responseStatus").innerHTML='Error';
			                		document.getElementById("incidentID").innerHTML='Incident submission failed. Please try again.';
			                	}
			                	$('#logIncSubmitResponse').modal('show');
			                    $("#logIncForm")[0].reset();
			                    /* $('.submitRequestButton').prop('disabled', false); */
			                },
							error: function(xhr){
					        	alert('Error' + xhr);
					        	/* $('.submitRequestButton').prop('disabled', false); */
					  		}
			            });
			        }
			    });
			});
			
	});
</script> -->

<script type="text/javascript">
	function getService(dropdown) {
		 	var selectedValue = dropdown.options[dropdown.selectedIndex].value;
		 	/* alert('Selected Value + ' + selectedValue); */
		 	var getServiceURL = "${getServiceURL}"
			var jsonSiteID = {"selectedSiteID" : selectedValue};
    		var dataJson={selectedSiteIDJson : JSON.stringify(jsonSiteID)};
    		$.ajax({
    			url: getServiceURL,
                type: "POST",
                data: dataJson,
                success: function(response){
                	$('#serviceNameSelect option[value!="0"]').remove();
                	var x=1;
                	var arr = response.split("#####");
                	var len = arr.length;
                	var reslen = len-x;
                	for(var i=0; i<reslen;i++){
                		
                		$('#serviceNameSelect').append($('<option>').text(response.split("#####")[i].split('@@@@@')[1]).val(response.split("#####")[i].split('@@@@@')[1]));
                	}
                	$('#serviceNameSelect option[value=""]').remove();
              },
				error: function(xhr){
					$('#serviceNameSelect option[value!="0"]').remove();
		        alert('Error' + xhr);
		    }
    		});
    	}
</script>

<div class="container" id="dashTicketsContainer">
 
	<div class="portletTitleBar">
		<div class="titleText">
			<liferay-ui:message key="INCIDENTS" />
		</div>
		<div class="navigationArrow">
			<a class="navArrowRight" href="<%=themeDisplay.getURLHome().replace("/c/portal/login?p_l_id=10185","")%>/tickets">
				<img class="navArrowRightImg" src="<%=request.getContextPath() %>/images/Link_G_24.png"/>
			</a>
		</div>
	</div>
 	
 	<div class="portletTitleContentSeparator">
        <img class="pTCSLine" src="<%=request.getContextPath() %>/images/u304_line.png"/>
    </div>
    <div class="table-responsive">
    	<table id="logIncTableMarg" class="table">
    		<tr>
    			<td class="customTD">
    				<div class="dashTickCountCont">
    					<div class="dashTickCountLeft">
    						<div class="impactServiceCount">
    							<div class="dashTickServiceCount">
    								${countP1P2}
    							</div>
    						</div>
    						<div class="impactServiceCountText"><liferay-ui:message key="Incidents with High Impact"/></div>
    						<br>
    						<div class="riskServiceCount">
    							<div class="dashTickServiceCount">
    								${countP3P4}
    							</div>
    						</div>
    						<div class="riskServiceCountText"><liferay-ui:message key="Incidents with Moderate Impact"/></div>
    					</div>
    				</div>
    			</td>
    			<td class="customTD">
    				<!-- <div class="logIncButton"> -->
	    				<button type="button" id="logTicketButton" class="btn btn-info btn-lg" data-toggle="modal" data-target="#LogIncidentModal" style="border-radius:5px !important;"><liferay-ui:message key="Log a New Incident"/></button>
	    				
				</td>
			</tr>
    	</table>
    </div>
    
 	<div class="dashTicketsCont">
 		
    	<div class="table-responsive">
    		<table class="table" id="ticketTableOverflow">
    		<c:forEach var = "listValue" items = "${incidentRowDetailsList}">
				<tr>
					<c:choose>
						<c:when test="${listValue.customerimpacted == true}">
							
							<td class="dashTickSpan1 customTD priorityColorRed">P${listValue.priority}</td>
						</c:when>
						<c:otherwise>
							<td class="dashTickSpan1 customTD priorityColorAmber">P${listValue.priority}</td>
						</c:otherwise>
					</c:choose>
	      			<td class="dashTickSpan7 customTD"><div class="elipsTicketSummary" title="${listValue.summary}">${listValue.summary}</div></td>
    				<td class="dashTickSpan3 customTD">${listValue.dueByTime}</td>
    				<td class="dashTickSpan1 customTD">
    					<c:choose>
							<c:when test="${listValue.showEscalationFlag == true}">
								<div class="escFlagIcon">
        							<img src="<%=request.getContextPath() %>/images/u550.png"/>
        						</div>
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
    				</td>
   				</tr>
 			 </c:forEach>
    		</table>
    	</div>
 	</div>
 	
 	
  	  	
  </div>
  <div class="modal fade" id="LogIncidentModal" role="dialog" data-backdrop="static" data-keyboard="false" aria-hidden="true" tabindex="-1">
    						<div class="modal-dialog modal-lg">
				  			<!-- Modal content-->
      							<div class="modal-content">
      								<form:form id="logIncForm" modelAttribute="logIncident" method="post" action="<%=submitFormURL.toString()%>">
      								<%--  --%>
        								<div class="modal-header">
          									<button type="button" class="close" data-dismiss="modal" style="border-radius:5px !important;">
          										<img class="closeMark" src="<%=request.getContextPath() %>/images/Close_G_24.png"/>
          									</button>
          									<div class="modal-title logTitle"><liferay-ui:message key="LOG A NEW INCIDENT"/></div>
        								</div>
        								<div class="modal-body">
						    				<div class="container-fluid">
						    					<div class="row" id="dashTickRow">
						    						<div class="P1P2Disclaimer">
						    							<img class="disclaimerIcon" src="<%=request.getContextPath() %>/images/Discription_G_24.png"/>
						    							<div class="disclaimerText"><liferay-ui:message key="${LogIncidentDisclaimerMsg}"/></div>
						    						</div>
						    						<!-- <div class="span2"></div> -->
						    					</div>
        										<div class="row" id="dashTickRow">
            										<div class="span3">
            											<div class="labelAlign">
            												<liferay-ui:message key="Request Summary:"/>
            											</div>
	            									</div>
	            									<div class="span9">
            											<form:input path="summary" name="summary" type="text" class="textBoxLogInc"/> 
		 											</div>
		 											<%-- <div class="span3">
		 												<form:errors path="summary" cssClass="error"/>
		 											</div> --%>
          										</div>
          										<div class="row" id="dashTickRow">
	            									<div class="span3">
            											<div class="labelAlign">
            												<liferay-ui:message key="Site Name:"/>
            											</div>
            										</div>
	            									<div class="span9">
            											<form:select id="siteID" name="siteIDSelect" path="siteid" items= "${siteNameList}" class="selectBoxLogInc" onchange="getService(this)">
            													<%-- <form:option value="NONE" label="--- Select ---"/> --%>
            											</form:select>
            										</div>
				        						</div>
          
          										<div class="row" id="dashTickRow">
            										<div class="span3">
            											<div class="labelAlign">
            												<liferay-ui:message key="Service Name:"/>
            											</div>
            										</div>
	            									<div class="span9">
            											<form:select path="serviceid" name="serviceid" id="serviceNameSelect" items= "${serviceIDList}" class="selectBoxLogInc">
            												<%-- <form:option value="NONE" label="--- Select ---"/> --%>
            											</form:select>
		 											</div>
            									</div>
		            			
        						    			<div class="row" id="dashTickRow">
            										<div class="span3"></div>
            										<div class="span9" id="dashTickCheckBox">
            										<%-- <form:checkbox path="MIM" value=""/> --%>
               											<input id="MultiServiceCheckBox" type="checkbox" onchange="addCommentNotes(this)"/>
               											<div class="checkBoxTextLogInc">
            												<liferay-ui:message key="Multiple services are impacted"/><br/>
            												&nbsp;&nbsp;&nbsp;
            												<liferay-ui:message key="(Add details in description)"/><br/>
            											</div>
            										</div>
            									</div>
          				
						          				<div class="row" id="dashTickRow">
            										<div class="span3">
            											<div class="labelAlign">
            												<liferay-ui:message key="Impact:"/>
            											<div class="helpIconDiv">
            													<a href="#" data-placement='bottom' rel="popover" data-original-title="Impact Description" data-content="${ImpactDescription}" data-container="body">
            														<%-- data-toggle="tooltip" title="${ImpactDescription}" --%>
            														<img class="helpIconImg" src="<%=request.getContextPath() %>/images/Discription_G_16.png">
	            												</a>
            												</div>
            											</div>
            										</div>
	            									<div class="span9">
	            										<form:select path="impact" name="impact" items= "${impactListMap}" class="selectBoxLogInc">
	            											<form:option value="NONE" label="--- Select ---"/>
	         											</form:select> 
            										</div>
          										</div>
          
          										<div class="row" id="dashTickRow">
            										<div class="span3">
            											<div class="labelAlign">
            												<liferay-ui:message key="Urgency:"/>
            												<div class="helpIconDiv"> 
	            												<a href="#" data-toggle="tooltip" title="${UrgencyDescription}">
            														<img class="helpIconImg" src="<%=request.getContextPath() %>/images/Discription_G_16.png">
            													</a>
            												</div>
            											</div>
            										</div>
	            									<div class="span9">
	            										<form:select path="urgency" name="urgency" items= "${urgencyListMap}" class="selectBoxLogInc">
	            											<form:option value="NONE" label="--- Select ---"/>
	         											</form:select> 
            										</div>
            									</div>
          										<div class="row" id="dashTickRow">	
            										<div class="span3">
            											<div class="labelAlign">
            												<liferay-ui:message key="Description:"/>
            											</div>	
            										</div>
	            									<div class="span9">
            											<form:textarea path="worklognotes" name="worklognotes" rows="4" cols="100" id="dashTickNotesTextArea"/>
													</div>
          										</div>
          									</div>
          			
        								</div>
        								<div class="modal-footer">
        									<button type="button" class="cancelButton" data-dismiss="modal" style="border-radius:5px !important;">
        									<div class="cancelButtonContent">
        										<div class="buttonIcon">
        											<img src="<%=request.getContextPath() %>/images/Cancel_W_16.png"/>
        										</div>
        										<div class="buttonText">
        											<liferay-ui:message key="Cancel"/>
        										</div>
        									</div>
        								</button>
					        			<button type="submit" class="submitRequestButton" id="submitRequestButtonID"style="border-radius:5px !important;">
        									<div class="submitButtonContent">
        										<div class="buttonIcon">
					        						<img src="<%=request.getContextPath() %>/images/SubmitRequest_W_16.png"/>
        										</div>
        										<div class="buttonText">
        											<liferay-ui:message key="Submit Request"/>
        										</div>
        									</div>
        								</button>
		        			    	</div>
      							</form:form>
      						</div>
						</div>
  					</div>
  <div class="modal fade" id="logIncSubmitResponse" data-backdrop="static" data-keyboard="false">
  	<div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"style="border-radius:5px !important;">
    		<img class="closeMark" src="<%=request.getContextPath() %>/images/Close_G_24.png"/>
    	</button>
        	<div id="responseStatus">Success!</div>
    </div>
    <div class="modal-body">
 	   <div id="incidentID"></div>
 	</div>
    <div class="modal-footer">
    	<button type="button" class="cancelButton" class="btn" data-dismiss="modal" style="border-radius:5px !important;">
    		<div class="buttonIcon">
				<img src="<%=request.getContextPath() %>/images/Close_W_16.png"/>
        	</div>
        	<div class="buttonText">
        		<liferay-ui:message key="Close"/>
        	</div>
    	</button>
    </div>
  </div>
  

