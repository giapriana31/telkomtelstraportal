package com.tt.hook.language;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Locale;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.tt.logging.Logger;
import com.tt.roles.CustomerRole;
import com.tt.utils.PropertyReader;
import com.tt.utils.RefreshUtil;

// set language in Liferay & okta , when user change language from top bar
public class setLanguage extends BaseStrutsAction 
{
	Properties prop = PropertyReader.getProperties();	
	CustomerRole role = new CustomerRole();
	private final static Logger log = Logger.getLogger(setLanguage.class);
	
	// set proxy
	static {
    	RefreshUtil.setProxyObject();
    }
	
    public String execute(HttpServletRequest request,HttpServletResponse response) throws Exception 
    {
    	log.info("setLanguage.java -> in execute()");
    	try
		{    	
    	  // get user from session    	  
    	  User user = UserLocalServiceUtil.getUser((Long)request.getSession().getAttribute(WebKeys.USER_ID));
    	  log.debug("setLanguage.java -> execute() -> User = "+user==null?"null":user.getEmailAddress());
    	  
    	  // get selected language (name is pre-defined by liferay)
		  String preferredLanguage = request.getParameter("_82_languageId");
		  String email = user.getEmailAddress();
		  
		  log.debug("setLanguage.java -> "+preferredLanguage);
		  log.debug("setLanguage.java -> "+email);
		  
		  // set language in user object & set user object to liferay
		  user.setLanguageId(preferredLanguage);
	      UserLocalServiceUtil.updateUser(user);	      
	      
	      // get locale from language name & set in session, so even if refresh is not done, language is changed instantaly
	      Locale locale = LocaleUtil.fromLanguageId(preferredLanguage);
	      request.getSession().setAttribute("org.apache.struts.action.LOCALE", locale);
	      
	      // create json object to send language to okta
	      JSONObject profile1 = new JSONObject();
	      JSONObject obj = new JSONObject();
	      obj.put("preferredLanguage", preferredLanguage);
	      obj.put("userPreferredLanguage", preferredLanguage);
	      profile1.put("profile", obj);
	      
	      // URL for okta
	      URL url = new URL(prop.getProperty("oktaURL")+"/api/v1/users/"+ email);
	      log.debug("setLanguage.java -> URL ="+url);
	      
	      HttpsURLConnection con = (HttpsURLConnection)url.openConnection();
	      con.setRequestProperty("Authorization", prop.getProperty("Token"));
	      con.setDoOutput(true);
	      con.setDoInput(true);
	      con.setUseCaches(false);
	      con.setRequestProperty("Content-Type", "application/json");
	      con.setRequestProperty("Accept", "application/json");
	      con.setRequestMethod("POST");
	      con.connect();
	      OutputStream os = con.getOutputStream();
	      OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
	      profile1.write(osw);
	      osw.flush();
	      osw.close();
	      os.close();
	      con.getResponseCode();
	      
	      // read error, if any
	      InputStream is;
	      if (con.getResponseCode() >= 400) 
	      {
	          is = con.getErrorStream();
	          BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	          StringBuilder results = new StringBuilder();
	          String line;
	          while ((line = reader.readLine()) != null) 
	          {
	              results.append(line);
	          }
	          con.disconnect();
	          log.error("setLanguage.java -> "+results);
	      } 
	      
	      return "";
		}
		
		catch(Exception e)
    	{			
			log.error("setLanguage.java -> Error "+e.getMessage());
			return "";
    	}    	
    }    
}
