package com.tt.hook.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.tt.logging.Logger;
import com.tt.roles.CustomerRole;

// set selected customer in session
public class setCustomerinSession extends BaseStrutsAction 
{
	private final static Logger log = Logger.getLogger(setCustomerinSession.class);
	
    public String execute(HttpServletRequest request,HttpServletResponse response) throws Exception 
    {
    	log.info("setCustomerinSession.java -> in execute()");
    	try
    	{
    		// get custoemrID & Name from request    		
			if(request.getParameter("customerID") !=null && request.getParameter("customerName")!=null)
			{
				log.debug("setCustomerinSession.java -> request.getParameter(\"customerID\") = " + request.getParameter("customerID"));
	    		log.debug("setCustomerinSession.java -> request.getParameter(\"customerName\") = " + request.getParameter("customerName"));
	    		
				CustomerRole customerRole = new CustomerRole();
				
				// get current user object (which is internal)
				//User user = customerRole.getLoggedInUser(request, response);
				  
				//log.debug("setCustomerinSession.java -> User = " + user==null ? "null":user.getEmailAddress());
				// change his ID to selected customer (as internal customer have no data like external customer)
			    //user.getExpandoBridge().setAttribute("CustomerUniqueID", request.getParameter("customerID").trim(), false);
			    
			    // update user
			    // UserLocalServiceUtil.updateUser(user);
				
			    // set this user to session, so other can access it
			    //customerRole.setExternalUserToSession(request, response, user);			    
			    //log.debug("setCustomerinSession.java -> User set in session = " + user==null ? "null" :user.getEmailAddress());
			    
			    // set customer ID & Name to session to display in top bar (dockbar can access only session variable)
				customerRole.setCustomerIDToSession(request, response, request.getParameter("customerID").trim());
				customerRole.setCustomerNameToSession(request, response , request.getParameter("customerName").trim());
				log.debug("setCustomerinSession.java -> customerID set in session = " +  request.getParameter("customerID").trim());
				log.debug("setCustomerinSession.java -> customerName set in session = " + request.getParameter("customerName").trim());
				
				//List<User> users = customerRole.getUsersByCustomAttributes("CustomerUniqueID", request.getParameter("customerID").toString().trim(), 1);
					
			}
			return "";	
    	}
    	catch(Exception e)
    	{
    		log.error("setCustomerinSession.java -> Error "+e.getMessage());
    		return "";
    	}
    	
    }
}
