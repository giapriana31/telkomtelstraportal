package com.tt.hook.login;

import java.text.Collator;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.tt.logging.Logger;
import com.tt.roles.CustomerRole;
import com.tt.utils.PropertyReader;
import com.tt.utils.TTGenericUtils;

// display select customer box & set user to session after user login
public class AfterLogin extends Action
{
    public static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(AfterLogin.class);
    
    public void run(HttpServletRequest req, HttpServletResponse res) 
    {
    	
    	log.debug("AfterLogin here in run()");
    	System.out.println("AfterLogin here#########");
    	
    	try 
    	{
    		CustomerRole customerRole = new CustomerRole();
    		
    		// get logged in user
    		User user = customerRole.getLoggedInUser(req, res);

    		// add permission checker for expando bridge as we are in Hook, not Portlet
    		PermissionChecker checker = PermissionCheckerFactoryUtil.create(user);
        	PermissionThreadLocal.setPermissionChecker(checker);
        	
        	req.getSession().setAttribute("isAdmin",TTGenericUtils.checkRolePortallTTAdmin(req));
        	req.getSession().setAttribute("isInternalAdmin",TTGenericUtils.checkRoleInternalAdmin(req));
         
        	System.out.println("Admin:::" +req.getSession().getAttribute("isAdmin"));
        	System.out.println("Internal Admin::" +req.getSession().getAttribute("isInternalAdmin"));
        	log.debug("Admin:::" +req.getSession().getAttribute("isAdmin"));  	
        	String customerName = user.getExpandoBridge().getAttribute("customerName")==null?"":user.getExpandoBridge().getAttribute("customerName").toString().trim();
    		    		
        	log.debug("AfterLogin.java -> User = "+user==null?"null":user.getEmailAddress());
        	
    		// if role is assigned, go ahed , else set error message in session
    		if(customerRole.isRoleAssigned(user))
    		{    			
    			log.debug("AfterLogin.java -> role assigned = true ");
    			
    			// if two roles are assgined...show error
	    		if(!customerRole.isValidUser(user))
	    		{
	    			
	    			log.debug("AfterLogin.java -> isValid = false");
	    			// set role assign error
	    			customerRole.setRoleAssignError(req, res,user);
	    			req.getSession().setAttribute(customerRole.LIFERAY_SHARED_ROLE_ASSIGN_ERROR_MESSAGE_KEY,"INVALID ROLES");
	    			
	    			// set logout url to session , based on role (this is for session timeout only, its jsp which can access session varible only)
	    			req.getSession().setAttribute("LIFERAY_SHARED_session_URL",prop.getProperty("Portal.Home.URL")+prop.getProperty("Hook.PreLogout.user.isInternal.false"));
	    		}	   
    			
    			// if user is internal
    			else if(customerRole.isInternal(user))
	    		{
	    			
	    			log.debug("AfterLogin.java -> isInternal = true ");
	    			customerRole.setInternalUserToSession(req, res, user);
	    			
	    			// set user role to session (required to set in session, because in dockbar jsp we have to access it via session only)
	    			customerRole.setInternalUserRoleToSession(req, res);
	    			customerRole.setCustomerIDToSession(req, res, user.getExpandoBridge().getAttribute("CustomerUniqueID").toString().trim());
	    			//customerRole.setCustomerNameToSession(req, res,customerName);
	    			
	    			log.debug("setCustomerinSession.java -> customerID set in session = " + user.getExpandoBridge().getAttribute("CustomerUniqueID").toString().trim());
	    			
	    			// set customer list to session (requried to set in session, becuase in dockbar jsp we have to access it via session only)
	    			customerRole.setCustomerIDNameMapToSession(req, res,getHashMap());
	    			
	    			// set logout url to session , based on role (this is for session timeout only, its jsp which can access session varible only) 
	    			req.getSession().setAttribute("LIFERAY_SHARED_session_URL",prop.getProperty("Portal.Home.URL")+prop.getProperty("Hook.PreLogout.user.isInternal.true"));
	    		}	    		
	    			
	    		// if user is external
	    		else if(customerRole.isExternal(user))
	    		{	    			
	    			
	    			log.debug("AfterLogin.java -> isExternal = true");
	    			customerRole.setExternalUserToSession(req, res,user);
	    			
	    			customerRole.setCustomerIDToSession(req, res, user.getExpandoBridge().getAttribute("CustomerUniqueID").toString().trim());
	    			//customerRole.setCustomerNameToSession(req, res,customerName);
	    			
	    			// set logout url to session , based on role (this is for session timeout only, its jsp which can access session varible only)
	    			req.getSession().setAttribute("LIFERAY_SHARED_session_URL",prop.getProperty("Portal.Home.URL")+prop.getProperty("Hook.PreLogout.user.isInternal.false"));
	    		}
	    		
	    		if(customerRole.isCustomerPortalAdmin(user))
	    		{
	    			
	    			log.debug("AfterLogin.java -> IsCustomerPortalAdmin = true");
	    			customerRole.setInternalUserToSession(req, res, user);
	    				    			
	    			customerRole.setCustomerIDToSession(req, res, user.getExpandoBridge().getAttribute("CustomerUniqueID").toString().trim());
	    			//customerRole.setCustomerNameToSession(req, res,customerName);
	    			
	    			// set logout url to session , based on role (this is for session timeout only, its jsp which can access session varible only)
	    			req.getSession().setAttribute("LIFERAY_SHARED_session_URL",prop.getProperty("Portal.Home.URL")+prop.getProperty("Hook.PreLogout.user.isInternal.true"));
	    		}
	    		
	    		 		
    		}
    		else
    		{
    			log.debug("AfterLogin.java -> roleAssigned = false");
    			
    			// set error message in session because no roles are assigned
    			customerRole.setRoleAssignError(req, res,user);
    			req.getSession().setAttribute(customerRole.LIFERAY_SHARED_ROLE_ASSIGN_ERROR_MESSAGE_KEY,"NO ROLES");
    			
    			// make user external (to logout everything) & create URL for logout
    			req.getSession().setAttribute("LIFERAY_SHARED_session_URL",prop.getProperty("Portal.Home.URL")+prop.getProperty("Hook.PreLogout.user.isInternal.false"));
    		}
	    	 
		}
    	catch(Exception e) 
    	{
    		req.getSession().setAttribute("LIFERAY_SHARED_session_URL",prop.getProperty("Portal.Home.URL")+prop.getProperty("Hook.PreLogout.user.isInternal.false"));
    		log.error("AfterLogin.java -> Error "+e.getMessage());		
		}
    }
    
	@SuppressWarnings({ "unchecked", "deprecation" })
	public String[] getHashMap() throws Exception
	{		
		log.debug("AfterLogin.java -> in getHashMap() ");
		try
		{
			// get customer list from databse
			Configuration cfg = new Configuration();		
		    cfg.configure("hibernate.cfg.xml");
		    
			Session session = cfg.buildSessionFactory().openSession();
			
			// query contain validation like not null & not empty
		    SQLQuery query = session.createSQLQuery(prop.getProperty("Hook.AftetLogin.selectCustomerQuery"));
		    	    
			List<String> list = (List<String>)query.list();
			log.debug("AfterLogin.java -> List size ="+list==null?"null":list.size()+"");
			
			// convert to array for performance
			String[] array = list.toArray(new String[list.size()]);
			
			// sort it by customer name
			Arrays.sort(array, Collator.getInstance());
			
			session.close();	
			return array;
			
		}
	    catch(Exception e)
	    {
	    	log.error("AfterLogin.java -> getHashMap() Error"+e.getMessage());
	    	return null;
	    }
	}
	
}
