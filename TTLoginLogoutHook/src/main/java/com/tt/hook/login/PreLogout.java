
package com.tt.hook.login;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.tt.utils.PropertyReader;
import com.tt.logging.Logger;
import com.tt.roles.CustomerRole;

// check users role before logout & invlaidate okta,solrwind
public class PreLogout extends Action
{
	public static Properties prop = PropertyReader.getProperties();
	private final static Logger log = Logger.getLogger(PreLogout.class);
	
    public void run(HttpServletRequest req, HttpServletResponse res) 
    {
    	log.info("PreLogout.java -> in run()");
    	try 
    	{
    		// validation for session & user    		
    		if(req.getSession() !=null && req.getSession().getAttribute(WebKeys.USER_ID)!=null && UserLocalServiceUtil.getUser((Long)req.getSession().getAttribute(WebKeys.USER_ID))!=null)
    		{
	    		CustomerRole customerRole = new CustomerRole();
	    		User user = customerRole.getLoggedInUser(req, res);
	    		log.debug("PreLogout.java -> User = "+user==null ? "null":user.getEmailAddress());
	    		
	    		// if role assigned, go ahed
	    		if(customerRole.isRoleAssigned(user))
	    		{
	    			log.debug("PreLogout.java -> Role assgined = true");	    			
	    			
	    			// set url based on role
		    		if(customerRole.isInternal(user) ||  customerRole.isCustomerPortalAdmin(user))
		    		{
		    			log.debug("PreLogout.java -> isInternal = true");
		    			res.sendRedirect(prop.getProperty("Hook.PreLogout.user.isInternal.true"));
		    		}
		    		else if(customerRole.isExternal(user))
		    		{
		    			log.debug("PreLogout.java -> isExternal = true");
		    			res.sendRedirect(prop.getProperty("Hook.PreLogout.user.isInternal.false"));
		    		}
		    		else if(!customerRole.isValidUser(user))
		    		{
		    			log.debug("PreLogout.java -> isValid = false");
		    			res.sendRedirect(prop.getProperty("Hook.PreLogout.user.isInternal.false"));
		    		}	    		
	    		}
	    		else
	    		{
	    			log.debug("PreLogout.java -> RoleAssigned = false");
	    			// if role assign error, make him external & logout everything
	    			res.sendRedirect(prop.getProperty("Hook.PreLogout.user.isInternal.false"));
	    		}
    		}
    		else
    		{
    			log.debug("PreLogout.java -> invalidUser = true");
    			// if invalid user, make him external & logout everything
    			res.sendRedirect(prop.getProperty("Hook.PreLogout.user.isInternal.false"));
    		}
		}
    	catch(Exception e) 
    	{
    		log.error("PreLogout.java -> Error "+e.getMessage());
		}
    }
}
