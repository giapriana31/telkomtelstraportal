function accordionClickEvent(listing) {
    listing.click(function() {
        if ($(this).parents('li').hasClass('active')) {            
            $(this).siblings('.detail').slideUp(function() {
                $(this).parents('li').removeClass('active');
            });
        } else {            
            listing.each(function() {
                $(this).siblings('.detail').slideUp(function() {
                    $(this).parents('li').removeClass('active');
                });
            });
            $(this).siblings('.detail').slideDown(function() {
                $(this).parents('li').addClass('active');
            });
        }
    });
}
accordionClickEvent($('.accordion > li > a'));
function footerAccordion(listing) {
    listing.unbind('click');
    accordionClickEvent(listing);   
    if ($(window).width() >= 930) {
        listing.unbind('click');
    }
}
footerAccordion($('footer .accordion > .title > .container > a'));
$(window).resize(function() {
    footerAccordion($('footer .accordion > .title > .container > a'));
	showLeftNavigation($(window).width());
});


$(document).ready(function(){
	footerAccordion($('footer .accordion > .title > .container > a'));
	readLeftNavigation();
	changeAvatarImg();
	showLeftNavigation($(window).width());
	$('#_145_navSiteNavigationNavbarBtn').attr('href', '#')
	$('.nav-item-sitenavigationtoggle').attr('style', 'display:none');
	showLeftNavigation($(window).width());	
	$("#_145_navSiteNavigationNavbarBtn").unbind("click").click( function(){
		$('#leftbannnerspan').toggle('slow');
	});
	
	$('[data-toggle="tooltip"]').tooltip();
});

function showLeftNavigation(width){
	if(width < 979 ){
		$('#leftbannnerspan').attr('style', 'display:none');
	}
	else{
		$('#leftbannnerspan').removeAttr('style');
	}
	
	
	
	if(width > 979 ){
		$('.user-avatar-container').removeAttr('style');
	}
	else{
		$('.user-avatar-container').attr('style', 'display:none');
	}
}



function changeAvatarImg(){
	var baseCtrlName = '#user-avatar-max-width';
	var baseSrc = $(baseCtrlName).attr('src');
	$('.user-avatar-image').each(function(i, imgObj){
		var idName = "";
		idName = $(this).attr('id');
		if(idName != 'user-avatar-max-width'){
			 $(this).attr('src', baseSrc);
			 $(this).addClass('user-avatar-img');
			 $(this).parent().addClass('user-avatar-img-parent');
		}
	})
}

function readLeftNavigation() {
	var listItems = $("#leftNavigation li");
	listItems.each(function(idx, li) {
		if(li.childElementCount > 1){
			
			var imagepath ;
			try {
				imagepath = li.children[1].children[0].getAttribute('src');
			}
			catch(error){
				imagepath = '';
			}
			var hostPath = $(location).attr('protocol') +$(location).attr('host'); 
			
			var completePath = imagepath;
			//$(this).find('#anchortag').attr('style', 'color: yellow');
			
			/*$(this).find('#anchortag').attr('style', 'background: transparent url( '+completePath+') scroll no-repeat left center; padding: 2px 0px 2px 40px; word-break:break-word; line-height:20px;');*/
			
			if(imagepath != ''){
				$(this).find('#anchortag').attr('style', 'background: transparent url( '+completePath+') scroll no-repeat left center;');
			}
			else{
				$(this).find('#anchortag').attr('style', 'background: transparent');
			}			
		}
	});
	
	
}
