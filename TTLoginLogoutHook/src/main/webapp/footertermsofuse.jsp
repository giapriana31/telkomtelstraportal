<!DOCTYPE html>
<html>
<head>
<link rel="apple-touch-icon" href="/TTLoginLogoutHook/images/apple-touch-icon.png"/>
<link href="/TTLoginLogoutHook/images/fav.png" rel="Shortcut Icon" />
<!-- you should always add your stylesheet (css) in the head tag so that it starts loading before the page html is being displayed -->	
	<STYLE>
	@font-face 
{
  font-family: 'MyWebFont';
  src: url('fonts/GothamRoundedBook.woff') format('woff'),
       url('fonts/GothamRoundedBook.ttf') format('truetype');
}
body
{
 font-family: 'MyWebFont';
}
	/*
* multi-line comment
*/
p{ line-height: 1em;

text-align:justify; }
h1, h2, h3, h4{
    color: #E32212;
	
	
	margin: 0 0 .5em 0;
}
h1{ font-size:42px;; }
h2{ font-size: 32px; }
a{
	color: black;
	text-decoration: none;
}
	a:hover,
	a:active{ text-decoration: underline; }

/* you can structure your code's white space so that it is as readable for when you come back in the future or for other people to read and edit quickly */

body{
     line-height: 1.2em; width: 100%; margin: 0; 
}
/* you can put your code all in one line like above */
#page{ margin: 20px; }

/* or on different lines like below */
#logo{
	width: 35%;
	margin-top: 5px;
	
	display: inline-block;
}
/* but try and be as concise as possible */
#nav{
	width: 60%;
	display: inline-block;
	text-align: right;
	float: right;
}
	#nav ul{}
		#nav ul li{
			display: inline-block;
			height: 62px;
		}
			#nav ul li a{
				padding: 20px;
				background: #e32212;
				color: white;
			}
			#nav ul li a:hover{
				background-color: #e32212;
				box-shadow: 0px 1px 1px #666;
			}
			#nav ul li a:active{ background-color: #e32212; }

#content{
	margin: 30px 0;
	font-size: 15Spx;
	padding: 20px;
	clear: both;
	
}
#footer
	border-bottom: 1px #ccc solid;
	margin-bottom: 10px;
}
	#footer p{
		text-align: right;
		text-transform: uppercase;
		font-size: 80%;
		color: grey;
	}

/* multiple styles seperated by a , */
#content,
ul li a{ }

	</STYLE>
	
	
	
	
	
<title>Terms of Use - TT Portal</title>		
	
	
</head>
<body>


<!-- webpage content goes here in the body -->
	<div id="page">
		<div id="logo">
			     <h1><div id="logoLink">Terms of Use</div></h1>
		</div>
		
		<div id="content" style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;font-weight:bold;">
			Please read them carefully, if you post on our Discussion Forum you are agreeing to adhere to these terms.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			These Terms and Conditions apply every time you post a message on our Discussion Forum.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			1.Your user name and password must only be used by you, keep them safe. If a posting is made using your name and password it will be considered to have been posted by you.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			2.You agree that you will not post any messages on any of our Discussion Forum that is either:
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			Unlawful, threatening, abusive, harassing, defamatory, libelous, deceptive, fraudulent, invasive of another's privacy, sexually explicit, pornographic or otherwise breaches our Discussion Forum Terms and/or the Privacy Policies
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;font-weight:bold;">
			We want to encourage creative, original and thought-provoking discussions.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			The discussion forums are not to be used as a free forum for the following
			</p>
			
			
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">	
			o	Personal attacks on others, to make disparaging remarks about any person or entity.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Interfere with anyone else's use and enjoyment of the Web Site. To encourage, incite, solicit, promote or provide information about illegal or criminal activities (including market manipulation, pornography, terrorism, explosives, weapons, drugs, programming viruses, computer hacking and copyright infringement )
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Content which abuses, threatens, promotes, or instigates physical harm or death to others, or yourself;
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Victimise, harass, degrade or intimidate an individual or group of individuals on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Infringe on any patent, trademark, copyright, right of publicity, or other intellectual property right of any party.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Contains software viruses' computer codes, files or programs to disrupt, damage, or limit the functioning of any software, hardware, or telecommunications equipment or to damage or obtain unauthorised access to any data or other information of any third party.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Impersonate any person or entity. Including any of our employees or representatives.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Disrupt normal flow of dialogue
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			o	Contain non related comments to the topic discussion. Example if related to a share or fund, keep to the subject.
			</p>
			
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			3.You agree you will never intentionally create market abuse by making false, misleading statements relating to investment prices and performances. Market abuse is an offence which could lead to fines or imprisonment.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			4.You agree that you will not post, either intentionally or otherwise, any material that could have the effect of manipulating the market value of any investment. This is strictly prohibited under the Market Abuse Directive and could lead to fines or imprisonment.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			5.You must not offer to buy or sell any investment or product or use the discussion forum to advertise any product or service.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			6.You agree that you will not post information that you know to be confidential or sensitive or otherwise in breach of the law. You should only post material that you know to be public knowledge. If you have any doubts do not post it on the site.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			7.We may change the Terms and Conditions at any time.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			If you have further questions regarding this Privacy Policy and Disclaimer, please contact us at getintouch@telkomtelstra.co.id
			</p>
		</div>
		
	</div>
</body>

</html>