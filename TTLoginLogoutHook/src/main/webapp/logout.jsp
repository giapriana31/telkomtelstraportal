<%@ page import="com.tt.utils.PropertyReader"%>
<%@ page import="java.util.Properties"%>

<link rel="apple-touch-icon" href="/TTLoginLogoutHook/images/apple-touch-icon-iphone.png"/>
<link rel="apple-touch-icon" href="/TTLoginLogoutHook/images/apple-touch-icon-ipad.png"/>
<link href="/TTLoginLogoutHook/images/fav.png" rel="Shortcut Icon" />

<%-- get logout URL from property --%>
<%
	final Properties prop = PropertyReader.getProperties();
		
	String oktaURL =	prop.getProperty("Hook.logout.JSP.oktaURL");
	String solarwindURL = prop.getProperty("Hook.logout.JSP.solarwindURL");
	String portalURL = prop.getProperty("Hook.logout.JSP.portalURL");
%>

<html>
<head>

<%-- set font to Gotham, as this page is outside theme, so we need to apply manually --%>

<style>
@font-face 
{
  font-family: 'MyWebFont';
  src: url('fonts/GothamRoundedBook.woff') format('woff'),
       url('fonts/GothamRoundedBook.ttf') format('truetype');
}
body
{
 font-family: 'MyWebFont';
}
</style>

<script>    
			
	function getQueryParameter ( parameterName ) 
	{
	  var queryString = window.top.location.search.substring(1);
	  var parameterName = parameterName + "=";
	  if(queryString.length>0) 
	  {			  
	    begin = queryString.indexOf ( parameterName );
	    if ( begin != -1 ) 
	    {
	      begin += parameterName.length;
	      end = queryString.indexOf ( "&" , begin );
	        if ( end == -1 ) 
	        {
	        	end = queryString.length
	      	}
	      return unescape ( queryString.substring ( begin, end ) );
	    }
	  }
	  return "null";
	}
	
</script>

<%-- based on URL logout Okta & solarwind --%>

<%
	if(request.getParameter("type")!=null && request.getParameter("type").toString().equalsIgnoreCase("external"))
	{
	request.getSession().invalidate();
		%>
		<script>
			document.write('<scri'+'pt src="<%=oktaURL%>"></'+'script>');
			document.write('<scri'+'pt src="<%=solarwindURL%>"></'+'script>');
			document.write('<scri'+'pt type="text/html" src="<%=portalURL%>"></'+'script>');
		</script>
	<%
	}	
	else if(request.getParameter("logout")!=null && request.getParameter("logout").toString().equalsIgnoreCase("sw"))
	{
	request.getSession().invalidate();		
	%>		
		<script>
			document.write('<scri'+'pt src="<%=oktaURL%>"></'+'script>');
			document.write('<scri'+'pt src="<%=solarwindURL%>"></'+'script>');
			document.write('<scri'+'pt src="<%=portalURL%>"></'+'script>');
		</script>
	<%
	}
	else if(request.getParameter("type")==null && request.getParameter("logout")==null)
	{
	request.getSession().invalidate();
	%>		
		<script>			
			document.write('<scri'+'pt src="<%=portalURL%>"></'+'script>');
		</script>
	<% 		
	}
%>
		
<body style="background-color: #BBBBBB;">
	<div style="margin-left:4%;margin-right:4%;">
		
		<div style="background: #e32212;">
			<img src="/TTLoginLogoutHook/images/logo.jpg" >
		</div>
		
		<div style="background-color:#F2F2F2;padding-top:10%;padding-bottom:10%;padding-left:30%;">	
				
			<div style="border: solid 1px #ccc;height: 50%;width: 60%;background-color: #fff;">
				<div id="logout" style="padding-top: 5em;border-bottom: solid 1px #ccc;margin-top: 15% font-size: 1.2em; padding-left: 12px; padding-bottom: 12px; padding-top: 12px;">
					LOGGED OUT
				</div>
				
				<div style="font-weight:bold;font-size:175%;color:#555555;text-align:center;margin-top:20%;margin-bottom:20%">
					You have successfully logged out!
				</div>						
			</div>
			
		</div>	
	</div>	
</body>