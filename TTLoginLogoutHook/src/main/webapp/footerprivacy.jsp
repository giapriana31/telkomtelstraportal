<!DOCTYPE html>
<html>
<head>
<link rel="apple-touch-icon" href="/TTLoginLogoutHook/images/apple-touch-icon.png"/>
<link href="/TTLoginLogoutHook/images/fav.png" rel="Shortcut Icon" />
<!-- you should always add your stylesheet (css) in the head tag so that it starts loading before the page html is being displayed -->	
	<STYLE>
	@font-face 
{
  font-family: 'MyWebFont';
  src: url('fonts/GothamRoundedBook.woff') format('woff'),
       url('fonts/GothamRoundedBook.ttf') format('truetype');
}
body
{
 font-family: 'MyWebFont';
}
	/*
* multi-line comment
*/
p{ line-height: 1em;

text-align:justify; }
h1, h2, h3, h4{
    color: #E32212;
	
	
	margin: 0 0 .5em 0;
}
h1{ font-size:42px;; }
h2{ font-size: 32px; }
a{
	color: black;
	text-decoration: none;
}
	a:hover,
	a:active{ text-decoration: underline; }

/* you can structure your code's white space so that it is as readable for when you come back in the future or for other people to read and edit quickly */

body{
     line-height: 1.2em; width: 100%; margin: 0; 
}
/* you can put your code all in one line like above */
#page{ margin: 20px; }

/* or on different lines like below */
#logo{
	width: 35%;
	margin-top: 5px;
	
	display: inline-block;
}
/* but try and be as concise as possible */
#nav{
	width: 60%;
	display: inline-block;
	text-align: right;
	float: right;
}
	#nav ul{}
		#nav ul li{
			display: inline-block;
			height: 62px;
		}
			#nav ul li a{
				padding: 20px;
				background: #e32212;
				color: white;
			}
			#nav ul li a:hover{
				background-color: #e32212;
				box-shadow: 0px 1px 1px #666;
			}
			#nav ul li a:active{ background-color: #e32212; }

#content{
	margin: 30px 0;
	font-size: 15Spx;
	padding: 20px;
	clear: both;
	
}
#footer
	border-bottom: 1px #ccc solid;
	margin-bottom: 10px;
}
	#footer p{
		text-align: right;
		text-transform: uppercase;
		font-size: 80%;
		color: grey;
	}

/* multiple styles seperated by a , */
#content,
ul li a{ }

	</STYLE>
	
	
	
<title>Privacy - TT Portal</title>	
	
	
	
</head>
<body>


<!-- webpage content goes here in the body -->
	<div id="page">
		<div id="logo">
			     <h1><div id="logoLink">Privacy Policy</div></h1>
		</div>
		
		<div id="content" style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			
			
			<h2>
			Scope
			
			</h2>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;font-weight:bold;">
			This Privacy Policy disclose our policies for handling personal data of users on the customer portal website:
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			1.Telkomtelstra may collect your information when you voluntarily and/or consent to provide telkomtelstra with your personal information when accessing our website.

			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			2.Telkomtelstra may also collect your information when you navigate in our website.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			3.Cookies: This site uses cookies to store information on your computer. Some of these cookies are essential to make our site work and others help us to improve by giving us some insight into how the site is being used. You need to enable cookie feature in your browser for your own convenience when accessing our website.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			4.IP (Internet Protocol) Address: Telkomtestra.co.id shall store your IP address for information purposes. We use IP address collectively to enable us identifying the location from where our customers are accessing our website.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			5.Telkomtelstra may use your personal information stored in this website to provide you with information regarding our products offer and services from time to time. If you choose not to provide certain information about you, we may not be able to provide you with the products or services you require, or the level of service that we may be able to offer. However, if you choose not to receive such information, you may opt out by emailing us at getintouch@telkomtelstra.co.id.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			6.We may require you to register with us in order to access some parts of the our website. When you register with us, you must keep your registration details confidential; you will be responsible for any access to our website using your registration details, even if the access is by another person.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			7.	We may disclose your personal information when (i) it is required by law and/or court order; (ii) we protect our copyright; and (iii) we protect our website from any unauthorized access and fraudulent act that may jeopardize the content of the website; and (iv) it is deemed necessary to third parties who provide services to us, including organizations and contractors that assist us with the purposes for which we use your information.
			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			8.	We may store your information in hard copy or electronic format, in storage facilities that we own and operate ourselves, or that are owned and operated by our service providers. We take reasonable steps to maintain the security of your information and to protect it from unauthorized disclosures.
			</p>
			
			
			
			
			
			
		</div>
		
	</div>
</body>

</html>