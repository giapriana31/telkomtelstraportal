<!DOCTYPE html>
<html>
<head>
<link rel="apple-touch-icon" href="/TTLoginLogoutHook/images/apple-touch-icon.png"/>
<link href="/TTLoginLogoutHook/images/fav.png" rel="Shortcut Icon" />
<!-- you should always add your stylesheet (css) in the head tag so that it starts loading before the page html is being displayed -->	
	<STYLE>
	@font-face 
{
  font-family: 'MyWebFont';
  src: url('fonts/GothamRoundedBook.woff') format('woff'),
       url('fonts/GothamRoundedBook.ttf') format('truetype');
}
body
{
 font-family: 'MyWebFont';
}
	/*
* multi-line comment
*/
p{ line-height: 1em;

text-align:justify; }
h1, h2, h3, h4{
    color: #E32212;
	
	
	margin: 0 0 .5em 0;
}
h1{ font-size:42px;; }
h2{ font-size: 32px; }
a{
	color: black;
	text-decoration: none;
}
	a:hover,
	a:active{ text-decoration: underline; }

/* you can structure your code's white space so that it is as readable for when you come back in the future or for other people to read and edit quickly */

body{
     line-height: 1.2em; width: 100%; margin: 0; 
}
/* you can put your code all in one line like above */
#page{ margin: 20px; }

/* or on different lines like below */
#logo{
	width: 35%;
	margin-top: 5px;
	
	display: inline-block;
}
/* but try and be as concise as possible */
#nav{
	width: 60%;
	display: inline-block;
	text-align: right;
	float: right;
}
	#nav ul{}
		#nav ul li{
			display: inline-block;
			height: 62px;
		}
			#nav ul li a{
				padding: 20px;
				background: #e32212;
				color: white;
			}
			#nav ul li a:hover{
				background-color: #e32212;
				box-shadow: 0px 1px 1px #666;
			}
			#nav ul li a:active{ background-color: #e32212; }

#content{
	margin: 30px 0;
	font-size: 15Spx;
	padding: 20px;
	clear: both;
	
}
#footer
	border-bottom: 1px #ccc solid;
	margin-bottom: 10px;
}
	#footer p{
		text-align: right;
		text-transform: uppercase;
		font-size: 80%;
		color: grey;
	}

/* multiple styles seperated by a , */
#content,
ul li a{ }

	</STYLE>
	
	
	
	
	
<title>Security - TT Portal</title>		
	
</head>
<body>


<!-- webpage content goes here in the body -->
	<div id="page">
		<div id="logo">
			     <h1><div id="logoLink">Disclaimer</div></h1>
		</div>
		
		<div id="content" style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			
			
		<!-- 	
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			1.We take customer privacy and data security very seriously. Our priority is making sure we keep your personal information safe and secure at all times. To do this, we have a number of security controls in place.
			We are always looking at ways to improve our security. We have intrusion detection systems to mitigate the risks associated with unauthorised access to our networks and to your personal information. 
			We also have processes to actively monitor our network for possible security threats and security event and information management systems to detect, analyse, and respond to identified incidents.
			There are a range of people and groups who may want to gain access to our systems or those of our trusted business partners. This could be for a variety of reasons, like fraudulent activity or system hacking. 

			</p>
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			2.	Our commitment to you is to work diligently every day to stop this from occurring.As technology changes and new threats emerge, we aim to keep ahead of any new risks. We are always looking at ways to improve our security systems and we will continue to invest in new security capabilities into the future.
			While we work hard to protect your personal information, we also encourage you to take steps to manage your privacy in a digital world. For some tips on how to protect your personal information
			</p> -->
			
			
			
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			1.	Telkomtelstra shall have the right to modify the information contained herein from time to time at its own discretion and publish an updated version on telkomtelstra website. By continuing to use the telkomtelstra website you will be deemed to accept the updated version of the website and agree to its content.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			2.	Your incompatibility and failure to access the website cannot be used as a reason to claim and file a suit against telkomtelstra.
			</p>
			<p style="color: #474141;text-align: justify;font-size: 16px;LINE-HEIGHT: 36px;">
			3.	By using this website, you will be deemed to approve the policy stipulated herein. If you choose not to agree with the policy, you are free not to use the website.
			</p>
			
			
			
			
			
			
			
			
			
			
			
			
		</div>
		
	</div>
</body>

</html>