package com.tt.client.common;

public class WebServiceErrorCodes
{
	public static final String RC_SUCCESS = "RC1001";
	public static final String RI_SUCCESS = "RI1001";
	public static final String RI_NO_CUSTOMER = "RI1002";
	public static final String RI_NO_INCIDENT = "RI1003";
	public static final String RI_TECHNICAL_FAULT = "RI1004";
	public static final String RO_SUCCESS = "RO1001";
	
}
