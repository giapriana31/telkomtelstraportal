
package com.tt.client.model.retrieveChangeRequest;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Changes">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Change" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Change_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="requested_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="risk" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="customer_site" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="change_source" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="assignment_resolver_group_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="assignee_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="requested_by_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="planned_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="planned_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="outage_required" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="outage_start_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="outage_end_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="outcome_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="customer_approval" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="customer_approval_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="customer_approver_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="customer_approval_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="affected_resources">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="affected_resource" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="affected_services">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="affected_service" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="approvals">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="approval" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="approver" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="approval_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="approval_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="comments">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="comment" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "response"
})
@XmlRootElement(name = "retrieveChangesResponse")
public class RetrieveChangesResponse {

    @XmlElement(required = true)
    protected RetrieveChangesResponse.Response response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveChangesResponse.Response }
     *     
     */
    public RetrieveChangesResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveChangesResponse.Response }
     *     
     */
    public void setResponse(RetrieveChangesResponse.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Changes">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Change" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Change_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="requested_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="risk" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="customer_site" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="change_source" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="assignment_resolver_group_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="assignee_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="requested_by_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="planned_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="planned_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="outage_required" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="outage_start_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="outage_end_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="outcome_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="customer_approval" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="customer_approval_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="customer_approver_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="customer_approval_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="affected_resources">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="affected_resource" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="affected_services">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="affected_service" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="approvals">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="approval" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="approver" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="approval_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="approval_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="comments">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="comment" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "changes",
        "errorCode",
        "errorMessage"
    })
    public static class Response {

        @XmlElement(name = "Changes", required = true)
        protected RetrieveChangesResponse.Response.Changes changes;
        @XmlElement(name = "error_code", required = true)
        protected String errorCode;
        @XmlElement(name = "error_message", required = true)
        protected String errorMessage;

        /**
         * Gets the value of the changes property.
         * 
         * @return
         *     possible object is
         *     {@link RetrieveChangesResponse.Response.Changes }
         *     
         */
        public RetrieveChangesResponse.Response.Changes getChanges() {
            return changes;
        }

        /**
         * Sets the value of the changes property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetrieveChangesResponse.Response.Changes }
         *     
         */
        public void setChanges(RetrieveChangesResponse.Response.Changes value) {
            this.changes = value;
        }

        /**
         * Gets the value of the errorCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorCode() {
            return errorCode;
        }

        /**
         * Sets the value of the errorCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorCode(String value) {
            this.errorCode = value;
        }

        /**
         * Gets the value of the errorMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorMessage() {
            return errorMessage;
        }

        /**
         * Sets the value of the errorMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorMessage(String value) {
            this.errorMessage = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Change" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Change_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="requested_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="risk" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="customer_site" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="change_source" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="assignment_resolver_group_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="assignee_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="requested_by_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="planned_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="planned_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="outage_required" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="outage_start_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="outage_end_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="outcome_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="customer_approval" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="customer_approval_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="customer_approver_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="customer_approval_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="affected_resources">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="affected_resource" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="affected_services">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="affected_service" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="approvals">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="approval" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="approver" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="approval_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="approval_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="comments">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="comment" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "change"
        })
        public static class Changes {

            @XmlElement(name = "Change", required = true)
            protected List<RetrieveChangesResponse.Response.Changes.Change> change;

            /**
             * Gets the value of the change property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the change property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getChange().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RetrieveChangesResponse.Response.Changes.Change }
             * 
             * 
             */
            public List<RetrieveChangesResponse.Response.Changes.Change> getChange() {
                if (change == null) {
                    change = new ArrayList<RetrieveChangesResponse.Response.Changes.Change>();
                }
                return this.change;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Change_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="requested_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="risk" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="customer_site" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="change_source" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="assignment_resolver_group_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="assignee_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="requested_by_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="planned_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="planned_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="outage_required" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="outage_start_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="outage_end_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="outcome_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="customer_approval" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="customer_approval_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="customer_approver_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="customer_approval_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="affected_resources">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="affected_resource" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="affected_services">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="affected_service" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="approvals">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="approval" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="approver" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="approval_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="approval_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="comments">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="comment" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "changeNumber",
                "requestedBy",
                "shortDescription",
                "description",
                "ticketStatus",
                "impact",
                "risk",
                "priority",
                "customerName",
                "customerUniqueId",
                "customerSite",
                "category",
                "type",
                "changeSource",
                "assignmentResolverGroupName",
                "assigneeName",
                "requestedByDate",
                "plannedStartDate",
                "plannedEndDate",
                "outageRequired",
                "outageStartTime",
                "outageEndTime",
                "outcomeStatus",
                "customerApproval",
                "customerApprovalStatus",
                "customerApproverName",
                "customerApprovalDate",
                "affectedResources",
                "affectedServices",
                "approvals",
                "created",
                "updated",
                "createdBy",
                "updatedBy",
                "comments"
            })
            public static class Change {

                @XmlElement(name = "Change_number", required = true)
                protected String changeNumber;
                @XmlElement(name = "requested_by", required = true)
                protected String requestedBy;
                @XmlElement(name = "short_description", required = true)
                protected String shortDescription;
                @XmlElement(required = true)
                protected String description;
                @XmlElement(name = "ticket_status", required = true)
                protected String ticketStatus;
                @XmlElement(required = true)
                protected String impact;
                @XmlElement(required = true)
                protected String risk;
                protected int priority;
                @XmlElement(name = "customer_name", required = true)
                protected String customerName;
                @XmlElement(name = "customer_unique_id")
                protected int customerUniqueId;
                @XmlElement(name = "customer_site", required = true)
                protected String customerSite;
                @XmlElement(required = true)
                protected String category;
                @XmlElement(required = true)
                protected String type;
                @XmlElement(name = "change_source", required = true)
                protected String changeSource;
                @XmlElement(name = "assignment_resolver_group_name", required = true)
                protected String assignmentResolverGroupName;
                @XmlElement(name = "assignee_name", required = true)
                protected String assigneeName;
                @XmlElement(name = "requested_by_date", required = true)
                protected String requestedByDate;
                @XmlElement(name = "planned_start_date", required = true)
                protected String plannedStartDate;
                @XmlElement(name = "planned_end_date", required = true)
                protected String plannedEndDate;
                @XmlElement(name = "outage_required", required = true)
                protected String outageRequired;
                @XmlElement(name = "outage_start_time", required = true)
                protected String outageStartTime;
                @XmlElement(name = "outage_end_time", required = true)
                protected String outageEndTime;
                @XmlElement(name = "outcome_status", required = true)
                protected String outcomeStatus;
                @XmlElement(name = "customer_approval", required = true)
                protected String customerApproval;
                @XmlElement(name = "customer_approval_status", required = true)
                protected String customerApprovalStatus;
                @XmlElement(name = "customer_approver_name", required = true)
                protected String customerApproverName;
                @XmlElement(name = "customer_approval_date", required = true)
                protected String customerApprovalDate;
                @XmlElement(name = "affected_resources", required = true)
                protected RetrieveChangesResponse.Response.Changes.Change.AffectedResources affectedResources;
                @XmlElement(name = "affected_services", required = true)
                protected RetrieveChangesResponse.Response.Changes.Change.AffectedServices affectedServices;
                @XmlElement(required = true)
                protected RetrieveChangesResponse.Response.Changes.Change.Approvals approvals;
                @XmlElement(required = true)
                protected String created;
                @XmlElement(required = true)
                protected String updated;
                @XmlElement(name = "created_by", required = true)
                protected String createdBy;
                @XmlElement(name = "updated_by", required = true)
                protected String updatedBy;
                @XmlElement(required = true)
                protected RetrieveChangesResponse.Response.Changes.Change.Comments comments;

                /**
                 * Gets the value of the changeNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getChangeNumber() {
                    return changeNumber;
                }

                /**
                 * Sets the value of the changeNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setChangeNumber(String value) {
                    this.changeNumber = value;
                }

                /**
                 * Gets the value of the requestedBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequestedBy() {
                    return requestedBy;
                }

                /**
                 * Sets the value of the requestedBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequestedBy(String value) {
                    this.requestedBy = value;
                }

                /**
                 * Gets the value of the shortDescription property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShortDescription() {
                    return shortDescription;
                }

                /**
                 * Sets the value of the shortDescription property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShortDescription(String value) {
                    this.shortDescription = value;
                }

                /**
                 * Gets the value of the description property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Sets the value of the description property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

                /**
                 * Gets the value of the ticketStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTicketStatus() {
                    return ticketStatus;
                }

                /**
                 * Sets the value of the ticketStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTicketStatus(String value) {
                    this.ticketStatus = value;
                }

                /**
                 * Gets the value of the impact property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getImpact() {
                    return impact;
                }

                /**
                 * Sets the value of the impact property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setImpact(String value) {
                    this.impact = value;
                }

                /**
                 * Gets the value of the risk property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRisk() {
                    return risk;
                }

                /**
                 * Sets the value of the risk property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRisk(String value) {
                    this.risk = value;
                }

                /**
                 * Gets the value of the priority property.
                 * 
                 */
                public int getPriority() {
                    return priority;
                }

                /**
                 * Sets the value of the priority property.
                 * 
                 */
                public void setPriority(int value) {
                    this.priority = value;
                }

                /**
                 * Gets the value of the customerName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerName() {
                    return customerName;
                }

                /**
                 * Sets the value of the customerName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerName(String value) {
                    this.customerName = value;
                }

                /**
                 * Gets the value of the customerUniqueId property.
                 * 
                 */
                public int getCustomerUniqueId() {
                    return customerUniqueId;
                }

                /**
                 * Sets the value of the customerUniqueId property.
                 * 
                 */
                public void setCustomerUniqueId(int value) {
                    this.customerUniqueId = value;
                }

                /**
                 * Gets the value of the customerSite property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerSite() {
                    return customerSite;
                }

                /**
                 * Sets the value of the customerSite property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerSite(String value) {
                    this.customerSite = value;
                }

                /**
                 * Gets the value of the category property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCategory() {
                    return category;
                }

                /**
                 * Sets the value of the category property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCategory(String value) {
                    this.category = value;
                }

                /**
                 * Gets the value of the type property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Gets the value of the changeSource property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getChangeSource() {
                    return changeSource;
                }

                /**
                 * Sets the value of the changeSource property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setChangeSource(String value) {
                    this.changeSource = value;
                }

                /**
                 * Gets the value of the assignmentResolverGroupName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAssignmentResolverGroupName() {
                    return assignmentResolverGroupName;
                }

                /**
                 * Sets the value of the assignmentResolverGroupName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAssignmentResolverGroupName(String value) {
                    this.assignmentResolverGroupName = value;
                }

                /**
                 * Gets the value of the assigneeName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getAssigneeName() {
                    return assigneeName;
                }

                /**
                 * Sets the value of the assigneeName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setAssigneeName(String value) {
                    this.assigneeName = value;
                }

                /**
                 * Gets the value of the requestedByDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequestedByDate() {
                    return requestedByDate;
                }

                /**
                 * Sets the value of the requestedByDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequestedByDate(String value) {
                    this.requestedByDate = value;
                }

                /**
                 * Gets the value of the plannedStartDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPlannedStartDate() {
                    return plannedStartDate;
                }

                /**
                 * Sets the value of the plannedStartDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPlannedStartDate(String value) {
                    this.plannedStartDate = value;
                }

                /**
                 * Gets the value of the plannedEndDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPlannedEndDate() {
                    return plannedEndDate;
                }

                /**
                 * Sets the value of the plannedEndDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPlannedEndDate(String value) {
                    this.plannedEndDate = value;
                }

                /**
                 * Gets the value of the outageRequired property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOutageRequired() {
                    return outageRequired;
                }

                /**
                 * Sets the value of the outageRequired property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOutageRequired(String value) {
                    this.outageRequired = value;
                }

                /**
                 * Gets the value of the outageStartTime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOutageStartTime() {
                    return outageStartTime;
                }

                /**
                 * Sets the value of the outageStartTime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOutageStartTime(String value) {
                    this.outageStartTime = value;
                }

                /**
                 * Gets the value of the outageEndTime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOutageEndTime() {
                    return outageEndTime;
                }

                /**
                 * Sets the value of the outageEndTime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOutageEndTime(String value) {
                    this.outageEndTime = value;
                }

                /**
                 * Gets the value of the outcomeStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOutcomeStatus() {
                    return outcomeStatus;
                }

                /**
                 * Sets the value of the outcomeStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOutcomeStatus(String value) {
                    this.outcomeStatus = value;
                }

                /**
                 * Gets the value of the customerApproval property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerApproval() {
                    return customerApproval;
                }

                /**
                 * Sets the value of the customerApproval property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerApproval(String value) {
                    this.customerApproval = value;
                }

                /**
                 * Gets the value of the customerApprovalStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerApprovalStatus() {
                    return customerApprovalStatus;
                }

                /**
                 * Sets the value of the customerApprovalStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerApprovalStatus(String value) {
                    this.customerApprovalStatus = value;
                }

                /**
                 * Gets the value of the customerApproverName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerApproverName() {
                    return customerApproverName;
                }

                /**
                 * Sets the value of the customerApproverName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerApproverName(String value) {
                    this.customerApproverName = value;
                }

                /**
                 * Gets the value of the customerApprovalDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerApprovalDate() {
                    return customerApprovalDate;
                }

                /**
                 * Sets the value of the customerApprovalDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerApprovalDate(String value) {
                    this.customerApprovalDate = value;
                }

                /**
                 * Gets the value of the affectedResources property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.AffectedResources }
                 *     
                 */
                public RetrieveChangesResponse.Response.Changes.Change.AffectedResources getAffectedResources() {
                    return affectedResources;
                }

                /**
                 * Sets the value of the affectedResources property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.AffectedResources }
                 *     
                 */
                public void setAffectedResources(RetrieveChangesResponse.Response.Changes.Change.AffectedResources value) {
                    this.affectedResources = value;
                }

                /**
                 * Gets the value of the affectedServices property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.AffectedServices }
                 *     
                 */
                public RetrieveChangesResponse.Response.Changes.Change.AffectedServices getAffectedServices() {
                    return affectedServices;
                }

                /**
                 * Sets the value of the affectedServices property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.AffectedServices }
                 *     
                 */
                public void setAffectedServices(RetrieveChangesResponse.Response.Changes.Change.AffectedServices value) {
                    this.affectedServices = value;
                }

                /**
                 * Gets the value of the approvals property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.Approvals }
                 *     
                 */
                public RetrieveChangesResponse.Response.Changes.Change.Approvals getApprovals() {
                    return approvals;
                }

                /**
                 * Sets the value of the approvals property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.Approvals }
                 *     
                 */
                public void setApprovals(RetrieveChangesResponse.Response.Changes.Change.Approvals value) {
                    this.approvals = value;
                }

                /**
                 * Gets the value of the created property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCreated() {
                    return created;
                }

                /**
                 * Sets the value of the created property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCreated(String value) {
                    this.created = value;
                }

                /**
                 * Gets the value of the updated property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUpdated() {
                    return updated;
                }

                /**
                 * Sets the value of the updated property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUpdated(String value) {
                    this.updated = value;
                }

                /**
                 * Gets the value of the createdBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCreatedBy() {
                    return createdBy;
                }

                /**
                 * Sets the value of the createdBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCreatedBy(String value) {
                    this.createdBy = value;
                }

                /**
                 * Gets the value of the updatedBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUpdatedBy() {
                    return updatedBy;
                }

                /**
                 * Sets the value of the updatedBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUpdatedBy(String value) {
                    this.updatedBy = value;
                }

                /**
                 * Gets the value of the comments property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.Comments }
                 *     
                 */
                public RetrieveChangesResponse.Response.Changes.Change.Comments getComments() {
                    return comments;
                }

                /**
                 * Sets the value of the comments property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RetrieveChangesResponse.Response.Changes.Change.Comments }
                 *     
                 */
                public void setComments(RetrieveChangesResponse.Response.Changes.Change.Comments value) {
                    this.comments = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="affected_resource" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "affectedResource"
                })
                public static class AffectedResources {

                    @XmlElement(name = "affected_resource", required = true)
                    protected List<RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource> affectedResource;

                    /**
                     * Gets the value of the affectedResource property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the affectedResource property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAffectedResource().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource }
                     * 
                     * 
                     */
                    public List<RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource> getAffectedResource() {
                        if (affectedResource == null) {
                            affectedResource = new ArrayList<RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource>();
                        }
                        return this.affectedResource;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ciId",
                        "ciName",
                        "siteId",
                        "cmdbClass"
                    })
                    public static class AffectedResource {

                        @XmlElement(name = "ci_id", required = true)
                        protected String ciId;
                        @XmlElement(name = "ci_name", required = true)
                        protected String ciName;
                        @XmlElement(name = "Site_Id", required = true)
                        protected String siteId;
                        @XmlElement(name = "cmdb_class", required = true)
                        protected String cmdbClass;

                        /**
                         * Gets the value of the ciId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiId() {
                            return ciId;
                        }

                        /**
                         * Sets the value of the ciId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiId(String value) {
                            this.ciId = value;
                        }

                        /**
                         * Gets the value of the ciName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiName() {
                            return ciName;
                        }

                        /**
                         * Sets the value of the ciName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiName(String value) {
                            this.ciName = value;
                        }

                        /**
                         * Gets the value of the siteId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteId() {
                            return siteId;
                        }

                        /**
                         * Sets the value of the siteId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteId(String value) {
                            this.siteId = value;
                        }

                        /**
                         * Gets the value of the cmdbClass property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCmdbClass() {
                            return cmdbClass;
                        }

                        /**
                         * Sets the value of the cmdbClass property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCmdbClass(String value) {
                            this.cmdbClass = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="affected_service" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "affectedService"
                })
                public static class AffectedServices {

                    @XmlElement(name = "affected_service", required = true)
                    protected List<RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService> affectedService;

                    /**
                     * Gets the value of the affectedService property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the affectedService property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAffectedService().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService }
                     * 
                     * 
                     */
                    public List<RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService> getAffectedService() {
                        if (affectedService == null) {
                            affectedService = new ArrayList<RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService>();
                        }
                        return this.affectedService;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ciId",
                        "ciName",
                        "siteId",
                        "cmdbClass"
                    })
                    public static class AffectedService {

                        @XmlElement(name = "ci_id", required = true)
                        protected String ciId;
                        @XmlElement(name = "ci_name", required = true)
                        protected String ciName;
                        @XmlElement(name = "Site_Id", required = true)
                        protected String siteId;
                        @XmlElement(name = "cmdb_class", required = true)
                        protected String cmdbClass;

                        /**
                         * Gets the value of the ciId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiId() {
                            return ciId;
                        }

                        /**
                         * Sets the value of the ciId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiId(String value) {
                            this.ciId = value;
                        }

                        /**
                         * Gets the value of the ciName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiName() {
                            return ciName;
                        }

                        /**
                         * Sets the value of the ciName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiName(String value) {
                            this.ciName = value;
                        }

                        /**
                         * Gets the value of the siteId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteId() {
                            return siteId;
                        }

                        /**
                         * Sets the value of the siteId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteId(String value) {
                            this.siteId = value;
                        }

                        /**
                         * Gets the value of the cmdbClass property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCmdbClass() {
                            return cmdbClass;
                        }

                        /**
                         * Sets the value of the cmdbClass property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCmdbClass(String value) {
                            this.cmdbClass = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="approval" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="approver" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="approval_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="approval_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "approval"
                })
                public static class Approvals {

                    @XmlElement(required = true)
                    protected List<RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval> approval;

                    /**
                     * Gets the value of the approval property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the approval property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getApproval().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval }
                     * 
                     * 
                     */
                    public List<RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval> getApproval() {
                        if (approval == null) {
                            approval = new ArrayList<RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval>();
                        }
                        return this.approval;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="approver" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="approval_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="approval_group" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "approver",
                        "approvalDateTime",
                        "approvalGroup"
                    })
                    public static class Approval {

                        @XmlElement(required = true)
                        protected String approver;
                        @XmlElement(name = "approval_date_time", required = true)
                        protected String approvalDateTime;
                        @XmlElement(name = "approval_group", required = true)
                        protected String approvalGroup;

                        /**
                         * Gets the value of the approver property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getApprover() {
                            return approver;
                        }

                        /**
                         * Sets the value of the approver property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setApprover(String value) {
                            this.approver = value;
                        }

                        /**
                         * Gets the value of the approvalDateTime property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getApprovalDateTime() {
                            return approvalDateTime;
                        }

                        /**
                         * Sets the value of the approvalDateTime property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setApprovalDateTime(String value) {
                            this.approvalDateTime = value;
                        }

                        /**
                         * Gets the value of the approvalGroup property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getApprovalGroup() {
                            return approvalGroup;
                        }

                        /**
                         * Sets the value of the approvalGroup property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setApprovalGroup(String value) {
                            this.approvalGroup = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="comment" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "comment"
                })
                public static class Comments {

                    @XmlElement(required = true)
                    protected List<RetrieveChangesResponse.Response.Changes.Change.Comments.Comment> comment;

                    /**
                     * Gets the value of the comment property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the comment property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getComment().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link RetrieveChangesResponse.Response.Changes.Change.Comments.Comment }
                     * 
                     * 
                     */
                    public List<RetrieveChangesResponse.Response.Changes.Change.Comments.Comment> getComment() {
                        if (comment == null) {
                            comment = new ArrayList<RetrieveChangesResponse.Response.Changes.Change.Comments.Comment>();
                        }
                        return this.comment;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "datetime",
                        "userid",
                        "note"
                    })
                    public static class Comment {

                        @XmlElement(required = true)
                        protected String datetime;
                        @XmlElement(required = true)
                        protected String userid;
                        @XmlElement(required = true)
                        protected String note;

                        /**
                         * Gets the value of the datetime property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getDatetime() {
                            return datetime;
                        }

                        /**
                         * Sets the value of the datetime property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setDatetime(String value) {
                            this.datetime = value;
                        }

                        /**
                         * Gets the value of the userid property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getUserid() {
                            return userid;
                        }

                        /**
                         * Sets the value of the userid property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setUserid(String value) {
                            this.userid = value;
                        }

                        /**
                         * Gets the value of the note property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNote() {
                            return note;
                        }

                        /**
                         * Sets the value of the note property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNote(String value) {
                            this.note = value;
                        }

                    }

                }

            }

        }

    }

}
