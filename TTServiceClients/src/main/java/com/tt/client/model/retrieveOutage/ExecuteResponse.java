
package com.tt.client.model.retrieveOutage;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Outages">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Outage" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Task_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Task_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Exclude_from_availability" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Actual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Contractual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Incident_closed_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Requestor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Created" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Affected_devices">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Affected_device" maxOccurs="unbounded" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *                                                           &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                           &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="Affected_services">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Affected_service" maxOccurs="unbounded" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="Affected_customers">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="Affected_customer" maxOccurs="unbounded" minOccurs="0">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "response"
})
@XmlRootElement(name = "executeResponse")
public class ExecuteResponse {

    @XmlElement(required = true)
    protected ExecuteResponse.Response response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ExecuteResponse.Response }
     *     
     */
    public ExecuteResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecuteResponse.Response }
     *     
     */
    public void setResponse(ExecuteResponse.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Outages">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Outage" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Task_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Task_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Exclude_from_availability" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Actual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Contractual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Incident_closed_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Requestor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Created" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Affected_devices">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Affected_device" maxOccurs="unbounded" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}short"/>
     *                                                 &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                                 &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="Affected_services">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Affected_service" maxOccurs="unbounded" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="Affected_customers">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="Affected_customer" maxOccurs="unbounded" minOccurs="0">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "outages",
        "errorCode",
        "errorMessage"
    })
    public static class Response {

        @XmlElement(name = "Outages", required = true)
        protected ExecuteResponse.Response.Outages outages;
        @XmlElement(name = "error_code", required = true)
        protected String errorCode;
        @XmlElement(name = "error_message", required = true)
        protected String errorMessage;

        /**
         * Gets the value of the outages property.
         * 
         * @return
         *     possible object is
         *     {@link ExecuteResponse.Response.Outages }
         *     
         */
        public ExecuteResponse.Response.Outages getOutages() {
            return outages;
        }

        /**
         * Sets the value of the outages property.
         * 
         * @param value
         *     allowed object is
         *     {@link ExecuteResponse.Response.Outages }
         *     
         */
        public void setOutages(ExecuteResponse.Response.Outages value) {
            this.outages = value;
        }

        /**
         * Gets the value of the errorCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorCode() {
            return errorCode;
        }

        /**
         * Sets the value of the errorCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorCode(String value) {
            this.errorCode = value;
        }

        /**
         * Gets the value of the errorMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorMessage() {
            return errorMessage;
        }

        /**
         * Sets the value of the errorMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorMessage(String value) {
            this.errorMessage = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Outage" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Task_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Task_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Exclude_from_availability" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Actual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Contractual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Incident_closed_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Requestor" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Created" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Affected_devices">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Affected_device" maxOccurs="unbounded" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}short"/>
         *                                       &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                                       &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="Affected_services">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Affected_service" maxOccurs="unbounded" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="Affected_customers">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="Affected_customer" maxOccurs="unbounded" minOccurs="0">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "outage"
        })
        public static class Outages {

            @XmlElement(name = "Outage")
            protected List<ExecuteResponse.Response.Outages.Outage> outage;

            /**
             * Gets the value of the outage property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the outage property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getOutage().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ExecuteResponse.Response.Outages.Outage }
             * 
             * 
             */
            public List<ExecuteResponse.Response.Outages.Outage> getOutage() {
                if (outage == null) {
                    outage = new ArrayList<ExecuteResponse.Response.Outages.Outage>();
                }
                return this.outage;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Task_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Task_type" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Exclude_from_availability" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Begin" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="End" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Actual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Contractual_duration" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Incident_closed_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Requestor" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Comments" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Created" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Affected_devices">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Affected_device" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}short"/>
             *                             &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                             &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Affected_services">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Affected_service" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="Affected_customers">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="Affected_customer" maxOccurs="unbounded" minOccurs="0">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "taskNumber",
                "taskType",
                "type",
                "shortDescription",
                "status",
                "category",
                "excludeFromAvailability",
                "begin",
                "end",
                "actualDuration",
                "contractualDuration",
                "incidentClosedDate",
                "requestor",
                "rootProductId",
                "comments",
                "created",
                "createdBy",
                "updated",
                "updatedBy",
                "affectedDevices",
                "affectedServices",
                "affectedCustomers"
            })
            public static class Outage {

                @XmlElement(name = "Task_number", required = true)
                protected String taskNumber;
                @XmlElement(name = "Task_type", required = true)
                protected String taskType;
                @XmlElement(required = true)
                protected String type;
                @XmlElement(name = "Short_description", required = true)
                protected String shortDescription;
                @XmlElement(name = "Status", required = true)
                protected String status;
                @XmlElement(name = "Category", required = true)
                protected String category;
                @XmlElement(name = "Exclude_from_availability", required = true)
                protected String excludeFromAvailability;
                @XmlElement(name = "Begin", required = true)
                protected String begin;
                @XmlElement(name = "End", required = true)
                protected String end;
                @XmlElement(name = "Actual_duration", required = true)
                protected String actualDuration;
                @XmlElement(name = "Contractual_duration", required = true)
                protected String contractualDuration;
                @XmlElement(name = "Incident_closed_date", required = true)
                protected String incidentClosedDate;
                @XmlElement(name = "Requestor", required = true)
                protected String requestor;
                @XmlElement(name = "Root_product_id", required = true)
                protected String rootProductId;
                @XmlElement(name = "Comments", required = true)
                protected String comments;
                @XmlElement(name = "Created", required = true)
                protected String created;
                @XmlElement(name = "Created_by", required = true)
                protected String createdBy;
                @XmlElement(name = "Updated", required = true)
                protected String updated;
                @XmlElement(name = "Updated_by", required = true)
                protected String updatedBy;
                @XmlElement(name = "Affected_devices", required = true)
                protected ExecuteResponse.Response.Outages.Outage.AffectedDevices affectedDevices;
                @XmlElement(name = "Affected_services", required = true)
                protected ExecuteResponse.Response.Outages.Outage.AffectedServices affectedServices;
                @XmlElement(name = "Affected_customers", required = true)
                protected ExecuteResponse.Response.Outages.Outage.AffectedCustomers affectedCustomers;

                /**
                 * Gets the value of the taskNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTaskNumber() {
                    return taskNumber;
                }

                /**
                 * Sets the value of the taskNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTaskNumber(String value) {
                    this.taskNumber = value;
                }

                /**
                 * Gets the value of the taskType property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTaskType() {
                    return taskType;
                }

                /**
                 * Sets the value of the taskType property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTaskType(String value) {
                    this.taskType = value;
                }

                /**
                 * Gets the value of the type property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getType() {
                    return type;
                }

                /**
                 * Sets the value of the type property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setType(String value) {
                    this.type = value;
                }

                /**
                 * Gets the value of the shortDescription property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShortDescription() {
                    return shortDescription;
                }

                /**
                 * Sets the value of the shortDescription property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShortDescription(String value) {
                    this.shortDescription = value;
                }

                /**
                 * Gets the value of the status property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getStatus() {
                    return status;
                }

                /**
                 * Sets the value of the status property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setStatus(String value) {
                    this.status = value;
                }

                /**
                 * Gets the value of the category property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCategory() {
                    return category;
                }

                /**
                 * Sets the value of the category property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCategory(String value) {
                    this.category = value;
                }

                /**
                 * Gets the value of the excludeFromAvailability property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getExcludeFromAvailability() {
                    return excludeFromAvailability;
                }

                /**
                 * Sets the value of the excludeFromAvailability property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setExcludeFromAvailability(String value) {
                    this.excludeFromAvailability = value;
                }

                /**
                 * Gets the value of the begin property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getBegin() {
                    return begin;
                }

                /**
                 * Sets the value of the begin property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setBegin(String value) {
                    this.begin = value;
                }

                /**
                 * Gets the value of the end property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEnd() {
                    return end;
                }

                /**
                 * Sets the value of the end property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEnd(String value) {
                    this.end = value;
                }

                /**
                 * Gets the value of the actualDuration property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getActualDuration() {
                    return actualDuration;
                }

                /**
                 * Sets the value of the actualDuration property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setActualDuration(String value) {
                    this.actualDuration = value;
                }

                /**
                 * Gets the value of the contractualDuration property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getContractualDuration() {
                    return contractualDuration;
                }

                /**
                 * Sets the value of the contractualDuration property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setContractualDuration(String value) {
                    this.contractualDuration = value;
                }

                /**
                 * Gets the value of the incidentClosedDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIncidentClosedDate() {
                    return incidentClosedDate;
                }

                /**
                 * Sets the value of the incidentClosedDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIncidentClosedDate(String value) {
                    this.incidentClosedDate = value;
                }

                /**
                 * Gets the value of the requestor property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequestor() {
                    return requestor;
                }

                /**
                 * Sets the value of the requestor property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequestor(String value) {
                    this.requestor = value;
                }

                /**
                 * Gets the value of the rootProductId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRootProductId() {
                    return rootProductId;
                }

                /**
                 * Sets the value of the rootProductId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRootProductId(String value) {
                    this.rootProductId = value;
                }

                /**
                 * Gets the value of the comments property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getComments() {
                    return comments;
                }

                /**
                 * Sets the value of the comments property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setComments(String value) {
                    this.comments = value;
                }

                /**
                 * Gets the value of the created property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCreated() {
                    return created;
                }

                /**
                 * Sets the value of the created property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCreated(String value) {
                    this.created = value;
                }

                /**
                 * Gets the value of the createdBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCreatedBy() {
                    return createdBy;
                }

                /**
                 * Sets the value of the createdBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCreatedBy(String value) {
                    this.createdBy = value;
                }

                /**
                 * Gets the value of the updated property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUpdated() {
                    return updated;
                }

                /**
                 * Sets the value of the updated property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUpdated(String value) {
                    this.updated = value;
                }

                /**
                 * Gets the value of the updatedBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUpdatedBy() {
                    return updatedBy;
                }

                /**
                 * Sets the value of the updatedBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUpdatedBy(String value) {
                    this.updatedBy = value;
                }

                /**
                 * Gets the value of the affectedDevices property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ExecuteResponse.Response.Outages.Outage.AffectedDevices }
                 *     
                 */
                public ExecuteResponse.Response.Outages.Outage.AffectedDevices getAffectedDevices() {
                    return affectedDevices;
                }

                /**
                 * Sets the value of the affectedDevices property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ExecuteResponse.Response.Outages.Outage.AffectedDevices }
                 *     
                 */
                public void setAffectedDevices(ExecuteResponse.Response.Outages.Outage.AffectedDevices value) {
                    this.affectedDevices = value;
                }

                /**
                 * Gets the value of the affectedServices property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ExecuteResponse.Response.Outages.Outage.AffectedServices }
                 *     
                 */
                public ExecuteResponse.Response.Outages.Outage.AffectedServices getAffectedServices() {
                    return affectedServices;
                }

                /**
                 * Sets the value of the affectedServices property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ExecuteResponse.Response.Outages.Outage.AffectedServices }
                 *     
                 */
                public void setAffectedServices(ExecuteResponse.Response.Outages.Outage.AffectedServices value) {
                    this.affectedServices = value;
                }

                /**
                 * Gets the value of the affectedCustomers property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ExecuteResponse.Response.Outages.Outage.AffectedCustomers }
                 *     
                 */
                public ExecuteResponse.Response.Outages.Outage.AffectedCustomers getAffectedCustomers() {
                    return affectedCustomers;
                }

                /**
                 * Sets the value of the affectedCustomers property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ExecuteResponse.Response.Outages.Outage.AffectedCustomers }
                 *     
                 */
                public void setAffectedCustomers(ExecuteResponse.Response.Outages.Outage.AffectedCustomers value) {
                    this.affectedCustomers = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Affected_customer" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "affectedCustomer"
                })
                public static class AffectedCustomers {

                    @XmlElement(name = "Affected_customer")
                    protected List<ExecuteResponse.Response.Outages.Outage.AffectedCustomers.AffectedCustomer> affectedCustomer;

                    /**
                     * Gets the value of the affectedCustomer property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the affectedCustomer property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAffectedCustomer().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ExecuteResponse.Response.Outages.Outage.AffectedCustomers.AffectedCustomer }
                     * 
                     * 
                     */
                    public List<ExecuteResponse.Response.Outages.Outage.AffectedCustomers.AffectedCustomer> getAffectedCustomer() {
                        if (affectedCustomer == null) {
                            affectedCustomer = new ArrayList<ExecuteResponse.Response.Outages.Outage.AffectedCustomers.AffectedCustomer>();
                        }
                        return this.affectedCustomer;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "customerName",
                        "customerId"
                    })
                    public static class AffectedCustomer {

                        @XmlElement(name = "customer_name", required = true)
                        protected String customerName;
                        @XmlElement(name = "customer_id")
                        protected int customerId;

                        /**
                         * Gets the value of the customerName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCustomerName() {
                            return customerName;
                        }

                        /**
                         * Sets the value of the customerName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCustomerName(String value) {
                            this.customerName = value;
                        }

                        /**
                         * Gets the value of the customerId property.
                         * 
                         */
                        public int getCustomerId() {
                            return customerId;
                        }

                        /**
                         * Sets the value of the customerId property.
                         * 
                         */
                        public void setCustomerId(int value) {
                            this.customerId = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Affected_device" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}short"/>
                 *                   &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
                 *                   &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "affectedDevice"
                })
                public static class AffectedDevices {

                    @XmlElement(name = "Affected_device")
                    protected List<ExecuteResponse.Response.Outages.Outage.AffectedDevices.AffectedDevice> affectedDevice;

                    /**
                     * Gets the value of the affectedDevice property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the affectedDevice property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAffectedDevice().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ExecuteResponse.Response.Outages.Outage.AffectedDevices.AffectedDevice }
                     * 
                     * 
                     */
                    public List<ExecuteResponse.Response.Outages.Outage.AffectedDevices.AffectedDevice> getAffectedDevice() {
                        if (affectedDevice == null) {
                            affectedDevice = new ArrayList<ExecuteResponse.Response.Outages.Outage.AffectedDevices.AffectedDevice>();
                        }
                        return this.affectedDevice;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}short"/>
                     *         &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}int"/>
                     *         &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ciId",
                        "ciName",
                        "customerId",
                        "siteId",
                        "cmdbClass"
                    })
                    public static class AffectedDevice {

                        @XmlElement(name = "ci_id", required = true)
                        protected String ciId;
                        @XmlElement(name = "ci_name", required = true)
                        protected String ciName;
                        @XmlElement(name = "customer_id")
                        protected short customerId;
                        @XmlElement(name = "Site_Id")
                        protected int siteId;
                        @XmlElement(name = "cmdb_class", required = true)
                        protected String cmdbClass;

                        /**
                         * Gets the value of the ciId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiId() {
                            return ciId;
                        }

                        /**
                         * Sets the value of the ciId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiId(String value) {
                            this.ciId = value;
                        }

                        /**
                         * Gets the value of the ciName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiName() {
                            return ciName;
                        }

                        /**
                         * Sets the value of the ciName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiName(String value) {
                            this.ciName = value;
                        }

                        /**
                         * Gets the value of the customerId property.
                         * 
                         */
                        public short getCustomerId() {
                            return customerId;
                        }

                        /**
                         * Sets the value of the customerId property.
                         * 
                         */
                        public void setCustomerId(short value) {
                            this.customerId = value;
                        }

                        /**
                         * Gets the value of the siteId property.
                         * 
                         */
                        public int getSiteId() {
                            return siteId;
                        }

                        /**
                         * Sets the value of the siteId property.
                         * 
                         */
                        public void setSiteId(int value) {
                            this.siteId = value;
                        }

                        /**
                         * Gets the value of the cmdbClass property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCmdbClass() {
                            return cmdbClass;
                        }

                        /**
                         * Sets the value of the cmdbClass property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCmdbClass(String value) {
                            this.cmdbClass = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="Affected_service" maxOccurs="unbounded" minOccurs="0">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "affectedService"
                })
                public static class AffectedServices {

                    @XmlElement(name = "Affected_service")
                    protected List<ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService> affectedService;

                    /**
                     * Gets the value of the affectedService property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the affectedService property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAffectedService().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService }
                     * 
                     * 
                     */
                    public List<ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService> getAffectedService() {
                        if (affectedService == null) {
                            affectedService = new ArrayList<ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService>();
                        }
                        return this.affectedService;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="customer_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ciId",
                        "ciName",
                        "customerId",
                        "siteId",
                        "cmdbClass"
                    })
                    public static class AffectedService {

                        @XmlElement(name = "ci_id", required = true)
                        protected String ciId;
                        @XmlElement(name = "ci_name", required = true)
                        protected String ciName;
                        @XmlElement(name = "customer_id", required = true)
                        protected String customerId;
                        @XmlElement(name = "Site_Id", required = true)
                        protected String siteId;
                        @XmlElement(name = "cmdb_class", required = true)
                        protected String cmdbClass;

                        /**
                         * Gets the value of the ciId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiId() {
                            return ciId;
                        }

                        /**
                         * Sets the value of the ciId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiId(String value) {
                            this.ciId = value;
                        }

                        /**
                         * Gets the value of the ciName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiName() {
                            return ciName;
                        }

                        /**
                         * Sets the value of the ciName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiName(String value) {
                            this.ciName = value;
                        }

                        /**
                         * Gets the value of the customerId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCustomerId() {
                            return customerId;
                        }

                        /**
                         * Sets the value of the customerId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCustomerId(String value) {
                            this.customerId = value;
                        }

                        /**
                         * Gets the value of the siteId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteId() {
                            return siteId;
                        }

                        /**
                         * Sets the value of the siteId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteId(String value) {
                            this.siteId = value;
                        }

                        /**
                         * Gets the value of the cmdbClass property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCmdbClass() {
                            return cmdbClass;
                        }

                        /**
                         * Sets the value of the cmdbClass property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCmdbClass(String value) {
                            this.cmdbClass = value;
                        }

                    }

                }

            }

        }

    }

}
