package com.tt.client.model.common;

import java.util.ArrayList;

import com.tt.model.Incident;

public class Incidents {

	ArrayList<Incident> IncList = new ArrayList<Incident>();;

	public ArrayList<Incident> getIncList() {
		return IncList;
	}

	public void setIncList(ArrayList<Incident> incList) {
		IncList = incList;
	}

	@Override
	public String toString() {
		return "Incidents [IncList=" + IncList + ", getIncList()="
				+ getIncList() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

}
