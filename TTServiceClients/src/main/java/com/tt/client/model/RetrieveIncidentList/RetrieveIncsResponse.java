
package com.tt.client.model.RetrieveIncidentList;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Incidents">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Incident" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Incident_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Opened_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="urgency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="caller" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="site_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                                       &lt;element name="site_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="site_zone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="site_service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="affected_resources">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="affected_resource" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="affected_services">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="affected_service" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                       &lt;element name="reported_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="subcategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="case_manager_user_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="case_manager_full_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="mim" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="customer_impacted" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="hierarchical_escalation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="service_level_target" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="resolution_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="closed_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="resolution_category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="resolution_sub_category_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="resolution_sub_category_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="resolution_sub_category_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="last_updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="comments">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="comment" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "response"
})
@XmlRootElement(name = "retrieveIncsResponse")
public class RetrieveIncsResponse {

    @XmlElement(required = true)
    protected RetrieveIncsResponse.Response response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveIncsResponse.Response }
     *     
     */
    public RetrieveIncsResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveIncsResponse.Response }
     *     
     */
    public void setResponse(RetrieveIncsResponse.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Incidents">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Incident" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Incident_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Opened_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="urgency" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="caller" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="site_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                             &lt;element name="site_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="site_zone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="site_service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="affected_resources">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="affected_resource" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="affected_services">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="affected_service" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                             &lt;element name="reported_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="subcategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="case_manager_user_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="case_manager_full_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="mim" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="customer_impacted" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="hierarchical_escalation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="service_level_target" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="resolution_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="closed_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="resolution_category" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="resolution_sub_category_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="resolution_sub_category_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="resolution_sub_category_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="last_updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="comments">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="comment" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "incidents",
        "errorCode",
        "errorMessage"
    })
    public static class Response {

        @XmlElement(name = "Incidents", required = true)
        protected RetrieveIncsResponse.Response.Incidents incidents;
        @XmlElement(name = "error_code", required = true)
        protected String errorCode;
        @XmlElement(name = "error_message", required = true)
        protected String errorMessage;

        /**
         * Gets the value of the incidents property.
         * 
         * @return
         *     possible object is
         *     {@link RetrieveIncsResponse.Response.Incidents }
         *     
         */
        public RetrieveIncsResponse.Response.Incidents getIncidents() {
            return incidents;
        }

        /**
         * Sets the value of the incidents property.
         * 
         * @param value
         *     allowed object is
         *     {@link RetrieveIncsResponse.Response.Incidents }
         *     
         */
        public void setIncidents(RetrieveIncsResponse.Response.Incidents value) {
            this.incidents = value;
        }

        /**
         * Gets the value of the errorCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorCode() {
            return errorCode;
        }

        /**
         * Sets the value of the errorCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorCode(String value) {
            this.errorCode = value;
        }

        /**
         * Gets the value of the errorMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorMessage() {
            return errorMessage;
        }

        /**
         * Sets the value of the errorMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorMessage(String value) {
            this.errorMessage = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Incident" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Incident_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Opened_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="urgency" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="caller" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="site_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *                   &lt;element name="site_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="site_zone" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="site_service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="affected_resources">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="affected_resource" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="affected_services">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="affected_service" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                   &lt;element name="reported_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="subcategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="case_manager_user_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="case_manager_full_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="mim" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="customer_impacted" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="hierarchical_escalation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="service_level_target" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="resolution_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="closed_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="resolution_category" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="resolution_sub_category_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="resolution_sub_category_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="resolution_sub_category_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="last_updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="comments">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="comment" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "incident"
        })
        public static class Incidents {

            @XmlElement(name = "Incident", required = true)
            protected List<RetrieveIncsResponse.Response.Incidents.Incident> incident;

            /**
             * Gets the value of the incident property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the incident property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getIncident().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RetrieveIncsResponse.Response.Incidents.Incident }
             * 
             * 
             */
            public List<RetrieveIncsResponse.Response.Incidents.Incident> getIncident() {
                if (incident == null) {
                    incident = new ArrayList<RetrieveIncsResponse.Response.Incidents.Incident>();
                }
                return this.incident;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Incident_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Opened_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="ticket_status" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="notes" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="impact" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="urgency" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="priority" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="customer_unique_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="customer_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="caller" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="site_id" type="{http://www.w3.org/2001/XMLSchema}int"/>
             *         &lt;element name="site_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="site_zone" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="site_service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="affected_resources">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="affected_resource" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="affected_services">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="affected_service" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *         &lt;element name="reported_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="category" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="subcategory" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="case_manager_user_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="case_manager_full_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="mim" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="customer_impacted" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="hierarchical_escalation" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="service_level_target" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="resolution_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="root_product_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="closed_at" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="resolution_category" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="resolution_sub_category_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="resolution_sub_category_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="resolution_sub_category_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="last_updated" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="comments">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="comment" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "incidentId",
                "openedAt",
                "shortDescription",
                "ticketStatus",
                "notes",
                "impact",
                "urgency",
                "priority",
                "customerUniqueId",
                "customerName",
                "caller",
                "siteId",
                "siteName",
                "siteZone",
                "siteServiceTier",
                "affectedResources",
                "affectedServices",
                "reportedDateTime",
                "category",
                "subcategory",
                "caseManagerUserId",
                "caseManagerFullName",
                "mim",
                "customerImpacted",
                "hierarchicalEscalation",
                "serviceLevelTarget",
                "resolutionDateTime",
                "rootProductId",
                "closedAt",
                "resolutionCategory",
                "resolutionSubCategory1",
                "resolutionSubCategory2",
                "resolutionSubCategory3",
                "lastUpdated",
                "created",
                "createdBy",
                "updatedBy",
                "comments"
            })
            public static class Incident {

                @XmlElement(name = "Incident_id", required = true)
                protected String incidentId;
                @XmlElement(name = "Opened_at", required = true)
                protected String openedAt;
                @XmlElement(name = "short_description", required = true)
                protected String shortDescription;
                @XmlElement(name = "ticket_status", required = true)
                protected String ticketStatus;
                @XmlElement(required = true)
                protected String notes;
                @XmlElement(required = true)
                protected String impact;
                @XmlElement(required = true)
                protected String urgency;
                protected int priority;
                @XmlElement(name = "customer_unique_id")
                protected int customerUniqueId;
                @XmlElement(name = "customer_name", required = true)
                protected String customerName;
                @XmlElement(required = true)
                protected String caller;
                @XmlElement(name = "site_id")
                protected int siteId;
                @XmlElement(name = "site_name", required = true)
                protected String siteName;
                @XmlElement(name = "site_zone", required = true)
                protected String siteZone;
                @XmlElement(name = "site_service_tier", required = true)
                protected String siteServiceTier;
                @XmlElement(name = "affected_resources", required = true)
                protected RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources affectedResources;
                @XmlElement(name = "affected_services", required = true)
                protected RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices affectedServices;
                @XmlElement(name = "reported_date_time", required = true)
                protected String reportedDateTime;
                @XmlElement(required = true)
                protected String category;
                @XmlElement(required = true)
                protected String subcategory;
                @XmlElement(name = "case_manager_user_id", required = true)
                protected String caseManagerUserId;
                @XmlElement(name = "case_manager_full_name", required = true)
                protected String caseManagerFullName;
                @XmlElement(required = true)
                protected String mim;
                @XmlElement(name = "customer_impacted", required = true)
                protected String customerImpacted;
                @XmlElement(name = "hierarchical_escalation", required = true)
                protected String hierarchicalEscalation;
                @XmlElement(name = "service_level_target", required = true)
                protected String serviceLevelTarget;
                @XmlElement(name = "resolution_date_time", required = true)
                protected String resolutionDateTime;
                @XmlElement(name = "root_product_id", required = true)
                protected String rootProductId;
                @XmlElement(name = "closed_at", required = true)
                protected String closedAt;
                @XmlElement(name = "resolution_category", required = true)
                protected String resolutionCategory;
                @XmlElement(name = "resolution_sub_category_1", required = true)
                protected String resolutionSubCategory1;
                @XmlElement(name = "resolution_sub_category_2", required = true)
                protected String resolutionSubCategory2;
                @XmlElement(name = "resolution_sub_category_3", required = true)
                protected String resolutionSubCategory3;
                @XmlElement(name = "last_updated", required = true)
                protected String lastUpdated;
                @XmlElement(required = true)
                protected String created;
                @XmlElement(name = "created_by", required = true)
                protected String createdBy;
                @XmlElement(name = "updated_by", required = true)
                protected String updatedBy;
                @XmlElement(required = true)
                protected RetrieveIncsResponse.Response.Incidents.Incident.Comments comments;

                /**
                 * Gets the value of the incidentId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIncidentId() {
                    return incidentId;
                }

                /**
                 * Sets the value of the incidentId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIncidentId(String value) {
                    this.incidentId = value;
                }

                /**
                 * Gets the value of the openedAt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getOpenedAt() {
                    return openedAt;
                }

                /**
                 * Sets the value of the openedAt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setOpenedAt(String value) {
                    this.openedAt = value;
                }

                /**
                 * Gets the value of the shortDescription property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShortDescription() {
                    return shortDescription;
                }

                /**
                 * Sets the value of the shortDescription property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShortDescription(String value) {
                    this.shortDescription = value;
                }

                /**
                 * Gets the value of the ticketStatus property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTicketStatus() {
                    return ticketStatus;
                }

                /**
                 * Sets the value of the ticketStatus property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTicketStatus(String value) {
                    this.ticketStatus = value;
                }

                /**
                 * Gets the value of the notes property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNotes() {
                    return notes;
                }

                /**
                 * Sets the value of the notes property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNotes(String value) {
                    this.notes = value;
                }

                /**
                 * Gets the value of the impact property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getImpact() {
                    return impact;
                }

                /**
                 * Sets the value of the impact property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setImpact(String value) {
                    this.impact = value;
                }

                /**
                 * Gets the value of the urgency property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUrgency() {
                    return urgency;
                }

                /**
                 * Sets the value of the urgency property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUrgency(String value) {
                    this.urgency = value;
                }

                /**
                 * Gets the value of the priority property.
                 * 
                 */
                public int getPriority() {
                    return priority;
                }

                /**
                 * Sets the value of the priority property.
                 * 
                 */
                public void setPriority(int value) {
                    this.priority = value;
                }

                /**
                 * Gets the value of the customerUniqueId property.
                 * 
                 */
                public int getCustomerUniqueId() {
                    return customerUniqueId;
                }

                /**
                 * Sets the value of the customerUniqueId property.
                 * 
                 */
                public void setCustomerUniqueId(int value) {
                    this.customerUniqueId = value;
                }

                /**
                 * Gets the value of the customerName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerName() {
                    return customerName;
                }

                /**
                 * Sets the value of the customerName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerName(String value) {
                    this.customerName = value;
                }

                /**
                 * Gets the value of the caller property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCaller() {
                    return caller;
                }

                /**
                 * Sets the value of the caller property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCaller(String value) {
                    this.caller = value;
                }

                /**
                 * Gets the value of the siteId property.
                 * 
                 */
                public int getSiteId() {
                    return siteId;
                }

                /**
                 * Sets the value of the siteId property.
                 * 
                 */
                public void setSiteId(int value) {
                    this.siteId = value;
                }

                /**
                 * Gets the value of the siteName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSiteName() {
                    return siteName;
                }

                /**
                 * Sets the value of the siteName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSiteName(String value) {
                    this.siteName = value;
                }

                /**
                 * Gets the value of the siteZone property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSiteZone() {
                    return siteZone;
                }

                /**
                 * Sets the value of the siteZone property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSiteZone(String value) {
                    this.siteZone = value;
                }

                /**
                 * Gets the value of the siteServiceTier property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSiteServiceTier() {
                    return siteServiceTier;
                }

                /**
                 * Sets the value of the siteServiceTier property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSiteServiceTier(String value) {
                    this.siteServiceTier = value;
                }

                /**
                 * Gets the value of the affectedResources property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources }
                 *     
                 */
                public RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources getAffectedResources() {
                    return affectedResources;
                }

                /**
                 * Sets the value of the affectedResources property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources }
                 *     
                 */
                public void setAffectedResources(RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources value) {
                    this.affectedResources = value;
                }

                /**
                 * Gets the value of the affectedServices property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices }
                 *     
                 */
                public RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices getAffectedServices() {
                    return affectedServices;
                }

                /**
                 * Sets the value of the affectedServices property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices }
                 *     
                 */
                public void setAffectedServices(RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices value) {
                    this.affectedServices = value;
                }

                /**
                 * Gets the value of the reportedDateTime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getReportedDateTime() {
                    return reportedDateTime;
                }

                /**
                 * Sets the value of the reportedDateTime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setReportedDateTime(String value) {
                    this.reportedDateTime = value;
                }

                /**
                 * Gets the value of the category property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCategory() {
                    return category;
                }

                /**
                 * Sets the value of the category property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCategory(String value) {
                    this.category = value;
                }

                /**
                 * Gets the value of the subcategory property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSubcategory() {
                    return subcategory;
                }

                /**
                 * Sets the value of the subcategory property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSubcategory(String value) {
                    this.subcategory = value;
                }

                /**
                 * Gets the value of the caseManagerUserId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCaseManagerUserId() {
                    return caseManagerUserId;
                }

                /**
                 * Sets the value of the caseManagerUserId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCaseManagerUserId(String value) {
                    this.caseManagerUserId = value;
                }

                /**
                 * Gets the value of the caseManagerFullName property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCaseManagerFullName() {
                    return caseManagerFullName;
                }

                /**
                 * Sets the value of the caseManagerFullName property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCaseManagerFullName(String value) {
                    this.caseManagerFullName = value;
                }

                /**
                 * Gets the value of the mim property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMim() {
                    return mim;
                }

                /**
                 * Sets the value of the mim property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMim(String value) {
                    this.mim = value;
                }

                /**
                 * Gets the value of the customerImpacted property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCustomerImpacted() {
                    return customerImpacted;
                }

                /**
                 * Sets the value of the customerImpacted property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCustomerImpacted(String value) {
                    this.customerImpacted = value;
                }

                /**
                 * Gets the value of the hierarchicalEscalation property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHierarchicalEscalation() {
                    return hierarchicalEscalation;
                }

                /**
                 * Sets the value of the hierarchicalEscalation property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHierarchicalEscalation(String value) {
                    this.hierarchicalEscalation = value;
                }

                /**
                 * Gets the value of the serviceLevelTarget property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceLevelTarget() {
                    return serviceLevelTarget;
                }

                /**
                 * Sets the value of the serviceLevelTarget property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceLevelTarget(String value) {
                    this.serviceLevelTarget = value;
                }

                /**
                 * Gets the value of the resolutionDateTime property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResolutionDateTime() {
                    return resolutionDateTime;
                }

                /**
                 * Sets the value of the resolutionDateTime property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResolutionDateTime(String value) {
                    this.resolutionDateTime = value;
                }

                /**
                 * Gets the value of the rootProductId property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRootProductId() {
                    return rootProductId;
                }

                /**
                 * Sets the value of the rootProductId property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRootProductId(String value) {
                    this.rootProductId = value;
                }

                /**
                 * Gets the value of the closedAt property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getClosedAt() {
                    return closedAt;
                }

                /**
                 * Sets the value of the closedAt property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setClosedAt(String value) {
                    this.closedAt = value;
                }

                /**
                 * Gets the value of the resolutionCategory property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResolutionCategory() {
                    return resolutionCategory;
                }

                /**
                 * Sets the value of the resolutionCategory property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResolutionCategory(String value) {
                    this.resolutionCategory = value;
                }

                /**
                 * Gets the value of the resolutionSubCategory1 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResolutionSubCategory1() {
                    return resolutionSubCategory1;
                }

                /**
                 * Sets the value of the resolutionSubCategory1 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResolutionSubCategory1(String value) {
                    this.resolutionSubCategory1 = value;
                }

                /**
                 * Gets the value of the resolutionSubCategory2 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResolutionSubCategory2() {
                    return resolutionSubCategory2;
                }

                /**
                 * Sets the value of the resolutionSubCategory2 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResolutionSubCategory2(String value) {
                    this.resolutionSubCategory2 = value;
                }

                /**
                 * Gets the value of the resolutionSubCategory3 property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getResolutionSubCategory3() {
                    return resolutionSubCategory3;
                }

                /**
                 * Sets the value of the resolutionSubCategory3 property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setResolutionSubCategory3(String value) {
                    this.resolutionSubCategory3 = value;
                }

                /**
                 * Gets the value of the lastUpdated property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLastUpdated() {
                    return lastUpdated;
                }

                /**
                 * Sets the value of the lastUpdated property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLastUpdated(String value) {
                    this.lastUpdated = value;
                }

                /**
                 * Gets the value of the created property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCreated() {
                    return created;
                }

                /**
                 * Sets the value of the created property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCreated(String value) {
                    this.created = value;
                }

                /**
                 * Gets the value of the createdBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCreatedBy() {
                    return createdBy;
                }

                /**
                 * Sets the value of the createdBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCreatedBy(String value) {
                    this.createdBy = value;
                }

                /**
                 * Gets the value of the updatedBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUpdatedBy() {
                    return updatedBy;
                }

                /**
                 * Sets the value of the updatedBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUpdatedBy(String value) {
                    this.updatedBy = value;
                }

                /**
                 * Gets the value of the comments property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RetrieveIncsResponse.Response.Incidents.Incident.Comments }
                 *     
                 */
                public RetrieveIncsResponse.Response.Incidents.Incident.Comments getComments() {
                    return comments;
                }

                /**
                 * Sets the value of the comments property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RetrieveIncsResponse.Response.Incidents.Incident.Comments }
                 *     
                 */
                public void setComments(RetrieveIncsResponse.Response.Incidents.Incident.Comments value) {
                    this.comments = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="affected_resource" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "affectedResource"
                })
                public static class AffectedResources {

                    @XmlElement(name = "affected_resource", required = true)
                    protected List<RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource> affectedResource;

                    /**
                     * Gets the value of the affectedResource property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the affectedResource property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAffectedResource().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource }
                     * 
                     * 
                     */
                    public List<RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource> getAffectedResource() {
                        if (affectedResource == null) {
                            affectedResource = new ArrayList<RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource>();
                        }
                        return this.affectedResource;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ciId",
                        "ciName",
                        "siteId",
                        "cmdbClass",
                        "siteServiceTier"
                    })
                    public static class AffectedResource {

                        @XmlElement(name = "ci_id", required = true)
                        protected String ciId;
                        @XmlElement(name = "ci_name", required = true)
                        protected String ciName;
                        @XmlElement(name = "Site_Id", required = true)
                        protected String siteId;
                        @XmlElement(name = "cmdb_class", required = true)
                        protected String cmdbClass;
                        @XmlElement(name = "Site_Service_tier", required = true)
                        protected String siteServiceTier;

                        /**
                         * Gets the value of the ciId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiId() {
                            return ciId;
                        }

                        /**
                         * Sets the value of the ciId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiId(String value) {
                            this.ciId = value;
                        }

                        /**
                         * Gets the value of the ciName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiName() {
                            return ciName;
                        }

                        /**
                         * Sets the value of the ciName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiName(String value) {
                            this.ciName = value;
                        }

                        /**
                         * Gets the value of the siteId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteId() {
                            return siteId;
                        }

                        /**
                         * Sets the value of the siteId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteId(String value) {
                            this.siteId = value;
                        }

                        /**
                         * Gets the value of the cmdbClass property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCmdbClass() {
                            return cmdbClass;
                        }

                        /**
                         * Sets the value of the cmdbClass property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCmdbClass(String value) {
                            this.cmdbClass = value;
                        }

                        /**
                         * Gets the value of the siteServiceTier property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteServiceTier() {
                            return siteServiceTier;
                        }

                        /**
                         * Sets the value of the siteServiceTier property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteServiceTier(String value) {
                            this.siteServiceTier = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="affected_service" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "affectedService"
                })
                public static class AffectedServices {

                    @XmlElement(name = "affected_service", required = true)
                    protected List<RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService> affectedService;

                    /**
                     * Gets the value of the affectedService property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the affectedService property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getAffectedService().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService }
                     * 
                     * 
                     */
                    public List<RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService> getAffectedService() {
                        if (affectedService == null) {
                            affectedService = new ArrayList<RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService>();
                        }
                        return this.affectedService;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="ci_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="cmdb_class" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Service_tier" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "ciId",
                        "ciName",
                        "siteId",
                        "cmdbClass",
                        "siteServiceTier"
                    })
                    public static class AffectedService {

                        @XmlElement(name = "ci_id", required = true)
                        protected String ciId;
                        @XmlElement(name = "ci_name", required = true)
                        protected String ciName;
                        @XmlElement(name = "Site_Id", required = true)
                        protected String siteId;
                        @XmlElement(name = "cmdb_class", required = true)
                        protected String cmdbClass;
                        @XmlElement(name = "Site_Service_tier", required = true)
                        protected String siteServiceTier;

                        /**
                         * Gets the value of the ciId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiId() {
                            return ciId;
                        }

                        /**
                         * Sets the value of the ciId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiId(String value) {
                            this.ciId = value;
                        }

                        /**
                         * Gets the value of the ciName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCiName() {
                            return ciName;
                        }

                        /**
                         * Sets the value of the ciName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCiName(String value) {
                            this.ciName = value;
                        }

                        /**
                         * Gets the value of the siteId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteId() {
                            return siteId;
                        }

                        /**
                         * Sets the value of the siteId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteId(String value) {
                            this.siteId = value;
                        }

                        /**
                         * Gets the value of the cmdbClass property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCmdbClass() {
                            return cmdbClass;
                        }

                        /**
                         * Sets the value of the cmdbClass property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCmdbClass(String value) {
                            this.cmdbClass = value;
                        }

                        /**
                         * Gets the value of the siteServiceTier property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteServiceTier() {
                            return siteServiceTier;
                        }

                        /**
                         * Sets the value of the siteServiceTier property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteServiceTier(String value) {
                            this.siteServiceTier = value;
                        }

                    }

                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="comment" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "comment"
                })
                public static class Comments {

                    @XmlElement(required = true)
                    protected List<RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment> comment;

                    /**
                     * Gets the value of the comment property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the comment property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getComment().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment }
                     * 
                     * 
                     */
                    public List<RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment> getComment() {
                        if (comment == null) {
                            comment = new ArrayList<RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment>();
                        }
                        return this.comment;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "datetime",
                        "userid",
                        "note"
                    })
                    public static class Comment {

                        @XmlElement(required = true)
                        protected String datetime;
                        @XmlElement(required = true)
                        protected String userid;
                        @XmlElement(required = true)
                        protected String note;

                        /**
                         * Gets the value of the datetime property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getDatetime() {
                            return datetime;
                        }

                        /**
                         * Sets the value of the datetime property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setDatetime(String value) {
                            this.datetime = value;
                        }

                        /**
                         * Gets the value of the userid property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getUserid() {
                            return userid;
                        }

                        /**
                         * Sets the value of the userid property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setUserid(String value) {
                            this.userid = value;
                        }

                        /**
                         * Gets the value of the note property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getNote() {
                            return note;
                        }

                        /**
                         * Sets the value of the note property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setNote(String value) {
                            this.note = value;
                        }

                    }

                }

            }

        }

    }

}
