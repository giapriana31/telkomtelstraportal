
package com.tt.client.model.updateInc;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sys_class_name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_created_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_import_row" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="sys_import_set" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_import_state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_import_state_comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_mod_count" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="sys_row_error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_tags" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_target_sys_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_target_table" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_transform_map" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_updated_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sys_updated_on" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="template_import_log" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_customer_notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_user_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__use_view" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__encoded_query" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__limit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__first_row" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__last_row" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__order_by" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__order_by_desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="__exclude_columns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sysClassName",
    "sysCreatedBy",
    "sysCreatedOn",
    "sysId",
    "sysImportRow",
    "sysImportSet",
    "sysImportState",
    "sysImportStateComment",
    "sysModCount",
    "sysRowError",
    "sysTags",
    "sysTargetSysId",
    "sysTargetTable",
    "sysTransformMap",
    "sysUpdatedBy",
    "sysUpdatedOn",
    "templateImportLog",
    "uCustomerNotes",
    "uNumber",
    "uUserId",
    "useView",
    "encodedQuery",
    "limit",
    "firstRow",
    "lastRow",
    "orderBy",
    "orderByDesc",
    "excludeColumns"
})
@XmlRootElement(name = "getRecords")
public class GetRecords {

    @XmlElement(name = "sys_class_name")
    protected String sysClassName;
    @XmlElement(name = "sys_created_by")
    protected String sysCreatedBy;
    @XmlElement(name = "sys_created_on")
    protected String sysCreatedOn;
    @XmlElement(name = "sys_id")
    protected String sysId;
    @XmlElement(name = "sys_import_row")
    protected BigInteger sysImportRow;
    @XmlElement(name = "sys_import_set")
    protected String sysImportSet;
    @XmlElement(name = "sys_import_state")
    protected String sysImportState;
    @XmlElement(name = "sys_import_state_comment")
    protected String sysImportStateComment;
    @XmlElement(name = "sys_mod_count")
    protected BigInteger sysModCount;
    @XmlElement(name = "sys_row_error")
    protected String sysRowError;
    @XmlElement(name = "sys_tags")
    protected String sysTags;
    @XmlElement(name = "sys_target_sys_id")
    protected String sysTargetSysId;
    @XmlElement(name = "sys_target_table")
    protected String sysTargetTable;
    @XmlElement(name = "sys_transform_map")
    protected String sysTransformMap;
    @XmlElement(name = "sys_updated_by")
    protected String sysUpdatedBy;
    @XmlElement(name = "sys_updated_on")
    protected String sysUpdatedOn;
    @XmlElement(name = "template_import_log")
    protected String templateImportLog;
    @XmlElement(name = "u_customer_notes")
    protected String uCustomerNotes;
    @XmlElement(name = "u_number")
    protected String uNumber;
    @XmlElement(name = "u_user_id")
    protected String uUserId;
    @XmlElement(name = "__use_view")
    protected String useView;
    @XmlElement(name = "__encoded_query")
    protected String encodedQuery;
    @XmlElement(name = "__limit")
    protected String limit;
    @XmlElement(name = "__first_row")
    protected String firstRow;
    @XmlElement(name = "__last_row")
    protected String lastRow;
    @XmlElement(name = "__order_by")
    protected String orderBy;
    @XmlElement(name = "__order_by_desc")
    protected String orderByDesc;
    @XmlElement(name = "__exclude_columns")
    protected String excludeColumns;

    /**
     * Gets the value of the sysClassName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysClassName() {
        return sysClassName;
    }

    /**
     * Sets the value of the sysClassName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysClassName(String value) {
        this.sysClassName = value;
    }

    /**
     * Gets the value of the sysCreatedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysCreatedBy() {
        return sysCreatedBy;
    }

    /**
     * Sets the value of the sysCreatedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysCreatedBy(String value) {
        this.sysCreatedBy = value;
    }

    /**
     * Gets the value of the sysCreatedOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysCreatedOn() {
        return sysCreatedOn;
    }

    /**
     * Sets the value of the sysCreatedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysCreatedOn(String value) {
        this.sysCreatedOn = value;
    }

    /**
     * Gets the value of the sysId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysId() {
        return sysId;
    }

    /**
     * Sets the value of the sysId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysId(String value) {
        this.sysId = value;
    }

    /**
     * Gets the value of the sysImportRow property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSysImportRow() {
        return sysImportRow;
    }

    /**
     * Sets the value of the sysImportRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSysImportRow(BigInteger value) {
        this.sysImportRow = value;
    }

    /**
     * Gets the value of the sysImportSet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysImportSet() {
        return sysImportSet;
    }

    /**
     * Sets the value of the sysImportSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysImportSet(String value) {
        this.sysImportSet = value;
    }

    /**
     * Gets the value of the sysImportState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysImportState() {
        return sysImportState;
    }

    /**
     * Sets the value of the sysImportState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysImportState(String value) {
        this.sysImportState = value;
    }

    /**
     * Gets the value of the sysImportStateComment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysImportStateComment() {
        return sysImportStateComment;
    }

    /**
     * Sets the value of the sysImportStateComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysImportStateComment(String value) {
        this.sysImportStateComment = value;
    }

    /**
     * Gets the value of the sysModCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSysModCount() {
        return sysModCount;
    }

    /**
     * Sets the value of the sysModCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSysModCount(BigInteger value) {
        this.sysModCount = value;
    }

    /**
     * Gets the value of the sysRowError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysRowError() {
        return sysRowError;
    }

    /**
     * Sets the value of the sysRowError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysRowError(String value) {
        this.sysRowError = value;
    }

    /**
     * Gets the value of the sysTags property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysTags() {
        return sysTags;
    }

    /**
     * Sets the value of the sysTags property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysTags(String value) {
        this.sysTags = value;
    }

    /**
     * Gets the value of the sysTargetSysId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysTargetSysId() {
        return sysTargetSysId;
    }

    /**
     * Sets the value of the sysTargetSysId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysTargetSysId(String value) {
        this.sysTargetSysId = value;
    }

    /**
     * Gets the value of the sysTargetTable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysTargetTable() {
        return sysTargetTable;
    }

    /**
     * Sets the value of the sysTargetTable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysTargetTable(String value) {
        this.sysTargetTable = value;
    }

    /**
     * Gets the value of the sysTransformMap property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysTransformMap() {
        return sysTransformMap;
    }

    /**
     * Sets the value of the sysTransformMap property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysTransformMap(String value) {
        this.sysTransformMap = value;
    }

    /**
     * Gets the value of the sysUpdatedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysUpdatedBy() {
        return sysUpdatedBy;
    }

    /**
     * Sets the value of the sysUpdatedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysUpdatedBy(String value) {
        this.sysUpdatedBy = value;
    }

    /**
     * Gets the value of the sysUpdatedOn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysUpdatedOn() {
        return sysUpdatedOn;
    }

    /**
     * Sets the value of the sysUpdatedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysUpdatedOn(String value) {
        this.sysUpdatedOn = value;
    }

    /**
     * Gets the value of the templateImportLog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateImportLog() {
        return templateImportLog;
    }

    /**
     * Sets the value of the templateImportLog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateImportLog(String value) {
        this.templateImportLog = value;
    }

    /**
     * Gets the value of the uCustomerNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUCustomerNotes() {
        return uCustomerNotes;
    }

    /**
     * Sets the value of the uCustomerNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUCustomerNotes(String value) {
        this.uCustomerNotes = value;
    }

    /**
     * Gets the value of the uNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNumber() {
        return uNumber;
    }

    /**
     * Sets the value of the uNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNumber(String value) {
        this.uNumber = value;
    }

    /**
     * Gets the value of the uUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUserId() {
        return uUserId;
    }

    /**
     * Sets the value of the uUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUserId(String value) {
        this.uUserId = value;
    }

    /**
     * Gets the value of the useView property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseView() {
        return useView;
    }

    /**
     * Sets the value of the useView property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseView(String value) {
        this.useView = value;
    }

    /**
     * Gets the value of the encodedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncodedQuery() {
        return encodedQuery;
    }

    /**
     * Sets the value of the encodedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncodedQuery(String value) {
        this.encodedQuery = value;
    }

    /**
     * Gets the value of the limit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimit() {
        return limit;
    }

    /**
     * Sets the value of the limit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimit(String value) {
        this.limit = value;
    }

    /**
     * Gets the value of the firstRow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstRow() {
        return firstRow;
    }

    /**
     * Sets the value of the firstRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstRow(String value) {
        this.firstRow = value;
    }

    /**
     * Gets the value of the lastRow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastRow() {
        return lastRow;
    }

    /**
     * Sets the value of the lastRow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastRow(String value) {
        this.lastRow = value;
    }

    /**
     * Gets the value of the orderBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * Sets the value of the orderBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderBy(String value) {
        this.orderBy = value;
    }

    /**
     * Gets the value of the orderByDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderByDesc() {
        return orderByDesc;
    }

    /**
     * Sets the value of the orderByDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderByDesc(String value) {
        this.orderByDesc = value;
    }

    /**
     * Gets the value of the excludeColumns property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludeColumns() {
        return excludeColumns;
    }

    /**
     * Sets the value of the excludeColumns property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludeColumns(String value) {
        this.excludeColumns = value;
    }

}
