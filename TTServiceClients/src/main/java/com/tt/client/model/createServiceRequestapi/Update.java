
package com.tt.client.model.createServiceRequestapi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sys_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="template_import_log" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_affected_business_service_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_affected_device_ci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_assignment_group" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_category" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_customer_impacted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_customer_notes_portal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_external_customer_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_govt_lead_userid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_impact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_notes_description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_short_description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_site_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_subcategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_ticket_source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_ticket_type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_urgency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_user_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sysId",
    "templateImportLog",
    "uAffectedBusinessServiceId",
    "uAffectedDeviceCi",
    "uAssignmentGroup",
    "uCategory",
    "uCustomerImpacted",
    "uCustomerNotesPortal",
    "uExternalCustomerId",
    "uGovtLeadUserid",
    "uImpact",
    "uLanguage",
    "uNotesDescription",
    "uShortDescription",
    "uSiteId",
    "uSubcategory",
    "uTicketSource",
    "uTicketType",
    "uUrgency",
    "uUserId"
})
@XmlRootElement(name = "update")
public class Update {

    @XmlElement(name = "sys_id", required = true)
    protected String sysId;
    @XmlElement(name = "template_import_log")
    protected String templateImportLog;
    @XmlElement(name = "u_affected_business_service_id")
    protected String uAffectedBusinessServiceId;
    @XmlElement(name = "u_affected_device_ci")
    protected String uAffectedDeviceCi;
    @XmlElement(name = "u_assignment_group")
    protected String uAssignmentGroup;
    @XmlElement(name = "u_category")
    protected String uCategory;
    @XmlElement(name = "u_customer_impacted")
    protected String uCustomerImpacted;
    @XmlElement(name = "u_customer_notes_portal")
    protected String uCustomerNotesPortal;
    @XmlElement(name = "u_external_customer_id")
    protected String uExternalCustomerId;
    @XmlElement(name = "u_govt_lead_userid")
    protected String uGovtLeadUserid;
    @XmlElement(name = "u_impact")
    protected String uImpact;
    @XmlElement(name = "u_language")
    protected String uLanguage;
    @XmlElement(name = "u_notes_description")
    protected String uNotesDescription;
    @XmlElement(name = "u_short_description")
    protected String uShortDescription;
    @XmlElement(name = "u_site_id")
    protected String uSiteId;
    @XmlElement(name = "u_subcategory")
    protected String uSubcategory;
    @XmlElement(name = "u_ticket_source")
    protected String uTicketSource;
    @XmlElement(name = "u_ticket_type")
    protected String uTicketType;
    @XmlElement(name = "u_urgency")
    protected String uUrgency;
    @XmlElement(name = "u_user_id")
    protected String uUserId;

    /**
     * Gets the value of the sysId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysId() {
        return sysId;
    }

    /**
     * Sets the value of the sysId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysId(String value) {
        this.sysId = value;
    }

    /**
     * Gets the value of the templateImportLog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateImportLog() {
        return templateImportLog;
    }

    /**
     * Sets the value of the templateImportLog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateImportLog(String value) {
        this.templateImportLog = value;
    }

    /**
     * Gets the value of the uAffectedBusinessServiceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUAffectedBusinessServiceId() {
        return uAffectedBusinessServiceId;
    }

    /**
     * Sets the value of the uAffectedBusinessServiceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUAffectedBusinessServiceId(String value) {
        this.uAffectedBusinessServiceId = value;
    }

    /**
     * Gets the value of the uAffectedDeviceCi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUAffectedDeviceCi() {
        return uAffectedDeviceCi;
    }

    /**
     * Sets the value of the uAffectedDeviceCi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUAffectedDeviceCi(String value) {
        this.uAffectedDeviceCi = value;
    }

    /**
     * Gets the value of the uAssignmentGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUAssignmentGroup() {
        return uAssignmentGroup;
    }

    /**
     * Sets the value of the uAssignmentGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUAssignmentGroup(String value) {
        this.uAssignmentGroup = value;
    }

    /**
     * Gets the value of the uCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUCategory() {
        return uCategory;
    }

    /**
     * Sets the value of the uCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUCategory(String value) {
        this.uCategory = value;
    }

    /**
     * Gets the value of the uCustomerImpacted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUCustomerImpacted() {
        return uCustomerImpacted;
    }

    /**
     * Sets the value of the uCustomerImpacted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUCustomerImpacted(String value) {
        this.uCustomerImpacted = value;
    }

    /**
     * Gets the value of the uCustomerNotesPortal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUCustomerNotesPortal() {
        return uCustomerNotesPortal;
    }

    /**
     * Sets the value of the uCustomerNotesPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUCustomerNotesPortal(String value) {
        this.uCustomerNotesPortal = value;
    }

    /**
     * Gets the value of the uExternalCustomerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUExternalCustomerId() {
        return uExternalCustomerId;
    }

    /**
     * Sets the value of the uExternalCustomerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUExternalCustomerId(String value) {
        this.uExternalCustomerId = value;
    }

    /**
     * Gets the value of the uGovtLeadUserid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUGovtLeadUserid() {
        return uGovtLeadUserid;
    }

    /**
     * Sets the value of the uGovtLeadUserid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUGovtLeadUserid(String value) {
        this.uGovtLeadUserid = value;
    }

    /**
     * Gets the value of the uImpact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUImpact() {
        return uImpact;
    }

    /**
     * Sets the value of the uImpact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUImpact(String value) {
        this.uImpact = value;
    }

    /**
     * Gets the value of the uLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getULanguage() {
        return uLanguage;
    }

    /**
     * Sets the value of the uLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setULanguage(String value) {
        this.uLanguage = value;
    }

    /**
     * Gets the value of the uNotesDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNotesDescription() {
        return uNotesDescription;
    }

    /**
     * Sets the value of the uNotesDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNotesDescription(String value) {
        this.uNotesDescription = value;
    }

    /**
     * Gets the value of the uShortDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUShortDescription() {
        return uShortDescription;
    }

    /**
     * Sets the value of the uShortDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUShortDescription(String value) {
        this.uShortDescription = value;
    }

    /**
     * Gets the value of the uSiteId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSiteId() {
        return uSiteId;
    }

    /**
     * Sets the value of the uSiteId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSiteId(String value) {
        this.uSiteId = value;
    }

    /**
     * Gets the value of the uSubcategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSubcategory() {
        return uSubcategory;
    }

    /**
     * Sets the value of the uSubcategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSubcategory(String value) {
        this.uSubcategory = value;
    }

    /**
     * Gets the value of the uTicketSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUTicketSource() {
        return uTicketSource;
    }

    /**
     * Sets the value of the uTicketSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUTicketSource(String value) {
        this.uTicketSource = value;
    }

    /**
     * Gets the value of the uTicketType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUTicketType() {
        return uTicketType;
    }

    /**
     * Sets the value of the uTicketType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUTicketType(String value) {
        this.uTicketType = value;
    }

    /**
     * Gets the value of the uUrgency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUrgency() {
        return uUrgency;
    }

    /**
     * Sets the value of the uUrgency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUrgency(String value) {
        this.uUrgency = value;
    }

    /**
     * Gets the value of the uUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUserId() {
        return uUserId;
    }

    /**
     * Sets the value of the uUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUserId(String value) {
        this.uUserId = value;
    }

}
