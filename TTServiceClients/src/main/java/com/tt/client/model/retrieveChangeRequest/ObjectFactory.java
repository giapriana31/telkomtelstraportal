
package com.tt.client.model.retrieveChangeRequest;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tt.client.model.retrieveChangeRequest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tt.client.model.retrieveChangeRequest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrieveChanges }
     * 
     */
    public RetrieveChanges createRetrieveChanges() {
        return new RetrieveChanges();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.AffectedServices }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.AffectedServices createRetrieveChangesResponseResponseChangesChangeAffectedServices() {
        return new RetrieveChangesResponse.Response.Changes.Change.AffectedServices();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response }
     * 
     */
    public RetrieveChangesResponse.Response createRetrieveChangesResponseResponse() {
        return new RetrieveChangesResponse.Response();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource createRetrieveChangesResponseResponseChangesChangeAffectedResourcesAffectedResource() {
        return new RetrieveChangesResponse.Response.Changes.Change.AffectedResources.AffectedResource();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes }
     * 
     */
    public RetrieveChangesResponse.Response.Changes createRetrieveChangesResponseResponseChanges() {
        return new RetrieveChangesResponse.Response.Changes();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.Comments.Comment }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.Comments.Comment createRetrieveChangesResponseResponseChangesChangeCommentsComment() {
        return new RetrieveChangesResponse.Response.Changes.Change.Comments.Comment();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change createRetrieveChangesResponseResponseChangesChange() {
        return new RetrieveChangesResponse.Response.Changes.Change();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval createRetrieveChangesResponseResponseChangesChangeApprovalsApproval() {
        return new RetrieveChangesResponse.Response.Changes.Change.Approvals.Approval();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.Comments }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.Comments createRetrieveChangesResponseResponseChangesChangeComments() {
        return new RetrieveChangesResponse.Response.Changes.Change.Comments();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse }
     * 
     */
    public RetrieveChangesResponse createRetrieveChangesResponse() {
        return new RetrieveChangesResponse();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.Approvals }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.Approvals createRetrieveChangesResponseResponseChangesChangeApprovals() {
        return new RetrieveChangesResponse.Response.Changes.Change.Approvals();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.AffectedResources }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.AffectedResources createRetrieveChangesResponseResponseChangesChangeAffectedResources() {
        return new RetrieveChangesResponse.Response.Changes.Change.AffectedResources();
    }

    /**
     * Create an instance of {@link RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService }
     * 
     */
    public RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService createRetrieveChangesResponseResponseChangesChangeAffectedServicesAffectedService() {
        return new RetrieveChangesResponse.Response.Changes.Change.AffectedServices.AffectedService();
    }

}
