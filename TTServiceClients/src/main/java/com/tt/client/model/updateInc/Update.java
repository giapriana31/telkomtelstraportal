
package com.tt.client.model.updateInc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sys_id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="template_import_log" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_customer_notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_number" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="u_user_id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sysId",
    "templateImportLog",
    "uCustomerNotes",
    "uNumber",
    "uUserId"
})
@XmlRootElement(name = "update")
public class Update {

    @XmlElement(name = "sys_id", required = true)
    protected String sysId;
    @XmlElement(name = "template_import_log")
    protected String templateImportLog;
    @XmlElement(name = "u_customer_notes")
    protected String uCustomerNotes;
    @XmlElement(name = "u_number")
    protected String uNumber;
    @XmlElement(name = "u_user_id")
    protected String uUserId;

    /**
     * Gets the value of the sysId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSysId() {
        return sysId;
    }

    /**
     * Sets the value of the sysId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSysId(String value) {
        this.sysId = value;
    }

    /**
     * Gets the value of the templateImportLog property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateImportLog() {
        return templateImportLog;
    }

    /**
     * Sets the value of the templateImportLog property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateImportLog(String value) {
        this.templateImportLog = value;
    }

    /**
     * Gets the value of the uCustomerNotes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUCustomerNotes() {
        return uCustomerNotes;
    }

    /**
     * Sets the value of the uCustomerNotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUCustomerNotes(String value) {
        this.uCustomerNotes = value;
    }

    /**
     * Gets the value of the uNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNumber() {
        return uNumber;
    }

    /**
     * Sets the value of the uNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNumber(String value) {
        this.uNumber = value;
    }

    /**
     * Gets the value of the uUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUUserId() {
        return uUserId;
    }

    /**
     * Sets the value of the uUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUUserId(String value) {
        this.uUserId = value;
    }

}
