
package com.tt.client.model.viewincidentapi;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tt.client.model.viewincidentapi package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tt.client.model.viewincidentapi
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Incident.Comments }
     * 
     */
    public ExecuteResponse.Response.Incident.Comments createExecuteResponseResponseIncidentComments() {
        return new ExecuteResponse.Response.Incident.Comments();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Incident.AffectedServices }
     * 
     */
    public ExecuteResponse.Response.Incident.AffectedServices createExecuteResponseResponseIncidentAffectedServices() {
        return new ExecuteResponse.Response.Incident.AffectedServices();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Incident }
     * 
     */
    public ExecuteResponse.Response.Incident createExecuteResponseResponseIncident() {
        return new ExecuteResponse.Response.Incident();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response }
     * 
     */
    public ExecuteResponse.Response createExecuteResponseResponse() {
        return new ExecuteResponse.Response();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Incident.AffectedResources.AffectedResource }
     * 
     */
    public ExecuteResponse.Response.Incident.AffectedResources.AffectedResource createExecuteResponseResponseIncidentAffectedResourcesAffectedResource() {
        return new ExecuteResponse.Response.Incident.AffectedResources.AffectedResource();
    }

    /**
     * Create an instance of {@link ExecuteResponse }
     * 
     */
    public ExecuteResponse createExecuteResponse() {
        return new ExecuteResponse();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Incident.AffectedResources }
     * 
     */
    public ExecuteResponse.Response.Incident.AffectedResources createExecuteResponseResponseIncidentAffectedResources() {
        return new ExecuteResponse.Response.Incident.AffectedResources();
    }

    /**
     * Create an instance of {@link Execute }
     * 
     */
    public Execute createExecute() {
        return new Execute();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Incident.Comments.Comment }
     * 
     */
    public ExecuteResponse.Response.Incident.Comments.Comment createExecuteResponseResponseIncidentCommentsComment() {
        return new ExecuteResponse.Response.Incident.Comments.Comment();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Incident.AffectedServices.AffectedService }
     * 
     */
    public ExecuteResponse.Response.Incident.AffectedServices.AffectedService createExecuteResponseResponseIncidentAffectedServicesAffectedService() {
        return new ExecuteResponse.Response.Incident.AffectedServices.AffectedService();
    }

}
