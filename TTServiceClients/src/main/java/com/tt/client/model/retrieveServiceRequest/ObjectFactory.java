
package com.tt.client.model.retrieveServiceRequest;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tt.client.model.retrieveServiceRequest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tt.client.model.retrieveServiceRequest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment createExecuteResponseResponseServiceRequestsServicerequestRequestedItemsRequestedItemCommentsComment() {
        return new ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems createExecuteResponseResponseServiceRequestsServicerequestRequestedItems() {
        return new ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames createExecuteResponseResponseServiceRequestsServicerequestRequestedItemsRequestedItemDeviceNames() {
        return new ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem createExecuteResponseResponseServiceRequestsServicerequestRequestedItemsRequestedItem() {
        return new ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests createExecuteResponseResponseServiceRequests() {
        return new ExecuteResponse.Response.ServiceRequests();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments createExecuteResponseResponseServiceRequestsServicerequestRequestedItemsRequestedItemComments() {
        return new ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests.Servicerequest }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests.Servicerequest createExecuteResponseResponseServiceRequestsServicerequest() {
        return new ExecuteResponse.Response.ServiceRequests.Servicerequest();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response }
     * 
     */
    public ExecuteResponse.Response createExecuteResponseResponse() {
        return new ExecuteResponse.Response();
    }

    /**
     * Create an instance of {@link Execute }
     * 
     */
    public Execute createExecute() {
        return new Execute();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName }
     * 
     */
    public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName createExecuteResponseResponseServiceRequestsServicerequestRequestedItemsRequestedItemDeviceNamesDeviceName() {
        return new ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName();
    }

    /**
     * Create an instance of {@link ExecuteResponse }
     * 
     */
    public ExecuteResponse createExecuteResponse() {
        return new ExecuteResponse();
    }

}
