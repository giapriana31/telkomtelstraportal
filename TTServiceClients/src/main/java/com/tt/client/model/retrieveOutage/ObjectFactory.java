
package com.tt.client.model.retrieveOutage;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tt.client.model.retrieveOutage package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tt.client.model.retrieveOutage
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages.Outage.AffectedServices }
     * 
     */
    public ExecuteResponse.Response.Outages.Outage.AffectedServices createExecuteResponseResponseOutagesOutageAffectedServices() {
        return new ExecuteResponse.Response.Outages.Outage.AffectedServices();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService }
     * 
     */
    public ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService createExecuteResponseResponseOutagesOutageAffectedServicesAffectedService() {
        return new ExecuteResponse.Response.Outages.Outage.AffectedServices.AffectedService();
    }

    /**
     * Create an instance of {@link ExecuteResponse }
     * 
     */
    public ExecuteResponse createExecuteResponse() {
        return new ExecuteResponse();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response }
     * 
     */
    public ExecuteResponse.Response createExecuteResponseResponse() {
        return new ExecuteResponse.Response();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages.Outage.AffectedCustomers }
     * 
     */
    public ExecuteResponse.Response.Outages.Outage.AffectedCustomers createExecuteResponseResponseOutagesOutageAffectedCustomers() {
        return new ExecuteResponse.Response.Outages.Outage.AffectedCustomers();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages.Outage.AffectedCustomers.AffectedCustomer }
     * 
     */
    public ExecuteResponse.Response.Outages.Outage.AffectedCustomers.AffectedCustomer createExecuteResponseResponseOutagesOutageAffectedCustomersAffectedCustomer() {
        return new ExecuteResponse.Response.Outages.Outage.AffectedCustomers.AffectedCustomer();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages.Outage.AffectedDevices }
     * 
     */
    public ExecuteResponse.Response.Outages.Outage.AffectedDevices createExecuteResponseResponseOutagesOutageAffectedDevices() {
        return new ExecuteResponse.Response.Outages.Outage.AffectedDevices();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages.Outage.AffectedDevices.AffectedDevice }
     * 
     */
    public ExecuteResponse.Response.Outages.Outage.AffectedDevices.AffectedDevice createExecuteResponseResponseOutagesOutageAffectedDevicesAffectedDevice() {
        return new ExecuteResponse.Response.Outages.Outage.AffectedDevices.AffectedDevice();
    }

    /**
     * Create an instance of {@link Execute }
     * 
     */
    public Execute createExecute() {
        return new Execute();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages.Outage }
     * 
     */
    public ExecuteResponse.Response.Outages.Outage createExecuteResponseResponseOutagesOutage() {
        return new ExecuteResponse.Response.Outages.Outage();
    }

    /**
     * Create an instance of {@link ExecuteResponse.Response.Outages }
     * 
     */
    public ExecuteResponse.Response.Outages createExecuteResponseResponseOutages() {
        return new ExecuteResponse.Response.Outages();
    }

}
