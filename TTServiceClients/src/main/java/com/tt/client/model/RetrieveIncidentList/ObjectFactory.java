
package com.tt.client.model.RetrieveIncidentList;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.tt.client.model.RetrieveIncidentList package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tt.client.model.RetrieveIncidentList
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources createRetrieveIncsResponseResponseIncidentsIncidentAffectedResources() {
        return new RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents.Incident.Comments }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents.Incident.Comments createRetrieveIncsResponseResponseIncidentsIncidentComments() {
        return new RetrieveIncsResponse.Response.Incidents.Incident.Comments();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService createRetrieveIncsResponseResponseIncidentsIncidentAffectedServicesAffectedService() {
        return new RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices.AffectedService();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse }
     * 
     */
    public RetrieveIncsResponse createRetrieveIncsResponse() {
        return new RetrieveIncsResponse();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response }
     * 
     */
    public RetrieveIncsResponse.Response createRetrieveIncsResponseResponse() {
        return new RetrieveIncsResponse.Response();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents createRetrieveIncsResponseResponseIncidents() {
        return new RetrieveIncsResponse.Response.Incidents();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource createRetrieveIncsResponseResponseIncidentsIncidentAffectedResourcesAffectedResource() {
        return new RetrieveIncsResponse.Response.Incidents.Incident.AffectedResources.AffectedResource();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents.Incident }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents.Incident createRetrieveIncsResponseResponseIncidentsIncident() {
        return new RetrieveIncsResponse.Response.Incidents.Incident();
    }

    /**
     * Create an instance of {@link RetrieveIncs }
     * 
     */
    public RetrieveIncs createRetrieveIncs() {
        return new RetrieveIncs();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices createRetrieveIncsResponseResponseIncidentsIncidentAffectedServices() {
        return new RetrieveIncsResponse.Response.Incidents.Incident.AffectedServices();
    }

    /**
     * Create an instance of {@link RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment }
     * 
     */
    public RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment createRetrieveIncsResponseResponseIncidentsIncidentCommentsComment() {
        return new RetrieveIncsResponse.Response.Incidents.Incident.Comments.Comment();
    }

}
