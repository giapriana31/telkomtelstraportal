
package com.tt.client.model.retrieveServiceRequest;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.7-b01-
 * Generated source version: 2.1
 * 
 */
@WebService(name = "ServiceNowSoap", targetNamespace = "http://www.service-now.com/RetrieveSR")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ServiceNowSoap {


    /**
     * 
     * @param fromDate
     * @param toDate
     * @param customerID
     * @return
     *     returns com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response
     */
    @WebMethod(action = "http://www.service-now.com/RetrieveSR/execute")
    @WebResult(name = "response", targetNamespace = "http://www.service-now.com/RetrieveSR")
    @RequestWrapper(localName = "execute", targetNamespace = "http://www.service-now.com/RetrieveSR", className = "com.tt.client.model.retrieveServiceRequest.Execute")
    @ResponseWrapper(localName = "executeResponse", targetNamespace = "http://www.service-now.com/RetrieveSR", className = "com.tt.client.model.retrieveServiceRequest.ExecuteResponse")
    public com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response execute(
        @WebParam(name = "to_date", targetNamespace = "http://www.service-now.com/RetrieveSR")
        String toDate,
        @WebParam(name = "from_date", targetNamespace = "http://www.service-now.com/RetrieveSR")
        String fromDate,
        @WebParam(name = "customerID", targetNamespace = "http://www.service-now.com/RetrieveSR")
        String customerID);

}
