
package com.tt.client.model.retrieveServiceRequest;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ServiceRequests">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="servicerequest" maxOccurs="unbounded">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="Service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="project_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="project_expected_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="requestor_for" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="request_state" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="sys_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="Requested_items">
 *                                         &lt;complexType>
 *                                           &lt;complexContent>
 *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                               &lt;sequence>
 *                                                 &lt;element name="RequestedItem" maxOccurs="unbounded">
 *                                                   &lt;complexType>
 *                                                     &lt;complexContent>
 *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                         &lt;sequence>
 *                                                           &lt;element name="Requested_Item_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="RITM_short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="RITM_catalog_item_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Complexity_service_request" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Service_Request_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Service_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Site_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Requested_item_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="Requested_item_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                           &lt;element name="device_names">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="device_name" maxOccurs="unbounded">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;sequence>
 *                                                                               &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                             &lt;/sequence>
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                           &lt;element name="comments">
 *                                                             &lt;complexType>
 *                                                               &lt;complexContent>
 *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                   &lt;sequence>
 *                                                                     &lt;element name="comment" maxOccurs="unbounded">
 *                                                                       &lt;complexType>
 *                                                                         &lt;complexContent>
 *                                                                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                                                             &lt;sequence>
 *                                                                               &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                               &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                               &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                                                             &lt;/sequence>
 *                                                                           &lt;/restriction>
 *                                                                         &lt;/complexContent>
 *                                                                       &lt;/complexType>
 *                                                                     &lt;/element>
 *                                                                   &lt;/sequence>
 *                                                                 &lt;/restriction>
 *                                                               &lt;/complexContent>
 *                                                             &lt;/complexType>
 *                                                           &lt;/element>
 *                                                         &lt;/sequence>
 *                                                       &lt;/restriction>
 *                                                     &lt;/complexContent>
 *                                                   &lt;/complexType>
 *                                                 &lt;/element>
 *                                               &lt;/sequence>
 *                                             &lt;/restriction>
 *                                           &lt;/complexContent>
 *                                         &lt;/complexType>
 *                                       &lt;/element>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "response"
})
@XmlRootElement(name = "executeResponse")
public class ExecuteResponse {

    @XmlElement(required = true)
    protected ExecuteResponse.Response response;

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link ExecuteResponse.Response }
     *     
     */
    public ExecuteResponse.Response getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecuteResponse.Response }
     *     
     */
    public void setResponse(ExecuteResponse.Response value) {
        this.response = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ServiceRequests">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="servicerequest" maxOccurs="unbounded">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="Service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="project_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="project_expected_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="requestor_for" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="request_state" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="sys_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="Requested_items">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="RequestedItem" maxOccurs="unbounded">
     *                                         &lt;complexType>
     *                                           &lt;complexContent>
     *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                               &lt;sequence>
     *                                                 &lt;element name="Requested_Item_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="RITM_short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="RITM_catalog_item_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Complexity_service_request" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Service_Request_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Service_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Site_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Requested_item_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="Requested_item_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                 &lt;element name="device_names">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="device_name" maxOccurs="unbounded">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;sequence>
     *                                                                     &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                                   &lt;/sequence>
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                                 &lt;element name="comments">
     *                                                   &lt;complexType>
     *                                                     &lt;complexContent>
     *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                         &lt;sequence>
     *                                                           &lt;element name="comment" maxOccurs="unbounded">
     *                                                             &lt;complexType>
     *                                                               &lt;complexContent>
     *                                                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                                                   &lt;sequence>
     *                                                                     &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                                     &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                                     &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                                                   &lt;/sequence>
     *                                                                 &lt;/restriction>
     *                                                               &lt;/complexContent>
     *                                                             &lt;/complexType>
     *                                                           &lt;/element>
     *                                                         &lt;/sequence>
     *                                                       &lt;/restriction>
     *                                                     &lt;/complexContent>
     *                                                   &lt;/complexType>
     *                                                 &lt;/element>
     *                                               &lt;/sequence>
     *                                             &lt;/restriction>
     *                                           &lt;/complexContent>
     *                                         &lt;/complexType>
     *                                       &lt;/element>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="error_code" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="error_message" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "serviceRequests",
        "errorCode",
        "errorMessage"
    })
    public static class Response {

        @XmlElement(name = "ServiceRequests", required = true)
        protected ExecuteResponse.Response.ServiceRequests serviceRequests;
        @XmlElement(name = "error_code", required = true)
        protected String errorCode;
        @XmlElement(name = "error_message", required = true)
        protected String errorMessage;

        /**
         * Gets the value of the serviceRequests property.
         * 
         * @return
         *     possible object is
         *     {@link ExecuteResponse.Response.ServiceRequests }
         *     
         */
        public ExecuteResponse.Response.ServiceRequests getServiceRequests() {
            return serviceRequests;
        }

        /**
         * Sets the value of the serviceRequests property.
         * 
         * @param value
         *     allowed object is
         *     {@link ExecuteResponse.Response.ServiceRequests }
         *     
         */
        public void setServiceRequests(ExecuteResponse.Response.ServiceRequests value) {
            this.serviceRequests = value;
        }

        /**
         * Gets the value of the errorCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorCode() {
            return errorCode;
        }

        /**
         * Sets the value of the errorCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorCode(String value) {
            this.errorCode = value;
        }

        /**
         * Gets the value of the errorMessage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorMessage() {
            return errorMessage;
        }

        /**
         * Sets the value of the errorMessage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorMessage(String value) {
            this.errorMessage = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="servicerequest" maxOccurs="unbounded">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="Service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="project_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="project_expected_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="requestor_for" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="request_state" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="sys_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="Requested_items">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="RequestedItem" maxOccurs="unbounded">
         *                               &lt;complexType>
         *                                 &lt;complexContent>
         *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                     &lt;sequence>
         *                                       &lt;element name="Requested_Item_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="RITM_short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="RITM_catalog_item_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Complexity_service_request" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Service_Request_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Service_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Site_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Requested_item_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="Requested_item_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                       &lt;element name="device_names">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="device_name" maxOccurs="unbounded">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;sequence>
         *                                                           &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                         &lt;/sequence>
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                       &lt;element name="comments">
         *                                         &lt;complexType>
         *                                           &lt;complexContent>
         *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                               &lt;sequence>
         *                                                 &lt;element name="comment" maxOccurs="unbounded">
         *                                                   &lt;complexType>
         *                                                     &lt;complexContent>
         *                                                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                                                         &lt;sequence>
         *                                                           &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                           &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                           &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                                                         &lt;/sequence>
         *                                                       &lt;/restriction>
         *                                                     &lt;/complexContent>
         *                                                   &lt;/complexType>
         *                                                 &lt;/element>
         *                                               &lt;/sequence>
         *                                             &lt;/restriction>
         *                                           &lt;/complexContent>
         *                                         &lt;/complexType>
         *                                       &lt;/element>
         *                                     &lt;/sequence>
         *                                   &lt;/restriction>
         *                                 &lt;/complexContent>
         *                               &lt;/complexType>
         *                             &lt;/element>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "servicerequest"
        })
        public static class ServiceRequests {

            @XmlElement(required = true)
            protected List<ExecuteResponse.Response.ServiceRequests.Servicerequest> servicerequest;

            /**
             * Gets the value of the servicerequest property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the servicerequest property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getServicerequest().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ExecuteResponse.Response.ServiceRequests.Servicerequest }
             * 
             * 
             */
            public List<ExecuteResponse.Response.ServiceRequests.Servicerequest> getServicerequest() {
                if (servicerequest == null) {
                    servicerequest = new ArrayList<ExecuteResponse.Response.ServiceRequests.Servicerequest>();
                }
                return this.servicerequest;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="Service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="project_start_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="project_expected_end_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="requestor_for" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="request_state" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="sys_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="sys_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="Requested_items">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="RequestedItem" maxOccurs="unbounded">
             *                     &lt;complexType>
             *                       &lt;complexContent>
             *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                           &lt;sequence>
             *                             &lt;element name="Requested_Item_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="RITM_short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="RITM_catalog_item_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Complexity_service_request" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Service_Request_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Service_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Site_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Requested_item_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="Requested_item_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                             &lt;element name="device_names">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="device_name" maxOccurs="unbounded">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;sequence>
             *                                                 &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                               &lt;/sequence>
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                             &lt;element name="comments">
             *                               &lt;complexType>
             *                                 &lt;complexContent>
             *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                     &lt;sequence>
             *                                       &lt;element name="comment" maxOccurs="unbounded">
             *                                         &lt;complexType>
             *                                           &lt;complexContent>
             *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                                               &lt;sequence>
             *                                                 &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                                 &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                                 &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                                               &lt;/sequence>
             *                                             &lt;/restriction>
             *                                           &lt;/complexContent>
             *                                         &lt;/complexType>
             *                                       &lt;/element>
             *                                     &lt;/sequence>
             *                                   &lt;/restriction>
             *                                 &lt;/complexContent>
             *                               &lt;/complexType>
             *                             &lt;/element>
             *                           &lt;/sequence>
             *                         &lt;/restriction>
             *                       &lt;/complexContent>
             *                     &lt;/complexType>
             *                   &lt;/element>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "serviceRequestNumber",
                "shortDescription",
                "description",
                "projectStartDate",
                "projectExpectedEndDate",
                "requestorFor",
                "requestState",
                "sysCreatedOn",
                "sysUpdatedOn",
                "createdBy",
                "updatedBy",
                "requestedItems"
            })
            public static class Servicerequest {

                @XmlElement(name = "Service_request_number", required = true)
                protected String serviceRequestNumber;
                @XmlElement(name = "short_description", required = true)
                protected String shortDescription;
                @XmlElement(required = true)
                protected String description;
                @XmlElement(name = "project_start_date", required = true)
                protected String projectStartDate;
                @XmlElement(name = "project_expected_end_date", required = true)
                protected String projectExpectedEndDate;
                @XmlElement(name = "requestor_for", required = true)
                protected String requestorFor;
                @XmlElement(name = "request_state", required = true)
                protected String requestState;
                @XmlElement(name = "sys_created_on", required = true)
                protected String sysCreatedOn;
                @XmlElement(name = "sys_updated_on", required = true)
                protected String sysUpdatedOn;
                @XmlElement(name = "created_by", required = true)
                protected String createdBy;
                @XmlElement(name = "updated_by", required = true)
                protected String updatedBy;
                @XmlElement(name = "Requested_items", required = true)
                protected ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems requestedItems;

                /**
                 * Gets the value of the serviceRequestNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getServiceRequestNumber() {
                    return serviceRequestNumber;
                }

                /**
                 * Sets the value of the serviceRequestNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setServiceRequestNumber(String value) {
                    this.serviceRequestNumber = value;
                }

                /**
                 * Gets the value of the shortDescription property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getShortDescription() {
                    return shortDescription;
                }

                /**
                 * Sets the value of the shortDescription property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setShortDescription(String value) {
                    this.shortDescription = value;
                }

                /**
                 * Gets the value of the description property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescription() {
                    return description;
                }

                /**
                 * Sets the value of the description property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescription(String value) {
                    this.description = value;
                }

                /**
                 * Gets the value of the projectStartDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProjectStartDate() {
                    return projectStartDate;
                }

                /**
                 * Sets the value of the projectStartDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProjectStartDate(String value) {
                    this.projectStartDate = value;
                }

                /**
                 * Gets the value of the projectExpectedEndDate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getProjectExpectedEndDate() {
                    return projectExpectedEndDate;
                }

                /**
                 * Sets the value of the projectExpectedEndDate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setProjectExpectedEndDate(String value) {
                    this.projectExpectedEndDate = value;
                }

                /**
                 * Gets the value of the requestorFor property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequestorFor() {
                    return requestorFor;
                }

                /**
                 * Sets the value of the requestorFor property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequestorFor(String value) {
                    this.requestorFor = value;
                }

                /**
                 * Gets the value of the requestState property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRequestState() {
                    return requestState;
                }

                /**
                 * Sets the value of the requestState property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRequestState(String value) {
                    this.requestState = value;
                }

                /**
                 * Gets the value of the sysCreatedOn property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSysCreatedOn() {
                    return sysCreatedOn;
                }

                /**
                 * Sets the value of the sysCreatedOn property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSysCreatedOn(String value) {
                    this.sysCreatedOn = value;
                }

                /**
                 * Gets the value of the sysUpdatedOn property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSysUpdatedOn() {
                    return sysUpdatedOn;
                }

                /**
                 * Sets the value of the sysUpdatedOn property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSysUpdatedOn(String value) {
                    this.sysUpdatedOn = value;
                }

                /**
                 * Gets the value of the createdBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCreatedBy() {
                    return createdBy;
                }

                /**
                 * Sets the value of the createdBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCreatedBy(String value) {
                    this.createdBy = value;
                }

                /**
                 * Gets the value of the updatedBy property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUpdatedBy() {
                    return updatedBy;
                }

                /**
                 * Sets the value of the updatedBy property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUpdatedBy(String value) {
                    this.updatedBy = value;
                }

                /**
                 * Gets the value of the requestedItems property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems }
                 *     
                 */
                public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems getRequestedItems() {
                    return requestedItems;
                }

                /**
                 * Sets the value of the requestedItems property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems }
                 *     
                 */
                public void setRequestedItems(ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems value) {
                    this.requestedItems = value;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="RequestedItem" maxOccurs="unbounded">
                 *           &lt;complexType>
                 *             &lt;complexContent>
                 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                 &lt;sequence>
                 *                   &lt;element name="Requested_Item_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="RITM_short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="RITM_catalog_item_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Complexity_service_request" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Service_Request_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Service_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Site_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Requested_item_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="Requested_item_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                   &lt;element name="device_names">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="device_name" maxOccurs="unbounded">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                   &lt;element name="comments">
                 *                     &lt;complexType>
                 *                       &lt;complexContent>
                 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                           &lt;sequence>
                 *                             &lt;element name="comment" maxOccurs="unbounded">
                 *                               &lt;complexType>
                 *                                 &lt;complexContent>
                 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *                                     &lt;sequence>
                 *                                       &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                                       &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                                       &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *                                     &lt;/sequence>
                 *                                   &lt;/restriction>
                 *                                 &lt;/complexContent>
                 *                               &lt;/complexType>
                 *                             &lt;/element>
                 *                           &lt;/sequence>
                 *                         &lt;/restriction>
                 *                       &lt;/complexContent>
                 *                     &lt;/complexType>
                 *                   &lt;/element>
                 *                 &lt;/sequence>
                 *               &lt;/restriction>
                 *             &lt;/complexContent>
                 *           &lt;/complexType>
                 *         &lt;/element>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "requestedItem"
                })
                public static class RequestedItems {

                    @XmlElement(name = "RequestedItem", required = true)
                    protected List<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem> requestedItem;

                    /**
                     * Gets the value of the requestedItem property.
                     * 
                     * <p>
                     * This accessor method returns a reference to the live list,
                     * not a snapshot. Therefore any modification you make to the
                     * returned list will be present inside the JAXB object.
                     * This is why there is not a <CODE>set</CODE> method for the requestedItem property.
                     * 
                     * <p>
                     * For example, to add a new item, do as follows:
                     * <pre>
                     *    getRequestedItem().add(newItem);
                     * </pre>
                     * 
                     * 
                     * <p>
                     * Objects of the following type(s) are allowed in the list
                     * {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem }
                     * 
                     * 
                     */
                    public List<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem> getRequestedItem() {
                        if (requestedItem == null) {
                            requestedItem = new ArrayList<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem>();
                        }
                        return this.requestedItem;
                    }


                    /**
                     * <p>Java class for anonymous complex type.
                     * 
                     * <p>The following schema fragment specifies the expected content contained within this class.
                     * 
                     * <pre>
                     * &lt;complexType>
                     *   &lt;complexContent>
                     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *       &lt;sequence>
                     *         &lt;element name="Requested_Item_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="service_request_number" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="RITM_short_description" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="RITM_catalog_item_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Complexity_service_request" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Service_Request_Category" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Service_Id" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Site_Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Requested_item_created_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="Requested_item_updated_on" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="created_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="updated_by" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *         &lt;element name="device_names">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="device_name" maxOccurs="unbounded">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *         &lt;element name="comments">
                     *           &lt;complexType>
                     *             &lt;complexContent>
                     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                 &lt;sequence>
                     *                   &lt;element name="comment" maxOccurs="unbounded">
                     *                     &lt;complexType>
                     *                       &lt;complexContent>
                     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                     *                           &lt;sequence>
                     *                             &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                             &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                             &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                     *                           &lt;/sequence>
                     *                         &lt;/restriction>
                     *                       &lt;/complexContent>
                     *                     &lt;/complexType>
                     *                   &lt;/element>
                     *                 &lt;/sequence>
                     *               &lt;/restriction>
                     *             &lt;/complexContent>
                     *           &lt;/complexType>
                     *         &lt;/element>
                     *       &lt;/sequence>
                     *     &lt;/restriction>
                     *   &lt;/complexContent>
                     * &lt;/complexType>
                     * </pre>
                     * 
                     * 
                     */
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "", propOrder = {
                        "requestedItemNumber",
                        "serviceRequestNumber",
                        "state",
                        "ritmShortDescription",
                        "ritmCatalogItemName",
                        "complexityServiceRequest",
                        "serviceRequestCategory",
                        "serviceId",
                        "siteName",
                        "requestedItemCreatedOn",
                        "requestedItemUpdatedOn",
                        "createdBy",
                        "updatedBy",
                        "deviceNames",
                        "comments"
                    })
                    public static class RequestedItem {

                        @XmlElement(name = "Requested_Item_number", required = true)
                        protected String requestedItemNumber;
                        @XmlElement(name = "service_request_number", required = true)
                        protected String serviceRequestNumber;
                        @XmlElement(name = "State", required = true)
                        protected String state;
                        @XmlElement(name = "RITM_short_description", required = true)
                        protected String ritmShortDescription;
                        @XmlElement(name = "RITM_catalog_item_name", required = true)
                        protected String ritmCatalogItemName;
                        @XmlElement(name = "Complexity_service_request", required = true)
                        protected String complexityServiceRequest;
                        @XmlElement(name = "Service_Request_Category", required = true)
                        protected String serviceRequestCategory;
                        @XmlElement(name = "Service_Id", required = true)
                        protected String serviceId;
                        @XmlElement(name = "Site_Name", required = true)
                        protected String siteName;
                        @XmlElement(name = "Requested_item_created_on", required = true)
                        protected String requestedItemCreatedOn;
                        @XmlElement(name = "Requested_item_updated_on", required = true)
                        protected String requestedItemUpdatedOn;
                        @XmlElement(name = "created_by", required = true)
                        protected String createdBy;
                        @XmlElement(name = "updated_by", required = true)
                        protected String updatedBy;
                        @XmlElement(name = "device_names", required = true)
                        protected ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames deviceNames;
                        @XmlElement(required = true)
                        protected ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments comments;

                        /**
                         * Gets the value of the requestedItemNumber property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRequestedItemNumber() {
                            return requestedItemNumber;
                        }

                        /**
                         * Sets the value of the requestedItemNumber property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRequestedItemNumber(String value) {
                            this.requestedItemNumber = value;
                        }

                        /**
                         * Gets the value of the serviceRequestNumber property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getServiceRequestNumber() {
                            return serviceRequestNumber;
                        }

                        /**
                         * Sets the value of the serviceRequestNumber property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setServiceRequestNumber(String value) {
                            this.serviceRequestNumber = value;
                        }

                        /**
                         * Gets the value of the state property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getState() {
                            return state;
                        }

                        /**
                         * Sets the value of the state property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setState(String value) {
                            this.state = value;
                        }

                        /**
                         * Gets the value of the ritmShortDescription property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRITMShortDescription() {
                            return ritmShortDescription;
                        }

                        /**
                         * Sets the value of the ritmShortDescription property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRITMShortDescription(String value) {
                            this.ritmShortDescription = value;
                        }

                        /**
                         * Gets the value of the ritmCatalogItemName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRITMCatalogItemName() {
                            return ritmCatalogItemName;
                        }

                        /**
                         * Sets the value of the ritmCatalogItemName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRITMCatalogItemName(String value) {
                            this.ritmCatalogItemName = value;
                        }

                        /**
                         * Gets the value of the complexityServiceRequest property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getComplexityServiceRequest() {
                            return complexityServiceRequest;
                        }

                        /**
                         * Sets the value of the complexityServiceRequest property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setComplexityServiceRequest(String value) {
                            this.complexityServiceRequest = value;
                        }

                        /**
                         * Gets the value of the serviceRequestCategory property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getServiceRequestCategory() {
                            return serviceRequestCategory;
                        }

                        /**
                         * Sets the value of the serviceRequestCategory property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setServiceRequestCategory(String value) {
                            this.serviceRequestCategory = value;
                        }

                        /**
                         * Gets the value of the serviceId property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getServiceId() {
                            return serviceId;
                        }

                        /**
                         * Sets the value of the serviceId property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setServiceId(String value) {
                            this.serviceId = value;
                        }

                        /**
                         * Gets the value of the siteName property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getSiteName() {
                            return siteName;
                        }

                        /**
                         * Sets the value of the siteName property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setSiteName(String value) {
                            this.siteName = value;
                        }

                        /**
                         * Gets the value of the requestedItemCreatedOn property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRequestedItemCreatedOn() {
                            return requestedItemCreatedOn;
                        }

                        /**
                         * Sets the value of the requestedItemCreatedOn property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRequestedItemCreatedOn(String value) {
                            this.requestedItemCreatedOn = value;
                        }

                        /**
                         * Gets the value of the requestedItemUpdatedOn property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getRequestedItemUpdatedOn() {
                            return requestedItemUpdatedOn;
                        }

                        /**
                         * Sets the value of the requestedItemUpdatedOn property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setRequestedItemUpdatedOn(String value) {
                            this.requestedItemUpdatedOn = value;
                        }

                        /**
                         * Gets the value of the createdBy property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getCreatedBy() {
                            return createdBy;
                        }

                        /**
                         * Sets the value of the createdBy property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setCreatedBy(String value) {
                            this.createdBy = value;
                        }

                        /**
                         * Gets the value of the updatedBy property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getUpdatedBy() {
                            return updatedBy;
                        }

                        /**
                         * Sets the value of the updatedBy property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setUpdatedBy(String value) {
                            this.updatedBy = value;
                        }

                        /**
                         * Gets the value of the deviceNames property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames }
                         *     
                         */
                        public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames getDeviceNames() {
                            return deviceNames;
                        }

                        /**
                         * Sets the value of the deviceNames property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames }
                         *     
                         */
                        public void setDeviceNames(ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames value) {
                            this.deviceNames = value;
                        }

                        /**
                         * Gets the value of the comments property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments }
                         *     
                         */
                        public ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments getComments() {
                            return comments;
                        }

                        /**
                         * Sets the value of the comments property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments }
                         *     
                         */
                        public void setComments(ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments value) {
                            this.comments = value;
                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="comment" maxOccurs="unbounded">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *                   &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *                   &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "comment"
                        })
                        public static class Comments {

                            @XmlElement(required = true)
                            protected List<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment> comment;

                            /**
                             * Gets the value of the comment property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the comment property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getComment().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment }
                             * 
                             * 
                             */
                            public List<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment> getComment() {
                                if (comment == null) {
                                    comment = new ArrayList<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.Comments.Comment>();
                                }
                                return this.comment;
                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="datetime" type="{http://www.w3.org/2001/XMLSchema}string"/>
                             *         &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string"/>
                             *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "datetime",
                                "userid",
                                "note"
                            })
                            public static class Comment {

                                @XmlElement(required = true)
                                protected String datetime;
                                @XmlElement(required = true)
                                protected String userid;
                                @XmlElement(required = true)
                                protected String note;

                                /**
                                 * Gets the value of the datetime property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getDatetime() {
                                    return datetime;
                                }

                                /**
                                 * Sets the value of the datetime property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setDatetime(String value) {
                                    this.datetime = value;
                                }

                                /**
                                 * Gets the value of the userid property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getUserid() {
                                    return userid;
                                }

                                /**
                                 * Sets the value of the userid property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setUserid(String value) {
                                    this.userid = value;
                                }

                                /**
                                 * Gets the value of the note property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getNote() {
                                    return note;
                                }

                                /**
                                 * Sets the value of the note property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setNote(String value) {
                                    this.note = value;
                                }

                            }

                        }


                        /**
                         * <p>Java class for anonymous complex type.
                         * 
                         * <p>The following schema fragment specifies the expected content contained within this class.
                         * 
                         * <pre>
                         * &lt;complexType>
                         *   &lt;complexContent>
                         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *       &lt;sequence>
                         *         &lt;element name="device_name" maxOccurs="unbounded">
                         *           &lt;complexType>
                         *             &lt;complexContent>
                         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                         *                 &lt;sequence>
                         *                   &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                         *                 &lt;/sequence>
                         *               &lt;/restriction>
                         *             &lt;/complexContent>
                         *           &lt;/complexType>
                         *         &lt;/element>
                         *       &lt;/sequence>
                         *     &lt;/restriction>
                         *   &lt;/complexContent>
                         * &lt;/complexType>
                         * </pre>
                         * 
                         * 
                         */
                        @XmlAccessorType(XmlAccessType.FIELD)
                        @XmlType(name = "", propOrder = {
                            "deviceName"
                        })
                        public static class DeviceNames {

                            @XmlElement(name = "device_name", required = true)
                            protected List<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName> deviceName;

                            /**
                             * Gets the value of the deviceName property.
                             * 
                             * <p>
                             * This accessor method returns a reference to the live list,
                             * not a snapshot. Therefore any modification you make to the
                             * returned list will be present inside the JAXB object.
                             * This is why there is not a <CODE>set</CODE> method for the deviceName property.
                             * 
                             * <p>
                             * For example, to add a new item, do as follows:
                             * <pre>
                             *    getDeviceName().add(newItem);
                             * </pre>
                             * 
                             * 
                             * <p>
                             * Objects of the following type(s) are allowed in the list
                             * {@link ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName }
                             * 
                             * 
                             */
                            public List<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName> getDeviceName() {
                                if (deviceName == null) {
                                    deviceName = new ArrayList<ExecuteResponse.Response.ServiceRequests.Servicerequest.RequestedItems.RequestedItem.DeviceNames.DeviceName>();
                                }
                                return this.deviceName;
                            }


                            /**
                             * <p>Java class for anonymous complex type.
                             * 
                             * <p>The following schema fragment specifies the expected content contained within this class.
                             * 
                             * <pre>
                             * &lt;complexType>
                             *   &lt;complexContent>
                             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                             *       &lt;sequence>
                             *         &lt;element name="ci_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
                             *       &lt;/sequence>
                             *     &lt;/restriction>
                             *   &lt;/complexContent>
                             * &lt;/complexType>
                             * </pre>
                             * 
                             * 
                             */
                            @XmlAccessorType(XmlAccessType.FIELD)
                            @XmlType(name = "", propOrder = {
                                "ciName"
                            })
                            public static class DeviceName {

                                @XmlElement(name = "ci_name", required = true)
                                protected String ciName;

                                /**
                                 * Gets the value of the ciName property.
                                 * 
                                 * @return
                                 *     possible object is
                                 *     {@link String }
                                 *     
                                 */
                                public String getCiName() {
                                    return ciName;
                                }

                                /**
                                 * Sets the value of the ciName property.
                                 * 
                                 * @param value
                                 *     allowed object is
                                 *     {@link String }
                                 *     
                                 */
                                public void setCiName(String value) {
                                    this.ciName = value;
                                }

                            }

                        }

                    }

                }

            }

        }

    }

}
