package com.tt.client.service.incident;

import java.net.Authenticator;
import com.tt.utils.RefreshUtil;

import javax.xml.ws.Holder;

import com.tt.client.common.TTServiceClientConstant;
import com.tt.auth.MyWSAuthenticator;
import com.tt.client.model.createServiceRequestapi.InsertResponse;
import com.tt.client.model.createServiceRequestapi.ServiceNowUTelstraIncident;
import com.tt.model.Incident;
import com.tt.utils.ServiceUtils;

public class createServiceRequestWSCallService {

	public InsertResponse insertIncidentWebService(Incident incident) {
		
		InsertResponse insertResponse = new InsertResponse();

		MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
		Authenticator.setDefault(myauth);
		ServiceNowUTelstraIncident serviceNowUTelstraIncident = new ServiceNowUTelstraIncident();

		String uAffectedBusinessServiceId = incident.getServiceid();
		String uAssignmentGroup = incident.getResolvergroupname();
		String uCustomerNotesPortal = incident.getWorklognotes();
		String uImpact = incident.getImpact();
		String uLanguage = incident.getCustomerpreferedlanguage();
		String uShortDescription = incident.getSummary();
		String uSiteId = incident.getSiteid();
		String uTicketSource = incident.getTicketsource();
		String uTicketType = incident.getTickettype();
		String uUrgency = incident.getUrgency();
		String uUserId = incident.getUserid();
		String uCategory = incident.getCategory();
		String uSubCategory = incident.getSubcategory();
		String uNotes = incident.getNotes();
		Holder<String> sysId = new Holder<String>();
		Holder<String> table = new Holder<String>();
		Holder<String> displayName = new Holder<String>();
		Holder<String> displayValue = new Holder<String>();
		Holder<String> status = new Holder<String>();
		Holder<String> statusMessage = new Holder<String>();
		Holder<String> errorMessage = new Holder<String>();
		
		System.out.println("serID::"+uAffectedBusinessServiceId);
		System.out.println("uAssignmentGroup::"+uAssignmentGroup);
		
		System.out.println("uCustomerNotesPortal::"+uCustomerNotesPortal);
		System.out.println("uImpact::"+uImpact);
		System.out.println("uLanguage::"+uLanguage);
		System.out.println("uShortDescription::"+uShortDescription);
		System.out.println("uAssignmentGroup::"+uAssignmentGroup);
		System.out.println("uSiteId::"+uSiteId);
		System.out.println("uTicketSource::"+uTicketSource);
		System.out.println("uUrgency::"+uUrgency);
		
		System.out.println("uTicketType::"+uTicketType);
		System.out.println("uUserId::"+uUserId);
		System.out.println("uCategory::"+uCategory);
		System.out.println("uSubcategory::"+uSubCategory);
		System.out.println("uNotes::"+uNotes);
		
	/*	serviceNowUTelstraIncident.getServiceNowSoap().insert("", 
				uAffectedBusinessServiceId, "", uAssignmentGroup, "",
				"", uImpact, uLanguage, uCustomerNotesPortal,
				uShortDescription, uSiteId, "", uTicketSource, uTicketType,
				uUrgency, uUserId, sysId, table, displayName, displayValue,
				status, statusMessage, errorMessage);*/

		
		serviceNowUTelstraIncident.getServiceNowSoap().insert("", 
				uAffectedBusinessServiceId, "", uAssignmentGroup, uCategory,
				"", uCustomerNotesPortal,"",uUserId, 
				uImpact, uLanguage, uNotes,
				uShortDescription, uSiteId, uSubCategory, uTicketSource, uTicketType,
				uUrgency, uUserId, sysId, table, displayName, displayValue,
				status, statusMessage, errorMessage);
		System.out.println("response from SNOW::::: "+insertResponse);
		
		insertResponse.setDisplayName(displayName.value);
		insertResponse.setDisplayValue(displayValue.value);
		insertResponse.setErrorMessage(errorMessage.value);
		insertResponse.setStatus(status.value);
		insertResponse.setSysId(sysId.value);
		insertResponse.setTable(table.value);
		return insertResponse;
	}

	/*public static void main(String[] args) {
		RefreshUtil.setProxyObject();
		createServiceRequestWSCallService callService = new createServiceRequestWSCallService();
		
		Incident incident = new Incident();
		System.out.println("testing started...");
		//uNumber="INC0046858";
		//uUserId="abel.tuter";
		//uCustomerNotes="This is Portal team";
		incident.setUserid("abel.tuter");
		//incident.setIncidentid("INC0046858");
		incident.setCustomerinformation("This is the info for cust.");
		
		incident.setServiceid("MNS 45");
		incident.setResolvergroupname("Level 1 Group");
		 
		incident.setImpact("High");
		 incident.setCustomerpreferedlanguage("English");
		 incident.setSummary("Testing From Portal Java");
		
		incident.setTicketsource("Portal_Self_Service");
		 incident.setTickettype("Portal Incident Enquiry");
		incident.setUrgency("Low");
		 
		incident.setWorklognotes("This is the info for Long description ");
		InsertResponse insert= callService.insertIncidentWebService(incident);
		System.out.println("this is the end of call"+insert.getDisplayName()+"::"+insert.getDisplayValue()+""+insert.getErrorMessage());
		System.out.println("this is the end of call");

	}*/

}
