package com.tt.client.service.incident;

import java.net.Authenticator;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.tt.logging.Logger;
import com.tt.auth.MyWSAuthenticator;
import com.tt.client.model.viewincidentapi.*;
import com.tt.client.model.viewincidentapi.ExecuteResponse.Response.Incident.AffectedResources.AffectedResource;
import com.tt.client.model.viewincidentapi.ExecuteResponse.Response.Incident.AffectedServices.AffectedService;
import com.tt.client.model.viewincidentapi.ExecuteResponse.Response.Incident.Comments;
import com.tt.client.model.viewincidentapi.ExecuteResponse.Response.Incident.Comments.Comment;
import com.tt.constants.TTPortalAppErrors;
import com.tt.exception.TTPortalAppException;
import com.tt.model.Configuration;
import com.tt.model.Incident;
import com.tt.model.Notes;
import com.tt.utils.ServiceUtils;
import com.tt.utils.RefreshUtil;
import com.tt.client.common.TTServiceClientConstant;

public class viewIncidentWSCallService {
	
	private final static Logger log = Logger.getLogger(viewIncidentWSCallService.class);

	public Incident viewIncident(Incident incident) throws TTPortalAppException  {

		log.info("viewIncident started");
		com.tt.client.model.viewincidentapi.ExecuteResponse.Response incWSResponse = null;
		Incident incidentRes = new Incident();
		try{
		MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
		Authenticator.setDefault(myauth);

		log.debug("Before calling Webservice");
		ServiceNowViewIncident serviceNowViewIncident = new ServiceNowViewIncident();
		log.debug("Initialised the WS");
		

		
		incWSResponse = serviceNowViewIncident.getServiceNowSoap()
				.execute(incident.getIncidentid());
		
		com.tt.client.model.viewincidentapi.ExecuteResponse.Response.Incident incidentWSResponse= incWSResponse.getIncident();
		
		String errorCode = incWSResponse.getErrorCode();
		
		String errorMessage = incWSResponse.getErrorMessage();
		
		
		
		log.debug("Execute the VIEWIncidentWS");
		log.debug("response we get from Webservice.... "
				+ incidentWSResponse);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat jktDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone timeZoneJKT = TimeZone.getTimeZone("Asia/Jakarta");
        jktDateFormat.setTimeZone(timeZoneJKT);
        DateFormat gmtDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        TimeZone timeZoneGMT = TimeZone.getTimeZone("GMT");
        gmtDateFormat.setTimeZone(timeZoneGMT);

		String dateInString = "";
		log.debug("i am here 1");
		
		if (incidentWSResponse != null
				&& !incidentWSResponse.getIncidentId().trim().equals("")) {

			incidentRes.setCaller(incidentWSResponse.getCaller());
			incidentRes.setCasemanager(incidentWSResponse
					.getCaseManagerFullName());
			incidentRes.setCasemanager(incidentWSResponse
					.getCaseManagerUserId());
			incidentRes.setCategory(incidentWSResponse.getCategory());
			incidentRes.setCustomerimpacted(Boolean
					.parseBoolean(incidentWSResponse.getCustomerImpacted()));
			incidentRes.setCustomername(incidentWSResponse.getCustomerName());
			if(incidentWSResponse.getHierarchicalEscalation()!=null && incidentWSResponse.getHierarchicalEscalation().equalsIgnoreCase("true") ){
			incidentRes.setHierEscalationFlag(true);
			}
			else{
				incidentRes.setHierEscalationFlag(false);
			}
			incidentRes.setImpact(incidentWSResponse.getImpact());
			incidentRes.setIncidentid(incidentWSResponse.getIncidentId());

			String lastUpdatedDate = incidentWSResponse.getLastUpdated();
			log.debug("date:::" + dateInString);
			if (!lastUpdatedDate.isEmpty()){
				incidentRes.setLastupdated(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(lastUpdatedDate))));
			}
			incidentRes.setMIM(incidentWSResponse.getMim());
			incidentRes.setNotes(incidentWSResponse.getNotes());

			
			String openedAt = incidentWSResponse.getOpenedAt();
			log.debug("date:::" + dateInString);
			if (!openedAt.isEmpty()){
				incidentRes.setOpendatetime(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(openedAt))));
			}
			incidentRes.setPriority(new Long(incidentWSResponse.getPriority()));

			String reportDate = incidentWSResponse.getReportedDateTime();
			if (!reportDate.isEmpty()){
				incidentRes.setReporteddatetime(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(reportDate))));
			}
			log.debug("after formatter:: "+incidentRes.getReporteddatetime());

			String serviceLevelTarget = incidentWSResponse.getServiceLevelTarget();
			if (!serviceLevelTarget.isEmpty()) {
				incidentRes.setServiceleveltarget(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(serviceLevelTarget))));
			}

			incidentRes.setSummary(incidentWSResponse.getShortDescription());
			incidentRes.setSitename(incidentWSResponse.getSiteName());
			incidentRes.setSitezone(incidentWSResponse.getSiteZone());
			incidentRes.setSiteservicetier(incidentWSResponse.getSiteServiceTier());
			incidentRes.setSubcategory(incidentWSResponse.getSubcategory());
			incidentRes.setIncidentstatus(incidentWSResponse.getTicketStatus());
			incidentRes.setUrgency(incidentWSResponse.getUrgency());

			log.debug("i am here 2");
			List<Configuration> affectedServiceList = new ArrayList<Configuration>();
			List<Configuration> affectedResourceList = new ArrayList<Configuration>();
			for (AffectedResource affectedResource : incidentWSResponse
					.getAffectedResources().getAffectedResource()) {
				Configuration ciIncidentMapping = new Configuration();
				log.debug(affectedResource.getCiId());
				ciIncidentMapping.setCiid(affectedResource.getCiName());
				affectedResourceList.add(ciIncidentMapping);
			}

			for (AffectedService affectedService : incidentWSResponse
					.getAffectedServices().getAffectedService()) {
				Configuration ciIncidentMapping = new Configuration();
				log.debug(affectedService.getCiId());
				ciIncidentMapping.setCiid(affectedService.getCiName());
				affectedServiceList.add(ciIncidentMapping);
			}
			
			List<Comment> commentList=	incWSResponse.getIncident().getComments().getComment();
			
			List<Notes> notes = new ArrayList<Notes>();
			for (Comment comment : commentList ) {
				Notes note = new Notes();
				if(comment!=null && comment.getDatetime()!= null && !comment.getDatetime().isEmpty())
				note.setTime(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(comment.getDatetime()))));
				note.setNote(comment.getNote());
				note.setName(comment.getUserid());
				log.debug("userid which is deployed:::"+note.getName());
				log.debug("userid which is deployed:::"+note.getTime());
				log.debug("userid which is deployed:::"+note.getNote());
				notes.add(note);
			}
			if(incidentWSResponse.getClosedAt()!=null && !incidentWSResponse.getClosedAt().isEmpty())
				incident.setCloseddate(dateFormat.parse(jktDateFormat.format(gmtDateFormat.parse(incidentWSResponse.getClosedAt()))));
			
			incidentRes.setAffectedResource(affectedResourceList);
			incidentRes.setAffectedService(affectedServiceList);
			incidentRes.setComments(notes);
			incident.setRootProductId(incidentWSResponse.getRootProductId());
			incident.setResolutioncategory(incidentWSResponse.getResolutionCategory());
			incident.setResolutionSubCategory1(incidentWSResponse.getResolutionSubCategory1());
			incident.setResolutionSubCategory2(incidentWSResponse.getResolutionSubCategory2());
			incident.setResolutionSubCategory3(incidentWSResponse.getResolutionSubCategory3());
			
			
			log.debug("esc.."+ incidentWSResponse.getHierarchicalEscalation());

			log.debug("i am here 3");
		} else {
			log.debug("There is an error. Read the error Code");
		}
		}catch(Exception e){
			e.printStackTrace();
			throw new TTPortalAppException(e, "SS:1111", TTPortalAppErrors.getErrorMessageByCode("SS:1111"));
			
		}
		log.info("viewIncident end");
		return incidentRes;
	}

	public static void main(String[] args) {

		RefreshUtil.setProxyObject();

		viewIncidentWSCallService callService = new viewIncidentWSCallService();

		Incident incident = new Incident();
		log.debug("testing started...");
		incident.setIncidentid("INC0000001");//

		log.debug("i am here jst before call");

		Incident inci = null;
		try {
			inci = callService.viewIncident(incident);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (inci != null && !inci.getIncidentid().equals("")) {
			log.debug("this is the end of call" + inci.getIncidentid()
					+ "::");
		} else
			log.debug("ERROR!!!!");
	}

}
