package com.tt.client.service.incident;

import java.net.Authenticator;

import com.tt.auth.MyWSAuthenticator;
import com.tt.client.common.TTServiceClientConstant;
import com.tt.client.model.retrieveChangeRequest.ServiceNowRetrieveChange;
import com.tt.utils.RefreshUtil;
import com.tt.utils.ServiceUtils;

public class RetrieveCRWSCallService {

	/**
	 * This method return the Incidents object from the Response object from the web service
	 * @param toDate  start date-time of the period
	 * @param fromDate  end date-time of the period
	 * @param customerId unique identifier of the customer
	 * @return RetrieveIncsResponse.Incidents which contains the list of all
	 *         incidents
	 */
	public com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response getRetrieveCRListResponse(String toDate, String fromDate, String customerId)
	{
		MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
		Authenticator.setDefault(myauth);

		ServiceNowRetrieveChange serviceNowRetrieveChange = new ServiceNowRetrieveChange();
		return serviceNowRetrieveChange.getServiceNowSoap().retrieveChanges(toDate, fromDate, customerId);
	}

	public static void main(String[] args)
	{
		RefreshUtil.setProxyObject();

		RetrieveCRWSCallService callService = new RetrieveCRWSCallService();

		System.out.println("testing started...");
		System.out.println("i am here jst before call");
		String toDate = "2015-07-13 00:00:00";
		String fromDate = "2015-06-25 00:00:00";
		String customerId = "91919";
		com.tt.client.model.retrieveChangeRequest.RetrieveChangesResponse.Response crList = null;
		try
		{

			crList = callService.getRetrieveCRListResponse(toDate, fromDate, customerId);
			//System.out.println("responseList...." + inci.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("this is the end of call" + crList + "::");
		System.out.println("this is the end of call");

	}



}
