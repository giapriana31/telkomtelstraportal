package com.tt.client.service.incident;

import java.net.Authenticator;

import com.tt.utils.RefreshUtil;
import com.tt.auth.MyWSAuthenticator;
import com.tt.client.common.TTServiceClientConstant;
import com.tt.client.model.retrieveServiceRequest.ExecuteResponse.Response;
import com.tt.client.model.retrieveServiceRequest.ServiceNowRetrieveSR;
import com.tt.utils.ServiceUtils;

public class ServiceRequestWSCallService {

    public Response getRetrieveServiceRequestResponse(String toDate, String fromDate, String customerId) {

	MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
	Authenticator.setDefault(myauth);

	ServiceNowRetrieveSR serviceNowRetrieveSR = new ServiceNowRetrieveSR();
	return serviceNowRetrieveSR.getServiceNowSoap().execute(toDate, fromDate, customerId);
    }

    public static void main(String[] args) {
    	RefreshUtil.setProxyObject();

	ServiceRequestWSCallService callService = new ServiceRequestWSCallService();

	System.out.println("testing started...");
	System.out.println("i am here jst before call");
	String toDate = "2015-07-13 00:00:00";
	String fromDate = "2015-06-25 00:00:00";
	String customerId = "91919";
	Response inci = null;
	try {

	    inci = callService.getRetrieveServiceRequestResponse("2015-07-29 23:00:00", "2015-07-29 00:00:00", "91919");
	    System.out.println("responseList...."+inci.getServiceRequests().getServicerequest().get(0).getServiceRequestNumber());
	} catch (Exception e) {
	    e.printStackTrace();
	}

	
    }
}
