package com.tt.client.service.incident;

import java.net.Authenticator;

import com.tt.auth.MyWSAuthenticator;
import com.tt.client.common.TTServiceClientConstant;
import com.tt.client.model.retrieveChangeRequest.ServiceNowRetrieveChange;
import com.tt.client.model.retrieveOutage.ServiceNowRetrieveOutage;
//import com.tt.client.model.retrieveOutage.ServiceNowRetrieveOutage;
import com.tt.utils.RefreshUtil;
import com.tt.utils.ServiceUtils;

public class RetrieveOutageWSCallService 
{
	
	/**
	 * This method return the Outages object from the Response object from the web service
	 * @param toDate  start date-time of the period
	 * @param fromDate  end date-time of the period
	 * @param customerId unique identifier of the customer
	 * @return RetrieveIncsResponse.Incidents which contains the list of all
	 *         incidents
	 */
	public com.tt.client.model.retrieveOutage.ExecuteResponse.Response getRetrieveOutageListResponse(String customerId, String toDate, String fromDate)
	{
		MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
		Authenticator.setDefault(myauth);

		ServiceNowRetrieveOutage serviceNowRetrieveOutage = new ServiceNowRetrieveOutage();
		return serviceNowRetrieveOutage.getServiceNowSoap().execute(customerId, toDate, fromDate);
	}

	public static void main(String[] args)
	{
		RefreshUtil.setProxyObject();

		RetrieveOutageWSCallService retrieveOutageWSCallService = new RetrieveOutageWSCallService();

		System.out.println("testing started...");
		System.out.println("i am here jst before call");
		String toDate = "2015-12-31 00:00:00";
		String fromDate = "2015-01-01 00:00:00";
		String customerId = "555001";
		com.tt.client.model.retrieveOutage.ExecuteResponse.Response outageList = null;
		try
		{
			outageList = retrieveOutageWSCallService.getRetrieveOutageListResponse(customerId, toDate, fromDate);
			System.out.println("responseList...." + outageList.getOutages().getOutage().size());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("this is the end of call" + outageList + "::");
		System.out.println("this is the end of call");

	}
}
