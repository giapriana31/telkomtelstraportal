/*
 * *****************************************************************************************************************
 * 
 * *****************************************************************************************************************
 * 
 * 
 * 
 * *****************************************************************************************************************
 */

package com.tt.client.service.incident;

import java.net.Authenticator;
import com.tt.utils.RefreshUtil;

import com.tt.auth.MyWSAuthenticator;
import com.tt.client.common.TTServiceClientConstant;

import com.tt.client.model.RetrieveIncidentList.ServiceNowRetrieveIncService;
import com.tt.utils.ServiceUtils;

/**
 * This class calls the RetrieveIncidentList Web Service. This web service
 * returns the list of all incidents that have been created / modified between
 * the given time frame for a particular customer
 * 
 * @author Infosys
 * @since 1.0
 */
public class RetrieveIncidentListWSCallService
{

	/**
	 * This method return the Incidents object from the Response object from the web service
	 * @param toDate  start date-time of the period
	 * @param fromDate  end date-time of the period
	 * @param customerId unique identifier of the customer
	 * @return RetrieveIncsResponse.Incidents which contains the list of all
	 *         incidents
	 */
	public com.tt.client.model.RetrieveIncidentList.RetrieveIncsResponse.Response getRetrieveIncidentListResponse(String toDate, String fromDate, String customerId)
	{
		MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
		Authenticator.setDefault(myauth);

		ServiceNowRetrieveIncService serviceNowViewIncident = new ServiceNowRetrieveIncService();
		return serviceNowViewIncident.getServiceNowSoap().retrieveIncs(toDate, fromDate, customerId);
	}

	public static void main(String[] args)
	{
		RefreshUtil.setProxyObject();

		RetrieveIncidentListWSCallService callService = new RetrieveIncidentListWSCallService();

		System.out.println("testing started...");
		System.out.println("i am here jst before call");
		String toDate = "2015-07-13 00:00:00";
		String fromDate = "2015-06-25 00:00:00";
		String customerId = "91919";
		com.tt.client.model.RetrieveIncidentList.RetrieveIncsResponse.Response inci = null;
		try
		{

			inci = callService.getRetrieveIncidentListResponse(toDate, fromDate, customerId);
			//System.out.println("responseList...." + inci.toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		System.out.println("this is the end of call" + inci.getIncidents().getIncident().get(0).getIncidentId() + "::");
		System.out.println("this is the end of call");

	}

}
