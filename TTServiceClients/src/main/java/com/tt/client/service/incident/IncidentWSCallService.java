package com.tt.client.service.incident;

import java.net.Authenticator;

import javax.xml.ws.Holder;

import com.tt.utils.RefreshUtil;
import com.tt.auth.MyWSAuthenticator;
import com.tt.client.common.TTServiceClientConstant;
import com.tt.client.model.incidentapi.InsertResponse;
import com.tt.client.model.incidentapi.ServiceNowUTelstraIncident;
import com.tt.model.Incident;
import com.tt.utils.ServiceUtils;

public class IncidentWSCallService {

	public InsertResponse insertIncidentWebService(Incident incident) {
		
		InsertResponse insertResponse = new InsertResponse();

		MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
		Authenticator.setDefault(myauth);
		ServiceNowUTelstraIncident serviceNowUTelstraIncident = new ServiceNowUTelstraIncident();

		String productType = incident.getRootProductId();
		String uAffectedBusinessServiceId = "";
		String uAffectedBusinessDeviceId = "";
		
		if("PrivateCloud".equalsIgnoreCase(productType)){
			uAffectedBusinessServiceId = incident.getServiceid();
			uAffectedBusinessDeviceId = incident.getVirtualMachines();
		}else{
			uAffectedBusinessServiceId = incident.getServiceid();
		}
		
		
		String uAssignmentGroup = incident.getResolvergroupname();
		String uCustomerNotesPortal = incident.getWorklognotes();
		String uImpact = incident.getImpact();
		String uLanguage = incident.getCustomerpreferedlanguage();
		String uShortDescription = incident.getSummary();
		String uSiteId = incident.getSiteid();
		String uTicketSource = incident.getTicketsource();
		String uTicketType = incident.getTickettype();
		String uUrgency = incident.getUrgency();
		String uUserId = incident.getUserid();

		Holder<String> sysId = new Holder<String>();
		Holder<String> table = new Holder<String>();
		Holder<String> displayName = new Holder<String>();
		Holder<String> displayValue = new Holder<String>();
		Holder<String> status = new Holder<String>();
		Holder<String> statusMessage = new Holder<String>();
		Holder<String> errorMessage = new Holder<String>();
		
		System.out.println("serID::"+uAffectedBusinessServiceId);
		System.out.println("uAssignmentGroup::"+uAssignmentGroup);
		
		System.out.println("uCustomerNotesPortal::"+uCustomerNotesPortal);
		System.out.println("uImpact::"+uImpact);
		System.out.println("uLanguage::"+uLanguage);
		System.out.println("uShortDescription::"+uShortDescription);
		System.out.println("uAssignmentGroup::"+uAssignmentGroup);
		System.out.println("uSiteId::"+uSiteId);
		System.out.println("uTicketSource::"+uTicketSource);
		System.out.println("uUrgency::"+uUrgency);
		System.out.println("uAffectedBusinessDeviceId:: "+uAffectedBusinessDeviceId);
		System.out.println("uTicketType::"+uTicketType);
		System.out.println("uUserId::"+uUserId);
		
		serviceNowUTelstraIncident.getServiceNowSoap().insert("", 
				uAffectedBusinessServiceId, uAffectedBusinessDeviceId, uAssignmentGroup, "",
				"", uImpact, uLanguage, uCustomerNotesPortal,
				uShortDescription, uSiteId, "", uTicketSource, uTicketType,
				uUrgency, uUserId, sysId, table, displayName, displayValue,
				status, statusMessage, errorMessage);

		System.out.println("response from SNOW::::: "+insertResponse);
		
		insertResponse.setDisplayName(displayName.value);
		insertResponse.setDisplayValue(displayValue.value);
		insertResponse.setErrorMessage(errorMessage.value);
		insertResponse.setStatus(status.value);
		insertResponse.setSysId(sysId.value);
		insertResponse.setTable(table.value);
		return insertResponse;
	}

	public static void main(String[] args) {
		
		RefreshUtil.setProxyObject();
		IncidentWSCallService callService = new IncidentWSCallService();
		
		Incident incident = new Incident();
		System.out.println("testing started...");
		InsertResponse insert= callService.insertIncidentWebService(incident);
		System.out.println("this is the end of call"+insert.getDisplayName()+"::"+insert.getDisplayValue()+"");
		System.out.println("this is the end of call");

	}

}
