package com.tt.client.service.incident;

import java.net.Authenticator;
import com.tt.utils.RefreshUtil;

import javax.xml.ws.Holder;

import com.tt.auth.MyWSAuthenticator;
import com.tt.client.model.updateInc.InsertResponse;
import com.tt.client.model.updateInc.ServiceNowUUpdateIncidentPortal;
import com.tt.model.Incident;
import com.tt.utils.ServiceUtils;
import com.tt.client.common.TTServiceClientConstant;

public class UpdateIncidentWSCall {

	
	public InsertResponse updateIncident(
			Incident incident) {

		MyWSAuthenticator myauth = ServiceUtils.getMyAuthenticator(TTServiceClientConstant.WSDL_USER_NAME, TTServiceClientConstant.WSDL_PASSWORD);
		Authenticator.setDefault(myauth);

		InsertResponse insertResponse = new InsertResponse();
		Holder<String> sysId = new Holder<String>();
		Holder<String> table = new Holder<String>();
		Holder<String> displayName = new Holder<String>();
		Holder<String> displayValue = new Holder<String>();
		Holder<String> status = new Holder<String>();
		Holder<String> statusMessage = new Holder<String>();
		Holder<String> errorMessage = new Holder<String>();

		String uUserId = "";
		if (incident != null && incident.getUserid() != null) {
			uUserId = incident.getUserid().toString();
		}
		String uNumber = incident.getIncidentid();
		String uCustomerNotes = incident.getWorklognotes();
		//uNumber="INC0046858";
		//uUserId="abel.tuter";
		//uCustomerNotes="This is Portal team";
		System.out.println("uCustomerNotes--->>"+uCustomerNotes);
		System.out.println("uNumber--->>"+uNumber);
		System.out.println("uUserId--->>"+uUserId);
		ServiceNowUUpdateIncidentPortal serviceNowUUpdateIncidentPortal = new ServiceNowUUpdateIncidentPortal();
		serviceNowUUpdateIncidentPortal.getServiceNowSoap().insert("",
				uCustomerNotes, uNumber, uUserId, sysId, table, displayName,
				displayValue, status, statusMessage, errorMessage);
	/*	serviceNowUUpdateIncidentPortal.getServiceNowSoap().insert("",
				uCustomerNotes, uNumber, uUserId, sysId, table, displayName,
				displayValue, status, statusMessage, errorMessage);*/

		insertResponse.setDisplayName(displayName.value);
		insertResponse.setDisplayValue(displayValue.value);
		insertResponse.setErrorMessage(errorMessage.value);
		insertResponse.setStatus(status.value);
		insertResponse.setSysId(sysId.value);
		insertResponse.setTable(table.value);
		return insertResponse;

	}


	public static void main(String[] args) {
		RefreshUtil.setProxyObject();
		UpdateIncidentWSCall callService = new UpdateIncidentWSCall();
		
		Incident incident = new Incident();
		
		callService.updateIncident(incident);
		System.out.println("after the call ends");
	}


}
