<style>
.main-container {
	background-color: #f1f1f1;
}

.row-2-col-1 {
	/*margin-right: 15px;*/
	
}

.row-2-col-2 {
	/*margin-left: 10px;*/
	
}
/*.tt-attention{
                     height: 244px ! important;
       }*/
/*
       @media screen and (min-width: 1340px) {
              #column-2{
                     width: 48% !important;
              }
              
              #column-3{
                     width: 48% !important;
                     margin-left: 15px;   
                     float: right;
              }
       }
       
       
       @media screen and (min-width: 768px) and (max-width: 1340px) {
              .row-2-col-1{
                     width: 100% !important;
              }
              
              .row-2-col-2{
                     width: 100% !important;
                     margin-left: 0 !important;
                     margin-top: 20px !important;
              }
       }*/
       
/* @media screen and (min-width: 1269px) and (max-width: 1500px){
#column-6 {
      margin-top: -15%;
}
}

@media screen and (min-width: 1501px) and (max-width: 1700px)
{
#column-6 {
     margin-top: -13%;
}
}

@media screen and (min-width: 1701px) and (max-width: 2100px)
{
#column-6 {
     margin-top: -11%;
}

} */
       
       
@media screen and (min-width: 768px) and (max-width: 799px) {
	#column-2 {
		width: 100% !important;
		margin-left: -20px;
	}
	#column-3 {
		width: 100% !important;
		left: -5%;
		position: relative;
		margin-top: 4%;
	}
	#column-4 {
		width: 100% !important;
		left: -5%;
		position: relative;
		margin-top: 3%;
	}
	#column-5 {
		width: 100% !important;
		left: -5%;
		position: relative;
		margin-top: 0%;
	}
	#column-6 {
		width: 100% !important;
		left: -2%;
		position: relative;
		margin-top: 6%;
	}
	#column-7 {
		position: relative;
		right: 0%;
		width: 95%;
	}
}

@media screen and (min-width: 600px) and (max-width: 768px) {
	.row-2-col-1 {
		margin-bottom: 20px;
	}
	#column-3 {
		margin-top: 20px;
	}
	#column-4 {
		margin-top: 20px;
	}
	#column-5 {
		margin-top: 20px;
	}
	#column-6 {
		margin-top: 40px;
	}
	#column-8{
		margin-left: 20px;
		width: 95%;
	}
}

@media screen and (min-width: 320px) and (max-width: 599px) {
	.tt-attention {
		height: 244px;
		width: 300px;
		line-height: 200px;
		vertical-align: middle;
		text-align: center;
	}
	#column-3 {
		margin-top: 20px;
	}
	#column-4 {
		margin-top: 20px;
	}
	#column-6 {
		margin-top: 20px;
	}
	#column-5 {
		margin-top: 20px;
	}
}

@media screen and (min-width: 800px) and (max-width: 1268px) {
	#column-2 {
		width: 50% !important;
		margin-left: -22px !important;
	}
	#column-3 {
		width: 48% !important;
	}
	#column-4 {
		width: 49% !important;
		float: left;
		margin-left: -22px !important;
		margin-top: 2.5%;
	}
	#column-5 {
		width: 49% !important;
		float: right;
		margin-right: 11px !important; 
		margin-top: 20px;
	}
	#column-6 {
		width: 49% !important;
		float: left;
		margin-left: 0px !important;
		margin-top: 4%;
	}
	#column-7 {
		float: right;
		width: 47% !important;
		margin-top: -135px;
	}
}

@media screen and (min-width: 1269px) {
	#column-2 {
		width: 27% !important;
		margin-left: -25px;
	}
	#column-3 {
		width: 27% !important;
	}
	#column-4 {
		width: 40% !important;
	}
	#column-5 {
		width: 40% !important;
		float: right;
		margin-right: 29px !important;
		margin-top: 18px;
	}
	#column-6 {
		width: 57% !important;
		float: left;
		margin-top: -16%;
		margin-left: 0px !important;
	}
	#column-7 {
		width: 40% !important;
		float: right !important;
		margin-right: 7px !important;
	}
}

@media screen and (min-width: 1200px) {
	#column-8 {
		width: 50% !important;
		margin-left: -10px;
	}
}	

</style>




<div class="columns_1_3_2_1_3" id="main-header" role="main">
                <div class="portlet-layout row-fluid">
                                <div class="portlet-column portlet-column-only span12"
                                                id="column-1-header">
                                                $processor.processColumn("column-1-header","portlet-column-content
                                                portlet-column-content-only")</div>
                </div>
</div>
<div class="columns_1_3_2_1 ml-20 mr-20 mt-20" id="main-content"
                role="main">
                <div class="portlet-layout row-fluid">
                                <div class="portlet-column portlet-column-only span12" id="column-1">
                                                $processor.processColumn("column-1","portlet-column-content
                                                portlet-column-content-only")</div>
                </div>
                <div class="portlet-layout row-fluid mt-20 span12">


                                <div class="portlet-column span2" id="column-2">
                                                $processor.processColumn("column-2","portlet-column-content
                                                portlet-column-content-first")</div>
                                <div class="portlet-column span2" id="column-3">
                                                $processor.processColumn("column-3", "portlet-column-content")</div>

                                <div class="portlet-column span2 " id="column-4">
                                                $processor.processColumn("column-4","portlet-column-content ")</div>

                                <div class="portlet-column span2 " id="column-5">
                                                $processor.processColumn( "column-5", "portlet-column-content ")</div>
                                <div class="portlet-column portlet-column-last span2" id="column-6">
                                                $processor.processColumn("column-6", "portlet-column-content
                                                portlet-column-content-last")</div>

                </div>
                <div class="portlet-column portlet-column-only span12" id="column-7">
                                $processor.processColumn("column-7","portlet-column-content
                                portlet-column-content-only")</div>
                <div class="portlet-layout row-fluid mt-20 span12">
                	<div class="portlet-column span8" id="column-8">
                                                $processor.processColumn("column-8","portlet-column-content
                                                portlet-column-content-first")</div>
	                <!-- <div class="portlet-column span4" id="column-9">
	                                               $processor.processColumn("column-9", "portlet-column-content")</div> 
	                <div class="portlet-column span4" id="column-10">
	                                               $processor.processColumn("column-10", "portlet-column-content")</div> -->
                </div>                


</div>