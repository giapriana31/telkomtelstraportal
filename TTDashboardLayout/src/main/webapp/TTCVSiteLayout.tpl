<style>
.main-container {
	background-color: #f1f1f1;
}

.row-2-col-1 {
	/*margin-right: 15px;*/
	
}

.row-2-col-2 {
	/*margin-left: 10px;*/
	
}
/*.tt-attention{
                     height: 244px ! important;
       }*/
/*
       @media screen and (min-width: 1340px) {
              #column-2{
                     width: 48% !important;
              }
              
              #column-3{
                     width: 48% !important;
                     margin-left: 15px;   
                     float: right;
              }
       }
       
       
       @media screen and (min-width: 768px) and (max-width: 1340px) {
              .row-2-col-1{
                     width: 100% !important;
              }
              
              .row-2-col-2{
                     width: 100% !important;
                     margin-left: 0 !important;
                     margin-top: 20px !important;
              }
       }*/
       
/* @media screen and (min-width: 1269px) and (max-width: 1500px){
#column-6 {
      margin-top: -15%;
}
}

@media screen and (min-width: 1501px) and (max-width: 1700px)
{
#column-6 {
     margin-top: -13%;
}
}

@media screen and (min-width: 1701px) and (max-width: 2100px)
{
#column-6 {
     margin-top: -11%;
}

} */
       
       
@media screen and (min-width: 768px) and (max-width: 799px) {
	#column-2 {
		width: 100% !important;
		margin-left: -20px;
	}
	#column-3 {
		width: 100% !important;
		left: -5%;
		position: relative;
		margin-top: 4%;
	}
	#column-4 {
		width: 100% !important;
		left: -5%;
		position: relative;
		margin-top: 3%;
	}
	#column-5 {
		width: 100% !important;
		left: -5%;
		position: relative;
		margin-top: 0%;
	}
	#column-6 {
		width: 100% !important;
		left: -2%;
		position: relative;
		margin-top: 6%;
	}
	#column-7 {
		position: relative;
		right: 0%;
		width: 95%;
	}
}

@media screen and (min-width: 600px) and (max-width: 768px) {
	.row-2-col-1 {
		margin-bottom: 20px;
	}
	#column-3 {
		margin-top: 20px;
	}
	#column-4 {
		margin-top: 20px;
	}
	#column-5 {
		margin-top: 20px;
	}
	#column-6 {
		margin-top: 40px;
	}
	#column-8{
		margin-left: 20px;
		width: 95%;
	}
}

@media screen and (min-width: 320px) and (max-width: 599px) {
	.tt-attention {
		height: 244px;
		width: 300px;
		line-height: 200px;
		vertical-align: middle;
		text-align: center;
	}
	#column-3 {
		margin-top: 20px;
	}
	#column-4 {
		margin-top: 20px;
	}
	#column-6 {
		margin-top: 20px;
	}
	#column-5 {
		margin-top: 20px;
	}
}

@media screen and (min-width: 800px) and (max-width: 1268px) {
	#column-2 {
		width: 50% !important;
		margin-left: -22px !important;
	}
	#column-3 {
		width: 48% !important;
	}
	#column-4 {
		width: 49% !important;
		float: left;
		margin-left: -22px !important;
		margin-top: 2.5%;
	}
	#column-5 {
		width: 49% !important;
		float: right;
		margin-right: 11px !important; 
		margin-top: 20px;
	}
	#column-6 {
		width: 49% !important;
		float: left;
		margin-left: 0px !important;
		margin-top: 4%;
	}
	#column-7 {
		float: right;
		width: 47% !important;
		margin-top: -135px;
	}
	#column-11 {
		width: 47.71795%;
    	margin-left: 21px;
	}
	
}

@media screen and (min-width: 1269px) {
	#column-2 {
		width: 30% !important;
		margin-left: 29px;
	}
	#column-3 {
		width: 30% !important;
	}
	#column-4 {
		width: 30% !important;
	}
	#column-5 {
		width: 46% !important;
		float: left;
		margin-left: 0px !important;
		margin-top: 0px;
	}
	#column-6 {
		width: 46% !important;
	}
	#column-7 {
		width: 44.5% !important;
		float: left !important;
		margin-left: 18px !important;
	}
	#column-8 {
		width: 46% !important;
	}
	
	#column-9 {
		width: 44.5% !important;
		float: left !important;
		margin-left: 18px !important;
	}	
		
}

@media screen and (max-width: 1700px) and (min-width: 1501px){
	#column-6 {
	    margin-left: 3% !important;
		width: 46% !important;
		margin-top: 0px;
	}
}

</style>




<!--START SITE PAGE CONTAINER-->
<div class="container-fluid tt-page-detail">
	<div class="row-fluid tt-page-breadcrumb">
		<div class="span6">
			<div>
				<span class="tt-img-dashboard tt-main-page-name"><liferay-ui:message key="Dashboard"/>&nbsp;&gt;&nbsp;</span>
				<span class="tt-current-page"><liferay-ui:message key="Site Information"/></span>
			</div>
		</div>
		<div class="span6 tt-back"><button type="button" onclick="window.location='<%=prop.getProperty("tt.homeURL")%>/<%=prop.getProperty("ttDashboardPageName")%>'" class="btn-primary-tt backButton">&nbsp;&lt;&nbsp;<liferay-ui:message key="Back"/></button>
		</div>
	</div>
	<div class="row-fluid tt-page-content">
		<ul id="tabc" class="nav nav-pills tt-site-ul">
			<li class="active tt-site-nav-pills selected" id="basedOnTicket-li">
				<a data-toggle="tab" href="#basedOnTicketTab" class="tt-site-nav-pills" id="basedOnTicket" onclick="basedOnTicket()"><liferay-ui:message key="Based on Incidents"/></a>
			</li>
			<li class="tt-site-nav-pills" id="basedOnSLATab-li">
				<a data-toggle="tab" href="#basedOnSLATab" class="tt-site-nav-pills" id="basedOnSLA" onclick="basedOnSLA()"><liferay-ui:message key="Based on Availability"/></a>
			</li>
		</ul>
	</div>
</div>

<!--START DISPLAY SITES CONTAINER-->
<div class="container-fluid tt-site-display">
	<div class="row-fluid">
		<div class="span12">
			<div class="span2 tt-displaySite-title"><liferay-ui:message key="Display Sites:"/></div>
			<div class="span6">
				<div class="row-fluid tt-displaySite-check">
					<div class="span2"><input type="checkbox" id="checkbox_all" class="tt-displaysite-position" checked/><liferay-ui:message key=" All"/></div>
					<div class="span3"><input type="checkbox" id="checkbox_high" class="tt-displaysite-position" /><liferay-ui:message key=" High Impact"/></div>
					<div class="span4"><input type="checkbox" id="checkbox_moderate" class="tt-displaysite-position" /><liferay-ui:message key=" Moderate Impact"/></div>
					<div class="span3"><input type="checkbox" id="checkbox_normal" class="tt-displaysite-position" /><liferay-ui:message key=" Normal"/></div>
				</div>
			</div>
		</div>
	</div>
</div>