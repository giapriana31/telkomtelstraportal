package com.tt.notification;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.notifications.BaseUserNotificationHandler;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.UserNotificationEvent;
import com.liferay.portal.service.ServiceContext;

public class DockBarUserNotificationHandler extends BaseUserNotificationHandler 
{
	public static final String PORTLET_ID = "TTNotificationService_WAR_TTNotificationServiceportlet";
	public String url = null;

	public DockBarUserNotificationHandler() 
	{
		setPortletId(DockBarUserNotificationHandler.PORTLET_ID);
	}
	
	@Override
	protected String getBody(UserNotificationEvent userNotificationEvent,ServiceContext serviceContext) throws Exception 
	{
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject(userNotificationEvent.getPayload());
		
		String title = "<strong>"+jsonObject.getString("title")+"</strong>";
		String notificationText = jsonObject.getString("description");
		url = jsonObject.getString("url");
		
		String body = StringUtil.replace(getBodyTemplate(), new String[] {"[$TITLE$]", "[$BODY_TEXT$]" },new String[] { title, notificationText });
		return body;
	}
	
	protected String getBodyTemplate() throws Exception 
	{		
		StringBundler sb = new StringBundler(5);
		if(url==null || url.trim().equals(""))
		{
			sb.append("<div class=\"title\">[$TITLE$]</div><div ");
			sb.append("class=\"body\">[$BODY_TEXT$]</div>");
		}
		else
		{
			sb.append("<div onclick=\"window.open('"+url+"', '_blank');\"><div class=\"title\">[$TITLE$]</div><div ");
			sb.append("class=\"body\">[$BODY_TEXT$]</div></div>");
			
		}
		return sb.toString();
	}
}