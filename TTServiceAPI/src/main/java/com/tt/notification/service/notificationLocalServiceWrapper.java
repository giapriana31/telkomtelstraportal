package com.tt.notification.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link notificationLocalService}.
 *
 * @author Prashant Patel
 * @see notificationLocalService
 * @generated
 */
public class notificationLocalServiceWrapper implements notificationLocalService,
    ServiceWrapper<notificationLocalService> {
    private notificationLocalService _notificationLocalService;

    public notificationLocalServiceWrapper(
        notificationLocalService notificationLocalService) {
        _notificationLocalService = notificationLocalService;
    }

    /**
    * Adds the notification to the database. Also notifies the appropriate model listeners.
    *
    * @param notification the notification
    * @return the notification that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.tt.notification.model.notification addnotification(
        com.tt.notification.model.notification notification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.addnotification(notification);
    }

    /**
    * Creates a new notification with the primary key. Does not add the notification to the database.
    *
    * @param notificationID the primary key for the new notification
    * @return the new notification
    */
    @Override
    public com.tt.notification.model.notification createnotification(
        long notificationID) {
        return _notificationLocalService.createnotification(notificationID);
    }

    /**
    * Deletes the notification with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param notificationID the primary key of the notification
    * @return the notification that was removed
    * @throws PortalException if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.tt.notification.model.notification deletenotification(
        long notificationID)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.deletenotification(notificationID);
    }

    /**
    * Deletes the notification from the database. Also notifies the appropriate model listeners.
    *
    * @param notification the notification
    * @return the notification that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.tt.notification.model.notification deletenotification(
        com.tt.notification.model.notification notification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.deletenotification(notification);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _notificationLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public com.tt.notification.model.notification fetchnotification(
        long notificationID)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.fetchnotification(notificationID);
    }

    /**
    * Returns the notification with the primary key.
    *
    * @param notificationID the primary key of the notification
    * @return the notification
    * @throws PortalException if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.tt.notification.model.notification getnotification(
        long notificationID)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.getnotification(notificationID);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the notifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of notifications
    * @param end the upper bound of the range of notifications (not inclusive)
    * @return the range of notifications
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<com.tt.notification.model.notification> getnotifications(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.getnotifications(start, end);
    }

    /**
    * Returns the number of notifications.
    *
    * @return the number of notifications
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getnotificationsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.getnotificationsCount();
    }

    /**
    * Updates the notification in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param notification the notification
    * @return the notification that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public com.tt.notification.model.notification updatenotification(
        com.tt.notification.model.notification notification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _notificationLocalService.updatenotification(notification);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _notificationLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _notificationLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _notificationLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public notificationLocalService getWrappednotificationLocalService() {
        return _notificationLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappednotificationLocalService(
        notificationLocalService notificationLocalService) {
        _notificationLocalService = notificationLocalService;
    }

    @Override
    public notificationLocalService getWrappedService() {
        return _notificationLocalService;
    }

    @Override
    public void setWrappedService(
        notificationLocalService notificationLocalService) {
        _notificationLocalService = notificationLocalService;
    }
}
