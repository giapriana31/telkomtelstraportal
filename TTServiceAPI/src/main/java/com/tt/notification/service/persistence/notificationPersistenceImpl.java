package com.tt.notification.service.persistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import com.tt.notification.NoSuchnotificationException;
import com.tt.notification.model.impl.notificationImpl;
import com.tt.notification.model.impl.notificationModelImpl;
import com.tt.notification.model.notification;
import com.tt.notification.service.persistence.notificationPersistence;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the notification service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Prashant Patel
 * @see notificationPersistence
 * @see notificationUtil
 * @generated
 */
public class notificationPersistenceImpl extends BasePersistenceImpl<notification>
    implements notificationPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link notificationUtil} to access the notification persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = notificationImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(notificationModelImpl.ENTITY_CACHE_ENABLED,
            notificationModelImpl.FINDER_CACHE_ENABLED, notificationImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(notificationModelImpl.ENTITY_CACHE_ENABLED,
            notificationModelImpl.FINDER_CACHE_ENABLED, notificationImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(notificationModelImpl.ENTITY_CACHE_ENABLED,
            notificationModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_NOTIFICATION = "SELECT notification FROM notification notification";
    private static final String _SQL_COUNT_NOTIFICATION = "SELECT COUNT(notification) FROM notification notification";
    private static final String _ORDER_BY_ENTITY_ALIAS = "notification.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No notification exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(notificationPersistenceImpl.class);
    private static notification _nullnotification = new notificationImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<notification> toCacheModel() {
                return _nullnotificationCacheModel;
            }
        };

    private static CacheModel<notification> _nullnotificationCacheModel = new CacheModel<notification>() {
            @Override
            public notification toEntityModel() {
                return _nullnotification;
            }
        };

    public notificationPersistenceImpl() {
        setModelClass(notification.class);
    }

    /**
     * Caches the notification in the entity cache if it is enabled.
     *
     * @param notification the notification
     */
    @Override
    public void cacheResult(notification notification) {
        EntityCacheUtil.putResult(notificationModelImpl.ENTITY_CACHE_ENABLED,
            notificationImpl.class, notification.getPrimaryKey(), notification);

        notification.resetOriginalValues();
    }

    /**
     * Caches the notifications in the entity cache if it is enabled.
     *
     * @param notifications the notifications
     */
    @Override
    public void cacheResult(List<notification> notifications) {
        for (notification notification : notifications) {
            if (EntityCacheUtil.getResult(
                        notificationModelImpl.ENTITY_CACHE_ENABLED,
                        notificationImpl.class, notification.getPrimaryKey()) == null) {
                cacheResult(notification);
            } else {
                notification.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all notifications.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(notificationImpl.class.getName());
        }

        EntityCacheUtil.clearCache(notificationImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the notification.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(notification notification) {
        EntityCacheUtil.removeResult(notificationModelImpl.ENTITY_CACHE_ENABLED,
            notificationImpl.class, notification.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<notification> notifications) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (notification notification : notifications) {
            EntityCacheUtil.removeResult(notificationModelImpl.ENTITY_CACHE_ENABLED,
                notificationImpl.class, notification.getPrimaryKey());
        }
    }

    /**
     * Creates a new notification with the primary key. Does not add the notification to the database.
     *
     * @param notificationID the primary key for the new notification
     * @return the new notification
     */
    @Override
    public notification create(long notificationID) {
        notification notification = new notificationImpl();

        notification.setNew(true);
        notification.setPrimaryKey(notificationID);

        return notification;
    }

    /**
     * Removes the notification with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param notificationID the primary key of the notification
     * @return the notification that was removed
     * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public notification remove(long notificationID)
        throws NoSuchnotificationException, SystemException {
        return remove((Serializable) notificationID);
    }

    /**
     * Removes the notification with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the notification
     * @return the notification that was removed
     * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public notification remove(Serializable primaryKey)
        throws NoSuchnotificationException, SystemException {
        Session session = null;

        try {
            session = openSession();

            notification notification = (notification) session.get(notificationImpl.class,
                    primaryKey);

            if (notification == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchnotificationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(notification);
        } catch (NoSuchnotificationException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected notification removeImpl(notification notification)
        throws SystemException {
        notification = toUnwrappedModel(notification);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(notification)) {
                notification = (notification) session.get(notificationImpl.class,
                        notification.getPrimaryKeyObj());
            }

            if (notification != null) {
                session.delete(notification);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (notification != null) {
            clearCache(notification);
        }

        return notification;
    }

    @Override
    public notification updateImpl(
        com.tt.notification.model.notification notification)
        throws SystemException {
        notification = toUnwrappedModel(notification);

        boolean isNew = notification.isNew();

        Session session = null;

        try {
            session = openSession();

            if (notification.isNew()) {
                session.save(notification);

                notification.setNew(false);
            } else {
                session.merge(notification);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(notificationModelImpl.ENTITY_CACHE_ENABLED,
            notificationImpl.class, notification.getPrimaryKey(), notification);

        return notification;
    }

    protected notification toUnwrappedModel(notification notification) {
        if (notification instanceof notificationImpl) {
            return notification;
        }

        notificationImpl notificationImpl = new notificationImpl();

        notificationImpl.setNew(notification.isNew());
        notificationImpl.setPrimaryKey(notification.getPrimaryKey());

        notificationImpl.setNotificationID(notification.getNotificationID());
        notificationImpl.setNotificationTitle(notification.getNotificationTitle());
        notificationImpl.setNotificationDescription(notification.getNotificationDescription());
        notificationImpl.setNotificationURL(notification.getNotificationURL());
        notificationImpl.setNotificationSender(notification.getNotificationSender());

        return notificationImpl;
    }

    /**
     * Returns the notification with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the notification
     * @return the notification
     * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public notification findByPrimaryKey(Serializable primaryKey)
        throws NoSuchnotificationException, SystemException {
        notification notification = fetchByPrimaryKey(primaryKey);

        if (notification == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchnotificationException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return notification;
    }

    /**
     * Returns the notification with the primary key or throws a {@link com.tt.notification.NoSuchnotificationException} if it could not be found.
     *
     * @param notificationID the primary key of the notification
     * @return the notification
     * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public notification findByPrimaryKey(long notificationID)
        throws NoSuchnotificationException, SystemException {
        return findByPrimaryKey((Serializable) notificationID);
    }

    /**
     * Returns the notification with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the notification
     * @return the notification, or <code>null</code> if a notification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public notification fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        notification notification = (notification) EntityCacheUtil.getResult(notificationModelImpl.ENTITY_CACHE_ENABLED,
                notificationImpl.class, primaryKey);

        if (notification == _nullnotification) {
            return null;
        }

        if (notification == null) {
            Session session = null;

            try {
                session = openSession();

                notification = (notification) session.get(notificationImpl.class,
                        primaryKey);

                if (notification != null) {
                    cacheResult(notification);
                } else {
                    EntityCacheUtil.putResult(notificationModelImpl.ENTITY_CACHE_ENABLED,
                        notificationImpl.class, primaryKey, _nullnotification);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(notificationModelImpl.ENTITY_CACHE_ENABLED,
                    notificationImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return notification;
    }

    /**
     * Returns the notification with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param notificationID the primary key of the notification
     * @return the notification, or <code>null</code> if a notification with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public notification fetchByPrimaryKey(long notificationID)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) notificationID);
    }

    /**
     * Returns all the notifications.
     *
     * @return the notifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<notification> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the notifications.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of notifications
     * @param end the upper bound of the range of notifications (not inclusive)
     * @return the range of notifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<notification> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the notifications.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of notifications
     * @param end the upper bound of the range of notifications (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of notifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<notification> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<notification> list = (List<notification>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_NOTIFICATION);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_NOTIFICATION;

                if (pagination) {
                    sql = sql.concat(notificationModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<notification>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<notification>(list);
                } else {
                    list = (List<notification>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the notifications from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (notification notification : findAll()) {
            remove(notification);
        }
    }

    /**
     * Returns the number of notifications.
     *
     * @return the number of notifications
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_NOTIFICATION);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the notification persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.tt.notification.model.notification")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<notification>> listenersList = new ArrayList<ModelListener<notification>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<notification>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(notificationImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
