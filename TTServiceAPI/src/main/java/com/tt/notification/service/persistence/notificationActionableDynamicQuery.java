package com.tt.notification.service.persistence;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

import com.tt.notification.model.notification;
import com.tt.notification.service.notificationLocalServiceUtil;

/**
 * @author Prashant Patel
 * @generated
 */
public abstract class notificationActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public notificationActionableDynamicQuery() throws SystemException {
        setBaseLocalService(notificationLocalServiceUtil.getService());
        setClass(notification.class);

        setClassLoader(com.tt.notification.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("notificationID");
    }
}
