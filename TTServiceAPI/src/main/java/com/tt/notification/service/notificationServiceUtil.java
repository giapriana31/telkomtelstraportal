package com.tt.notification.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for notification. This utility wraps
 * {@link com.tt.notification.service.impl.notificationServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Prashant Patel
 * @see notificationService
 * @see com.tt.notification.service.base.notificationServiceBaseImpl
 * @see com.tt.notification.service.impl.notificationServiceImpl
 * @generated
 */
public class notificationServiceUtil {
    private static notificationService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.tt.notification.service.impl.notificationServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.lang.String setNotification(java.lang.String title,
        java.lang.String description, java.lang.String url,
        java.lang.String producerSystem, java.lang.String producerUser,
        java.lang.String usersEmail, java.lang.String customerID) {
        return getService()
                   .setNotification(title, description, url, producerSystem,
            producerUser, usersEmail, customerID);
    }

    public static void clearService() {
        _service = null;
    }

    public static notificationService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    notificationService.class.getName());

            if (invokableService instanceof notificationService) {
                _service = (notificationService) invokableService;
            } else {
                _service = new notificationServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(notificationServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(notificationService service) {
    }
}
