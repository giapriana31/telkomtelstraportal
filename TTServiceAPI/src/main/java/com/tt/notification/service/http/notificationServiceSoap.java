package com.tt.notification.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import com.tt.notification.service.notificationServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link com.tt.notification.service.notificationServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link com.tt.notification.model.notificationSoap}.
 * If the method in the service utility returns a
 * {@link com.tt.notification.model.notification}, that is translated to a
 * {@link com.tt.notification.model.notificationSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Prashant Patel
 * @see notificationServiceHttp
 * @see com.tt.notification.model.notificationSoap
 * @see com.tt.notification.service.notificationServiceUtil
 * @generated
 */
public class notificationServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(notificationServiceSoap.class);

    public static java.lang.String setNotification(java.lang.String title,
        java.lang.String description, java.lang.String url,
        java.lang.String producerSystem, java.lang.String producerUser,
        java.lang.String usersEmail, java.lang.String customerID)
        throws RemoteException {
        try {
            java.lang.String returnValue = notificationServiceUtil.setNotification(title,
                    description, url, producerSystem, producerUser, usersEmail,
                    customerID);

            return returnValue;
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
