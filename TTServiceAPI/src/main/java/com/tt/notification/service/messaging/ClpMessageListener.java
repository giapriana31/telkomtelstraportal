package com.tt.notification.service.messaging;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;

import com.tt.notification.service.ClpSerializer;
import com.tt.notification.service.notificationLocalServiceUtil;
import com.tt.notification.service.notificationServiceUtil;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            notificationLocalServiceUtil.clearService();

            notificationServiceUtil.clearService();
        }
    }
}
