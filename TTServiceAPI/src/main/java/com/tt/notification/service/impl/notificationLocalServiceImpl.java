package com.tt.notification.service.impl;

import com.tt.notification.service.base.notificationLocalServiceBaseImpl;

/**
 * The implementation of the notification local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.tt.notification.service.notificationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Prashant Patel
 * @see com.tt.notification.service.base.notificationLocalServiceBaseImpl
 * @see com.tt.notification.service.notificationLocalServiceUtil
 */
public class notificationLocalServiceImpl
    extends notificationLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.tt.notification.service.notificationLocalServiceUtil} to access the notification local service.
     */
}
