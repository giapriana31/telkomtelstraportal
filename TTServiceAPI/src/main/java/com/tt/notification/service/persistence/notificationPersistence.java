package com.tt.notification.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.tt.notification.model.notification;

/**
 * The persistence interface for the notification service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Prashant Patel
 * @see notificationPersistenceImpl
 * @see notificationUtil
 * @generated
 */
public interface notificationPersistence extends BasePersistence<notification> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link notificationUtil} to access the notification persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the notification in the entity cache if it is enabled.
    *
    * @param notification the notification
    */
    public void cacheResult(com.tt.notification.model.notification notification);

    /**
    * Caches the notifications in the entity cache if it is enabled.
    *
    * @param notifications the notifications
    */
    public void cacheResult(
        java.util.List<com.tt.notification.model.notification> notifications);

    /**
    * Creates a new notification with the primary key. Does not add the notification to the database.
    *
    * @param notificationID the primary key for the new notification
    * @return the new notification
    */
    public com.tt.notification.model.notification create(long notificationID);

    /**
    * Removes the notification with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param notificationID the primary key of the notification
    * @return the notification that was removed
    * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.tt.notification.model.notification remove(long notificationID)
        throws com.liferay.portal.kernel.exception.SystemException,
            com.tt.notification.NoSuchnotificationException;

    public com.tt.notification.model.notification updateImpl(
        com.tt.notification.model.notification notification)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the notification with the primary key or throws a {@link com.tt.notification.NoSuchnotificationException} if it could not be found.
    *
    * @param notificationID the primary key of the notification
    * @return the notification
    * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.tt.notification.model.notification findByPrimaryKey(
        long notificationID)
        throws com.liferay.portal.kernel.exception.SystemException,
            com.tt.notification.NoSuchnotificationException;

    /**
    * Returns the notification with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param notificationID the primary key of the notification
    * @return the notification, or <code>null</code> if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.tt.notification.model.notification fetchByPrimaryKey(
        long notificationID)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the notifications.
    *
    * @return the notifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.tt.notification.model.notification> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the notifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of notifications
    * @param end the upper bound of the range of notifications (not inclusive)
    * @return the range of notifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.tt.notification.model.notification> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the notifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of notifications
    * @param end the upper bound of the range of notifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of notifications
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.tt.notification.model.notification> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the notifications from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of notifications.
    *
    * @return the number of notifications
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
