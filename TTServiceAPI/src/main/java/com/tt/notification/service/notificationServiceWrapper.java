package com.tt.notification.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link notificationService}.
 *
 * @author Prashant Patel
 * @see notificationService
 * @generated
 */
public class notificationServiceWrapper implements notificationService,
    ServiceWrapper<notificationService> {
    private notificationService _notificationService;

    public notificationServiceWrapper(notificationService notificationService) {
        _notificationService = notificationService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _notificationService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _notificationService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _notificationService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.lang.String setNotification(java.lang.String title,
        java.lang.String description, java.lang.String url,
        java.lang.String producerSystem, java.lang.String producerUser,
        java.lang.String usersEmail, java.lang.String customerID) {
        return _notificationService.setNotification(title, description, url,
            producerSystem, producerUser, usersEmail, customerID);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public notificationService getWrappednotificationService() {
        return _notificationService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappednotificationService(
        notificationService notificationService) {
        _notificationService = notificationService;
    }

    @Override
    public notificationService getWrappedService() {
        return _notificationService;
    }

    @Override
    public void setWrappedService(notificationService notificationService) {
        _notificationService = notificationService;
    }
}
