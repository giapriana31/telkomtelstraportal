package com.tt.notification.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.tt.notification.model.notification;

import java.util.List;

/**
 * The persistence utility for the notification service. This utility wraps {@link notificationPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Prashant Patel
 * @see notificationPersistence
 * @see notificationPersistenceImpl
 * @generated
 */
public class notificationUtil {
    private static notificationPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(notification notification) {
        getPersistence().clearCache(notification);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<notification> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<notification> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<notification> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static notification update(notification notification)
        throws SystemException {
        return getPersistence().update(notification);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static notification update(notification notification,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(notification, serviceContext);
    }

    /**
    * Caches the notification in the entity cache if it is enabled.
    *
    * @param notification the notification
    */
    public static void cacheResult(
        com.tt.notification.model.notification notification) {
        getPersistence().cacheResult(notification);
    }

    /**
    * Caches the notifications in the entity cache if it is enabled.
    *
    * @param notifications the notifications
    */
    public static void cacheResult(
        java.util.List<com.tt.notification.model.notification> notifications) {
        getPersistence().cacheResult(notifications);
    }

    /**
    * Creates a new notification with the primary key. Does not add the notification to the database.
    *
    * @param notificationID the primary key for the new notification
    * @return the new notification
    */
    public static com.tt.notification.model.notification create(
        long notificationID) {
        return getPersistence().create(notificationID);
    }

    /**
    * Removes the notification with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param notificationID the primary key of the notification
    * @return the notification that was removed
    * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.tt.notification.model.notification remove(
        long notificationID)
        throws com.liferay.portal.kernel.exception.SystemException,
            com.tt.notification.NoSuchnotificationException {
        return getPersistence().remove(notificationID);
    }

    public static com.tt.notification.model.notification updateImpl(
        com.tt.notification.model.notification notification)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(notification);
    }

    /**
    * Returns the notification with the primary key or throws a {@link com.tt.notification.NoSuchnotificationException} if it could not be found.
    *
    * @param notificationID the primary key of the notification
    * @return the notification
    * @throws com.tt.notification.NoSuchnotificationException if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.tt.notification.model.notification findByPrimaryKey(
        long notificationID)
        throws com.liferay.portal.kernel.exception.SystemException,
            com.tt.notification.NoSuchnotificationException {
        return getPersistence().findByPrimaryKey(notificationID);
    }

    /**
    * Returns the notification with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param notificationID the primary key of the notification
    * @return the notification, or <code>null</code> if a notification with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.tt.notification.model.notification fetchByPrimaryKey(
        long notificationID)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(notificationID);
    }

    /**
    * Returns all the notifications.
    *
    * @return the notifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.tt.notification.model.notification> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the notifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of notifications
    * @param end the upper bound of the range of notifications (not inclusive)
    * @return the range of notifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.tt.notification.model.notification> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the notifications.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.tt.notification.model.impl.notificationModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of notifications
    * @param end the upper bound of the range of notifications (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of notifications
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.tt.notification.model.notification> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the notifications from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of notifications.
    *
    * @return the number of notifications
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static notificationPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (notificationPersistence) PortletBeanLocatorUtil.locate(com.tt.notification.service.ClpSerializer.getServletContextName(),
                    notificationPersistence.class.getName());

            ReferenceRegistry.registerReference(notificationUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(notificationPersistence persistence) {
    }
}
