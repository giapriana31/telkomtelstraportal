package com.tt.notification.service.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import com.liferay.portal.kernel.dao.jdbc.DataAccess;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserNotificationEvent;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserNotificationEventLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.tt.notification.DockBarUserNotificationHandler;
import com.tt.notification.service.base.notificationServiceBaseImpl;

public class notificationServiceImpl extends notificationServiceBaseImpl {
    
	//private final static Logger log = Logger.getLogger(notificationServiceImpl.class);
	 
	@SuppressWarnings("unchecked")
	public String setNotification(String title,String description,String url,String producerSystem, String producerUser,String usersEmail,String customerID)
	{
		try
		{
			
		title = new String(DatatypeConverter.parseBase64Binary(title));
		description = new String(DatatypeConverter.parseBase64Binary(description));
		url = new String(DatatypeConverter.parseBase64Binary(url));
		producerSystem = new String(DatatypeConverter.parseBase64Binary(producerSystem));
		producerUser = new String(DatatypeConverter.parseBase64Binary(producerUser));
		usersEmail = new String(DatatypeConverter.parseBase64Binary(usersEmail));
		customerID = new String(DatatypeConverter.parseBase64Binary(customerID));
		//System.out.println("API INVOKED FOR "+title+" CUSSTOMER ID IS "+customerID);
		
		Connection con=null;		
		CallableStatement stmt = null;
		try
		{
			con = DataAccess.getUpgradeOptimizedConnection();						
			stmt = con.prepareCall("{call dumpNotifications()}");	        
	        stmt.executeUpdate();
		}
		finally
		{
			 DataAccess.cleanUp(con, stmt);
		}
		
		ArrayList<User> userList = new ArrayList<User>();
		
		if(customerID == null || customerID.trim().equals("") || customerID.equalsIgnoreCase("null"))
		{
//			System.out.println("SENDING NOTIFICATION BY EMAIL ID");
			User user = UserLocalServiceUtil.getUserByEmailAddress(PortalUtil.getDefaultCompanyId(), usersEmail);
			userList.add(user);
		}
		else
		{
//			System.out.println("SENDING NOTIFICATION BY CUSTOEMER ID "+customerID);
			List<ExpandoValue> values = ExpandoValueLocalServiceUtil.getColumnValues(PortalUtil.getDefaultCompanyId(),ClassNameLocalServiceUtil.getClassNameId(User.class),ExpandoTableConstants.DEFAULT_TABLE_NAME,"CustomerUniqueID",customerID,-1,-1);
//			System.out.println("TOTAL USERS "+values.size());
			for(int i = 0; i < values.size(); i++)
			{
				userList.add(UserLocalServiceUtil.getUser(values.get(i).getClassPK()));				
			}
		}	
		
		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();			
		
		for(User user : userList)
		{
			JSONObject payloadJSON = JSONFactoryUtil.createJSONObject();
			payloadJSON.put("userId", user.getUserId());				
			payloadJSON.put("title", title);
			payloadJSON.put("description", description);
			payloadJSON.put("url", url);
			payloadJSON.put("producerSystem", producerSystem);
			payloadJSON.put("producerUser", producerUser);
			
			DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(UserNotificationEvent.class, PortalClassLoaderUtil.getClassLoader());
			Criterion criterion = RestrictionsFactoryUtil.eq("companyId", PortalUtil.getDefaultCompanyId());			 
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("userId", user.getUserId()));
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("payload", payloadJSON.toString()));
			//criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("delivered", false));
			dynamicQuery.add(criterion);			 
			
			List<UserNotificationEvent> events = UserLocalServiceUtil.dynamicQuery(dynamicQuery);
			if(events==null || events.size()==0)
			{
//				System.out.println("SENDING NOTIFICATION TO "+user.getEmailAddress()+" ID is "+title);
				UserNotificationEventLocalServiceUtil.addUserNotificationEvent(user.getUserId(),DockBarUserNotificationHandler.PORTLET_ID,
						new Date().getTime(), user.getUserId(),payloadJSON.toString(), false, serviceContext);	
			}
			else
			{
				for(UserNotificationEvent e : events)
				{
					UserNotificationEvent event = e;
//					System.out.println("DUPLICATE NOTIFICATION "+event.getUserNotificationEventId());
//					System.out.println("UPDATING TIMESTAMP");
//					System.out.println("OLD WAS "+new Date(event.getTimestamp()));
					event.setTimestamp(new Date().getTime());
					event.setArchived(false);
					event.setDelivered(false);
					UserNotificationEventLocalServiceUtil.updateUserNotificationEvent(event);
//					System.out.println("NEW IS "+new Date(event.getTimestamp()));
				}
			}
		}
	}
	
	catch(Exception e)
	{
//		System.out.println(e);
		return "failure\n\n\n" + e.toString();
	}
	
	return "success";
}
}
