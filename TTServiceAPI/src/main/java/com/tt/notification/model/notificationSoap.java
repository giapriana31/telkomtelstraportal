package com.tt.notification.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.tt.notification.service.http.notificationServiceSoap}.
 *
 * @author Prashant Patel
 * @see com.tt.notification.service.http.notificationServiceSoap
 * @generated
 */
public class notificationSoap implements Serializable {
    private long _notificationID;
    private String _notificationTitle;
    private String _notificationDescription;
    private String _notificationURL;
    private String _notificationSender;

    public notificationSoap() {
    }

    public static notificationSoap toSoapModel(notification model) {
        notificationSoap soapModel = new notificationSoap();

        soapModel.setNotificationID(model.getNotificationID());
        soapModel.setNotificationTitle(model.getNotificationTitle());
        soapModel.setNotificationDescription(model.getNotificationDescription());
        soapModel.setNotificationURL(model.getNotificationURL());
        soapModel.setNotificationSender(model.getNotificationSender());

        return soapModel;
    }

    public static notificationSoap[] toSoapModels(notification[] models) {
        notificationSoap[] soapModels = new notificationSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static notificationSoap[][] toSoapModels(notification[][] models) {
        notificationSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new notificationSoap[models.length][models[0].length];
        } else {
            soapModels = new notificationSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static notificationSoap[] toSoapModels(List<notification> models) {
        List<notificationSoap> soapModels = new ArrayList<notificationSoap>(models.size());

        for (notification model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new notificationSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _notificationID;
    }

    public void setPrimaryKey(long pk) {
        setNotificationID(pk);
    }

    public long getNotificationID() {
        return _notificationID;
    }

    public void setNotificationID(long notificationID) {
        _notificationID = notificationID;
    }

    public String getNotificationTitle() {
        return _notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        _notificationTitle = notificationTitle;
    }

    public String getNotificationDescription() {
        return _notificationDescription;
    }

    public void setNotificationDescription(String notificationDescription) {
        _notificationDescription = notificationDescription;
    }

    public String getNotificationURL() {
        return _notificationURL;
    }

    public void setNotificationURL(String notificationURL) {
        _notificationURL = notificationURL;
    }

    public String getNotificationSender() {
        return _notificationSender;
    }

    public void setNotificationSender(String notificationSender) {
        _notificationSender = notificationSender;
    }
}
