package com.tt.notification.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the notification service. Represents a row in the &quot;JSON_notification&quot; database table, with each column mapped to a property of this class.
 *
 * @author Prashant Patel
 * @see notificationModel
 * @see com.tt.notification.model.impl.notificationImpl
 * @see com.tt.notification.model.impl.notificationModelImpl
 * @generated
 */
public interface notification extends notificationModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.tt.notification.model.impl.notificationImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
