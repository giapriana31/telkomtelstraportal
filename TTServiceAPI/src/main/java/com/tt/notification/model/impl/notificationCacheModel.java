package com.tt.notification.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.tt.notification.model.notification;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing notification in entity cache.
 *
 * @author Prashant Patel
 * @see notification
 * @generated
 */
public class notificationCacheModel implements CacheModel<notification>,
    Externalizable {
    public long notificationID;
    public String notificationTitle;
    public String notificationDescription;
    public String notificationURL;
    public String notificationSender;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{notificationID=");
        sb.append(notificationID);
        sb.append(", notificationTitle=");
        sb.append(notificationTitle);
        sb.append(", notificationDescription=");
        sb.append(notificationDescription);
        sb.append(", notificationURL=");
        sb.append(notificationURL);
        sb.append(", notificationSender=");
        sb.append(notificationSender);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public notification toEntityModel() {
        notificationImpl notificationImpl = new notificationImpl();

        notificationImpl.setNotificationID(notificationID);

        if (notificationTitle == null) {
            notificationImpl.setNotificationTitle(StringPool.BLANK);
        } else {
            notificationImpl.setNotificationTitle(notificationTitle);
        }

        if (notificationDescription == null) {
            notificationImpl.setNotificationDescription(StringPool.BLANK);
        } else {
            notificationImpl.setNotificationDescription(notificationDescription);
        }

        if (notificationURL == null) {
            notificationImpl.setNotificationURL(StringPool.BLANK);
        } else {
            notificationImpl.setNotificationURL(notificationURL);
        }

        if (notificationSender == null) {
            notificationImpl.setNotificationSender(StringPool.BLANK);
        } else {
            notificationImpl.setNotificationSender(notificationSender);
        }

        notificationImpl.resetOriginalValues();

        return notificationImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        notificationID = objectInput.readLong();
        notificationTitle = objectInput.readUTF();
        notificationDescription = objectInput.readUTF();
        notificationURL = objectInput.readUTF();
        notificationSender = objectInput.readUTF();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(notificationID);

        if (notificationTitle == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(notificationTitle);
        }

        if (notificationDescription == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(notificationDescription);
        }

        if (notificationURL == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(notificationURL);
        }

        if (notificationSender == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(notificationSender);
        }
    }
}
