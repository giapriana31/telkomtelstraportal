package com.tt.notification.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link notification}.
 * </p>
 *
 * @author Prashant Patel
 * @see notification
 * @generated
 */
public class notificationWrapper implements notification,
    ModelWrapper<notification> {
    private notification _notification;

    public notificationWrapper(notification notification) {
        _notification = notification;
    }

    @Override
    public Class<?> getModelClass() {
        return notification.class;
    }

    @Override
    public String getModelClassName() {
        return notification.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("notificationID", getNotificationID());
        attributes.put("notificationTitle", getNotificationTitle());
        attributes.put("notificationDescription", getNotificationDescription());
        attributes.put("notificationURL", getNotificationURL());
        attributes.put("notificationSender", getNotificationSender());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long notificationID = (Long) attributes.get("notificationID");

        if (notificationID != null) {
            setNotificationID(notificationID);
        }

        String notificationTitle = (String) attributes.get("notificationTitle");

        if (notificationTitle != null) {
            setNotificationTitle(notificationTitle);
        }

        String notificationDescription = (String) attributes.get(
                "notificationDescription");

        if (notificationDescription != null) {
            setNotificationDescription(notificationDescription);
        }

        String notificationURL = (String) attributes.get("notificationURL");

        if (notificationURL != null) {
            setNotificationURL(notificationURL);
        }

        String notificationSender = (String) attributes.get(
                "notificationSender");

        if (notificationSender != null) {
            setNotificationSender(notificationSender);
        }
    }

    /**
    * Returns the primary key of this notification.
    *
    * @return the primary key of this notification
    */
    @Override
    public long getPrimaryKey() {
        return _notification.getPrimaryKey();
    }

    /**
    * Sets the primary key of this notification.
    *
    * @param primaryKey the primary key of this notification
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _notification.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the notification i d of this notification.
    *
    * @return the notification i d of this notification
    */
    @Override
    public long getNotificationID() {
        return _notification.getNotificationID();
    }

    /**
    * Sets the notification i d of this notification.
    *
    * @param notificationID the notification i d of this notification
    */
    @Override
    public void setNotificationID(long notificationID) {
        _notification.setNotificationID(notificationID);
    }

    /**
    * Returns the notification title of this notification.
    *
    * @return the notification title of this notification
    */
    @Override
    public java.lang.String getNotificationTitle() {
        return _notification.getNotificationTitle();
    }

    /**
    * Sets the notification title of this notification.
    *
    * @param notificationTitle the notification title of this notification
    */
    @Override
    public void setNotificationTitle(java.lang.String notificationTitle) {
        _notification.setNotificationTitle(notificationTitle);
    }

    /**
    * Returns the notification description of this notification.
    *
    * @return the notification description of this notification
    */
    @Override
    public java.lang.String getNotificationDescription() {
        return _notification.getNotificationDescription();
    }

    /**
    * Sets the notification description of this notification.
    *
    * @param notificationDescription the notification description of this notification
    */
    @Override
    public void setNotificationDescription(
        java.lang.String notificationDescription) {
        _notification.setNotificationDescription(notificationDescription);
    }

    /**
    * Returns the notification u r l of this notification.
    *
    * @return the notification u r l of this notification
    */
    @Override
    public java.lang.String getNotificationURL() {
        return _notification.getNotificationURL();
    }

    /**
    * Sets the notification u r l of this notification.
    *
    * @param notificationURL the notification u r l of this notification
    */
    @Override
    public void setNotificationURL(java.lang.String notificationURL) {
        _notification.setNotificationURL(notificationURL);
    }

    /**
    * Returns the notification sender of this notification.
    *
    * @return the notification sender of this notification
    */
    @Override
    public java.lang.String getNotificationSender() {
        return _notification.getNotificationSender();
    }

    /**
    * Sets the notification sender of this notification.
    *
    * @param notificationSender the notification sender of this notification
    */
    @Override
    public void setNotificationSender(java.lang.String notificationSender) {
        _notification.setNotificationSender(notificationSender);
    }

    @Override
    public boolean isNew() {
        return _notification.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _notification.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _notification.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _notification.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _notification.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _notification.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _notification.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _notification.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _notification.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _notification.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _notification.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new notificationWrapper((notification) _notification.clone());
    }

    @Override
    public int compareTo(com.tt.notification.model.notification notification) {
        return _notification.compareTo(notification);
    }

    @Override
    public int hashCode() {
        return _notification.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<com.tt.notification.model.notification> toCacheModel() {
        return _notification.toCacheModel();
    }

    @Override
    public com.tt.notification.model.notification toEscapedModel() {
        return new notificationWrapper(_notification.toEscapedModel());
    }

    @Override
    public com.tt.notification.model.notification toUnescapedModel() {
        return new notificationWrapper(_notification.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _notification.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _notification.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _notification.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof notificationWrapper)) {
            return false;
        }

        notificationWrapper notificationWrapper = (notificationWrapper) obj;

        if (Validator.equals(_notification, notificationWrapper._notification)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public notification getWrappednotification() {
        return _notification;
    }

    @Override
    public notification getWrappedModel() {
        return _notification;
    }

    @Override
    public void resetOriginalValues() {
        _notification.resetOriginalValues();
    }
}
