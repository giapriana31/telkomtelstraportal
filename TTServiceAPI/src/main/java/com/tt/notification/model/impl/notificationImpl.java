package com.tt.notification.model.impl;

/**
 * The extended model implementation for the notification service. Represents a row in the &quot;JSON_notification&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.tt.notification.model.notification} interface.
 * </p>
 *
 * @author Prashant Patel
 */
public class notificationImpl extends notificationBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a notification model instance should use the {@link com.tt.notification.model.notification} interface instead.
     */
    public notificationImpl() {
    }
}
