package com.tt.notification.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import com.tt.notification.service.ClpSerializer;
import com.tt.notification.service.notificationLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class notificationClp extends BaseModelImpl<notification>
    implements notification {
    private long _notificationID;
    private String _notificationTitle;
    private String _notificationDescription;
    private String _notificationURL;
    private String _notificationSender;
    private BaseModel<?> _notificationRemoteModel;
    private Class<?> _clpSerializerClass = com.tt.notification.service.ClpSerializer.class;

    public notificationClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return notification.class;
    }

    @Override
    public String getModelClassName() {
        return notification.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _notificationID;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setNotificationID(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _notificationID;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("notificationID", getNotificationID());
        attributes.put("notificationTitle", getNotificationTitle());
        attributes.put("notificationDescription", getNotificationDescription());
        attributes.put("notificationURL", getNotificationURL());
        attributes.put("notificationSender", getNotificationSender());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long notificationID = (Long) attributes.get("notificationID");

        if (notificationID != null) {
            setNotificationID(notificationID);
        }

        String notificationTitle = (String) attributes.get("notificationTitle");

        if (notificationTitle != null) {
            setNotificationTitle(notificationTitle);
        }

        String notificationDescription = (String) attributes.get(
                "notificationDescription");

        if (notificationDescription != null) {
            setNotificationDescription(notificationDescription);
        }

        String notificationURL = (String) attributes.get("notificationURL");

        if (notificationURL != null) {
            setNotificationURL(notificationURL);
        }

        String notificationSender = (String) attributes.get(
                "notificationSender");

        if (notificationSender != null) {
            setNotificationSender(notificationSender);
        }
    }

    @Override
    public long getNotificationID() {
        return _notificationID;
    }

    @Override
    public void setNotificationID(long notificationID) {
        _notificationID = notificationID;

        if (_notificationRemoteModel != null) {
            try {
                Class<?> clazz = _notificationRemoteModel.getClass();

                Method method = clazz.getMethod("setNotificationID", long.class);

                method.invoke(_notificationRemoteModel, notificationID);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNotificationTitle() {
        return _notificationTitle;
    }

    @Override
    public void setNotificationTitle(String notificationTitle) {
        _notificationTitle = notificationTitle;

        if (_notificationRemoteModel != null) {
            try {
                Class<?> clazz = _notificationRemoteModel.getClass();

                Method method = clazz.getMethod("setNotificationTitle",
                        String.class);

                method.invoke(_notificationRemoteModel, notificationTitle);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNotificationDescription() {
        return _notificationDescription;
    }

    @Override
    public void setNotificationDescription(String notificationDescription) {
        _notificationDescription = notificationDescription;

        if (_notificationRemoteModel != null) {
            try {
                Class<?> clazz = _notificationRemoteModel.getClass();

                Method method = clazz.getMethod("setNotificationDescription",
                        String.class);

                method.invoke(_notificationRemoteModel, notificationDescription);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNotificationURL() {
        return _notificationURL;
    }

    @Override
    public void setNotificationURL(String notificationURL) {
        _notificationURL = notificationURL;

        if (_notificationRemoteModel != null) {
            try {
                Class<?> clazz = _notificationRemoteModel.getClass();

                Method method = clazz.getMethod("setNotificationURL",
                        String.class);

                method.invoke(_notificationRemoteModel, notificationURL);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getNotificationSender() {
        return _notificationSender;
    }

    @Override
    public void setNotificationSender(String notificationSender) {
        _notificationSender = notificationSender;

        if (_notificationRemoteModel != null) {
            try {
                Class<?> clazz = _notificationRemoteModel.getClass();

                Method method = clazz.getMethod("setNotificationSender",
                        String.class);

                method.invoke(_notificationRemoteModel, notificationSender);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getnotificationRemoteModel() {
        return _notificationRemoteModel;
    }

    public void setnotificationRemoteModel(BaseModel<?> notificationRemoteModel) {
        _notificationRemoteModel = notificationRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _notificationRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_notificationRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            notificationLocalServiceUtil.addnotification(this);
        } else {
            notificationLocalServiceUtil.updatenotification(this);
        }
    }

    @Override
    public notification toEscapedModel() {
        return (notification) ProxyUtil.newProxyInstance(notification.class.getClassLoader(),
            new Class[] { notification.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        notificationClp clone = new notificationClp();

        clone.setNotificationID(getNotificationID());
        clone.setNotificationTitle(getNotificationTitle());
        clone.setNotificationDescription(getNotificationDescription());
        clone.setNotificationURL(getNotificationURL());
        clone.setNotificationSender(getNotificationSender());

        return clone;
    }

    @Override
    public int compareTo(notification notification) {
        long primaryKey = notification.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof notificationClp)) {
            return false;
        }

        notificationClp notification = (notificationClp) obj;

        long primaryKey = notification.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{notificationID=");
        sb.append(getNotificationID());
        sb.append(", notificationTitle=");
        sb.append(getNotificationTitle());
        sb.append(", notificationDescription=");
        sb.append(getNotificationDescription());
        sb.append(", notificationURL=");
        sb.append(getNotificationURL());
        sb.append(", notificationSender=");
        sb.append(getNotificationSender());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("com.tt.notification.model.notification");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>notificationID</column-name><column-value><![CDATA[");
        sb.append(getNotificationID());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>notificationTitle</column-name><column-value><![CDATA[");
        sb.append(getNotificationTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>notificationDescription</column-name><column-value><![CDATA[");
        sb.append(getNotificationDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>notificationURL</column-name><column-value><![CDATA[");
        sb.append(getNotificationURL());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>notificationSender</column-name><column-value><![CDATA[");
        sb.append(getNotificationSender());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
