create table JSON_notification (
	notificationID LONG not null primary key,
	notificationTitle VARCHAR(75) null,
	notificationDescription VARCHAR(75) null,
	notificationURL VARCHAR(75) null,
	notificationSender VARCHAR(75) null
);