package com.tt.logticketservicemapping.mvc.web.controller;


import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

@Controller("logTicketServiceMappingController")
@RequestMapping("VIEW")
public class LogTicketServiceMappingController {

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException, SystemException {	
	    
	    return "logTicket";
	}
	
}
