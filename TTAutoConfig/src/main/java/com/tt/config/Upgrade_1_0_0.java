package com.tt.config;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.tt.config.site.UpgradeTT;
import com.tt.config.site.RemoveTTConfig;

public class Upgrade_1_0_0 extends UpgradeProcess {

    @Override
    public int getThreshold() {
        return 100;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {
    	
    	upgrade(RemoveTTConfig.class);
    	upgrade(UpgradeTT.class);
    }
}
