package com.tt.config.support;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class PageConfiguration {

    private Map<String, ColumnConfiguration> columns = new LinkedHashMap<String, ColumnConfiguration>();
    
    private PageConfiguration pageConfig;
    
    public PageConfiguration () {};
    
    public PageConfiguration(PageConfiguration config) {
    	this.pageConfig = config;
    }

    public ColumnConfiguration inColumn(String columnId) {
        if (!columns.containsKey(columnId)) {
            columns.put(columnId, new ColumnConfiguration());
        }
        return columns.get(columnId);
    }

    public Iterable<String> getColumns() {
        return columns.keySet();
    }

    public List<PortletConfiguration> getPortlets(final String columnId) {
        return columns.get(columnId).getPortlets();
    }

}
