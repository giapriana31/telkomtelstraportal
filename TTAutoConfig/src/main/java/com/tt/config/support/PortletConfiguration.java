package com.tt.config.support;

import java.util.LinkedHashMap;
import java.util.Map;

public class PortletConfiguration {

    private final String portletId;
    private int columnPosition;

    private Map<String, String> preferences = new LinkedHashMap<String, String>();

    public PortletConfiguration(final String portletId, final int columnPosition) {
        this.portletId = portletId;
        this.columnPosition = columnPosition;
    }
    
    public PortletConfiguration(final String portletId) {
        this.portletId = portletId;
        this.columnPosition = -1;
    }

    public String getPortletId() {
        return portletId;
    }

    public PortletConfiguration withPreference(String key,
                                               String value) {
        preferences.put(key, value);
        return this;
    }

    public Map<String, String> getPreferences() {
        return preferences;
    }

	public int getColumnPosition() {
		return columnPosition;
	}
    
    

}
