package com.tt.config.support;

import java.util.LinkedList;
import java.util.List;

public class ColumnConfiguration {

    private List<PortletConfiguration> portlets = new LinkedList<PortletConfiguration>();

    public PortletConfiguration addPortlet(final String portletId, final int columnPosition) {
        portlets.add(new PortletConfiguration(portletId, columnPosition));
        return portlets.get(portlets.size() - 1);
    }
    
    public PortletConfiguration addPortlet(final String portletId) {
        portlets.add(new PortletConfiguration(portletId, -1));
        return portlets.get(portlets.size() - 1);
    }

    public List<PortletConfiguration> getPortlets() {
        return portlets;
    }

}
