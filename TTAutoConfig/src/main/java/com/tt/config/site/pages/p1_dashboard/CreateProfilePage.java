package com.tt.config.site.pages.p1_dashboard;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.service.LayoutLocalService;
import com.liferay.portal.service.ServiceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import static com.liferay.portal.kernel.util.StringPool.BLANK;
import static com.liferay.portal.kernel.uuid.PortalUUIDUtil.generate;
import static com.liferay.portal.model.LayoutConstants.TYPE_PORTLET;
import static com.liferay.portal.service.GroupLocalServiceUtil.getGroup;
import static com.liferay.portal.service.UserLocalServiceUtil.getDefaultUserId;
import static com.liferay.portal.util.PortalUtil.getDefaultCompanyId;
import static java.util.Collections.singletonMap;
import static org.springframework.beans.factory.annotation.Autowire.BY_TYPE;

@Configurable(autowire = BY_TYPE,
              dependencyCheck = true)
public class CreateProfilePage extends UpgradeProcess {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateProfilePage.class);
	
    @Autowired
    private LayoutLocalService layoutLocalService;

    public LayoutLocalService getLayoutLocalService() {
        return layoutLocalService;
    }

    public void setLayoutLocalService(final LayoutLocalService layoutLocalService) {
        this.layoutLocalService = layoutLocalService;
    }

    @Override
    protected void doUpgrade() throws Exception {
    	
    	LOGGER.info("Creating page '/profile-settings'");

        ServiceContext serviceContext = new ServiceContext() {{
            setUuid(generate());
        }};

        Layout layout = layoutLocalService.addLayout(/* userId */ getDefaultUserId(getDefaultCompanyId()),
                                                     /* groupId */ getGroup(getDefaultCompanyId(), "TT").getGroupId(),
                                                     /* privateLayout */ true,
                                                     /* parentLayoutId */ 0L,
                                                     /* nameMap */ singletonMap(LocaleUtil.getDefault(), "Profile Settings"),
                                                     /* titleMap */ singletonMap(LocaleUtil.getDefault(), "Profile Settings"),
                                                     /* descriptionMap */ singletonMap(LocaleUtil.getDefault(), "TT Profile Settings"),
                                                     /* keywordMap */ singletonMap(LocaleUtil.getDefault(), BLANK),
                                                     /* robotsMap */ singletonMap(LocaleUtil.getDefault(), BLANK),
                                                     /* type */ TYPE_PORTLET,
                                                     /* hidden */ true,
                                                     /* friendlyUrl */ "/profile-settings",
                                                     /* serviceContext */ serviceContext);

        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
        layoutTypePortlet.setLayoutTemplateId(/* userId */ getDefaultUserId(getDefaultCompanyId()),
                                              /* newTemplateId */ "layout-column_column-1");

        layoutLocalService.updateLayout(layout);
    }

}
