package com.tt.config.site.pages.p2_tickets;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class UpgradeTickets extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception { 
        upgrade(CreateTicketsPage.class);
        upgrade(CreateUpdateTicketsPage.class);
        
        upgrade(AssignTicketsPortlets.class);
        upgrade(AssignUpdateTicketsPortlets.class);
    }
}