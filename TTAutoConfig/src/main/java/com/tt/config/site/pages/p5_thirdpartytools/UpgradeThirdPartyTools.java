package com.tt.config.site.pages.p5_thirdpartytools;

import org.springframework.transaction.annotation.Transactional;
import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.tt.config.site.pages.p5_thirdpartytools.CreateThirdPartyToolsPage;

public class UpgradeThirdPartyTools extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {        
        upgrade(CreateThirdPartyToolsPage.class);

    }
}