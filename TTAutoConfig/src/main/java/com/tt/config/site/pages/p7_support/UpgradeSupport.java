package com.tt.config.site.pages.p7_support;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class UpgradeSupport extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {        
        upgrade(CreateSupportPage.class);
        upgrade(CreateOnlineForumsPage.class);
        upgrade(CreateKnowledgeBasePage.class);
        upgrade(AssignSupportPortlets.class);
        upgrade(AssignOnlineForumsPortlets.class);
        upgrade(AssignKnowledgeBasePortlets.class);
    }
}