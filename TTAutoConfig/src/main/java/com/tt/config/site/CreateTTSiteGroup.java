package com.tt.config.site;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.LayoutSet;
import com.liferay.portal.service.GroupLocalService;
import com.liferay.portal.service.LayoutSetLocalService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import static com.liferay.portal.model.GroupConstants.TYPE_SITE_PRIVATE;
import static com.liferay.portal.service.UserLocalServiceUtil.getDefaultUserId;
import static com.liferay.portal.util.PortalUtil.getDefaultCompanyId;
import static org.springframework.beans.factory.annotation.Autowire.BY_TYPE;

@Configurable(autowire = BY_TYPE,
              dependencyCheck = true)
public class CreateTTSiteGroup extends UpgradeProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateTTSiteGroup.class);
	
    @Autowired
    private GroupLocalService groupLocalService;

    @Autowired
    private LayoutSetLocalService layoutSetLocalService;

    public GroupLocalService getGroupLocalService() {
        return groupLocalService;
    }

    public void setGroupLocalService(final GroupLocalService groupLocalService) {
        this.groupLocalService = groupLocalService;
    }

    public LayoutSetLocalService getLayoutSetLocalService() {
        return layoutSetLocalService;
    }

    public void setLayoutSetLocalService(final LayoutSetLocalService layoutSetLocalService) {
        this.layoutSetLocalService = layoutSetLocalService;
    }

    @Override
    protected void doUpgrade() throws Exception {
    

        Group group = groupLocalService.addGroup(/* userId */ getDefaultUserId(getDefaultCompanyId()),
                                                 /* className */ Group.class.getName(),
                                                 /* classPK */ -1,
                                                 /* name */ "TT",
                                                 /* description */ "Telkom Telstra Site",
                                                 /* type */ TYPE_SITE_PRIVATE,
                                                 /* friendlyURL */ "/tt",
                                                 /* site */ true,
                                                 /* active */ true,
                                                 /* serviceContext */ null);

        LOGGER.info("Creating private layout set for 'TT' site");
        
        LayoutSet publicLayoutSet = layoutSetLocalService.getLayoutSet(/* groupId */ group.getGroupId(),
                                                                       /* privateLayout */ false);
        publicLayoutSet.setThemeId("TTPortalTheme_WAR_TTPortalTheme");
        layoutSetLocalService.updateLayoutSet(publicLayoutSet);
        
        LOGGER.info("Creating private layout set for 'TT' site");

        LayoutSet privateLayoutSet = layoutSetLocalService.getLayoutSet(/* groupId */ group.getGroupId(),
                                                                        /* privateLayout */ true);
        privateLayoutSet.setThemeId("TTPortalTheme_WAR_TTPortalTheme");
        layoutSetLocalService.updateLayoutSet(privateLayoutSet);

    }

}
