package com.tt.config.site.pages.p6_servicerequest;

import static com.liferay.portal.service.GroupLocalServiceUtil.getGroup;
import static com.liferay.portal.service.UserLocalServiceUtil.getDefaultUserId;
import static com.liferay.portal.util.PortalUtil.getDefaultCompanyId;
import static org.springframework.beans.factory.annotation.Autowire.BY_TYPE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.service.LayoutLocalService;
import com.liferay.portal.service.RoleLocalService;
import com.liferay.portlet.PortletPreferencesFactory;
import com.tt.config.support.PageConfiguration;
import com.tt.config.support.PortletConfiguration;

@Configurable(autowire = BY_TYPE,
              dependencyCheck = true)

public class AssignServiceRequestPortlets extends UpgradeProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(AssignServiceRequestPortlets.class);

    private PageConfiguration PAGE_CONFIGURATION = new PageConfiguration();

    public void updatePageConfig() {

        PAGE_CONFIGURATION.inColumn("column-1")
                          .addPortlet("TTServiceRequestMappingPortlet_WAR_TTApplicationPortlets");
        
        PAGE_CONFIGURATION.inColumn("column-1")
        				  .addPortlet("TTScreenSharePortlet_WAR_TTScreenSharePortlet");

    }

    @Autowired
    private LayoutLocalService layoutLocalService;

    @Autowired
    private PortletPreferencesFactory portletPreferencesFactory;
    
    @Autowired
    private RoleLocalService roleLocalService;

    public RoleLocalService getRoleLocalService() {
        return roleLocalService;
    }

    public void setRoleLocalService(final RoleLocalService roleLocalService) {
        this.roleLocalService = roleLocalService;
    }

    public LayoutLocalService getLayoutLocalService() {
        return layoutLocalService;
    }

    public void setLayoutLocalService(final LayoutLocalService layoutLocalService) {
        this.layoutLocalService = layoutLocalService;
    }

    public PortletPreferencesFactory getPortletPreferencesFactory() {
        return portletPreferencesFactory;
    }

    public void setPortletPreferencesFactory(final PortletPreferencesFactory portletPreferencesFactory) {
        this.portletPreferencesFactory = portletPreferencesFactory;
    }

    @Override
    protected void doUpgrade() throws Exception {

        LOGGER.info("Configuring portlets on '/service-request'");
        
        updatePageConfig();

        Group group = getGroup(/* companyId */ getDefaultCompanyId(),
                               /* name */ "TT");

        Layout layout = layoutLocalService.getFriendlyURLLayout(/* groupId */ group.getGroupId(),
                                                                /* privateLayout */ true,
                                                                /* friendlyUrl */ "/service-request");

        LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();

        for (String columnId : PAGE_CONFIGURATION.getColumns()) {
            for (PortletConfiguration portlet : PAGE_CONFIGURATION.getPortlets(columnId)) {

                LOGGER.info("Adding portlet '{}' to column '{}'", portlet.getPortletId(), columnId);

                layoutTypePortlet.addPortletId(/* userId */ getDefaultUserId(getDefaultCompanyId()),
                                                                          /* portletId */ portlet.getPortletId(),
                                                                          /* columnId */ columnId,
                                                                          /* columnPosition */ -1,
                                                                          /* checkPermission */ false);
                layoutLocalService.updateLayout(layout);
            }
        }
    }
}
