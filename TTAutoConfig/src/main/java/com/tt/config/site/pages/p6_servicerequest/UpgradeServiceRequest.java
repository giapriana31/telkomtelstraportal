package com.tt.config.site.pages.p6_servicerequest;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class UpgradeServiceRequest extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {        
        upgrade(CreateServiceRequestPage.class);
        upgrade(AssignServiceRequestPortlets.class);

    }
}