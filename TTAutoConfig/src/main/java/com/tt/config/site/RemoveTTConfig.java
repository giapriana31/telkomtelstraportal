package com.tt.config.site;

import static com.liferay.portal.service.GroupLocalServiceUtil.getGroup;
import static com.liferay.portal.util.PortalUtil.getDefaultCompanyId;
import static org.springframework.beans.factory.annotation.Autowire.BY_TYPE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Layout;
import com.liferay.portal.service.GroupLocalService;
import com.liferay.portal.service.LayoutLocalServiceUtil;

@Configurable(autowire = BY_TYPE,
              dependencyCheck = true)

public class RemoveTTConfig extends UpgradeProcess {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoveTTConfig.class);
    
    @Autowired
    private GroupLocalService groupLocalService;

    public GroupLocalService getGroupLocalService() {
        return groupLocalService;
    }

    public void setGroupLocalService(final GroupLocalService groupLocalService) {
        this.groupLocalService = groupLocalService;
    }
    
    @Override
    protected void doUpgrade() throws Exception{

        LOGGER.info("Remove TT config items");

        try {
        	Group group = getGroup(/* companyId */ getDefaultCompanyId(),
                    /* name */ "TT");
        	List<Layout> layouts = LayoutLocalServiceUtil.getLayouts(group.getGroupId(), true);
        	
        	LOGGER.info("deleting {} layouts from group TT", layouts.size());
        	for (Layout l: layouts)
        	{
        		LayoutLocalServiceUtil.deleteLayout(l);
        	}
        	
        	groupLocalService.deleteGroup(group);
        	
        } catch(Exception ex) {
        	
        	LOGGER.warn("existing group 'TT' not found. This is not a problem if running for the first time");
        }
    }
}
