package com.tt.config.site.pages.p4_tools;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class UpgradeTools extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {        
    	
    	upgrade(CreateToolsPage.class);
    }
}