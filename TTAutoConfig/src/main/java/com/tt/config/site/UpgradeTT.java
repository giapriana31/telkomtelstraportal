package com.tt.config.site;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;
import com.tt.config.site.CreateTTSiteGroup;
import com.tt.config.site.pages.p1_dashboard.UpgradeDashboard;
import com.tt.config.site.pages.p2_tickets.UpgradeTickets;
import com.tt.config.site.pages.p3_reports.UpgradeReports;
import com.tt.config.site.pages.p4_tools.UpgradeTools;
import com.tt.config.site.pages.p5_thirdpartytools.UpgradeThirdPartyTools;
import com.tt.config.site.pages.p6_servicerequest.UpgradeServiceRequest;
import com.tt.config.site.pages.p7_support.UpgradeSupport;

public class UpgradeTT extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {        
    	
    	upgrade(CreateTTSiteGroup.class);
        upgrade(UpgradeDashboard.class);
        upgrade(UpgradeTickets.class);
        upgrade(UpgradeReports.class);
        upgrade(UpgradeTools.class);
        upgrade(UpgradeThirdPartyTools.class);
        upgrade(UpgradeServiceRequest.class);
        upgrade(UpgradeSupport.class);
    }
}
