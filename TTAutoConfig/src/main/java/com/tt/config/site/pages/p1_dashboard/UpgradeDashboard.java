package com.tt.config.site.pages.p1_dashboard;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class UpgradeDashboard extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {        
        upgrade(CreateDashboardPage.class);
        upgrade(CreateSitesPage.class);
        upgrade(CreateNetworkDetailsPage.class);
        upgrade(CreateAvailabilityPage.class);
        upgrade(CreateProfilePage.class);
        upgrade(CreateFooterPage.class);
        
        upgrade(AssignDashboardPortlets.class);
        upgrade(AssignSitesPortlets.class);
        upgrade(AssignNetworkDetailsPortlets.class);        
        upgrade(AssignAvailabilityPortlets.class);
        upgrade(AssignProfilePortlets.class);
        upgrade(AssignFooterPortlets.class);
    }
}