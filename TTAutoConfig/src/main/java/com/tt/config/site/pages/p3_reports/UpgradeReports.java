package com.tt.config.site.pages.p3_reports;

import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.upgrade.UpgradeProcess;

public class UpgradeReports extends UpgradeProcess {

    @Override
    @Transactional(rollbackFor = Exception.class)
    protected void doUpgrade() throws Exception {        
        upgrade(CreateReportsPage.class);
        upgrade(AssignReportsPortlets.class);

    }
}