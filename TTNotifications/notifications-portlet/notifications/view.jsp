<%@ include file="/init.jsp" %>

<style>
#bttabs
{
  width: 100% !important;
}
.support-header
{
  display: none !important;
}
.nav.nav-pills.support-ul
{
  display: none !important;
}
.pl-25
{
	padding-left: 0px !important;
}
.span9.user-notifications-list-container
{
  width: 100% !important;
}
.left-nav.previous a
{
	font-weight:bold;
	color: red !important;
}
.next.right-nav a
{
	font-weight:bold;
	color: red !important;
}
.portlet-title-text
{
	color: #999 !important;
}
.portlet-icon-back
{
    color: #999 !important;
}
.page-info
{
    text-align: left !important;
	padding-left: 10px;
	font-weight:bold;
}
.notifications-portlet .user-notifications-list .user-notification
{
    border: 1px solid #DDD;
	margin: 0 20;
}
.portlet-topper
{
	height: inherit !important;
}
.sla_back_button
{
	height:38px;
	width:193px;	
	color: white;	
	border-radius:5px !important;
}
</style>

<script>
var data = '<table style="width:99%;margin-top:10px!important;margin-left:15px!important;margin-bottom:10px!important;"><tr><td style="width:50%;font-size: 24px; color: #333;"><img id="bellImage" src="<%=themeDisplay.getPathThemeImages()%>/bell.png" style="height: 40px !important;width: 40px !important;"><liferay-ui:message key="notifications"/></td><td style="width:50%;"><input type="button" value=" <  <liferay-ui:message key="Back"/>" onclick="window.location=window.history.go(-1)" class="sla_back_button" style="background: url(<%=themeDisplay.getPathThemeImages()%>/u5.png) no-repeat;font-size: 125%;float:right;margin-right:10px;border-radius:5px !important;"/></td></tr></table>';

data += '<hr style="height:1px;border:none;color:#DDD;background-color:#DDD;margin-left:-10px;margin-top:0px!important;margin-bottom:0px!important;"/>';

$( "#p_p_id_1_WAR_notificationsportlet_ .portlet .portlet-topper").html(data);
</script>

<%
boolean actionable = ParamUtil.getBoolean(request, "actionable");
%>

<div class="clearfix user-notifications-container <%= actionable ? "actionable" : "non-actionable" %>">
	<aui:row>
		<!--<aui:col cssClass="nav-bar user-notifications-sidebar" width="<%= 25 %>">
			<div class="nav">
				<a class="clearfix non-actionable <%= !actionable ? "selected" : "" %>" href="javascript:;">
					<span class="title"><liferay-ui:message key="notifications" /></span>

					<%
					int unreadNonActionableUserNotificationsCount = NotificationsUtil.getArchivedUserNotificationEventsCount(themeDisplay.getUserId(), false, false);
					%>

					<span class="count"><%= unreadNonActionableUserNotificationsCount %></span>
				</a>
			</div>

			<div class="nav">
				<a class="actionable clearfix <%= actionable ? "selected" : "" %>" href="javascript:;">
					<span class="title"><liferay-ui:message key="requests" /></span>

					<%
					int unreadActionableUserNotificationsCount = NotificationsUtil.getArchivedUserNotificationEventsCount(themeDisplay.getUserId(), true, false);
					%>

					<span class="count"><%= unreadActionableUserNotificationsCount %></span>
				</a>
			</div>

			<div class="nav">
				<a class="clearfix manage" href="javascript:;">
					<span class="title"><liferay-ui:message key="notification-delivery" /></span>
				</a>
			</div>
		</aui:col>-->

		<aui:col cssClass="user-notifications-list-container" width="<%= 75 %>" style="width:100%;">
			<ul class="unstyled user-notifications-list">
				<li class="clearfix pagination top">
					<span class="hide left-nav previous"><a href="javascript:;"><liferay-ui:message key="previous" /></a></span>

					<span class="hide page-info"></span>

					<span class="hide next right-nav"><a href="javascript:;"><liferay-ui:message key="next" /></a></span>
				</li>

				<div class="mark-all-as-read"><a class="hide" href="javascript:;" style="color:red !important;font-weight: bold!important;"><liferay-ui:message key="Mark all as read" /></a></div>

				<div class="user-notifications"></div>

				<li class="bottom clearfix pagination">
					<span class="hide left-nav previous"><a href="javascript:;"><liferay-ui:message key="previous" /></a></span>

					<span class="hide page-info"></span>

					<span class="hide next right-nav"><a href="javascript:;"><liferay-ui:message key="next" /></a></span>
				</li>
			</ul>

			<div class="hide notifications-configurations"></div>
		</aui:col>
	</aui:row>
</div>

<aui:script use="aui-base,liferay-plugin-notifications">
	var notificationsCount = '.non-actionable .count';

	if (<%= actionable %>) {
		notificationsCount = '.actionable .count'
	}

	var notificationsList = new Liferay.NotificationsList(
		{
			actionable: <%= actionable %>,
			baseActionURL: '<%= PortletURLFactoryUtil.create(request, portletDisplay.getId(), themeDisplay.getPlid(), PortletRequest.ACTION_PHASE) %>',
			baseRenderURL: '<%= PortletURLFactoryUtil.create(request, portletDisplay.getId(), themeDisplay.getPlid(), PortletRequest.RENDER_PHASE) %>',
			baseResourceURL: '<%= PortletURLFactoryUtil.create(request, portletDisplay.getId(), themeDisplay.getPlid(), PortletRequest.RESOURCE_PHASE) %>',
			delta: <%= fullViewDelta %>,
			fullView: <%= true %>,
			markAllAsReadNode: '.user-notifications-list .mark-all-as-read',
			namespace: '<portlet:namespace />',
			nextPageNode: '.pagination .next',
			notificationsContainer: '.notifications-portlet .user-notifications-container',
			notificationsCount: notificationsCount,
			notificationsNode: '.user-notifications-list .user-notifications',
			paginationInfoNode: '.pagination .page-info',
			previousPageNode: '.pagination .previous',
			portletKey: '<%= portletDisplay.getId() %>',
			start: 0
		}
	);

	new Liferay.Notifications(
		{
			baseRenderURL: '<%= PortletURLFactoryUtil.create(request, portletDisplay.getId(), themeDisplay.getPlid(), PortletRequest.RENDER_PHASE) %>',
			notificationsList: notificationsList
		}
	)
</aui:script>