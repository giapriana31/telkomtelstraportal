create table Notifications_UserNotificationEvent (
	notificationEventId bigint(20) not null primary key,
	companyId bigint(20),
	userId bigint(20),
	userNotificationEventId bigint(20),
	timestamp bigint(20),
	delivered tinyint(4),
	actionRequired tinyint(4),
	archived tinyint(4)
);