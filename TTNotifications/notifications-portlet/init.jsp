<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ page import="com.liferay.notifications.util.NotificationsUtil" %><%@
page import="com.liferay.notifications.util.PortletKeys" %><%@
page import="com.liferay.notifications.util.PortletPropsValues" %><%@
page import="com.liferay.notifications.util.comparator.PortletIdComparator" %><%@
page import="com.liferay.portal.kernel.notifications.UserNotificationDefinition" %><%@
page import="com.liferay.portal.kernel.notifications.UserNotificationDeliveryType" %><%@
page import="com.liferay.portal.kernel.notifications.UserNotificationManagerUtil" %><%@
page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %><%@
page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil" %><%@
page import="com.liferay.portal.kernel.util.ParamUtil" %><%@
page import="com.liferay.portal.kernel.util.StringPool" %><%@
page import="com.liferay.portal.model.Group" %><%@
page import="com.liferay.portal.model.GroupConstants" %><%@
page import="com.liferay.portal.model.LayoutConstants" %><%@
page import="com.liferay.portal.model.UserNotificationDelivery" %><%@
page import="com.liferay.portal.service.GroupLocalServiceUtil" %><%@
page import="com.liferay.portal.service.LayoutLocalServiceUtil" %><%@
page import="com.liferay.portal.service.UserNotificationDeliveryLocalServiceUtil" %><%@
page import="com.liferay.portal.service.UserNotificationEventLocalServiceUtil" %><%@
page import="com.liferay.portal.util.PortalUtil" %><%@
page import="com.liferay.portlet.PortletURLFactoryUtil" %><%@
page import="java.text.SimpleDateFormat" %><%@
page import="java.util.Date" %><%@
page import="java.text.Format" %><%@
page import="java.util.List" %><%@
page import="java.util.Map" %><%@
page import="java.util.TreeMap" %><%@
page import="javax.portlet.PortletRequest" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<%
int dockbarViewDelta = 5;
int fullViewDelta = 10;
Format simpleDateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat("dd/mm/yy hh:mm:ss", themeDisplay.getLocale(), themeDisplay.getTimeZone());
%>
