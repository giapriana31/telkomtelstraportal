<%@ page import="com.tt.roles.CustomerRole"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL id="saveImage" var="saveImage"></portlet:resourceURL>
<portlet:resourceURL id="DownloadFile" var="DownloadFile"></portlet:resourceURL>
<portlet:resourceURL id="SendEmail" var="SendEmail"></portlet:resourceURL>
<portlet:resourceURL id="ValidEmailURL" var="ValidEmailURL"></portlet:resourceURL>
<script src="${pageContext.request.contextPath}/js/canvg.js"></script>
<script src="${pageContext.request.contextPath}/js/rgbcolor.js" ></script>
<script src="${pageContext.request.contextPath}/js/StackBlur.js" ></script>
<script src="${pageContext.request.contextPath}/js/feedback_Bahasa.js"></script>
<script src="${pageContext.request.contextPath}/js/feedback_English.js" ></script>
<script src="${pageContext.request.contextPath}/js/htmltocanvas.js" ></script>
<script src="<%=request.getContextPath()%>/js/bootstrap-modal-2.3.2.js" type="text/javascript"></script>

<% CustomerRole roles = new CustomerRole (); %>

<script type="text/javascript">

var validEmailRes = "${ValidEmailURL}";
var saveURL  = "${saveImage}";
var downloadURL = "${DownloadFile}";
var emailURL = "${SendEmail}";
var ButtonEnabled = <%=roles.allowUserToPerformAction(request, response)%>;
var cssAttr = "<%= themeDisplay.getUser().getDisplayEmailAddress()%>";
var currentLanguage = "<%= themeDisplay.getUser().getLocale()%>";

if (currentLanguage=="in_ID") {
	$.feedback_bahasa({
		html2canvasURL : 'html2canvas.js',
		feedbackButton : '.feedback-btn',
		strokeStyle : 'red',
		shadowColor : 'red'
	});
} else {
	$.feedback_english({
		html2canvasURL : 'html2canvas.js',
		feedbackButton : '.feedback-btn',
		strokeStyle : 'red',
		shadowColor : 'red'
	});
}
	
</script>



<div style="position: fixed; top: 400px; right: 0px; z-index: 1060 !important">
	<%if(themeDisplay.getUser().getLocale().toString().equals("in_ID")){%>
		<a href="#" class="feedback-btn feedback-btn-gray" id="intersupportScreenBahasa"></a>
	<%}else{%>
		<a href="#" class="feedback-btn feedback-btn-gray" id="intersupportScreen"></a> 
	<%} %>
 	

</div>

<div class="modal fade" id="emailSuccessModal" data-backdrop="static" 
								data-keyboard="false">
								<div class="modal-header">
									<button type="button" class="close" id="closeSuccessPop" data-dismiss="modal"
										aria-hidden="true" style="border-radius:5px !important;">
										X
									</button>
									
								</div>
								<div class="modal-body">
								<div id="innerModalBody">
								<liferay-ui:message key="Your email has been sent successfully"/>
								</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="img-OK btn-primary-tt" id="OK"
										data-dismiss="modal" style="border-radius:5px !important;">
										OK
									</button>
								</div>
							</div>