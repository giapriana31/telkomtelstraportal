// feedback.js
// 2013, KÃ¡zmÃ©r Rapavi, https://github.com/ivoviz/feedback
// Licensed under the MIT license.
// Version 2.0

var nodesToReplace;
function svgToCanvas (targetElem) {
					
					var svgElem = targetElem.find('svg');
					nodesToReplace = [];
					svgElem.each(function(index, node) {
						var parentNode = node.parentNode;
						var svg = parentNode.innerHTML;

						var canvas = document.createElement('canvas');

						canvg(canvas, svg);
						nodesToReplace.push({
							parent: parentNode,
							svg: node,
							canvas: canvas
						});
					});
				}

(function($){

	$.feedback_bahasa = function(options) {

	    var settings = $.extend({
				ajaxURL: 				'',
				postBrowserInfo: 		true,
				postHTML:				false,
				postURL:				true,
				proxy:					undefined,
				letterRendering:		true,
				initButtonText: 		'Send feedback',
				strokeStyle:			'red',
				shadowColor:			'red',
				shadowOffsetX:			1,
				shadowOffsetY:			1,
				shadowBlur:				10,
				lineJoin:				'bevel',
				lineWidth:				3,
				html2canvasURL:			'html2canvas.js',
				feedbackButton: 		'.feedback-btn',
				showDescriptionModal: 	true,
				isDraggable: 			true,
				onScreenshotTaken: 		function(){},
				tpl: {
					description:	'<div id="feedback-welcome"><div class="feedback-logo">Masukan</div><p>Umpan memungkinkan Anda mengirim kami saran tentang produk kami. Kami menyambut laporan masalah, ide fitur dan komentar umum </p> <p> Mulailah dengan menulis deskripsi singkat:. </P><textarea id="feedback-note-tmp" placeholder="Tambahkan Deskripsi"></textarea><p>Selanjutnya kita akan membiarkan Anda mengidentifikasi area halaman yang terkait dengan deskripsi Anda.</p><button id="feedback-welcome-next" class="feedback-next-btn feedback-btn-gray">Next</button><div id="feedback-welcome-error">Silakan masuk ke deskripsi.</div><div class="feedback-wizard-close"></div></div>',
					highlighter:	'<div class="container-fluid" id="feedback-highlighter"><div class="row-fluid"><div class="span12"><div class="row-fluid feedback-logo"><div class="span11 tt-highLight-header">Layar Membagi</div><div class="span1 feedback-wizard-close"></div></div><div class="row-fluid"><div class="span12"><p>Klik dan tarik untuk memilih area.</p></div></div><div class="row-fluid"><div class="span12 highLight"><div class="feedback-sethighlight feedback-active" id="highlightDiv"><div id="iconHighDiv"><div class="icoHigh"></div></div><label class="upper">Sorot informasi penting.</label></div></div></div><div class="row-fluid"><div class="span12 blackOut"><div class="feedback-setblackout" id="blackoutDiv"><div id="iconBlackDiv"><div class="icoBlack"></div></div><label class="lower">Hitam informasi pribadi.</label></div></div></div><div class="row-fluid"><div class="span12"><div class="feedback-buttonsFirstPage"><div id="feedback-highlighter-next" class="feedback-next-btn feedback-btn-gray"><div class="nextIcon"></div><div class="nextLabel">Berikutnya</div></div><div id="feedback-highlighter-cancel" class="feedback-nextCancel-btn feedback-btn-gray"><div class="cancelIcon"></div><div class="canLabel">Membatalkan</div></div><button id="feedback-highlighter-back" class="feedback-back-btn feedback-btn-gray">Kembali</button></div></div></div></div></div></div>',
					overview:		'<div class="container-fluid" id="feedback-overview"><div class="row-fluid"><div class="span12"><div class="row-fluid feedback-logo"><div class="span11 tt-highLight-header">Layar Share</div><div class="span1 feedback-wizard-close"></div></div><div class="row-fluid tt-screenshare-modal"><div class="span12"><div class="row-fluid"> <div class="span6" id="feedback-overview-screenshot"><div class="screenShot">Screenshot</div></div><div class="span6" id="feedback-overview-description"><div class="row-fluid tt-row"><div class="span4" id="tt-title">Untuk:</div><div class="span8"><input id="feedBackTo"></div></div><div class="row-fluid tt-row"><div class="span4" id="tt-title">CC:</div><div class="span8"><input type="text" id="feedBackCC" value="'+cssAttr+'" readonly></div></div><div class="row-fluid tt-row"><div class="span4" id="tt-title"></div><div class="span8"><input type="checkbox" class="feedBackCCCheckClass" id="feedBackCCCheck" checked><span id="checkboxDescription">Mengirimkan saya salinan</span></input></div></div><div class="row-fluid tt-row"><div class="span4" id="tt-title">Subyek:</div><div class="span8"><input id="feedBackSubject"></div></div><div class="row-fluid tt-row"><div class="span12" id="feedback-overview-description-text"><h6></h6></div></div></div></div><div class="row-fluid tt-row"><div class="span12"><div id="feedback-overview-error" class="MandatoryError">Masukkan semua bidang.</div><div id="feedback-overview-error" class="EmailValidationError">Cukup masukkan alamat email yang valid.</div></div><div class="row-fluid tt-row-button"><div class="span6 tt-span-row"><div class="row-fluid"><div class="span6 tt-button-row"><div id="feedback-print" class="feedback-print-btn feedback-btn-blue"><div class="printIcon"></div><div class="printLabel">Mencetak</div></div></div><div class="span6 tt-button-row"><div id="feedback-email" class="feedback-email-btn feedback-btn-blue"><div class="sendMailIcon"></div><div class="sendMailLabel">Kirim Surat</div></div></div></div></div><div class="span6 tt-span-row"><div class="row-fluid"><div class="span6 tt-button-row"><div id="feedback-submit" class="feedback-submit-btn feedback-btn-blue"><div class="saveIcon"></div><div class="saveLabel">Simpan Gambar</div></div></div><div class="span6 tt-button-row"><div id="feedback-overview-back" class="feedback-back-btn feedback-btn-gray"><div class="cancelIcon"></div><div class="canLabel">Membatalkan</div></div></div></div></div></div></div></div></div></div></div></div>',
					submitSuccess:	'<div id="feedback-submit-success"><div class="feedback-logo">Masukan</div><p>Terima kasih atas tanggapan Anda. Kami menghargai setiap masukan yang kami terima.</p><p>Kami tidak bisa menanggapi secara individual untuk setiap orang, tapi kami akan menggunakan komentar Anda karena kami berusaha untuk meningkatkan pengalaman Anda.</p><button class="feedback-close-btn feedback-btn-blue">OK</button><div class="feedback-wizard-close"></div></div>',
					submitError:	'<div id="feedback-submit-error"><div class="feedback-logo">Masukan</div><p>Sayangnya kesalahan saat mengirim umpan balik Anda. Silakan coba lagi.</p><button class="feedback-close-btn feedback-btn-blue">OK</button><div class="feedback-wizard-close"></div></div>'
				},
				onClose: 				function() {},
				screenshotStroke:		true,
				highlightElement:		false,
				initialBox:				false
	    }, options);
			var supportedBrowser = !!window.HTMLCanvasElement;
			var isFeedbackButtonNative = settings.feedbackButton == '.feedback-btn';
			var _html2canvas = false;
			if (supportedBrowser) {
//				if(isFeedbackButtonNative) {
//					$('body').append('<button class="feedback-btn feedback-btn-gray">' + settings.initButtonText + '</button>');
//				}
				$(document).on('click', settings.feedbackButton, function(){
					if(isFeedbackButtonNative) {
						$(this).hide();
					}
					if (!_html2canvas) {
						$.getScript(settings.html2canvasURL, function() {
							_html2canvas = true;
						});
					}
					var canDraw = false,
						img = '',
						h 	= $(document).height(),
						w 	= $(document).width(),
						tpl = '<div id="feedback-module">';

					if (settings.initialBox) {
						tpl += settings.tpl.description;
					}

					tpl += settings.tpl.highlighter + settings.tpl.overview + '<canvas id="feedback-canvas"></canvas><div id="feedback-helpers"></div><input id="feedback-note" name="feedback-note" type="hidden"></div>';

					$('body').append(tpl);

					moduleStyle = {
						'position':	'absolute',
						'left': 	'0px',
						'top':		'0px'
					};
					canvasAttr = {
						'width': w,
						'height': h
					};

					$('#feedback-module').css(moduleStyle);
					$('#feedback-canvas').attr(canvasAttr).css('z-index', '30000');

					if (!settings.initialBox) {
						$('#feedback-highlighter-back').remove();
						canDraw = true;
						$('#feedback-canvas').css('cursor', 'crosshair');
						$('#feedback-helpers').show();
						$('#feedback-welcome').hide();
						$('#feedback-highlighter').show();
					}

					if(settings.isDraggable) {
						$('#feedback-highlighter').on('mousedown', function(e) {
							var $d = $(this).addClass('feedback-draggable'),
								drag_h 	= $d.outerHeight(),
								drag_w 	= $d.outerWidth(),
								pos_y 	= $d.offset().top + drag_h - e.pageY,
								pos_x 	= $d.offset().left + drag_w - e.pageX;
							$d.css('z-index', 40000).parents().on('mousemove', function(e) {
								_top 	= e.pageY + pos_y - drag_h;
								_left 	= e.pageX + pos_x - drag_w;
								_bottom = drag_h - e.pageY;
								_right 	= drag_w - e.pageX;

								if (_left < 0) _left = 0;
								if (_top < 0) _top = 0;
								if (_right > $(window).width())
									_left = $(window).width() - drag_w;
								if (_left > $(window).width() - drag_w)
									_left = $(window).width() - drag_w;
								if (_bottom > $(document).height())
									_top = $(document).height() - drag_h;
								if (_top > $(document).height() - drag_h)
									_top = $(document).height() - drag_h;

								$('.feedback-draggable').offset({
									top:	_top,
									left:	_left
								}).on("mouseup", function() {
									$(this).removeClass('feedback-draggable');
								});
							});
							e.preventDefault();
						}).on('mouseup', function(){
							$(this).removeClass('feedback-draggable');
							$(this).parents().off('mousemove mousedown');
						});
					}

					var ctx = $('#feedback-canvas')[0].getContext('2d');

					ctx.fillStyle = 'rgba(102,102,102,0.5)';
					ctx.fillRect(0, 0, $('#feedback-canvas').width(), $('#feedback-canvas').height());

					rect 		= {};
					drag 		= false;
					highlight 	= 1,
					post		= {};

					if (settings.postBrowserInfo) {
						post.browser 				= {};
						post.browser.appCodeName	= navigator.appCodeName;
						post.browser.appName		= navigator.appName;
						post.browser.appVersion		= navigator.appVersion;
						post.browser.cookieEnabled	= navigator.cookieEnabled;
						post.browser.onLine			= navigator.onLine;
						post.browser.platform		= navigator.platform;
						post.browser.userAgent		= navigator.userAgent;
						post.browser.plugins		= [];

						$.each(navigator.plugins, function(i) {
							post.browser.plugins.push(navigator.plugins[i].name);
						});
						$('#feedback-browser-info').show();
					}

					if (settings.postURL) {
						post.url = document.URL;
						$('#feedback-page-info').show();
					}

					if (settings.postHTML) {
						post.html = $('html').html();
						$('#feedback-page-structure').show();
					}

					if (!settings.postBrowserInfo && !settings.postURL && !settings.postHTML)
						$('#feedback-additional-none').show();

					$(document).on('mousedown', '#feedback-canvas', function(e) {
						if (canDraw) {

							rect.startX = e.pageX - $(this).offset().left;
							rect.startY = e.pageY - $(this).offset().top;
							rect.w = 0;
							rect.h = 0;
							drag = true;
						}
					});

					$(document).on('mouseup', function(){
						if (canDraw && drag) {
							drag = false;

							var dtop	= rect.startY,
								dleft	= rect.startX,
								dwidth	= rect.w,
								dheight	= rect.h;
								dtype	= 'highlight';

							if (dwidth == 0 || dheight == 0) return;

							if (dwidth < 0) {
								dleft 	+= dwidth;
								dwidth 	*= -1;
							}
							if (dheight < 0) {
								dtop 	+= dheight;
								dheight *= -1;
							}

							if (dtop + dheight > $(document).height())
								dheight = $(document).height() - dtop;
							if (dleft + dwidth > $(document).width())
								dwidth = $(document).width() - dleft;

							if (highlight == 0)
								dtype = 'blackout';

							$('#feedback-helpers').append('<div class="feedback-helper" data-type="' + dtype + '" data-time="' + Date.now() + '" style="position:absolute;top:' + dtop + 'px;left:' + dleft + 'px;width:' + dwidth + 'px;height:' + dheight + 'px;z-index:30000;"></div>');

							redraw(ctx);
							rect.w = 0;
						}

					});

					$(document).on('mousemove', function(e) {
						if (canDraw && drag) {
							$('#feedback-highlighter').css('cursor', 'default');

							rect.w = (e.pageX - $('#feedback-canvas').offset().left) - rect.startX;
							rect.h = (e.pageY - $('#feedback-canvas').offset().top) - rect.startY;

							ctx.clearRect(0, 0, $('#feedback-canvas').width(), $('#feedback-canvas').height());
							ctx.fillStyle = 'rgba(102,102,102,0.5)';
							ctx.fillRect(0, 0, $('#feedback-canvas').width(), $('#feedback-canvas').height());
							$('.feedback-helper').each(function() {
								if ($(this).attr('data-type') == 'highlight')
									drawlines(ctx, parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
							});
							if (highlight==1) {
								drawlines(ctx, rect.startX, rect.startY, rect.w, rect.h);
								ctx.clearRect(rect.startX, rect.startY, rect.w, rect.h);
							}
							$('.feedback-helper').each(function() {
								if ($(this).attr('data-type') == 'highlight')
									ctx.clearRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
							});
							$('.feedback-helper').each(function() {
								if ($(this).attr('data-type') == 'blackout') {
									ctx.fillStyle = 'rgba(0,0,0,1)';
									ctx.fillRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height())
								}
							});
							if (highlight == 0) {
								ctx.fillStyle = 'rgba(0,0,0,0.5)';
								ctx.fillRect(rect.startX, rect.startY, rect.w, rect.h);
							}
						}
					});

					if (settings.highlightElement) {
						var highlighted = [],
							tmpHighlighted = [],
							hidx = 0;

						$(document).on('mousemove click', '#feedback-canvas',function(e) {
							if (canDraw) {
								redraw(ctx);
								tmpHighlighted = [];

								$('#feedback-canvas').css('cursor', 'crosshair');

								$('* :not(body,script,iframe,div,section,.feedback-btn,#feedback-module *)').each(function(){
									if ($(this).attr('data-highlighted') === 'true')
										return;

									if (e.pageX > $(this).offset().left && e.pageX < $(this).offset().left + $(this).width() && e.pageY > $(this).offset().top + parseInt($(this).css('padding-top'), 10) && e.pageY < $(this).offset().top + $(this).height() + parseInt($(this).css('padding-top'), 10)) {
											tmpHighlighted.push($(this));
									}
								});

								var $toHighlight = tmpHighlighted[tmpHighlighted.length - 1];

								if ($toHighlight && !drag) {
									$('#feedback-canvas').css('cursor', 'pointer');

									var _x = $toHighlight.offset().left - 2,
										_y = $toHighlight.offset().top - 2,
										_w = $toHighlight.width() + parseInt($toHighlight.css('padding-left'), 10) + parseInt($toHighlight.css('padding-right'), 10) + 6,
										_h = $toHighlight.height() + parseInt($toHighlight.css('padding-top'), 10) + parseInt($toHighlight.css('padding-bottom'), 10) + 6;

									if (highlight == 1) {
										drawlines(ctx, _x, _y, _w, _h);
										ctx.clearRect(_x, _y, _w, _h);
										dtype = 'highlight';
									}

									$('.feedback-helper').each(function() {
										if ($(this).attr('data-type') == 'highlight')
											ctx.clearRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
									});

									if (highlight == 0) {
										dtype = 'blackout';
										ctx.fillStyle = 'rgba(0,0,0,0.5)';
										ctx.fillRect(_x, _y, _w, _h);
									}

									$('.feedback-helper').each(function() {
										if ($(this).attr('data-type') == 'blackout') {
											ctx.fillStyle = 'rgba(0,0,0,1)';
											ctx.fillRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
										}
									});

									if (e.type == 'click' && e.pageX == rect.startX && e.pageY == rect.startY) {
										$('#feedback-helpers').append('<div class="feedback-helper" data-highlight-id="' + hidx + '" data-type="' + dtype + '" data-time="' + Date.now() + '" style="position:absolute;top:' + _y + 'px;left:' + _x + 'px;width:' + _w + 'px;height:' + _h + 'px;z-index:30000;"></div>');
										highlighted.push(hidx);
										++hidx;
										redraw(ctx);
									}
								}
							}
						});
					}

					$(document).on('mouseleave', 'body,#feedback-canvas', function() {
						redraw(ctx);
					});

					$(document).on('mouseenter', '.feedback-helper', function() {
						redraw(ctx);
					});

					$(document).on('click', '#feedback-welcome-next', function() {
						if ($('#feedback-note').val().length > 0) {
							canDraw = true;
							$('#feedback-canvas').css('cursor', 'crosshair');
							$('#feedback-helpers').show();
							$('#feedback-welcome').hide();
							$('#feedback-highlighter').show();
						}
						else {
							$('#feedback-welcome-error').show();
						}
					});

					$(document).on('mouseenter mouseleave', '.feedback-helper', function(e) {
						if (drag)
							return;

						rect.w = 0;
						rect.h = 0;

						if (e.type === 'mouseenter') {
							$(this).css('z-index', '30001');
							$(this).append('<div class="feedback-helper-inner" style="width:' + ($(this).width() - 2) + 'px;height:' + ($(this).height() - 2) + 'px;position:absolute;margin:1px;"></div>');
							$(this).append('<div id="feedback-close"></div>');
							$(this).find('#feedback-close').css({
								'top' 	: -1 * ($(this).find('#feedback-close').height() / 2) + 'px',
								'left' 	: $(this).width() - ($(this).find('#feedback-close').width() / 2) + 'px'
							});

							if ($(this).attr('data-type') == 'blackout') {
								/* redraw white */
								ctx.clearRect(0, 0, $('#feedback-canvas').width(), $('#feedback-canvas').height());
								ctx.fillStyle = 'rgba(102,102,102,0.5)';
								ctx.fillRect(0, 0, $('#feedback-canvas').width(), $('#feedback-canvas').height());
								$('.feedback-helper').each(function() {
									if ($(this).attr('data-type') == 'highlight')
										drawlines(ctx, parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
								});
								$('.feedback-helper').each(function() {
									if ($(this).attr('data-type') == 'highlight')
										ctx.clearRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
								});

								ctx.clearRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height())
								ctx.fillStyle = 'rgba(0,0,0,0.75)';
								ctx.fillRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());

								ignore = $(this).attr('data-time');

								/* redraw black */
								$('.feedback-helper').each(function() {
									if ($(this).attr('data-time') == ignore)
										return true;
									if ($(this).attr('data-type') == 'blackout') {
										ctx.fillStyle = 'rgba(0,0,0,1)';
										ctx.fillRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height())
									}
								});
							}
						}
						else {
							$(this).css('z-index','30000');
							$(this).children().remove();
							if ($(this).attr('data-type') == 'blackout') {
								redraw(ctx);
							}
						}
					});

					$(document).on('click', '#feedback-close', function() {
						if (settings.highlightElement && $(this).parent().attr('data-highlight-id'))
							var _hidx = $(this).parent().attr('data-highlight-id');

						$(this).parent().remove();

						if (settings.highlightElement && _hidx)
							$('[data-highlight-id="' + _hidx + '"]').removeAttr('data-highlighted').removeAttr('data-highlight-id');

						redraw(ctx);
					});

					$('#feedback-module').on('click', '.feedback-wizard-close,.feedback-close-btn', function() {
						close();
					});

					$(document).on('keyup', function(e) {
						if (e.keyCode == 27)
							close();
					});

					$(document).on('selectstart dragstart', document, function(e) {
						e.preventDefault();
					});

					$(document).on('click', '#feedback-highlighter-back', function() {
						canDraw = false;
						$('#feedback-canvas').css('cursor', 'default');
						$('#feedback-helpers').hide();
						$('#feedback-highlighter').hide();
						$('#feedback-welcome-error').hide();
						$('#feedback-welcome').show();
					});

					$(document).on('mousedown', '.feedback-sethighlight', function() {
						highlight = 1;
						$(this).addClass('feedback-active');
						$('.feedback-setblackout').removeClass('feedback-active');
					});

					$(document).on('mousedown', '.feedback-setblackout', function() {
						highlight = 0;
						$(this).addClass('feedback-active');
						$('.feedback-sethighlight').removeClass('feedback-active');
					});
					
					$(document).on('click', '#feedback-highlighter-cancel', function() {
						canDraw = false;
						$('#feedback-helpers').hide();
						$('#feedback-highlighter').hide();
						$('#feedback-overview').hide();
						close();
					});

					$(document).on('click', '#feedback-highlighter-next', function() {
						canDraw = false;
						$('#feedback-canvas').css('cursor', 'default');
						var sy = $(document).scrollTop(),
							//dh = $(window).height();
							dh = $(document).height();
						$('#feedback-helpers').hide();
						$('#feedback-highlighter').hide();
						if (!settings.screenshotStroke) {
							redraw(ctx, false);
						}
					
					if(svgToCanvas)
						svgToCanvas($('body'));
					
					for (var i = 0; i < nodesToReplace.length;i++)		
					{
						var replacement = nodesToReplace[i];
						replacement.parent.removeChild(replacement.svg);
						replacement.parent.appendChild(replacement.canvas);
					}
					
					if($('.feedback-helper').length==0)
						redraw(ctx, false, true);
					
						html2canvas($('body'), {
							onrendered: function(canvas) {
								if (!settings.screenshotStroke) {
									redraw(ctx);
								}
								_canvas = $('<canvas id="feedback-canvas-tmp" width="'+ w +'" height="'+ dh +'"/>').hide().appendTo('body');
								_ctx = _canvas.get(0).getContext('2d');
								_ctx.drawImage(canvas, 0, sy, w, dh, 0, 0, w, dh);
								img = _canvas.get(0).toDataURL();
								$(document).scrollTop(sy);
								post.img = img;
								settings.onScreenshotTaken(post.img);
								if(settings.showDescriptionModal) {
									$('#feedback-canvas-tmp').remove();
									$('#feedback-overview').show();
									$('#feedback-overview-description-text>textarea').remove();
									$('#feedback-overview-screenshot>img').remove();
									$('<textarea id="feedback-overview-note" placeholder="Tambahkan Deskripsi">' + $('#feedback-note').val() + '</textarea>').insertAfter('#feedback-overview-description-text h6:eq(0)');
									$('#feedback-overview-screenshot').append('<img class="feedback-screenshot" src="' + img + '" />');
								}
								else {
									$('#feedback-module').remove();
									close();
									_canvas.remove();
								}
							},
							proxy: settings.proxy,
							letterRendering: settings.letterRendering
						});
						if(!ButtonEnabled)
						{																			
							$('#feedback-email').css('background-color', '#CCC');																
						}
					});
					
					
					
					$(document).on('keydown','#feedBackTo',(function(e){

						
					    if (e.keyCode == 65 && e.ctrlKey) {
					        e.target.select()
					    }

					}));
													
													

													$(document).on('keydown','#feedBackSubject',(function(e){

						
					    if (e.keyCode == 65 && e.ctrlKey) {
					        e.target.select()
					    }

					}));
													

													$(document).on('keydown','#feedback-overview-note',(function(e){

						
					    if (e.keyCode == 65 && e.ctrlKey) {
					        e.target.select()
					    }

					}));
					
					

					$(document).on('click', '#feedback-overview-back', function(e) {
						canDraw = true;
						$('#feedback-canvas').css('cursor', 'crosshair');
						$('#feedback-overview').hide();
						$('#feedback-helpers').show();
						$('#feedback-highlighter').show();
						$('#feedback-overview-error').hide();
					});

					$(document).on('keyup', '#feedback-note-tmp,#feedback-overview-note', function(e) {
						var tx;
						if (e.target.id === 'feedback-note-tmp')
							tx = $('#feedback-note-tmp').val();
						else {
							tx = $('#feedback-overview-note').val();
							$('#feedback-note-tmp').val(tx);
						}

						$('#feedback-note').val(tx);
					});

					$(document).on('click', '#feedback-submit', function() {
						canDraw = false;
				
						$('#feedback-submit-success,#feedback-submit-error').remove();
						$('#feedback-overview').hide();
						var feedBackToVar = $('#feedBackTo').val();
						var feedBackSubVar = $('#feedBackSubject').val();
						var feedBackNoteVar = $('#feedback-note').val();
						post.img = img;
						
		                var data = {feedback: JSON.stringify(post)};
		                $.ajax({
							url: saveURL,
							dataType: 'json',
							type: 'POST',
							data: data,
							success: function() {
								close();

								var $a = $('<iframe>', {
							        src: downloadURL,
							        id:  'myFrame',
							        frameborder: 0,
							        scrolling: 'no'
							        
							    });

							$(document.body).append($a);},
							error: function(){
								close();

								var $a = $('<iframe>', {
							        src: downloadURL,
							        id:  'myFrame',
							        frameborder: 0,
							        scrolling: 'no'
							        
							    });

							$(document.body).append($a);}
						});
					
					});
					
					$(document).on('click', '#feedback-email', function() {
						
						if(!ButtonEnabled)
							{													
								return null;
							}
						$('#feedback-overview-error').hide();
						canDraw = false;
						if ($('#feedback-note').val().length > 0 && $('#feedBackTo').val().length > 0 && $('#feedBackSubject').val().length > 0) {
							
							var feedBackToVar = $('#feedBackTo').val();
							var feedBackSubVar = $('#feedBackSubject').val();
							var feedBackNoteVar = $('#feedback-note').val();
							var feedBackSubmitMsgVar = "Your email has been sent successfully";	// Added by Ganesh Nashikar for defect# 6
							postData= {};
							postData.img = img;
							postData.emailFrom = cssAttr;
							postData.note = $('#feedback-note').val();
							postData.emailTo = feedBackToVar;
							postData.emailSub = feedBackSubVar;
							if($("#feedBackCCCheck").is(':checked')){
								postData.CCEmailAdd = cssAttr;
							}
							var emailData = {feedbackEmail: JSON.stringify(postData)};
							$.ajax({
								url: validEmailRes,
								dataType: 'json',
								type: 'POST',
								data: emailData,
								success: function(data) {
									if(data[0].result==false){
										$('.EmailValidationError').show();
									} else if(data[0].result==true){
										$('#feedback-submit-success,#feedback-submit-error,#feedback-email').remove();
										$('#feedback-overview').hide();
										//alert(feedBackSubmitMsgVar); // Added by Ganesh Nashikar for defect#6
										sendMail(emailData);
									}
								},
								error: function(){
									$('#feedback-submit-success,#feedback-submit-error,#feedback-email').remove();
									$('#feedback-overview').hide();
									close();
									location.reload();}
							});
						}
						else {
							$('.MandatoryError').show();
						}
					});
					
					

					$("#feedback-print").click(function(){
	 
												$('#feedback-overview-error')
														.hide();
												canDraw = false;
												printImage(img);
	});
				});
			}
			
			function sendMail(emailData){
				$.ajax({
					url: emailURL,
					dataType: 'json',
					type: 'POST',
					data: emailData,
					success: function() {
						$('#emailSuccessModal').show();
							$('#emailSuccessModal').modal('show');
				
					close();
							$('#emailSuccessModal').on('hidden.bs.modal', function () {
								
								/*location.reload();*/
								
							})
						
						/*close();
						location.reload();
					*/	},
					error: function(){
						$('#emailSuccessModal').show();
							$('#emailSuccessModal').modal('show');
						
							
						close();
					//	location.reload();
						$('#emailSuccessModal').on('hidden.bs.modal', function () {
							
							/*location.reload();*/
							
						})
					/*	close();
						location.reload();
				*/		
					}
				});
			}
			
			function printImage(img)
			{
			        var printWindow = window.open('', 'Print Window','height=700,width=900');
			        printWindow.document.write('<html><head><title>Print Window</title>');
			        printWindow.document.write('</head><body ><img src=\'');
			        printWindow.document.write(img);
			        printWindow.document.write('\' /></body></html>');
			        printWindow.document.close();
			        printWindow.print();
			        printWindow.close();
			}
			
			
			function close() {
				canDraw = false;
				$(document).off('mouseenter mouseleave', '.feedback-helper');
				$(document).off('mouseup keyup');
				$(document).off('mousedown', '.feedback-setblackout');
				$(document).off('mousedown', '.feedback-sethighlight');
				$(document).off('mousedown click', '#feedback-close');
				$(document).off('mousedown', '#feedback-canvas');
				$(document).off('click', '#feedback-highlighter-next');
				$(document).off('click', '#feedback-highlighter-back');
				$(document).off('click', '#feedback-welcome-next');
				$(document).off('click', '#feedback-overview-back');
				$(document).off('mouseleave', 'body');
				$(document).off('mouseenter', '.feedback-helper');
				$(document).off('selectstart dragstart', document);
				$('#feedback-module').off('click', '.feedback-wizard-close,.feedback-close-btn');
				$(document).off('click', '#feedback-submit');

				if (settings.highlightElement) {
					$(document).off('click', '#feedback-canvas');
					$(document).off('mousemove', '#feedback-canvas');
				}
				$('[data-highlighted="true"]').removeAttr('data-highlight-id').removeAttr('data-highlighted');
				$('#feedback-module').remove();
				$('.feedback-btn').show();

			for (var i = 0; i < nodesToReplace.length;i++)		
			{
				var replacement = nodesToReplace[i];
				replacement.parent.removeChild(replacement.canvas);
				replacement.parent.appendChild(replacement.svg);
			}
				settings.onClose.call(this);
			}

			function redraw(ctx, border, noOverlay) {
				border = typeof border !== 'undefined' ? border : true;
				ctx.clearRect(0, 0, $('#feedback-canvas').width(), $('#feedback-canvas').height());
				if(noOverlay)
					ctx.fillStyle = 'rgba(102,102,102,0)';
				else
					ctx.fillStyle = 'rgba(102,102,102,0.5)';
				ctx.fillRect(0, 0, $('#feedback-canvas').width(), $('#feedback-canvas').height());
				$('.feedback-helper').each(function() {
					if ($(this).attr('data-type') == 'highlight')
						if (border)
							drawlines(ctx, parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
				});
				$('.feedback-helper').each(function() {
					if ($(this).attr('data-type') == 'highlight')
						ctx.clearRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
				});
				$('.feedback-helper').each(function() {
					if ($(this).attr('data-type') == 'blackout') {
						ctx.fillStyle = 'rgba(0,0,0,1)';
						ctx.fillRect(parseInt($(this).css('left'), 10), parseInt($(this).css('top'), 10), $(this).width(), $(this).height());
					}
				});
			}

			function drawlines(ctx, x, y, w, h) {
				ctx.strokeStyle		= settings.strokeStyle;
				ctx.shadowColor		= settings.shadowColor;
				ctx.shadowOffsetX	= settings.shadowOffsetX;
				ctx.shadowOffsetY	= settings.shadowOffsetY;
				ctx.shadowBlur		= settings.shadowBlur;
				ctx.lineJoin		= settings.lineJoin;
				ctx.lineWidth		= settings.lineWidth;

				ctx.strokeRect(x,y,w,h);

				ctx.shadowOffsetX	= 0;
				ctx.shadowOffsetY	= 0;
				ctx.shadowBlur		= 0;
				ctx.lineWidth		= 1;
			}

};

}(jQuery));


$('#feedBackTo').keydown(function(e){
	
    if (e.keyCode == 65 && e.ctrlKey) {
        e.target.select()
    }

});


$('#feedBackSubject').keydown(function(e){
	
    if (e.keyCode == 65 && e.ctrlKey) {
        e.target.select()
    }

});


$('#feedback-overview-note').keydown(function(e){
	
    if (e.keyCode == 65 && e.ctrlKey) {
        e.target.select()
    }

});


$(document).ready(function(){

	$('#emailSuccessModal').hide();
	 $("#closeSuccessPop").click(function(){

		 $('#emailSuccessModal').hide();
	    });
	 
	 $("#OK").click(function(){
		 $('#emailSuccessModal').hide();
	    });
  
});