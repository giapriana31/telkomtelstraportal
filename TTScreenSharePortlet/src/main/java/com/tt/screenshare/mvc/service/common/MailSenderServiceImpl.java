package com.tt.screenshare.mvc.service.common;

import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.tt.logging.Logger;

import com.tt.screenshare.mvc.web.controller.SaveImageController;

@Service
public class MailSenderServiceImpl {
	private final static Logger log = Logger.getLogger(SaveImageController.class);
	private JavaMailSender mailSender;
	private SimpleMailMessage simpleMailMessage;

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void sendMail(String from, String to, String subject, String msg) {
		log.debug("MailSenderServiceImpl * sendMail * Inside the method");
		try{
		simpleMailMessage.setFrom(from);
		simpleMailMessage.setTo(to);
		simpleMailMessage.setSubject(subject);
		simpleMailMessage.setText(msg);

		mailSender.send(simpleMailMessage);
		}catch (Exception e) {
			 log.error("Exception in sendMail method "+e.getMessage());
		}
		log.debug("MailSenderServiceImpl * sendMail * Exit the method");
	}

	public void sendMailWithAttachment(String from, List<String> to, String subject,
			String msg, String ccEmailId, Map<String, Object> attachment) {
		log.debug("MailSenderServiceImpl * sendMailWithAttachment * Inside the method");
		MimeMessage message = mailSender.createMimeMessage();
		log.debug("MailSenderServiceImpl * sendMailWithAttachment * message-----"+message);
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			String[] emailTo = to.toArray(new String[to.size()]);
			helper.setFrom(from);
			helper.setTo(emailTo);
			helper.setSubject(subject);
			helper.setText(msg);
			log.debug("MailSenderServiceImpl * sendMailWithAttachment * ccEmailId-------"+ccEmailId);
			log.debug("MailSenderServiceImpl * sendMailWithAttachment * ccEmailId.length()--------"+ccEmailId.length());
			if (ccEmailId != null && ccEmailId.length() > 0) {
				helper.setCc(ccEmailId);
			}
			
			String fileName = (String) attachment.get("attachmentFilename");
			log.debug("MailSenderServiceImpl * sendMailWithAttachment * fileName" +fileName);
			byte[] imageByte = (byte[]) attachment.get("byteArray");
			log.debug("MailSenderServiceImpl * sendMailWithAttachment * imageByte" +imageByte);
			InputStreamSource fileStream = new ByteArrayResource(imageByte);
			
			String contentType = (String) attachment.get("contentType");
			log.debug("MailSenderServiceImpl * sendMailWithAttachment * contentType" +contentType);
			helper.addAttachment(fileName, fileStream, contentType);

		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
		log.debug("MailSenderServiceImpl * sendMailWithAttachment * Exit the method");
	}

}