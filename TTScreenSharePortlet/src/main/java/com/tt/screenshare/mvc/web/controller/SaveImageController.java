package com.tt.screenshare.mvc.web.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.tt.logging.Logger;
import com.tt.utils.PropertyReader;
import sun.misc.BASE64Decoder;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.tt.screenshare.mvc.service.common.MailSenderServiceImpl;
import com.tt.utils.PropertyReader;

@Controller("saveImageController")
@RequestMapping("VIEW")
public class SaveImageController {
	private final static Logger log = Logger.getLogger(SaveImageController.class);
	private MailSenderServiceImpl mailSenderServiceImpl;
	public static Properties prop = PropertyReader.getProperties();

	public void setMailSenderServiceImpl(MailSenderServiceImpl mailSenderServiceImpl) 
	{
		this.mailSenderServiceImpl = mailSenderServiceImpl;
	}

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse renderResponse, Model model) throws JSONException 
	{
		return "view";
	}

	@ResourceMapping(value = "saveImage")
	public void saveImage(ResourceRequest req, ResourceResponse res) 
	{
		log.debug("SaveImageController * saveImage * Inside the method");
		HttpServletRequest request = PortalUtil.getHttpServletRequest(req);	
	
		try 
		{
			byte[] imageByte = null;
			BufferedImage image = null;
			BASE64Decoder decoder = new BASE64Decoder();
			JSONObject obj = new JSONObject(request.getParameter("feedback"));
			
			try 
			{
				log.info("SaveImageController * saveImage * In controller try");
				imageByte = decoder.decodeBuffer(obj.getString("img").replace("data:image/png;base64,", ""));
				ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
				image = ImageIO.read(bis);
				bis.close();
				
				File outputfile = new File(prop.getProperty("screenshot.image.path")+prop.getProperty("screenshot.image.prefix"));
				ImageIO.write(image, "png", outputfile);
				request.getSession().setAttribute("path", outputfile.getPath());
				
			} catch (Exception p) {
				log.error("Exception in saveImage method" +p.getMessage());
			}
			
		} catch (Exception e) {
			log.error("Exception in saveImage method" +e.getMessage());
		   
		}
		log.debug("SaveImageController * saveImage * Exit the method");
	}

	@ResourceMapping(value = "DownloadFile")
	public void downloadFile(ResourceRequest req, ResourceResponse res)throws Exception 
	{
		log.debug("SaveImageController * downloadFile * Inside the method");
		try{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss");
		Calendar cal = new GregorianCalendar();		
		cal.setTimeZone(cal.getTimeZone());		
		String currentDate=sdf.format(cal.getTime());
		
		HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
		HttpServletResponse response = PortalUtil.getHttpServletResponse(res);
		
		File downloadFile = new File((String) request.getSession().getAttribute("path"));		
		FileInputStream inStream = new FileInputStream(downloadFile);

		response.setContentType("image/png");
		response.setHeader("Content-Disposition",String.format("attachment; filename=\"%s\"",downloadFile.getName()+currentDate+".png"));
		response.setContentLength((int) downloadFile.length());

		OutputStream outStream = response.getOutputStream();

		int bytesRead = -1;
		byte[] buffer = new byte[4096];

		while ((bytesRead = inStream.read(buffer)) != -1) 
			outStream.write(buffer, 0, bytesRead);
		
		inStream.close();
		outStream.close();
		response.getOutputStream().flush();
		response.getOutputStream().close();

	}catch (Exception e) {
		log.error("Exception in downloadFile method" +e.getMessage());
	}
	log.debug("SaveImageController * downloadFile * Exit the method");
	}

	
	@ResourceMapping(value = "ValidEmailURL")
	public void validEmail(ResourceRequest req, ResourceResponse res) {
		log.debug("SaveImageController * validEmail * Inside the method");
		HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
		HttpServletResponse response = PortalUtil.getHttpServletResponse(res);
		
		com.liferay.portal.kernel.json.JSONArray resultJsonArray=JSONFactoryUtil.createJSONArray();
		com.liferay.portal.kernel.json.JSONObject jsonValidate=JSONFactoryUtil.createJSONObject();;
		try {
			JSONObject obj = new JSONObject(request.getParameter("feedbackEmail"));
			String emailStr =  obj.getString("emailTo");
			String[] emailArray = emailStr.split(";") ;
			List<String> emailList = new ArrayList<String>();
			Boolean isValid = true;
			for (String email : emailArray) {
				log.debug("SaveImageController * validEmail * email"+email);
				log.debug("SaveImageController * validEmail * email.trim().length()"+email.trim().length());
				if(email!=null && email.trim().length()>0){
					emailList.add(email.trim());
				}
			}
			
			for (String email : emailList) {
				if(!email.trim().contains("@")){
					isValid = false;
					break;
				}else if(email.contains("@")){
					String[] emailSub = email.trim().split("@");
					log.debug("emailSub.length"+emailSub.length);
					if(emailSub.length>1){
						isValid=true;
					}else{
						isValid = false;
						break;
					}
				}
			}
			
			if(!isValid){
				log.info("false");
				jsonValidate.put("result",false);
			}else{
				log.info("true");	
				jsonValidate.put("result",true);
			}
		} catch (Exception e) {
			log.error("Exception in validEmail method" +e.getMessage());
		}
		resultJsonArray.put(jsonValidate);
		log.debug(resultJsonArray.toString());
		PrintWriter out;
		try {
			out = response.getWriter();
			log.debug(resultJsonArray.toString());
		} catch (IOException e) {
			log.error("Exception in validEmail method" +e.getMessage());
		}
		log.debug("SaveImageController * validEmail * Exit the method");
	}
	
	@SuppressWarnings("restriction")
	@ResourceMapping(value = "SendEmail")
	public void sendEmail(ResourceRequest req, ResourceResponse res) {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(req);
		
		log.debug("SaveImageController * sendEmail * Inside the method");
		try {
			byte[] imageByte = null;
			BASE64Decoder decoder = new BASE64Decoder();
			JSONObject obj = new JSONObject(request.getParameter("feedbackEmail"));
			String emailFrom = obj.getString("emailFrom");
			String emailDescription = obj.getString("note");
			String emailTo =  obj.getString("emailTo");
			String emailSubject =  obj.getString("emailSub");
			String ccEmailId = null;
			
			if(obj.has("CCEmailAdd")) {
				ccEmailId = obj.getString("CCEmailAdd");
				log.debug("SaveImageController * sendEmail * ccEmailId"+ccEmailId);
			}
			
			
			try {
				
				imageByte = decoder.decodeBuffer(obj.getString("img").replace(
						"data:image/png;base64,", ""));
			} catch (Exception p) {
				log.error("Exception in sendEmail method" +p.getMessage());
			}
			
			sentMail(emailFrom, emailTo, emailSubject, emailDescription, ccEmailId, imageByte);
			
		} catch (Exception e) {
			log.error("Exception in sendEmail method" +e.getMessage());		   
		}
		log.debug("SaveImageController * sendEmail * Exit the method");
	}
	
	
	public void sentMail(String emailFrom, String emailTo, String emailSubject, String emailDescription, String ccEmailId,  byte[] imageByte) {
		
		log.debug("SaveImageController * sentMail * Inside the method");
		try{
			log.debug("SaveImageController * sentMail * ccEmailId"+ccEmailId);
		String from = emailFrom;
		Map<String, Object> attachmentMap = new HashMap<String, Object>();
		attachmentMap.put("attachmentFilename", "ScreenShare.png");
		attachmentMap.put("byteArray", imageByte);
		attachmentMap.put("contentType", "image/png");
		
		List<String> emailToList ;
		log.debug("SaveImageController * sentMail * emailTo"+emailTo);
		if(emailTo!=null  && emailTo.contains(";")){
			String[] emailArray = emailTo.split(";") ;
			 emailToList = new ArrayList<String>();
			 log.debug("SaveImageController * sentMail * emailToList--------------->>"+ emailToList == null ? "NULL" : emailToList.toString());
			for (String email : emailArray) {
				log.debug("SaveImageController * sentMail * email"+email);
				if(email!=null && email.trim().length()>0){
					emailToList.add(email.trim());
				}
			}
		}else{
			emailToList = new ArrayList<String>();
			emailToList.add(emailTo.trim());
		
		}
		
		mailSenderServiceImpl.sendMailWithAttachment(from, emailToList, emailSubject, emailDescription, ccEmailId, attachmentMap);
	}catch (Exception e) {
		log.error("Exception in sentMail method" +e.getMessage());	
	   
	}
	log.debug("SaveImageController * sentMail * Exit the method");
}

}
