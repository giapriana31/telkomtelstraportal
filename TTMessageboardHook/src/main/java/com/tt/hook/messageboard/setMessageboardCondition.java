package com.tt.hook.messageboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

import com.tt.logging.Logger;

public class setMessageboardCondition extends BaseStrutsAction 
{
	private final static Logger log = Logger.getLogger(setMessageboardCondition.class);
	
    public String execute(HttpServletRequest request,HttpServletResponse response) throws Exception 
    {
    	log.info("setMessageboardCondition.java -> in execute()");
    	try
    	{			
			String pref = request.getParameter("preference").toString();		
			String email = request.getParameter("email").toString();
			String companyID = request.getParameter("companyID").toString();			
			
			log.debug("setMessageboardCondition.java -> preference = "+ pref);
			log.debug("setMessageboardCondition.java -> email = "+ email);
			log.debug("setMessageboardCondition.java -> companyID = "+ companyID);
			
			if(pref!=null && email!=null && companyID!=null)
			{
				User user = UserLocalServiceUtil.getUserByEmailAddress(Long.parseLong(companyID, 10), email);
				log.debug("setMessageboardCondition.java -> User = "+ user==null?"null":user.getEmailAddress());
				
				if(pref!=null && !pref.equals(""))
				{					
					if(pref.equalsIgnoreCase("true"))
						user.getExpandoBridge().setAttribute("Messageboardcondition", "true", false);
					else
						user.getExpandoBridge().setAttribute("Messageboardcondition", "false", false);			
				}
			}
			return "";	
    	}
    	catch(Exception e)
    	{
    		log.debug("setMessageboardCondition.java -> Error "+e.toString());
    		return "";
    	}
    	
    }
}
