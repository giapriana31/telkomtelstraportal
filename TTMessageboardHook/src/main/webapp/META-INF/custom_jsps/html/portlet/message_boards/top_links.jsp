<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<%@ include file="/html/portlet/message_boards/init.jsp" %> 

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

  <link rel="stylesheet" href="/TTPortalTheme/css/bootstrap.min.css">
  <script src="/TTPortalTheme/js/jquery-1.11.3.min.js" type="text/javascript"></script>
  <script src="/TTPortalTheme/js/bootstrap.min.js" type="text/javascript"></script>

<!--
<script src="/TTPortalTheme/js/aui-min.js" type="text/javascript"></script>

<script type="text/javascript">

YUI().use(
		  'aui-dropdown',
		  function(Y) {
		    new Y.Dropdown(
		      {
		        boundingBox: '#myDropdown',
		        trigger: '#myTrigger'
		      }
		    ).render();
		  }
		);
</script>

//-->
<%
String topLink = ParamUtil.getString(request, "topLink", "message-boards-home");

MBCategory category = (MBCategory)request.getAttribute(WebKeys.MESSAGE_BOARDS_CATEGORY);

long categoryId = MBUtil.getCategoryId(request, category);

boolean viewCategory = GetterUtil.getBoolean((String)request.getAttribute("view.jsp-viewCategory"));

PortletURL portletURL = renderResponse.createRenderURL();

portletURL.setParameter("struts_action", "/message_boards/view");
%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

       <aui:nav-bar> 
       

<div id="myDropdown" class="dropdown" >
         <button id="myTrigger" class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><liferay-ui:message key="Discussion Boards"/><span class="caret"></span>
         </button>

        <!-- Dropdown -->
        <ul class="dropdown-menu" aria-labelledby="myTrigger">
                
           
		<%
		String label = "message-boards-home";

		portletURL.setParameter("topLink", label);
		portletURL.setParameter("tag", StringPool.BLANK);
		%>

		 <li><a tabindex="-1"  href="<%= portletURL.toString() %>" label="<%= label %>" selected="<%= topLink.equals(label) %>"><liferay-ui:message key="Discussion Boards"/></a></li>

		<%
		label = "recent-posts";

		portletURL.setParameter("topLink", label);
		%>

		<li><a tabindex="-1"  href="<%= portletURL.toString() %>" label="<%= label %>" selected="<%= topLink.equals(label) %>" ><liferay-ui:message key="Recent Posts"/></a></li>

		<c:if test="<%= themeDisplay.isSignedIn() && !portletName.equals(PortletKeys.MESSAGE_BOARDS_ADMIN) %>">

			<%
			label = "my-posts";

			portletURL.setParameter("topLink", label);
			%>

		     <li><a tabindex="-1"  href="<%= portletURL.toString() %>" label="<%= label %>" selected="<%= topLink.equals(label) %>" ><liferay-ui:message key="My Posts"/></a></li>

			<c:if test="<%= MBUtil.getEmailMessageAddedEnabled(portletPreferences) || MBUtil.getEmailMessageUpdatedEnabled(portletPreferences) %>">

				<%
				label = "my-subscriptions";

				portletURL.setParameter("topLink", label);
				%>

				     <li><a tabindex="-1" href="<%= portletURL.toString() %>" label="<%= label %>" selected="<%= topLink.equals(label) %>" ><liferay-ui:message key="My Subscriptions"/></a></li>
			</c:if>
		</c:if>

		<%
		label = "statistics";

		portletURL.setParameter("topLink", label);
		%>

	<!--	<aui:nav-item cssClass='<%= topLink.equals(label) ? "active" : StringPool.BLANK %>' href="<%= portletURL.toString() %>" label="<%= label %>" selected="<%= topLink.equals(label) %>" />-->
		<!--		<liferay-ui:input-search id="keywords1" showButton ="true" placeholder ="" /> -->
		<c:if test="<%= MBPermission.contains(permissionChecker, scopeGroupId, ActionKeys.BAN_USER) %>">

			<%
			label = "banned-users";

			portletURL.setParameter("topLink", label);
			%>

			     <li><a tabindex="-1"  href="<%= portletURL.toString() %>" label="<%= label %>" selected="<%= topLink.equals(label) %>" ><liferay-ui:message key="Banned Users"/></a></li>
		</c:if>
	 </ul>
 </div>
</div>



<div id="search">
	<c:if test="<%= showSearch %>">
		<liferay-portlet:renderURL varImpl="searchURL">
			<portlet:param name="struts_action" value="/message_boards/search" />
		</liferay-portlet:renderURL>

		<aui:nav-bar-search cssClass="pull-right">
			<div class="form-search">
				<aui:form action="<%= searchURL %>" method="get" name="searchFm">
					<liferay-portlet:renderURLParams varImpl="searchURL" />
					<aui:input name="redirect" type="hidden" value="<%= currentURL %>" />
					<aui:input name="breadcrumbsCategoryId" type="hidden" value="<%= categoryId %>" />
					<aui:input name="searchCategoryId" type="hidden" value="<%= categoryId %>" />

		
					<aui:input inlineField="<%= true %>" label="" name="keywords1" title="search-categories" type="text"  />

					<aui:button type="button" class="search-button" icon="icon-search"  onClick="submit();">
					
					</aui:button>
				</aui:form>
			</div>
		</aui:nav-bar-search>

		<c:if test="<%= windowState.equals(WindowState.MAXIMIZED) && !themeDisplay.isFacebook() %>">
			<aui:script>
				Liferay.Util.focusFormField(document.getElementById('<portlet:namespace />keywords1'));
			</aui:script>
		</c:if>
	</c:if>
</div>
</aui:nav-bar>



<c:if test="<%= layout.isTypeControlPanel() %>">
	<div id="breadcrumb">
		<liferay-ui:breadcrumb showCurrentGroup="<%= false %>" showCurrentPortlet="<%= false %>" showGuestGroup="<%= false %>" showLayout="<%= false %>" showPortletBreadcrumb="<%= true %>" />
	</div>
</c:if>
