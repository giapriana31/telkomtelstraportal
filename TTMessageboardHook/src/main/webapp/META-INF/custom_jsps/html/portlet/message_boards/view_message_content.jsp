<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */
--%>

<%@ include file="/html/portlet/message_boards/init.jsp" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
String redirect = ParamUtil.getString(request, "redirect");

MBMessageDisplay messageDisplay = (MBMessageDisplay)request.getAttribute(WebKeys.MESSAGE_BOARDS_MESSAGE);

MBMessage message = messageDisplay.getMessage();

MBCategory category = messageDisplay.getCategory();

MBThread thread = messageDisplay.getThread();

MBThread previousThread = messageDisplay.getPreviousThread();
MBThread nextThread = messageDisplay.getNextThread();

String threadView = messageDisplay.getThreadView();

MBThreadFlag threadFlag = MBThreadFlagLocalServiceUtil.getThreadFlag(themeDisplay.getUserId(), thread);
%>

<c:choose>
	<c:when test="<%= Validator.isNull(redirect) %>">
		<portlet:renderURL var="backURL">
			<portlet:param name="struts_action" value="/message_boards/view" />
			<portlet:param name="mbCategoryId" value="<%= (category != null) ? String.valueOf(category.getCategoryId()) : String.valueOf(MBCategoryConstants.DEFAULT_PARENT_CATEGORY_ID) %>" />
		</portlet:renderURL>

		<liferay-ui:header
			backLabel='<%= (category != null) ? category.getName() : "message-boards-home" %>'
			backURL="<%= backURL.toString() %>"
			localizeTitle="<%= false %>"
			title="<%= message.getSubject() %>"
		/>
	</c:when>
	<c:otherwise>
		<liferay-ui:header
			backURL="<%= redirect %>"
			localizeTitle="<%= false %>"
			title="<%= message.getSubject() %>"
		/>
	</c:otherwise>
</c:choose>


<!-- Code commented : thread-view-controls not to be shown on UI. 
<ul class="thread-view-controls">
	<c:if test="<%= PropsValues.MESSAGE_BOARDS_THREAD_VIEWS.length > 1 %>">
		<c:if test="<%= ArrayUtil.contains(PropsValues.MESSAGE_BOARDS_THREAD_VIEWS, MBThreadConstants.THREAD_VIEW_COMBINATION) %>">
			<li class="thread-icon">

				<%
				currentURLObj.setParameter("threadView", MBThreadConstants.THREAD_VIEW_COMBINATION);
				%>

				<liferay-ui:icon
					image="../message_boards/thread_view_combination"
					message="combination-view"
					method="get"
					url="<%= currentURLObj.toString() %>"
				/>
			</li>
		</c:if>

		<c:if test="<%= ArrayUtil.contains(PropsValues.MESSAGE_BOARDS_THREAD_VIEWS, MBThreadConstants.THREAD_VIEW_FLAT) %>">
			<li class="thread-icon">

				<%
				currentURLObj.setParameter("threadView", MBThreadConstants.THREAD_VIEW_FLAT);
				%>

				<liferay-ui:icon
					image="../message_boards/thread_view_flat"
					message="flat-view"
					method="get"
					url="<%= currentURLObj.toString() %>"
				/>
			</li>
		</c:if>

		<c:if test="<%= ArrayUtil.contains(PropsValues.MESSAGE_BOARDS_THREAD_VIEWS, MBThreadConstants.THREAD_VIEW_TREE) %>">
			<li class="thread-icon">

				<%
				currentURLObj.setParameter("threadView", MBThreadConstants.THREAD_VIEW_TREE);
				%>

				<liferay-ui:icon
					image="../message_boards/thread_view_tree"
					message="tree-view"
					method="get"
					url="<%= currentURLObj.toString() %>"
				/>
			</li>
		</c:if>
	</c:if>
</ul>

-->


<!-- 
Changes done by ANAND : Start
 -->
 <div class = "subandUnsub">
<c:if test="<%= MBMessagePermission.contains(permissionChecker, message, ActionKeys.SUBSCRIBE) && (MBUtil.getEmailMessageAddedEnabled(portletPreferences) || MBUtil.getEmailMessageUpdatedEnabled(portletPreferences)) %>">
				<c:choose>
					<c:when test="<%= SubscriptionLocalServiceUtil.isSubscribed(user.getCompanyId(), user.getUserId(), MBThread.class.getName(), message.getThreadId()) %>">
						<portlet:actionURL var="unsubscribeURL">
							<portlet:param name="struts_action" value="/message_boards/edit_message" />
							<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.UNSUBSCRIBE %>" />
							<portlet:param name="redirect" value="<%= currentURL %>" />
							<portlet:param name="messageId" value="<%= String.valueOf(message.getMessageId()) %>" />
						</portlet:actionURL>

					<liferay-ui:icon 
							image="unsubscribe"
							message="Unsubscribe"
							label="<%= true %>"
							url="<%= unsubscribeURL %>"
						/> 
					</c:when>
					<c:otherwise>
						<portlet:actionURL var="subscribeURL">
							<portlet:param name="struts_action" value="/message_boards/edit_message" />
							<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.SUBSCRIBE %>" />
							<portlet:param name="redirect" value="<%= currentURL %>" />
							<portlet:param name="messageId" value="<%= String.valueOf(message.getMessageId()) %>" />
						</portlet:actionURL>

						<liferay-ui:icon
							image="subscribe"
							message="Subscribe"
							label="<%= true %>"
							url="<%= subscribeURL %>"
						/> 
					</c:otherwise>
				</c:choose>
			</c:if>
</div>
<!-- 
Chanegs done by ANAND : End
 -->

	<div class="thread-actions">
		<liferay-ui:icon-list>
			<c:if test="<%= MBCategoryPermission.contains(permissionChecker, scopeGroupId, (category != null) ? category.getCategoryId() : MBCategoryConstants.DEFAULT_PARENT_CATEGORY_ID, ActionKeys.ADD_MESSAGE) %>">
				<portlet:renderURL var="addMessageURL">
					<portlet:param name="struts_action" value="/message_boards/edit_message" />
					<portlet:param name="redirect" value="<%= currentURL %>" />
					<portlet:param name="mbCategoryId" value="<%= (category != null) ? String.valueOf(category.getCategoryId()) : String.valueOf(MBCategoryConstants.DEFAULT_PARENT_CATEGORY_ID) %>" />
				</portlet:renderURL>

				<a class="btn btn-primary" href="<%= addMessageURL %>"
								title="Post New Thread"><i class="icon-plus"></i><liferay-ui:message key="Post New Thread"/></a>


			<%-- 	<liferay-ui:icon class= "btn"
					image="post"
					message="post-new-thread"
					url="<%= addMessageURL %>"
				/> --%>
			</c:if>

			<c:if test="<%= !thread.isLocked() && MBMessagePermission.contains(permissionChecker, message, ActionKeys.PERMISSIONS) %>">

				<%
				MBMessage rootMessage = null;

				if (message.isRoot()) {
					rootMessage = message;
				}
				else {
					rootMessage = MBMessageLocalServiceUtil.getMessage(message.getRootMessageId());
				}
				%>

				<liferay-security:permissionsURL
					modelResource="<%= MBMessage.class.getName() %>"
					modelResourceDescription="<%= rootMessage.getSubject() %>"
					resourcePrimKey="<%= String.valueOf(thread.getRootMessageId()) %>"
					var="permissionsURL"
					windowState="<%= LiferayWindowState.POP_UP.toString() %>"
				/>

		<a class="btn btn-secondary btn2" href="<%= permissionsURL %>"
								title="permissions">Permissions</a>
				<%-- <liferay-ui:icon
					image="permissions"
					method="get"
					url="<%= permissionsURL %>"
					useDialog="<%= true %>"
				/> --%>
			</c:if>

			<c:if test="<%= enableRSS && MBMessagePermission.contains(permissionChecker, message, ActionKeys.VIEW) %>">

				<%
				rssURL.setParameter("mbCategoryId", StringPool.BLANK);
				rssURL.setParameter("threadId", String.valueOf(message.getThreadId()));
				%>

				<liferay-ui:rss
					delta="<%= rssDelta %>"
					displayStyle="<%= rssDisplayStyle %>"
					feedType="<%= rssFeedType %>"
					resourceURL="<%= rssURL %>"
				/>
			</c:if>

			<%-- <c:if test="<%= MBMessagePermission.contains(permissionChecker, message, ActionKeys.SUBSCRIBE) && (MBUtil.getEmailMessageAddedEnabled(portletPreferences) || MBUtil.getEmailMessageUpdatedEnabled(portletPreferences)) %>">
				<c:choose>
					<c:when test="<%= SubscriptionLocalServiceUtil.isSubscribed(user.getCompanyId(), user.getUserId(), MBThread.class.getName(), message.getThreadId()) %>">
						<portlet:actionURL var="unsubscribeURL">
							<portlet:param name="struts_action" value="/message_boards/edit_message" />
							<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.UNSUBSCRIBE %>" />
							<portlet:param name="redirect" value="<%= currentURL %>" />
							<portlet:param name="messageId" value="<%= String.valueOf(message.getMessageId()) %>" />
						</portlet:actionURL>

				<form action="<%= unsubscribeURL %>" method="post">
				<aui:button class="btn btn-secondary btn2"  style ="width: 121px;background: #f3f3f3;color: #333; padding: 4px 12px;
  margin-bottom: 0;font-size: 14px;line-height: 20px;text-align: center;vertical-align: middle;cursor: pointer;  text-shadow:none; text-shadow: none;font-family: 'Gotham Rounded Book';font-weight: 400;"
   type="submit" value="Unsubscribe" title ="Unsubscribe"/>
				<a class="btn btn-secondary" href="<%= unsubscribeURL %>"  method ="post"
								title="Unsubscribe"><i class="icon-plus"></i><liferay-ui:message key="Unsubscribe"/></a>
				</form>		



			<a class="btn btn-secondary btn2" href="<%= unsubscribeURL %>"
								title="Unsubscribe"><liferay-ui:message key="Unsubscribe"/></a>
						<liferay-ui:icon 
							image="unsubscribe"
							url="<%= unsubscribeURL %>"
						/>
					</c:when>
					<c:otherwise>
						<portlet:actionURL var="subscribeURL">
							<portlet:param name="struts_action" value="/message_boards/edit_message" />
							<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.SUBSCRIBE %>" />
							<portlet:param name="redirect" value="<%= currentURL %>" />
							<portlet:param name="messageId" value="<%= String.valueOf(message.getMessageId()) %>" />
						</portlet:actionURL>

				<form action="<%= subscribeURL %>" method="post">
				 <aui:button class="btn btn-secondary btn2" style ="width: 121px;background: #f3f3f3;color: #333; padding: 4px 12px;
  margin-bottom: 0;font-size: 14px;line-height: 20px;text-align: center;vertical-align: middle;cursor: pointer;  text-shadow:none; text-shadow: none;font-family: 'Gotham Rounded Book';font-weight: 400;"
   type="submit" value="Subscribe" title ="Subscribe" /> 
				</form>	


				<a class="btn btn-secondary btn2" href="<%= subscribeURL %>"
								title="subscribe"><liferay-ui:message key="Subscribe"/></a>
						<liferay-ui:icon
							image="subscribe"
							url="<%= subscribeURL %>"
						/>
					</c:otherwise>
				</c:choose>
			</c:if> --%>

			<c:if test="<%= MBCategoryPermission.contains(permissionChecker, scopeGroupId, (category != null) ? category.getCategoryId() : MBCategoryConstants.DEFAULT_PARENT_CATEGORY_ID, ActionKeys.LOCK_THREAD) %>">
				<c:choose>
					<c:when test="<%= thread.isLocked() %>">
						<portlet:actionURL var="unlockThreadURL">
							<portlet:param name="struts_action" value="/message_boards/edit_message" />
							<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.UNLOCK %>" />
							<portlet:param name="redirect" value="<%= currentURL %>" />
							<portlet:param name="threadId" value="<%= String.valueOf(message.getThreadId()) %>" />
						</portlet:actionURL>

			<a class="btn btn-secondary btn2" href="<%= unlockThreadURL %>"
								title="Unlock thread"><liferay-ui:message key="Unlock Thread"/></a>

					<%-- <liferay-ui:icon 
								image="unlock" 
							message="unlock-thread"
							url="<%= unlockThreadURL %>" 
						/>  --%>
					</c:when>
					<c:otherwise>
						<portlet:actionURL var="lockThreadURL">
							<portlet:param name="struts_action" value="/message_boards/edit_message" />
							<portlet:param name="<%= Constants.CMD %>" value="<%= Constants.LOCK %>" />
							<portlet:param name="redirect" value="<%= currentURL %>" />
							<portlet:param name="threadId" value="<%= String.valueOf(message.getThreadId()) %>" />
						</portlet:actionURL>

				<a class="btn btn-secondary btn2" href="<%= lockThreadURL %>"
								title="lock thread"><liferay-ui:message key="Lock Thread"/></a>
						<%-- <liferay-ui:icon 
							image="lock"
							message="lock-thread"
							url="<%= lockThreadURL %>"
						/> --%>
					</c:otherwise>
				</c:choose>
			</c:if>

			<c:if test="<%= MBCategoryPermission.contains(permissionChecker, scopeGroupId, (category != null) ? category.getCategoryId() : MBCategoryConstants.DEFAULT_PARENT_CATEGORY_ID, ActionKeys.MOVE_THREAD) %>">
				<portlet:renderURL var="editThreadURL">
					<portlet:param name="struts_action" value="/message_boards/move_thread" />
					<portlet:param name="redirect" value="<%= currentURL %>" />
					<portlet:param name="mbCategoryId" value="<%= (category != null) ? String.valueOf(category.getCategoryId()) : String.valueOf(MBCategoryConstants.DEFAULT_PARENT_CATEGORY_ID) %>" />
					<portlet:param name="threadId" value="<%= String.valueOf(message.getThreadId()) %>" />
				</portlet:renderURL>

						<a class="btn btn-secondary btn2" href="<%= editThreadURL %>"
								title="move thread"><liferay-ui:message key="Move Thread"/></a>
				<%-- <liferay-ui:icon 
					image="forward"
					message="move-thread"
					url="<%= editThreadURL %>"
				/> --%>
			</c:if>

			<%-- <c:if test="<%= MBMessagePermission.contains(permissionChecker, message, ActionKeys.DELETE) && !thread.isLocked() %>">
				<portlet:renderURL var="parentCategoryURL">
					<portlet:param name="struts_action" value="/message_boards/view" />
					<portlet:param name="mbCategoryId" value="<%= (category != null) ? String.valueOf(category.getCategoryId()) : String.valueOf(MBCategoryConstants.DEFAULT_PARENT_CATEGORY_ID) %>" />
				</portlet:renderURL>

				<portlet:actionURL var="deleteURL">
					<portlet:param name="struts_action" value="/message_boards/delete_thread" />
					<portlet:param name="<%= Constants.CMD %>" value="<%= TrashUtil.isTrashEnabled(themeDisplay.getScopeGroupId()) ? Constants.MOVE_TO_TRASH : Constants.DELETE %>" />
					<portlet:param name="redirect" value="<%= parentCategoryURL %>" />
					<portlet:param name="threadId" value="<%= String.valueOf(message.getThreadId()) %>" />
				</portlet:actionURL>

			<a class="btn btn-secondary btn2" href="<%= deleteURL %>"
								title="move to the recycle bin"><liferay-ui:message key="Move to the Recycle Bin"/></a>
				<liferay-ui:icon-delete 
					trash="<%= TrashUtil.isTrashEnabled(themeDisplay.getScopeGroupId()) %>"
					url="<%= deleteURL %>"
				/>
			</c:if> --%>
		</liferay-ui:icon-list>
	</div>



<div class="thread-controls">




<%-- 	<c:if test="<%= PropsValues.MESSAGE_BOARDS_THREAD_PREVIOUS_AND_NEXT_NAVIGATION_ENABLED %>">
		<div class="thread-navigation">
			<liferay-ui:message key="threads" />

			[

			<c:choose>
				<c:when test="<%= previousThread != null %>">
					<portlet:renderURL var="previousThreadURL">
						<portlet:param name="struts_action" value="/message_boards/view_message" />
						<portlet:param name="messageId" value="<%= String.valueOf(previousThread.getRootMessageId()) %>" />
					</portlet:renderURL>

					<aui:a href="<%= previousThreadURL %>" label="previous" />
				</c:when>
				<c:otherwise>
					<liferay-ui:message key="previous" />
				</c:otherwise>
			</c:choose>

			|

			<c:choose>
				<c:when test="<%= nextThread != null %>">
					<portlet:renderURL var="nextThreadURL">
						<portlet:param name="struts_action" value="/message_boards/view_message" />
						<portlet:param name="messageId" value="<%= String.valueOf(nextThread.getRootMessageId()) %>" />
					</portlet:renderURL>

					<aui:a href="<%= nextThreadURL %>" label="next" />
				</c:when>
				<c:otherwise>
					 <liferay-ui:message key="next" /> 
				</c:otherwise>
			</c:choose>

			]
		</div>
	</c:if> --%>


	<div class="clear"></div>
</div>


<div>

	<%
	MBTreeWalker treeWalker = messageDisplay.getTreeWalker();

	List<MBMessage> messages = null;

	if (treeWalker != null) {
		messages = new ArrayList<MBMessage>();

		messages.addAll(treeWalker.getMessages());

		messages = ListUtil.sort(messages, new MessageCreateDateComparator(true));
	}

	AssetUtil.addLayoutTags(request, AssetTagLocalServiceUtil.getTags(MBMessage.class.getName(), thread.getRootMessageId()));
	%>

	<div class="message-scroll" id="<portlet:namespace />message_0"></div>

	<c:if test="<%= threadView.equals(MBThreadConstants.THREAD_VIEW_COMBINATION) && (messages.size() > 1) %>">
		<liferay-ui:toggle-area id="toggle_id_message_boards_view_message_thread">
			<table class="toggle_id_message_boards_view_message_thread">

			<%
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER, treeWalker);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_CATEGORY, category);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_CUR_MESSAGE, treeWalker.getRoot());
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_DEPTH, new Integer(0));
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_LAST_NODE, Boolean.valueOf(false));
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_SEL_MESSAGE, message);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_THREAD, thread);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_THREAD_FLAG, threadFlag);
			%>

			<liferay-util:include page="/html/portlet/message_boards/view_thread_shortcut.jsp" />

			</table>
		</liferay-ui:toggle-area>
	</c:if>

	<%
	boolean viewableThread = false;
	%>

	<c:choose>
		<c:when test="<%= threadView.equals(MBThreadConstants.THREAD_VIEW_TREE) %>">

			<%
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER, treeWalker);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_CATEGORY, category);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_CUR_MESSAGE, treeWalker.getRoot());
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_DEPTH, new Integer(0));
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_LAST_NODE, Boolean.valueOf(false));
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_SEL_MESSAGE, message);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_THREAD, thread);
			request.setAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_VIEWABLE_THREAD, Boolean.FALSE.toString());
			%>

			<liferay-util:include page="/html/portlet/message_boards/view_thread_tree.jsp" />

			<%
			viewableThread = GetterUtil.getBoolean((String)request.getAttribute(WebKeys.MESSAGE_BOARDS_TREE_WALKER_VIEWABLE_THREAD));
			%>

		</c:when>
		<c:otherwise>
			<%@ include file="/html/portlet/message_boards/view_thread_flat.jspf" %>
		</c:otherwise>
	</c:choose>

	<c:if test="<%= !viewableThread %>">
		<div class="alert alert-error">
			<liferay-ui:message key="you-do-not-have-permission-to-access-the-requested-resource" />
		</div>
	</c:if>
</div>
