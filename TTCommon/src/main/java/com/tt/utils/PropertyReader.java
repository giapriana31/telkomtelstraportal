package com.tt.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import org.apache.commons.configuration.PropertiesConfiguration;

import com.tt.logging.Logger;


public class PropertyReader {
	
    private static String REPORT_CONFIG_FILE = "/data1/apps/workspace/IJVWebPortalApplication/propertyfile/config_tt.properties";
	//private static final String REPORT_CONFIG_FILE = "d:\\properties\\config_tt.properties";
    //private static final String CONFIG_FILE = "config.properties";
    private static final String ENCODING = "UTF-8";

    private static InnerClass innerClass = new InnerClass();
    private static PropertiesConfiguration config;
    public static Properties prop = new Properties();
    
    static {
    	if(RefreshUtil.isWindows()){
    		REPORT_CONFIG_FILE = "d:\\properties\\config_tt.properties";
    	}
        reload();
    }

    /**
     * Reload the value from source
     */
    public static void reload() {
        try {
        	Logger logger = Logger.getLogger(PropertyReader.class);
        	
        	logger.info("Running property reader from : "+innerClass.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        	logger.info("Trying to read property file from location: "+new File(REPORT_CONFIG_FILE).getAbsolutePath());
        	
            InputStream propFile =
            innerClass.getClass().getClassLoader().getResourceAsStream(REPORT_CONFIG_FILE);
            
            config = new PropertiesConfiguration();
            config.setDelimiterParsingDisabled(true);
            config.load(propFile, ENCODING);            
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Reload the value from source
     */
    public static void reload(String filePath) {
        System.out.println("Reloading ConfigProperty, filePath=" + filePath);
            
        try {
            config = new PropertiesConfiguration();
            config.setDelimiterParsingDisabled(true);
            config.load(new FileInputStream(filePath), ENCODING);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }    

    /**
     * Get config value by key
     *
     * @param key config name
     * @return value of the config
     */
    public static String getConfig(String key) {
        String str = config.getString(key);

        if (str == null) {
            str = "";
        }

        return str;
    }

    /**
     * Get list of config by key
     *
     * @param key config name
     * @return list of properties
     */
    public static List<?> getConfigList(String key) {
        return config.getList(key);
    }

    /**
     * Innear class for getting class loader purpose 
     * Not required ...
     */
    public static class InnerClass {
        public InnerClass()
        {}
    }
    
	/// ******	Main Method to Test property Strings ******	///
    public static void main(String args[]){
    	System.out.println(getConfig("TTReport.region"));
    	
    	System.out.println(getConfigList("TTReport.region"));
    	
    }
    
    public static Properties getProperties(){
		try{
			String configFileName = "/data1/apps/workspace/IJVWebPortalApplication/propertyfile/config_tt.properties";
		
			if(RefreshUtil.isWindows()){
				configFileName = "d:\\properties\\config_tt.properties";
	    	}
			
			InputStream is = new FileInputStream(configFileName);
			prop.load(is);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return prop;
	}
    
    public static Properties getProperties(String propertyFileName){
		try{
			String configFileName = "/data1/apps/workspace/IJVWebPortalApplication/propertyfile/" + propertyFileName;
		
			if(RefreshUtil.isWindows()){
				configFileName = "d:\\properties\\" + propertyFileName;
	    	}
			
			InputStream is = new FileInputStream(configFileName);
			prop.load(is);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return prop;
	}
}
