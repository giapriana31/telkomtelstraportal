package com.tt.utils;

import com.tt.auth.MyWSAuthenticator;

public class ServiceUtils {

	public static MyWSAuthenticator getMyAuthenticator(String username, String password){
		return new MyWSAuthenticator(username, password);
	}
}
