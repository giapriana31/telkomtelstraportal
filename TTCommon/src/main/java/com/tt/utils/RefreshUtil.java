/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys			20-JUL-2015			Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.utils;

import java.io.IOException;
import javax.net.ssl.HttpsURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javassist.bytecode.stackmap.BasicBlock.Catch;

import com.tt.logging.Logger;

/**
 * Utility class
 * 
 * @author Infosys
 * @version 1.0
 */
public class RefreshUtil {
	
    private static final DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    
    private static Properties prop = PropertyReader.getProperties();
    private final static Logger log = Logger.getLogger(RefreshUtil.class);
    

    /**
     * Converts String to Date
     * 
     * @param dateString
     * @return
     * @throws ParseException
     */
    public static Date convertStringToDate(String dateString)  throws ParseException {
    	Date date = dateString == null || dateString.equals("") ? null: FORMATTER.parse(dateString);
    	return date;
    }
    
    public static Date convertStringToOnlyDate(String dateString)  throws ParseException {
    	Date date = dateString == null || dateString.equals("") ? null: DATE_FORMATTER.parse(dateString);
    	return date;
    }
    
    public static void setProxyObject(){    	
	
    	System.out.println("!!!!!!!!!!!!!!!!!!!!!! :: setProxyobject");
		//get the proxy details for appropriate env. 
		String envType = prop.getProperty("this.environment.type");
		
		if(null==envType || envType.equalsIgnoreCase("localdev")){
			
			System.getProperties().put("http.proxyHost", prop.getProperty("someProxyURL"));
	        System.getProperties().put("http.proxyPort", prop.getProperty("someProxyPort"));
	        System.getProperties().put("http.proxyUser", prop.getProperty("someUserName"));
	        System.getProperties().put("http.proxyPassword", prop.getProperty("somePassword"));
	        System.getProperties().put("http.nonProxyHosts", prop.getProperty("nonProxy"));
	        System.getProperties().put("https.proxyHost", prop.getProperty("someHttpsProxyURL"));
	        System.getProperties().put("https.proxyPort", prop.getProperty("someHttpsProxyPort"));
	        System.getProperties().put("https.proxyUser", prop.getProperty("someHttpsUserName"));
	        System.getProperties().put("https.proxyPassword", prop.getProperty("someHttpsPassword"));
	        System.out.println("!!!!!!!!!!!!!!!!!!!!! all set");
		}    
		else if(envType.equalsIgnoreCase("prod") || envType.equalsIgnoreCase("batch")){
			
			System.getProperties().put("http.proxyHost", prop.getProperty("someProxyURL"));
	        System.getProperties().put("http.proxyPort", prop.getProperty("someProxyPort"));
	        System.getProperties().put("https.proxyHost", prop.getProperty("someHttpsProxyURL"));
	        System.getProperties().put("https.proxyPort", prop.getProperty("someHttpsProxyPort"));
		} 	
    	
		log.debug("\nUsing the http.proxyHost As:" + System.getProperty("http.proxyHost"));
		log.debug("\nUsing the http.proxyPort As:" + System.getProperty("http.proxyPort"));
		log.debug("\nUsing the http.proxyUser As:" + System.getProperty("http.proxyUser"));
		log.debug("\nUsing the http.proxyPassword As:" + System.getProperty("http.proxyPassword"));
    }
    
    public static HttpsURLConnection getHttpURLConnection(  URL url ){
    	String envType = prop.getProperty("this.environment.type");
    	HttpsURLConnection con=null;
    	try{
    	if (null==envType || envType.equalsIgnoreCase("localdev")||envType.equalsIgnoreCase("prod") || envType.equalsIgnoreCase("batch")) {
			
    		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
					prop.getProperty("someProxyURL"), Integer.parseInt(prop
							.getProperty("someProxyPort"))));
			

				 con = (HttpsURLConnection) url
						.openConnection(proxy);
			 
		}else{
			
			con = (HttpsURLConnection) url
					.openConnection();
		}
		}catch(Exception e){
			
			e.printStackTrace();
		}
			
    
		return con;
    	
    	
    }
    
    public static boolean isWindows(){
    	String OS = System.getProperty("os.name").toLowerCase();
    	return (OS.indexOf("win") >= 0);
    }    
   
}
