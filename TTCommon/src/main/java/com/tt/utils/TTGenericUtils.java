
package com.tt.utils;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.tt.constants.GenericConstants;

public class TTGenericUtils 
{
	
	public static Properties prop = PropertyReader.getProperties();
	
	// get customerID from session
	public static String getCustomerIDFromSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			return req.getSession().getAttribute(prop.getProperty("Hook.LIFERAY_SHARED_customerID_KEY").trim()).toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "0";
		}
	}
	
	// check for permission based on roles
	public static boolean allowUserToPerformAction(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			User user = getLoggedInUser(req, res);
			if(user != null && isExternal(user))
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}	
	
	// return current logged-in user
	public static User getLoggedInUser(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			User user = UserLocalServiceUtil.getUser((Long)req.getSession().getAttribute(WebKeys.USER_ID));			
			return user;
			//Myuser user = new MyUser().getExpandoBridge.getAttribute();
			//getExpandoBridge().getAttribute("asd");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}		
	}		
		
	// get 
	public static String getCustomerNameFromSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			return req.getSession().getAttribute("LIFERAY_SHARED_customerName").toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "null";
		}
	}
	// check for external user
	public static boolean isExternal(User user)
	{
		try
		
		{
			List<Role> listRole = getUserRoleList(user);
			
			for(Role role : listRole)
	    	{
	    		if(!prop.getProperty("Hook.LIFERAY_SHARED_ALLWED_CUSTOMER_ROLES").trim().toLowerCase().contains(role.getName().toLowerCase()))
	    		{	
	    			return false;    				    			
	    		}	    		
	    	}
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}	
	
	// get list of roles assigned to user
	public static List<Role> getUserRoleList(User user)
	{
		try
		{
			return user.getRoles();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static User getLoggedInUser(HttpServletRequest request){
		User user=null;
		
		
		try {
			 user=UserLocalServiceUtil.getUser((Long)request.getSession().getAttribute(WebKeys.USER_ID));
			
			
		} catch (PortalException | SystemException e) {
			e.printStackTrace();
			return null;
		}
		
		return user;
		
	}
	
	public static boolean hasAccessToUser(User user, String feature)
	{
		List<Role> userRoles = getUserRoleList(user);
		
		prop = PropertyReader.getProperties("feature_roles.properties");
		
		String allowedRolesString = prop.getProperty(feature);
		
		/* if key is not present in properties file then role can access the feature */
		if(allowedRolesString == null)
			return true;
		else
		{
			String[] allowedRoles = allowedRolesString.split(",");
					
			for(Role roleFromList : userRoles)
			{
				System.out.println("roleFromList :: " + roleFromList);
				for(String roleForFeature : allowedRoles)
				{
					System.out.println("roleForFeature :: " + roleForFeature);
					if(roleFromList.getName().trim().toLowerCase().equalsIgnoreCase(roleForFeature.trim().toLowerCase()))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean doesNotHaveAccessToUser(User user, String feature)
	{
		List<Role> userRoles = getUserRoleList(user);
		
		prop = PropertyReader.getProperties("feature_roles.properties");
		
		String notAllowedRolesString = prop.getProperty(feature);
		
		/* if key is not present in properties file then role can access the feature */
		if(notAllowedRolesString == null)
			return true;
		else
		{
			String[] notAllowedRoles = notAllowedRolesString.split(",");
					
			for(Role roleFromList : userRoles)
			{
				System.out.println("roleFromList :: " + roleFromList);
				for(String roleForFeature : notAllowedRoles)
				{
					System.out.println("roleForFeature :: " + roleForFeature);
					if(roleFromList.getName().trim().toLowerCase().equalsIgnoreCase(roleForFeature.trim().toLowerCase()))
					{
						return false;
					}
				}
			}
		}
		return true;
	}
	
	
	public static void getCurrentView(HttpServletRequest request) throws PortalException, SystemException
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long groupId = themeDisplay.getSiteGroupId();
		String url = themeDisplay.getPortalURL();
		String friendlyUrl = themeDisplay.getLayout().getFriendlyURL();
		String portalUrl = PortalUtil.getLayoutURL(themeDisplay.getLayout(), themeDisplay);
		String title = themeDisplay.getLayout().getName(themeDisplay.getLocale());
		
		String groupName = themeDisplay.getLayout().getGroup().getName();
		System.out.println("Group Name :: " + groupName);
		System.out.println("friendly url :: " + friendlyUrl);
		System.out.println("portal url :: " + portalUrl);
		System.out.println("Group Id :: " + groupId);
		System.out.println("URL :: " + url);
		System.out.println("Title :: " + title);
	}
	
	public static String getCurrentViewName(HttpServletRequest request) throws PortalException, SystemException 
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String groupName = themeDisplay.getLayout().getGroup().getName();
		
		return groupName;
	}
	
	public static boolean checkRolePortallTTAdmin(HttpServletRequest request) throws PortalException, SystemException 
	{
		User user = getLoggedInUser(request);
		
		List<Role> userRoles = getUserRoleList(user);
		
		for (Role role : userRoles) 
		{
			System.out.println("Role assigned to user :: " + role.getName());
			if(role.getName().equalsIgnoreCase(GenericConstants.ROLE_ENDCUSTOMERADMIN))
			{
				return true;
			}
		}	
		return false;
	}
	
	
	public static boolean checkRoleInternalAdmin(HttpServletRequest request) throws PortalException, SystemException 
	{
		User user = getLoggedInUser(request);
		List<Role> userRoles = getUserRoleList(user);
		
		for (Role role : userRoles) 
		{
			System.out.println("Role assigned to user :: " + role.getName());
			System.out.println("Roles from Generic::" +GenericConstants.ROLE_NOCMANAGER);
			if(role.getName().equalsIgnoreCase(GenericConstants.ROLE_NOCMANAGER) || role.getName().equalsIgnoreCase(GenericConstants.ROLE_CUSTOMERPORTALADMIN))
			{
				return true;
			}
			 
		}	
		return false;
	}
	
	/*public static HttpResponse getResponseFromURL(Map<String,String> attrMap, String QueryString)
	{
		String finalURL = "";
		
		HttpClient httpclient = HttpClientBuilder.create().build();
		
		if("".equalsIgnoreCase(QueryString))
		{
			finalURL = attrMap.get("url");
		}
		else
		{
			finalURL = attrMap.get("url") + QueryString;
		}
		
		return null;
	}*/
}
