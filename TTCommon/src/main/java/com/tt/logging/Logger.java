package com.tt.logging;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.PatternLayout;

public class Logger {
	
org.apache.log4j.Logger logger;
	
	static{
		try{
			//basic configuration
			BasicConfigurator.configure();
		  	}
		catch(Exception e){
			System.out.println(" Unable to Configure, instantiate logger :" );
			e.printStackTrace();
		}
	}
	
	/*try{
		final FileAppender fileappender = new FileAppender(new PatternLayout());		
	}
  */
  	
  

  //Logger class 
  private Logger(String className) {
    logger = org.apache.log4j.Logger.getLogger(className);
  }

  //Logger method,params: class name
  private Logger(Class className) {
    logger = org.apache.log4j.Logger.getLogger(className);
  }

  //getLogger method,params: String - class name
  public static Logger getLogger(String className) {
    return new Logger(className);
  }

  //getLogger method,params: class name
  public static Logger getLogger(Class className) {
    return new Logger(className);
  }

  //debug method, params: message
  public void debug(String msg) {
    logger.debug(msg);
    
  }

/*  public void debug(String msg, String fileName) {
		  logger.addAppender(fileappender);	  
  }
*/  
  
  //debug method, params: message, throwable object
  public void debug(String msg, Throwable t) {
    logger.debug(msg,t);
  }

  //info method, params: message
  public void info(String msg) {
    logger.info(msg);
  }

  //info method. params: message, throwable object
  public void info(String msg, Throwable t) {
    logger.info(msg, t);
  }

  // warn method, params: message
  public void warn(String msg) {
    logger.warn(msg);
  }

  //warn method, params: message,throwable object
  public void warn(String msg, Throwable t) {
    logger.warn(msg, t);
  }

  //error method , params:message
  public void error(String msg) {
    logger.error(msg);
  }

  //params : string msg and throwable object
  public void error(String msg, Throwable t) {
    logger.error(msg, t);
  }

}
