package com.tt.roles;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.expando.model.ExpandoTableConstants;
import com.liferay.portlet.expando.model.ExpandoValue;
import com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil;
import com.tt.utils.PropertyReader;

public class CustomerRole 
{	
	public Properties prop = PropertyReader.getProperties();
		
	// Session object name, keep it public becuase dockbar &session jsp are accessing it
	public String LIFERAY_SHARED_UserObject_KEY = "";
	public String LIFERAY_SHARED_userRole_KEY = "";
	public String LIFERAY_SHARED_userRole_INTERNAL_VALUE = "";
	public String LIFERAY_SHARED_userRole_EXTERNAL_VALUE = "";
	public String LIFERAY_SHARED_userRole_CUSTOMER = "";
	public String LIFERAY_SHARED_ALLWED_CUSTOMER_ROLES = "";
	public String LIFERAY_SHARED_ALLWED_EMPTY_ROLES = "";
	public String LIFERAY_SHARED_customerID_KEY = "";
	public String LIFERAY_SHARED_customerName_KEY = "";
	public String LIFERAY_SHARED_CUSTOMERMAP_KEY = "";
	public String LIFERAY_SHARED_ROLE_ASSIGN_ERROR_KEY = "";
	public String LIFERAY_SHARED_ROLE_ASSIGN_ERROR_MESSAGE_KEY = "";
	public String LIFERAY_SHARED_USER_ASSIGN_ERROR_KEY = "";
	public String LIFERAY_SHARED_CustomerPortalAdmin = "";
	public String LIFERAY_SHARED_ALLWED_INTERNAL_ROLES = "";
	
	public CustomerRole() 
	{
		// set value from property file
		LIFERAY_SHARED_UserObject_KEY = prop.getProperty("Hook.LIFERAY_SHARED_UserObject_KEY").trim();
		LIFERAY_SHARED_userRole_KEY = prop.getProperty("Hook.LIFERAY_SHARED_userRole_KEY").trim();
		LIFERAY_SHARED_userRole_INTERNAL_VALUE = prop.getProperty("Hook.LIFERAY_SHARED_userRole_INTERNAL_VALUE").trim();
		LIFERAY_SHARED_userRole_EXTERNAL_VALUE = prop.getProperty("Hook.LIFERAY_SHARED_userRole_EXTERNAL_VALUE").trim();
		LIFERAY_SHARED_userRole_CUSTOMER = prop.getProperty("Hook.LIFERAY_SHARED_userRole_CUSTOMER").trim();
		LIFERAY_SHARED_ALLWED_CUSTOMER_ROLES = prop.getProperty("Hook.LIFERAY_SHARED_ALLWED_CUSTOMER_ROLES").trim();
		LIFERAY_SHARED_ALLWED_EMPTY_ROLES = prop.getProperty("Hook.LIFERAY_SHARED_ALLWED_EMPTY_ROLES").trim();	
		LIFERAY_SHARED_customerID_KEY = prop.getProperty("Hook.LIFERAY_SHARED_customerID_KEY").trim();
		LIFERAY_SHARED_customerName_KEY = prop.getProperty("Hook.LIFERAY_SHARED_customerName_KEY").trim();
		LIFERAY_SHARED_CUSTOMERMAP_KEY = prop.getProperty("Hook.LIFERAY_SHARED_CUSTOMERMAP_KEY").trim();
		LIFERAY_SHARED_ROLE_ASSIGN_ERROR_KEY = prop.getProperty("Hook.LIFERAY_SHARED_ROLE_ASSIGN_ERROR_KEY").trim();
		LIFERAY_SHARED_ROLE_ASSIGN_ERROR_MESSAGE_KEY = prop.getProperty("Hook.LIFERAY_SHARED_ROLE_ASSIGN_ERROR_MESSAGE_KEY").trim();
		LIFERAY_SHARED_USER_ASSIGN_ERROR_KEY = prop.getProperty("Hook.LIFERAY_SHARED_USER_ASSIGN_ERROR_KEY").trim();
		LIFERAY_SHARED_CustomerPortalAdmin = prop.getProperty("Hook.LIFERAY_SHARED_CustomerPortalAdmin").trim();
		LIFERAY_SHARED_ALLWED_INTERNAL_ROLES = prop.getProperty("Hook.LIFERAY_SHARED_ALLWED_INTERNAL_ROLES").trim();
	}
	
	// return current logged-in user
	public User getLoggedInUser(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			User user = UserLocalServiceUtil.getUser((Long)req.getSession().getAttribute(WebKeys.USER_ID));			
			return user;			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}		
	}
	
//	public Boolean setInternalUsertoSession(HttpServletRequest req, HttpServletResponse res, User user)
//	{
//		try
//		{
//			req.getSession().setAttribute(LIFERAY_SHARED_UserObject_KEY, user);
//			return true;    	
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//			return false;
//		}
//	}
	
	// get internal user from session
	public User getInternalUserFromSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			return (User)req.getSession().getAttribute(LIFERAY_SHARED_UserObject_KEY);    	
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	// check wheter roles are assigned or not
	public boolean isRoleAssigned(User user)
	{
		try
		{	
			List<String> currentList = new ArrayList<String>();
			List<String> userRoles = new ArrayList<String>();
			String arr[] = LIFERAY_SHARED_ALLWED_EMPTY_ROLES.split(",");
			
			List<Role> listRole = getUserRoleList(user);	
			
			for(Role role : listRole)
				userRoles.add(role.getName().toLowerCase());
			
			for(int i=0;i<arr.length;i++)
				if(arr[i]!=null && !arr[i].trim().equalsIgnoreCase(""))
					currentList.add(arr[i].toLowerCase().trim());
			
			for(String s: currentList)
				userRoles.remove(s);
			
			if(userRoles.size()==0)
				return false;
			else
				return true;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// check for internal user	
	public boolean isInternal(User user)
	{
		try
		{
			List<Role> listRole = getUserRoleList(user);	
			
			for(Role role : listRole)
	    	{
				String arr[] = LIFERAY_SHARED_ALLWED_INTERNAL_ROLES.split(",");
				for(int i=0;i<arr.length;i++)
					if(arr[i].trim().equalsIgnoreCase(role.getName().trim()))
						return true;	    			
	    	}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// check for external user
	public boolean isExternal(User user)
	{
		try
		{
			List<Role> listRole = getUserRoleList(user);
			
			for(Role role : listRole)
	    	{
				String arr[] = LIFERAY_SHARED_userRole_CUSTOMER.split(",");
				for(int i=0;i<arr.length;i++)
					if(arr[i].trim().equalsIgnoreCase(role.getName().trim()))
						return true;				
	    	}
			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// check for valid user (either internal or external)
	public boolean isValidUser(User user)
	{
		try
		{
			if(isExternal(user) && !isInternal(user) && !isCustomerPortalAdmin(user))
			{
				return true;
			}
			else if(!isExternal(user) && (isInternal(user) || isCustomerPortalAdmin(user)))
			{
				return true;
			}			
			else
				return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean isCustomerPortalAdmin(User user)
	{		
		try
		{
			List<String> userRoles = new ArrayList<String>();
			String arr[] = LIFERAY_SHARED_CustomerPortalAdmin.split(",");
			List<Role> listRole = getUserRoleList(user);	
			
			for(Role role : listRole)
				userRoles.add(role.getName().toLowerCase());
			
			for(int i=0;i<arr.length;i++)
			{
				if(arr[i]!=null && !arr[i].trim().equalsIgnoreCase(""))
					for(String s : userRoles)
						if(arr[i].equalsIgnoreCase(s))
						{
							return true;
						}
			}		
			return false;		
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// set internal user to session 
	public boolean setInternalUserRoleToSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			User user = getInternalUserFromSession(req, res);
			if(isInternal(user))
			{
				req.getSession().setAttribute(LIFERAY_SHARED_userRole_KEY,LIFERAY_SHARED_userRole_INTERNAL_VALUE);
			}
			else if(isExternal(user))
			{
				req.getSession().setAttribute(LIFERAY_SHARED_userRole_KEY,LIFERAY_SHARED_userRole_EXTERNAL_VALUE);
			}
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// set external user to session
	public boolean setExternalUserToSession(HttpServletRequest req, HttpServletResponse res,User user)
	{
		try
		{			
			req.getSession().setAttribute(LIFERAY_SHARED_UserObject_KEY,user);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// set internal user to session
	public boolean setInternalUserToSession(HttpServletRequest req, HttpServletResponse res,User user)
	{
		try
		{
			if(user !=null)
				req.getSession().setAttribute(LIFERAY_SHARED_UserObject_KEY,user);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// get customerID from session
	public String getCustomerIDFromSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			return req.getSession().getAttribute(LIFERAY_SHARED_customerID_KEY).toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "0";
		}
	}
		
	// set customer ID from session to display in dockbar
	public boolean setCustomerIDToSession(HttpServletRequest req, HttpServletResponse res, String ID)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_customerID_KEY,ID);			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
		
	// remove customer ID from session, else it will display old one
	public boolean removeCustomerIDToSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_customerID_KEY,null);			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// set customer Name from session to display in dockbar
	public boolean setCustomerNameToSession(HttpServletRequest req, HttpServletResponse res, String Name)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_customerName_KEY,Name);			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// remove customer Name from session else it will display old one
	public boolean removeCustomerNameToSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_customerName_KEY,null);			
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// get customer Name from session to display in dockbar
	public String getCustomerNameFromSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			return req.getSession().getAttribute(LIFERAY_SHARED_customerName_KEY).toString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "null";
		}
	}
	
	// get list of roles assigned to user
	public List<Role> getUserRoleList(User user)
	{
		try
		{
			return user.getRoles();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	
	// find user custom attribute value (now its not required, but keep it as reference)	
	public List<User> getUsersByCustomAttributes(String columnName,String Searchvalue,int limit)
	{
		try
		{
			List<ExpandoValue> values = ExpandoValueLocalServiceUtil.getColumnValues(PortalUtil.getDefaultCompanyId(),ClassNameLocalServiceUtil.getClassNameId(User.class),ExpandoTableConstants.DEFAULT_TABLE_NAME,columnName,Searchvalue,-1,-1);
			List<User> users = new ArrayList<User>();
			
			for(int i = 0; i < values.size() && users.size()<limit; i++) 
			{
				try 
				{
					User user = UserLocalServiceUtil.getUser(values.get(i).getClassPK());
					users.add(user);					
				} 
				catch(NoSuchUserException e) {}
			}
			
			return users;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// check for permission based on roles
	public boolean allowUserToPerformAction(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			User user = getLoggedInUser(req, res);
			if(user != null && isExternal(user))
				return true;
			else
				return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// set active user from session (selected user in case of internal user)
	public User getActiveUserFromSession(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			if(req.getSession().getAttribute(LIFERAY_SHARED_UserObject_KEY)==null)
			{
				return getLoggedInUser(req, res);
			}
			else
			{
				return (User)req.getSession().getAttribute(LIFERAY_SHARED_UserObject_KEY);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	// set customer list in session to displayin dockbar for selection 
	public boolean setCustomerIDNameMapToSession(HttpServletRequest req, HttpServletResponse res,String[] map)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_CUSTOMERMAP_KEY,map);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	// set role assigned error
	public void setRoleAssignError(HttpServletRequest req, HttpServletResponse res,User user)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_ROLE_ASSIGN_ERROR_KEY,true);			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// set user assigned error
	public void setUserAssignError(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_USER_ASSIGN_ERROR_KEY,true);			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	// remove user assignment error
	public void removeUserAssignError(HttpServletRequest req, HttpServletResponse res)
	{
		try
		{
			req.getSession().setAttribute(LIFERAY_SHARED_USER_ASSIGN_ERROR_KEY,null);			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
}
