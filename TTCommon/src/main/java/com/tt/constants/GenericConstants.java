package com.tt.constants;

public class GenericConstants {
	

	public static final String PREF_RESOURCE_IDENTIFIER_PREFIX = "javax.portlet.preference";
	public static final String LIFERAY = "Liferay";
	
	/*Service Request and Change Request Constants*/
	public static final String CUSTOMER = "Customer";
	public static final String CUSTOMER_INITIATED = "CustomerInitiated";
	public static final String SERVICE_REQUEST = "ServiceRequest";
	public static final String TTINITIATED = "TTInitiated";
	public static final String INTERNAL = "Internal";
	public static final String EXTERNAL = "External";
	
	public static final String PENDING= "Pending";
    public static final String APPROVED= "Approved";

    public static final String STATUSUNMANAGED = "Unmanaged";
	public static final String STATUSCOMMISSIONING = "Commissioning";
	public static final String STATUSOPERATIONAL = "Operational";
	public static final String CUSTOMERSTATUSACTIVE = "Active";
	public static final String COMMAINSIDESINGLEQUOTES = "','";
	public static final String SINGLEQUOTECLOSINGBRACE = "')";
    
    public static final String CUSTOMER_INITIATED_PENDING = "CustomerInitiatedPending";
    public static final String CUSTOMER_INITIATED_APPROVED = "CustomerInitiatedApproved";
    public static final String TTINITIATED_PENDING = "TTInitiatedPending";
    public static final String TTINITIATED_APPROVED = "TTInitiatedApproved";
    
    public static final String PROFILESETTINGS_FEATURE = "profilesetting";
    public static final String SECURITYSETTINGS_FEATURE = "securitysetting";
    public static final String MANAGEUSERS_FEATURE = "manageusers";
    public static final String REPORTSLINK_FEATURE = "reportslink";
    
    public static final String BLANK = "";
    public static final String NA_CONTRACT_REMAINING_TIME = "999999";
    public static final String NA_VALUE = "9999.99";
    public static final String NA_BV_SS_PERFORMANCE = "-1.00";
    public static final String NA = "NA";
    public static final String HYPHEN = "-";
    
    public static final String PRODUCTTYPE_MNS = "MNS";
    public static final String PRODUCTTYPE_PRIVATECLOUD = "Private Cloud";
    public static final String PRODUCTTYPE_UC = "Unified Comm";
    public static final String PRODUCTTYPE_WHISPIR = "Whispir";
    public static final String PRODUCTTYPE_IPSCAPE = "IPScape";
    public static final String PRODUCTTYPE_MANDOE = "Mandoe";
    public static final String PRODUCTTYPE_SECURITY = "Security";
    public static final String PRODUCTTYPE_SAAS = "SaaS";
    public static final String PRODUCTTYPE_DONAT = "Donat";
    public static final String PRODUCTTYPE_MAINTENACE = "Tenoss";
    public static final String PRODUCTTYPE_RIVERBED = "OPTIMIZATION";
    public static final String PRODUCTCLASSIFICATION_RIVERBED = "M-WANOPT";
    public static final String PRODUCTTYPE_SITE = "SITE";
    
    public static final String MNS_CONTACTPERSON = "mnscontactperson";
    public static final String PRIVATECLOUD_CONTACTPERSON = "privatecloudcontactperson";
    public static final String UC_CONTACTPERSON = "unifiedcommcontactperson";
    public static final String WHISPIR_CONTACTPERSON = "whispircontactperson";
    public static final String IPSCAPE_CONTACTPERSON = "ipscapecontactperson";
    public static final String MANDOE_CONTACTPERSON = "mandoecontactperson";
    public static final String SECURITY_CONTACTPERSON = "securitycontactperson";
    
    /* previously known as PortalTTAdministrator*/
    public static final String ROLE_ENDCUSTOMERADMIN = "EndCustomerAdmin";
    public static final String ROLE_NOCMANAGER = "NOCManager";
    public static final String ROLE_CUSTOMERPORTALADMIN = "CustomerPortalAdmin";
    
    
    public static final String RITM_LIKE_MAC = "mac";
    public static final String RITM_LIKE_NEW = "new";
    public static final String RITM_LIKE_DELETE = "delete";
    public static final String RITM_MNS_NEW = "MNS Service";
    public static final String RITM_MNS_NEW_DISPALY = "MNS New";
    public static final String RITM_MAC_TYPE_SIMPLE = "Simple";
    public static final String RITM_MAC_TYPE_COMPLEX = "Complex";
	public static final String EXPIRED = "expired";
	public static final String ZERO_VALUE = "0.00";
	
	public static final String MONTHS = "months";
	public static final String TITLE = "title";
}
