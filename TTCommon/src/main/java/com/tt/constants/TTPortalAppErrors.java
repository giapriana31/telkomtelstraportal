package com.tt.constants;

import java.util.HashMap;

public class TTPortalAppErrors extends GenericConstants {
	
	public static final HashMap<String, String> TTPORTALERRORCODES = new HashMap<String, String>();
		
	//TODO: needs to load the messages from external language cum properties file 
	static {
		
		//System Errors
		TTPORTALERRORCODES.put("SY:1111", "Error in retreving the user Unique Id");
		TTPORTALERRORCODES.put("SY:5555", "TT Portal Fatal System Error");
		TTPORTALERRORCODES.put("SY:2222", "TT Portal Print Writter Error");
		
		//DB Errors
		TTPORTALERRORCODES.put("DB:1111", "Error in retreving the user Unique Id");
		
		//Session Errors
		TTPORTALERRORCODES.put("SS:1111", "Error in retreving the user Unique Id");
		
		
	}
	
	public static String getErrorMessageByCode(String errCode){
		
		return TTPORTALERRORCODES.get(errCode);
	}
	
	public static String getFatalErrorMessage(){
		
		return TTPORTALERRORCODES.get("5555");
	}
	
	
}
