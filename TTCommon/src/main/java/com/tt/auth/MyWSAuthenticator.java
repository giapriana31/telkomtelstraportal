package com.tt.auth;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

public class MyWSAuthenticator  extends Authenticator{
	
	private String user;
	private String password;

	public MyWSAuthenticator(String user, String password) {
		this.user = user;
		this.password = password;
	}

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		PasswordAuthentication auth = new PasswordAuthentication(user,
				password.toCharArray());
		return auth;
	}


}
