package com.tt.exception;

import com.tt.constants.TTPortalAppErrors;
import com.tt.logging.Logger;

public class TTPortalAppException extends GenericException{
	
	private final static Logger log = Logger.getLogger(TTPortalAppException.class);
	private static final long serialVersionUID = 8470109645527810916L;
	private Exception e = null;
	
	//TODO: Put a generic logic to logging the erro/exception in logger instead at each location
		
	public TTPortalAppException(String errCode, String errMsg) {
		super(errCode, errMsg);
		log.error(errMsg);
		// TODO Auto-generated constructor stub
	}	
	
	public TTPortalAppException(Exception e, String errCode, String errMsg) {
		super(errCode, errMsg);
		log.error(e.getMessage());
		this.e = e;
	}
	
	public TTPortalAppException(Exception e) {		
		super("SY:5555", TTPortalAppErrors.getErrorMessageByCode("SY:5555"));
		log.error(e.getMessage());
		this.e = e;
	}

}
