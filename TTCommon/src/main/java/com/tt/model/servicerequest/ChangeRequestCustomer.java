package com.tt.model.servicerequest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "changerequestcustomer")
public class ChangeRequestCustomer {

	private String customeruniqueid;
	private String customername;
	private ChangeRequest changerequestid;
	private Long id;

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomeruniqueid() {
		return customeruniqueid;
	}

	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	@ManyToOne
	@JoinColumn(name = "changerequestid", referencedColumnName = "changerequestid")
	public ChangeRequest getChangerequestid() {
		return changerequestid;
	}

	public void setChangerequestid(ChangeRequest changerequestid) {
		this.changerequestid = changerequestid;
	}

}
