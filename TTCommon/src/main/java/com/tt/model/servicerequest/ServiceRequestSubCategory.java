package com.tt.model.servicerequest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="servicerequestsubcategory")
public class ServiceRequestSubCategory {
	@Id
	private Integer id;
	private String category;
	@Column(name="subcategory")
	private String subCategory;
	private String active;
	private String cmdbClass;
	private String rootProductId;
	
	public String getRootProductId() {
		return rootProductId;
	}
	public void setRootProductId(String rootProductId) {
		this.rootProductId = rootProductId;
	}
	public String getCmdbClass() {
		return cmdbClass;
	}
	public void setCmdbClass(String cmdbClass) {
		this.cmdbClass = cmdbClass;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	
	
	
}
