package com.tt.model.servicerequest;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "servicerequest")
public class ServiceRequest {
    
    private String requestid;
    private String subject;
    private String description;
    private Date startdate;
    private Date enddate;
    private String requestor;
    private String status;
    private Date createddate;
    private Date lastupdatedate;
    private String createdby;
    private String lastupdatedby;
    private String customeruniqueid;
    private List<ServiceRequestItem> serviceRequestItem;    
    private String requesttype;
   /* private Date plannedstartdate;
    private Date plannedenddate;
    private Date outagestarttime;
    private Date outageendtime;*/
    
    /*private String category;
    private String risk;
    private String devicename;
    private String priority;
    private String impact;
    private String resolvergroup;
    private String casemanager;
    private String comments;
    private String outagepresent; 
    private String servicename;
    private String sitename;*/
    
    
    /*public Date getPlannedstartdate() {
		return plannedstartdate;
	}
	public void setPlannedstartdate(Date plannedstartdate) {
		this.plannedstartdate = plannedstartdate;
	}
	public Date getPlannedenddate() {
		return plannedenddate;
	}
	public void setPlannedenddate(Date plannedenddate) {
		this.plannedenddate = plannedenddate;
	}
	public Date getOutagestarttime() {
		return outagestarttime;
	}
	public void setOutagestarttime(Date outagestarttime) {
		this.outagestarttime = outagestarttime;
	}
	public Date getOutageendtime() {
		return outageendtime;
	}
	public void setOutageendtime(Date outageendtime) {
		this.outageendtime = outageendtime;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getRisk() {
		return risk;
	}
	public void setRisk(String risk) {
		this.risk = risk;
	}
	public String getDevicename() {
		return devicename;
	}
	public void setDevicename(String devicename) {
		this.devicename = devicename;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	public String getResolvergroup() {
		return resolvergroup;
	}
	public void setResolvergroup(String resolvergroup) {
		this.resolvergroup = resolvergroup;
	}
	public String getCasemanager() {
		return casemanager;
	}
	public void setCasemanager(String casemanager) {
		this.casemanager = casemanager;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getOutagepresent() {
		return outagepresent;
	}
	public void setOutagepresent(String outagepresent) {
		this.outagepresent = outagepresent;
	}
	
	public String getServicename() {
		return servicename;
	}
	public void setServicename(String servicename) {
		this.servicename = servicename;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}*/
    
    
	public String getRequesttype() {
		return requesttype;
	}
	public void setRequesttype(String requesttype) {
		this.requesttype = requesttype;
	}
	@Id
    @Column(name="requestid")
    public String getRequestid() {
        return requestid;
    }
    public void setRequestid(String requestid) {
        this.requestid = requestid;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Date getStartdate() {
        return startdate;
    }
    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }
    public Date getEnddate() {
        return enddate;
    }
    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }
    public String getRequestor() {
        return requestor;
    }
    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public Date getCreateddate() {
        return createddate;
    }
    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }
    public Date getLastupdatedate() {
        return lastupdatedate;
    }
    public void setLastupdatedate(Date lastupdatedate) {
        this.lastupdatedate = lastupdatedate;
    }
    public String getCreatedby() {
        return createdby;
    }
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }
    public String getLastupdatedby() {
        return lastupdatedby;
    }
    public void setLastupdatedby(String lastupdatedby) {
        this.lastupdatedby = lastupdatedby;
    }
    public String getCustomeruniqueid() {
        return customeruniqueid;
    }
    public void setCustomeruniqueid(String customeruniqueid) {
        this.customeruniqueid = customeruniqueid;
    }
    @OneToMany(mappedBy = "servicerequestid", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    public List<ServiceRequestItem> getServiceRequestItem() {
        return serviceRequestItem;
    }
    public void setServiceRequestItem(List<ServiceRequestItem> serviceRequestItem) {
        this.serviceRequestItem = serviceRequestItem;
    }
    
    

}
