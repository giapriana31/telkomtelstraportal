package com.tt.model.servicerequest;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
@Table(name="servicerequestitem")
public class ServiceRequestItem {

    
    private String itemid;
    private ServiceRequest servicerequestid;
    private String projecttype;
    private String status;
    private Date lastupdate;
    private Date createddate;
    private String category;
    private String subcategory;
    private String createdby;
    private String lastupdatedby;
    private String sitename;
    private String servicename;
    private String itemtype;
    private List<ServiceRequestItemDevices> serviceRequestItemDevicesList; 
    private List<ServiceRequestItemNotes> serviceRequestItemNotesList;
    
    @Id
    @Column(name="itemid")
    public String getItemid() {
        return itemid;
    }
    public void setItemid(String itemid) {
        this.itemid = itemid;
    }
    
    @Column(name="itemtype")
    public String getItemtype() {
		return itemtype;
	}
	public void setItemtype(String itemtype) {
		this.itemtype = itemtype;
	}
    
    @ManyToOne
    @JoinColumn(name="servicerequestid", referencedColumnName="requestid")
    public ServiceRequest getServicerequestid() {
        return servicerequestid;
    }
    public void setServicerequestid(ServiceRequest servicerequestid) {
        this.servicerequestid = servicerequestid;
    }
    
    @Column(name="projecttype")
    public String getProjecttype() {
        return projecttype;
    }
    public void setProjecttype(String projecttype) {
        this.projecttype = projecttype;
    }
    
    @Column(name = "sristatus")
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    
    @Column(name = "srilastupdate")
    public Date getLastupdate() {
        return lastupdate;
    }
    public void setLastupdate(Date lastupdate) {
        this.lastupdate = lastupdate;
    }
    
    @Column(name = "sricreateddate")
    public Date getCreateddate() {
        return createddate;
    }
    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getSubcategory() {
        return subcategory;
    }
    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }
    
    @Column(name = "sricreatedby")
    public String getCreatedby() {
        return createdby;
    }
    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }
    
    @Column(name = "srilastupdatedby")
    public String getLastupdatedby() {
        return lastupdatedby;
    }
    public void setLastupdatedby(String lastupdatedby) {
        this.lastupdatedby = lastupdatedby;
    }
    public String getSitename() {
        return sitename;
    }
    public void setSitename(String sitename) {
        this.sitename = sitename;
    }
    public String getServicename() {
        return servicename;
    }
    public void setServicename(String servicename) {
        this.servicename = servicename;
    }
    
    @OneToMany(mappedBy = "itemid", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<ServiceRequestItemDevices> getServiceRequestItemDevicesList() {
        return serviceRequestItemDevicesList;
    }
    public void setServiceRequestItemDevicesList(
    	List<ServiceRequestItemDevices> serviceRequestItemDevicesList) {
        this.serviceRequestItemDevicesList = serviceRequestItemDevicesList;
    }
    @OneToMany(mappedBy = "itemid", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<ServiceRequestItemNotes> getServiceRequestItemNotesList() {
        return serviceRequestItemNotesList;
    }
    public void setServiceRequestItemNotesList(
    	List<ServiceRequestItemNotes> serviceRequestItemNotesList) {
        this.serviceRequestItemNotesList = serviceRequestItemNotesList;
    }
}
