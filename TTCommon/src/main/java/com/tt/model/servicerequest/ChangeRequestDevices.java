package com.tt.model.servicerequest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="changerequestdevices")
public class ChangeRequestDevices {
	
	private Long id;
	private String ciid;
	private String ciname;
    private ChangeRequest changerequestid;
   
    
    @Id
    @GeneratedValue
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
    
    public String getCiid() {
		return ciid;
	}
	public void setCiid(String ciid) {
		this.ciid = ciid;
	}
	
	
	public String getCiname() {
		return ciname;
	}
	public void setCiname(String ciname) {
		this.ciname = ciname;
	}
	
	
	@ManyToOne
    @JoinColumn(name="changerequestid", referencedColumnName="changerequestid")
	public ChangeRequest getChangerequestid() {
		return changerequestid;
	}
	public void setChangerequestid(ChangeRequest changerequestid) {
		this.changerequestid = changerequestid;
	}    
   
}
