package com.tt.model.servicerequest;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class is for Change Request which are approved by telkomtelstra.
 *
 */

@Entity
@Table(name = "changerequestapprovals")
public class ChangeRequestApprovals {
	private String approver;
	private Date approvaldate;
	private String approvalgroup;
	private ChangeRequest changerequestid;
	private Long id;

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public Date getApprovaldate() {
		return approvaldate;
	}

	public void setApprovaldate(Date approvaldate) {
		this.approvaldate = approvaldate;
	}

	public String getApprovalgroup() {
		return approvalgroup;
	}

	public void setApprovalgroup(String approvalgroup) {
		this.approvalgroup = approvalgroup;
	}

	@ManyToOne
	@JoinColumn(name = "changerequestid", referencedColumnName = "changerequestid")
	public ChangeRequest getChangerequestid() {
		return changerequestid;
	}

	public void setChangerequestid(ChangeRequest changerequestid) {
		this.changerequestid = changerequestid;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
