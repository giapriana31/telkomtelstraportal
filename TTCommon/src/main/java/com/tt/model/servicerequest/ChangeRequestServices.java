package com.tt.model.servicerequest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="changerequestservices")
public class ChangeRequestServices {
	
	private Long id;
	private String serviceid;
	private String servicename;
	private ChangeRequest changerequestid;

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	@ManyToOne
    @JoinColumn(name="changerequestid", referencedColumnName="changerequestid")
	public ChangeRequest getChangerequestid() {
		return changerequestid;
	}

	public void setChangerequestid(ChangeRequest changerequestid) {
		this.changerequestid = changerequestid;
	}
}
