package com.tt.model.servicerequest;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "changerequestnotes")
public class ChangeRequestNotes {

	private Long id;
	private ChangeRequest changerequestid;
    private String userid;
    private Date createddatetime;
    private String notes;
    
    
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "changerequestid", referencedColumnName = "changerequestid")
	public ChangeRequest getChangerequestid() {
		return changerequestid;
	}

	public void setChangerequestid(ChangeRequest changerequestid) {
		this.changerequestid = changerequestid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Date getCreateddatetime() {
		return createddatetime;
	}

	public void setCreateddatetime(Date createddatetime) {
		this.createddatetime = createddatetime;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	
}
