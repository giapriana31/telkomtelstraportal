package com.tt.model.servicerequest;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="servicerequestitemdevices")
public class ServiceRequestItemDevices {

    
    private Long id;
    private ServiceRequestItem itemid;
    private String ciid;
    private String ciname;
    
    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name="itemid", referencedColumnName="itemid")
    public ServiceRequestItem getItemid() {
        return itemid;
    }
    public void setItemid(ServiceRequestItem itemid) {
        this.itemid = itemid;
    }
    public String getCiid() {
        return ciid;
    }
    public void setCiid(String ciid) {
        this.ciid = ciid;
    }
    public String getCiname() {
        return ciname;
    }
    public void setCiname(String ciname) {
        this.ciname = ciname;
    }

}
