package com.tt.model.servicerequest;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="servicerequestnotes")
public class ServiceRequestItemNotes {

    private Long id;
    private ServiceRequestItem itemid;
    private String username;
    private String userid;
    private String custinfonotes;
    private Date modifiedtime;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name="itemid", referencedColumnName="itemid")
    public ServiceRequestItem getItemid() {
        return itemid;
    }
    public void setItemid(ServiceRequestItem itemid) {
        this.itemid = itemid;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
    public String getCustinfonotes() {
        return custinfonotes;
    }
    public void setCustinfonotes(String custinfonotes) {
        this.custinfonotes = custinfonotes;
    }
    public Date getModifiedtime() {
        return modifiedtime;
    }
    public void setModifiedtime(Date modifiedtime) {
        this.modifiedtime = modifiedtime;
    }
}
