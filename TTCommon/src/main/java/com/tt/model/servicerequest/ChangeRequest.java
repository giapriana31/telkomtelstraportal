package com.tt.model.servicerequest;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "changerequest")
public class ChangeRequest {

	private String changerequestid;
	private String subject;
	private String description;
	private String requestor;
	private String status;
	private Date createddate;
	private Date lastupdatedate;
	private String category;
	private Date plannedstartdate;
	private Date plannedenddate;
	private String risk;
	private String priority;
	private String resolvergroup;
	private String casemanager;
	private String comments;
	private String outagepresent;
	private Date outagestarttime;
	private Date outageendtime;
	private String impact;
	private String sitename;
	private String changesource;
	private String changetype;
	private String customerapproval;
	private String customerapprover;
	private Date customerapprovaldate;
	private String customerapprovalstatus;
	private Date requestedbydate;
	private String outcomestatus;

	private List<ChangeRequestCustomer> changeRequestCustomer;
	private List<ChangeRequestDevices> changeRequestDevices;
	private List<ChangeRequestServices> changeRequestServices;
	private List<ChangeRequestNotes> changeRequestNotes;
	private List<ChangeRequestApprovals> changeRequestApprovals;

	@OneToMany(mappedBy = "changerequestid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List<ChangeRequestDevices> getChangeRequestDevices() {
		return changeRequestDevices;
	}

	public void setChangeRequestDevices(
			List<ChangeRequestDevices> changeRequestDevices) {
		this.changeRequestDevices = changeRequestDevices;
	}

	@OneToMany(mappedBy = "changerequestid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List<ChangeRequestServices> getChangeRequestServices() {
		return changeRequestServices;
	}

	public void setChangeRequestServices(
			List<ChangeRequestServices> changeRequestServices) {
		this.changeRequestServices = changeRequestServices;
	}

	@OneToMany(mappedBy = "changerequestid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List<ChangeRequestCustomer> getChangeRequestCustomer() {
		return changeRequestCustomer;
	}

	public void setChangeRequestCustomer(
			List<ChangeRequestCustomer> changeRequestCustomers) {
		this.changeRequestCustomer = changeRequestCustomers;
	}

	@OneToMany(mappedBy = "changerequestid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List<ChangeRequestNotes> getChangeRequestNotes() {
		return changeRequestNotes;
	}

	public void setChangeRequestNotes(
			List<ChangeRequestNotes> changeRequestNotes) {
		this.changeRequestNotes = changeRequestNotes;
	}

	@OneToMany(mappedBy = "changerequestid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List<ChangeRequestApprovals> getChangeRequestApprovals() {
		return changeRequestApprovals;
	}

	public void setChangeRequestApprovals(
			List<ChangeRequestApprovals> changeRequestApprovals) {
		this.changeRequestApprovals = changeRequestApprovals;
	}

	public Date getPlannedstartdate() {
		return plannedstartdate;
	}

	public void setPlannedstartdate(Date plannedstartdate) {
		this.plannedstartdate = plannedstartdate;
	}

	public Date getPlannedenddate() {
		return plannedenddate;
	}

	public void setPlannedenddate(Date plannedenddate) {
		this.plannedenddate = plannedenddate;
	}

	public Date getOutagestarttime() {
		return outagestarttime;
	}

	public void setOutagestarttime(Date outagestarttime) {
		this.outagestarttime = outagestarttime;
	}

	public Date getOutageendtime() {
		return outageendtime;
	}

	public void setOutageendtime(Date outageendtime) {
		this.outageendtime = outageendtime;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getResolvergroup() {
		return resolvergroup;
	}

	public void setResolvergroup(String resolvergroup) {
		this.resolvergroup = resolvergroup;
	}

	public String getCasemanager() {
		return casemanager;
	}

	public void setCasemanager(String casemanager) {
		this.casemanager = casemanager;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getOutagepresent() {
		return outagepresent;
	}

	public void setOutagepresent(String outagepresent) {
		this.outagepresent = outagepresent;
	}

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	@Id
	@Column(name = "changerequestid")
	public String getChangerequestid() {
		return changerequestid;
	}

	public void setChangerequestid(String changerequestid) {
		this.changerequestid = changerequestid;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getLastupdatedate() {
		return lastupdatedate;
	}

	public void setLastupdatedate(Date lastupdatedate) {
		this.lastupdatedate = lastupdatedate;
	}

	public String getChangesource() {
		return changesource;
	}

	public void setChangesource(String changesource) {
		this.changesource = changesource;
	}

	public String getChangetype() {
		return changetype;
	}

	public void setChangetype(String changetype) {
		this.changetype = changetype;
	}

	public String getCustomerapprover() {
		return customerapprover;
	}

	public void setCustomerapprover(String customerapprover) {
		this.customerapprover = customerapprover;
	}

	public Date getCustomerapprovaldate() {
		return customerapprovaldate;
	}

	public void setCustomerapprovaldate(Date customerapproverdate) {
		this.customerapprovaldate = customerapproverdate;
	}

	public Date getRequestedbydate() {
		return requestedbydate;
	}

	public void setRequestedbydate(Date requestedbydate) {
		this.requestedbydate = requestedbydate;
	}

	public String getOutcomestatus() {
		return outcomestatus;
	}

	public void setOutcomestatus(String outcomestatus) {
		this.outcomestatus = outcomestatus;
	}
	
	public String getCustomerapproval() {
		return customerapproval;
	}
	
	public void setCustomerapproval(String customerapproval) {
		this.customerapproval = customerapproval;
	}

	public String getCustomerapprovalstatus() {
		return customerapprovalstatus;
	}

	public void setCustomerapprovalstatus(String customerapprovalstatus) {
		this.customerapprovalstatus = customerapprovalstatus;
	}
}
