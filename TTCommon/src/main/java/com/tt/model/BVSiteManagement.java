package com.tt.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bvsitemanagement")
public class BVSiteManagement {
	private Long id;
	private String region;
	private String sitename;
	private String servicetier;
	private String status;
	private String performance;
	private String NetworkAverage;
	private String NetworkPeak;
	private String CPEAverage;
	private String CPEPeak;
	private String customerId;
	private String commitment;
	
	
	

	public String getCommitment() {
		return commitment;
	}

	public void setCommitment(String commitment) {
		this.commitment = commitment;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getServicetier() {
		return servicetier;
	}

	public void setServicetier(String servicetier) {
		this.servicetier = servicetier;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPerformance() {
		return performance;
	}

	public void setPerformance(String performance) {
		this.performance = performance;
	}

	public String getNetworkAverage() {
		return NetworkAverage;
	}

	public void setNetworkAverage(String networkAverage) {
		NetworkAverage = networkAverage;
	}

	public String getNetworkPeak() {
		return NetworkPeak;
	}

	public void setNetworkPeak(String networkPeak) {
		NetworkPeak = networkPeak;
	}

	public String getCPEAverage() {
		return CPEAverage;
	}

	public void setCPEAverage(String cPEAverage) {
		CPEAverage = cPEAverage;
	}

	public String getCPEPeak() {
		return CPEPeak;
	}

	public void setCPEPeak(String cPEPeak) {
		CPEPeak = cPEPeak;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
