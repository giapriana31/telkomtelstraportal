package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="deliverymetadata")
public class DeliveryEVMetadata {

	private String id;
	private String monthWindow;
	private String customerUniqueId;
	private String cmdbClass;
	private String totalCount;
	private String operationalSiteCount;
	private String deliveryActivationSitesCount;
	private String monitoringSitesCount;
	private String detailedDesignSitesCount;
	private String procurementSitesCount;
	private String orderreceivedSitesCount;
	
	
	
	
	
	

	@Column(name="deliveryactivationsitescount")
	public String getDeliveryActivationSitesCount() {
		return deliveryActivationSitesCount;
	}
	public void setDeliveryActivationSitesCount(String deliveryActivationSitesCount) {
		this.deliveryActivationSitesCount = deliveryActivationSitesCount;
	}
	
	@Column(name="monitoringsitescount")
	public String getMonitoringSitesCount() {
		return monitoringSitesCount;
	}
	public void setMonitoringSitesCount(String monitoringSitesCount) {
		this.monitoringSitesCount = monitoringSitesCount;
	}
	
	@Column(name="detaileddesignsitescount")
	public String getDetailedDesignSitesCount() {
		return detailedDesignSitesCount;
	}
	public void setDetailedDesignSitesCount(String detailedDesignSitesCount) {
		this.detailedDesignSitesCount = detailedDesignSitesCount;
	}
	
	@Column(name="procurementsitescount")
	public String getProcurementSitesCount() {
		return procurementSitesCount;
	}
	public void setProcurementSitesCount(String procurementSitesCount) {
		this.procurementSitesCount = procurementSitesCount;
	}
	
	@Column(name="orderreceivedsitescount")
	public String getOrderreceivedSitesCount() {
		return orderreceivedSitesCount;
	}
	public void setOrderreceivedSitesCount(String orderreceivedSitesCount) {
		this.orderreceivedSitesCount = orderreceivedSitesCount;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Column(name="monthwindow")
	public String getMonthWindow() {
		return monthWindow;
	}
	public void setMonthWindow(String monthWindow) {
		this.monthWindow = monthWindow;
	}
	
	@Column(name="customeruniqueid")
	public String getCustomerUniqueId() {
		return customerUniqueId;
	}
	public void setCustomerUniqueId(String customerUniqueId) {
		this.customerUniqueId = customerUniqueId;
	}
	
	@Column(name="cmdbclass")
	public String getCmdbClass() {
		return cmdbClass;
	}
	public void setCmdbClass(String cmdbClass) {
		this.cmdbClass = cmdbClass;
	}
	
	@Column(name="operationalsitescount")
	public String getOperationalSiteCount() {
		return operationalSiteCount;
	}
	public void setOperationalSiteCount(String operationalSiteCount) {
		this.operationalSiteCount = operationalSiteCount;
	}
	
	@Column(name="totalcount")
	public String getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}
	
	
}
