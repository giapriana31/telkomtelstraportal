package com.tt.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="networkperformancemetrics")
public class NetworkMapSite {
	
	private Long id;
	private String customeruniqueid;
	private NetworkProvince networksiteprovince;
	private String networkprovincecolor;
	private int countredsite;
	private int countambersite;
	private int countgreensite;
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
	
	@OneToOne
	@JoinColumn(name = "networksiteprovince", referencedColumnName="networksiteprovince")
	public NetworkProvince getNetworksiteprovince() {
		return networksiteprovince;
	}
	public void setNetworksiteprovince(NetworkProvince networksiteprovince) {
		this.networksiteprovince = networksiteprovince;
	}
	
	public String getNetworkprovincecolor() {
		return networkprovincecolor;
	}
	public void setNetworkprovincecolor(String networkprovincecolor) {
		this.networkprovincecolor = networkprovincecolor;
	}
	public int getCountredsite() {
		return countredsite;
	}
	public void setCountredsite(int countredsite) {
		this.countredsite = countredsite;
	}
	public int getCountambersite() {
		return countambersite;
	}
	public void setCountambersite(int countambersite) {
		this.countambersite = countambersite;
	}
	public int getCountgreensite() {
		return countgreensite;
	}
	public void setCountgreensite(int countgreensite) {
		this.countgreensite = countgreensite;
	}

	
}
