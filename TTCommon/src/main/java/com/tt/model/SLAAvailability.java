package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sitesavailability")
public class SLAAvailability {

	private String customer_ID;
	private String site_name;
	private String site_tier;
	private String avg_avail;
	private String site_percentile_average;
	private String site_color;
	private String monthyear;

	@Id
	@Column(name="site_name")
	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	@Column(name="site_tier")
	public String getSite_tier() {
		return site_tier;
	}
	public void setSite_tier(String site_tier) {
		this.site_tier = site_tier;
	}

	@Column(name="avg_avail")
	public String getAvg_avail() {
		return avg_avail;
	}
	public void setAvg_avail(String avg_avail) {
		this.avg_avail = avg_avail;
	}

	@Column(name="site_percentile_average")
	public String getSite_percentile_average() {
		return site_percentile_average;
	}
	public void setSite_percentile_average(String site_percentile_average) {
		this.site_percentile_average = site_percentile_average;
	}

	@Column(name="site_color")
	public String getSite_color() {
		return site_color;
	}
	public void setSite_color(String site_color) {
		this.site_color = site_color;
	}

	@Column(name="Customer_ID")
	public String getCustomer_ID() {
		return customer_ID;
	}
	public void setCustomer_ID(String customer_ID) {
		this.customer_ID = customer_ID;
	}
	
	public String getMonthyear() {
		return monthyear;
	}
	public void setMonthyear(String monthyear) {
		this.monthyear = monthyear;
	}
}