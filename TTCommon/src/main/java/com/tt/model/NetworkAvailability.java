package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="networkavailability")
public class NetworkAvailability {

	
	private String customerId;
	private String avgAvail;
	private String color;
	private String monthyear;
	
	@Id
	@Column(name="CUSTOMER_ID")
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="AVG_AVAIL")
	public String getAvgAvail() {
		return avgAvail;
	}
	public void setAvgAvail(String avgAvail) {
		this.avgAvail = avgAvail;
	}
	
	@Column(name="NETWORK_COLOR")
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public String getMonthyear() {
		return monthyear;
	}
	public void setMonthyear(String monthyear) {
		this.monthyear = monthyear;
	}
	
	
}
