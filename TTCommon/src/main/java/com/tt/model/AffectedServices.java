package com.tt.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "affectedoutageservices")
public class AffectedServices {
	
	private Long id;
	private Outage serviceoutageid;
	private String ciid;
	private String customerid;
	private String siteid;
	private String cmdbclass;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "serviceoutageid", referencedColumnName = "serviceoutageid")
	public Outage getServiceoutageid() {
		return serviceoutageid;
	}

	public void setServiceoutageid(Outage serviceoutageid) {
		this.serviceoutageid = serviceoutageid;
	}

	public String getCiid() {
		return ciid;
	}

	public void setCiid(String ciid) {
		this.ciid = ciid;
	}

	public String getCustomerId() {
		return customerid;
	}

	public void setCustomerId(String customerId) {
		this.customerid = customerId;
	}

	public String getSiteId() {
		return siteid;
	}

	public void setSiteId(String siteId) {
		this.siteid = siteId;
	}

	public String getCmdbClass() {
		return cmdbclass;
	}

	public void setCmdbClass(String cmdbClass) {
		this.cmdbclass = cmdbClass;
	}

}
