package com.tt.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bvsubscribedservices")
public class BVSubscribedServices {
	
	private Long id;
	private String productname;
	private String contractduration;
	private String contractremainingtime;
	private String status;
	private String performance;
	private String contact;
	private String customeruniqueid;
	private String capacity;
	private String commitment;
	
	


	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getCommitment() {
		return commitment;
	}

	public void setCommitment(String commitment) {
		this.commitment = commitment;
	}

	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getContractduration() {
		return contractduration;
	}

	public void setContractduration(String contractduration) {
		this.contractduration = contractduration;
	}

	public String getContractremainingtime() {
		return contractremainingtime;
	}

	public void setContractremainingtime(String contractremainingtime) {
		this.contractremainingtime = contractremainingtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPerformance() {
		return performance;
	}

	public void setPerformance(String performance) {
		this.performance = performance;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getCustomeruniqueid() {
		return customeruniqueid;
	}

	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
}
