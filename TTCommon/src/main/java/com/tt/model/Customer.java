/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Infosys
 *
 */
@Entity
@Table(name ="customer")
public class Customer
{
	@Id
	private String customerUniqueId;
	
	@Column(name="country")
	private String country;
	
	@Column(name="fiscalyear")
	private String fiscalYear;
	
	@Column(name="accountName")
	private String accountName;
	
	@Column(name="state")
	private String state;
	
	@Column(name="createdBy")
	private String createdBy;
	
	@Column(name="createdDate")
	private Date createdDate;
	
	@Column(name="updatedBy")
	private String updatedBy;
	
	@Column(name="updatedDate")
	private Date updatedDate;
	
	@Column(name="bronzeavailabilitysla")
	private Long bronzeAvailabilitySLA;

	@Column(name="description")
	private String description;
	
	@Column(name="goldAvailabilitySLA")
	private Long goldAvailabilitySLA;

	@Column(name="silverAvailabilitySLA")
	private Long silverAvailabilitySLA;

	@Column(name="active")
	private String active; 
	
	@Column(name="status")
	private String status;
	
	@Column(name="customertype")
	private String customerType;
	
    @Column(name = "numberofoktausers")
    private Integer numberOfOktaUsers;
    
    
    private BigDecimal managedAvailability;
    
    
    

    @Column(name="fullymanagedcontractedavailability")
	public BigDecimal getManagedAvailability() {
		return managedAvailability;
	}

	public void setManagedAvailability(BigDecimal managedAvailability) {
		this.managedAvailability = managedAvailability;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCustomerUniqueId()
	{
		return customerUniqueId;
	}

	public void setCustomerUniqueId(String customerUniqueId)
	{
		this.customerUniqueId = customerUniqueId;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getFiscalYear()
	{
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear)
	{
		this.fiscalYear = fiscalYear;
	}

	public String getAccountName()
	{
		return accountName;
	}

	public void setAccountName(String accountName)
	{
		this.accountName = accountName;
	}

	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public String getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate()
	{
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate)
	{
		this.updatedDate = updatedDate;
	}

	public Long getBronzeAvailabilitySLA()
	{
		return bronzeAvailabilitySLA;
	}

	public void setBronzeAvailabilitySLA(Long bronzeAvailabilitySLA)
	{
		this.bronzeAvailabilitySLA = bronzeAvailabilitySLA;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Long getGoldAvailabilitySLA()
	{
		return goldAvailabilitySLA;
	}

	public void setGoldAvailabilitySLA(Long goldAvailabilitySLA)
	{
		this.goldAvailabilitySLA = goldAvailabilitySLA;
	}

	public Long getSilverAvailabilitySLA()
	{
		return silverAvailabilitySLA;
	}

	public void setSilverAvailabilitySLA(Long silverAvailabilitySLA)
	{
		this.silverAvailabilitySLA = silverAvailabilitySLA;
	}

	public String getActive()
	{
		return active;
	}

	public void setActive(String active)
	{
		this.active = active;
	}


    public Integer getNumberOfOktaUsers() {
		return numberOfOktaUsers;
	}

	public void setNumberOfOktaUsers(Integer numberOfOktaUsers) {
		this.numberOfOktaUsers = numberOfOktaUsers;
	}
	
}
