package com.tt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "ten2infi")
public class Tenoss {
	
	@Id
	@Column(name="TenId")
	private int tenId;
	
	@Column(name="CustomerUniqueId")
	private String customerUniqueId;
	
	@Column(name="CustomerName")
	private String customerName;
	
	@Column(name="SiteId")
	private String siteId;
	
	@Column(name="SiteName")
	private String siteName;
	
	@Column(name="CIID")
	private String ciid;
	
	@Column(name="CIName")
	private String ciName;
	
	@Column(name="CarriageServiceId")
	private String carriageServiceId;
	
	@Column(name="CIDeliveryStatus")
	private String ciDeliveryStatus;
	
	@Column(name="NSOrder_TQ")
	private String nsOrderTq;
	
	@Column(name="NSOrder_AO")
	private String nsOrderAo;
	
	@Column(name="Status_TICARES")
	private String statusTicares;
	
	@Column(name="Task_Name_TICARES")
	private String taskNameTicares;
	
	@Column(name="Task_Stage_TENOSS")
	private String taskStageTenoss;
	
	@Column(name="Task_Status_TENOSS")
	private String taskStatusTenoss;
	
	@Column(name="Task_Name_TENOSS")
	private String taskNameTenoss;
	
	@Column(name="Modified_By")
	private String modifiedBy;
	
	@Column(name="Modified_Date")
	private Date modifiedDate;
	
	@Column(name="TargetProvisioningDate")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date targetProvisioningDate;
	
	@Column(name="TargetInstallDate")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date targetInstallDate;

	@Column(name="BatchDate")
	private Date batchDate;
	
	@Column(name="SRIReferenceNumber")
	private String sriReferenceNumber;
	
	
	public int getTenId() {
		return tenId;
	}
	
	public void setTenId(int tenId) {
		this.tenId = tenId;
	}
	
	public String getCustomerUniqueId() {
		return customerUniqueId;
	}
	public void setCustomerUniqueId(String customerUniqueId) {
		this.customerUniqueId = customerUniqueId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getSiteId() {
		return siteId;
	}
	
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	public String getSiteName() {
		return siteName;
	}
	
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	
	public String getCiid() {
		return ciid;
	}
	
	public void setCiid(String ciid) {
		this.ciid = ciid;
	}
	
	public String getCiName() {
		return ciName;
	}
	
	public void setCiName(String ciName) {
		this.ciName = ciName;
	}
	
	public String getCarriageServiceId() {
		return carriageServiceId;
	}
	
	public void setCarriageServiceId(String carriageServiceId) {
		this.carriageServiceId = carriageServiceId;
	}
	
	public String getCiDeliveryStatus() {
		return ciDeliveryStatus;
	}
	
	public void setCiDeliveryStatus(String ciiDeliveryStatus) {
		this.ciDeliveryStatus = ciiDeliveryStatus;
	}
	
	public String getNsOrderTq() {
		return nsOrderTq;
	}
	
	public void setNsOrderTq(String nsOrderTq) {
		this.nsOrderTq = nsOrderTq;
	}
	
	public String getNsOrderAo() {
		return nsOrderAo;
	}
	
	public void setNsOrderAo(String nsOrderAo) {
		this.nsOrderAo = nsOrderAo;
	}
	
	public String getStatusTicares() {
		return statusTicares;
	}
	
	public void setStatusTicares(String statusTicares) {
		this.statusTicares = statusTicares;
	}
	
	public String getTaskNameTicares() {
		return taskNameTicares;
	}
	
	public void setTaskNameTicares(String taskNameTicares) {
		this.taskNameTicares = taskNameTicares;
	}
	
	public String getTaskStageTenoss() {
		return taskStageTenoss;
	}
	
	public void setTaskStageTenoss(String taskStageTenoss) {
		this.taskStageTenoss = taskStageTenoss;
	}
	
	public String getTaskStatusTenoss() {
		return taskStatusTenoss;
	}
	
	public void setTaskStatusTenoss(String taskStatusTenoss) {
		this.taskStatusTenoss = taskStatusTenoss;
	}
	
	public String getTaskNameTenoss() {
		return taskNameTenoss;
	}
	
	public void setTaskNameTenoss(String taskNameTenoss) {
		this.taskNameTenoss = taskNameTenoss;
	}
	
	public String getModifiedBy() {
		return modifiedBy;
	}
	
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public Date getModifiedDate() {
		return modifiedDate;
	}
	
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public Date getTargetProvisioningDate() {
		return targetProvisioningDate;
	}

	public void setTargetProvisioningDate(Date targetProvisioningDate) {
		this.targetProvisioningDate = targetProvisioningDate;
	}

	public Date getTargetInstallDate() {
		return targetInstallDate;
	}

	public void setTargetInstallDate(Date targetInstallDate) {
		this.targetInstallDate = targetInstallDate;
	}
	
	public Date getBatchDate() {
		return batchDate;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = batchDate;
	}
	
	public String getSriReferenceNumber() {
		return sriReferenceNumber;
	}

	public void setSriReferenceNumber(String sriReferenceNumber) {
		this.sriReferenceNumber = sriReferenceNumber;
	}
}
