package com.tt.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SecondaryTable;
import javax.persistence.Table;

@Entity
@Table(name="site")
@SecondaryTable(name="allsites")
public class Site {

	private String city;
	private String customername;
	private String customercontact;
	private String country;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private String customersite;
	private String stateprovince;
	private String street;
	private String createdby;
	private Date createddate;
	private String sysupdatedby;
	private Date sysupdatedon;
	private String timezone;
	private String zippostalcode;
	private String siteid;
	private String zone;
	private String servicetier;
	private String siteoperationalhours;
	private String active;
	private String customeruniqueid;
	private String faxphone;
	private String parent;
	private String phone;
	private String Created_Alias;
	private String Created_By;
	private String Created_Date;
	private String Customer_Contact;
	private String Customer_Name;
	private String Fax;
	private String ID;
	private String Last_Activity_Date;
	private String Last_Modified_Alias;
	private String 	Last_Modified_By;
	private String 	Last_Modified_Date;
	private String Owner_Alias;
	private String 	Owner_Name;
	private String 	Owner_Role;
	private String Service_Tier;
	private String 	Site_operational_hours;
	private String 	Time_Zone;
	private String 	status;

    private String isDataCenter;
    private String dataCenterType;
    private String dataCenterName;
    private String isManaged;
    
    @Column(name = "rootproductid")
    private String rootProductId;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getCustomercontact() {
		return customercontact;
	}
	public void setCustomercontact(String customercontact) {
		this.customercontact = customercontact;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public String getCustomersite() {
		return customersite;
	}
	public void setCustomersite(String customersite) {
		this.customersite = customersite;
	}
	public String getStateprovince() {
		return stateprovince;
	}
	public void setStateprovince(String stateprovince) {
		this.stateprovince = stateprovince;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public String getSysupdatedby() {
		return sysupdatedby;
	}
	public void setSysupdatedby(String sysupdatedby) {
		this.sysupdatedby = sysupdatedby;
	}
	public Date getSysupdatedon() {
		return sysupdatedon;
	}
	public void setSysupdatedon(Date sysupdatedon) {
		this.sysupdatedon = sysupdatedon;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getZippostalcode() {
		return zippostalcode;
	}
	public void setZippostalcode(String zippostalcode) {
		this.zippostalcode = zippostalcode;
	}
	@Id
	@Column(name="siteid")
	public String getSiteid() {
		return siteid;
	}
	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getServicetier() {
		return servicetier;
	}
	public void setServicetier(String servicetier) {
		this.servicetier = servicetier;
	}
	public String getSiteoperationalhours() {
		return siteoperationalhours;
	}
	public void setSiteoperationalhours(String siteoperationalhours) {
		this.siteoperationalhours = siteoperationalhours;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
	public String getFaxphone() {
		return faxphone;
	}
	public void setFaxphone(String faxphone) {
		this.faxphone = faxphone;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCreated_Alias() {
		return Created_Alias;
	}
	public void setCreated_Alias(String created_Alias) {
		Created_Alias = created_Alias;
	}
	public String getCreated_By() {
		return Created_By;
	}
	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}
	public String getCreated_Date() {
		return Created_Date;
	}
	public void setCreated_Date(String created_Date) {
		Created_Date = created_Date;
	}
	public String getCustomer_Contact() {
		return Customer_Contact;
	}
	public void setCustomer_Contact(String customer_Contact) {
		Customer_Contact = customer_Contact;
	}
	public String getCustomer_Name() {
		return Customer_Name;
	}
	public void setCustomer_Name(String customer_Name) {
		Customer_Name = customer_Name;
	}
	public String getFax() {
		return Fax;
	}
	public void setFax(String fax) {
		Fax = fax;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getLast_Activity_Date() {
		return Last_Activity_Date;
	}
	public void setLast_Activity_Date(String last_Activity_Date) {
		Last_Activity_Date = last_Activity_Date;
	}
	public String getLast_Modified_Alias() {
		return Last_Modified_Alias;
	}
	public void setLast_Modified_Alias(String last_Modified_Alias) {
		Last_Modified_Alias = last_Modified_Alias;
	}
	public String getLast_Modified_By() {
		return Last_Modified_By;
	}
	public void setLast_Modified_By(String last_Modified_By) {
		Last_Modified_By = last_Modified_By;
	}
	public String getLast_Modified_Date() {
		return Last_Modified_Date;
	}
	public void setLast_Modified_Date(String last_Modified_Date) {
		Last_Modified_Date = last_Modified_Date;
	}
	public String getOwner_Alias() {
		return Owner_Alias;
	}
	public void setOwner_Alias(String owner_Alias) {
		Owner_Alias = owner_Alias;
	}
	public String getOwner_Name() {
		return Owner_Name;
	}
	public void setOwner_Name(String owner_Name) {
		Owner_Name = owner_Name;
	}
	public String getOwner_Role() {
		return Owner_Role;
	}
	public void setOwner_Role(String owner_Role) {
		Owner_Role = owner_Role;
	}
	public String getService_Tier() {
		return Service_Tier;
	}
	public void setService_Tier(String service_Tier) {
		Service_Tier = service_Tier;
	}
	public String getSite_operational_hours() {
		return Site_operational_hours;
	}
	public void setSite_operational_hours(String site_operational_hours) {
		Site_operational_hours = site_operational_hours;
	}
	public String getTime_Zone() {
		return Time_Zone;
	}
	public void setTime_Zone(String time_Zone) {
		Time_Zone = time_Zone;
	}
	
	public String getIsDataCenter() {
		return isDataCenter;
	}

	public void setIsDataCenter(String isDataCenter) {
		this.isDataCenter = isDataCenter;
	}

	public String getDataCenterType() {
		return dataCenterType;
	}

	public void setDataCenterType(String dataCenterType) {
		this.dataCenterType = dataCenterType;
	}

	public String getDataCenterName() {
		return dataCenterName;
	}

	public void setDataCenterName(String dataCenterName) {
		this.dataCenterName = dataCenterName;
	}

	public String getIsManaged() {
		return isManaged;
	}

	public void setIsManaged(String isManaged) {
		this.isManaged = isManaged;
	}

	@Column(name = "pvsitedeliverystatus")
	private String pvsitedeliverystatus;
	  
	public String getPvsitedeliverystatus() {
		return pvsitedeliverystatus;
	}

	public void setPvsitedeliverystatus(String pvsitedeliverystatus) {
		this.pvsitedeliverystatus = pvsitedeliverystatus;
	}

	public String getRootProductId() {
		return rootProductId;
	}

	public void setRootProductId(String rootProductId) {
		this.rootProductId = rootProductId;
	}
	
}
