package com.tt.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.sql.rowset.serial.SerialBlob;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "swreports")
public class SWReports {

	private String customerId;
	private String reportName;
	private String reportType;
	private String reportPath;
	private String reportCategory;
	
	private String reportId;
	private Date createTime;
	private SerialBlob reportData;
	private Date updateDate;
	private List<SWReportImages> reportImages;
	
	
	@OneToMany(mappedBy="reportId",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List<SWReportImages> getReportImages() {
		return reportImages;
	}

	public void setReportImages(List<SWReportImages> reportImages) {
		this.reportImages = reportImages;
	}

	@Column(name="update_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Column(name="report_data")
	@Lob
	public SerialBlob getReportData() {
		return reportData;
	}

	public void setReportData(SerialBlob reportData) {
		this.reportData = reportData;
	}

	@Column(name="customerId")
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="report_name")
	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	@Column(name="report_type")
	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	
	@Column(name="report_path")
	public String getReportPath() {
		return reportPath;
	}

	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}
	
	@Column(name="report_category")
	public String getReportCategory() {
		return reportCategory;
	}

	public void setReportCategory(String reportCategory) {
		this.reportCategory = reportCategory;
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public String getReportId() {
		return reportId;
	}
                                                          
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	@Column(name="create_time")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
