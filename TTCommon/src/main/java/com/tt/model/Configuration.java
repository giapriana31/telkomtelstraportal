package com.tt.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "configurationitem")
public class Configuration implements Serializable{

    private Long id;
    private String ciid;
    private String ciname;
    private String citype;
    private String cmdblass;
    private String comments;
    private String createdby;
    private Date createddate;
    private String customercontractid;
    private String portalclassification;
    private String siteid;
    private String specialinstructions;
    private String srreferenceid;
    private String sysid;
    private Date updateddate;
    private String updatedby;
    private String carriageserviced;
    private Boolean customeredge;
    private String productId;
    private String rootProductId;
    private String deliverystatus;
    private String color;
    private String link;
    private String activatedon;
    private String customercommiteddate;
    private String srireferencenumber;
    private String deliverystage;
    private String status;
    private String sitename;
    private VBlock vBlock;
    private VirtualMachine virtualMachine;
    private VCenter vCenter;
    private CloudBusiness cloudBusiness;
    private String customeruniqueid;
    private List<Incident> incidents = new ArrayList<>();
    @Column(name="cistatus")
	private String ciStatus;
	@Column(name="contractduration")
	private String contractDuration;
	@Column(name="contractenddate")
	private Date contractEndDate;
	@Column(name="contractstartdate")
	private Date contractStartDate;
	
	@Column(name="productclassification")
	private String productclassification;
    
	@Column(name="serreqnumber")
	private String serreqnumber;
	
	@Column(name="ritmnumber")
	private String ritmnumber;
	
	@Column(name="project_reference_number")
    private String projectreferencenumber;
	
    @Column(name="security_tiers")
    private String securitytiers;
	
    public String getRitmnumber() {
		return ritmnumber;
	}

	public void setRitmnumber(String ritmnumber) {
		this.ritmnumber = ritmnumber;
	}

	
   	public String getSerreqnumber() {
		return serreqnumber;
	}

	public void setSerreqnumber(String serreqnumber) {
		this.serreqnumber = serreqnumber;
	}
	
	public String getProductclassification() {
		return productclassification;
	}

	public void setProductclassification(String productclassification) {
		this.productclassification = productclassification;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getRootProductId() {
		return rootProductId;
	}

	public void setRootProductId(String rootProductId) {
		this.rootProductId = rootProductId;
	}

	public String getSrireferencenumber() {
		return srireferencenumber;
	}

	public void setSrireferencenumber(String srireferencenumber) {
		this.srireferencenumber = srireferencenumber;
	}

	public String getDeliverystage() {
		return deliverystage;
	}

	public void setDeliverystage(String deliverystage) {
		this.deliverystage = deliverystage;
	}

	public void setCustomeredge(Boolean customeredge) {
		this.customeredge = customeredge;
	}
    
	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

   

	@ManyToMany(cascade = CascadeType.REFRESH)
	@JoinTable(name = "incdtoservicemapping",
		joinColumns = { @JoinColumn(name = "serviceciid", referencedColumnName = "ciid") }, 
		inverseJoinColumns = { @JoinColumn(name = "incidentid", referencedColumnName = "incidentid") })
    public List<Incident> getIncidents() {
	return incidents;
    }

    public void setIncidents(List<Incident> incidents) {
	this.incidents = incidents;
    }

    @Column(name = "ciid")
    public String getCiid() {
	return ciid;
    }

    public void setCiid(String ciid) {
	this.ciid = ciid;
    }

    public String getCiname() {
	return ciname;
    }

    public void setCiname(String ciname) {
	this.ciname = ciname;
    }

    public String getCitype() {
	return citype;
    }

    public void setCitype(String citype) {
	this.citype = citype;
    }

    @Column(name = "cmdbclass")
    public String getCmdblass() {
	return cmdblass;
    }

    public void setCmdblass(String cmdblass) {
	this.cmdblass = cmdblass;
    }

    public String getComments() {
	return comments;
    }

    public void setComments(String comments) {
	this.comments = comments;
    }

    public String getCreatedby() {
	return createdby;
    }

    public void setCreatedby(String createdby) {
	this.createdby = createdby;
    }

    public Date getCreateddate() {
	return createddate;
    }

    public void setCreateddate(Date createddate) {
	this.createddate = createddate;
    }

    public String getCustomercontractid() {
	return customercontractid;
    }

    public void setCustomercontractid(String customercontractid) {
	this.customercontractid = customercontractid;
    }

    public String getPortalclassification() {
	return portalclassification;
    }

    public void setPortalclassification(String portalclassification) {
	this.portalclassification = portalclassification;
    }

    public String getSiteid() {
	return siteid;
    }

    public void setSiteid(String siteid) {
	this.siteid = siteid;
    }

    public String getSpecialinstructions() {
	return specialinstructions;
    }

    public void setSpecialinstructions(String specialinstructions) {
	this.specialinstructions = specialinstructions;
    }

    public String getSrreferenceid() {
	return srreferenceid;
    }

    public void setSrreferenceid(String srreferenceid) {
	this.srreferenceid = srreferenceid;
    }

    public String getSysid() {
	return sysid;
    }

    public void setSysid(String sysid) {
	this.sysid = sysid;
    }

    public Date getUpdateddate() {
	return updateddate;
    }

    public void setUpdateddate(Date updateddate) {
	this.updateddate = updateddate;
    }

    public String getUpdatedby() {
	return updatedby;
    }

    public void setUpdatedby(String updatedby) {
	this.updatedby = updatedby;
    }

    public String getCarriageserviced() {
	return carriageserviced;
    }

    public void setCarriageserviced(String carriageserviced) {
	this.carriageserviced = carriageserviced;
    }

	public String getDeliverystatus() {
		return deliverystatus;
	}

	public void setDeliverystatus(String deliverystatus) {
		this.deliverystatus = deliverystatus;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getActivatedon() {
		return activatedon;
	}

	public void setActivatedon(String activatedon) {
		this.activatedon = activatedon;
	}

	public String getCustomercommiteddate() {
		return customercommiteddate;
	}

	public void setCustomercommiteddate(String customercommiteddate) {
		this.customercommiteddate = customercommiteddate;
	}

	@OneToOne(mappedBy = "ciid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public VBlock getvBlock() {
		return vBlock;
	}

	public void setvBlock(VBlock vBlock) {
		this.vBlock = vBlock;
	}

	@OneToOne(mappedBy = "ciid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public VirtualMachine getVirtualMachine() {
		return virtualMachine;
	}

	public void setVirtualMachine(VirtualMachine virtualMachine) {
		this.virtualMachine = virtualMachine;
	}

	@OneToOne(mappedBy = "ciid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public VCenter getvCenter() {
		return vCenter;
	}

	public void setvCenter(VCenter vCenter) {
		this.vCenter = vCenter;
	}

	@OneToOne(mappedBy = "ciid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public CloudBusiness getCloudBusiness() {
		return cloudBusiness;
	}

	public void setCloudBusiness(CloudBusiness cloudBusiness) {
		this.cloudBusiness = cloudBusiness;
	}
	
	@Id
	@GeneratedValue
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}


	
	
	public String getCiStatus() {
		return ciStatus;
	}

	public void setCiStatus(String ciStatus) {
		this.ciStatus = ciStatus;
	}
	


	public String getContractDuration() {
		return contractDuration;
	}

	public void setContractDuration(String contractDuration) {
		this.contractDuration = contractDuration;
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public Date getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(Date contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public String getCustomeruniqueid() {
		return customeruniqueid;
	}

	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}

	@Column(name="project_reference_number")
	public String getProjectreferencenumber() {
		return projectreferencenumber;
	}

	public void setProjectreferencenumber(String projectreferencenumber) {
		this.projectreferencenumber = projectreferencenumber;
	}

	@Column(name="security_tiers")
	public String getSecuritytiers() {
		return securitytiers;
	}

	public void setSecuritytiers(String securitytiers) {
		this.securitytiers = securitytiers;
	}

}
