package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="incdtodevicemapping")
public class IncidentToDeviceMapping {
	
	private int id;
	private String deviceciid;
	private String incidentid;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getIncidentid() {
		return incidentid;
	}
	public void setIncidentid(String incidentid) {
		this.incidentid = incidentid;
	}
	public String getDeviceciid() {
		return deviceciid;
	}
	public void setDeviceciid(String deviceciid) {
		this.deviceciid = deviceciid;
	}
	
}
