package com.tt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="incddashboardmetrics")
public class IncidentDashboardMapping {
	
	private Long id;
	private String 	customeruniqueid;
	private String incidentid;
	private String siteid;
	private String sitename;
	private String priority;
	private String impact;
	private String cmdbclass;
	private String sitetier;
	private String sitecolor;
	private String incidentstatus;
	@Column(name="hierarchyescalationflag")
	private Boolean hierEscalationFlag;
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date serviceleveltarget;
	@Column(name="mim")
	private String MIM;
	private String customerimpacted;
	private String summary;
	private String serviceid;
	@Column(name="citype")
	private String citype;
	private Date createddate;
	private Date updateddate;
	private String urgency;
	private String incdcategory;
	private String sitestatus;
	private String rootproductid;
	

	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getMIM() {
		return MIM;
	}
	public void setMIM(String mIM) {
		MIM = mIM;
	}
	public String getCustomerimpacted() {
		return customerimpacted;
	}
	public void setCustomerimpacted(String customerimpacted) {
		this.customerimpacted = customerimpacted;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
	
	public String getIncidentid() {
		return incidentid;
	}
	public void setIncidentid(String incidentid) {
		this.incidentid = incidentid;
	}
	public String getSiteid() {
		return siteid;
	}
	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}
	public String getSitename() {
		return sitename;
	}
	public void setSitename(String sitename) {
		this.sitename = sitename;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getImpact() {
		return impact;
	}
	public void setImpact(String impact) {
		this.impact = impact;
	}
	public String getCmdbclass() {
		return cmdbclass;
	}
	public void setCmdbclass(String cmdbclass) {
		this.cmdbclass = cmdbclass;
	}
	public String getSitetier() {
		return sitetier;
	}
	public String getServiceid() {
		return serviceid;
	}
	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}
	public String getCitype() {
		return citype;
	}
	public void setCitype(String citype) {
		this.citype = citype;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public Date getUpdateddate() {
		return updateddate;
	}
	public void setUpdateddate(Date updateddate) {
		this.updateddate = updateddate;
	}
	public String getUrgency() {
		return urgency;
	}
	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}
	public String getIncdcategory() {
		return incdcategory;
	}
	public void setIncdcategory(String incdcategory) {
		this.incdcategory = incdcategory;
	}
	public void setSitetier(String sitetier) {
		this.sitetier = sitetier;
	}
	public String getSitecolor() {
		return sitecolor;
	}
	public void setSitecolor(String sitecolor) {
		this.sitecolor = sitecolor;
	}
	public String getIncidentstatus() {
		return incidentstatus;
	}
	public void setIncidentstatus(String incidentstatus) {
		this.incidentstatus = incidentstatus;
	}
	public Boolean getHierEscalationFlag() {
	    return hierEscalationFlag;
	}
	public void setHierEscalationFlag(Boolean hierEscalationFlag) {
	    this.hierEscalationFlag = hierEscalationFlag;
	}
	public Date getServiceleveltarget() {
	    return serviceleveltarget;
	}
	public void setServiceleveltarget(Date serviceleveltarget) {
	    this.serviceleveltarget = serviceleveltarget;
	}
	public String getSitestatus() {
		return sitestatus;
	}
	public void setSitestatus(String sitestatus) {
		this.sitestatus = sitestatus;
	}
	public String getRootproductid() {
		return rootproductid;
	}
	public void setRootproductid(String rootproductid) {
		this.rootproductid = rootproductid;
	}
	
	

}
