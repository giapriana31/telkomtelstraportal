/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys			10-JUL-2015			Initial Version
 * 
 * 
 * **************************************************************************************************************
 */

package com.tt.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

/**
 * 
 * @author Infosys
 * @version 1.0
 *
 */
@Entity
@Table(name = "incident")
public class Incident {

	@Id
	private String incidentid;
	private Date opendatetime;
	private String summary;
	private String incidentstatus;
	private String impact;
	private String urgency;
	private Long priority;
	private String customeruniqueid;
	private String customername;
	private String caller;
	private String sitename;
	private String siteid;
	private String sitezone;
	private String siteservicetier;
	private Date reporteddatetime;
	private String category;
	private String subcategory;
	
	private String userid;
	private String fullname;
	private String MIM;
	private Date resolutiondatetime;
	private Boolean customerimpacted;
	@Column(columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date serviceleveltarget;
	private Date lastupdated;
	private String notes;
	private Boolean hierEscalationFlag;
	private String modifiedBy;
	private String createdBy;
	private Date modifiedDate;
	private Date createdDate;
	private String actualcustomeruniqueid;
	
	@Transient
	private String region;
	@Transient
	private Boolean mimCheck;
	
	@Transient
	private String productType;
	
	@Column(name="privatecloudbusinessservice")
	private String privateCloudBusinessService;
	@Column(name="virtualmachines")
	private String virtualMachines;

	@Column(name = "ID")
	private int id = 0;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "incidentid")
	private List<Notes> comments;

	@ManyToMany(cascade = CascadeType.REFRESH)
	@JoinTable(name = "incdtoservicemapping",
		joinColumns = { @JoinColumn(name = "incidentid", referencedColumnName = "incidentid") }, 
		inverseJoinColumns = { @JoinColumn(name = "serviceciid", referencedColumnName = "ciid") })
	private List<Configuration> services;

	@Transient
	private List<Configuration> affectedResource;
	@Transient
	private List<Configuration> affectedService;
	@Transient
	private String affectedResourceString;
	@Transient
	private String affectedServiceString;
	@Transient
	private String dueByTime;

	private String serviceid;
	private String tickettype;
	private String ticketsource;
	private String casemanager;
	private String resolvergroupname;
	private String customerpreferedlanguage;
	@Size(max = 200, message = "Max limit of characters allowed is 200")
	private String worklognotes;
	@Column(name = "showEscalationFlag")
	private boolean showEscalationFlag;
	private Date closeddate;
	
	@Column(name="rootproductid")
	private String rootProductId;
	private String resolutioncategory;
	private String resolutionSubCategory1;
	private String resolutionSubCategory2;
	private String resolutionSubCategory3;

	
	
	public Boolean getMimCheck() {
		return mimCheck;
	}

	public void setMimCheck(Boolean mimCheck) {
		this.mimCheck = mimCheck;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getHierEscalationFlag() {
		return hierEscalationFlag;
	}

	public void setHierEscalationFlag(Boolean hierEscalationFlag) {
		this.hierEscalationFlag = hierEscalationFlag;
	}

	public String getIncidentid() {
		return incidentid;
	}

	public void setIncidentid(String incidentid) {
		this.incidentid = incidentid;
	}

	public Date getOpendatetime() {
		return opendatetime;
	}

	public void setOpendatetime(Date opendatetime) {
		this.opendatetime = opendatetime;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getIncidentstatus() {
		return incidentstatus;
	}

	public void setIncidentstatus(String incidentstatus) {
		this.incidentstatus = incidentstatus;
	}

/*	public String getStatusreason() {
		return statusreason;
	}

	public void setStatusreason(String statusreason) {
		this.statusreason = statusreason;
	}

*/	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getUrgency() {
		return urgency;
	}

	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}

	public Long getPriority() {
		return priority;
	}

	public void setPriority(Long priority) {
		this.priority = priority;
	}

/*	public String getCustomerinformation() {
		return customerinformation;
	}

	public void setCustomerinformation(String customerinformation) {
		this.customerinformation = customerinformation;
	}
*/
	public String getCustomeruniqueid() {
		return customeruniqueid;
	}

	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

/*	public String getExternalcustomerid() {
		return externalcustomerid;
	}

	public void setExternalcustomerid(String externalcustomerid) {
		this.externalcustomerid = externalcustomerid;
	}
*/
	public String getSitename() {
		return sitename;
	}

	public void setSitename(String sitename) {
		this.sitename = sitename;
	}

	public String getSiteid() {
		return siteid;
	}

	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}

/*	public Date getTimezone() {
		return timezone;
	}

	public void setTimezone(Date timezone) {
		this.timezone = timezone;
	}
*/
	public String getSitezone() {
		return sitezone;
	}

	public void setSitezone(String sitezone) {
		this.sitezone = sitezone;
	}

	public String getSiteservicetier() {
		return siteservicetier;
	}

	public void setSiteservicetier(String siteservicetier) {
		this.siteservicetier = siteservicetier;
	}


	public Date getReporteddatetime() {
		return reporteddatetime;
	}

	public void setReporteddatetime(Date reporteddatetime) {
		this.reporteddatetime = reporteddatetime;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubcategory() {
		return subcategory;
	}

	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

/*	public String getGovernancelead() {
		return governancelead;
	}

	public void setGovernancelead(String governancelead) {
		this.governancelead = governancelead;
	}
*/
	public String getMIM() {
		return MIM;
	}

	public void setMIM(String mIM) {
		MIM = mIM;
	}

/*	public String getAssignment() {
		return assignment;
	}

	public void setAssignment(String assignment) {
		this.assignment = assignment;
	}

	public String getAssinee() {
		return assinee;
	}

	public void setAssinee(String assinee) {
		this.assinee = assinee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResolutiondetails() {
		return resolutiondetails;
	}

	public void setResolutiondetails(String resolutiondetails) {
		this.resolutiondetails = resolutiondetails;
	}

	public String getResolutionmethod() {
		return resolutionmethod;
	}

	public void setResolutionmethod(String resolutionmethod) {
		this.resolutionmethod = resolutionmethod;
	}

	public String getResolutioncause() {
		return resolutioncause;
	}

	public void setResolutioncause(String resolutioncause) {
		this.resolutioncause = resolutioncause;
	}
*/
	public Date getResolutiondatetime() {
		return resolutiondatetime;
	}

	public void setResolutiondatetime(Date resolutiondatetime) {
		this.resolutiondatetime = resolutiondatetime;
	}

/*	public String getResolutioncategorization() {
		return resolutioncategorization;
	}

	public void setResolutioncategorization(String resolutioncategorization) {
		this.resolutioncategorization = resolutioncategorization;
	}

	public String getResolutioncategory() {
		return resolutioncategory;
	}

	public void setResolutioncategory(String resolutioncategory) {
		this.resolutioncategory = resolutioncategory;
	}

	public String getResoultionsubcategory() {
		return resoultionsubcategory;
	}

	public void setResoultionsubcategory(String resoultionsubcategory) {
		this.resoultionsubcategory = resoultionsubcategory;
	}

	public String getExternalincidentreferenceid() {
		return externalincidentreferenceid;
	}

	public void setExternalincidentreferenceid(
			String externalincidentreferenceid) {
		this.externalincidentreferenceid = externalincidentreferenceid;
	}

	public String getWatchlist() {
		return watchlist;
	}

	public void setWatchlist(String watchlist) {
		this.watchlist = watchlist;
	}

	public String getRelatedrecordfields() {
		return relatedrecordfields;
	}

	public void setRelatedrecordfields(String relatedrecordfields) {
		this.relatedrecordfields = relatedrecordfields;
	}

	public String getAdditionalfields() {
		return additionalfields;
	}

	public void setAdditionalfields(String additionalfields) {
		this.additionalfields = additionalfields;
	}

	
*/
	public Date getLastupdated() {
		return lastupdated;
	}

	public void setLastupdated(Date lastupdated) {
		this.lastupdated = lastupdated;
	}

/*	
	public String getColorcode() {
		return colorcode;
	}

	public void setColorcode(String colorcode) {
		this.colorcode = colorcode;
	}
*/
	public Boolean getCustomerimpacted() {
		return customerimpacted;
	}

	public void setCustomerimpacted(Boolean customerimpacted) {
		this.customerimpacted = customerimpacted;
	}

	public Date getServiceleveltarget() {
		return serviceleveltarget;
	}

	public void setServiceleveltarget(Date serviceleveltarget) {
		this.serviceleveltarget = serviceleveltarget;
	}

/*	public int getColorPriority() {
		return colorPriority;
	}

	public void setColorPriority(int colorPriority) {
		this.colorPriority = colorPriority;
	}
*/
	public List<Configuration> getServices() {
		return services;
	}

	public void setServices(List<Configuration> services) {
		this.services = services;
	}

	public List<Configuration> getAffectedResource() {
		return affectedResource;
	}

	public void setAffectedResource(List<Configuration> affectedResource) {
		this.affectedResource = affectedResource;
	}

	public List<Configuration> getAffectedService() {
		return affectedService;
	}

	public void setAffectedService(List<Configuration> affectedService) {
		this.affectedService = affectedService;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		
		if(notes!=null && notes.length()>=2000){
			this.notes = notes.substring(0, 1999);
		}else 
			this.notes = notes;
	}

	@Column(name = "modifiedby")
	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	@Column(name = "createdby")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "modifieddate")
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Column(name = "createddate")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getActualcustomeruniqueid() {
		return actualcustomeruniqueid;
	}

	public void setActualcustomeruniqueid(String actualcustomeruniqueid) {
		this.actualcustomeruniqueid = actualcustomeruniqueid;
	}

	public List<Notes> getComments() {
		return comments;
	}

	public void setComments(List<Notes> comments) {
		this.comments = comments;
	}

	public String getAffectedResourceString() {
		return affectedResourceString;
	}

	public void setAffectedResourceString(String affectedResourceString) {
		this.affectedResourceString = affectedResourceString;
	}

	public String getAffectedServiceString() {
		return affectedServiceString;
	}

	public void setAffectedServiceString(String affectedServiceString) {
		this.affectedServiceString = affectedServiceString;
	}

	public String getDueByTime() {
		return dueByTime;
	}

	public void setDueByTime(String dueByTime) {
		this.dueByTime = dueByTime;
	}

	public String getServiceid() {
		return serviceid;
	}

	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}

	public String getTickettype() {
		return tickettype;
	}

	public void setTickettype(String tickettype) {
		this.tickettype = tickettype;
	}

	public String getTicketsource() {
		return ticketsource;
	}

	public void setTicketsource(String ticketsource) {
		this.ticketsource = ticketsource;
	}

 	public String getCasemanager() {
		return casemanager;
	}

	public void setCasemanager(String casemanager) {
		this.casemanager = casemanager;
	}
	
	public String getCustomerpreferedlanguage() {
		return customerpreferedlanguage;
	}

	public void setCustomerpreferedlanguage(String customerpreferedlanguage) {
		this.customerpreferedlanguage = customerpreferedlanguage;
	}
	
	public String getResolvergroupname() {
		return resolvergroupname;
	}

	public void setResolvergroupname(String resolvergroupname) {
		this.resolvergroupname = resolvergroupname;
	}
	
	public String getWorklognotes() {
		return worklognotes;
	}

	public void setWorklognotes(String worklognotes) {
		this.worklognotes = worklognotes;
	}
	
	public boolean isShowEscalationFlag() {
	    return showEscalationFlag;
	}

	public void setShowEscalationFlag(boolean showEscalationFlag) {
	    this.showEscalationFlag = showEscalationFlag;
	}

	public Date getCloseddate() {
		return closeddate;
	}

	public void setCloseddate(Date closeddate) {
		this.closeddate = closeddate;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getPrivateCloudBusinessService() {
		return privateCloudBusinessService;
	}

	public void setPrivateCloudBusinessService(String privateCloudBusinessService) {
		this.privateCloudBusinessService = privateCloudBusinessService;
	}

	public String getVirtualMachines() {
		return virtualMachines;
	}

	public void setVirtualMachines(String virtualMachines) {
		this.virtualMachines = virtualMachines;
	}


	public String getRootProductId() {
		return rootProductId;
	}

	public void setRootProductId(String rootProductId) {
		this.rootProductId = rootProductId;
	}

	public String getResolutioncategory() {
		return resolutioncategory;
	}

	public void setResolutioncategory(String resolutioncategory) {
		this.resolutioncategory = resolutioncategory;
	}

	
	public String getResolutionSubCategory1() {
		return resolutionSubCategory1;
	}

	public void setResolutionSubCategory1(String resolutionSubCategory1) {
		this.resolutionSubCategory1 = resolutionSubCategory1;
	}

	public String getResolutionSubCategory2() {
		return resolutionSubCategory2;
	}

	public void setResolutionSubCategory2(String resolutionSubCategory2) {
		this.resolutionSubCategory2 = resolutionSubCategory2;
	}

	public String getResolutionSubCategory3() {
		return resolutionSubCategory3;
	}

	public void setResolutionSubCategory3(String resolutionSubCategory3) {
		this.resolutionSubCategory3 = resolutionSubCategory3;
	}
}
