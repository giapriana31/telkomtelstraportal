package com.tt.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "serviceoutage")
public class Outage {

	private long serviceoutageid;
	private String incidentid;
	private String excludedFlag;
	private Date outagestarttime;
	private Date outageendtime;
	private String contractedoutage;
	private Date incidentcloseddate;
	private List<AffectedServices> affectedServices;
	private String rootProductId;
	private String category;
	
	@Id
	@GeneratedValue
	@Column(name = "serviceoutageid")
	public long getServiceoutageid() {
		return serviceoutageid;
	}

	public void setServiceoutageid(long serviceoutageid) {
		this.serviceoutageid = serviceoutageid;
	}

	public String getIncidentid() {
		return incidentid;
	}

	public void setIncidentid(String incidentid) {
		this.incidentid = incidentid;
	}

	public String getExcludedFlag() {
		return excludedFlag;
	}

	public void setExcludedFlag(String excludedFlag) {
		this.excludedFlag = excludedFlag;
	}

	public Date getOutagestarttime() {
		return outagestarttime;
	}

	public void setOutagestarttime(Date outagestarttime) {
		this.outagestarttime = outagestarttime;
	}

	public Date getOutageendtime() {
		return outageendtime;
	}

	public void setOutageendtime(Date outageendtime) {
		this.outageendtime = outageendtime;
	}

	public String getContractedoutage() {
		return contractedoutage;
	}

	public void setContractedoutage(String contractedoutage) {
		this.contractedoutage = contractedoutage;
	}

	public Date getIncidentcloseddate() {
		return incidentcloseddate;
	}

	public void setIncidentcloseddate(Date incidentcloseddate) {
		this.incidentcloseddate = incidentcloseddate;
	}

	@OneToMany(mappedBy = "serviceoutageid", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	public List<AffectedServices> getAffectedServices() {
		return affectedServices;
	}

	public void setAffectedServices(List<AffectedServices> affectedServices) {
		this.affectedServices = affectedServices;
	}

	public String getRootProductId() {
		return rootProductId;
	}

	public void setRootProductId(String rootProductId) {
		this.rootProductId = rootProductId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
