package com.tt.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sfdcsite")
public class CustomerSiteMapping {

	private String city;
	private String customername;
	private Long customercontact;
	private String country;
	private BigDecimal latitude;
	private BigDecimal longitude;
	private String customersite;
	private String stateprovince;
	private String street;
	private String createdby;
	private Date createddate;
	private String lastmodifiedby;
	private Date lastmodifieddate;
	private Date timezone;
	private String zippostalcode;
	private String siteid;
	private String zone;
	private String servicetier;
	private String siteoperationalhours;
	private String active;
	private Long customeruniqueid;


	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public Long getCustomercontact() {
		return customercontact;
	}
	public void setCustomercontact(Long customercontact) {
		this.customercontact = customercontact;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public String getCustomersite() {
		return customersite;
	}
	public void setCustomersite(String customersite) {
		this.customersite = customersite;
	}
	public String getStateprovince() {
		return stateprovince;
	}
	public void setStateprovince(String stateprovince) {
		this.stateprovince = stateprovince;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	public String getLastmodifiedby() {
		return lastmodifiedby;
	}
	public void setLastmodifiedby(String lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}
	public Date getLastmodifieddate() {
		return lastmodifieddate;
	}
	public void setLastmodifieddate(Date lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}
	public Date getTimezone() {
		return timezone;
	}
	public void setTimezone(Date timezone) {
		this.timezone = timezone;
	}
	public String getZippostalcode() {
		return zippostalcode;
	}
	public void setZippostalcode(String zippostalcode) {
		this.zippostalcode = zippostalcode;
	}
	@Id
	@Column(name="siteid")
	public String getSiteid() {
		return siteid;
	}
	public void setSiteid(String siteid) {
		this.siteid = siteid;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getServicetier() {
		return servicetier;
	}
	public void setServicetier(String servicetier) {
		this.servicetier = servicetier;
	}
	public String getSiteoperationalhours() {
		return siteoperationalhours;
	}
	public void setSiteoperationalhours(String siteoperationalhours) {
		this.siteoperationalhours = siteoperationalhours;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public Long getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(Long customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}

	
}
