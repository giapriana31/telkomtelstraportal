package com.tt.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="networkprovince")
public class NetworkProvince {
	
	@Id
	private String networksiteprovince;
	private double latitude;
	private double longitude;

	public String getNetworksiteprovince() {
		return networksiteprovince;
	}
	public void setNetworksiteprovince(String networksiteprovince) {
		this.networksiteprovince = networksiteprovince;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	
	
}
