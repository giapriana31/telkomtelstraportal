package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


@Entity
@Table(name="province")
public class ProvinceModel {

	private String provincename;
	private int latitude;
	private int longitude;
	
//	@GeneratedValue(strategy=GenerationType.TABLE,generator="PROVINCE_")
//	@TableGenerator(name="PROVINCE_", table="SEQUENCE", pkColumnName="SEQUENCE_ID", pkColumnValue="PROVINCE_", valueColumnName="SEQUENCE_VALUE", allocationSize=1, initialValue=1)
	@Id
	@Column(name="provincename")
	public String getProvincename() {
		return provincename;
	}
	public void setProvincename(String provincename) {
		this.provincename = provincename;
	}
	@Column(name="latitude")
	public int getLatitude() {
		return latitude;
	}
	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}
	@Column(name="longitude")
	public int getLongitude() {
		return longitude;
	}
	public void setLongitude(int longitude) {
		this.longitude = longitude;
	}
	
	
}
