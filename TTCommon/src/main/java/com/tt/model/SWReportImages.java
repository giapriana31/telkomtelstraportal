package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.sql.rowset.serial.SerialBlob;


@Entity
@Table(name="swreportimages")
public class SWReportImages {
	
	private String imageid;
	private SWReports reportId;
	private SerialBlob image_data;
	private String customerId;
	private String category;
	
	
	
	
	
	@Column(name="customerId")
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="imageid")
	public String getImageid() {
		return imageid;
	}
	public void setImageid(String imageid) {
		this.imageid = imageid;
	}
	
	@ManyToOne
	@JoinColumn(name="reportId" ,referencedColumnName="reportId")
	public SWReports getReportId() {
		return reportId;
	}
	public void setReportId(SWReports reportId) {
		this.reportId = reportId;
	}
	
	@Column(name="image_data")
	@Lob
	public SerialBlob getImage_data() {
		return image_data;
	}

	public void setImage_data(SerialBlob image_data) {
		this.image_data = image_data;
	}
	
	
	
	

}
