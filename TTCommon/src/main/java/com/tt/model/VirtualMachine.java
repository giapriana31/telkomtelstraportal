package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="virtualmachine")
public class VirtualMachine {
	
	@Id
	@GeneratedValue
	private long id;
	
	@OneToOne
	@JoinColumn(name = "ciid", referencedColumnName="ciid")
	private Configuration ciid;
	private String state;
	private String template;

	@Column(name = "leasestart")
	private String leaseStart;

	@Column(name = "leaseend")
	private String leaseEnd;
	private String cpus;
	private String disks;

	@Column(name = "disksize")
	private String diskSize;
	private String memory;

	@Column(name = "networkadapters")
	private String networkAdapters;

	@Column(name = "correlationid")
	private String correlationId;

	@Column(name = "imagepath")
	private String imagePath;

	@Column(name = "modelid")
	private String modelId;
	private String description;
	private String link;
	
	@Column(name = "vmalias" )
	private String vmAlias;

	public String getVmAlias() {
		return vmAlias;
	}

	public void setVmAlias(String vmAlias) {
		this.vmAlias = vmAlias;
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public String getLeaseStart() {
		return leaseStart;
	}

	public void setLeaseStart(String leaseStart) {
		this.leaseStart = leaseStart;
	}

	public String getLeaseEnd() {
		return leaseEnd;
	}

	public void setLeaseEnd(String leaseEnd) {
		this.leaseEnd = leaseEnd;
	}

	public String getCpus() {
		return cpus;
	}

	public void setCpus(String cpus) {
		this.cpus = cpus;
	}

	public String getDisks() {
		return disks;
	}

	public void setDisks(String disks) {
		this.disks = disks;
	}

	public String getDiskSize() {
		return diskSize;
	}

	public void setDiskSize(String diskSize) {
		this.diskSize = diskSize;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getNetworkAdapters() {
		return networkAdapters;
	}

	public void setNetworkAdapters(String networkAdapters) {
		this.networkAdapters = networkAdapters;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public Configuration getCiid() {
		return ciid;
	}

	public void setCiid(Configuration ciid) {
		this.ciid = ciid;
	}
}
