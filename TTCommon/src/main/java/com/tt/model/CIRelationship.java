/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys				24-JUL-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * POJO for cirelationship table
 * 
 * @author Infosys
 * @version 1.0
 */
@Entity
@Table(name = "cirelationship")
public class CIRelationship {
	@Id
	private Long id;

	@Column(name = "relationship")
	private String relationShip;

	@Column(name = "baseciname")
	private String baseCIName;

	@Column(name = "dependentciname")
	private String dependentCIName;

	@Column(name = "baseciid")
	private String baseCIId;

	@Column(name = "dependentciid")
	private String dependentCIId;

	private String dependentciclass;
	private String baseciclass;

	@Column(columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createddate;

	@Column(columnDefinition = "DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateddate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRelationShip() {
		return relationShip;
	}

	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}

	public String getBaseCIName() {
		return baseCIName;
	}

	public void setBaseCIName(String baseCIName) {
		this.baseCIName = baseCIName;
	}

	public String getDependentCIName() {
		return dependentCIName;
	}

	public void setDependentCIName(String dependentCIName) {
		this.dependentCIName = dependentCIName;
	}

	public String getBaseCIId() {
		return baseCIId;
	}

	public void setBaseCIId(String baseCIId) {
		this.baseCIId = baseCIId;
	}

	public String getDependentCIId() {
		return dependentCIId;
	}

	public void setDependentCIId(String dependentCIId) {
		this.dependentCIId = dependentCIId;
	}

	public String getDependentciclass() {
		return dependentciclass;
	}

	public void setDependentciclass(String dependentciclass) {
		this.dependentciclass = dependentciclass;
	}

	public String getBaseciclass() {
		return baseciclass;
	}

	public void setBaseciclass(String baseciclass) {
		this.baseciclass = baseciclass;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	public Date getUpdateddate() {
		return updateddate;
	}

	public void setUpdateddate(Date updateddate) {
		this.updateddate = updateddate;
	}

}
