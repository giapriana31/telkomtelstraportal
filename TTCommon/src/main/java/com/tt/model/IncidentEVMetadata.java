/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 1.0						Infosys			10-JUL-2015			Initial Version
 * 
 * 
 * **************************************************************************************************************
 */

package com.tt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Infosys
 * @version 1.0
 *
 */
@Entity
@Table(name = "incidentevmetadata")
public class IncidentEVMetadata {

	private String monthwindow;
	private String customeruniqueid;
	private String category;
	private String completedincidentcount;
	private String totalincidentcount;
	private int id;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getMonthwindow() {
		return monthwindow;
	}
	public void setMonthwindow(String monthwindow) {
		this.monthwindow = monthwindow;
	}
	public String getCustomeruniqueid() {
		return customeruniqueid;
	}
	public void setCustomeruniqueid(String customeruniqueid) {
		this.customeruniqueid = customeruniqueid;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompletedincidentcount() {
		return completedincidentcount;
	}
	public void setCompletedincidentcount(String completedincidentcount) {
		this.completedincidentcount = completedincidentcount;
	}
	public String getTotalincidentcount() {
		return totalincidentcount;
	}
	public void setTotalincidentcount(String totalincidentcount) {
		this.totalincidentcount = totalincidentcount;
	}
	
	
}
