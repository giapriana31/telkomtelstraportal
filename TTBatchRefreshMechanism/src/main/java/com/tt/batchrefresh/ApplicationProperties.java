/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;


/** This class returns an instance of a Properties object which has been loaded
 * with Contstants.properties
 * @author Infosys
 * @version 1.0
 */
public class ApplicationProperties implements Cloneable
{
	private static Properties appProperties;
	private static final Logger logger = Logger.getLogger(ApplicationProperties.class);
	
	private ApplicationProperties()
	{
		appProperties = new Properties();
		try
		{
			InputStream in = new FileInputStream("Contstants.properties");
			//InputStream in = new FileInputStream(new File(ApplicationProperties.class.getClassLoader().getResource("Contstants.properties").toString()));
			appProperties.load(in);
		}
		catch (IOException e)
		{
			logger.error("Unable to load porperties file\n\t-->"+e.getMessage());
                        logger.error(e);
		}
	}
	
	/* This is the standard practice for singleton classes. 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() throws CloneNotSupportedException 
	{
		throw new CloneNotSupportedException();
	}
	
	
	/**
	 * @return instance of Properties
	 */
	public static Properties  getInstance()
	{
		if(null == appProperties)
			new ApplicationProperties();
		return appProperties;
	}
}

