/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Infosys
 *
 */
public class BatchRefreshQueue
{
	private static List<BatchRefresh> refreshList = new ArrayList<>();
	static
	{
		refreshList.add(new CustomerBatchRefresh());
		refreshList.add(new CIBatchRefresh());
		refreshList.add(new CIRelationsBatchRefresh());
		refreshList.add(new SitesBatchRefresh());
		refreshList.add(new IncidentBatchRefresh());
		refreshList.add(new SRBatchRefresh());
	}
	/**
	 * @return the refreshlist
	 */
	public static List<BatchRefresh> getRefreshlist()
	{
		return refreshList;
	}
	
	
}
