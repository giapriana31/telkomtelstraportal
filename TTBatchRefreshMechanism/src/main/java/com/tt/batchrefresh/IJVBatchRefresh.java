/*
 * **************************************************************************************************************
 * REVISION HISTORY			AUTHOR				DATE			DESCRIPTION
 * **************************************************************************************************************
 * 		1.0					Infosys				24-JUL-2015		Initial Version 
 * 
 * **************************************************************************************************************
 */
package com.tt.batchrefresh;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.bind.JAXBException;

import org.apache.log4j.Logger;

import com.tt.EmptySWDump;
import com.tt.RefreshOutage;
import com.tt.UpdateSWReports;
import com.tt.batchrefresh.dao.BatchRefreshDAO;

/**
 * @author Infosys
 *
 */
public class IJVBatchRefresh
{
	private static final Logger logger = Logger.getLogger(IJVBatchRefresh.class);

	public void refreshData(String fromDate, String toDate, String interval, String entityType) throws ParseException, IOException, JAXBException
	{

		for (BatchRefresh batchrefresh : BatchRefreshQueue.getRefreshlist())
		{
			batchrefresh.refreshData(fromDate, toDate, interval, entityType);
		}
	}

	public static void main(String[] args)
	{
		String fromDate = null;
		String toDate = null;

		String mode = args[0];
		String interval = args[1];
		
		String custId = "";

		if ("MONTHLY".equals(interval)){
			
			fromDate = args[2] + " " + args[3];
			toDate = args[4] + " " + args[5];
			
		} else if ("CUSTOM".equals(interval)){
			
			fromDate = args[2] + " " + args[3];
			toDate = fromDate;
			
		} else if ("Weekly".equalsIgnoreCase(interval)){
			
			fromDate = "2015-01-01 00:00:00";
			
		} else if ("PARTIAL".equalsIgnoreCase(interval)){
			
			custId = args[2];
			
			fromDate = new BatchRefreshDAO().retrieveLastRefreshDate(mode, "HOURLY");
			if (null == fromDate)
				fromDate = "2015-01-01 00:00:00";

			toDate = fromDate;
			
		} else {
			
			fromDate = new BatchRefreshDAO().retrieveLastRefreshDate(mode, interval);
			if (null == fromDate)
				fromDate = "2015-01-01 00:00:00";

			toDate = fromDate;
		}
		Date startDate = new Date(System.currentTimeMillis());
		logger.warn("Starting refresh for MODE : " + mode + " for times : " + fromDate + " , " + toDate + " at system time : " + startDate);

		try
		{
			Date dt = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			switch (mode)
			{
			case "ALL":
				new IJVBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "Customer":
				new CustomerBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "ContractAvailability":
				new ContractAvailabilityBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "Configuration":
				new CIBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "ConfigurationSaaS":
				new CIBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "ConfigurationPC":
				new CIPrivateCloudBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "CIRelationship":
				new CIRelationsBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "CIRelationshipPC":
				new PrivateCloudCIRelationBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "CIRelationshipSaaS":
				new CIRelationsBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "Site":
				new SitesBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "SitesBackupRefresh":
				new SitesBackupRefresh().refresh();
				break;
			case "DeleteSiteRefresh":
				new DeleteSiteRefresh().refresh();
				break;
			case "SitesPopulation":
				new SitesPopulation().refresh();
				break;
			case "Incident":
				toDate = df.format(dt);
				new IncidentBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "IncidentPartialCustomer":
				toDate = df.format(dt);
				new IncidentBatchRefresh().refreshDataPartial(fromDate, toDate, interval, mode, custId);
				break;	
			case "ServiceRequest":
				toDate = df.format(dt);
				new SRBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "ServiceRequestPartialCustomer":
				toDate = df.format(dt);
				new SRBatchRefresh().refreshDataPartial(fromDate, toDate, interval, mode, custId);
				break;	
			case "ChangeRequest":
				toDate = df.format(dt);
				new SRBatchRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "slarefresh":
				SLARefresh sl = new SLARefresh();
				sl.refreshSLA();
				sl.refreshSiteColor();
				sl.refreshServiceColor();
				break;
			case "dataretrieve":
				new DataRetrieve().retrieveData();
			case "SWReports":
				new UpdateSWReports().refreshSWReports(interval);
				break;
			case "EmptySWDump":
			new EmptySWDump().EmptyDump(interval);
				break;
			case "Outage":
				toDate = df.format(dt);
				new RefreshOutage().refreshOutageData(fromDate, toDate, interval);
				break;
			case "OutageMonthly":
				new RefreshOutage().refreshSitesAvailabilityTable();
				break;
			case "SWCPENetwork":
				new CPENetworkSWDataRetrieve().refreshData(fromDate, toDate, interval, mode);
				break;
			case "BusinessView":
				new BusinessViewDataRefresh().refreshData(fromDate, toDate, interval, mode);
				break;
			case "BVSubscribedServices":
				new BusinessViewDataRefresh().refreshDataDaily();
				break;
			case "ExecutiveViewMonthly":
				new ExecutiveViewMonthly().refreshData(fromDate, toDate, interval, mode);
				break;
			default:
				logger.info("UNKOWN MODE");
			}
		}
		catch (IOException | ParseException | JAXBException e)
		{
			logger.error("ERROR : " + e);
		}
		Date endDate = new Date(System.currentTimeMillis());
		long diff = endDate.getTime()-startDate.getTime();
		long diffSeconds = diff / 1000 % 60;
		logger.warn("Finished refresh for MODE : " + mode + " for times : " + fromDate + " , " + toDate + " at system time : " + endDate);
		logger.warn("Time taken for refreshing MODE : " + mode + " is " + diffSeconds +" seconds.");
	}
}
