package com.tt.bean;

import java.util.Map;

public class RequestObject {

	public RequestObject() {
	}
	
	Map additionalParameters;
	
	String key;
	private boolean isKey;
	
	String url;
	private boolean isURL = false;
	String username;
	private boolean isUserName =false;
	String password;
	private boolean isPassword = false;
	String proxyIP;
	private boolean isProxyIP=false;
	int proxyPort;
	private boolean isProxyPort=false;
	String contentType;
	private boolean isContentType=false;
	String contentLanguage;
	private boolean isContentLanguage=false;
	private boolean hasAdditionalParams=false;
	
	private String serverUri;
	private boolean isServerUri = false;
	
	
	public String getServerUri() {
		return serverUri;
	}
	public void setServerUri(String serverUri) {
		this.serverUri = serverUri;
		this.isServerUri = true;
	}
	
	public boolean isServerUri(){
		return this.isServerUri;
	}

	String method;
	private boolean isMethod = false;
		
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
		this.isKey = true;
	}
	public boolean isKey() {
		return this.isKey;
	}

	public Map getAdditionalParameters() {
		return additionalParameters;
	}
	public void setAdditionalParameters(Map additionalParameters) {
		this.additionalParameters = additionalParameters;
		this.hasAdditionalParams = true;
	}
	
	public boolean hasAdditionalParameters(){
		return this.hasAdditionalParams;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
		this.isURL = true;
	}
	public boolean isUrl(){
		return this.isURL;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
		this.isUserName = true;
	}
	
	public boolean isUserName(){
		return this.isUserName;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
		this.isPassword = true;
	}
	public boolean isPassword(){
		return this.isPassword;
	}
	
	public String getProxyIP() {
		return proxyIP;
	}
	public void setProxyIP(String proxyIP) {
		this.proxyIP = proxyIP;
		this.isProxyIP = true;
	}
	
	public boolean isProxyIP(){
		return this.isProxyIP;
	}
	

	public int getProxyPort() {
		return proxyPort;
	}
	public void setProxyPort(String proxyPort) {
		
		this.proxyPort = Integer.parseInt(proxyPort);
		this.isProxyPort=true;
	}

	public boolean isProxyPort(){
		return this.isProxyPort;
	}
	
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
		this.isContentType = true;
	}
	public boolean isContentType(){
		return this.isContentType;
	}
	
	public String getContentLanguage() {
		return contentLanguage;
	}
	public void setContentLanguage(String contentLanguage) {
		this.contentLanguage = contentLanguage;
		this.isContentLanguage = true;
	}
	public boolean isContentLanguage(){
		return this.isContentLanguage;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
		this.isMethod = true;
	}
	
	public boolean isMethod(){
		return this.isMethod;
	}

	
}

