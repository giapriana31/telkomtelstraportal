package com.tt.bean;

import java.util.List;

import com.tt.bean.ResponseObject;

public class CustomerList extends ResponseObject {

	public CustomerList() {
		// TODO Auto-generated constructor stub
	}
	
	List<Customer> custList;

	public List<Customer> getCustList() {
		return custList;
	}

	public void setCustList(List<Customer> custList) {
		this.custList = custList;
	}

}
