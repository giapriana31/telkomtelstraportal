package com.tt.bean;

import java.io.Serializable;
import java.util.List;

public class Incident implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List result;

	public List getResult() {
		return result;
	}

	public void setResult(List result) {
		this.result = result;
	}
	
	private Long id;
	private String name;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
