package com.tt.bean;

import com.tt.bean.ResponseObject;

public class Customer extends ResponseObject
{
    private String[] tags;

    private String phone;

    private String index;

    private String favoriteFruit;

    private String greeting;

    private String about;

    private String guid;

    private String isActive;

    private String picture;

    private String balance;

    private Friends[] friends;

    private String _id;

    private String address;

    private String eyeColor;

    private String email;

    private String registered;

    private String age;

    private String name;

    private String company;

    private String gender;

    private String longitude;

    private String latitude;

    public String[] getTags ()
    {
        return tags;
    }

    public void setTags (String[] tags)
    {
        this.tags = tags;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getIndex ()
    {
        return index;
    }

    public void setIndex (String index)
    {
        this.index = index;
    }

    public String getFavoriteFruit ()
    {
        return favoriteFruit;
    }

    public void setFavoriteFruit (String favoriteFruit)
    {
        this.favoriteFruit = favoriteFruit;
    }

    public String getGreeting ()
    {
        return greeting;
    }

    public void setGreeting (String greeting)
    {
        this.greeting = greeting;
    }

    public String getAbout ()
    {
        return about;
    }

    public void setAbout (String about)
    {
        this.about = about;
    }

    public String getGuid ()
    {
        return guid;
    }

    public void setGuid (String guid)
    {
        this.guid = guid;
    }

    public String getIsActive ()
    {
        return isActive;
    }

    public void setIsActive (String isActive)
    {
        this.isActive = isActive;
    }

    public String getPicture ()
    {
        return picture;
    }

    public void setPicture (String picture)
    {
        this.picture = picture;
    }

    public String getBalance ()
    {
        return balance;
    }

    public void setBalance (String balance)
    {
        this.balance = balance;
    }

    public Friends[] getFriends ()
    {
        return friends;
    }

    public void setFriends (Friends[] friends)
    {
        this.friends = friends;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getEyeColor ()
    {
        return eyeColor;
    }

    public void setEyeColor (String eyeColor)
    {
        this.eyeColor = eyeColor;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getRegistered ()
    {
        return registered;
    }

    public void setRegistered (String registered)
    {
        this.registered = registered;
    }

    public String getAge ()
    {
        return age;
    }

    public void setAge (String age)
    {
        this.age = age;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCompany ()
    {
        return company;
    }

    public void setCompany (String company)
    {
        this.company = company;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tags = "+tags+", phone = "+phone+", index = "+index+", favoriteFruit = "+favoriteFruit+", greeting = "+greeting+", about = "+about+", guid = "+guid+", isActive = "+isActive+", picture = "+picture+", balance = "+balance+", friends = "+friends+", _id = "+_id+", address = "+address+", eyeColor = "+eyeColor+", email = "+email+", registered = "+registered+", age = "+age+", name = "+name+", company = "+company+", gender = "+gender+", longitude = "+longitude+", latitude = "+latitude+"]";
    }
}
