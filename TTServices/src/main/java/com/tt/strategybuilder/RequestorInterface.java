/**
 * 
 */
package com.tt.strategybuilder;

import java.util.List;
import java.util.Map;

import com.tt.bean.RequestObject;
import com.tt.bean.ResponseObject;

/**
 * @author Arul_Arumugam
 * Interface to hold the behaviour pattern decision
 */
public interface RequestorInterface {
	public ResponseObject getResultDate(RequestObject resObj);
	//public List getList(String key,RequestObject resObj);
}
