package com.tt.strategybuilder;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tt.strategybuilder.DBServiceCall;
import com.tt.strategybuilder.FileSystemCall;
import com.tt.strategybuilder.RequestorInterface;
import com.tt.strategybuilder.RestFullServiceCall;
import com.tt.strategybuilder.SOAPServiceCall;

import com.tt.singletonbuilder.ResourceHandler;
import com.tt.bean.RequestObject;
import com.tt.bean.ResponseObject;

public class ServiceRequestor {

	private RequestorInterface behaviour;
	private String key;
	private String strbehaviour;
	
	public ServiceRequestor(String key)	{
		this.key = key;
		String[] s = key.split("_");
		this.strbehaviour = s[s.length-2];
				
		if(strbehaviour.equalsIgnoreCase("rest"))
			this.behaviour = new RestFullServiceCall();
		if(strbehaviour.equalsIgnoreCase("soap"))
			this.behaviour = new SOAPServiceCall();
		if(strbehaviour.equalsIgnoreCase("db"))
			this.behaviour = new DBServiceCall();
		if(strbehaviour.equalsIgnoreCase("file"))
			this.behaviour = new FileSystemCall();
	}	

	public RequestorInterface getBehaviour()	{		
			return behaviour;
	}	
	
	/*
	 * executeMethod is the key method for this service framework
	 */
	public ResponseObject executeMethod(RequestObject reqObject)	{

		Map valuesmap = ResourceHandler.getInstance().getConfigByKeyMap(this.key);
		reqObject.setKey(this.key);
		Iterator it = valuesmap.entrySet().iterator();
		Map mp = new HashMap();
		boolean hasaddParams = false;
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        String subkey = (String) pair.getKey();
	        String subvalue = (String) pair.getValue();

	        //startegy pattern to decide the behaviour using the given key.
	        if(subkey.equalsIgnoreCase("URL")){
	        	reqObject.setUrl(subvalue);
	        }else if(subkey.equalsIgnoreCase("USERNAME")){
	        	reqObject.setUsername(subvalue);
	        }else if(subkey.equalsIgnoreCase("PASSWORD")){
	        	reqObject.setPassword(subvalue);
	        }else if(subkey.equalsIgnoreCase("PROXY_IP")){
	        	reqObject.setProxyIP(subvalue);
	        }else if(subkey.equalsIgnoreCase("PROXY_PORT")){
	        	reqObject.setProxyPort(subvalue);
	        }else if(subkey.equalsIgnoreCase("CONTENT_TYPE")){
	        	reqObject.setContentType(subvalue);
	        }else if(subkey.equalsIgnoreCase("CONTENT_LANGUAGE")){
	        	reqObject.setContentLanguage(subvalue);
	        }else if(subkey.equalsIgnoreCase("METHOD")){
	        	reqObject.setMethod(subvalue);
	        }else if(subkey.equalsIgnoreCase("SERVERURI")){
	        	reqObject.setServerUri(subvalue);
	        }else {
	        	//if there is any additional parameter that would be set in additional params map
	        	//this data would be handled in the service requestor call based on the need.
	        	mp.put(subkey,subvalue);
	        	//below attribute would be used to handle the additional parameters
	        	hasaddParams = true;
	        }
	    
	    }
	    if(hasaddParams)
	    	reqObject.setAdditionalParameters(mp);

	    ResponseObject rs = behaviour.getResultDate(reqObject);
		return rs;
	}	
	public String getKey() {		
		return key;
	}	
	public void setKey(String key) {		
		this.key = key;
	}
	
	
}
