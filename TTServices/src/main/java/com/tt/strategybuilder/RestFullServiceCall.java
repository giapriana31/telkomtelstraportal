package com.tt.strategybuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
//import org.apache.commons.codec.binary.Base64;


import java.util.HashMap;
import java.util.Map;

import com.tt.bean.RequestObject;
import com.tt.bean.ResponseObject;

/*
 * This class takes responsible to invoke any rest web services call.
 */
public class RestFullServiceCall implements RequestorInterface {

	public RestFullServiceCall() {
		// TODO Auto-generated constructor stub
	}

	
public ResponseObject getResultDate(RequestObject reqObj){	
		
		System.out.println(" Called RestfullServiceCall Received url "+reqObj.getUrl());
		String method="GET";
		String contentType ="application/x-www-form-urlencoded";
		String contentLanguage ="en-US";
		
		ResponseObject responseObject = new ResponseObject();
		SocketAddress addr = null;
		Proxy proxy = null;
		String plainCreds ="";
		Map additionalParameters = new HashMap();
		String stradditionalParams = null;
		try{
			//URL connection to connect and receive JSON
			URL url = new URL(reqObj.getUrl());
			HttpURLConnection conn = null;

			System.out.println(" Method "+method);
			try{
	            if( reqObj.isProxyIP() && reqObj.isProxyPort()){
	            	addr = new InetSocketAddress(reqObj.getProxyIP(), reqObj.getProxyPort());
	            	proxy = new Proxy(Proxy.Type.HTTP, addr);
	            	conn = (HttpURLConnection) url.openConnection(proxy);
	            }else{
	            	conn = (HttpURLConnection) url.openConnection();
	            }
	            if(reqObj.isUserName() && reqObj.isPassword()) {
	            	//plainCreds = "nikita_b:nikita19";
	            	plainCreds = reqObj.getUsername()+":"+reqObj.getPassword();
	            }
				//String encode	dAuthString = new Base64().encode(plainCreds);
				//.encodeToString(plainCreds.getBytes());
				String basicAuth = "Basic " + plainCreds;
				conn.setRequestProperty ("Authorization", basicAuth);
			//	System.out.println("encodedAuthString  Basic--->>" + "Basic "+ encodedAuthString);
				if (reqObj.hasAdditionalParameters()){
					additionalParameters = reqObj.getAdditionalParameters();
					//converting map to String and making as key=value&key2=value2 pattern
					//stradditionalParams = additionalParameters.toString().replace("{", "").replace("}","").replace(", ", "&");
					stradditionalParams = additionalParameters.toString();
				}
	
				if( reqObj.isMethod()){
					method = reqObj.getMethod();
				}
				conn.setRequestMethod(method);
				
				if(reqObj.isContentType())
						contentType = reqObj.getContentType();
				if(reqObj.isContentLanguage())
						contentLanguage = reqObj.getContentLanguage();
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				//conn.setRequestProperty("Content-Length", "" + Integer.toString(postData.getBytes().length));
				conn.setRequestProperty("Content-Language", "en-US");
				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);
				
			}catch(Exception ex){
				System.out.println(" Exception while connecting URL");
				ex.printStackTrace();
			}
			
			// Passing input params
			 //	String input = "{\"qty\":100,\"name\":\"iPad 4\"}";
 
				OutputStream os = conn.getOutputStream();
				os.write(stradditionalParams.getBytes());
				os.flush();
			 
			//Checking response code before try reading the data.
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "+ conn.getResponseCode());
			}
			
			//output from the URL captured using input stream
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output="";
			String temp;
			//Iterating the output from the source system
			while ((temp = br.readLine()) != null) {
				 output += temp;
			}
			//System.out.println(output); //Enabled this for debug.
			//Setting the response jsonstring into Responseobject's attribute jsondata.
			responseObject.setJsondata(output);
			
		}catch(Exception ex){
			System.out.println(" Exception while trying with app context xml "+ex);
			ex.printStackTrace();
			//return null;
		}
		
		return responseObject;
	}
}
