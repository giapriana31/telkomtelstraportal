package com.tt.singletonbuilder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;

import com.tt.singletonbuilder.ResourceHandler;

/*
 * Note: confFile path should be changed when you copy to any new location.
 * ResourceHandle is taking responsible to read the property file and manage to give mapped data
 */
public class ResourceHandler {

	private static ResourceHandler resourceHandlerObj;
	private static String confFile = "";	
	private static Properties property = new Properties();
	
	final static String SEMICOLOM_DELIMITOR =";";
	final static String COMMA_DELIMITOR =",";
	final static String OPEN_BRACE_DELIMITOR="{";
	final static String CLOSE_BRACE_DELIMITOR="}";
	final static String EQUALTO_DELIMITOR ="=:";
	private Map keymap = new HashMap();

	/**
	 * Create private constructor
	 */
	private ResourceHandler() {

	}

	public static synchronized  ResourceHandler getInstance() {
		try {
			if (null == resourceHandlerObj) {
				resourceHandlerObj = new ResourceHandler();
				resourceHandlerObj.manageMappingURL();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			resourceHandlerObj = null;
		}
		return resourceHandlerObj;
	}

	public Map getConfigByKeyMap(String key) {
		return (Map)keymap.get(key);
	}

	/**
	 * Service Config reader from properties and parse the same and store it in map
	 */
	private void manageMappingURL() throws Exception {
		try{
			
			Map valuesmap = new HashMap();
			confFile = System.getenv("CONFIG_XML_PATH");
			System.out.println("PATH = " +confFile);

			//Read the property file
			FileReader fr = new FileReader(confFile); 
			BufferedReader br = new BufferedReader(fr); 
			String s; 
			StringBuffer sb= new StringBuffer();
			//iterate each line of property and store it as single string
			while((s = br.readLine()) != null) { 
				sb.append(new StringBuffer(s)); 
			} 
			fr.close(); 
			String  inputString = sb.toString();
			//Tokenize the property file read string with semicolon
			StringTokenizer st1 = new StringTokenizer(inputString,SEMICOLOM_DELIMITOR);
			
			while(st1.hasMoreTokens()){
				valuesmap = new HashMap();
	            String sb1 = st1.nextToken();
	            //This below key will play major role in interacting with multi systems
	            String key = sb1.substring(0, sb1.indexOf(EQUALTO_DELIMITOR));
	            String restofString = sb1.substring(sb1.indexOf(OPEN_BRACE_DELIMITOR)+1,sb1.indexOf(CLOSE_BRACE_DELIMITOR));
	            StringTokenizer st2 = new StringTokenizer(restofString,COMMA_DELIMITOR);
	         // This iterator reads each key-value pair for every key
	            while(st2.hasMoreTokens()){
		            StringBuffer sb2 = new StringBuffer(st2.nextToken());
		            String[] list = sb2.toString().split(EQUALTO_DELIMITOR);
		            valuesmap.put(list[0].trim(), list[1].trim());
	            }
	         //   map inside map, each key will hold one map
	            keymap.put(key.trim(), valuesmap);
	        }
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
