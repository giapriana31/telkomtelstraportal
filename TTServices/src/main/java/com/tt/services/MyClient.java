package com.tt.services;

import org.codehaus.jackson.map.ObjectMapper;

import com.tt.strategybuilder.ServiceRequestor;

import com.tt.services.RemoteServiceManager;
import com.tt.bean.Customer;
import com.tt.bean.CustomerList;
import com.tt.bean.Friends;
import com.tt.bean.RequestObject;
import com.tt.bean.ResponseObject;

public class MyClient {

	public MyClient() {
		// TODO Auto-generated constructor stub
	}
	
public static void main(String[] args) {
		
		System.out.println("I am in ServiceRequestor....Main method called");
		String key1 = "ticketsview_CustomerService_getTickets_rest_get";
		String key2 = "ticketsview_TicketsService_viewTickets_rest_get";
		String key3 = "reportview_ServicesService_getUsageReport_soap_get";
		try{
			//Key values is very important for ServiceRequestor
			ServiceRequestor st = new ServiceRequestor(key2);
			//implicitly key will be taken from the previous step and invokes the method to method to get json data
			ResponseObject resObj = (ResponseObject)st.executeMethod(new RequestObject());
			System.out.println(" RESPONSE DATA in my client :"+resObj.getJsondata());
			//Below two lines of code converts the result json into required object type. 
			//It is requestor responsible to give the required class name while parsing the json data
			ObjectMapper mapper = new ObjectMapper();
			CustomerList custList = mapper.readValue(resObj.getJsondata(), CustomerList.class);
			
			//Iterating the result data
			for(Customer cust:custList.getCustList()){
				System.out.println(" My Client Object values"+ cust.getPhone()+" Converted name "+cust.getIndex());
			}
			
			//Repeating the above step again but this time i get only one object
			System.out.println(" \n \n \n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
/*			ServiceRequestor st1 = new ServiceRequestor(key2);
			st1.executeMethod(new RequestObject());
			
			
			ServiceRequestor st2 = new ServiceRequestor(key1);
			Friends friendObj = (Friends)st2.executeMethod(new RequestObject());
			System.out.println(" FRIEND OBJ RESPONSE "+friendObj.getId()+"   NAME "+friendObj.getName());
		*/
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
}
